Do you seek to command the Rangers of the Edain Settlements, friend #?
These rangers will follow your leadership for a small price, #.
Bring some silver coins and you can have some warriors under your command.
Are you here with the intention of hiring warriors?
I can grant you the leadership of some excellent warriors in return for silver coins.
The Edain Settlements now respect you enough to follow your command, #.
The Warriors of the Edain Settlements only follow good and valiant warriors into battle. You are worthy to lead us.
My men are the finest warriors in all of the Edain Settlements!
I will give you some of my men for a dozen silver coins.
Such great warriors as my men do not fight for nothing, #! They require payment.
You are worthy enough to command my valiant warriors.