Your death shall come swift as the arrow that pierces flesh, #!
I shall make an end of you!
You are not welcome in these lands, #!
Flee our lands, you wretched ally of the Morgoth-filth!
You are an enemy of the race of Men, #!
What madness led you here?
Crawl back into the night, #!
You thought you could match our skill? You are sorely mistaken!
When did # forsake reason for madness?
Your doom approaches, #!
Do not underestimate the Edain Settlements.
Go back to the darkness whence you came!
What devilry brought you here?
Begone from these lands!
You shall meet your doom swiftly, scum!
I will not allow you to harm the free folk of these lands!