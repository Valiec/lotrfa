Only strong friends of the Fair Streams may command our levies, #.
If you wish to command the fighting men of Dínen, you must prove that we can trust you.
Are you friend or foe, #?
They say you are a friend of Dínen, but I cannot be sure until you have done more good.
One does not simply command Dínen levies without earning some reputation first.
You are not yet thought of highly enough to lead our levies into battle, #.
Show us that you can be trusted, #, and we will allow you to lead our soldiers.
You wish to command my levymen? You'll have to prove yourself to the people of Dínen first, #.
Only a true friend of Dínen can lead our valiant soldiers into battle.
You'll have to prove yourself as an ally of Dínen before you can command my levies, #.
You must do more good deeds for the people of Dínen if you wish to command our armies.
Prove your allegiance to the Fair Streams and I will let you command my levymen.