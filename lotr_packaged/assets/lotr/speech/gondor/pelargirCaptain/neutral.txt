Only strong friends of this area may command our armies, #.
If you wish to command our forces, you must prove that we can trust you.
Are you friend or foe, #?
They say you are friendly, but I cannot be sure until you have done more good.
One does not simply command my soldiers without earning some reputation first.
You are not yet thought of highly enough to lead our soldiers into battle, #.
Show us that you can be trusted, #, and we will allow you to lead our soldiers.
You wish to command my soldiers? You'll have to prove yourself to the people of this area first, #.
Only a true friend of our people can lead our valiant warriors into battle.
You'll have to prove yourself as an ally of this colony before you can command my tridents, #.
You must do more good deeds for the people of this colony if you wish to command our armies.
Prove your allegiance to this area and I will let you command my warriors.