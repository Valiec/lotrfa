Only strong friends of Dor-lómin may command our armies, #.
If you wish to command the forces of Dor-lómin, you must prove that we can trust you.
Are you friend or foe, #?
They say you are a friend of Dor-lómin, but I cannot be sure until you have done more good.
One does not simply command our soldiers without earning some reputation first.
You are not yet thought of highly enough to lead our soldiers into battle, #.
Show us that you can be trusted, #, and we will allow you to lead our soldiers.
You wish to command my soldiers? You'll have to prove yourself to the people of Dor-lómin first, #.
Only a true friend of Dor-lómin can lead our gallant soldiers into battle.
You'll have to prove yourself as an ally of Dor-lómin before you can command my swords, #.
You must do more good deeds for the people of these hills if you wish to command our armies.
Prove your allegiance to Dor-lómin and I will let you command my gallant warriors.