Our women are so alike in voice and appearance that they are often mistaken for our men.
Roaring fires! Malt beer! Ripe meat off the bone!
Let us drink together!
Shall we drink, #?
Dwarven ale is the finest drink in all Middle-earth!
The wealth of the Dwarves is surpassed by no other race.
There is some good stonework here.
What tidings do you bring from the world of Men and Elves, #?
My beard is the envy of all clans.
You need to work on that beard, #.
The smith-skill of the Dwarves is unmatched!
Do you have news from our folk in the East?
Welcome to my home, #! 
You're a bit too tall for these mines, #! Ha! Ha!
Welcome to the Ered Luin, #! Many riches await you here!
Do you have news from our folk around the world?
You're a bit too tall for these mines, #! Ha! Ha!
Welcome to our clan, #! Many riches await you here.