Anfalas has the best spots to go fetch some Gondorians for the market.
I ain't sailing North no more. Been trading with the Gulfings instead. 
Aye, I visited Ain an-Ahâr. Ain't that special, really.
Furthest inland I've been would be Ajtiaz an-Ahâr.
Aye, I've sold off some slaves in my time. What of it?
I just got all of these from a fresh plunderin' trip.
Want some of me spoils, #?
Aye, I can part with some of these. What'ye like?
It won't come cheap, ye know? We had to fish some of these things out of shipwrecks.
If yer looking for slaves, they'd be halfway across the Arênin by now.
Ye seem like the type who'd know a good deal when ye see one. I respect that.
Could I int'rest ye in some of me wares?
I'm always open for another trade. What are ye looking for?
Competition drives better bargains, #.
I saw ye look, yes, me wares are THAT good!
Ye have an eye for detail, am I right? Take a closer look at the quality of me wares, #.
Ey, have ye come to trade with me?
I am sure I have something in me wares that'll interest ye, #!
A good trader ne'er says no to nother trade.
I think that's a price we can agree on.
I've already given ye me lowest price, #!
Ye drive a hard bargain...
Meet me halfway here, I also have to make a living.
Greetings, #! Come, see the beauty of the wares I brought along!
What is it that ye wish to buy? For I sell treasures from all over the world!
I'm prepared to barter silver with ye, #. What is it that ye're needing?
Welcome to the bazaar, #! What can I offer ye today?
Stolen treasures from the ends of the earth, for just a few silver coins!
Welcome, #! Welcome! I offer ye many fine goods from other lands!
For a few silver coins, I can sell ye some of our loot!
Welcome to me stall! What can I get ye?
I offer ye many treasures from many lands!
Used to be a Corsair, I did. Can still fight too, aye.
Should go back to being a Corsair. They always paid much better than these bleedin' bazaars.