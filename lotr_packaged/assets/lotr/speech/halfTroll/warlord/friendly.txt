I hear that you want some blade-swingers. Aye. I can give you some.
You want some boys? I've got some!
You are worthy enough to command our mighty armies, #.
The Men of the North shall burn! Will you command our forces, #?
Give some coins and I'll give you some troops!
Do you wish to hunt for some Man-flesh? My boys can help you. For some coins! Ha!
My boys will outmatch any pitiful North-man!
For just a few coins, you can be commanding the mightiest warriors in all of Harad!
If you wish to command our mighty warriors, you'll need to pay me some coins, #.
War is calling, #. Shall you command us?
Can you command my troops, #? You'd better have some coins!
The might of Harad shall be unleashed!
My warriors will not fight for nothing. You'll have to get me some silver coins first, #.
Coins, coins, coins. Do you have them?
War isn't cheap. Can you pay for it, #?
My boys are ready to hunt some Man-flesh!
For just a few silver coins, you can get yourself some strong warriors!
You want some of my soldiers? Get me some coins first!
Give me some coins and I'll give you some of my Half-trolls!
No Man may stand in the way of my mighty warriors. But can you command them?