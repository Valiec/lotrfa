Greetings, #. What can I forge for you?
The smith-skill of my kin is amongst the greatest in these lands!
I have many mighty weapons that I can offer you!
No Orc-arrow may pierce the armour that I forge!
What do you need, mellon-nin?
No spawn of evil may stand against our bows and blades!
What can I get you, #? 
No Orc-blade can match the sharpness and might of my blades!
The foes of our kin shall tremble beneath the mighty blades that I forge!
I am always in need of more metals for my smith-work. I will reward you if you bring me some.
I have learnt much of the skills of forging and smithing in my hundreds of years of walking in this world.