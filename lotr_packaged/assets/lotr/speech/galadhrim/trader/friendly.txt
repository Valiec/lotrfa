Greetings, #!
The fruits of Doriath may be bought for silver coins.
If you desire Elven crafts, I could exchange something for silver coins.
Do you wish to trade, #?
I value silver coins as much as the next merchant.
Greetings, #. Perhaps you have something to trade?
I am an Elf of the Sindar.
I shall soon be leaving these shores.
I bring you the craftwork of Doriath!
Mae govannen, #! Do you wish to trade with me?
I have travelled far and wide... and now I come to you! I have many treasures that will interest you, #.
I hail from Doriath, and I bring many gifts and treasures with me!
You have not seen beauty until you have beheld Menegroth, #!