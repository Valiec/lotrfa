Must get back to work.
I fear the lash...
Freedom? Ha! We have no freedom here, #.
This work is hard, but I must not complain.
The very air is foul here!
All these crops, and not a scrap for us to eat...
Maybe... maybe one day I shall be free.
You servants of darkness are worse than the foulest Morgoth-scum.
I have lived on these farms, and it seems I shall die on these farms.
If I don't get back to work soon, I will get another ten lashes!
I must obey. I must serve my masters.
We will never be free.
My masters' armies do not feed themselves, #! We must do his bidding or face death and torture!
Shall I ever be free?
Is there still hope beyond the walls of Angband?