./gradlew reobf

echo "Clearing lotr_packaged/eoa..."
rm -rf lotr_packaged/eoa
rm -rf lotr_packaged/META-INF
rm -f lotr_packaged/mcmod.info

echo "Unpacking jar..."
jar xf build/libs/lotrfa-build.jar

echo "Moving class files to lotr_packaged"
mv eoa lotr_packaged/eoa
mv META-INF lotr_packaged/META-INF
mv mcmod.info lotr_packaged\mcmod.info

echo "Cleaning workspace"
rm -rf assets

cd lotr_packaged

echo "Packing new jar with manifest..."
jar cmf META-INF/MANIFEST.MF ../lotrfa-$1.jar ./*   
