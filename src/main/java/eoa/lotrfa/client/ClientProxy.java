package eoa.lotrfa.client;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import eoa.lotrfa.client.gui.LOTRFAMapLabels;
import eoa.lotrfa.client.render.entity.*;
import eoa.lotrfa.client.render.item.LOTRFARenderBannerItem;
import eoa.lotrfa.client.render.item.LOTRFARenderLargeItem;
import eoa.lotrfa.common.CommonProxy;
import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.entity.animal.LOTREntityBlackSwan;
import eoa.lotrfa.common.entity.item.LOTRFAEntityBanner;
import eoa.lotrfa.common.entity.item.LOTRFAEntityBannerWall;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.client.LOTRTickHandlerClient;
import lotr.client.render.entity.*;
import lotr.client.render.item.*;
import lotr.common.LOTRMod;
import lotr.common.entity.item.*;
import lotr.common.entity.npc.LOTREntityElf;
import lotr.common.entity.npc.LOTREntityWarg;
import lotr.common.item.*;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;

public class ClientProxy extends CommonProxy {

    @Override
    public void init(FMLInitializationEvent event) {
        LOTRFAMapLabels.init();
    }

    @Override
    public void registerRenderers() {
        System.setProperty("fml.skipFirstTextureLoad", "false");
        
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityUlfangMan.class, new LOTRRenderUlfang());
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityElf.class, new LOTRRenderFAElf());
        
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityAngbandBoldog.class, new LOTRRenderBoldog());
        
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityBrethilMan.class, new LOTRRenderBrethil());
        
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityDorLominMan.class, new LOTRRenderDorLominMan());
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityDorLominVinehand.class, new LOTRRenderDorLominMan());
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityNanDungorthebSpider.class, new LOTRRenderNanDungorthebSpider());
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityAngbandBalrog.class, new LOTRRenderMiniBalrog());
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityBlackSwan.class, new LOTRRenderBlackSwan());

        RenderingRegistry.registerEntityRenderingHandler(LOTREntityTolInGaurhothWraith.class, new LOTRRenderTolInGaurhothWraith());

        RenderingRegistry.registerEntityRenderingHandler(LOTREntityDorDaedelothTroll.class, new LOTRRenderTroll());
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityDorDaedelothMountainTroll.class, new LOTRRenderMountainTroll());

        RenderingRegistry.registerEntityRenderingHandler(LOTREntityDoriathSmith.class, new LOTRRenderElvenSmith("galadhrimSmith_cloak", "galadhrimSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityDoriathBaker.class, new LOTRRenderElvenSmith("galadhrimSmith_cloak", "galadhrimSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityDoriathHuntsman.class, new LOTRRenderElvenSmith("galadhrimSmith_cloak", "galadhrimSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityDoriathLumberman.class, new LOTRRenderElvenSmith("galadhrimSmith_cloak", "galadhrimSmith_cape"));

        RenderingRegistry.registerEntityRenderingHandler(LOTREntityGondolinSmith.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityGondolinFlorist.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityGondolinStonemason.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));

        RenderingRegistry.registerEntityRenderingHandler(LOTREntityHithlumSmith.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityHithlumFlorist.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityHithlumGreengrocer.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityFeanorianStablemaster.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));

        RenderingRegistry.registerEntityRenderingHandler(LOTREntityNargothrondSmith.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityNargothrondGoldsmith.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityNargothrondStonemason.class, new LOTRRenderElvenSmith("highElfSmith_cloak", "highElfSmith_cape"));

        RenderingRegistry.registerEntityRenderingHandler(LOTREntityLaegrimSmith.class, new LOTRRenderElvenSmith("woodElfSmith_cloak", "woodElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityLaegrimFlorist.class, new LOTRRenderElvenSmith("woodElfSmith_cloak", "woodElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityLaegrimHuntsman.class, new LOTRRenderElvenSmith("woodElfSmith_cloak", "woodElfSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityLaegrimLumberman.class, new LOTRRenderElvenSmith("woodElfSmith_cloak", "woodElfSmith_cape"));

        RenderingRegistry.registerEntityRenderingHandler(LOTREntityFalathrimFishmonger.class, new LOTRRenderElvenSmith("galadhrimSmith_cloak", "galadhrimSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityFalathrimSmith.class, new LOTRRenderElvenSmith("galadhrimSmith_cloak", "galadhrimSmith_cape"));
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityFalathrimLumberman.class, new LOTRRenderElvenSmith("galadhrimSmith_cloak", "galadhrimSmith_cape"));
        
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityBanner.class, new LOTRFARenderBanner());
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityBannerWall.class, new LOTRFARenderBannerWall());
        RenderingRegistry.registerEntityRenderingHandler(LOTRFAEntityBanner.class, new LOTRFARenderBanner());
        RenderingRegistry.registerEntityRenderingHandler(LOTRFAEntityBannerWall.class, new LOTRFARenderBannerWall());
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityWarg.class, new LOTRFARenderWarg());
        RenderingRegistry.registerEntityRenderingHandler(LOTREntityWargskinRug.class, new LOTRFARenderWargskinRug());

        for (Item item : LOTRFAReflectionHelper.getAllLOTRFAItems()) {
            if (item == null) continue;
            LOTRFARenderLargeItem largeItemRenderer = LOTRFARenderLargeItem.getLargeIconSize(item);

            boolean large = largeItemRenderer != null;
            if (item instanceof LOTRItemCrossbow) {
                MinecraftForgeClient.registerItemRenderer(item, new LOTRRenderCrossbow());
            }
            else if (item instanceof LOTRItemBow) {
                MinecraftForgeClient.registerItemRenderer(item, new LOTRRenderBow(largeItemRenderer));
            }
            else if (item instanceof LOTRItemSword && ((LOTRItemSword) item).isElvenBlade()) {
                MinecraftForgeClient.registerItemRenderer(item, new LOTRRenderElvenBlade(24.0, largeItemRenderer));
            }
            else if (large) {
                MinecraftForgeClient.registerItemRenderer(item, largeItemRenderer);
            }
        }
        MinecraftForgeClient.registerItemRenderer(LOTRFAItems.banner, new LOTRFARenderBannerItem());
        MinecraftForgeClient.registerItemRenderer(LOTRMod.banner, new LOTRFARenderBannerItem());
    }

    @Override
    public void editTickHanderClient() {
        LOTRTickHandlerClient.currentAlignmentFaction = LOTRFAFactions.brethil;
        LOTRTickHandlerClient.currentDimensionRegion = LOTRFAFactions.DimensionRegions.good;
    }
}
