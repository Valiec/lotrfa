package eoa.lotrfa.client.render.block;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.world.IBlockAccess;

public abstract class BaseSimpleBlockRenderer implements ISimpleBlockRenderingHandler {
    private int renderID;
    private boolean render3DInInventory;

    public BaseSimpleBlockRenderer(int renderID, boolean render3DInInventory) {
        this.renderID = renderID;
        this.render3DInInventory = render3DInInventory;
    }
    
    @Override
    public boolean shouldRender3DInInventory(int modelId) {
        return render3DInInventory;
    }

    @Override
    public int getRenderId() {
        return renderID;
    }
    
    @Override
    public abstract void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer);

    @Override
    public abstract boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer);
}
