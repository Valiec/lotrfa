package eoa.lotrfa.client.render.entity;

import org.lwjgl.opengl.GL11;
import eoa.lotrfa.client.model.entity.LOTRModelMiniBalrog;
import eoa.lotrfa.common.entity.npc.LOTREntityAngbandBalrog;
import lotr.client.render.entity.LOTRRandomSkins;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderMiniBalrog extends RenderLiving {
    private static LOTRRandomSkins balrogSkins;
    private static ResourceLocation fireTexture;
    private LOTRModelMiniBalrog balrogModel;
    private LOTRModelMiniBalrog fireModel = new LOTRModelMiniBalrog(0.0f);

    public LOTRRenderMiniBalrog() {
        super(new LOTRModelMiniBalrog(), 0.5f);
        this.balrogModel = (LOTRModelMiniBalrog) this.mainModel;
        balrogSkins = LOTRRandomSkins.loadSkinsList("lotr:mob/balrog/balrog");
        this.fireModel.setFireModel();
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        return balrogSkins.getRandomSkin((LOTREntityAngbandBalrog) entity);
    }

    @Override
    public void doRender(Entity entity, double d, double d1, double d2, float f, float f1) {
        LOTREntityAngbandBalrog balrog = (LOTREntityAngbandBalrog) entity;
        ItemStack heldItem = balrog.getHeldItem();
        this.fireModel.heldItemRight = heldItem == null ? 0 : 2;
        this.balrogModel.heldItemRight = this.fireModel.heldItemRight;
        super.doRender(balrog, d, d1, d2, f, f1);
    }

    @Override
    protected void preRenderCallback(EntityLivingBase entity, float f) {
        LOTREntityAngbandBalrog balrog = (LOTREntityAngbandBalrog) entity;
        float scale = 2.0f;
        GL11.glScalef(scale, scale, scale);
        if (balrog.isWreathedInFlame()) {
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240.0f, 240.0f);
        }
    }

    @Override
    protected int shouldRenderPass(EntityLivingBase entity, int pass, float f) {
        LOTREntityAngbandBalrog balrog = (LOTREntityAngbandBalrog) entity;
        if (balrog.isWreathedInFlame()) {
            if (pass == 1) {
                OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240.0f, 240.0f);
                GL11.glMatrixMode(5890);
                GL11.glLoadIdentity();
                float f1 = balrog.ticksExisted + f;
                float f2 = f1 * 0.01f;
                float f3 = f1 * 0.01f;
                GL11.glTranslatef(f2, f3, 0.0f);
                GL11.glMatrixMode(5888);
                GL11.glAlphaFunc(516, 0.01f);
                GL11.glEnable(3042);
                GL11.glBlendFunc(1, 1);
                float alpha = 0.3f + MathHelper.sin(f1 * 0.05f) * 0.15f;
                GL11.glColor4f(alpha, alpha, alpha, 1.0f);
                GL11.glDisable(2896);
                GL11.glDepthMask(false);
                this.setRenderPassModel(this.fireModel);
                this.bindTexture(fireTexture);
                return 1;
            }
            if (pass == 2) {
                GL11.glMatrixMode(5890);
                GL11.glLoadIdentity();
                GL11.glMatrixMode(5888);
                GL11.glAlphaFunc(516, 0.1f);
                GL11.glDisable(3042);
                GL11.glEnable(2896);
                GL11.glDepthMask(true);
            }
        }
        return -1;
    }

    @Override
    protected void renderEquippedItems(EntityLivingBase entity, float f) {
        GL11.glColor3f(1.0f, 1.0f, 1.0f);
        ItemStack heldItem = entity.getHeldItem();
        if (heldItem != null) {
            GL11.glPushMatrix();
            this.balrogModel.body.postRender(0.0625f);
            this.balrogModel.rightArm.postRender(0.0625f);
            GL11.glTranslatef(-0.25f, 1.5f, -0.125f);
            float scale = 1.25f;
            GL11.glScalef(scale, (-scale), scale);
            GL11.glRotatef(-100.0f, 1.0f, 0.0f, 0.0f);
            GL11.glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
            this.renderManager.itemRenderer.renderItem(entity, heldItem, 0);
            if (heldItem.getItem().requiresMultipleRenderPasses()) {
                for (int x = 1; x < heldItem.getItem().getRenderPasses(heldItem.getItemDamage()); ++x) {
                    this.renderManager.itemRenderer.renderItem(entity, heldItem, x);
                }
            }
            GL11.glPopMatrix();
        }
    }

    static {
        fireTexture = new ResourceLocation("lotr:mob/balrog/fire.png");
    }
}
