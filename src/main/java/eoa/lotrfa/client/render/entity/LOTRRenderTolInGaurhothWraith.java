package eoa.lotrfa.client.render.entity;

import org.lwjgl.opengl.GL11;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothWraith;
import lotr.client.LOTRTickHandlerClient;
import lotr.client.model.LOTRModelBarrowWight;
import lotr.client.render.entity.LOTRRandomSkins;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderTolInGaurhothWraith extends RenderBiped {

    private static LOTRRandomSkins wraithSkins;

    public LOTRRenderTolInGaurhothWraith() {
        super(new LOTRModelBarrowWight(), 0.0f);
        LOTRRenderTolInGaurhothWraith.wraithSkins = LOTRRandomSkins.loadSkinsList("lotrfa:mob/wraith");
    }

    @Override
    public ResourceLocation getEntityTexture(final Entity entity) {
        final LOTREntityTolInGaurhothWraith wight = (LOTREntityTolInGaurhothWraith) entity;
        return LOTRRenderTolInGaurhothWraith.wraithSkins.getRandomSkin(wight);
    }

    @Override
    public void doRender(final Entity entity, final double d, final double d1, final double d2, final float f, final float f1) {
        super.doRender(entity, d, d1, d2, f, f1);
        final LOTREntityTolInGaurhothWraith wight = (LOTREntityTolInGaurhothWraith) entity;
        if (wight.addedToChunk) {
            final Entity viewer = Minecraft.getMinecraft().renderViewEntity;
            if (viewer != null && wight.getTargetEntityID() == viewer.getEntityId()) {
                LOTRTickHandlerClient.anyWightsViewed = true;
            }
        }
    }

    @Override
    protected void preRenderCallback(final EntityLivingBase entity, final float f) {
        super.preRenderCallback(entity, f);
        final float hover = MathHelper.sin((entity.ticksExisted + f) * 0.05f) * 0.2f;
        GL11.glTranslatef(0.0f, hover, 0.0f);
        if (entity.deathTime > 0) {
            float death = (entity.deathTime + f - 1.0f) / 20.0f;
            death = Math.max(0.0f, death);
            death = Math.min(1.0f, death);
            final float scale = 1.0f + death * 1.0f;
            GL11.glScalef(scale, scale, scale);
            GL11.glEnable(3042);
            GL11.glBlendFunc(770, 771);
            GL11.glEnable(3008);
            GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f - death);
        }
    }

    @Override
    protected float getDeathMaxRotation(final EntityLivingBase entity) {
        return 0.0f;
    }
}
