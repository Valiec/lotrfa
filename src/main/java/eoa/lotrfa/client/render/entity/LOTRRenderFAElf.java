package eoa.lotrfa.client.render.entity;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.tuple.Pair;
import eoa.lotrfa.common.entity.npc.*;
import lotr.client.render.entity.LOTRRandomSkins;
import lotr.client.render.entity.LOTRRenderElf;
import lotr.common.entity.npc.LOTREntityElf;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderFAElf extends LOTRRenderElf {
    
    private static Map<Class<? extends LOTREntityElf>, Pair<LOTRRandomSkins, LOTRRandomSkins>> skinMap;
    
    @Override
    public ResourceLocation getEntityTexture(Entity entity) {
        LOTREntityElf elf = (LOTREntityElf) entity;
        boolean male = elf.familyInfo.isMale();
        
        if (elf.isJazz()) return super.getEntityTexture(elf);
        
        for(Class<? extends LOTREntityElf> elfClass : skinMap.keySet()) {
            if(elfClass.isInstance(elf)) {
                Pair<LOTRRandomSkins, LOTRRandomSkins> elfSkins = skinMap.get(elfClass);
                return male ? elfSkins.getLeft().getRandomSkin(elf) : elfSkins.getRight().getRandomSkin(elf);
            }
        }
        
        return super.getEntityTexture(entity);
    }
    
    private static Pair<LOTRRandomSkins, LOTRRandomSkins> getGenderPair(String npc) {
        return getGenderPair(npc, npc);
    }
    
    private static Pair<LOTRRandomSkins, LOTRRandomSkins> getGenderPair(String faction, String npc) {
        return Pair.of(loadNPCSkins(faction, npc, true), loadNPCSkins(faction, npc, false));
    }
    
    private static LOTRRandomSkins loadNPCSkins(String faction, String npc, boolean male) {
        String gender = male ? "male" : "female";
        String path = "lotrfa:mob/" + faction + "/" + npc + "_" + gender;
        
        return LOTRRandomSkins.loadSkinsList(path);
    }
    
    static {
        skinMap = new HashMap<Class<? extends LOTREntityElf>, Pair<LOTRRandomSkins, LOTRRandomSkins>>();
        skinMap.put(LOTREntityDoriathElf.class, getGenderPair("doriath"));
        skinMap.put(LOTREntityLaegrimElf.class, getGenderPair("laegrim"));
        skinMap.put(LOTREntityHithlumElf.class, getGenderPair("hithlum"));
        skinMap.put(LOTREntityFeanorianElf.class, getGenderPair("feanorian"));
        skinMap.put(LOTREntityFalathrimElf.class, getGenderPair("falathrim"));
        skinMap.put(LOTREntityGondolinElf.class, getGenderPair("gondolin"));
        skinMap.put(LOTREntityNargothrondElf.class, getGenderPair("nargothrond"));
    }
}
