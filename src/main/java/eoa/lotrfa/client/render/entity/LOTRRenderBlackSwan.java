package eoa.lotrfa.client.render.entity;

import lotr.client.render.entity.LOTRRenderSwan;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderBlackSwan extends LOTRRenderSwan {

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        return swanSkin;
    }
    
    static {
        swanSkin = new ResourceLocation("lotrfa:mob/black_swan.png");
    }

}
