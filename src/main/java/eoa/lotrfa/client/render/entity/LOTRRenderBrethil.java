package eoa.lotrfa.client.render.entity;

import eoa.lotrfa.common.entity.npc.LOTREntityBrethilMan;
import eoa.lotrfa.common.entity.npc.LOTREntityBrethilSoldier;
import lotr.client.model.LOTRModelHuman;
import lotr.client.render.entity.LOTRRandomSkins;
import lotr.client.render.entity.LOTRRenderBiped;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderBrethil extends LOTRRenderBiped {

    private static LOTRRandomSkins skinsMale;
    private static LOTRRandomSkins skinsFemale;
    private static LOTRRandomSkins skinsSoldier;
    private static LOTRRandomSkins skinsRanger;
    
    protected ModelBiped outfitModel;

    public LOTRRenderBrethil() {
        super(new LOTRModelHuman(), 0.5f);
        this.setRenderPassModel(this.outfitModel = new LOTRModelHuman(0.6f, false));
        LOTRRenderBrethil.skinsMale = LOTRRandomSkins.loadSkinsList("lotrfa:mob/brethil/brethil_male");
        LOTRRenderBrethil.skinsFemale = LOTRRandomSkins.loadSkinsList("lotrfa:mob/brethil/brethil_female");
        LOTRRenderBrethil.skinsSoldier = LOTRRandomSkins.loadSkinsList("lotrfa:mob/brethil/brethil_soldier");
        LOTRRenderBrethil.skinsRanger = LOTRRandomSkins.loadSkinsList("lotrfa:mob/brethil/brethil_ranger");
    }

    @Override
    public ResourceLocation getEntityTexture(Entity entity) {
        LOTREntityBrethilMan npc = (LOTREntityBrethilMan) entity;
        if (!npc.familyInfo.isMale()) {
            return LOTRRenderBrethil.skinsFemale.getRandomSkin(npc);
        }
        if (npc instanceof LOTREntityBrethilSoldier) {
            return LOTRRenderBrethil.skinsSoldier.getRandomSkin(npc);
        }   
        /*if (npc instanceof LOTREntityBrethilRanger) {
            return LOTRRenderBrethil.skinsRanger.getRandomSkin(npc);
        }*/
        return LOTRRenderBrethil.skinsMale.getRandomSkin(npc);
    }
}
