package eoa.lotrfa.client.render.entity;

import eoa.lotrfa.common.entity.npc.LOTREntityNanDungorthebSpider;
import lotr.client.render.entity.LOTRRenderSpiderBase;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderNanDungorthebSpider extends LOTRRenderSpiderBase {

    private static ResourceLocation[] spiderSkins = new ResourceLocation[] {new ResourceLocation("lotr:mob/spider/spider_mirkwood.png"), new ResourceLocation("lotr:mob/spider/spider_mirkwoodSlowness.png"), new ResourceLocation("lotr:mob/spider/spider_mirkwoodPoison.png")};

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        LOTREntityNanDungorthebSpider spider = (LOTREntityNanDungorthebSpider) entity;
        return spiderSkins[spider.getSpiderType()];
    }
}
