package eoa.lotrfa.client.render.entity;

import eoa.lotrfa.common.entity.npc.LOTREntityDorLominMan;
import eoa.lotrfa.common.entity.npc.LOTREntityDorLominSoldier;
import lotr.client.model.LOTRModelHuman;
import lotr.client.render.entity.LOTRRandomSkins;
import lotr.client.render.entity.LOTRRenderBiped;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderDorLominMan extends LOTRRenderBiped {

    private static LOTRRandomSkins skinsMale;
    private static LOTRRandomSkins skinsFemale;
    private static LOTRRandomSkins skinsSoldier;
    protected ModelBiped outfitModel;

    public LOTRRenderDorLominMan() {
        super(new LOTRModelHuman(), 0.5f);
        this.setRenderPassModel(this.outfitModel = new LOTRModelHuman(0.6f, false));
        LOTRRenderDorLominMan.skinsMale = LOTRRandomSkins.loadSkinsList("lotrfa:mob/dorLomin/dorLomin_male");
        LOTRRenderDorLominMan.skinsFemale = LOTRRandomSkins.loadSkinsList("lotrfa:mob/dorLomin/dorLomin_female");
        LOTRRenderDorLominMan.skinsSoldier = LOTRRandomSkins.loadSkinsList("lotrfa:mob/dorLomin/dorLomin_soldier");
    }

    @Override
    public ResourceLocation getEntityTexture(Entity entity) {
        LOTREntityDorLominMan npc = (LOTREntityDorLominMan) entity;
        if (!npc.familyInfo.isMale()) {
            return LOTRRenderDorLominMan.skinsFemale.getRandomSkin(npc);
        }
        if (npc instanceof LOTREntityDorLominSoldier) {
            return LOTRRenderDorLominMan.skinsSoldier.getRandomSkin(npc);
        }
        return LOTRRenderDorLominMan.skinsMale.getRandomSkin(npc);
    }
}