package eoa.lotrfa.client.render.entity;

import eoa.lotrfa.common.entity.npc.LOTREntityAngbandBoldog;
import lotr.client.model.LOTRModelHalfTroll;
import lotr.client.render.entity.LOTRRandomSkins;
import lotr.client.render.entity.LOTRRenderBiped;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderBoldog extends LOTRRenderBiped
{
    private static LOTRRandomSkins boldogSkins;
    
    public LOTRRenderBoldog() {
        super(new LOTRModelHalfTroll(), 0.5f);
        LOTRRenderBoldog.boldogSkins = LOTRRandomSkins.loadSkinsList("lotrfa:mob/boldog");
    }
    
    @Override
    protected void func_82421_b() {
        this.field_82423_g = new LOTRModelHalfTroll(1.0f);
        this.field_82425_h = new LOTRModelHalfTroll(0.5f);
    }
    
    @Override
    public ResourceLocation getEntityTexture(final Entity entity) {
        final LOTREntityAngbandBoldog halfTroll = (LOTREntityAngbandBoldog)entity;
        return LOTRRenderBoldog.boldogSkins.getRandomSkin(halfTroll);
    }
    
    @Override
    public float getHeldItemYTranslation() {
        return 0.45f;
    }
}
