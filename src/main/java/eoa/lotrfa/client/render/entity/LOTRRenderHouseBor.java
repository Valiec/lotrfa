package eoa.lotrfa.client.render.entity;

import eoa.lotrfa.common.entity.npc.LOTREntityBorMan;
import eoa.lotrfa.common.entity.npc.LOTREntityBorSoldier;
import lotr.client.model.LOTRModelHuman;
import lotr.client.render.entity.LOTRRandomSkins;
import lotr.client.render.entity.LOTRRenderBiped;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderHouseBor extends LOTRRenderBiped {

    private static LOTRRandomSkins skinsMale;
    private static LOTRRandomSkins skinsFemale;
    private static LOTRRandomSkins skinsSoldier;
    
    protected ModelBiped outfitModel;

    public LOTRRenderHouseBor() {
        super(new LOTRModelHuman(), 0.5f);
        this.setRenderPassModel(this.outfitModel = new LOTRModelHuman(0.6f, false));
        LOTRRenderHouseBor.skinsMale = LOTRRandomSkins.loadSkinsList("lotrfa:mob/houseBor/houseBor_male");
        LOTRRenderHouseBor.skinsFemale = LOTRRandomSkins.loadSkinsList("lotrfa:mob/houseBor/houseBor_female");
        LOTRRenderHouseBor.skinsSoldier = LOTRRandomSkins.loadSkinsList("lotrfa:mob/houseBor/houseBor_soldier");
    }

    @Override
    public ResourceLocation getEntityTexture(Entity entity) {
        LOTREntityBorMan npc = (LOTREntityBorMan) entity;
        if (!npc.familyInfo.isMale()) {
            return LOTRRenderHouseBor.skinsFemale.getRandomSkin(npc);
        }
        if (npc instanceof LOTREntityBorSoldier) {
            return LOTRRenderHouseBor.skinsSoldier.getRandomSkin(npc);
        }   
        /*if (npc instanceof LOTREntityBrethilRanger) {
            return LOTRRenderBrethil.skinsRanger.getRandomSkin(npc);
        }*/
        return LOTRRenderHouseBor.skinsMale.getRandomSkin(npc);
    }
}
