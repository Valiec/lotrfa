package eoa.lotrfa.client.render.entity;

import eoa.lotrfa.client.model.entity.LOTRFAModelWargskinRug;
import lotr.client.render.entity.LOTRRenderRugBase;
import lotr.common.entity.item.LOTREntityWargskinRug;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class LOTRFARenderWargskinRug extends LOTRRenderRugBase {
    public LOTRFARenderWargskinRug() {
        super(new LOTRFAModelWargskinRug());
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        LOTREntityWargskinRug rug = (LOTREntityWargskinRug) entity;
        return LOTRFARenderWarg.getWargSkin(rug.getRugType());
    }

}
