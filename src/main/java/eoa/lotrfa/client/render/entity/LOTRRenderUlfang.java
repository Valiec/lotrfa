package eoa.lotrfa.client.render.entity;

import eoa.lotrfa.common.entity.npc.LOTREntityUlfangBerserker;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangMan;
import lotr.client.model.LOTRModelHuman;
import lotr.client.render.entity.LOTRRandomSkins;
import lotr.client.render.entity.LOTRRenderBiped;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class LOTRRenderUlfang extends LOTRRenderBiped{
	
    private static LOTRRandomSkins ulfangSkinsMale;
    private static LOTRRandomSkins ulfangSkinsFemale;
    private static LOTRRandomSkins ulfangSkinsBerserker;
    protected ModelBiped outfitModel;
    
    public LOTRRenderUlfang() {
        super(new LOTRModelHuman(), 0.5f);
        this.setRenderPassModel(this.outfitModel = new LOTRModelHuman(0.6f, false));
        LOTRRenderUlfang.ulfangSkinsMale = LOTRRandomSkins.loadSkinsList("lotrfa:mob/houseUlfang/ulfang_male");
        LOTRRenderUlfang.ulfangSkinsFemale = LOTRRandomSkins.loadSkinsList("lotrfa:mob/houseUlfang/ulfang_female");
        LOTRRenderUlfang.ulfangSkinsBerserker = LOTRRandomSkins.loadSkinsList("lotrfa:mob/houseUlfang/ulfang_berserker");
    }
    
    @Override
    public ResourceLocation getEntityTexture(final Entity entity) {
        LOTREntityUlfangMan npc = (LOTREntityUlfangMan) entity;
        
        if (npc instanceof LOTREntityUlfangBerserker) {
            return LOTRRenderUlfang.ulfangSkinsBerserker.getRandomSkin(npc);
        }
        
        if (npc.familyInfo.isMale()) {
            return LOTRRenderUlfang.ulfangSkinsMale.getRandomSkin(npc);
        }
        return LOTRRenderUlfang.ulfangSkinsFemale.getRandomSkin(npc);
    }

}
