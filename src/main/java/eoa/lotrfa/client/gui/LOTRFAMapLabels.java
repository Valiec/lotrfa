package eoa.lotrfa.client.gui;

import eoa.lotrfa.common.reflection.LOTRFAEnumHelper;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import eoa.lotrfa.common.world.biome.LOTRFABiomes;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.world.biome.LOTRBiome;

public class LOTRFAMapLabels {

    public static void init() {
        removeMapLabels(); // TODO actually remove them from enum not just hide.
        addMapLabels();
    }

    private static void removeMapLabels() {
        for (LOTRMapLabels label : LOTRMapLabels.values()) LOTRFAReflectionHelper.removeMapLabel(label);
    }

    private static void addMapLabels() {
        // LOTRFAEnumHelper.addMapLabel("ENUMNAME", "stringName", 1, 1, 1, 1, 1, 2);
        // LOTRFAEnumHelper.addMapLabel("ENUMNAME2", LOTRFABiomes.aelinUial, 1, 1, 1, 1, 1, 2);
    	
        LOTRFAEnumHelper.addMapLabel("BELEGAER", "belegaer", 328, 800, 12.0f, 0, -6.0f, -1.0f);
        LOTRFAEnumHelper.addMapLabel("BELEGAER_SUB", "belegaer_sub", 328, 890, 5.0f, 0, -6.0f, -1.0f);
    	
    	LOTRFAEnumHelper.addMapLabel("DOR_DAIDELOS", LOTRBiome.forodwaith, 964, 275, 8.0f, 10, -5.5f, 0.5f);
    	LOTRFAEnumHelper.addMapLabel("HELCARAXE", LOTRFABiomes.helcaraxe, 457, 272, 6.0f, -10, -3.5f, 0.5f);
    	LOTRFAEnumHelper.addMapLabel("NAN_GURUTHOS", LOTRFABiomes.nanGuruthos, 1129, 371, 1.0f, -30, -1.5f, 1.5f);
    	
    	LOTRFAEnumHelper.addMapLabel("ANFAUGLITH", LOTRFABiomes.anfauglith, 911, 462, 5.0f, 5, -5.5f, -0.5f);
    	LOTRFAEnumHelper.addMapLabel("DOR_DAEDELOTH", LOTRFABiomes.dorDaedeloth, 860, 456, 2.5f, -2, -0.5f, 1.5f);
    	LOTRFAEnumHelper.addMapLabel("ERED_ENGRIN", LOTRFABiomes.eredEngrin, 880, 400, 4.0f, 5, -3.5f, 1.5f);
    	LOTRFAEnumHelper.addMapLabel("LOTHLANN", "lothlann", 1006, 470, 2.5f, -30, -0.5f, 1.5f);
    	LOTRFAEnumHelper.addMapLabel("NAN_DUAITH", LOTRFABiomes.nanDuaith, 707, 390, 1.0f, 40, -1.5f, 2.5f);
    	LOTRFAEnumHelper.addMapLabel("MARCH_MAEDHROS", LOTRFABiomes.marchMaedhros, 952, 510, 0.5f, 10, -0.5f, 2.5f);
    	LOTRFAEnumHelper.addMapLabel("TAURDRU", LOTRFABiomes.taurdru, 1004, 499, 1.0f, -50, -0.5f, 2.5f);
    	
    	LOTRFAEnumHelper.addMapLabel("HITHLUM", LOTRFABiomes.hithlum, 660, 458, 2.0f, 0, -5.5f, 2.5f);
    	LOTRFAEnumHelper.addMapLabel("DOR_LOMIN", LOTRFABiomes.dorLomin, 670, 517, 1.0f, 0, -1.5f, 2.5f);
    	LOTRFAEnumHelper.addMapLabel("MITHRIM", LOTRFABiomes.mithrim, 710, 465, 1.0f, -15, -0.5f, 2.5f);
		LOTRFAEnumHelper.addMapLabel("ERED_WETHRIN", LOTRFABiomes.eredWethrin, 714, 523, 1.5f, -30, -2.5f, 2.5f);
		LOTRFAEnumHelper.addMapLabel("ERED_LOMIN", LOTRFABiomes.eredLomin, 593, 446, 2.0f, 60, -2.5f, 2.5f);
		LOTRFAEnumHelper.addMapLabel("NEVRAST", LOTRFABiomes.nevrast, 603, 540, 1.0f, -30, -0.5f, 2.5f);
		LOTRFAEnumHelper.addMapLabel("LAMMOTH", LOTRFABiomes.lammoth, 562, 459, 2.0f, 50, -1.5f, 1.5f);
		LOTRFAEnumHelper.addMapLabel("FIRTH_DRENGIST", "firthDrengist", 582, 489, 0.5f, 30, 1.5f, 2.5f);
		
		LOTRFAEnumHelper.addMapLabel("HIMLAD", LOTRFABiomes.himlad, 902, 553, 2.0f, -30, -0.5f, 2.5f);
		LOTRFAEnumHelper.addMapLabel("HIMRING", "himring", 953, 526, 1.0f, -2, -0.5f, 2.5f);
		LOTRFAEnumHelper.addMapLabel("THARGELION", "thargelion", 1004, 588, 2.0f, 70, -1.5f, 2.5f);
		
		LOTRFAEnumHelper.addMapLabel("TAUR_NU_FUIN", LOTRFABiomes.taurNuFuin, 832, 504, 2.0f, -5, -1.5f, 0.5f);
		LOTRFAEnumHelper.addMapLabel("ERED_GORGOROTH", LOTRFABiomes.eredGorgoroth, 851, 527, 1.0f, -10, 0.5f, 2.5f);
		
		LOTRFAEnumHelper.addMapLabel("EAST_BELERIAND", LOTRFABiomes.beleriandEast, 909, 647, 2.5f, -5, -5.5f, -0.5f);
		LOTRFAEnumHelper.addMapLabel("ANDRAM", "andram", 794, 656, 1.5f, 10, -0.5f, 2.5f);
		
		LOTRFAEnumHelper.addMapLabel("WEST_BELERIAND", LOTRFABiomes.beleriandWest, 687, 645, 2.5f, 30, -5.5f, -0.5f);
		LOTRFAEnumHelper.addMapLabel("TALATH_DIRNEN", LOTRFABiomes.talathDirnen, 725, 603, 1.5f, 50, -0.5f, 2.5f);
		LOTRFAEnumHelper.addMapLabel("FALAS", LOTRFABiomes.falas, 606, 649, 4.0f, 45, -0.5f, 2.5f);
		LOTRFAEnumHelper.addMapLabel("ARVERNIEN", LOTRFABiomes.arvernien, 719, 743, 2.5f, -10, -0.5f, 2.5f);
		
		LOTRFAEnumHelper.addMapLabel("TAUR_IM_DUINATH", LOTRFABiomes.taurImDuinath, 855, 817, 2.5f, -45, -2.5f, 2.5f);
    	
    	LOTRFAEnumHelper.addMapLabel("BLUE_MOUNTAINS", LOTRBiome.blueMountains, 1078, 671, 5.0f, 80, -3.5f, 0.5f);
    	
    	LOTRFAEnumHelper.addMapLabel("BAY_BALAR", "bayBalar", 705, 855, 2.0f, -40, -3.5f, 1.5f);
    	
    	LOTRFAEnumHelper.addMapLabel("DORIATH", "doriath", 839, 591, 2.5f, -15, -1.5f, 0.5f);
    	
    	LOTRFAEnumHelper.addMapLabel("OSSIRIAND", LOTRFABiomes.ossiriand, 1025, 735, 3.0f, 80, -1.5f, 0.5f);
    	
    	LOTRFAEnumHelper.addMapLabel("REGION", LOTRFABiomes.region, 835, 605, 2.0f, -5, 0.5f, 2.5f);
    	LOTRFAEnumHelper.addMapLabel("NELDORETH", LOTRFABiomes.neldoreth, 819, 576, 1.0f, -2, 0.5f, 2.5f);
    	LOTRFAEnumHelper.addMapLabel("ARTHORIEN", LOTRFABiomes.arthorien, 884, 596, 0.5f, -80, 0.5f, 2.5f);
    	LOTRFAEnumHelper.addMapLabel("NIVRIM", LOTRFABiomes.doriath, 779, 619, 1.0f, -80, 0.5f, 2.5f);
    	LOTRFAEnumHelper.addMapLabel("BRETHIL", LOTRFABiomes.brethil, 759, 577, 1.0f, 30, 0.5f, 2.5f);

        /*
         * TODO needed?
         * Object[] lvals5 = {"doriath", 715, 744, 5.5f, -40, -5.2f, 0.0f};
         * EnumHelper.addEnum(LOTRMapLabels.class, "DORIATH", label, lvals5);
         * 
         * Object[] lvals10 = {"ossiriand", 995, 934, 4.5f, -55, -5.5f, 0.5f};
         * EnumHelper.addEnum(LOTRMapLabels.class, "OSSIRIAND", label, lvals10);
         * 
         * Object[] lvals16 = {"balar", 530, 1106, 4.0f, -35, -3.0f, 1.0f};
         * EnumHelper.addEnum(LOTRMapLabels.class, "BALAR", label, lvals16);
         * 
         * Object[] lvals20 = {"neldoreth", 694, 722, 1.5f, 0, -1.5f, 3.0f};
         * EnumHelper.addEnum(LOTRMapLabels.class, "NELDORETH", label, lvals20);
         * 
         * Object[] lvals21 = {"region", 747, 745, 3.0f, -30, -1.5f, 3.0f};
         * EnumHelper.addEnum(LOTRMapLabels.class, "REGION", label, lvals21);
         * 
         * Object[] lvals22 = {"brethil", 599, 715, 2.0f, 0, -1.5f, 3.0f};
         * EnumHelper.addEnum(LOTRMapLabels.class, "BRETHIL", label, lvals22);
         * 
         * Object[] lvals29 = {"nanTathren", 624, 945, 2.0f, 0, -1.5f, 3.0f};
         * EnumHelper.addEnum(LOTRMapLabels.class, "NAN_TATHREN", label, lvals29);
         * 
         * Object[] lvals32 = {"echoriath", 622, 629, 1.5f, 0, -1.5f, 3.0f};
         * EnumHelper.addEnum(LOTRMapLabels.class, "ECHORIATH", label, lvals32);
         * 
         */
    }
}
