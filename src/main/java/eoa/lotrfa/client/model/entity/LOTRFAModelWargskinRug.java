package eoa.lotrfa.client.model.entity;

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

public class LOTRFAModelWargskinRug extends ModelBase {
    private LOTRFAModelWarg wargModel = new LOTRFAModelWarg();

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        this.setRotationAngles();
        
        GL11.glTranslatef(0.0f, -0.3f, 0.0f);
        
        GL11.glPushMatrix();
        GL11.glScalef(1.5f, 0.4f, 1.0f);
        this.wargModel.body0.render(f5);
        GL11.glPopMatrix();
        
        GL11.glPushMatrix();
        GL11.glScalef(1.5f, 0.4f, 1.0f);
        this.wargModel.tail.isHidden = true;
        this.wargModel.body1.render(f5);
        this.wargModel.tail.isHidden = false;
        GL11.glPopMatrix();
        
        GL11.glPushMatrix();
        GL11.glScalef(1.5f, 1f, 0.8f);
        GL11.glTranslatef(0.0f, 0.1f, 0.3f);
        this.wargModel.tail.render(f5);
        GL11.glPopMatrix();
        
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0f, -0.3f, 0.05f);
        this.wargModel.head0.render(f5);
        GL11.glPopMatrix();
        
        GL11.glPushMatrix();
        GL11.glTranslatef(-0.3f, 0.0f, 0.0f);
        this.wargModel.leg10.render(f5);
        this.wargModel.leg30.render(f5);
        GL11.glPopMatrix();
        
        GL11.glPushMatrix();
        GL11.glTranslatef(0.3f, 0.0f, 0.0f);
        this.wargModel.leg20.render(f5);
        this.wargModel.leg40.render(f5);
        GL11.glPopMatrix();
    }

    private void setRotationAngles() {
        this.wargModel.tail.rotateAngleX = (float) Math.toRadians(90.0);
        this.wargModel.tail_1.rotateAngleX = (float) Math.toRadians(0);
        this.wargModel.leg10.rotateAngleX = (float) Math.toRadians(30.0);
        this.wargModel.leg10.rotateAngleZ = (float) Math.toRadians(90.0);
        this.wargModel.leg20.rotateAngleX = (float) Math.toRadians(30.0);
        this.wargModel.leg20.rotateAngleZ = (float) Math.toRadians(-90.0);
        this.wargModel.leg30.rotateAngleX = (float) Math.toRadians(-20.0);
        this.wargModel.leg30.rotateAngleZ = (float) Math.toRadians(90.0);
        this.wargModel.leg40.rotateAngleX = (float) Math.toRadians(-20.0);
        this.wargModel.leg40.rotateAngleZ = (float) Math.toRadians(-90.0);
    }
}
