package eoa.lotrfa.client.model.entity;

import lotr.client.render.entity.LOTRGlowingEyes;
import lotr.common.entity.npc.LOTREntityWarg;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;

public class LOTRFAModelWarg extends ModelBase implements LOTRGlowingEyes.Model {
    public ModelRenderer body0;
    public ModelRenderer body1;
    public ModelRenderer head0;
    public ModelRenderer head1;
    public ModelRenderer head4;
    public ModelRenderer head5;
    public ModelRenderer ear;
    public ModelRenderer ear_1;
    public ModelRenderer ear_2;
    public ModelRenderer ear_3;
    public ModelRenderer tail;
    public ModelRenderer tail_1;
    public ModelRenderer tail_2;
    public ModelRenderer leg10;
    public ModelRenderer leg11;
    public ModelRenderer leg20;
    public ModelRenderer leg21;
    public ModelRenderer leg30;
    public ModelRenderer leg31;
    public ModelRenderer leg40;
    public ModelRenderer leg41;


    public LOTRFAModelWarg() {
        this(0.0f);
    }
    
    public LOTRFAModelWarg(float scale) {
        this.textureWidth = 130;
        this.textureHeight = 75;
        this.tail_1 = new ModelRenderer(this, 100, 59);
        this.tail_1.mirror = true;
        this.tail_1.setRotationPoint(0.0F, 6.5F, 2.0F);
        this.tail_1.addBox(-2.5F, 0.0F, -2.5F, 5, 9, 5, scale);
        this.setRotateAngle(tail_1, 0.2617993877991494F, 0.0F, 0.0F);
        this.ear_2 = new ModelRenderer(this, 95, 26);
        this.ear_2.setRotationPoint(-1.5F, -5.0F, 0.0F);
        this.ear_2.addBox(-1.0F, -1.0F, -0.5F, 2, 1, 1, scale);
        this.leg30 = new ModelRenderer(this, 62, 33);
        this.leg30.mirror = true;
        this.leg30.setRotationPoint(-1.9F, 5.0F, -8.0F);
        this.leg30.addBox(-6.0F, -2.0F, -2.5F, 6, 11, 8, scale + 0.05F);
        this.leg10 = new ModelRenderer(this, 62, 0);
        this.leg10.mirror = true;
        this.leg10.setRotationPoint(-2.3F, 6.0F, 12.0F);
        this.leg10.addBox(-6.0F, -1.0F, -2.5F, 6, 9, 8, scale + 0.05F);
        this.head1 = new ModelRenderer(this, 104, 18);
        this.head1.setRotationPoint(0.0F, 0.0F, -0.4F);
        this.head1.addBox(-3.0F, -1.0F, -12.0F, 6, 6, 6, scale);
        this.ear = new ModelRenderer(this, 95, 19);
        this.ear.mirror = true;
        this.ear.setRotationPoint(-1.0F, -3.0F, -2.0F);
        this.ear.addBox(-3.0F, -5.0F, -0.5F, 3, 5, 1, scale);
        this.setRotateAngle(ear, 0.0F, 0.0F, -0.31869712141416456F);
        this.leg21 = new ModelRenderer(this, 66, 17);
        this.leg21.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.leg21.addBox(0.5F, 8.0F, -1.0F, 5, 10, 5, scale);
        this.body1 = new ModelRenderer(this, 0, 28);
        this.body1.setRotationPoint(0.0F, 1.3F, 1.0F);
        this.body1.addBox(-6.5F, 0.0F, 0.0F, 13, 11, 18, scale + 0.05F);
        this.head4 = new ModelRenderer(this, 100, 30);
        this.head4.setRotationPoint(0.0F, -0.9F, -4.3F);
        this.head4.addBox(-3.0F, 0.0F, 0.0F, 6, 6, 10, scale);
        this.setRotateAngle(head4, -0.40980330836826856F, 0.0F, 0.0F);
        this.ear_3 = new ModelRenderer(this, 95, 26);
        this.ear_3.setRotationPoint(1.5F, -5.0F, 0.0F);
        this.ear_3.addBox(-1.0F, -1.0F, -0.5F, 2, 1, 1, scale);
        this.tail = new ModelRenderer(this, 94, 47);
        this.tail.mirror = true;
        this.tail.setRotationPoint(0.0F, 3.3F, 14.3F);
        this.tail.addBox(-2.0F, 0.0F, 0.0F, 4, 8, 4, scale);
        this.setRotateAngle(tail, 0.7285004297824331F, 0.0F, 0.0F);
        this.body0 = new ModelRenderer(this, 0, 0);
        this.body0.mirror = true;
        this.body0.setRotationPoint(1.0F, 2.0F, 2.1F);
        this.body0.addBox(-8.0F, -2.0F, -14.0F, 14, 13, 14, scale + 0.05F);
        this.tail_2 = new ModelRenderer(this, 112, 47);
        this.tail_2.mirror = true;
        this.tail_2.setRotationPoint(0.0F, 9.0F, 0.0F);
        this.tail_2.addBox(-2.0F, -6.0F, -2.0F, 4, 9, 4, scale);
        this.leg41 = new ModelRenderer(this, 66, 52);
        this.leg41.setRotationPoint(-0.5F, 0.0F, 0.0F);
        this.leg41.addBox(0.5F, 8.0F, -1.0F, 5, 11, 5, scale);
        this.leg40 = new ModelRenderer(this, 62, 33);
        this.leg40.setRotationPoint(1.9F, 5.0F, -8.0F);
        this.leg40.addBox(0.0F, -2.0F, -2.5F, 6, 11, 8, scale + 0.05F);
        this.head0 = new ModelRenderer(this, 92, 0);
        this.head0.setRotationPoint(0.0F, 3.2F, -12.0F);
        this.head0.addBox(-5.0F, -5.0F, -8.0F, 10, 10, 8, scale + 0.05F);
        this.ear_1 = new ModelRenderer(this, 95, 19);
        this.ear_1.setRotationPoint(1.0F, -3.0F, -2.0F);
        this.ear_1.addBox(0.0F, -5.0F, -0.5F, 3, 5, 1, scale);
        this.setRotateAngle(ear_1, 0.0F, 0.0F, 0.31869712141416456F);
        this.leg20 = new ModelRenderer(this, 62, 0);
        this.leg20.setRotationPoint(2.3F, 6.0F, 12.0F);
        this.leg20.addBox(0.0F, -1.0F, -2.5F, 6, 9, 8, scale + 0.05F);
        this.leg11 = new ModelRenderer(this, 66, 17);
        this.leg11.mirror = true;
        this.leg11.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.leg11.addBox(-5.5F, 8.0F, -1.0F, 5, 10, 5, scale);
        this.leg31 = new ModelRenderer(this, 66, 52);
        this.leg31.mirror = true;
        this.leg31.setRotationPoint(0.5F, 0.0F, 0.0F);
        this.leg31.addBox(-5.5F, 8.0F, -1.0F, 5, 11, 5, scale);
        this.head5 = new ModelRenderer(this, 22, 59);
        this.head5.setRotationPoint(-1.0F, -2.6F, -16.7F);
        this.head5.addBox(-3.0F, 0.0F, 0.0F, 6, 5, 10, scale);
        this.tail.addChild(this.tail_1);
        this.ear.addChild(this.ear_2);
        this.head0.addChild(this.head1);
        this.head0.addChild(this.ear);
        this.leg20.addChild(this.leg21);
        this.head0.addChild(this.head4);
        this.ear_1.addChild(this.ear_3);
        this.body1.addChild(this.tail);
        this.tail_1.addChild(this.tail_2);
        this.leg40.addChild(this.leg41);
        this.head0.addChild(this.ear_1);
        this.leg10.addChild(this.leg11);
        this.leg30.addChild(this.leg31);
        this.body0.addChild(this.head5);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) { 
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.body0.render(f5);
        this.body1.render(f5);
        this.head0.render(f5);
        this.leg10.render(f5);
        this.leg20.render(f5);
        this.leg30.render(f5);
        this.leg40.render(f5);
    }
    
    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        this.head0.rotateAngleX = f4 / 57.295776f;
        this.head0.rotateAngleY = f3 / 57.295776f;
        this.leg10.rotateAngleX = MathHelper.cos(f * 0.6662f) * 0.9f * f1;
        this.leg20.rotateAngleX = MathHelper.cos(f * 0.6662f + 3.1415927f) * 0.9f * f1;
        this.leg30.rotateAngleX = MathHelper.cos(f * 0.6662f + 3.1415927f) * 0.9f * f1;
        this.leg40.rotateAngleX = MathHelper.cos(f * 0.6662f) * 0.9f * f1;
        this.tail.rotateAngleX = (float) ((1- ((LOTREntityWarg) entity).getTailRotation() / -1.2f) * Math.toRadians(40.0));
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderGlowingEyes(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.head0.render(f5);
    }
}
