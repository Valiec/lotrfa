package eoa.lotrfa.common;

import java.util.*;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.*;

public class LOTRFATitle extends LOTRTitle {
	public static LOTRTitle supporter;

    public LOTRFATitle(String s) {
    	super(s);
    }

    public LOTRFATitle(String s, LOTRAchievement ach) {
        super(s, ach);
    }
    
    @Override
    public String getUntranslatedName() {
        if (LOTRFAReflectionHelper.getTitleUseAchievementName(this) && LOTRFAReflectionHelper.getTitleTitleAchievement(this) != null) {
            return LOTRFAReflectionHelper.getTitleTitleAchievement(this).getUntranslatedTitle();
        }
        return "lotrfa.title." + getTitleName();
    }

    public static void init() {
    	removeAlignementTitlesExcluding(LOTRFaction.BLUE_MOUNTAINS);
    	LOTRFAReflectionHelper.removeTitles(Arrays.asList(LOTRTitle.MORDOR_slaver));
    	addTitles();
    }
    
    public static void addTitles() {    	
    	supporter = new LOTRFATitle("supporter").setShieldExclusive(LOTRFAShields.supporter);
    	
    	
    	for(LOTRFaction faction : LOTRFAReflectionHelper.getLOTRFAFactions()) {
    		LOTRAchievement achievement = faction.getAlignmentAchievements().get(1000);
            if (achievement == null) continue;
            
            achievement.createTitle();
            LOTRTitle title = achievement.getAchievementTitle();
            title.setAlignment(faction, 1000);
    	}
    }
    
    private static void removeAlignementTitlesExcluding(LOTRFaction... factions) {
    	List<LOTRTitle> titles = new ArrayList<LOTRTitle>();
    	Titles : for(LOTRTitle title : LOTRTitle.allTitles) {
    		if(LOTRFAReflectionHelper.getTitleTitleType(title) == TitleType.ALIGNMENT) {
    			for(LOTRFaction faction : factions) {
    				if(LOTRFAReflectionHelper.getTitleAlignmentFactions(title).contains(faction)) continue Titles;
    			}
    			titles.add(title);
    		}
    	}

		LOTRFAReflectionHelper.removeTitles(titles);
    }
}
