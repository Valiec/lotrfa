package eoa.lotrfa.common.item;

import eoa.lotrfa.common.LOTRFACreativeTabs;
import net.minecraft.item.Item;

public class LOTRFAItemIngot extends Item {

    public LOTRFAItemIngot() {
        setCreativeTab(LOTRFACreativeTabs.tabMaterials);
    }

}
