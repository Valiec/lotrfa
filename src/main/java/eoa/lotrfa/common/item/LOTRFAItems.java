package eoa.lotrfa.common.item;

import com.google.common.base.CaseFormat;
import cpw.mods.fml.common.registry.GameRegistry;
import eoa.lotrfa.common.LOTRFA;
import eoa.lotrfa.common.item.equipment.*;
import eoa.lotrfa.common.item.story.LOTRFAItemDragonHelm;
import eoa.lotrfa.common.item.story.LOTRFAItemSilmaril;
import lotr.common.LOTRCreativeTabs;
import lotr.common.LOTRMod;
import lotr.common.item.*;
import lotr.common.item.LOTRItemMountArmor.Mount;
import net.minecraft.item.Item;

public class LOTRFAItems {

    // Other
	public static Item dragonHelm;
    public static Item nauglamir;
    public static Item silmaril;
    public static Item banner;
    public static Item structureSpawner;

    // Resources
    public static Item pettyDwarfSteel;

    // Tools
    public static Item falathrimPickaxe;
    public static Item falathrimAxe;
    public static Item falathrimShovel;
    public static Item falathrimHoe;

    public static Item feanorianPickaxe;
    public static Item feanorianAxe;
    public static Item feanorianShovel;
    public static Item feanorianHoe;

    public static Item gondolinPickaxe;
    public static Item gondolinAxe;
    public static Item gondolinShovel;
    public static Item gondolinHoe;

    public static Item pettyDwarfMattock;
    public static Item pettyDwarfPickaxe;
    public static Item pettyDwarfAxe;
    public static Item pettyDwarfShovel;
    public static Item pettyDwarfHoe;

    // Weapons
    public static Item borThrowingKnife;
    public static Item borGreatsword;
    public static Item borSword;
    public static Item borBattleaxe;
    public static Item borSpear;
    public static Item borDagger;
    public static Item borDaggerPoisoned;
    public static Item borBow;
    public static Item borPike;

    public static Item brethilSword;
    public static Item brethilBattleaxe;
    public static Item brethilSpear;
    public static Item brethilDagger;
    public static Item brethilDaggerPoisoned;
    public static Item brethilBow;
    public static Item brethilPike;

    public static Item doriathBattleaxe;

    public static Item dorLominSword;
    public static Item dorLominBattleaxe;
    public static Item dorLominSpear;
    public static Item dorLominDagger;
    public static Item dorLominDaggerPoisoned;
    public static Item dorLominBow;
    public static Item dorLominPike;

    public static Item falathrimSword;
    public static Item falathrimDagger;
    public static Item falathrimDaggerPoisoned;
    public static Item falathrimLongspear;
    public static Item falathrimBattlestaff;
    public static Item falathrimBow;
    public static Item falathrimSpear;

    public static Item feanorianSword;
    public static Item feanorianBattlestaff;
    public static Item feanorianSpear;
    public static Item feanorianDagger;
    public static Item feanorianDaggerPoisoned;
    public static Item feanorianBow;
    public static Item feanorianLongspear;
    public static Item feanorianLance;

    public static Item gondolinSword;
    public static Item gondolinSpear;
    public static Item gondolinBow;
    public static Item gondolinLongspear;
    public static Item gondolinGlaive;
    public static Item gondolinDagger;
    public static Item gondolinDaggerPoisoned;
    public static Item gondolinWarhammer;

    public static Item hithlumLance;

    public static Item houseBeorSword;
    public static Item houseBeorBattleaxe;
    public static Item houseBeorSpear;
    public static Item houseBeorDagger;
    public static Item houseBeorDaggerPoisoned;
    public static Item houseBeorBow;
    public static Item houseBeorPike;
    
    public static Item tolInGaurhothCleaver;

    public static Item pettyDwarfSword;
    public static Item pettyDwarfBattleaxe; // ...and my axe!
    public static Item pettyDwarfPike;
    public static Item pettyDwarfSpear;
    public static Item pettyDwarfDagger;
    public static Item pettyDwarfDaggerPoisoned;
    public static Item pettyDwarfWarhammer;
    public static Item pettyDwarfThrowingAxe;

    public static Item ulfangBattleaxe;
    public static Item ulfangPike;
    public static Item ulfangThrowingAxe;
    public static Item ulfangBow;
    public static Item ulfangPoleaxe;
    public static Item ulfangSpear;
    public static Item ulfangSword;
    public static Item ulfangDagger;
    public static Item ulfangDaggerPoisoned;

    public static Item utumnoRemnantBerserkerCleaver;

    public static Item valinorSword;
    public static Item valinorBattlestaff;
    public static Item valinorSpear;
    public static Item valinorDagger;
    public static Item valinorDaggerPoisoned;
    public static Item valinorBow;
    public static Item valinorLongspear;

    // Armor
    public static Item feanorianHelmet;
    public static Item feanorianChestplate;
    public static Item feanorianLeggings;
    public static Item feanorianBoots;
    public static Item feanorianHorseArmor;
    
    public static Item ulfangHorseArmor;

    public static Item gondolinHorseArmor;

    public static Item nargothrondRangerHelmet;
    public static Item nargothrondRangerChestplate;
    public static Item nargothrondRangerLeggings;
    public static Item nargothrondRangerBoots;

    public static Item pettyDwarfHelmet;
    public static Item pettyDwarfChestplate;
    public static Item pettyDwarfLeggings;
    public static Item pettyDwarfBoots;
    public static Item pettyDwarfBoarArmor;

    public static Item dorDaidelosHelmet;
    public static Item dorDaidelosChestplate;
    public static Item dorDaidelosLeggings;
    public static Item dorDaidelosBoots;
    
    public static Item borHelmet;
    public static Item borChestplate;
    public static Item borLeggings;
    public static Item borBoots;
    
    public static Item borCaptainHelmet;
    public static Item borCaptainChestplate;
    public static Item borCaptainLeggings;
    public static Item borCaptainBoots;
    
    public static Item brethilHelmet;
    public static Item brethilChestplate;
    public static Item brethilLeggings;
    public static Item brethilBoots;
    
    public static Item dorLominHelmet;
    public static Item dorLominChestplate;
    public static Item dorLominLeggings;
    public static Item dorLominBoots;
    
    public static Item brethilRangerHelmet;
    public static Item brethilRangerChestplate;
    public static Item brethilRangerLeggings;
    public static Item brethilRangerBoots;
    
    public static Item houseBeorHelmet;
    public static Item houseBeorChestplate;
    public static Item houseBeorLeggings;
    public static Item houseBeorBoots;

    public static void init() {
        LOTRFAItemBanner.FABannerTypes.init();
        initItems();
        registerItems();
        modifyItems();
    }

    private static void initItems() {
        // Other
    	dragonHelm = new LOTRFAItemDragonHelm();
        nauglamir = new LOTRFAItemArmor(LOTRFAMaterial.NAUGLAMIR, 1).setCreativeTab(LOTRCreativeTabs.tabStory); // TODO story item. Own class?
        silmaril = new LOTRFAItemSilmaril();
        banner = new LOTRFAItemBanner();
        structureSpawner = new LOTRFAItemStructureSpawner();

        // Resources
        pettyDwarfSteel = new LOTRFAItemIngot();

        // Tools
        falathrimPickaxe = new LOTRItemPickaxe(LOTRMaterial.DOL_AMROTH);
        falathrimAxe = new LOTRItemAxe(LOTRMaterial.DOL_AMROTH);
        falathrimShovel = new LOTRItemShovel(LOTRMaterial.DOL_AMROTH);
        falathrimHoe = new LOTRItemHoe(LOTRMaterial.DOL_AMROTH);

        feanorianPickaxe = new LOTRItemPickaxe(LOTRFAMaterial.FEANORIAN);
        feanorianAxe = new LOTRItemAxe(LOTRFAMaterial.FEANORIAN);
        feanorianShovel = new LOTRItemShovel(LOTRFAMaterial.FEANORIAN);
        feanorianHoe = new LOTRItemHoe(LOTRFAMaterial.FEANORIAN);

        gondolinPickaxe = new LOTRItemPickaxe(LOTRMaterial.GONDOLIN);
        gondolinAxe = new LOTRItemAxe(LOTRMaterial.GONDOLIN);
        gondolinShovel = new LOTRItemShovel(LOTRMaterial.GONDOLIN);
        gondolinHoe = new LOTRItemHoe(LOTRMaterial.GONDOLIN);

        pettyDwarfMattock = new LOTRItemMattock(LOTRFAMaterial.PETTY_DWARVEN);
        pettyDwarfPickaxe = new LOTRItemPickaxe(LOTRFAMaterial.PETTY_DWARVEN);
        pettyDwarfAxe = new LOTRItemAxe(LOTRFAMaterial.PETTY_DWARVEN);
        pettyDwarfShovel = new LOTRItemShovel(LOTRFAMaterial.PETTY_DWARVEN);
        pettyDwarfHoe = new LOTRItemHoe(LOTRFAMaterial.PETTY_DWARVEN);

        // Weapons
        borThrowingKnife = new LOTRFAItemThrowingKnife(LOTRFAMaterial.HOUSE_BOR);
        borGreatsword = new LOTRFAItemGreatsword(LOTRFAMaterial.HOUSE_BOR);
        borSword = new LOTRItemSword(LOTRFAMaterial.HOUSE_BOR);
        borBattleaxe = new LOTRItemBattleaxe(LOTRFAMaterial.HOUSE_BOR);
        borSpear = new LOTRItemSpear(LOTRFAMaterial.HOUSE_BOR);
        borDagger = new LOTRItemDagger(LOTRFAMaterial.HOUSE_BOR);
        borDaggerPoisoned = new LOTRItemDagger(LOTRFAMaterial.HOUSE_BOR, LOTRItemDagger.DaggerEffect.POISON);
        borBow = new LOTRItemBow(LOTRFAMaterial.HOUSE_BOR, 0.5, 0.5f).setDrawTime(16);
        borPike = new LOTRItemPike(LOTRFAMaterial.HOUSE_BOR);

        brethilSword = new LOTRItemSword(LOTRFAMaterial.BRETHIL);
        brethilBattleaxe = new LOTRItemBattleaxe(LOTRFAMaterial.BRETHIL);
        brethilSpear = new LOTRItemSpear(LOTRFAMaterial.BRETHIL);
        brethilDagger = new LOTRItemDagger(LOTRFAMaterial.BRETHIL);
        brethilDaggerPoisoned = new LOTRItemDagger(LOTRFAMaterial.BRETHIL, LOTRItemDagger.DaggerEffect.POISON);
        brethilBow = new LOTRItemBow(LOTRFAMaterial.BRETHIL, 0.5, 0.5f).setDrawTime(16);
        brethilPike = new LOTRItemPike(LOTRFAMaterial.BRETHIL);

        doriathBattleaxe = new LOTRItemBattleaxe(LOTRMaterial.GALADHRIM);

        dorLominSword = new LOTRItemSword(LOTRFAMaterial.DOR_LOMIN);
        dorLominBattleaxe = new LOTRItemHammer(LOTRFAMaterial.DOR_LOMIN);
        dorLominSpear = new LOTRItemSpear(LOTRFAMaterial.DOR_LOMIN);
        dorLominDagger = new LOTRItemDagger(LOTRFAMaterial.DOR_LOMIN);
        dorLominDaggerPoisoned = new LOTRItemDagger(LOTRFAMaterial.DOR_LOMIN, LOTRItemDagger.DaggerEffect.POISON);
        dorLominBow = new LOTRItemBow(LOTRFAMaterial.DOR_LOMIN, 0.5, 0.5f).setDrawTime(16);
        dorLominPike = new LOTRItemPike(LOTRFAMaterial.DOR_LOMIN);

        falathrimSword = new LOTRItemSword(LOTRFAMaterial.FALATHRIM).setIsElvenBlade();
        falathrimDagger = new LOTRItemDagger(LOTRFAMaterial.FALATHRIM).setIsElvenBlade();
        falathrimDaggerPoisoned = new LOTRItemDagger(LOTRFAMaterial.FALATHRIM, LOTRItemDagger.DaggerEffect.POISON).setIsElvenBlade();
        falathrimLongspear = new LOTRItemPolearmLong(LOTRFAMaterial.FALATHRIM);
        falathrimBattlestaff = new LOTRItemPolearm(LOTRFAMaterial.FALATHRIM);
        falathrimBow = new LOTRItemBow(LOTRFAMaterial.FALATHRIM, 0.5, 0.5f).setDrawTime(30);
        falathrimSpear = new LOTRItemSpear(LOTRFAMaterial.FALATHRIM);

        feanorianSword = new LOTRItemSword(LOTRFAMaterial.FEANORIAN).setIsElvenBlade();
        feanorianBattlestaff = new LOTRItemPolearm(LOTRFAMaterial.FEANORIAN);
        feanorianSpear = new LOTRItemSpear(LOTRFAMaterial.FEANORIAN);
        feanorianDagger = new LOTRItemDagger(LOTRFAMaterial.FEANORIAN).setIsElvenBlade();
        feanorianDaggerPoisoned = new LOTRItemDagger(LOTRFAMaterial.FEANORIAN, LOTRItemDagger.DaggerEffect.POISON).setIsElvenBlade();
        feanorianBow = new LOTRItemBow(LOTRFAMaterial.FEANORIAN, 0.5, 0.5f).setDrawTime(16);
        feanorianLongspear = new LOTRItemPolearmLong(LOTRFAMaterial.FEANORIAN);
        feanorianLance = new LOTRItemLance(LOTRFAMaterial.FEANORIAN);

        gondolinSword = new LOTRItemSword(LOTRMaterial.GONDOLIN).setIsElvenBlade();
        gondolinSpear = new LOTRItemSpear(LOTRMaterial.GONDOLIN);
        gondolinBow = new LOTRItemBow(LOTRMaterial.GONDOLIN, 0.5, 0.5f).setDrawTime(16);
        gondolinLongspear = new LOTRItemPolearmLong(LOTRMaterial.GONDOLIN);
        gondolinGlaive = new LOTRItemPolearm(LOTRMaterial.GONDOLIN);
        gondolinDagger = new LOTRItemDagger(LOTRMaterial.GONDOLIN).setIsElvenBlade();
        gondolinDaggerPoisoned = new LOTRItemDagger(LOTRMaterial.GONDOLIN, LOTRItemDagger.DaggerEffect.POISON).setIsElvenBlade();
        gondolinWarhammer = new LOTRItemHammer(LOTRMaterial.GONDOLIN);

        hithlumLance = new LOTRItemLance(LOTRMaterial.HIGH_ELVEN);

        houseBeorSword = new LOTRItemSword(LOTRMaterial.GONDOR);
        houseBeorBattleaxe = new LOTRItemBattleaxe(LOTRMaterial.GONDOR);
        houseBeorSpear = new LOTRItemSpear(LOTRMaterial.GONDOR);
        houseBeorDagger = new LOTRItemDagger(LOTRMaterial.GONDOR);
        houseBeorDaggerPoisoned = new LOTRItemDagger(LOTRMaterial.GONDOR, LOTRItemDagger.DaggerEffect.POISON);
        houseBeorBow = new LOTRItemBow(LOTRMaterial.GONDOR, 0.5, 0.5f).setDrawTime(16);
        houseBeorPike = new LOTRItemPike(LOTRMaterial.GONDOR);
        
        tolInGaurhothCleaver = new LOTRFAItemCleaver(LOTRMaterial.DOL_GULDUR);

        pettyDwarfSword = new LOTRItemSword(LOTRFAMaterial.PETTY_DWARVEN);
        pettyDwarfBattleaxe = new LOTRItemBattleaxe(LOTRFAMaterial.PETTY_DWARVEN); // ...and my axe!
        pettyDwarfPike = new LOTRItemPike(LOTRFAMaterial.PETTY_DWARVEN);
        pettyDwarfSpear = new LOTRItemSpear(LOTRFAMaterial.PETTY_DWARVEN);
        pettyDwarfDagger = new LOTRItemDagger(LOTRFAMaterial.PETTY_DWARVEN);
        pettyDwarfDaggerPoisoned = new LOTRItemDagger(LOTRFAMaterial.PETTY_DWARVEN, LOTRItemDagger.DaggerEffect.POISON);
        pettyDwarfWarhammer = new LOTRItemHammer(LOTRFAMaterial.PETTY_DWARVEN);
        pettyDwarfThrowingAxe = new LOTRItemThrowingAxe(LOTRFAMaterial.PETTY_DWARVEN);

        ulfangBattleaxe = new LOTRItemBattleaxe(LOTRFAMaterial.ULFANG);
        ulfangPike = new LOTRItemPolearm(LOTRFAMaterial.ULFANG);
        ulfangThrowingAxe = new LOTRItemThrowingAxe(LOTRFAMaterial.ULFANG);
        ulfangBow = new LOTRItemBow(LOTRFAMaterial.ULFANG, 0.0, 0.2f).setDrawTime(16);
        ulfangSpear = new LOTRItemSpear(LOTRFAMaterial.ULFANG);
        ulfangPoleaxe = new LOTRItemPike(LOTRFAMaterial.ULFANG);
        ulfangSword = new LOTRItemSword(LOTRFAMaterial.ULFANG);
        ulfangDagger = new LOTRItemDagger(LOTRFAMaterial.ULFANG);
        ulfangDaggerPoisoned = new LOTRItemDagger(LOTRFAMaterial.ULFANG, LOTRItemDagger.DaggerEffect.POISON);

        utumnoRemnantBerserkerCleaver = new LOTRFAItemCleaver(LOTRMaterial.URUK);

        valinorSword = new LOTRItemSword(LOTRFAMaterial.VALINOR).setIsElvenBlade();
        valinorBattlestaff = new LOTRItemPolearm(LOTRFAMaterial.VALINOR);
        valinorSpear = new LOTRItemSpear(LOTRFAMaterial.VALINOR);
        valinorDagger = new LOTRItemDagger(LOTRFAMaterial.VALINOR).setIsElvenBlade();
        valinorDaggerPoisoned = new LOTRItemDagger(LOTRFAMaterial.VALINOR, LOTRItemDagger.DaggerEffect.POISON).setIsElvenBlade();
        valinorBow = new LOTRItemBow(LOTRFAMaterial.VALINOR, 0.5, 0.5f).setDrawTime(16);
        valinorLongspear = new LOTRItemPolearmLong(LOTRFAMaterial.VALINOR);

        // Armor
        feanorianHelmet = new LOTRFAItemFeanorianHelmet();
        feanorianChestplate = new LOTRFAItemArmor(LOTRFAMaterial.FEANORIAN, 1);
        feanorianLeggings = new LOTRFAItemArmor(LOTRFAMaterial.FEANORIAN, 2);
        feanorianBoots = new LOTRFAItemArmor(LOTRFAMaterial.FEANORIAN, 3);
        feanorianHorseArmor = new LOTRFAItemMountArmor(LOTRFAMaterial.FEANORIAN, Mount.HORSE);
        
        ulfangHorseArmor = new LOTRFAItemMountArmor(LOTRFAMaterial.ULFANG, Mount.HORSE);

        gondolinHorseArmor = new LOTRFAItemMountArmor(LOTRMaterial.GONDOLIN, Mount.HORSE);

        nargothrondRangerHelmet = new LOTRFAItemArmor(LOTRFAMaterial.NARGOTHROND_RANGER, 0);
        nargothrondRangerChestplate = new LOTRFAItemArmor(LOTRFAMaterial.NARGOTHROND_RANGER, 1);
        nargothrondRangerLeggings = new LOTRFAItemArmor(LOTRFAMaterial.NARGOTHROND_RANGER, 2);
        nargothrondRangerBoots = new LOTRFAItemArmor(LOTRFAMaterial.NARGOTHROND_RANGER, 3);

        pettyDwarfHelmet = new LOTRFAItemArmor(LOTRFAMaterial.PETTY_DWARVEN, 0);
        pettyDwarfChestplate = new LOTRFAItemArmor(LOTRFAMaterial.PETTY_DWARVEN, 1);
        pettyDwarfLeggings = new LOTRFAItemArmor(LOTRFAMaterial.PETTY_DWARVEN, 2);
        pettyDwarfBoots = new LOTRFAItemArmor(LOTRFAMaterial.PETTY_DWARVEN, 3);
        pettyDwarfBoarArmor = new LOTRFAItemMountArmor(LOTRFAMaterial.PETTY_DWARVEN, Mount.BOAR);

        dorDaidelosHelmet = new LOTRFAItemArmor(LOTRFAMaterial.DOR_DAIDELOS, 0);
        dorDaidelosChestplate = new LOTRFAItemArmor(LOTRFAMaterial.DOR_DAIDELOS, 1);
        dorDaidelosLeggings = new LOTRFAItemArmor(LOTRFAMaterial.DOR_DAIDELOS, 2);
        dorDaidelosBoots = new LOTRFAItemArmor(LOTRFAMaterial.DOR_DAIDELOS, 3);
        
        borHelmet = new LOTRFAItemArmor(LOTRFAMaterial.HOUSE_BOR, 0);
        borChestplate = new LOTRFAItemArmor(LOTRFAMaterial.HOUSE_BOR, 1);
        borLeggings = new LOTRFAItemArmor(LOTRFAMaterial.HOUSE_BOR, 2);
        borBoots = new LOTRFAItemArmor(LOTRFAMaterial.HOUSE_BOR, 3);
        
        borCaptainHelmet = new LOTRFAItemHouseBorCaptainHelmet();
        borCaptainChestplate = new LOTRFAItemArmor(LOTRFAMaterial.HOUSE_BOR_CAPTAIN, 1);
        borCaptainLeggings = new LOTRFAItemArmor(LOTRFAMaterial.HOUSE_BOR_CAPTAIN, 2);
        borCaptainBoots = new LOTRFAItemArmor(LOTRFAMaterial.HOUSE_BOR_CAPTAIN, 3);
        
        brethilHelmet = new LOTRFAItemArmor(LOTRFAMaterial.BRETHIL, 0);
        brethilChestplate = new LOTRFAItemArmor(LOTRFAMaterial.BRETHIL, 1);
        brethilLeggings = new LOTRFAItemArmor(LOTRFAMaterial.BRETHIL, 2);
        brethilBoots = new LOTRFAItemArmor(LOTRFAMaterial.BRETHIL, 3);
        
        brethilRangerHelmet = new LOTRFAItemArmor(LOTRFAMaterial.BRETHIL_RANGER, 0);
        brethilRangerChestplate = new LOTRFAItemArmor(LOTRFAMaterial.BRETHIL_RANGER, 1);
        brethilRangerLeggings = new LOTRFAItemArmor(LOTRFAMaterial.BRETHIL_RANGER, 2);
        brethilRangerBoots = new LOTRFAItemArmor(LOTRFAMaterial.BRETHIL_RANGER, 3);
        
        dorLominHelmet = new LOTRFAItemArmor(LOTRFAMaterial.DOR_LOMIN, 0);
        dorLominChestplate = new LOTRFAItemArmor(LOTRFAMaterial.DOR_LOMIN, 1);
        dorLominLeggings = new LOTRFAItemArmor(LOTRFAMaterial.DOR_LOMIN, 2);
        dorLominBoots = new LOTRFAItemArmor(LOTRFAMaterial.DOR_LOMIN, 3);
        
        houseBeorHelmet = new LOTRFAItemArmor(LOTRFAMaterial.BEOR, 0);
        houseBeorChestplate = new LOTRFAItemArmor(LOTRFAMaterial.BEOR, 1);
        houseBeorLeggings = new LOTRFAItemArmor(LOTRFAMaterial.BEOR, 2);
        houseBeorBoots = new LOTRFAItemArmor(LOTRFAMaterial.BEOR, 3);
        
    }

    private static void registerItems() {
        // Other
    	nameAndRegisterItem(dragonHelm, "dragonHelm");
        nameAndRegisterItem(nauglamir, "nauglamir");
        nameAndRegisterItem(silmaril, "silmaril");
        nameAndRegisterItem(banner, "banner");
        nameAndRegisterItem(structureSpawner, "structureSpawner");

        // Resources
        nameAndRegisterItem(pettyDwarfSteel, "pettyDwarfSteel");

        // Tools
        nameAndRegisterItem(falathrimPickaxe, "falathrimPickaxe");
        nameAndRegisterItem(falathrimAxe, "falathrimAxe");
        nameAndRegisterItem(falathrimShovel, "falathrimShovel");
        nameAndRegisterItem(falathrimHoe, "falathrimHoe");

        nameAndRegisterItem(feanorianPickaxe, "feanorianPickaxe");
        nameAndRegisterItem(feanorianAxe, "feanorianAxe");
        nameAndRegisterItem(feanorianShovel, "feanorianShovel");
        nameAndRegisterItem(feanorianHoe, "feanorianHoe");

        nameAndRegisterItem(gondolinPickaxe, "gondolinPickaxe");
        nameAndRegisterItem(gondolinAxe, "gondolinAxe");
        nameAndRegisterItem(gondolinShovel, "gondolinShovel");
        nameAndRegisterItem(gondolinHoe, "gondolinHoe");

        nameAndRegisterItem(pettyDwarfMattock, "pettyDwarfMattock");
        nameAndRegisterItem(pettyDwarfPickaxe, "pettyDwarfPickaxe");
        nameAndRegisterItem(pettyDwarfAxe, "pettyDwarfAxe");
        nameAndRegisterItem(pettyDwarfShovel, "pettyDwarfShovel");
        nameAndRegisterItem(pettyDwarfHoe, "pettyDwarfHoe");

        // Weapons

        nameAndRegisterItem(doriathBattleaxe, "doriathBattleaxe");

        nameAndRegisterItem(falathrimSword, "falathrimSword");
        nameAndRegisterItem(falathrimDagger, "falathrimDagger");
        nameAndRegisterItem(falathrimDaggerPoisoned, "falathrimDaggerPoisoned");
        nameAndRegisterItem(falathrimLongspear, "falathrimLongspear");
        nameAndRegisterItem(falathrimBattlestaff, "falathrimBattlestaff");
        nameAndRegisterItem(falathrimBow, "falathrimBow");
        nameAndRegisterItem(falathrimSpear, "falathrimSpear");

        nameAndRegisterItem(feanorianSword, "feanorianSword");
        nameAndRegisterItem(feanorianBattlestaff, "feanorianBattlestaff");
        nameAndRegisterItem(feanorianSpear, "feanorianSpear");
        nameAndRegisterItem(feanorianDagger, "feanorianDagger");
        nameAndRegisterItem(feanorianDaggerPoisoned, "feanorianDaggerPoisoned");
        nameAndRegisterItem(feanorianBow, "feanorianBow");
        nameAndRegisterItem(feanorianLongspear, "feanorianLongspear");
        nameAndRegisterItem(feanorianLance, "feanorianLance");

        nameAndRegisterItem(gondolinSword, "gondolinSword");
        nameAndRegisterItem(gondolinSpear, "gondolinSpear");
        nameAndRegisterItem(gondolinBow, "gondolinBow");
        nameAndRegisterItem(gondolinLongspear, "gondolinLongspear");
        nameAndRegisterItem(gondolinGlaive, "gondolinGlaive");
        nameAndRegisterItem(gondolinDagger, "gondolinDagger");
        nameAndRegisterItem(gondolinDaggerPoisoned, "gondolinDaggerPoisoned");
        nameAndRegisterItem(gondolinWarhammer, "gondolinWarhammer");

        nameAndRegisterItem(hithlumLance, "hithlumLance");
        
        nameAndRegisterItem(tolInGaurhothCleaver, "tolInGaurhothCleaver");

        nameAndRegisterItem(pettyDwarfSword, "pettyDwarfSword");
        nameAndRegisterItem(pettyDwarfBattleaxe, "pettyDwarfBattleaxe"); // ...and my axe!
        nameAndRegisterItem(pettyDwarfPike, "pettyDwarfPike");
        nameAndRegisterItem(pettyDwarfSpear, "pettyDwarfSpear");
        nameAndRegisterItem(pettyDwarfDagger, "pettyDwarfDagger");
        nameAndRegisterItem(pettyDwarfDaggerPoisoned, "pettyDwarfDaggerPoisoned");
        nameAndRegisterItem(pettyDwarfWarhammer, "pettyDwarfWarhammer");
        nameAndRegisterItem(pettyDwarfThrowingAxe, "pettyDwarfThrowingAxe");

        nameAndRegisterItem(ulfangBattleaxe, "ulfangBattleaxe");
        nameAndRegisterItem(ulfangPike, "ulfangPike");
        nameAndRegisterItem(ulfangThrowingAxe, "ulfangThrowingAxe");
        nameAndRegisterItem(ulfangBow, "ulfangBow");
        nameAndRegisterItem(ulfangSpear, "ulfangSpear");
        nameAndRegisterItem(ulfangPoleaxe, "ulfangPoleaxe");
        nameAndRegisterItem(ulfangSword, "ulfangSword");
        nameAndRegisterItem(ulfangDagger, "ulfangDagger");
        nameAndRegisterItem(ulfangDaggerPoisoned, "ulfangDaggerPoisoned");

        nameAndRegisterItem(utumnoRemnantBerserkerCleaver, "utumnoRemnantBerserkerCleaver");

        nameAndRegisterItem(valinorSword, "valinorSword");
        nameAndRegisterItem(valinorBattlestaff, "valinorBattlestaff");
        nameAndRegisterItem(valinorSpear, "valinorSpear");
        nameAndRegisterItem(valinorDagger, "valinorDagger");
        nameAndRegisterItem(valinorDaggerPoisoned, "valinorDaggerPoisoned");
        nameAndRegisterItem(valinorBow, "valinorBow");
        nameAndRegisterItem(valinorLongspear, "valinorLongspear");

        // Armor
        nameAndRegisterItem(feanorianHelmet, "feanorianHelmet");
        nameAndRegisterItem(feanorianChestplate, "feanorianChestplate");
        nameAndRegisterItem(feanorianLeggings, "feanorianLeggings");
        nameAndRegisterItem(feanorianBoots, "feanorianBoots");
        nameAndRegisterItem(feanorianHorseArmor, "feanorianHorseArmor");
        
        nameAndRegisterItem(ulfangHorseArmor, "ulfangHorseArmor");

        nameAndRegisterItem(gondolinHorseArmor, "gondolinHorseArmor");

        nameAndRegisterItem(nargothrondRangerHelmet, "nargothrondRangerHelmet");
        nameAndRegisterItem(nargothrondRangerChestplate, "nargothrondRangerChestplate");
        nameAndRegisterItem(nargothrondRangerLeggings, "nargothrondRangerLeggings");
        nameAndRegisterItem(nargothrondRangerBoots, "nargothrondRangerBoots");

        nameAndRegisterItem(pettyDwarfHelmet, "pettyDwarfHelmet");
        nameAndRegisterItem(pettyDwarfChestplate, "pettyDwarfChestplate");
        nameAndRegisterItem(pettyDwarfLeggings, "pettyDwarfLeggings");
        nameAndRegisterItem(pettyDwarfBoots, "pettyDwarfBoots");
        nameAndRegisterItem(pettyDwarfBoarArmor, "pettyDwarfBoarArmor");

        nameAndRegisterItem(dorDaidelosHelmet, "dorDaidelosHelmet");
        nameAndRegisterItem(dorDaidelosChestplate, "dorDaidelosChestplate");
        nameAndRegisterItem(dorDaidelosLeggings, "dorDaidelosLeggings");
        nameAndRegisterItem(dorDaidelosBoots, "dorDaidelosBoots");
        
        // Brethil
        nameAndRegisterItem(brethilSword, "brethilSword");
        nameAndRegisterItem(brethilBattleaxe, "brethilBattleaxe");
        nameAndRegisterItem(brethilSpear, "brethilSpear");
        nameAndRegisterItem(brethilDagger, "brethilDagger");
        nameAndRegisterItem(brethilDaggerPoisoned, "brethilDaggerPoisoned");
        nameAndRegisterItem(brethilBow, "brethilBow");
        nameAndRegisterItem(brethilPike, "brethilPike");
        
        nameAndRegisterItem(brethilHelmet, "brethilHelmet");
        nameAndRegisterItem(brethilChestplate, "brethilChestplate");
        nameAndRegisterItem(brethilLeggings, "brethilLeggings");
        nameAndRegisterItem(brethilBoots, "brethilBoots");
        
        nameAndRegisterItem(brethilRangerHelmet, "brethilRangerHelmet");
        nameAndRegisterItem(brethilRangerChestplate, "brethilRangerChestplate");
        nameAndRegisterItem(brethilRangerLeggings, "brethilRangerLeggings");
        nameAndRegisterItem(brethilRangerBoots, "brethilRangerBoots");
        
        // Dor-lomin
        nameAndRegisterItem(dorLominSword, "dorLominSword");
        nameAndRegisterItem(dorLominBattleaxe, "dorLominBattleaxe");
        nameAndRegisterItem(dorLominSpear, "dorLominSpear");
        nameAndRegisterItem(dorLominDagger, "dorLominDagger");
        nameAndRegisterItem(dorLominDaggerPoisoned, "dorLominDaggerPoisoned");
        nameAndRegisterItem(dorLominBow, "dorLominBow");
        nameAndRegisterItem(dorLominPike, "dorLominPike");
        
        nameAndRegisterItem(dorLominHelmet, "dorLominHelmet");
        nameAndRegisterItem(dorLominChestplate, "dorLominChestplate");
        nameAndRegisterItem(dorLominLeggings, "dorLominLeggings");
        nameAndRegisterItem(dorLominBoots, "dorLominBoots");
         
        // House Bor
        nameAndRegisterItem(borThrowingKnife, "borThrowingKnife");
        nameAndRegisterItem(borGreatsword, "borGreatsword");
        nameAndRegisterItem(borSword, "borSword");
        nameAndRegisterItem(borBattleaxe, "borBattleaxe");
        nameAndRegisterItem(borSpear, "borSpear");
        nameAndRegisterItem(borDagger, "borDagger");
        nameAndRegisterItem(borDaggerPoisoned, "borDaggerPoisoned");
        nameAndRegisterItem(borBow, "borBow");
        nameAndRegisterItem(borPike, "borPike");
        
        nameAndRegisterItem(borHelmet, "borHelmet");
        nameAndRegisterItem(borChestplate, "borChestplate");
        nameAndRegisterItem(borLeggings, "borLeggings");
        nameAndRegisterItem(borBoots, "borBoots");
        
        nameAndRegisterItem(borCaptainHelmet, "borCaptainHelmet");
        nameAndRegisterItem(borCaptainChestplate, "borCaptainChestplate");
        nameAndRegisterItem(borCaptainLeggings, "borCaptainLeggings");
        nameAndRegisterItem(borCaptainBoots, "borCaptainBoots");
        
        // Beorrim
        nameAndRegisterItem(houseBeorSword, "houseBeorSword");
        nameAndRegisterItem(houseBeorBattleaxe, "houseBeorBattleaxe");
        nameAndRegisterItem(houseBeorSpear, "houseBeorSpear");
        nameAndRegisterItem(houseBeorDagger, "houseBeorDagger");
        nameAndRegisterItem(houseBeorDaggerPoisoned, "houseBeorDaggerPoisoned");
        nameAndRegisterItem(houseBeorBow, "houseBeorBow");
        nameAndRegisterItem(houseBeorPike, "houseBeorPike");
        
        nameAndRegisterItem(houseBeorHelmet, "houseBeorHelmet");
        nameAndRegisterItem(houseBeorChestplate, "houseBeorChestplate");
        nameAndRegisterItem(houseBeorLeggings, "houseBeorLeggings");
        nameAndRegisterItem(houseBeorBoots, "houseBeorBoots");
    }

    private static void modifyItems() {
        // ((LOTRItemHammer)(LOTRMod.maceMallornCharred)).addWeaponDamage(-7.0F);
        ((LOTRItemSword) (LOTRMod.swordUtumno)).addWeaponDamage(1.5F);
        ((LOTRItemDagger) (LOTRMod.daggerUtumno)).addWeaponDamage(1.5F);
        ((LOTRItemDagger) (LOTRMod.daggerUtumnoPoisoned)).addWeaponDamage(1.5F);
        ((LOTRItemSpear) (LOTRMod.spearUtumno)).addWeaponDamage(1.5F);
        ((LOTRItemBattleaxe) (LOTRMod.battleaxeUtumno)).addWeaponDamage(1.5F);
        ((LOTRItemHammer) (LOTRMod.hammerUtumno)).addWeaponDamage(1.5F);
        ((LOTRItemSword) (LOTRMod.swordGondolin)).addWeaponDamage(-2.0F);
        ((LOTRFAItemCleaver) (LOTRFAItems.tolInGaurhothCleaver)).addWeaponDamage(0.5F);
    }

    private static void nameAndRegisterItem(Item item, String name) {
        String lowerUnderscoreName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, name);
        item.setUnlocalizedName(name);
        item.setTextureName(LOTRFA.MODID + ":" + lowerUnderscoreName);
        GameRegistry.registerItem(item, lowerUnderscoreName);
    }

    private static void nameAndRegisterItem(Item item, String name, String textureName) {
        String lowerUnderscoreName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, name);
        item.setUnlocalizedName(name);
        item.setTextureName(textureName);
        GameRegistry.registerItem(item, lowerUnderscoreName);
    }
}
