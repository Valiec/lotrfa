package eoa.lotrfa.common.item;

import java.util.List;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.lotrfa.common.world.structure.LOTRFAStructures;
import eoa.lotrfa.common.world.structure.LOTRFAStructures.StructureInfo;
import lotr.common.LOTRLevelData;
import lotr.common.item.LOTRItemStructureSpawner;
import lotr.common.world.structure.LOTRWorldGenStructureBase;
import lotr.common.world.structure2.LOTRWorldGenStructureBase2;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class LOTRFAItemStructureSpawner extends LOTRItemStructureSpawner {
    @SideOnly(Side.CLIENT)
    private IIcon baseIcon;
    @SideOnly(Side.CLIENT)
    private IIcon overlayIcon;

    @Override
    public String getItemStackDisplayName(ItemStack itemstack) {
        String s = (StatCollector.translateToLocal(getUnlocalizedName() + ".name")).trim();
        String structureName = LOTRFAStructures.getNameFromID(itemstack.getItemDamage());

        if (structureName != null) {
            s = s + " " + StatCollector.translateToLocal("lotrfa.structure." + structureName + ".name");
        }

        return s;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getColorFromItemStack(ItemStack itemstack, int i) {
        StructureInfo info = LOTRFAStructures.structureSpawners.get(itemstack.getItemDamage());
        return info != null ? (i == 0 ? info.primaryColor : info.secondaryColor) : 16777215;
    }

    @Override
    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int side, float f, float f1, float f2) {
        if (world.isRemote) return true;
        
        if (LOTRLevelData.structuresBanned()) {
            entityplayer.addChatMessage(new ChatComponentTranslation("chat.lotr.spawnStructure.disabled", new Object[0]));
            return false;
        }
        if (LOTRLevelData.isPlayerBannedForStructures(entityplayer)) {
            entityplayer.addChatMessage(new ChatComponentTranslation("chat.lotr.spawnStructure.banned", new Object[0]));
            return false;
        }        
        if (lastStructureSpawnTick > 0) {
            entityplayer.addChatMessage(new ChatComponentTranslation("chat.lotr.spawnStructure.wait", new Object[] {lastStructureSpawnTick / 20D}));
            return false;
        }

        i += Facing.offsetsXForSide[side];
        j += Facing.offsetsYForSide[side];
        k += Facing.offsetsZForSide[side];

        if (spawnStructure(entityplayer, world, itemstack.getItemDamage(), i, j, k) && !entityplayer.capabilities.isCreativeMode) {
            itemstack.stackSize--;
        }

        return true;
    }

    private boolean spawnStructure(EntityPlayer entityplayer, World world, int id, int i, int j, int k) {
        if (!LOTRFAStructures.structureSpawners.containsKey(id)) return false;
        
        WorldGenerator generator = LOTRFAStructures.getStructureFromID(id);
        if (generator != null) {
            boolean generated = false;

            if (generator instanceof LOTRWorldGenStructureBase2) {
                LOTRWorldGenStructureBase2 structureGenerator = (LOTRWorldGenStructureBase2) generator;
                structureGenerator.restrictions = false;
                structureGenerator.usingPlayer = entityplayer;
                generated = structureGenerator.generateWithSetRotation(world, world.rand, i, j, k, structureGenerator.usingPlayerRotation());
            }
            else if (generator instanceof LOTRWorldGenStructureBase) {
                LOTRWorldGenStructureBase structureGenerator = (LOTRWorldGenStructureBase) generator;
                structureGenerator.restrictions = false;
                structureGenerator.usingPlayer = entityplayer;
                generated = structureGenerator.generate(world, world.rand, i, j, k);
            }
            
            if (generated) {
                lastStructureSpawnTick = 20;
                world.playSoundAtEntity(entityplayer, "lotr:item.structureSpawner", 1F, 1F);
            }
            return generated;
        }
        return false;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIconFromDamageForRenderPass(int i, int j) {
        return j == 0 ? baseIcon : overlayIcon;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconregister) {
        baseIcon = iconregister.registerIcon(getIconString() + "_base");
        overlayIcon = iconregister.registerIcon(getIconString() + "_overlay");
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void getSubItems(Item item, CreativeTabs tab, List list) {
        for(StructureInfo info : LOTRFAStructures.structureSpawners.values()) {
            list.add(new ItemStack(item, 1, info.spawnedID));
        }
    }
}
