package eoa.lotrfa.common.item;

import java.lang.reflect.*;
import eoa.lotrfa.common.LOTRFA;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.LOTRMod;
import lotr.common.item.LOTRMaterial;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Items;
import net.minecraft.item.*;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;

public class LOTRFAMaterial {
    // TODO Check names
    public static LOTRMaterial PETTY_DWARVEN = newLOTRMaterial("PETTY_DWARVEN", 700, 3.0f, 0.7f, 7.0f, 3, 10, null);
    public static LOTRMaterial NAUGLAMIR = newLOTRMaterial("NAUGLAMIR", 1, 0f, 0f, 0f, 0, 0, null, null, false, true);
    public static LOTRMaterial VALINOR = newLOTRMaterial("VALINOR", 1500, 5.0f, 0.7f, 0.0f, 2, 15, null);
    public static LOTRMaterial FEANORIAN = newLOTRMaterial("FEANORIAN", 700, 3.0f, 0.6f, 8.0f, 2, 15, null);
    public static LOTRMaterial HITHLUM = newLOTRMaterial("HITHLUM", 700, 3.0f, 0.6f, 8.0f, 2, 15, null);
    public static LOTRMaterial DORIATH = newLOTRMaterial("DORIATH", 700, 3.0f, 0.6f, 8.0f, 2, 15, null);
    public static LOTRMaterial FALATHRIM = newLOTRMaterial("FALATHRIM", 700, 3.0f, 0.6f, 8.0f, 2, 15, null);
    public static LOTRMaterial DRAGON_HELM = newLOTRMaterial("DRAGON_HELM", 700, 3.0f, 0.9f, 8.0f, 2, 15, null, null, false, true);
    public static LOTRMaterial ULFANG = newLOTRMaterial("ULFANG", 250, 2.0f, 0.5f, 6.0f, 2, 8, null);
    public static LOTRMaterial NARGOTHROND_RANGER = newLOTRMaterial("NARGOTHROND_RANGER", 300, 0.0f, 0.4f, 0.0f, 0, 15, null);
    public static LOTRMaterial DOR_DAIDELOS = newLOTRMaterial("DOR_DAIDELOS", 300, 0.0f, 0.4f, 0.0f, 0, 15, null);
    public static LOTRMaterial BEOR = newLOTRMaterial("BEOR", 300, 2.5f, 0.5f, 0.0f, 2, 6, null);
    public static LOTRMaterial HOUSE_BOR = newLOTRMaterial("HOUSE_BOR", 300, 2.5f, 0.5f, 2.0f, 6, 10, null);
    public static LOTRMaterial HOUSE_BOR_CAPTAIN = newLOTRMaterial("HOUSE_BOR_CAPTAIN", 400, 2.5f, 0.6f, 2.0f, 6, 10, null);
    public static LOTRMaterial BRETHIL = newLOTRMaterial("BRETHIL", 300, 2.5f, 0.5f, 2.0f, 6, 10, null);
    public static LOTRMaterial BRETHIL_RANGER = newLOTRMaterial("BRETHIL_RANGER", 300, 0.0f, 0.4f, 0.0f, 0, 15, null);
    public static LOTRMaterial DOR_LOMIN = newLOTRMaterial("DOR_LOMIN", 400, 2.5f, 0.6f, 2.0f, 6, 10, null);

    private static boolean setup = false;
    private static Constructor<LOTRMaterial> constructor;
    private static Method setUses;
    private static Method setDamage;
    private static Method setProtection;
    private static Method setSpeed;
    private static Method setHarvestLevel;
    private static Method setEnchantibility;
    private static Method setCraftingMaterial;
    private static Method setUndamageable;
    private static Method setManFlesh;

    public static void setupCraftingItems() {
        LOTRFAReflectionHelper.setMaterialCraftingItem(PETTY_DWARVEN, LOTRFAItems.pettyDwarfSteel);
        LOTRFAReflectionHelper.setMaterialCraftingItem(VALINOR, LOTRMod.elfSteel);
        LOTRFAReflectionHelper.setMaterialCraftingItem(FEANORIAN, LOTRMod.elfSteel);
        LOTRFAReflectionHelper.setMaterialCraftingItem(FALATHRIM, LOTRMod.elfSteel);
        LOTRFAReflectionHelper.setMaterialCraftingItem(HITHLUM, LOTRMod.elfSteel);
        LOTRFAReflectionHelper.setMaterialCraftingItem(DORIATH, LOTRMod.elfSteel);
        LOTRFAReflectionHelper.setMaterialCraftingItem(ULFANG, Items.iron_ingot);
        LOTRFAReflectionHelper.setMaterialCraftingItem(NARGOTHROND_RANGER, LOTRMod.elfSteel, Items.leather);
        LOTRFAReflectionHelper.setMaterialCraftingItem(DOR_DAIDELOS, LOTRMod.fur, LOTRMod.orcSteel);
        LOTRFAReflectionHelper.setMaterialCraftingItem(BEOR, Items.iron_ingot, Items.leather);
        LOTRFAReflectionHelper.setMaterialCraftingItem(HOUSE_BOR, Items.iron_ingot, Items.leather);
        LOTRFAReflectionHelper.setMaterialCraftingItem(HOUSE_BOR_CAPTAIN, Items.iron_ingot);
        LOTRFAReflectionHelper.setMaterialCraftingItem(BRETHIL, Items.iron_ingot, Items.leather);
        LOTRFAReflectionHelper.setMaterialCraftingItem(BRETHIL_RANGER, Items.leather);
        LOTRFAReflectionHelper.setMaterialCraftingItem(DOR_LOMIN, Items.iron_ingot);
        setExistingCraftingItem(LOTRMaterial.HALF_TROLL, LOTRMod.blackUrukSteel);
        setExistingCraftingItem(LOTRMaterial.GONDOLIN, LOTRMod.elfSteel);
        setExistingCraftingItem(LOTRMaterial.ARNOR, LOTRMod.elfSteel);
    }
    
    public static void changeLOTRMaterials() {
        editLOTRMaterial(LOTRMaterial.GONDOLIN, 700, 3.0f, 0.6f, 8.0f, 4, 15, null, null, false, false);
        editLOTRMaterial(LOTRMaterial.ARNOR, 1500, 5.0f, 0.7f, -1f, 2, 15, null, null, false, false);
        editLOTRMaterial(LOTRMaterial.HALF_TROLL, 300, 2.5f, 0.6f, 1f, 5, 5, null, null, false, false);
        editLOTRMaterial(LOTRMaterial.DUNLENDING, 300, 2.5f, 0.5f, 2.0f, 6, 10, null, null, false, false);
        editLOTRMaterial(LOTRMaterial.GUNDABAD_URUK, 300, 2.5f, 0.6f, 2.0f, 6, 10, null, null, false, false);
    }

    private static LOTRMaterial newLOTRMaterial(String name, int uses, float damage, float protection, float speed, int harvestLevel, int enchantability, Item craftingMaterial) {
        return newLOTRMaterial(name, uses, damage, protection, speed, harvestLevel, enchantability, craftingMaterial, false);
    }

    private static LOTRMaterial newLOTRMaterial(String name, int uses, float damage, float protection, float speed, int harvestLevel, int enchantability, Item craftingMaterial, boolean manFlesh) {
        return newLOTRMaterial(name, uses, damage, protection, speed, harvestLevel, enchantability, craftingMaterial, craftingMaterial, manFlesh, false);
    }

    private static LOTRMaterial newLOTRMaterial(String name, int uses, float damage, float protection, float speed, int harvestLevel, int enchantability, Item craftingMaterialTool, Item craftingMaterialArmor) {
        return newLOTRMaterial(name, uses, damage, protection, speed, harvestLevel, enchantability, craftingMaterialTool, craftingMaterialArmor, false, false);
    }

    private static LOTRMaterial newLOTRMaterial(String name, int uses, float damage, float protection, float speed, int harvestLevel, int enchantability, Item craftingMaterialTool, Item craftingMaterialArmor, boolean manFlesh, boolean undamageable) {
        setup();
        LOTRMaterial material = null;
        try {
            material = constructor.newInstance(name);
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e) {
            LOTRFA.logger.error("Failed to create LOTRMaterial " + name, e);
        }
        return editLOTRMaterial(material, uses, damage, protection, speed, harvestLevel, enchantability, craftingMaterialTool, craftingMaterialArmor, manFlesh, undamageable);
    }

    private static LOTRMaterial editLOTRMaterial(LOTRMaterial material, int uses, float damage, float protection, float speed, int harvestLevel, int enchantability, Item craftingMaterialTool, Item craftingMaterialArmor, boolean manFlesh, boolean undamageable) {
        setup();
        try {
            if (uses != -1) setUses.invoke(material, new Object[] {uses});
            if (damage != -1) setDamage.invoke(material, new Object[] {damage});
            if (protection != -1) setProtection.invoke(material, new Object[] {protection});
            if (speed != -1) setSpeed.invoke(material, new Object[] {speed});
            if (harvestLevel != -1) setHarvestLevel.invoke(material, new Object[] {harvestLevel});
            if (enchantability != -1) setEnchantibility.invoke(material, new Object[] {enchantability});
            if (craftingMaterialTool != null && craftingMaterialArmor != null) setCraftingMaterial.invoke(material, new Object[] {craftingMaterialTool, craftingMaterialArmor});
            if (undamageable) setUndamageable.invoke(material, new Object[] {});
            if (manFlesh) setManFlesh.invoke(material, new Object[] {});
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            LOTRFA.logger.error("Failed to edit LOTRMaterial " + material.toString(), e);
        }
        return material;

    }

    private static void setup() {
        if (setup) return;
        try {
            Class<LOTRMaterial> clazz = LOTRMaterial.class;
            constructor = clazz.getDeclaredConstructor(new Class[] {String.class});
            constructor.setAccessible(true);
            setUses = clazz.getDeclaredMethod("setUses", new Class[] {int.class});
            setUses.setAccessible(true);
            setDamage = clazz.getDeclaredMethod("setDamage", new Class[] {float.class});
            setDamage.setAccessible(true);
            setProtection = clazz.getDeclaredMethod("setProtection", new Class[] {float.class});
            setProtection.setAccessible(true);
            setSpeed = clazz.getDeclaredMethod("setSpeed", new Class[] {float.class});
            setSpeed.setAccessible(true);
            setHarvestLevel = clazz.getDeclaredMethod("setHarvestLevel", new Class[] {int.class});
            setHarvestLevel.setAccessible(true);
            setEnchantibility = clazz.getDeclaredMethod("setEnchantability", new Class[] {int.class});
            setEnchantibility.setAccessible(true);
            setCraftingMaterial = clazz.getDeclaredMethod("setCraftingItems", new Class[] {Item.class, Item.class});
            setCraftingMaterial.setAccessible(true);
            setUndamageable = clazz.getDeclaredMethod("setUndamageable", new Class[] {});
            setUndamageable.setAccessible(true);
            setManFlesh = clazz.getDeclaredMethod("setManFlesh", new Class[] {});
            setManFlesh.setAccessible(true);
            setup = true;
        }
        catch (NoSuchMethodException | SecurityException e) {
            LOTRFA.logger.error("Failed to setup LOTRFAMaterials.", e);
        }
    }
    
    private static void setExistingCraftingItem(LOTRMaterial material, Item item) {
        setExistingCraftingItem(material, new ItemStack(item), item);
    }
    
    private static void setExistingCraftingItem(LOTRMaterial material, ItemStack toolItem, Item armorItem) {
        LOTRFAReflectionHelper.setToolMaterialRepairItem(material.toToolMaterial(), toolItem);
        material.toArmorMaterial().customCraftingMaterial = armorItem;
    }
    
    public static ArmorMaterial getFullArmorMaterial(EntityLivingBase entity) {
        ArmorMaterial material = null;
        for (int i = 1; i <= 4; i++) {
            ItemStack item = entity.getEquipmentInSlot(i);
            if (item == null || !(item.getItem() instanceof ItemArmor)) return null;
            
            ArmorMaterial itemMaterial = ((ItemArmor) item.getItem()).getArmorMaterial();
            if(material != null && itemMaterial != material) return null;

            material = itemMaterial;
        }
        return material;
    }
}
