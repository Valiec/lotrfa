package eoa.lotrfa.common.item.equipment;

import eoa.lotrfa.common.LOTRFACreativeTabs;
import lotr.common.item.*;

public class LOTRFAItemCleaver extends LOTRItemSword {

    public LOTRFAItemCleaver(LOTRMaterial material) {
        super(material);
        setCreativeTab(LOTRFACreativeTabs.tabCombat);
        lotrWeaponDamage += 1.0f;

        LOTRWeaponStats.registerMeleeSpeed(LOTRFAItemCleaver.class, 0.88f);
        LOTRWeaponStats.registerMeleeReach(LOTRFAItemCleaver.class, 1.15f);
    }
    
    


}
