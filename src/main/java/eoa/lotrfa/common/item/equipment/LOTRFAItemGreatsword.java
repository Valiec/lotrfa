package eoa.lotrfa.common.item.equipment;

import eoa.lotrfa.common.LOTRFACreativeTabs;
import lotr.common.item.*;

public class LOTRFAItemGreatsword extends LOTRItemSword {

    public LOTRFAItemGreatsword(LOTRMaterial material) {
        super(material);
        setCreativeTab(LOTRFACreativeTabs.tabCombat);
        lotrWeaponDamage += 0.5f;

        LOTRWeaponStats.registerMeleeSpeed(LOTRFAItemGreatsword.class, 0.9f);
        LOTRWeaponStats.registerMeleeReach(LOTRFAItemGreatsword.class, 1.15f);
    }

}
