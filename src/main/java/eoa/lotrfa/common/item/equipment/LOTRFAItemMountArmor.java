package eoa.lotrfa.common.item.equipment;

import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.item.LOTRItemMountArmor;
import lotr.common.item.LOTRMaterial;
import net.minecraft.item.Item;

public class LOTRFAItemMountArmor extends LOTRItemMountArmor {

    public LOTRFAItemMountArmor(LOTRMaterial material, Mount mount) {
        super(material, mount);
    }
    
    //Unused, overrides to prevent issues.
    @Override
    public LOTRItemMountArmor setTemplateItem(Item item) {
        return this;
    }
    
    @Override
    public String getArmorTexture() {
        String path = null;
        
        String mountName = LOTRFAReflectionHelper.getMountArmorType(this).textureName;
        String materialName = getMountArmorMaterial().name().toLowerCase();
        if (materialName.startsWith("lotr_")) materialName = materialName.substring("lotr_".length());
        
        path = "lotrfa:armor/mount/" + mountName + "_" + materialName + ".png";
        
        return path;
    }

}
