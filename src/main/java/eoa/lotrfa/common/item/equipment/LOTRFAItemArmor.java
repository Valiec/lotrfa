package eoa.lotrfa.common.item.equipment;

import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.item.LOTRItemArmor;
import lotr.common.item.LOTRMaterial;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;

public class LOTRFAItemArmor extends LOTRItemArmor {

    public LOTRFAItemArmor(LOTRMaterial material, int slotType) {
        super(material, slotType);
    }
    
    public LOTRFAItemArmor(LOTRMaterial material, int slotType, String textureSuffix) {
        super(material, slotType, textureSuffix);
    }
    
    @Override
    public String getArmorTexture(ItemStack itemstack, Entity entity, int slot, String type) {
        String texture = "lotrfa:armor/" + LOTRFAReflectionHelper.getArmorName(this);
        if (type != null) texture += "_" + type;
        
        return texture + ".png";
    }
}
