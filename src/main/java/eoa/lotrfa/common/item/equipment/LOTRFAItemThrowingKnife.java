package eoa.lotrfa.common.item.equipment;

import lotr.common.item.LOTRItemThrowingAxe;
import lotr.common.item.LOTRMaterial;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class LOTRFAItemThrowingKnife extends LOTRItemThrowingAxe {

    public LOTRFAItemThrowingKnife(LOTRMaterial material) {
        this(material.toToolMaterial());
    }
    
    public LOTRFAItemThrowingKnife(Item.ToolMaterial material) {
        super(material);
        this.setMaxStackSize(3);
    }

    
    @Override
    public float getRangedDamageMultiplier(ItemStack itemstack, Entity shooter, Entity hit) {
        return super.getRangedDamageMultiplier(itemstack, shooter, hit) * 0.5f;
    }
}
