package eoa.lotrfa.common.item.equipment;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.lotrfa.common.item.LOTRFAMaterial;
import lotr.client.model.LOTRModelRohanMarshalHelmet;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;

public class LOTRFAItemHouseBorCaptainHelmet extends LOTRFAItemArmor {
    @SideOnly(Side.CLIENT)
    private ModelBiped model;

    public LOTRFAItemHouseBorCaptainHelmet() {
        super(LOTRFAMaterial.HOUSE_BOR_CAPTAIN, 0, "helmet");
    }

    @Override
    @SideOnly(Side.CLIENT)
    public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack itemStack, int armorSlot) {
        if (armorSlot == 0) {
            if (model == null) model = new LOTRModelRohanMarshalHelmet(1.0f);
            return model;
        }
        return super.getArmorModel(entityLiving, itemStack, armorSlot);
    }
}
