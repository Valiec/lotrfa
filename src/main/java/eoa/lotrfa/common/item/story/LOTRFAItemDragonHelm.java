package eoa.lotrfa.common.item.story;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.lotrfa.common.item.LOTRFAMaterial;
import eoa.lotrfa.common.item.equipment.LOTRFAItemArmor;
import lotr.client.model.LOTRModelUmbarHelmet;
import lotr.common.LOTRCreativeTabs;
import lotr.common.item.LOTRStoryItem;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;

public class LOTRFAItemDragonHelm extends LOTRFAItemArmor implements LOTRStoryItem {
    @SideOnly(Side.CLIENT)
    private ModelBiped model;

    public LOTRFAItemDragonHelm() {
        super(LOTRFAMaterial.DRAGON_HELM, 0);
        setCreativeTab(LOTRCreativeTabs.tabStory);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack itemStack, int armorSlot) {
        if (armorSlot == 0) {
            if (model == null) model = new LOTRModelUmbarHelmet(1.0f);
            return model;
        }
        return super.getArmorModel(entityLiving, itemStack, armorSlot);
    }
    
    @Override
    public String getArmorTexture(ItemStack itemstack, Entity entity, int slot, String type) {
        if(slot == 0) return "lotrfa:armor/dragon_helm.png";
        return super.getArmorTexture(itemstack, entity, slot, type);
    }
}
