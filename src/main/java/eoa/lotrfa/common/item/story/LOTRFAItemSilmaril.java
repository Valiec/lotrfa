package eoa.lotrfa.common.item.story;

import java.util.List;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lotr.common.*;
import lotr.common.LOTRFaction.FactionType;
import lotr.common.item.LOTRStoryItem;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class LOTRFAItemSilmaril extends Item implements LOTRStoryItem {

    @SideOnly(Side.CLIENT)
    private IIcon[] icons;

    public LOTRFAItemSilmaril() {
        setCreativeTab(LOTRCreativeTabs.tabStory);
        setHasSubtypes(true);
        setMaxStackSize(1);
        setHasSubtypes(true);
    }

    @Override
    public void onUpdate(ItemStack itemStack, World world, Entity entity, int p_77663_4_, boolean p_77663_5_) {
        if(entity.ticksExisted % 10 != 0) return; 
        
        if (entity instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) entity;
            ItemStack item = player.getHeldItem();
            
            if(item != null && item.getItem() == this && !canHoldSilmaril(player)) {
                entity.setFire(10);
            }
        }
    }
    
    private boolean canHoldSilmaril(EntityPlayer player) {
        LOTRPlayerData pd = LOTRLevelData.getData(player);
        //TODO change to pledge
        for(LOTRFaction faction : LOTRFaction.getAllOfType(FactionType.TYPE_ELF)) {
            if(pd.getAlignment(faction) >= 50) return true;
        }

        return false;
    }

    @Override
    public boolean hasEffect(ItemStack par1) {
        return true;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void getSubItems(Item item, CreativeTabs tab, List list) {
        for(SilmarilType type : SilmarilType.values()) {
            list.add(new ItemStack(item, 1, type.ordinal()));
        }
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister register) {
        icons = new IIcon[SilmarilType.values().length];
        
        for(SilmarilType type : SilmarilType.values()) {
            icons[type.ordinal()] = register.registerIcon(this.getIconString() + "_" + type.getName());
        }
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int damage) {
        if(damage <= icons.length - 1) {
            return icons[damage];
        }
        
        return super.getIconFromDamage(damage);
    }
    
    @Override
    public String getUnlocalizedName(ItemStack itemstack) {
        return super.getUnlocalizedName() + "." + getSilmarilType(itemstack).getName();
    }
    
    public static SilmarilType getSilmarilType(ItemStack item) {
        if(item == null) return null;
        
        return SilmarilType.values()[item.getItemDamage()];
    }
    
    public static enum SilmarilType {
        FIRE("fire"),
        WATER("water"),
        SKY("sky");
        
        private String name;
        
        private SilmarilType(String name) {
            this.name = name;
        }
        
        public String getName() {
            return name;
        }
    }
}
