package eoa.lotrfa.common.item;

import java.util.*;
import java.util.Map.Entry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.lotrfa.common.*;
import eoa.lotrfa.common.entity.item.LOTRFAEntityBanner;
import eoa.lotrfa.common.entity.item.LOTRFAEntityBannerWall;
import eoa.lotrfa.common.reflection.LOTRFAEnumHelper;
import lotr.common.*;
import lotr.common.item.LOTRItemBanner;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class LOTRFAItemBanner extends LOTRItemBanner {
    @SideOnly(value = Side.CLIENT)
    private Map<Integer, IIcon> FABannerIconsForID;

    public LOTRFAItemBanner() {
        super();
        this.setCreativeTab(LOTRFACreativeTabs.tabFA);
    }

    public static BannerType getBannerType(ItemStack itemstack) {
        if (itemstack.getItem() instanceof LOTRFAItemBanner) {
            return getBannerType(itemstack.getItemDamage());
        }
        return null;
    }

    public static BannerType getBannerType(int i) {
        return FABannerTypes.forID(i);
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public void getSubItems(Item item, CreativeTabs tab, List list) {
        for (BannerType type : FABannerTypes.FABannerTypes) {
            list.add(new ItemStack(item, 1, type.bannerID));
        }
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public void registerIcons(IIconRegister iconregister) {
        this.FABannerIconsForID = new HashMap<Integer, IIcon>();
        for (BannerType banner : FABannerTypes.FABannerTypes) {
            FABannerIconsForID.put(banner.bannerID, iconregister.registerIcon(this.getIconString() + "_" + banner.bannerName));
        }
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public IIcon getIconFromDamage(int i) {
        IIcon icon = FABannerIconsForID.get(i);
        if (icon == null) {
            // A fail safe
            for (Entry<Integer, IIcon> entry : FABannerIconsForID.entrySet()) return FABannerIconsForID.get(entry.getKey());
        }
        return icon;
    }
    
    @Override
    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int side, float f, float f1, float f2) {
        BannerType bannerType = LOTRFAItemBanner.getBannerType(itemstack);
        NBTTagCompound protectData = LOTRFAItemBanner.getProtectionData(itemstack);
        if (world.getBlock(i, j, k).isReplaceable(world, i, j, k)) {
            side = 1;
        }
        else if (side == 1) {
            ++j;
        }
        if (side == 0) {
            return false;
        }
        if (side == 1) {
            if (!entityplayer.canPlayerEdit(i, j, k, side, itemstack)) {
                return false;
            }
            Block block = world.getBlock(i, j - 1, k);
            int meta = world.getBlockMetadata(i, j - 1, k);
            if (block.isSideSolid(world, i, j - 1, k, ForgeDirection.UP)) {
                int protectRange;
                if (LOTRConfig.allowBannerProtection && !entityplayer.capabilities.isCreativeMode && (protectRange = LOTRBannerProtection.getProtectionRange(block, meta)) > 0) {
                    LOTRFaction faction = bannerType.faction;
                    if (LOTRLevelData.getData(entityplayer).getAlignment(faction) < 1) {
                        if (!world.isRemote) {
                            LOTRAlignmentValues.notifyAlignmentNotHighEnough(entityplayer, 1, faction);
                        }
                        return false;
                    }
                    if (!world.isRemote && LOTRBannerProtection.isProtectedByBanner(world, i, j, k, LOTRBannerProtection.forPlayer(entityplayer), false, protectRange)) {
                        entityplayer.addChatMessage(new ChatComponentTranslation("chat.lotr.alreadyProtected", new Object[0]));
                        return false;
                    }
                }
                if (!world.isRemote) {
                    LOTRFAEntityBanner banner = new LOTRFAEntityBanner(world);
                    banner.setLocationAndAngles(i + 0.5, j, k + 0.5, 90.0f * (MathHelper.floor_double(entityplayer.rotationYaw * 4.0f / 360.0f + 0.5) & 3), 0.0f);
                    if (world.checkNoEntityCollision(banner.boundingBox) && world.getCollidingBoundingBoxes(banner, banner.boundingBox).size() == 0 && !world.isAnyLiquid(banner.boundingBox)) {
                        banner.setBannerType(bannerType);
                        banner.setPlacingPlayer(entityplayer);
                        if (protectData != null) {
                            banner.readProtectionFromNBT(protectData);
                        }
                        world.spawnEntityInWorld(banner);
                        if (banner.isProtectingTerritory()) {
                            LOTRLevelData.getData(entityplayer).addAchievement(LOTRAchievement.bannerProtect);
                        }
                        world.playSoundAtEntity(banner, Blocks.planks.stepSound.func_150496_b(), (Blocks.planks.stepSound.getVolume() + 1.0f) / 2.0f, Blocks.planks.stepSound.getPitch() * 0.8f);
                        --itemstack.stackSize;
                        return true;
                    }
                    banner.setDead();
                }
            }
        }
        else {
            if (!entityplayer.canPlayerEdit(i, j, k, side, itemstack)) {
                return false;
            }
            if (!world.isRemote) {
                int l = Direction.facingToDirection[side];
                LOTRFAEntityBannerWall banner = new LOTRFAEntityBannerWall(world, i, j, k, l);
                if (banner.onValidSurface()) {
                    banner.setBannerType(bannerType);
                    if (protectData != null) {
                        banner.setProtectData((NBTTagCompound) protectData.copy());
                    }
                    world.spawnEntityInWorld(banner);
                    world.playSoundAtEntity(banner, Blocks.planks.stepSound.func_150496_b(), (Blocks.planks.stepSound.getVolume() + 1.0f) / 2.0f, Blocks.planks.stepSound.getPitch() * 0.8f);
                    --itemstack.stackSize;
                    return true;
                }
                banner.setDead();
            }
        }
        return false;
    }

    public static class FABannerTypes {
        public static List<BannerType> FABannerTypes;
        public static BannerType angband;
        public static BannerType brethil;
        public static BannerType belegost;
        public static BannerType dorDaedeloth;
        public static BannerType dorDaidelos;
        public static BannerType dorLomin;
        public static BannerType doriath;
        public static BannerType falathrim;
        public static BannerType feanorian;
        public static BannerType gondolin;
        public static BannerType gondolinFountain;
        public static BannerType gondolinGoldenFlower;
        public static BannerType gondolinHammerWrath;
        public static BannerType gondolinHarp;
        public static BannerType gondolinHeavenlyArch;
        public static BannerType gondolinMole;
        public static BannerType gondolinPillar;
        public static BannerType gondolinSwallow;
        public static BannerType gondolinTowerSnow;
        public static BannerType gondolinTree;
        public static BannerType gondolinWing;
        public static BannerType hithlum;
        public static BannerType houseBeor;
        public static BannerType houseBor;
        public static BannerType houseUlfang;
        public static BannerType laegrim;
        public static BannerType nargothrond;
        public static BannerType nogrod;
        public static BannerType pettyDwarves;
        public static BannerType tolSirion;
        public static BannerType tolInGaurhoth;
        public static BannerType utumnoRemnant;
        public static BannerType valinor;
        

        public static void init() {
            FABannerTypes = new ArrayList<BannerType>();
            angband = addAndReturn(LOTRFAEnumHelper.addBannerType("ANGBAND", 50, "angband", LOTRFAFactions.angband));
            brethil = addAndReturn(LOTRFAEnumHelper.addBannerType("BRETHIL", 51, "brethil", LOTRFAFactions.brethil));
            belegost = addAndReturn(LOTRFAEnumHelper.addBannerType("BELEGOST", 52, "belegost", LOTRFaction.BLUE_MOUNTAINS));
            dorDaedeloth = addAndReturn(LOTRFAEnumHelper.addBannerType("DOR_DAEDELOTH", 71, "dorDaedeloth", LOTRFAFactions.angband));
            dorDaidelos = addAndReturn(LOTRFAEnumHelper.addBannerType("DOR_DAIDELOS", 53, "dorDaidelos", LOTRFAFactions.dorDaidelos));
            dorLomin = addAndReturn(LOTRFAEnumHelper.addBannerType("DOR_LOMIN", 54, "dorLomin", LOTRFAFactions.dorLomin));
            doriath = addAndReturn(LOTRFAEnumHelper.addBannerType("DORIATH", 55, "doriath", LOTRFAFactions.doriath));
            falathrim = addAndReturn(LOTRFAEnumHelper.addBannerType("FALATHRIM", 56, "falathrim", LOTRFAFactions.falathrim));
            feanorian = addAndReturn(LOTRFAEnumHelper.addBannerType("FEANORIAN", 57, "feanorian", LOTRFAFactions.feanorians));
            gondolin = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN", 58, "gondolin", LOTRFAFactions.gondolin));
            gondolinFountain = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_FOUNTAIN", 71, "gondolinFountain", LOTRFAFactions.gondolin));
            gondolinGoldenFlower = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_GOLDEN_FLOWER", 72, "gondolinGoldenFlower", LOTRFAFactions.gondolin));
            gondolinHammerWrath = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_HAMMER_WRATH", 73, "gondolinHammerWrath", LOTRFAFactions.gondolin));
            gondolinHarp = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_HARP", 74, "gondolinHarp", LOTRFAFactions.gondolin));
            gondolinHeavenlyArch = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_HEAVENLY_ARCH", 75, "gondolinHeavenlyArch", LOTRFAFactions.gondolin));
            gondolinMole = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_MOLE", 76, "gondolinMole", LOTRFAFactions.gondolin));
            gondolinPillar = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_PILLAR", 77, "gondolinPillar", LOTRFAFactions.gondolin));
            gondolinSwallow = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_SWALLOW", 78, "gondolinSwallow", LOTRFAFactions.gondolin));
            gondolinTowerSnow = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_TOWER_SNOW", 79, "gondolinTowerSnow", LOTRFAFactions.gondolin));
            gondolinTree = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_TREE", 80, "gondolinTree", LOTRFAFactions.gondolin));
            gondolinWing = addAndReturn(LOTRFAEnumHelper.addBannerType("GONDOLIN_WING", 81, "gondolinWing", LOTRFAFactions.gondolin));
            hithlum = addAndReturn(LOTRFAEnumHelper.addBannerType("HITHLUM", 59, "hithlum", LOTRFAFactions.hithlum));
            houseBeor = addAndReturn(LOTRFAEnumHelper.addBannerType("HOUSE_BEOR", 60, "houseBeor", LOTRFaction.UNALIGNED));
            houseBor = addAndReturn(LOTRFAEnumHelper.addBannerType("HOUSE_BOR", 61, "houseBor", LOTRFAFactions.houseBor));
            houseUlfang = addAndReturn(LOTRFAEnumHelper.addBannerType("HOUSE_ULFANG", 62, "houseUlfang", LOTRFAFactions.houseUlfang));
            laegrim = addAndReturn(LOTRFAEnumHelper.addBannerType("LAEGRIM", 63, "laegrim", LOTRFAFactions.laegrim));
            nargothrond = addAndReturn(LOTRFAEnumHelper.addBannerType("NARGOTHROND", 64, "nargothrond", LOTRFAFactions.nargothrond));
            nogrod = addAndReturn(LOTRFAEnumHelper.addBannerType("NOGROD", 65, "nogrod", LOTRFaction.BLUE_MOUNTAINS));
            pettyDwarves = addAndReturn(LOTRFAEnumHelper.addBannerType("PETTY_DWARVES", 66, "pettyDwarves", LOTRFAFactions.pettyDwarf));
            tolSirion = addAndReturn(LOTRFAEnumHelper.addBannerType("TOL_SIRION", 67, "tolSirion", LOTRFAFactions.nargothrond));
            tolInGaurhoth = addAndReturn(LOTRFAEnumHelper.addBannerType("TOL_IN_GAURHOTH", 68, "tolInGaurhoth", LOTRFAFactions.tolInGaurhoth));
            utumnoRemnant = addAndReturn(LOTRFAEnumHelper.addBannerType("UTUMNO_REMNANT", 69, "utumnoRemnant", LOTRFAFactions.dorDaidelos));
            valinor = addAndReturn(LOTRFAEnumHelper.addBannerType("VALINOR", 70, "valinor", LOTRFaction.UNALIGNED));
        }

        public static BannerType forID(int id) {
            for (BannerType banner : FABannerTypes) {
                if (banner.bannerID == id) return banner;
            }

            LOTRFA.logger.error("Unknown banner type of ID: " + id);
            return null;
        }

        private static BannerType addAndReturn(BannerType banner) {
            FABannerTypes.add(banner);
            return banner;
        }

    }

}
