package eoa.lotrfa.common;

import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.LOTRCreativeTabs;
import net.minecraft.item.ItemStack;

public class LOTRFACreativeTabs {
    public static LOTRCreativeTabs tabCombat;
    public static LOTRCreativeTabs tabTools;
    public static LOTRCreativeTabs tabMaterials;
    public static LOTRCreativeTabs tabBlocks;
    public static LOTRCreativeTabs tabFA;

    public static void init() {
        tabCombat = LOTRFAReflectionHelper.getLOTRCreativeTab("tabCombat");
        tabTools = LOTRFAReflectionHelper.getLOTRCreativeTab("tabTools");
        tabMaterials = LOTRFAReflectionHelper.getLOTRCreativeTab("tabMaterials");
        tabBlocks = LOTRFAReflectionHelper.getLOTRCreativeTab("tabBlock");
        tabFA = new LOTRCreativeTabs("tabFA");
    }

    public static void setupIcons() {
        tabFA.theIcon = new ItemStack(LOTRFAItems.silmaril);
    }

}
