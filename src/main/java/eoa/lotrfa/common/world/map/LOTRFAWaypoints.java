package eoa.lotrfa.common.world.map;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.reflection.LOTRFAEnumHelper;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.LOTRFaction;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.map.LOTRWaypoint.Region;

public class LOTRFAWaypoints {

    public static LOTRWaypoint amonDarthir;
    public static LOTRWaypoint amonErebFA;
    public static LOTRWaypoint amonEthir;
    public static LOTRWaypoint amonObel;
    public static LOTRWaypoint amonRudh;
    public static LOTRWaypoint angband;
    public static LOTRWaypoint annonInGelydh;
    public static LOTRWaypoint ardGalen;
    public static LOTRWaypoint balar;
    public static LOTRWaypoint baradEithel;
    public static LOTRWaypoint baradNimras;
    public static LOTRWaypoint belegostFA;
    public static LOTRWaypoint brithombar;
    public static LOTRWaypoint dorDaedeloth;
    public static LOTRWaypoint dorFirnIGuinar;
    public static LOTRWaypoint dorLomin;
    public static LOTRWaypoint eglarest;
    public static LOTRWaypoint eithelIvrin;
    public static LOTRWaypoint estolad;
    public static LOTRWaypoint fensOfSirion;
    public static LOTRWaypoint foen;
    public static LOTRWaypoint gardhBoren;
    public static LOTRWaypoint gatesOfSirion;
    public static LOTRWaypoint havensOfSirion;
    public static LOTRWaypoint himlad;
    public static LOTRWaypoint himring;
    public static LOTRWaypoint iantIaur;
    public static LOTRWaypoint lakeHelevorn;
    public static LOTRWaypoint lakeMithrim;
    public static LOTRWaypoint lammoth;
    public static LOTRWaypoint lanthirLamath;
    public static LOTRWaypoint lantInOnodrim;
    public static LOTRWaypoint linaewen;
    public static LOTRWaypoint lothlann;
    public static LOTRWaypoint maglorsGap;
    public static LOTRWaypoint mountDolmedFA;
    public static LOTRWaypoint mountRerirFA;
    public static LOTRWaypoint nanDuaith;
    public static LOTRWaypoint brithiach;
    public static LOTRWaypoint nanElmoth;
    public static LOTRWaypoint nanTathren;
    public static LOTRWaypoint nimbrethil;
    public static LOTRWaypoint nogrodFA;
    public static LOTRWaypoint northHithlum;
    public static LOTRWaypoint ramdal;
    public static LOTRWaypoint talathDirnen;
    public static LOTRWaypoint talathdru;
    public static LOTRWaypoint tarnAeluin;
    public static LOTRWaypoint taurEnFaroth;
    public static LOTRWaypoint thargelion;
    public static LOTRWaypoint tolGalen;
    public static LOTRWaypoint tolSirion;
    public static LOTRWaypoint vinyamar;
    public static LOTRWaypoint crossingTaeglin;
    public static LOTRWaypoint passAglon;
    public static LOTRWaypoint passAnach;
    public static LOTRWaypoint arossiach;
    public static LOTRWaypoint sarnAthrad;
    public static LOTRWaypoint rivilsWell;
    public static LOTRWaypoint orodNaThon;
    public static LOTRWaypoint ladrosWasteland;
    public static LOTRWaypoint littleGelion;

    public static LOTRWaypoint amonGwareth;
    public static LOTRWaypoint dryRiver;
    public static LOTRWaypoint menegroth;
    public static LOTRWaypoint nargothrond;
    public static LOTRWaypoint haudaliebar;

    public static void setupWaypoints() {
        Regions.init();
        removeWaypoints();
        addWaypoints();

        // TODO move this to new class
        /*
         * try {
         * Field menu = LOTRGuiMainMenu.class.getDeclaredField("waypointRoute");
         * menu.setAccessible(true);
         * List<LOTRWaypoint> wproute = (List<LOTRWaypoint>) menu.get(LOTRGuiMainMenu.class); wproute.clear();
         * wproute.add(LOTRWaypoint.HOBBITON);
         * wproute.add(LOTRFAWaypoints.nogrodFA);
         * wproute.add(LOTRFAWaypoints.taurEnFaroth);
         * wproute.add(LOTRFAWaypoints.thangorodrim);
         * menu.set(LOTRGuiMainMenu.class, wproute);
         * }
         * catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
         * e.printStackTrace();
         * }
         */
    }

    public static void removeWaypoints() {
        for (LOTRWaypoint wp : LOTRWaypoint.values()) LOTRFAReflectionHelper.disableWaypoint(wp);
    }

    public static void addWaypoints() {
        amonDarthir = LOTRFAEnumHelper.addWaypoint("AMON_DARTHIR", Regions.hithlum, LOTRFaction.UNALIGNED, 692, 537);
        amonErebFA = LOTRFAEnumHelper.addWaypoint("AMON_EREB_FA", Regions.eastBeleriand, LOTRFAFactions.feanorians, 945, 692);
        amonEthir = LOTRFAEnumHelper.addWaypoint("AMON_ETHIR", Regions.eastBeleriand, LOTRFAFactions.hithlum, 708, 639);
        amonObel = LOTRFAEnumHelper.addWaypoint("AMON_OBEL", Regions.brethil, LOTRFAFactions.brethil, 757, 580);
        amonRudh = LOTRFAEnumHelper.addWaypoint("AMON_RUDH", Regions.amonRudh, LOTRFaction.UNALIGNED, 756, 605);
        angband = LOTRFAEnumHelper.addWaypoint("ANGBAND", Regions.angband, LOTRFAFactions.angband, 850, 425);
        annonInGelydh = LOTRFAEnumHelper.addWaypoint("ANNON_IN_GELYDH", Regions.hithlum, LOTRFAFactions.hithlum, 643, 498);
        ardGalen = LOTRFAEnumHelper.addWaypoint("ARD_GALEN", Regions.ardGalen, LOTRFaction.UNALIGNED, 844, 462);
        balar = LOTRFAEnumHelper.addWaypoint("BALAR", LOTRWaypoint.Region.OCEAN, LOTRFaction.UNALIGNED, 681, 829);
        baradEithel = LOTRFAEnumHelper.addWaypoint("BARAD_EITHEL", Regions.hithlum, LOTRFAFactions.hithlum, 749, 483);
        baradNimras = LOTRFAEnumHelper.addWaypoint("BARAD_NIMRAS", Regions.falas, LOTRFAFactions.hithlum, 577, 681);
        belegostFA = LOTRFAEnumHelper.addWaypoint("BELEGOST_FA", LOTRWaypoint.Region.BLUE_MOUNTAINS, LOTRFaction.BLUE_MOUNTAINS, 1063, 607);
        brithombar = LOTRFAEnumHelper.addWaypoint("BRITHOMBAR", Regions.falas, LOTRFAFactions.falathrim, 580, 648);
        dorDaedeloth = LOTRFAEnumHelper.addWaypoint("DOR_DAEDELOTH", Regions.ardGalen, LOTRFAFactions.angband, 852, 441);
        dorFirnIGuinar = LOTRFAEnumHelper.addWaypoint("DOR_FIRN_I_GUINAR", Regions.ossiriand, LOTRFAFactions.laegrim, 1048, 822);
        dorLomin = LOTRFAEnumHelper.addWaypoint("DOR_LOMIN", Regions.dorLomin, LOTRFAFactions.dorLomin, 670, 516);
        eglarest = LOTRFAEnumHelper.addWaypoint("EGLAREST", Regions.falas, LOTRFAFactions.falathrim, 629, 664);
        eithelIvrin = LOTRFAEnumHelper.addWaypoint("EITHEL_IVRIN", Regions.westBeleriand, LOTRFaction.UNALIGNED, 687, 551);
        estolad = LOTRFAEnumHelper.addWaypoint("ESTOLAD", Regions.estolad, LOTRFaction.UNALIGNED, 931, 591);
        fensOfSirion = LOTRFAEnumHelper.addWaypoint("FENS_OF_SIRION", Regions.passSirion, LOTRFaction.UNALIGNED, 771, 643);
        foen = LOTRFAEnumHelper.addWaypoint("FOEN", Regions.taurNuFuin, LOTRFaction.UNALIGNED, 828, 503);
        gardhBoren = LOTRFAEnumHelper.addWaypoint("GARDH_BOREN", Regions.gardhBoren, LOTRFaction.UNALIGNED, 947, 554);
        gatesOfSirion = LOTRFAEnumHelper.addWaypoint("GATES_OF_SIRION", Regions.eastBeleriand, LOTRFaction.UNALIGNED, 761, 659);
        havensOfSirion = LOTRFAEnumHelper.addWaypoint("HAVENS_OF_SIRION", Regions.westBeleriand, LOTRFaction.UNALIGNED, 735, 769);
        himlad = LOTRFAEnumHelper.addWaypoint("HIMLAD", Regions.feanor, LOTRFaction.UNALIGNED, 901, 549);
        himring = LOTRFAEnumHelper.addWaypoint("HIMRING", Regions.feanor, LOTRFAFactions.hithlum, 930, 524);
        iantIaur = LOTRFAEnumHelper.addWaypoint("IANT_IAUR", Regions.ossiriand, LOTRFAFactions.laegrim, 859, 546);
        lakeHelevorn = LOTRFAEnumHelper.addWaypoint("LAKE_HELEVORN", LOTRWaypoint.Region.BLUE_MOUNTAINS, LOTRFaction.UNALIGNED, 1033, 535);
        lakeMithrim = LOTRFAEnumHelper.addWaypoint("LAKE_MITHRIM", Regions.hithlum, LOTRFAFactions.hithlum, 715, 479);
        lammoth = LOTRFAEnumHelper.addWaypoint("LAMMOTH", Regions.lammoth, LOTRFaction.UNALIGNED, 557, 454);
        lanthirLamath = LOTRFAEnumHelper.addWaypoint("LANTHIR_LAMATH", Regions.ossiriand, LOTRFAFactions.laegrim, 1060, 800);
        lantInOnodrim = LOTRFAEnumHelper.addWaypoint("LANT_IN_ONODRIM", Regions.southBeleriand, LOTRFaction.FANGORN, 863, 808);
        linaewen = LOTRFAEnumHelper.addWaypoint("LINAEWEN", Regions.westBeleriand, LOTRFaction.UNALIGNED, 617, 552);
        lothlann = LOTRFAEnumHelper.addWaypoint("LOTHLANN", Regions.lothlann, LOTRFaction.UNALIGNED, 1005, 467);
        maglorsGap = LOTRFAEnumHelper.addWaypoint("MAGLOR'S_GAP", Regions.feanor, LOTRFAFactions.hithlum, 991, 523);
        mountDolmedFA = LOTRFAEnumHelper.addWaypoint("MOUNT_DOLMED_FA", LOTRWaypoint.Region.BLUE_MOUNTAINS, LOTRFaction.UNALIGNED, 1042, 622);
        mountRerirFA = LOTRFAEnumHelper.addWaypoint("MOUNT_RERIR_FA", LOTRWaypoint.Region.BLUE_MOUNTAINS, LOTRFaction.UNALIGNED, 1025, 530);
        nanDuaith = LOTRFAEnumHelper.addWaypoint("NAN_DUAITH", Regions.nanDuaith, LOTRFaction.UNALIGNED, 718, 399);
        brithiach = LOTRFAEnumHelper.addWaypoint("BRITHIACH", Regions.westBeleriand, LOTRFaction.UNALIGNED, 762, 548);
        nanElmoth = LOTRFAEnumHelper.addWaypoint("NAN_ELMOTH", Regions.nanElmoth, LOTRFAFactions.laegrim, 917, 580);
        nanTathren = LOTRFAEnumHelper.addWaypoint("NAN_TATHREN", Regions.westBeleriand, LOTRFaction.UNALIGNED, 764, 713);
        nimbrethil = LOTRFAEnumHelper.addWaypoint("NIMBRETHIL", Regions.westBeleriand, LOTRFaction.UNALIGNED, 715, 745);
        nogrodFA = LOTRFAEnumHelper.addWaypoint("NOGROD_FA", LOTRWaypoint.Region.BLUE_MOUNTAINS, LOTRFaction.BLUE_MOUNTAINS, 1060, 626);
        northHithlum = LOTRFAEnumHelper.addWaypoint("NORTH_HITHLUM", Regions.hithlum, LOTRFAFactions.hithlum, 663, 455);
        ramdal = LOTRFAEnumHelper.addWaypoint("RAMDAL", Regions.eastBeleriand, LOTRFaction.UNALIGNED, 909, 688);
        talathDirnen = LOTRFAEnumHelper.addWaypoint("TALATH_DIRNEN", Regions.eastBeleriand, LOTRFAFactions.hithlum, 724, 600);
        talathdru = LOTRFAEnumHelper.addWaypoint("TALATHDRU", Regions.taurdru, LOTRFaction.UNALIGNED, 998, 506);
        tarnAeluin = LOTRFAEnumHelper.addWaypoint("TARN_AELUIN", Regions.taurNuFuin, LOTRFaction.UNALIGNED, 854, 496);
        taurEnFaroth = LOTRFAEnumHelper.addWaypoint("TAUR_EN_FAROTH", Regions.nargothrond, LOTRFaction.UNALIGNED, 688, 662);
        thargelion = LOTRFAEnumHelper.addWaypoint("THARGELION", Regions.eastBeleriand, LOTRFaction.UNALIGNED, 1004, 592);
        tolGalen = LOTRFAEnumHelper.addWaypoint("TOL_GALEN", Regions.ossiriand, LOTRFAFactions.laegrim, 1033, 809);
        tolSirion = LOTRFAEnumHelper.addWaypoint("TOL_SIRION", Regions.passSirion, LOTRFAFactions.tolInGaurhoth, 755, 510);
        vinyamar = LOTRFAEnumHelper.addWaypoint("VINYAMAR", Regions.hithlum, LOTRFAFactions.hithlum, 553, 568);
        crossingTaeglin = LOTRFAEnumHelper.addWaypoint("CROSSING_TAEGLIN", Regions.westBeleriand, LOTRFaction.UNALIGNED, 740, 586);
        passAnach = LOTRFAEnumHelper.addWaypoint("PASS_ANACH", Regions.taurNuFuin, LOTRFaction.UNALIGNED, 793, 528);
        passAglon = LOTRFAEnumHelper.addWaypoint("PASS_AGLON", Regions.feanor, LOTRFaction.UNALIGNED, 905, 526);
        arossiach = LOTRFAEnumHelper.addWaypoint("AROSSIACH", Regions.feanor, LOTRFaction.UNALIGNED, 879, 544);
        sarnAthrad = LOTRFAEnumHelper.addWaypoint("SARN_ATHRAD", Regions.eastBeleriand, LOTRFaction.UNALIGNED, 993, 652);
        rivilsWell = LOTRFAEnumHelper.addWaypoint("RIVILS_WELL", Regions.taurNuFuin, LOTRFaction.UNALIGNED, 799, 505);
        ladrosWasteland = LOTRFAEnumHelper.addWaypoint("LADROS_WASTELAND", Regions.taurNuFuin, LOTRFaction.UNALIGNED, 884, 486);
        orodNaThon = LOTRFAEnumHelper.addWaypoint("OROD_NA_THON", Regions.taurNuFuin, LOTRFaction.UNALIGNED, 844, 512);
        littleGelion = LOTRFAEnumHelper.addWaypoint("LITTLE_GELION", Regions.gardhBoren, LOTRFaction.UNALIGNED, 967, 538);

        amonGwareth = LOTRFAEnumHelper.addWaypoint("AMON_GWARETH", LOTRFAWaypoints.Regions.gondolin, LOTRFAFactions.gondolin, 769, 529, true);
        dryRiver = LOTRFAEnumHelper.addWaypoint("DRY_RIVER", LOTRFAWaypoints.Regions.gondolin, LOTRFAFactions.gondolin, 760, 535, true);
        menegroth = LOTRFAEnumHelper.addWaypoint("MENEGROTH", LOTRFAWaypoints.Regions.doriath, LOTRFAFactions.doriath, 843, 589, true);
        nargothrond = LOTRFAEnumHelper.addWaypoint("NARGOTHROND", LOTRFAWaypoints.Regions.nargothrond, LOTRFAFactions.nargothrond, 704, 643, true);
        haudaliebar = LOTRFAEnumHelper.addWaypoint("HAUDALIEBAR", LOTRFAWaypoints.Regions.ossiriand, LOTRFAFactions.laegrim, 1028, 691, true);
    }

    public static class Regions {
        public static Region amonRudh;
        public static Region angband;
        public static Region ardGalen;
        public static Region brethil;
        public static Region doriath;
        public static Region dorLomin;
        public static Region eastBeleriand;
        public static Region estolad;
        public static Region falas;
        public static Region feanor;
        public static Region gardhBoren;
        public static Region gondolin;
        public static Region hithlum;
        public static Region lammoth;
        public static Region nanDuaith;
        public static Region nanDungortheb;
        public static Region nanElmoth;
        public static Region nanGuruthos;
        public static Region nargothrond;
        public static Region ossiriand;
        public static Region passSirion;
        public static Region taurdru;
        public static Region taurNuFuin;
        public static Region westBeleriand;
        public static Region southBeleriand;
        public static Region lothlann;

        public static void init() {
            // TODO add don't use most base regins anymore LOTRWaypoint.Region.
            amonRudh = LOTRFAEnumHelper.addWaypointRegion("AMON_RUDH");
            angband = LOTRFAEnumHelper.addWaypointRegion("ANGBAND");
            ardGalen = LOTRFAEnumHelper.addWaypointRegion("ARD_GALEN");
            lothlann = LOTRFAEnumHelper.addWaypointRegion("LOTHLANN");
            brethil = LOTRFAEnumHelper.addWaypointRegion("BRETHIL");
            doriath = LOTRFAEnumHelper.addWaypointRegion("DORIATH");
            dorLomin = LOTRFAEnumHelper.addWaypointRegion("DOR_LOMIN");
            eastBeleriand = LOTRFAEnumHelper.addWaypointRegion("EAST_BELERIAND");
            estolad = LOTRFAEnumHelper.addWaypointRegion("ESTOLAD");
            falas = LOTRFAEnumHelper.addWaypointRegion("FALAS");
            feanor = LOTRFAEnumHelper.addWaypointRegion("FEANOR");
            gardhBoren = LOTRFAEnumHelper.addWaypointRegion("GARDH_BOREN");
            gondolin = LOTRFAEnumHelper.addWaypointRegion("GONDOLIN");
            hithlum = LOTRFAEnumHelper.addWaypointRegion("HITHLUM");
            lammoth = LOTRFAEnumHelper.addWaypointRegion("LAMMOTH");
            nanDuaith = LOTRFAEnumHelper.addWaypointRegion("NAN_DUAITH");
            nanDungortheb = LOTRFAEnumHelper.addWaypointRegion("NAN_DUNGORTHEB");
            nanElmoth = LOTRFAEnumHelper.addWaypointRegion("NAN_ELMOTH");
            nanGuruthos = LOTRFAEnumHelper.addWaypointRegion("NAN_GURUTHOS");
            nargothrond = LOTRFAEnumHelper.addWaypointRegion("NARGOTHROND");
            ossiriand = LOTRFAEnumHelper.addWaypointRegion("OSSIRIAND");
            passSirion = LOTRFAEnumHelper.addWaypointRegion("PASS_SIRION");
            taurdru = LOTRFAEnumHelper.addWaypointRegion("TAURDRU");
            taurNuFuin = LOTRFAEnumHelper.addWaypointRegion("TAUR_NU_FUIN");
            westBeleriand = LOTRFAEnumHelper.addWaypointRegion("WEST_BELERIAND");
            southBeleriand = LOTRFAEnumHelper.addWaypointRegion("SOUTH_BELERIAND");
        }
    }
}
