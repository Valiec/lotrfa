package eoa.lotrfa.common.world.map;

import java.util.Random;
import eoa.lotrfa.common.reflection.LOTRFAUnsafeHelper;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRRoadType.RoadBlock;
import net.minecraft.init.Blocks;
import net.minecraft.world.biome.BiomeGenBase;

public abstract class LOTRFARoadType {
    public static final LOTRFARoadType TEST = new LOTRFARoadType() {
        @Override
        public RoadBlock getBlock(Random random, BiomeGenBase biome, boolean top, boolean slab) {
            return new RoadBlock(Blocks.sponge, 0);
        }
    };
    
    private LOTRRoadType LOTRRoad;
    
    public abstract RoadBlock getBlock(Random random, BiomeGenBase biome, boolean top, boolean slab);

    public float getRepair() {
        return 1.0f;
    }

    public boolean hasFlowers() {
        return false;
    }

    public LOTRFARoadType setRepair(float f) {
        final LOTRFARoadType baseRoad = this;
        return new LOTRFARoadType() {

            @Override
            public RoadBlock getBlock(Random rand, BiomeGenBase biome, boolean top, boolean slab) {
                return baseRoad.getBlock(rand, biome, top, slab);
            }

            @Override
            public float getRepair() {
                return f;
            }

            @Override
            public boolean hasFlowers() {
                return baseRoad.hasFlowers();
            }
        };
    }

    public LOTRFARoadType setHasFlowers(boolean flag) {
        final LOTRFARoadType baseRoad = this;
        return new LOTRFARoadType() {

            @Override
            public RoadBlock getBlock(Random rand, BiomeGenBase biome, boolean top, boolean slab) {
                return baseRoad.getBlock(rand, biome, top, slab);
            }

            @Override
            public float getRepair() {
                return baseRoad.getRepair();
            }

            @Override
            public boolean hasFlowers() {
                return flag;
            }
        };
    }

    public LOTRRoadType getLOTRRoadType() {
        if(LOTRRoad == null) {
            LOTRRoad = getBlankRoadCopy();
            LOTRFAUnsafeHelper.RoadType.changeToRoadType(LOTRRoad, this);
        }
        return LOTRRoad;
    }


    private LOTRRoadType getBlankRoadCopy() {
        return LOTRRoadType.PATH.setRepair(LOTRRoadType.PATH.getRepair());
    }
}
