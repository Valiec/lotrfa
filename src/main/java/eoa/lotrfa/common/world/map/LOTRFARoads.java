package eoa.lotrfa.common.world.map;

import java.util.ArrayList;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.world.map.LOTRRoads;

public class LOTRFARoads {

    public static void init() {
        clearRoads();
        addRoads();
    }

    private static void clearRoads() {
        LOTRRoads.allRoads = new ArrayList<LOTRRoads>();
        LOTRFAReflectionHelper.clearRoadDataBase();
    }

    public static void addRoads() {
        LOTRFAReflectionHelper.registerRoad("OldSouthRoad", LOTRFAWaypoints.nargothrond, LOTRFAWaypoints.talathDirnen, LOTRFAWaypoints.crossingTaeglin, new int[] {744, 547}, new int[] {747, 517});
        LOTRFAReflectionHelper.registerRoad("OldSouthRoad2", new int[] {747, 517}, new int[] {751, 510}, new int[] {761, 499}, new int[] {756, 491}, LOTRFAWaypoints.baradEithel);
        LOTRFAReflectionHelper.registerRoad("PassSirion", new int[] {751, 510}, LOTRFAWaypoints.tolSirion);
        LOTRFAReflectionHelper.registerRoad("Brithiach", new int[] {744, 547}, LOTRFAWaypoints.brithiach, new int[] {772, 556});
        LOTRFAReflectionHelper.registerRoad("Brithiach2", new int[] {772, 556}, new int[] {779, 558}, new int[] {785, 557});
        LOTRFAReflectionHelper.registerRoad("NorthDoriath", LOTRFAWaypoints.arossiach, LOTRFAWaypoints.iantIaur, new int[] {826, 545}, new int[] {785, 557});
        LOTRFAReflectionHelper.registerRoad("PassAglon", LOTRFAWaypoints.arossiach, new int[] {897, 534}, LOTRFAWaypoints.passAglon);
        LOTRFAReflectionHelper.registerRoad("Himlad", LOTRFAWaypoints.arossiach, LOTRFAWaypoints.himlad, new int[] {924, 563}, new int[] {933, 574}, LOTRFAWaypoints.estolad);
        LOTRFAReflectionHelper.registerRoad("Himring", new int[] {923, 544}, LOTRFAWaypoints.himring);
        LOTRFAReflectionHelper.registerRoad("MaglorGap", LOTRFAWaypoints.himlad, new int[] {923, 544}, LOTRFAWaypoints.littleGelion, LOTRFAWaypoints.maglorsGap, LOTRFAWaypoints.talathdru);
        LOTRFAReflectionHelper.registerRoad("Helevorn", new int[] {967, 538}, new int[] {1011, 551}, new int[] {1032, 542}, LOTRFAWaypoints.lakeHelevorn);
        LOTRFAReflectionHelper.registerRoad("DwarfRoad", LOTRFAWaypoints.estolad, new int[] {951, 620}, new int[] {965, 639}, new int[] {981, 651}, LOTRFAWaypoints.sarnAthrad);
        LOTRFAReflectionHelper.registerRoad("DwarfRoad2", LOTRFAWaypoints.sarnAthrad, new int[] {1022, 640}, LOTRFAWaypoints.nogrodFA);
        LOTRFAReflectionHelper.registerRoad("DwarfRoad3", LOTRFAWaypoints.nogrodFA, LOTRFAWaypoints.belegostFA);
        LOTRFAReflectionHelper.registerRoad("Menegroth", LOTRFAWaypoints.estolad, new int[] {886, 600}, LOTRFAWaypoints.menegroth);
        LOTRFAReflectionHelper.registerRoad("FenSerech", new int[] {761, 499}, new int[] {767, 493}, new int[] {778, 496});
        LOTRFAReflectionHelper.registerRoad("Dorthonion", new int[] {778, 496}, new int[] {783, 501}, new int[] {793, 505});
        LOTRFAReflectionHelper.registerRoad("Dorthonion1", new int[] {793, 505}, new int[] {810, 510}, new int[] {842, 506});
        LOTRFAReflectionHelper.registerRoad("Dorthonion2", new int[] {842, 506}, new int[] {863, 502}, new int[] {870, 496});
        LOTRFAReflectionHelper.registerRoad("Ladros", new int[] {870, 496}, new int[] {875, 488}, LOTRFAWaypoints.ladrosWasteland, new int[] {906, 478});
        LOTRFAReflectionHelper.registerRoad("Lothlann", LOTRFAWaypoints.talathdru, new int[] {974, 496}, new int[] {933, 484}, new int[] {906, 478});
        LOTRFAReflectionHelper.registerRoad("Falas", LOTRFAWaypoints.talathDirnen, new int[] {701, 617}, new int[] {674, 626}, new int[] {651, 632}, new int[] {609, 635}, new int[] {593, 641}, LOTRFAWaypoints.brithombar);
        LOTRFAReflectionHelper.registerRoad("Falas2", new int[] {651, 632}, new int[] {648, 646}, LOTRFAWaypoints.eglarest);

        /*
         * LOTRFAReflectionHelper.registerRoad("DwarfRoadNorth", LOTRFAWaypoints.nogrodFA, LOTRFAWaypoints.belegostFA);
         * LOTRFAReflectionHelper.registerRoad("DwarfRoad", LOTRFAWaypoints.nogrodFA, new int[]{1062, 797}, new int[]{1023, 787}, new int[]{978,794}, LOTRFAWaypoints.estolad, new int[]{812,713}, LOTRFAWaypoints.himlad, new int[]{797,676}, new int[]{777,682}, LOTRFAWaypoints.iantIaur, new int[]{727,688}, new int[]{704,692}, new int[]{686,688}, new int[]{658,688}, new int[]{630,679}, new int[]{609,675}, new int[]{592,667}, new int[]{581,639}, LOTRFAWaypoints.tolSirion);
         * LOTRFAReflectionHelper.registerRoad("FalasRoad", LOTRFAWaypoints.brithombar, new int[] {322,858}, new int[] {368,856}, new int[] {386,842}, LOTRFAWaypoints.eglarest});
         * LOTRFAReflectionHelper.registerRoad("BrethilRoad", new int[] {592,667}, new int[]{LOTRFAWaypoints.amonObel.getX() + 1, LOTRFAWaypoints.amonObel.getY());
         * LOTRFAReflectionHelper.registerRoad("FalasRoadNorth", new int[]{581,639}, new int[]{527,708}, new int[] {386,842});
         * LOTRFAReflectionHelper.registerRoad("ThargelionRoad", LOTRFAWaypoints.himlad, new int[]{927,675}, LOTRFAWaypoints.thargelion);
         * LOTRFAReflectionHelper.registerRoad("SindarRoad", new int[]{777,682}, new int[]{753,709}, new int[]{745,725}, new int[]{739,731}, new int[]{735,732}, new int[]{733,736});
         * LOTRFAReflectionHelper.registerRoad("HimringRoad", new int[]{927,675}, LOTRFAWaypoints.gardhBoren, LOTRFAWaypoints.himring);
         */
    }
}
