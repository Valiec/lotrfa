package eoa.lotrfa.common.world.biome;

import java.util.Random;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangAxeThrower;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangCampfire;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangHillFort;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangWatchtower;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.feature.LOTRWorldGenBlastedLand;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenBurntHouse;
import lotr.common.world.structure2.LOTRWorldGenRuinedHouse;
import lotr.common.world.structure2.LOTRWorldGenStoneRuin;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRBiomeGenThargelion extends LOTRBiomeGenEastBeleriand {

    public LOTRBiomeGenThargelion(int i, boolean major) {
        super(i, major);
        this.clearBiomeVariants();
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.CHARRED, 1000);
        this.addBiomeVariant(LOTRBiomeVariant.SCRUBLAND, 3.0f);
        this.addBiomeVariant(LOTRBiomeVariant.HILLS_SCRUBLAND);
        this.addBiomeVariant(LOTRBiomeVariant.WASTELAND);
        this.addBiomeVariant(LOTRBiomeVariant.MOUNTAIN);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BEECH, 0.2f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BIRCH, 0.2f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LARCH, 0.2f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_PINE, 0.2f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_ASPEN, 0.2f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_MAPLE, 0.2f);
        this.decorator.addTree(LOTRTreeType.OAK, 1000);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 300);
        this.decorator.addTree(LOTRTreeType.SPRUCE, 300);
        this.decorator.addTree(LOTRTreeType.BEECH, 100);
        this.decorator.addTree(LOTRTreeType.BEECH_LARGE, 50);
        this.decorator.addTree(LOTRTreeType.BIRCH, 10);
        this.decorator.addTree(LOTRTreeType.BIRCH_LARGE, 5);
        this.decorator.addTree(LOTRTreeType.ASPEN, 50);
        this.decorator.addTree(LOTRTreeType.ASPEN_LARGE, 10);
        this.decorator.addTree(LOTRTreeType.APPLE, 1);
        this.decorator.addTree(LOTRTreeType.PEAR, 1);
        this.decorator.treesPerChunk = 0;
        this.decorator.willowPerChunk = 1;
        this.decorator.flowersPerChunk = 3;
        this.decorator.grassPerChunk = 10;
        this.decorator.doubleGrassPerChunk = 6;
        this.decorator.whiteSand = false;
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenBlastedLand(false), 30);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangCampfire(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangHillFort(false), 1200);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangWatchtower(false), 500);
        this.decorator.addRandomStructure(new LOTRWorldGenRuinedHouse(false), 500);
        this.decorator.addRandomStructure(new LOTRWorldGenBurntHouse(false), 1000);
        this.decorator.addRandomStructure(new LOTRWorldGenStoneRuin.STONE(1, 4), 1000);
        this.spawnableGoodList.clear();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(500, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityUlfangWarrior.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityUlfangArcher.class, 2, 4, 6), new LOTRSpawnEntry(LOTREntityUlfangAxeThrower.class, 2, 4, 6)}));
        //this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityUlfangBerserker.class, 1, 2, 2));
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseUlfang, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseBor, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.feanorians, LOTREventSpawner.EventChance.COMMON);
        this.setBanditChance(LOTREventSpawner.EventChance.COMMON);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        double d1 = biomeTerrainNoise.func_151601_a(i * 0.08, k * 0.08);
        double d2 = biomeTerrainNoise.func_151601_a(i * 0.7, k * 0.7);
        if (d1 + d2 > 0.1) {
            this.topBlock = Blocks.dirt;
            this.topBlockMeta = 1;
            this.fillerBlock = this.topBlock;
            this.fillerBlockMeta = this.topBlockMeta;
        }
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    public LOTRBiome.GrassBlockAndMeta getRandomGrass(Random random) {
        if (random.nextInt(3) == 0) {
            return new LOTRBiome.GrassBlockAndMeta(Blocks.tallgrass, 1);
        }
        return new LOTRBiome.GrassBlockAndMeta(LOTRMod.tallGrass, 0);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.eastBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterThargelion;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.COBBLESTONE;
    }

}
