package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityNanDungorthebSpider;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenMordor;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.feature.LOTRWorldGenWebOfUngoliant;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRBiomeGenNanDungortheb extends LOTRBiomeGenMordor {

    public static final int WIGHT_SKY = 9674385;
    public static final int WIGHT_CLOUDS = 11842740;
    public static final int WIGHT_FOG = 10197915;

    public LOTRBiomeGenNanDungortheb(int i, boolean major) {
        super(i, major);
        this.topBlock = Blocks.grass;
        this.topBlockMeta = 0;
        this.fillerBlock = Blocks.stone;
        this.fillerBlockMeta = 0;

        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityNanDungorthebSpider.class, 500, 6, 8));
        this.clearBiomeVariants();
        this.biomeColors.setFoggy(true);
        this.biomeColors.setSky(0x343436);
        this.biomeColors.setClouds(0x3c3d42);
        this.biomeColors.setFog(0x56575a);
        this.biomeColors.setWater(0x4d4b50);
        this.biomeColors.setGrass(0x42593e);
        this.biomeColors.setFoliage(0x42593e);
        this.decorator.generateCobwebs = false; // webs of ungoliant are generated separately
        this.decorator.clearRandomStructures();
        // this.decorator.addRandomStructure(new LOTRWorldGenMordorSpiderPit(false),
        // 40);
        this.decorator.clearTrees(); // no trees here other than charred ones
        this.decorator.addTree(LOTRTreeType.CHARRED, 1000);
        this.clearTravellingTraders();
        this.invasionSpawns.clearInvasions();
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    protected void generateMountainTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, int xzIndex, int ySize, int height, int rockDepth, LOTRBiomeVariant variant) {
        for (int j = ySize - 1; j >= 0; --j) {
            int index = xzIndex * ySize + j;
            Block block = blocks[index];
            if (block != Blocks.stone) continue;
            // blocks[index] = Blocks.stone;
            meta[index] = 0;
        }
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNanDungortheb;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.nanDungortheb;
    }

    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.MIRKWOOD.getSubregion("dolGuldur");
    }

    @Override
    public void decorate(World world, Random random, int i, int k) {
        this.decorator.decorate(world, random, i, k);
        for (int l = 0; l < 4; ++l) {
            int i1 = i + random.nextInt(16) + 8;
            int j1 = random.nextInt(128);
            int k1 = k + random.nextInt(16) + 8;
            new LOTRWorldGenWebOfUngoliant(false, 64).generate(world, random, i1, j1, k1);
        }
    }

    @Override
    public float getTreeIncreaseChance() {
        return 0.75f;
    }

    @Override
    public boolean isGorgoroth() {
        return false;
    }

    @Override
    protected boolean hasMordorSoils() {
        return false;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

}
