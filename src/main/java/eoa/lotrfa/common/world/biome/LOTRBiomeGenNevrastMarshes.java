package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenSwanfleet;
import lotr.common.world.map.LOTRWaypoint;

public class LOTRBiomeGenNevrastMarshes extends LOTRBiomeGenSwanfleet {

    public LOTRBiomeGenNevrastMarshes(int i, boolean major) {
        super(i, major);
        this.biomeColors.setGrass(0x29a24b);
        this.spawnableEvilList.clear();
        this.spawnableGoodList.clear();
        this.invasionSpawns.clearInvasions();
        this.decorator.clearRandomStructures();
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.hithlum;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNevrast;
    }

}
