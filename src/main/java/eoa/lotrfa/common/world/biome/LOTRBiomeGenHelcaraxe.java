package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiomeGenForodwaithGlacier;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRWorldGenBoulder;
import lotr.common.world.spawning.LOTREventSpawner;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class LOTRBiomeGenHelcaraxe extends LOTRBiomeGenForodwaithGlacier {

    private WorldGenerator boulderGenSmall;
    private WorldGenerator boulderGenLarge;
    private WorldGenerator clayBoulderGenSmall;
    private WorldGenerator clayBoulderGenLarge;

    public LOTRBiomeGenHelcaraxe(int i, boolean major) {
        super(i, major);
        this.topBlock = Blocks.snow;
        this.fillerBlock = Blocks.ice;
        this.boulderGenSmall = new LOTRWorldGenBoulder(Blocks.snow, 0, 1, 4);
        this.boulderGenLarge = new LOTRWorldGenBoulder(Blocks.snow, 0, 5, 8).setHeightCheck(6);
        this.clayBoulderGenSmall = new LOTRWorldGenBoulder(Blocks.packed_ice, 0, 1, 4);
        this.clayBoulderGenLarge = new LOTRWorldGenBoulder(Blocks.packed_ice, 0, 5, 10).setHeightCheck(6);
        this.biomeColors.setFoggy(true);
        this.biomeColors.setSky(0x40464c);
        this.biomeColors.setFog(0x5e6e78);
        this.biomeColors.setWater(0x283744);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
    }

    @Override
    protected void generateMountainTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, int xzIndex, int ySize, int height, int rockDepth, LOTRBiomeVariant variant) {
        for (int j = 200; j >= 0; --j) {
            int index = xzIndex * ySize + j;
            Block block = blocks[index];
            if (block == Blocks.air || block == Blocks.snow || block == Blocks.ice || block == Blocks.bedrock) continue;
            blocks[index] = Blocks.packed_ice;
            meta[index] = 0;
        }
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterHelcaraxe;
    }

    @Override
    public void decorate(final World world, final Random random, final int i, final int k) {
        super.decorate(world, random, i, k);
        for (int l = 0; l < 20; ++l) {
            final int i2 = i + random.nextInt(16) + 8;
            final int k2 = k + random.nextInt(16) + 8;
            if (random.nextInt(5) == 0) {
                this.clayBoulderGenSmall.generate(world, random, i2, world.getHeightValue(i2, k2), k2);
            }
            else {
                this.boulderGenSmall.generate(world, random, i2, world.getHeightValue(i2, k2), k2);
            }
        }
        for (int l = 0; l < 20; ++l) {
            final int i2 = i + random.nextInt(16) + 8;
            final int k2 = k + random.nextInt(16) + 8;
            if (random.nextInt(5) == 0) {
                this.clayBoulderGenLarge.generate(world, random, i2, world.getHeightValue(i2, k2), k2);
            }
            else {
                this.boulderGenLarge.generate(world, random, i2, world.getHeightValue(i2, k2), k2);
            }
        }
        for (int l = 0; l < 10; ++l) {
            Block block = Blocks.snow;
            if (random.nextInt(5) == 0) {
                block = Blocks.packed_ice;
            }
            for (int l2 = 0; l2 < 10; ++l2) {
                final int i3 = i + random.nextInt(16) + 8;
                final int k3 = k + random.nextInt(16) + 8;
                final int j1 = world.getHeightValue(i3, k3);
                if (world.getBlock(i3, j1 - 1, k3) == block) {
                    for (int height = j1 + random.nextInt(4), j2 = j1; j2 < height; ++j2) {
                        if (LOTRMod.isOpaque(world, i3, j2, k3)) {
                            break;
                        }
                        world.setBlock(i3, j2, k3, block, 0, 3);
                    }
                }
            }
        }
    }

}
