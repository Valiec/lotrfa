package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;

public class LOTRBiomeGenNevrast extends LOTRBiomeGenGenericGrassland {

    public LOTRBiomeGenNevrast(int i, boolean major) {
        super(i, major);
        this.biomeColors.setGrass(0x29a24b);
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.hithlum;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNevrast;
    }

}
