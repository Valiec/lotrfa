package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianElf;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenFeanorianTower;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.world.biome.LOTRBiomeGenTowerHills;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenAmonEreb extends LOTRBiomeGenTowerHills {

    public LOTRBiomeGenAmonEreb(int i, boolean major) {
        super(i, major);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFeanorianElf.class, 10, 4, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFeanorianWarrior.class, 2, 4, 4));
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenFeanorianTower(false), 300);
        this.invasionSpawns.clearInvasions();
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterAmonEreb;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.eastBeleriand;
    }

}
