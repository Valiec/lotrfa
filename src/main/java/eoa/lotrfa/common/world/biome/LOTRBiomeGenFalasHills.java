package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenFalasHills extends LOTRBiomeGenFalas {

    public LOTRBiomeGenFalasHills(int i, boolean major) {
        super(i, major);
        this.decorator.generateOrcDungeon = false;
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(30, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityFalathrimSailor.class, 20, 2, 6), new LOTRSpawnEntry(LOTREntityFalathrimArcher.class, 10, 2, 6), new LOTRSpawnEntry(LOTREntityFalathrimMariner.class, 5, 2, 4)}));
        this.decorator.clearRandomStructures();
        this.decorator.clearVillages();
        this.clearBiomeVariants();
        this.addBiomeVariantSet(LOTRBiomeVariant.SET_MOUNTAINS);
        this.decorator.grassPerChunk = 5;
        this.decorator.doubleGrassPerChunk = 1;
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimMerchantStall(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimMerchantStall(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimSmithy(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimHouse(false), 100);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimTurret(false), 600);
        this.invasionSpawns.clearInvasions();
        this.clearTravellingTraders();
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

}
