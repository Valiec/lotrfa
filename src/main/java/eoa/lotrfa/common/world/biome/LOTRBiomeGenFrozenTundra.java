package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.entity.npc.LOTREntityDorDaidelosOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaidelosOrcArcher;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenDorDaidelosCamp;
import lotr.common.world.biome.LOTRBiomeGenTundra;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.init.Blocks;

public class LOTRBiomeGenFrozenTundra extends LOTRBiomeGenTundra {

    public LOTRBiomeGenFrozenTundra(int i, boolean major) {
        super(i, major);
        this.topBlock = Blocks.snow;
        this.biomeColors.setSky(0x69938b);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(200, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDorDaidelosOrc.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityDorDaidelosOrcArcher.class, 5, 4, 6)}));
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenDorDaidelosCamp(false), 300);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.dorDaidelos, LOTREventSpawner.EventChance.COMMON);
    }

}
