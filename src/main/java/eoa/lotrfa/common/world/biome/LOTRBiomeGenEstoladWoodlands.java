package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.world.structure.village.LOTRVillageGenEstolad;
import lotr.common.world.biome.LOTRBiomeGenRohanWoodlands;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRRoadType;

public class LOTRBiomeGenEstoladWoodlands extends LOTRBiomeGenRohanWoodlands {

    public LOTRBiomeGenEstoladWoodlands(int i, boolean major) {
        super(i, major);
        this.spawnableEvilList.clear();
        this.invasionSpawns.clearInvasions();
        this.decorator.generateOrcDungeon = false;
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.HOLLY_LARGE, 1000);
        this.decorator.addTree(LOTRTreeType.HOLLY, 100);
        this.decorator.addTree(LOTRTreeType.GREEN_OAK_LARGE, 50);
        this.decorator.addTree(LOTRTreeType.RED_OAK, 60);
        this.decorator.addTree(LOTRTreeType.RED_OAK_LARGE, 30);
        this.decorator.addTree(LOTRTreeType.OAK, 50);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.MIRK_OAK, 50);
        this.decorator.addTree(LOTRTreeType.SPRUCE, 10);
        this.decorator.addTree(LOTRTreeType.SPRUCE_THIN, 5);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA, 2);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA_THIN, 2);
        this.decorator.addTree(LOTRTreeType.CHESTNUT, 2);
        this.decorator.addTree(LOTRTreeType.CHESTNUT_LARGE, 5);
        this.decorator.addTree(LOTRTreeType.LARCH, 20);
        this.decorator.addTree(LOTRTreeType.FIR, 20);
        this.decorator.addTree(LOTRTreeType.PINE, 40);
        this.decorator.addTree(LOTRTreeType.ASPEN, 50);
        this.decorator.addTree(LOTRTreeType.ASPEN_LARGE, 10);
        this.decorator.clearVillages();
        this.decorator.addVillage(new LOTRVillageGenEstolad(this, 0.6f));
        // TODO Auto-generated constructor stub
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

}
