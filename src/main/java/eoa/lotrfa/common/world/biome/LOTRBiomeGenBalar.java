package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenOcean;
import lotr.common.world.map.LOTRWaypoint;

public class LOTRBiomeGenBalar extends LOTRBiomeGenOcean {

    public LOTRBiomeGenBalar(int i, boolean major) {
        super(i, major);
        this.decorator.clearRandomStructures();
        this.clearTravellingTraders();
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRWaypoint.Region.OCEAN;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterBalar;
    }
}
