package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.LOTRAchievement;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.world.World;

public class LOTRBiomeGenAmonRudh extends LOTRBiomeGenTalathDirnen {

    public LOTRBiomeGenAmonRudh(int i, boolean major) {
        super(i, major);
        this.decorator.flowersPerChunk = 16;
        this.registerMountainsFlowers();
        this.addFlower(LOTRFABlocks.seregon, 0, 100);
        this.addFlower(LOTRFABlocks.aeglos, 0, 20);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityPettyDwarf.class, 3, 1, 1));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityPettyDwarfMiner.class, 2, 1, 1));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityPettyDwarfWarrior.class, 2, 1, 1));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityPettyDwarfAxeThrower.class, 1, 1, 1));
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenPettyDwarfSmithy(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenPettyDwarfCamp(false), 1500);
        this.clearBiomeVariants();
        this.setBanditChance(LOTREventSpawner.EventChance.UNCOMMON);
    }

    @Override
    public void decorate(World world, Random random, int i, int k) {
        int i1;
        int l;
        super.decorate(world, random, i, k);
        for (l = 0; l < 4; ++l) {
            i1 = i + random.nextInt(16) + 8;
            int j1 = 70 + random.nextInt(60);
            int k1 = k + random.nextInt(16) + 8;
            new LOTRWorldGenPettyDwarfHouse(false, false).generate(world, random, i1, j1, k1);
        }
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.amonRudh;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterAmonRudh;
    }

}
