package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.LOTREntityNanDungorthebSpider;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiomeGenMirkwoodMountains;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRWorldGenBoulder;
import lotr.common.world.feature.LOTRWorldGenGrassPatch;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class LOTRBiomeGenEredGorgoroth extends LOTRBiomeGenMirkwoodMountains {

    private WorldGenerator boulderGenSmall;
    private WorldGenerator boulderGenLarge;
    private WorldGenerator clayBoulderGenSmall;
    private WorldGenerator clayBoulderGenLarge;
    private WorldGenerator grassPatchGen;
    public static final int WIGHT_SKY = 9674385;
    public static final int WIGHT_CLOUDS = 11842740;
    public static final int WIGHT_FOG = 10197915;

    public LOTRBiomeGenEredGorgoroth(int i, boolean major) {
        super(i, major);
        this.boulderGenSmall = new LOTRWorldGenBoulder(Blocks.stone, 0, 1, 4);
        this.boulderGenLarge = new LOTRWorldGenBoulder(Blocks.stone, 0, 5, 8).setHeightCheck(6);
        this.clayBoulderGenSmall = new LOTRWorldGenBoulder(LOTRFABlocks.graniteRough, 0, 1, 4);
        this.clayBoulderGenLarge = new LOTRWorldGenBoulder(LOTRFABlocks.graniteRough, 0, 5, 10).setHeightCheck(6);
        this.grassPatchGen = new LOTRWorldGenGrassPatch();
        this.topBlock = Blocks.stone;
        this.topBlockMeta = 0;
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityNanDungorthebSpider.class, 500, 6, 8));
        this.biomeColors.setFoggy(true);
        this.clearBiomeVariants();
        this.addBiomeVariantSet(LOTRBiomeVariant.SET_MOUNTAINS);
        this.decorator.clearRandomStructures();
        this.decorator.clearTrees();
        this.biomeColors.setSky(0x343436);
        this.biomeColors.setClouds(0x3c3d42);
        this.biomeColors.setFog(0x56575a);
        this.biomeColors.setWater(0x4d4b50);
        this.biomeColors.setGrass(0x42593e);
        this.biomeColors.setFoliage(0x42593e);
        this.clearTravellingTraders();
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.invasionSpawns.clearInvasions();
        this.spawnableCreatureList.clear();
        this.spawnableLOTRAmbientList.clear();
    }

    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.MIRKWOOD.getSubregion("dolGuldur");
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterEredGorgoroth;
    }

    @Override
    public void decorate(final World world, final Random random, final int i, final int k) {
        super.decorate(world, random, i, k);
        for (int l = 0; l < 20; ++l) {
            final int i2 = i + random.nextInt(16) + 8;
            final int k2 = k + random.nextInt(16) + 8;
            if (random.nextInt(5) == 0) {
                this.clayBoulderGenSmall.generate(world, random, i2, world.getHeightValue(i2, k2), k2);
            }
            else {
                this.boulderGenSmall.generate(world, random, i2, world.getHeightValue(i2, k2), k2);
            }
        }
        for (int l = 0; l < 20; ++l) {
            final int i2 = i + random.nextInt(16) + 8;
            final int k2 = k + random.nextInt(16) + 8;
            if (random.nextInt(5) == 0) {
                this.clayBoulderGenLarge.generate(world, random, i2, world.getHeightValue(i2, k2), k2);
            }
            else {
                this.boulderGenLarge.generate(world, random, i2, world.getHeightValue(i2, k2), k2);
            }
        }
        for (int l = 0; l < 10; ++l) {
            Block block = Blocks.stone;
            if (random.nextInt(5) == 0) {
                block = LOTRFABlocks.graniteRough;
            }
            for (int l2 = 0; l2 < 10; ++l2) {
                final int i3 = i + random.nextInt(16) + 8;
                final int k3 = k + random.nextInt(16) + 8;
                final int j1 = world.getHeightValue(i3, k3);
                if (world.getBlock(i3, j1 - 1, k3) == block) {
                    for (int height = j1 + random.nextInt(4), j2 = j1; j2 < height; ++j2) {
                        if (LOTRMod.isOpaque(world, i3, j2, k3)) {
                            break;
                        }
                        world.setBlock(i3, j2, k3, block, 0, 3);
                    }
                }
            }
        }
        for (int l = 0; l < 3; ++l) {
            final int i2 = i + random.nextInt(16) + 8;
            final int k2 = k + random.nextInt(16) + 8;
            this.grassPatchGen.generate(world, random, i2, world.getHeightValue(i2, k2), k2);
        }
    }

    @Override
    public boolean canSpawnHostilesInDay() {
        return true;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.nanDungortheb;
    }

}
