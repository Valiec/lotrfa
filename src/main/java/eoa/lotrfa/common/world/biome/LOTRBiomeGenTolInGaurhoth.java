package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.animal.LOTREntityBlackSwan;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothOrcArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothWarg;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothWraith;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothAltar;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothCamp;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothTower;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothWargPit;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.entity.animal.LOTREntityGorcrow;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenSmallStoneRuin;
import lotr.common.world.structure2.LOTRWorldGenStoneRuin;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class LOTRBiomeGenTolInGaurhoth extends LOTRBiomeGenPassSirion {

    public LOTRBiomeGenTolInGaurhoth(int i, boolean major) {
        super(i, major);
        this.spawnableCreatureList.clear();
        this.spawnableWaterCreatureList.clear();
        this.spawnableLOTRAmbientList.add(new BiomeGenBase.SpawnListEntry(LOTREntityGorcrow.class, 8, 4, 4));
        this.spawnableLOTRAmbientList.add(new SpawnListEntry(LOTREntityBlackSwan.class, 20, 4, 8));
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothWarg.class, 100, 4, 10));
        //this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothTroll.class, 5, 1, 3));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothWraith.class, 10, 1, 3));
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreMorgulIron, 8), 20.0f, 0, 64);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreGulduril, 8), 8.0f, 0, 32);
        this.decorator.treesPerChunk = 0;
        this.decorator.grassPerChunk = 3;
        this.decorator.dryReedChance = 1.0f;
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.OAK, 200);
        this.decorator.addTree(LOTRTreeType.OAK_DESERT, 500);
        this.decorator.addTree(LOTRTreeType.OAK_DEAD, 500);
        this.decorator.addTree(LOTRTreeType.CHARRED, 500);
        this.flowers.clear();
        this.clearTravellingTraders();
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.biomeColors.setFoggy(true);
        this.biomeColors.setGrass(6054733);
        this.biomeColors.setFoliage(4475954);
        this.biomeColors.setSky(0x5b6d67);
        this.biomeColors.setClouds(5860197);
        this.biomeColors.setFog(0x2f433a);
        this.biomeColors.setWater(0x30453a);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenStoneRuin.DOL_GULDUR(1, 4), 5);
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothAltar(false), 70);
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothTower(false), 60);
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothCamp(false), 50);
        this.decorator.addRandomStructure(new LOTRWorldGenSmallStoneRuin(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothWargPit(false), 30);
        this.invasionSpawns.clearInvasions();
    }

    @Override
    public boolean canSpawnHostilesInDay() {
        return true;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.passSirion;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterTolInGaurhoth;
    }

}
