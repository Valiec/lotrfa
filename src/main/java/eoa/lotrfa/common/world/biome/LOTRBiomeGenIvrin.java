package eoa.lotrfa.common.world.biome;

import java.util.Random;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityHithlumElf;
import eoa.lotrfa.common.entity.npc.LOTREntityNargothrondElf;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRBiomeGenIvrin extends LOTRBiomeGenWestBeleriand {

    public LOTRBiomeGenIvrin(int i, boolean major) {
        super(i, major);
        this.biomeColors.setGrass(9751852);
        this.biomeColors.setWater(0x0080f0);
        this.decorator.flowersPerChunk = 16;
        this.decorator.doubleFlowersPerChunk = 3;
        this.decorator.grassPerChunk = 12;
        this.decorator.doubleGrassPerChunk = 3;
        this.addBiomeVariantSet(LOTRBiomeVariant.SET_FOREST);
        this.decorator.treesPerChunk = 10;
        this.decorator.addTree(LOTRTreeType.PINE, 1000);
        this.decorator.addTree(LOTRTreeType.FIR, 200);
        this.decorator.addTree(LOTRTreeType.SPRUCE, 200);
        this.registerForestFlowers();
        this.spawnableEvilList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityNargothrondElf.class, 1, 1, 2));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityHithlumElf.class, 5, 1, 2));
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        double d1 = biomeTerrainNoise.func_151601_a(i * 0.08, k * 0.08);
        double d2 = biomeTerrainNoise.func_151601_a(i * 0.7, k * 0.7);
        if (d1 + d2 > 0.1) {
            this.topBlock = Blocks.stone;
            this.topBlockMeta = 1;
            this.fillerBlock = this.topBlock;
            this.fillerBlockMeta = this.topBlockMeta;
        }
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.westBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterIvrin;
    }

}
