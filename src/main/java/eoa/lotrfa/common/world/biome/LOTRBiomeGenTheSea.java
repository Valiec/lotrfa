package eoa.lotrfa.common.world.biome;

import lotr.common.world.biome.LOTRBiomeGenOcean;

public class LOTRBiomeGenTheSea extends LOTRBiomeGenOcean {

    public LOTRBiomeGenTheSea(int i, boolean major) {
        super(i, major);
        this.spawnableWaterCreatureList.clear();
        this.spawnableLOTRAmbientList.clear();
    }

}
