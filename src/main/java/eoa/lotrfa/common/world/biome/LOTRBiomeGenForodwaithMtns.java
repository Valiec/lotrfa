package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaidelosOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaidelosOrcArcher;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiomeGenForodwaithMountains;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class LOTRBiomeGenForodwaithMtns extends LOTRBiomeGenForodwaithMountains {

    public LOTRBiomeGenForodwaithMtns(int i, boolean major) {
        super(i, major);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreMorgulIron, 8), 20.0f, 0, 64);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreGulduril, 8), 8.0f, 0, 32);
        this.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(300, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDorDaidelosOrc.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityDorDaidelosOrcArcher.class, 5, 4, 6)}));
        this.invasionSpawns.addInvasion(LOTRFAInvasions.dorDaidelos, LOTREventSpawner.EventChance.UNCOMMON);
    }

    @Override
    protected void generateMountainTerrain(final World world, final Random random, final Block[] blocks, final byte[] meta, final int i, final int k, final int xzIndex, final int ySize, final int height, final int rockDepth, final LOTRBiomeVariant variant) {
    }

}
