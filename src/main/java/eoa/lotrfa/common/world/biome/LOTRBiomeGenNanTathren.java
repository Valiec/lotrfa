package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.entity.animal.LOTREntitySwan;
import lotr.common.entity.npc.LOTREntityEnt;
import lotr.common.entity.npc.LOTREntityHuorn;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.world.biome.BiomeGenBase;

public class LOTRBiomeGenNanTathren extends LOTRBiomeGenWestBeleriand {

    public LOTRBiomeGenNanTathren(int i, boolean major) {
        super(i, major);
        this.setGoodEvilWeight(100, 0);
        this.decorator.addTree(LOTRTreeType.WILLOW, 400);
        this.decorator.addTree(LOTRTreeType.WILLOW_WATER, 400);
        this.addBiomeVariant(LOTRBiomeVariant.FLOWERS);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST);
        this.decorator.flowersPerChunk = 16;
        this.decorator.doubleFlowersPerChunk = 3;
        this.decorator.grassPerChunk = 12;
        this.decorator.doubleGrassPerChunk = 3;
        this.decorator.willowPerChunk = 10;
        this.decorator.treesPerChunk = 10;
        this.registerPlainsFlowers();
        this.spawnableLOTRAmbientList.add(new BiomeGenBase.SpawnListEntry(LOTREntitySwan.class, 20, 4, 8));
        this.spawnableEvilList.clear();
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityEnt.class, 10, 4, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityHuorn.class, 20, 4, 4));
        this.invasionSpawns.clearInvasions();
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);

    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.westBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNanTathren;
    }

}
