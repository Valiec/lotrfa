package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.LOTRAchievement;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenBaradNimras extends LOTRBiomeGenFalas {

    public LOTRBiomeGenBaradNimras(int i, boolean major) {
        super(i, major);
        this.clearTravellingTraders();
        this.invasionSpawns.clearInvasions();
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(30, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityFalathrimSailor.class, 20, 2, 6), new LOTRSpawnEntry(LOTREntityFalathrimArcher.class, 10, 2, 6), new LOTRSpawnEntry(LOTREntityFalathrimMariner.class, 5, 2, 4)}));
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimMerchantStall(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimMerchantStall(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimSmithy(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimHouse(false), 100);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimTurret(false), 600);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.falas;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterBaradNimras;
    }
}
