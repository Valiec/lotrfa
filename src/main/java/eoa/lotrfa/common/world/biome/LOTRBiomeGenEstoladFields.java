package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.world.structure.village.LOTRVillageGenEstolad;
import lotr.common.world.biome.LOTRBiomeGenWold;
import lotr.common.world.map.LOTRRoadType;

public class LOTRBiomeGenEstoladFields extends LOTRBiomeGenWold {

    public LOTRBiomeGenEstoladFields(int i, boolean major) {
        super(i, major);
        this.spawnableEvilList.clear();
        this.invasionSpawns.clearInvasions();
        this.decorator.generateOrcDungeon = false;
        this.decorator.clearVillages();
        this.decorator.addVillage(new LOTRVillageGenEstolad(this, 0.6f));
        // TODO Auto-generated constructor stub
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

}
