package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenDoriathMerchantStall;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiomeGenLothlorien;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;

public class LOTRBiomeGenSindarinRealmNeldoreth extends LOTRBiomeGenLothlorien {
    public LOTRBiomeGenSindarinRealmNeldoreth(int i, boolean major) {
        super(i, major);
        this.flowers.clear();
        this.registerForestFlowers();
        this.biomeColors.resetGrass();
        this.biomeColors.resetFog();
        this.spawnableEvilList.clear();
        this.setGoodEvilWeight(100, 0);
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.MALLORN_PARTY, 50);
        this.decorator.addTree(LOTRTreeType.MALLORN_EXTREME, 30);
        this.decorator.addTree(LOTRTreeType.BEECH, 500);
        this.decorator.addTree(LOTRTreeType.BEECH_PARTY, 320);
        this.decorator.addTree(LOTRTreeType.BEECH_LARGE, 50);
        this.decorator.addTree(LOTRTreeType.GREEN_OAK_LARGE, 30);
        this.decorator.addTree(LOTRTreeType.RED_OAK, 40);
        this.decorator.addTree(LOTRTreeType.RED_OAK_LARGE, 20);
        this.decorator.addTree(LOTRTreeType.OAK, 50);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.SPRUCE, 100);
        this.decorator.addTree(LOTRTreeType.CHESTNUT, 50);
        this.decorator.addTree(LOTRTreeType.CHESTNUT_LARGE, 50);
        this.decorator.addTree(LOTRTreeType.BEECH, 50);
        this.decorator.addTree(LOTRTreeType.BEECH_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.LARCH, 50);
        this.decorator.addTree(LOTRTreeType.FIR, 80);
        this.decorator.addTree(LOTRTreeType.PINE, 100);
        this.decorator.addTree(LOTRTreeType.ASPEN, 50);
        this.decorator.addTree(LOTRTreeType.ASPEN_LARGE, 10);
        this.invasionSpawns.clearInvasions();
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.decorator.addRandomStructure(new LOTRWorldGenDoriathMerchantStall(false, 0), 120);
        this.decorator.addRandomStructure(new LOTRWorldGenDoriathMerchantStall(false, 1), 120);
        this.decorator.addRandomStructure(new LOTRWorldGenDoriathMerchantStall(false, 2), 120);
        this.addFlower(LOTRMod.elanor, 0, 20);
        this.addFlower(LOTRMod.niphredil, 0, 14);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.doriath;
    }

}
