package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class LOTRBiomeGenEredEngrin extends LOTRBiome {
	
    public LOTRBiomeGenEredEngrin(int i, boolean major) {
    	super(i, major);
        this.setEnableSnow();
        this.topBlock = LOTRFABlocks.rockSarllith;
        this.topBlockMeta = 0;
        this.fillerBlock = Blocks.stone;
        this.fillerBlockMeta = 0;
    	this.addBiomeVariantSet(LOTRBiomeVariant.SET_MOUNTAINS);
    	this.decorator.biomeOreFactor = 2.0f;
    	this.decorator.biomeGemFactor = 1.5f;
    	this.decorator.treesPerChunk = 0;
    	this.decorator.flowersPerChunk = 0;
    	this.decorator.grassPerChunk = 0;
    	this.decorator.doubleGrassPerChunk = 0;
    	this.decorator.addOre(new WorldGenMinable(LOTRMod.oreMorgulIron, 8), 20.0f, 0, 64);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreGulduril, 8), 8.0f, 0, 32);
    	this.biomeColors.setGrass(0x485b20);
    	this.biomeColors.setFoliage(0x506325);
    	this.biomeColors.setSky(0x7c5561);
    	this.biomeColors.setClouds(0x785555);
    	this.biomeColors.setFog(0x877d57);
    	this.biomeColors.setWater(0x304844);
    	this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcBombardier.class, 5, 1, 2));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothWarg.class, 30, 4, 4));
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();

    
    }
    @Override
    public boolean getEnableRiver() {
    	return false;
    }

    @Override
    protected void generateMountainTerrain(final World world, final Random random, final Block[] blocks, final byte[] meta, final int i, final int k, final int xzIndex, final int ySize, final int height, final int rockDepth, final LOTRBiomeVariant variant) {
        final int snowHeight = 100 - rockDepth;
        for (int stoneHeight = snowHeight - 30, j = ySize - 1; j >= stoneHeight; --j) {
            final int index = xzIndex * ySize + j;
            final Block block = blocks[index];
            if (j >= snowHeight && block == this.topBlock) {
                blocks[index] = Blocks.snow;
                meta[index] = 0;
            }
            else if (block == this.topBlock || block == this.fillerBlock) {
                blocks[index] = LOTRFABlocks.rockSarllith;
                meta[index] = 0;
            }
        }
    }

    @Override
    public float getChanceToSpawnAnimals() {
    	return 0.2f;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterEredEngrin;
    }
    
    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.ardGalen;
    }
    
    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.MORDOR.getSubregion("mordor");
    }

}

