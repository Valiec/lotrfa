package eoa.lotrfa.common.world.biome;

import java.util.Random;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import net.minecraft.block.Block;
import net.minecraft.world.World;

public class LOTRBiomeGenEredWethrinFoothills extends LOTRBiomeGenEredWethrin {

    public LOTRBiomeGenEredWethrinFoothills(int i, boolean major, int achieve) {
        super(i, major, achieve);
        this.clearBiomeVariants();
        this.spawnableEvilList.clear();
    }

    @Override
    protected void generateMountainTerrain(final World world, final Random random, final Block[] blocks, final byte[] meta, final int i, final int k, final int xzIndex, final int ySize, final int height, final int rockDepth, final LOTRBiomeVariant variant) {
    }

}
