package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityDoriathElf;
import eoa.lotrfa.common.entity.npc.LOTREntityDoriathWarden;
import eoa.lotrfa.common.entity.npc.LOTREntityDoriathWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.LOTRBiomeGenMirkwoodNorth;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenNanElmoth extends LOTRBiomeGenMirkwoodNorth {

    public LOTRBiomeGenNanElmoth(int i, boolean major) {
        super(i, major);
        this.spawnableEvilList.clear();
        this.invasionSpawns.clearInvasions();
        this.decorator.addTree(LOTRTreeType.GREEN_OAK_LARGE, 800);
        this.decorator.addTree(LOTRTreeType.RED_OAK_LARGE, 800);
        this.biomeColors.setFoggy(true);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(100, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDoriathElf.class, 5, 1, 3), new LOTRSpawnEntry(LOTREntityDoriathWarrior.class, 1, 1, 2), new LOTRSpawnEntry(LOTREntityDoriathWarden.class, 20, 1, 3)}));
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
        this.invasionSpawns.clearInvasions();
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNanElmoth;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.nanElmoth;
    }

}
