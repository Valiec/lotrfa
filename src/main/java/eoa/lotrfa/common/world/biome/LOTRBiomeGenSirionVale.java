package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityBrethilSoldier;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothOrcArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothWarg;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothCamp;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.LOTRBiomeGenAnduinVale;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenStoneRuin;

public class LOTRBiomeGenSirionVale extends LOTRBiomeGenAnduinVale {

    public LOTRBiomeGenSirionVale(int i, boolean major) {
        super(i, major);
        this.biomeColors.setGrass(0x3f6825);
        this.setBanditChance(LOTREventSpawner.EventChance.UNCOMMON);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(3000, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityBrethilSoldier.class, 10, 1, 3)}));
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothWarg.class, 10, 4, 10));
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothCamp(false), 1500);
        this.decorator.addRandomStructure(new LOTRWorldGenStoneRuin.STONE(1, 3), 400);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhoth, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhothWarg, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.hithlum, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.dorLomin, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.nargothrond, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.brethil, LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.westBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterValeSirion;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.COBBLESTONE;
    }

}
