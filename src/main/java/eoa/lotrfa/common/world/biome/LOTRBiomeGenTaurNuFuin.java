package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothCamp;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothTower;
import lotr.common.LOTRAchievement;
import lotr.common.entity.animal.LOTREntityButterfly;
import lotr.common.entity.animal.LOTREntityGorcrow;
import lotr.common.world.biome.LOTRBiomeGenMirkwood;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenTaurNuFuin extends LOTRBiomeGenMirkwood {

    public LOTRBiomeGenTaurNuFuin(int i, boolean major) {
        super(i, major);
        this.spawnableWaterCreatureList.clear();
        this.spawnableLOTRAmbientList.clear();
        this.spawnableLOTRAmbientList.add(new SpawnListEntry(LOTREntityButterfly.class, 10, 4, 4));
        this.spawnableLOTRAmbientList.add(new SpawnListEntry(LOTREntityGorcrow.class, 6, 4, 4));
        this.variantChance = 0.2F;
        this.decorator.treesPerChunk = 8;
        this.decorator.willowPerChunk = 1;
        this.decorator.vinesPerChunk = 20;
        this.decorator.logsPerChunk = 3;
        this.decorator.flowersPerChunk = 0;
        this.decorator.grassPerChunk = 12;
        this.decorator.doubleGrassPerChunk = 6;
        this.decorator.enableFern = true;
        this.decorator.mushroomsPerChunk = 4;
        this.decorator.generateCobwebs = false;
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearBiomeVariants();
        this.addBiomeVariantSet(LOTRBiomeVariant.SET_FOREST);
        this.addBiomeVariant(LOTRBiomeVariant.HILLS_FOREST);
        this.biomeColors.setGrass(0x425f3f);
        this.biomeColors.setFoliage(0x303a2f);
        this.biomeColors.setFog(3302525);
        this.biomeColors.setFoggy(true);
        this.biomeColors.setWater(0x2e2739);
        this.setGoodEvilWeight(70, 30);
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.PINE, 150);
        this.decorator.addTree(LOTRTreeType.SHIRE_PINE, 700);
        this.decorator.addTree(LOTRTreeType.SPRUCE, 125);
        this.decorator.addTree(LOTRTreeType.SPRUCE_THIN, 100);
        this.decorator.addTree(LOTRTreeType.FIR, 150);
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothWraith.class, 10, 1, 3));
        this.clearTravellingTraders();
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothCamp(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothTower(false), 400);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhoth, LOTREventSpawner.EventChance.COMMON);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.taurNuFuin;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterTaurNuFuin;
    }

    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.MIRKWOOD.getSubregion("mirkwood");
    }

    @Override
    public float getChanceToSpawnAnimals() {
        return 0.1F;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.COBBLESTONE;
    }

}
