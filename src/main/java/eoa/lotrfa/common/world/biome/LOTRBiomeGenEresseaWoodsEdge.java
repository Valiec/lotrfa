package eoa.lotrfa.common.world.biome;

import lotr.common.entity.npc.LOTREntityGaladhrimTrader;
import lotr.common.world.biome.LOTRBiomeGenLothlorienEdge;

public class LOTRBiomeGenEresseaWoodsEdge extends LOTRBiomeGenLothlorienEdge {

    public LOTRBiomeGenEresseaWoodsEdge(int i, boolean major) {
        super(i, major);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityGaladhrimTrader.class);
        // TODO Auto-generated constructor stub
    }

}
