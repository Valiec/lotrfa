package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityFalathrimSailor;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenErynVorn;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenNimbrethil extends LOTRBiomeGenErynVorn {

    public LOTRBiomeGenNimbrethil(int i, boolean major) {
        super(i, major);
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.BIRCH, 800); //LAIRELOSSE
        this.decorator.addTree(LOTRTreeType.BIRCH_LARGE, 400); //LAIRELOSSE_LARGE
        this.decorator.addTree(LOTRTreeType.BIRCH_PARTY, 200);
        this.decorator.addTree(LOTRTreeType.BIRCH, 200);
        this.decorator.addTree(LOTRTreeType.OAK, 200);
        this.decorator.clearRandomStructures();
        this.spawnableEvilList.clear();
        this.decorator.generateOrcDungeon = false;
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFalathrimSailor.class, 6, 1, 3));
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
        this.clearTravellingTraders();
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.westBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNimbrethil;
    }

}
