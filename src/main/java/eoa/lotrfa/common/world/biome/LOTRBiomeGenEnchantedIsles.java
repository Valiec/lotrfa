package eoa.lotrfa.common.world.biome;

import lotr.common.world.biome.LOTRBiomeGenMirkwoodNorth;

public class LOTRBiomeGenEnchantedIsles extends LOTRBiomeGenMirkwoodNorth {

    public LOTRBiomeGenEnchantedIsles(int i, boolean major) {
        super(i, major);
        this.spawnableGoodList.clear();
        this.spawnableEvilList.clear();
        this.clearTravellingTraders();
        this.decorator.clearRandomStructures();
    }

}
