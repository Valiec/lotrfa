package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.world.biome.LOTRBiomeGenMirkwoodNorth;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenOssiriandWoodlands extends LOTRBiomeGenMirkwoodNorth {

    public LOTRBiomeGenOssiriandWoodlands(int i, boolean major) {
        super(i, major);
        this.spawnableEvilList.clear();
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityLaegrimElf.class, 10, 4, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityLaegrimScout.class, 2, 4, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityLaegrimWarrior.class, 1, 4, 4));
        this.biomeColors.setGrass(0x507a32);
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.GREEN_OAK, 115);
        this.decorator.addTree(LOTRTreeType.GREEN_OAK_LARGE, 10);
        this.decorator.addTree(LOTRTreeType.RED_OAK, 150);
        this.decorator.addTree(LOTRTreeType.RED_OAK_LARGE, 50);
        this.decorator.addTree(LOTRTreeType.OAK, 50);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.MIRK_OAK, 50);
        this.decorator.addTree(LOTRTreeType.BEECH_LARGE, 100); //MALLORN_PARTY
        this.decorator.addTree(LOTRTreeType.CHESTNUT, 20);
        this.decorator.addTree(LOTRTreeType.CHESTNUT_LARGE, 50);
        this.decorator.addTree(LOTRTreeType.BEECH, 200);
        this.decorator.addTree(LOTRTreeType.BEECH, 600); //MALLORN
        this.decorator.addTree(LOTRTreeType.FIR, 200);
        this.decorator.addTree(LOTRTreeType.PINE, 200);
        this.decorator.addTree(LOTRTreeType.BEECH_PARTY, 100); //MALLORN_BOUGHA
        this.decorator.addTree(LOTRTreeType.ASPEN, 500);
        this.decorator.addTree(LOTRTreeType.ASPEN_LARGE, 100);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenLaegrimHouse(false), 16);
        this.decorator.addRandomStructure(new LOTRWorldGenLaegrimHideout(false), 40);
        this.decorator.addRandomStructure(new LOTRWorldGenLaegrimForge(false), 80);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.laegrim, LOTREventSpawner.EventChance.UNCOMMON);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterOssiriand;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.ossiriand;
    }

}
