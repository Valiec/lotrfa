package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;

public class LOTRBiomeGenAndram extends LOTRBiomeGenGenericMountains{

	public LOTRBiomeGenAndram(int i, boolean major, int achieve) {
		super(i, major, achieve);
		this.setBanditChance(LOTREventSpawner.EventChance.RARE);
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
	}
	
    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.eastBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterAndram;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.DORWINION;
    }

}
