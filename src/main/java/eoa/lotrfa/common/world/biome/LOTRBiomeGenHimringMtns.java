package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianElf;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenFeanorianHillTower;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class LOTRBiomeGenHimringMtns extends LOTRBiomeGenGenericMountains {

    public LOTRBiomeGenHimringMtns(int i, boolean major) {
        super(i, major, 4);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenFeanorianHillTower(false), 300);
        this.decorator.clearOres();
        this.setGoodEvilWeight(90, 10);
        this.decorator.addSoil(new WorldGenMinable(Blocks.dirt, 32), 40.0f, 0, 256);
        this.decorator.addSoil(new WorldGenMinable(Blocks.gravel, 32), 20.0f, 0, 256);
        this.decorator.addOre(new WorldGenMinable(Blocks.coal_ore, 16), 40.0f, 0, 128);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreCopper, 8), 16.0f, 0, 128);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreTin, 8), 16.0f, 0, 128);
        this.decorator.addOre(new WorldGenMinable(Blocks.iron_ore, 8), 20.0f, 0, 64);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreSulfur, 8), 2.0f, 0, 64);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreSaltpeter, 8), 2.0f, 0, 64);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreSalt, 12), 2.0f, 0, 64);
        this.decorator.addOre(new WorldGenMinable(Blocks.gold_ore, 8), 2.0f, 0, 32);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreSilver, 8), 3.0f, 0, 32);
        this.decorator.addGem(new WorldGenMinable(LOTRMod.oreGem, 1, 6, Blocks.stone), 2.0f, 0, 64);
        this.decorator.addGem(new WorldGenMinable(LOTRMod.oreGem, 0, 6, Blocks.stone), 2.0f, 0, 64);
        this.decorator.addGem(new WorldGenMinable(LOTRMod.oreGem, 4, 5, Blocks.stone), 1.5f, 0, 48);
        this.decorator.addGem(new WorldGenMinable(LOTRMod.oreGem, 6, 5, Blocks.stone), 1.5f, 0, 48);
        this.decorator.addGem(new WorldGenMinable(LOTRMod.oreGem, 2, 4, Blocks.stone), 1.0f, 0, 32);
        this.decorator.addGem(new WorldGenMinable(LOTRMod.oreGem, 3, 4, Blocks.stone), 1.0f, 0, 32);
        this.decorator.addGem(new WorldGenMinable(LOTRMod.oreGem, 7, 4, Blocks.stone), 0.75f, 0, 24);
        this.decorator.addGem(new WorldGenMinable(LOTRMod.oreGem, 5, 4, Blocks.stone), 0.5f, 0, 16); // I need to do all
                                                                                                     // of these bc
                                                                                                     // there's no
                                                                                                     // removeSoil
                                                                                                     // method
        this.decorator.addSoil(new WorldGenMinable(LOTRMod.rock, 5, 60, Blocks.stone), 2.0f, 0, 64);
        this.spawnableGoodList.clear();
        this.spawnableEvilList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFeanorianElf.class, 10, 4, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFeanorianWarrior.class, 2, 4, 4));
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterHimring;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.feanor;
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

    @Override
    protected void generateMountainTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, int xzIndex, int ySize, int height, int rockDepth, LOTRBiomeVariant variant) {
        int stoneHeight = 100 - rockDepth;
        double d1 = biomeTerrainNoise.func_151601_a(i * 0.07D, k * 0.07D);
        double d2 = biomeTerrainNoise.func_151601_a(i * 0.4D, k * 0.4D);
        for (int j = ySize - 1; j >= stoneHeight; --j) {
            int index = xzIndex * ySize + j;
            Block block = blocks[index];
            if (block != this.topBlock && block != this.fillerBlock && block != Blocks.stone) continue;
            blocks[index] = LOTRMod.rock;
            meta[index] = 5;
            if (d1 + d2 > 0.5D) {
                blocks[index] = Blocks.stone;
                meta[index] = 0;
            }
            if (d1 + d2 < 0.1D) {
                blocks[index] = Blocks.grass;
                meta[index] = 0;
            }
        }
    }

}
