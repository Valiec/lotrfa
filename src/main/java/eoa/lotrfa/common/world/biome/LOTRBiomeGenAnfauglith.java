package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenDDCamp2;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenDDWatchtower;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.feature.LOTRWorldGenBoulder;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.gen.NoiseGeneratorPerlin;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;

public class LOTRBiomeGenAnfauglith extends LOTRBiome {
    private NoiseGeneratorPerlin noiseDirt = new NoiseGeneratorPerlin(new Random(42956029606L), 1);
    private NoiseGeneratorPerlin noiseGravel = new NoiseGeneratorPerlin(new Random(7185609602367L), 1);
    private NoiseGeneratorPerlin noiseMordorGravel = new NoiseGeneratorPerlin(new Random(12480634985056L), 1);
    private WorldGenerator boulderGen = new LOTRWorldGenBoulder(LOTRMod.rock, 0, 1, 2);

    public LOTRBiomeGenAnfauglith(int i, boolean major) {
        super(i, major);
        this.setDisableRain();
        this.topBlock = LOTRFABlocks.anfauglithAsh;
        this.topBlockMeta = 0;
        this.fillerBlock = LOTRFABlocks.anfauglithAsh;
        this.fillerBlockMeta = 0;
        this.biomeColors.setFoggy(true);
        this.spawnableCreatureList.clear();
        this.spawnableWaterCreatureList.clear();
        this.spawnableLOTRAmbientList.clear();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcBombardier.class, 3, 1, 2));
        this.decorator.treesPerChunk = 0;
        this.decorator.flowersPerChunk = 0;
        this.decorator.grassPerChunk = 0;
        this.decorator.addTree(LOTRTreeType.CHARRED, 1000);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenDDCamp2(false), 300);
        this.decorator.addRandomStructure(new LOTRWorldGenDDWatchtower(false), 1000);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreMorgulIron, 8), 20.0f, 0, 64);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreGulduril, 8), 8.0f, 0, 32);
        this.biomeColors.setSky(5523773);
        this.biomeColors.setClouds(3355443);
        this.biomeColors.setFog(0x4e473f);
        this.biomeColors.setWater(2498845);
        this.biomeColors.setGrass(0x5a6643);
        this.biomeColors.setFoliage(0x576a34);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.hithlum, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angband, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseUlfang, LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterAnfauglith;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.ardGalen;
    }

    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.MORDOR.getSubregion("dagorlad");
    }

    @Override
    public boolean getEnableRiver() {
        return false;
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.MORDOR;
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        double d1 = this.noiseDirt.func_151601_a(i * 0.09, k * 0.09);
        double d2 = this.noiseDirt.func_151601_a(i * 0.6, k * 0.6);
        double d3 = this.noiseGravel.func_151601_a(i * 0.09, k * 0.09);
        double d4 = this.noiseGravel.func_151601_a(i * 0.6, k * 0.6);
        double d5 = this.noiseMordorGravel.func_151601_a(i * 0.09, k * 0.09);
        double d6 = this.noiseMordorGravel.func_151601_a(i * 0.6, k * 0.6);
        
        if (d5 + d6 > 0.5) {
            this.topBlock = LOTRMod.mordorDirt;
            this.topBlockMeta = 0;
        }
        else if (d3 + d4 > 0.96) {
            this.topBlock = LOTRMod.rock;
            this.topBlockMeta = 0;
        }
        else if (d1 + d2 > 0.7) {
            this.topBlock = LOTRMod.mordorGravel;
            this.topBlockMeta = 0;
        }
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    public void decorate(World world, Random random, int i, int k) {
        super.decorate(world, random, i, k);
        if (random.nextInt(24) == 0) {
            int boulders = 1 + random.nextInt(4);
            for (int l = 0; l < boulders; ++l) {
                int i1 = i + random.nextInt(16) + 8;
                int k1 = k + random.nextInt(16) + 8;
                this.boulderGen.generate(world, random, i1, world.getHeightValue(i1, k1), k1);
            }
        }
    }

    @Override
    public float getTreeIncreaseChance() {
        return 0.05f;
    }

    @Override
    public int spawnCountMultiplier() {
        return 3;
    }

    @Override
    public boolean canSpawnHostilesInDay() {
        return true;
    }
}
