package eoa.lotrfa.common.world.biome;

import java.util.*;
import com.google.common.math.IntMath;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenDorLominCamp;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.map.LOTRRoads;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRBiomeGenDorLomin extends LOTRBiomeGenHithlum {

    private List<LOTRSpawnEntry> vineyardSpawnList = new ArrayList<LOTRSpawnEntry>();

    public LOTRBiomeGenDorLomin(int i, boolean major) {
        super(i, major);
        this.addBiomeVariant(LOTRBiomeVariant.VINEYARD, 8.0f);
        this.invasionSpawns.clearInvasions();
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenDorLominCamp(false), 1500);
        this.biomeColors.setGrass(0x3a962c);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(100, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDorLominLevyman.class, 20, 2, 4), new LOTRSpawnEntry(LOTREntityDorLominSoldier.class, 10, 3, 8), new LOTRSpawnEntry(LOTREntityDorLominArcher.class, 5, 3, 8)}));
        this.vineyardSpawnList.add(new LOTRSpawnEntry(LOTREntityDorLominVinehand.class, 7, 4, 6));
        this.vineyardSpawnList.add(new LOTRSpawnEntry(LOTREntityHithlumVinehand.class, 3, 4, 6));
        this.vineyardSpawnList.add(new LOTRSpawnEntry(LOTREntityHithlumVinekeeper.class, 2, 1, 1));
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.dorLomin;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterDorLomin;
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        boolean vineyard = variant == LOTRBiomeVariant.VINEYARD;
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        int chunkX = i & 15;
        int chunkZ = k & 15;
        int xzIndex = chunkX * 16 + chunkZ;
        int ySize = blocks.length / 256;
        if (vineyard && !LOTRRoads.isRoadAt(i, k)) {
            for (int j = 128; j >= 0; --j) {
                int index = xzIndex * ySize + j;
                Block above = blocks[index + 1];
                Block block = blocks[index];
                if (block == null || !block.isOpaqueCube() || above != null && above.getMaterial() != Material.air) continue;
                int i1 = IntMath.mod(i, 6);
                int i2 = IntMath.mod(i, 24);
                int k1 = IntMath.mod(k, 32);
                int k2 = IntMath.mod(k, 64);
                if ((i1 == 0 || i1 == 5) && k1 != 0) {
                    double d;
                    blocks[index] = Blocks.farmland;
                    meta[index] = 0;
                    int h = 2;
                    if (biomeTerrainNoise.func_151601_a(i, k) > 0.0) {
                        ++h;
                    }
                    boolean red = biomeTerrainNoise.func_151601_a(i * (d = 0.01), k * d) > 0.0;
                    Block vineBlock = red ? LOTRMod.grapevineRed : LOTRMod.grapevineWhite;
                    for (int j1 = 1; j1 <= h; ++j1) {
                        blocks[index + j1] = vineBlock;
                        meta[index + j1] = 7;
                    }
                    break;
                }
                if (i1 >= 2 && i1 <= 3) {
                    blocks[index] = LOTRMod.dirtPath;
                    meta[index] = 0;
                    if (i1 != i2 || (i1 != 2 || k2 != 16) && (i1 != 3 || k2 != 48)) break;
                    int h = 3;
                    for (int j1 = 1; j1 <= h; ++j1) {
                        if (j1 == h) {
                            blocks[index + j1] = Blocks.torch;
                            meta[index + j1] = 5;
                            continue;
                        }
                        blocks[index + j1] = LOTRMod.fence2;
                        meta[index + j1] = 10;
                    }
                    break;
                }
                blocks[index] = this.topBlock;
                meta[index] = (byte) this.topBlockMeta;
                break;
            }
        }
    }

    @Override
    public List<LOTRSpawnEntry> getNPCSpawnList(World world, Random random, int i, int j, int k, LOTRBiomeVariant variant) {
        if (variant == LOTRBiomeVariant.VINEYARD) {
            return this.vineyardSpawnList;
        }
        return super.getNPCSpawnList(world, random, i, j, k, variant);
    }

}
