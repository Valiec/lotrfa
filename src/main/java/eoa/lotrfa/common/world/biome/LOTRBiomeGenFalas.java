package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.LOTRAchievement;
import lotr.common.entity.animal.LOTREntitySeagull;
import lotr.common.world.biome.LOTRBiomeGenDorEnErnil;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.world.biome.BiomeGenBase;

public class LOTRBiomeGenFalas extends LOTRBiomeGenDorEnErnil {

    public LOTRBiomeGenFalas(int i, boolean major) {
        super(i, major);
        this.biomeColors.setGrass(0x2ea62e);
        this.decorator.generateOrcDungeon = false;
        this.spawnableLOTRAmbientList.add(new BiomeGenBase.SpawnListEntry(LOTREntitySeagull.class, 6, 4, 4));
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFalathrimElf.class, 10, 4, 4));
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(30, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityFalathrimSailor.class, 20, 2, 6), new LOTRSpawnEntry(LOTREntityFalathrimArcher.class, 10, 2, 6), new LOTRSpawnEntry(LOTREntityFalathrimMariner.class, 5, 2, 4)}));
        this.decorator.clearRandomStructures();
        this.spawnableEvilList.clear();
        this.decorator.clearVillages();
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimMerchantStall(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimMerchantStall(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimSmithy(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimHouse(false), 100);
        this.decorator.addRandomStructure(new LOTRWorldGenFalathrimTurret(false), 600);
        this.invasionSpawns.clearInvasions();
        this.clearTravellingTraders();
        this.decorator.addTree(LOTRTreeType.MALLORN_BOUGHS, 5);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.falas;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterFalas;
    }

}
