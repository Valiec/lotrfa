package eoa.lotrfa.common.world.biome;

import lotr.common.world.biome.LOTRBiomeGenBarrowDowns;

public class LOTRBiomeGenBarrows extends LOTRBiomeGenBarrowDowns {

    public LOTRBiomeGenBarrows(int i, boolean major) {
        super(i, major);
        this.spawnableEvilList.clear();
        this.decorator.generateOrcDungeon = false;
        this.invasionSpawns.clearInvasions();
        // TODO Auto-generated constructor stub
    }

}
