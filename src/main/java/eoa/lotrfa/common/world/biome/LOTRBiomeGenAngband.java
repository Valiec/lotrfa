package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.*;
import eoa.lotrfa.common.world.structure.nature.LOTRWorldGenAngbandBoulder;
import eoa.lotrfa.common.world.structure.nature.LOTRWorldGenDDBlastedLand;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiomeGenMordor;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.gen.NoiseGeneratorPerlin;

public class LOTRBiomeGenAngband extends LOTRBiomeGenMordor {

    private NoiseGeneratorPerlin noiseDirt = new NoiseGeneratorPerlin(new Random(42956029606L), 1);
    private NoiseGeneratorPerlin noiseGravel = new NoiseGeneratorPerlin(new Random(7185609602367L), 1);
    private NoiseGeneratorPerlin noiseMordorGravel = new NoiseGeneratorPerlin(new Random(12480634985056L), 1);

    public LOTRBiomeGenAngband(int i, boolean major) {
        super(i, major);
        // this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityMiniBalrog.class, 6,
        // 1, 2));

        this.setDisableRain();
        this.boulderGen = new LOTRWorldGenAngbandBoulder(LOTRMod.rock, 0, 2, 8);
        this.topBlock = LOTRMod.rock;
        this.topBlockMeta = 0;
        this.fillerBlock = LOTRMod.rock;
        this.fillerBlockMeta = 0;
        this.biomeColors.setFoggy(true);
        this.decorator.grassPerChunk = 4;
        this.biomeColors.setGrass(7896151);
        this.biomeColors.setFoliage(7896151);
        this.decorator.doubleGrassPerChunk = 2;
        this.decorator.clearTrees();
        this.decorator.treesPerChunk = 0;
        this.decorator.willowPerChunk = 0;
        this.decorator.reedPerChunk = 0;
        this.clearBiomeVariants();
        this.addBiomeVariant(LOTRBiomeVariant.FLOWERS);
        this.decorator.clearTrees();
        this.decorator.dryReedChance = 0f;
        this.decorator.addTree(LOTRTreeType.CHARRED, 10);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenAngbandTower(false), 300);
        this.decorator.addRandomStructure(new LOTRWorldGenDDBlastedLand(false), 40);
        this.decorator.addRandomStructure(new LOTRWorldGenAngbandWargPit(false), 200); // !
        this.decorator.addRandomStructure(new LOTRWorldGenAngbandCamp(false), 50); // !
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityAngbandOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityAngbandOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityAngbandWarg.class, 5, 2, 4));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTormentedElfSmith.class, 5, 2, 3));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityAngbandBoldog.class, 3, 1, 3));
        this.invasionSpawns.clearInvasions();
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
        // this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityMiniBalrog.class, 6,
        // 1, 2));
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.angband;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterAngband;
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        double d1 = this.noiseDirt.func_151601_a(i * 0.09, k * 0.09);
        double d2 = this.noiseDirt.func_151601_a(i * 0.6, k * 0.6);
        double d3 = this.noiseGravel.func_151601_a(i * 0.09, k * 0.09);
        double d4 = this.noiseGravel.func_151601_a(i * 0.6, k * 0.6);
        double d5 = this.noiseMordorGravel.func_151601_a(i * 0.09, k * 0.09);
        double d6 = this.noiseMordorGravel.func_151601_a(i * 0.6, k * 0.6);
        
        if (d5 + d6 > 0.5) {
            this.topBlock = LOTRFABlocks.anfauglithAsh;
            this.topBlockMeta = 0;
        }
        else if (d3 + d4 > 0.96) {
            this.topBlock = LOTRMod.mordorDirt;
            this.topBlockMeta = 0;
        }
        else if (d1 + d2 > 0.7) {
            this.topBlock = LOTRMod.mordorGravel;
            this.topBlockMeta = 0;
        }
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

}
