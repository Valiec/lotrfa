package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.*;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRBiomeGenGenericMountainFoothills extends LOTRBiomeGenGenericMountains {

    // public LOTRAchievement[] biomeAchieves = {LOTRAchievement.enterGreyMountains,
    // LOTRFA.enterAndram, LOTRFA.enterNevrastMtns, LOTRFA.enterEredLomin,
    // LOTRFA.enterFeanorLands};

    public int biomeAchieveId;

    public LOTRBiomeGenGenericMountainFoothills(int i, boolean major, int achieve) {
        super(i, major, achieve);
        this.addBiomeVariantSet(LOTRBiomeVariant.SET_MOUNTAINS);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LARCH, 0.2F);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_PINE, 0.2F);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_ASPEN, 0.2F);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_MAPLE, 0.2F);
        this.decorator.biomeGemFactor = 1.0F;
        this.decorator.flowersPerChunk = 0;
        this.decorator.grassPerChunk = 0;
        this.decorator.doubleGrassPerChunk = 0;
        this.decorator.addTree(LOTRTreeType.SPRUCE, 400);
        this.decorator.addTree(LOTRTreeType.SPRUCE_THIN, 400);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA, 50);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA_THIN, 10);
        this.decorator.addTree(LOTRTreeType.LARCH, 500);
        this.decorator.addTree(LOTRTreeType.FIR, 500);
        this.decorator.addTree(LOTRTreeType.PINE, 500);
        this.registerMountainsFlowers();
        this.biomeColors.setSky(10862798);
        this.decorator.generateOrcDungeon = false;
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityMordorOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityMordorOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityMordorWarg.class, 6, 4, 4));
        this.invasionSpawns.clearInvasions();
        this.decorator.clearRandomStructures();
        this.biomeAchieveId = achieve;
        // TODO Auto-generated constructor stub
    }

    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.GREY_MOUNTAINS.getSubregion("greyMountains");
    }

    @Override
    public boolean getEnableRiver() {
        return false;
    }

    @Override
    protected void generateMountainTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, int xzIndex, int ySize, int height, int rockDepth, LOTRBiomeVariant variant) {
        int snowHeight = 150 - rockDepth;
        int stoneHeight = snowHeight - 40;

        for (int j = ySize - 1; j >= stoneHeight; --j) {
            int index = xzIndex * ySize + j;
            Block block = blocks[index];
            if (j >= snowHeight && block == this.topBlock) {
                blocks[index] = Blocks.snow;
                meta[index] = 0;
            }
            else if (block == this.topBlock || block == this.fillerBlock) {
                blocks[index] = Blocks.stone;
                meta[index] = 0;
            }
        }

    }

    @Override
    public void decorate(World world, Random random, int i, int k) {
        super.decorate(world, random, i, k);

        int count;
        int i1;
        int j1;
        int k1;
        for (count = 0; count < 3; ++count) {
            i1 = i + random.nextInt(16) + 8;
            j1 = k + random.nextInt(16) + 8;
            k1 = world.getHeightValue(i1, j1);
            if (k1 < 100) {
                this.decorator.genTree(world, random, i1, k1, j1);
            }
        }

        for (count = 0; count < 2; ++count) {
            i1 = i + random.nextInt(16) + 8;
            j1 = random.nextInt(128);
            k1 = k + random.nextInt(16) + 8;
            if (j1 < 100) {
                this.getRandomWorldGenForGrass(random).generate(world, random, i1, j1, k1);
            }
        }

    }

    @Override
    public float getChanceToSpawnAnimals() {
        return 0.0F;
    }

    @Override
    public float getTreeIncreaseChance() {
        return 0.0F;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        LOTRAchievement[] biomeAchieves = {LOTRFAAchievements.enterAndram, LOTRFAAchievements.enterNevrast, LOTRFAAchievements.enterEredLomin};

        return biomeAchieves[this.biomeAchieveId];
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        LOTRWaypoint.Region[] biomeRegions = {LOTRWaypoint.Region.MISTY_MOUNTAINS, LOTRWaypoint.Region.ERIADOR, LOTRWaypoint.Region.MISTY_MOUNTAINS, LOTRWaypoint.Region.MISTY_MOUNTAINS, LOTRWaypoint.Region.ERIADOR};
        return biomeRegions[this.biomeAchieveId];
    }

}
