package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.animal.LOTREntityBlackSwan;
import eoa.lotrfa.common.entity.npc.LOTREntityDoriathWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenSwanfleet;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenAelinUial extends LOTRBiomeGenSwanfleet {

    public LOTRBiomeGenAelinUial(int i, boolean major) {
        super(i, major);
        this.biomeColors.setFoggy(true);
        this.biomeColors.setWater(0x1f0f47);
        this.decorator.vinesPerChunk = 20;
        this.decorator.logsPerChunk = 3;
        this.decorator.flowersPerChunk = 0;
        this.decorator.grassPerChunk = 10;
        this.decorator.doubleGrassPerChunk = 8;
        this.decorator.enableFern = true;
        this.decorator.mushroomsPerChunk = 3;
        this.decorator.waterlilyPerChunk = 3;
        this.decorator.canePerChunk = 10;
        this.decorator.reedPerChunk = 3;
        this.spawnableLOTRAmbientList.add(new SpawnListEntry(LOTREntityBlackSwan.class, 20, 4, 8));
        this.spawnableEvilList.clear();
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityDoriathWarrior.class, 1, 2, 4));
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.westBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterAelinUial;
    }

}
