package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;

public class LOTRBiomeGenOssiriand extends LOTRBiomeGenGenericGrassland {

    public LOTRBiomeGenOssiriand(int i, boolean major) {
        super(i, major);
        this.biomeColors.setGrass(0x507a32);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.laegrim, LOTREventSpawner.EventChance.COMMON);
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.ossiriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterOssiriand;
    }

}
