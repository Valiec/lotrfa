package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenBelegostDwarfHouse;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.*;
import lotr.common.world.biome.LOTRBiomeGenBlueMountains;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.world.World;

public class LOTRBiomeGenBelegost extends LOTRBiomeGenBlueMountains {

    public LOTRBiomeGenBelegost(int i, boolean major) {
        super(i, major);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityBlueDwarf.class, 100, 4, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityBlueDwarfMiner.class, 15, 1, 3));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityBlueDwarfWarrior.class, 20, 4, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityBlueDwarfAxeThrower.class, 10, 4, 4));
        this.spawnableEvilList.clear();
        this.decorator.generateOrcDungeon = false;
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, LOTREventSpawner.EventChance.RARE);
        this.setGoodEvilWeight(100, 0);
        this.decorator.clearRandomStructures();
        //this.decorator.addRandomStructure(new LOTRWorldGenBelegostDwarvenTower(false), 400);
        //this.decorator.addRandomStructure(new LOTRWorldGenBelegostDwarfSmithy(false), 400);
        // this.decorator.addRandomStructure(new
        // LOTRWorldGenBlueMountainsStronghold(false), 400);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void decorate(World world, Random random, int i, int k) {
        int l;
        int i1;
        this.decorator.decorate(world, random, i, k);
        for (l = 0; l < 4; ++l) {
            i1 = i + random.nextInt(16) + 8;
            int j1 = 70 + random.nextInt(60);
            int k1 = k + random.nextInt(16) + 8;
            new LOTRWorldGenBelegostDwarfHouse(false, true).generate(world, random, i1, j1, k1);
        }
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterBelegostMtns;
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.DWARVEN;
    }
}
