package eoa.lotrfa.common.world.biome;

import java.util.Random;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangAxeThrower;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangBerserker;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangCampfire;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangHillFort;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangHillmanVillage;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangMerchantStall;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangTavern;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangWatchtower;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.entity.animal.LOTREntityBear;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.feature.LOTRWorldGenBoulder;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRInvasions;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenSmallStoneRuin;
import lotr.common.world.structure2.LOTRWorldGenStoneRuin;
import net.minecraft.block.Block;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.gen.NoiseGeneratorPerlin;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.util.ForgeDirection;

public class LOTRBiomeGenTaurdru extends LOTRBiome {

    protected static NoiseGeneratorPerlin noiseDirt;
    protected static NoiseGeneratorPerlin noiseStone;
    protected static NoiseGeneratorPerlin noiseSnow;
    private WorldGenerator boulderGen;

    public LOTRBiomeGenTaurdru(int i, boolean major) {
        super(i, major);
        this.boulderGen = new LOTRWorldGenBoulder(Blocks.stone, 0, 1, 3);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangTavern(false), 160);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangCampfire(false), 80);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangHillFort(false), 1200);
        this.decorator.addRandomStructure(new LOTRWorldGenStoneRuin.STONE(1, 3), 1000);
        this.decorator.addRandomStructure(new LOTRWorldGenSmallStoneRuin(false), 600);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangHillmanVillage(false), 800);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangMerchantStall(false), 700);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangWatchtower(false), 500);
        this.setBanditChance(LOTREventSpawner.EventChance.UNCOMMON);
        this.clearTravellingTraders();
        this.clearBiomeVariants();
        this.variantChance = 0.75f;
        this.addBiomeVariantSet(LOTRBiomeVariant.SET_FOREST);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LIGHT);
        this.addBiomeVariant(LOTRBiomeVariant.DEADFOREST_SPRUCE);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_PINE);
        this.decorator.treesPerChunk = 2;
        this.decorator.flowersPerChunk = 3;
        this.decorator.grassPerChunk = 8;
        this.decorator.doubleGrassPerChunk = 2;
        this.decorator.doubleFlowersPerChunk = 1;
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.SPRUCE, 200);
        this.decorator.addTree(LOTRTreeType.SPRUCE_THIN, 100);
        this.decorator.addTree(LOTRTreeType.SPRUCE_DEAD, 50);
        this.decorator.addTree(LOTRTreeType.FIR, 200);
        this.decorator.addTree(LOTRTreeType.PINE, 200);
        this.registerTaigaFlowers();
        this.addBiomeVariant(LOTRBiomeVariant.HILLS);
        this.biomeColors.setGrass(0x466a24);
        this.biomeColors.setSky(0x344258);
        this.biomeColors.setClouds(0x9a9898);
        this.biomeColors.setFog(0x968d82);
        this.biomeColors.setWater(0x203040);
        this.setGoodEvilWeight(30, 70);
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.spawnableLOTRAmbientList.clear();
        this.spawnableGoodList.clear();
        this.spawnableCreatureList.clear();
        this.spawnableCreatureList.add(new BiomeGenBase.SpawnListEntry(EntityWolf.class, 8, 4, 8));
        this.spawnableCreatureList.add(new BiomeGenBase.SpawnListEntry(LOTREntityBear.class, 4, 1, 4));
        this.spawnableEvilList.clear();
        //this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityUlfangMan.class, 30, 4, 6));
        this.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(200, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityUlfangWarrior.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityUlfangArcher.class, 2, 4, 6)}));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityUlfangAxeThrower.class, 2, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityUlfangBerserker.class, 1, 2, 2));
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseBor, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.feanorians, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRInvasions.BLUE_MOUNTAINS, LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.taurdru;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterTaurdru;
    }

    @Override
    public float getChanceToSpawnAnimals() {
        return 0.5f;
    }

    public static boolean isTundraSnowy(final int i, final int k) {
        final double d1 = LOTRBiomeGenTaurdru.noiseSnow.func_151601_a(i * 0.002, k * 0.002);
        double d2 = LOTRBiomeGenTaurdru.noiseSnow.func_151601_a(i * 0.05, k * 0.05);
        double d3 = LOTRBiomeGenTaurdru.noiseSnow.func_151601_a(i * 0.3, k * 0.3);
        d2 *= 0.3;
        d3 *= 0.3;
        return d1 + d2 + d3 > 0.8;
    }

    @Override
    public void generateBiomeTerrain(final World world, final Random random, final Block[] blocks, final byte[] meta, final int i, final int k, final double stoneNoise, final int height, final LOTRBiomeVariant variant) {
        final Block topBlock_pre = this.topBlock;
        final int topBlockMeta_pre = this.topBlockMeta;
        final Block fillerBlock_pre = this.fillerBlock;
        final int fillerBlockMeta_pre = this.fillerBlockMeta;
        final double d1 = LOTRBiomeGenTaurdru.noiseDirt.func_151601_a(i * 0.07, k * 0.07);
        final double d2 = LOTRBiomeGenTaurdru.noiseDirt.func_151601_a(i * 0.3, k * 0.3);
        final double d3 = LOTRBiomeGenTaurdru.noiseStone.func_151601_a(i * 0.07, k * 0.07);
        final double d4 = LOTRBiomeGenTaurdru.noiseStone.func_151601_a(i * 0.3, k * 0.3);
        if (d3 + d4 > 1.2) {
            this.topBlock = Blocks.stone;
            this.topBlockMeta = 0;
            this.fillerBlock = this.topBlock;
            this.fillerBlockMeta = this.topBlockMeta;
        }
        else if (d1 + d2 > 0.8) {
            this.topBlock = Blocks.dirt;
            this.topBlockMeta = 1;
        }
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    public void decorate(final World world, final Random random, final int i, final int k) {
        super.decorate(world, random, i, k);
        if (random.nextInt(2) == 0) {
            final int i2 = i + random.nextInt(16) + 8;
            final int k2 = k + random.nextInt(16) + 8;
            final int j1 = world.getHeightValue(i2, k2);
            for (int bushes = 4 + random.nextInt(20), l = 0; l < bushes; ++l) {
                final int i3 = i2 + MathHelper.getRandomIntegerInRange(random, -4, 4);
                final int k3 = k2 + MathHelper.getRandomIntegerInRange(random, -4, 4);
                final int j2 = j1 + MathHelper.getRandomIntegerInRange(random, -1, 1);
                final Block below = world.getBlock(i3, j2 - 1, k3);
                final Block block = world.getBlock(i3, j2, k3);
                if (below.canSustainPlant((IBlockAccess) world, i3, j2 - 1, k3, ForgeDirection.UP, (IPlantable) Blocks.sapling) && !block.getMaterial().isLiquid() && block.isReplaceable(world, i3, j2, k3)) {
                    Block leafBlock = Blocks.leaves;
                    int leafMeta = 1;
                    if (random.nextInt(3) == 0) {
                        leafBlock = LOTRMod.leaves3;
                        leafMeta = 0;
                    }
                    else if (random.nextInt(3) == 0) {
                        leafBlock = LOTRMod.leaves2;
                        leafMeta = 1;
                    }
                    world.setBlock(i3, j2, k3, leafBlock, leafMeta | 0x4, 2);
                }
            }
        }
        if (random.nextInt(40) == 0) {
            for (int boulders = 1 + random.nextInt(4), m = 0; m < boulders; ++m) {
                final int i4 = i + random.nextInt(16) + 8;
                final int k4 = k + random.nextInt(16) + 8;
                this.boulderGen.generate(world, random, i4, world.getHeightValue(i4, k4), k4);
            }
        }
    }

    @Override
    public float getTreeIncreaseChance() {
        return 0.04f;
    }

    static {
        LOTRBiomeGenTaurdru.noiseDirt = new NoiseGeneratorPerlin(new Random(47684796930956L), 1);
        LOTRBiomeGenTaurdru.noiseStone = new NoiseGeneratorPerlin(new Random(8894086030764L), 1);
        LOTRBiomeGenTaurdru.noiseSnow = new NoiseGeneratorPerlin(new Random(2490309256000602L), 1);
    }

    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.FORODWAITH.getSubregion("tundra");
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.MORDOR;
    }

}
