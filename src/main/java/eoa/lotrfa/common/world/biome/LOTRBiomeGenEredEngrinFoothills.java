package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.world.spawning.LOTREventSpawner;

public class LOTRBiomeGenEredEngrinFoothills extends LOTRBiomeGenEredEngrin {

    public LOTRBiomeGenEredEngrinFoothills(int i, boolean major) {
        super(i, major);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.dorDaidelos, LOTREventSpawner.EventChance.UNCOMMON);
    }

}
