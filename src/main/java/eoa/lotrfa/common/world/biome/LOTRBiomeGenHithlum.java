package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityHithlumElf;
import eoa.lotrfa.common.entity.npc.LOTREntityHithlumWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenHithlum extends LOTRBiome {

    public LOTRBiomeGenHithlum(int i, boolean major) {
        super(i, major);
        this.spawnableEvilList.clear();
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(200, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityHithlumElf.class, 10, 4, 4), new LOTRSpawnEntry(LOTREntityHithlumWarrior.class, 2, 4, 4)}));
        this.addBiomeVariant(LOTRBiomeVariant.VINEYARD, 8.0f);
        this.setGoodEvilWeight(100, 0);
        this.decorator.clearOres();
        //this.biomeColors.setFoggy(true);
        this.addBiomeVariant(LOTRBiomeVariant.FLOWERS);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LIGHT);
        this.addBiomeVariant(LOTRBiomeVariant.STEPPE);
        this.addBiomeVariant(LOTRBiomeVariant.HILLS);
        this.addBiomeVariant(LOTRBiomeVariant.HILLS_FOREST);
        this.addBiomeVariant(LOTRBiomeVariant.DENSEFOREST_OAK);
        this.addBiomeVariant(LOTRBiomeVariant.DENSEFOREST_BIRCH);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BEECH, 0.2f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BIRCH, 0.5f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_ASPEN, 0.5f);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_APPLE_PEAR, 1.0f);
        this.decorator.setTreeCluster(10, 20);
        this.decorator.treesPerChunk = 0;
        this.decorator.willowPerChunk = 1;
        this.decorator.flowersPerChunk = 3;
        this.decorator.grassPerChunk = 8;
        this.decorator.doubleGrassPerChunk = 1;
        this.decorator.addTree(LOTRTreeType.OAK, 100);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 25);
        this.decorator.addTree(LOTRTreeType.BIRCH, 500);
        this.decorator.addTree(LOTRTreeType.BIRCH_TALL, 500);
        this.decorator.addTree(LOTRTreeType.BIRCH_LARGE, 200);
        this.decorator.addTree(LOTRTreeType.BIRCH_PARTY, 50);
        this.decorator.addTree(LOTRTreeType.BEECH, 100);
        this.decorator.addTree(LOTRTreeType.BEECH_LARGE, 25);
        this.decorator.addTree(LOTRTreeType.CHESTNUT, 40);
        this.decorator.addTree(LOTRTreeType.CHESTNUT_LARGE, 10);
        this.decorator.addTree(LOTRTreeType.ASPEN, 300);
        this.decorator.addTree(LOTRTreeType.ASPEN_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.APPLE, 2);
        this.decorator.addTree(LOTRTreeType.PEAR, 2);
        this.registerPlainsFlowers();
        //this.biomeColors.setGrass(0x50b54a); //3c7a39
        this.biomeColors.setGrass(0x459640);
        this.biomeColors.setFoliage(0x31642e);
        this.biomeColors.setFog(0x678277);
        this.biomeColors.setWater(0x2a3850);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenHithlumHouse(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenHithlumHall(false), 800);
        this.decorator.addRandomStructure(new LOTRWorldGenHithlumForge(false), 600);
        this.decorator.addRandomStructure(new LOTRWorldGenHithlumTower(false), 800);
        this.decorator.addRandomStructure(new LOTRWorldGenHithlumMerchantStall(false), 600);
        this.decorator.addRandomStructure(new LOTRWorldGenHithlumBrewery(false), 600);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
        this.invasionSpawns.clearInvasions();
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.hithlum;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterHithlum;
    }
    
    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.LINDON.getSubregion("lindon");
    }

}
