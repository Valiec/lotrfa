package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityBrethilArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityBrethilLevyman;
import eoa.lotrfa.common.entity.npc.LOTREntityBrethilSoldier;
import eoa.lotrfa.common.entity.npc.LOTREntityDoriathWarden;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenBrethilTower;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.LOTRBiomeGenBlackrootVale;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenBrethil extends LOTRBiomeGenBlackrootVale {

    public LOTRBiomeGenBrethil(int i, boolean major) {
        super(i, major);
        this.decorator.treesPerChunk = 10;
        this.addBiomeVariant(LOTRBiomeVariant.DENSEFOREST_LEBETHRON);
        this.addBiomeVariant(LOTRBiomeVariant.DENSEFOREST_BIRCH);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BIRCH);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BEECH);
        this.addBiomeVariant(LOTRBiomeVariant.CLEARING);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LIGHT);
        this.decorator.clearTrees();
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(100, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityBrethilLevyman.class, 20, 4, 6), new LOTRSpawnEntry(LOTREntityBrethilSoldier.class, 5, 2, 6), new LOTRSpawnEntry(LOTREntityBrethilArcher.class, 15, 2, 6)}));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityDoriathWarden.class, 1, 1, 1));
        this.spawnableEvilList.clear();
        this.decorator.addTree(LOTRTreeType.OAK, 500);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 200);
        this.decorator.addTree(LOTRTreeType.DARK_OAK, 100);
        this.decorator.addTree(LOTRTreeType.BIRCH, 500);
        this.decorator.addTree(LOTRTreeType.BEECH, 200);
        this.decorator.addTree(LOTRTreeType.BEECH_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.BIRCH_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.LEBETHRON, 100);
        this.decorator.addTree(LOTRTreeType.LEBETHRON_LARGE, 50);
        this.decorator.addTree(LOTRTreeType.FIR, 300);
        this.decorator.addTree(LOTRTreeType.LARCH, 100);
        this.decorator.addTree(LOTRTreeType.ASPEN, 100);
        this.decorator.addTree(LOTRTreeType.CHESTNUT, 200);
        this.decorator.addTree(LOTRTreeType.CHESTNUT_LARGE, 50);
        this.decorator.addTree(LOTRTreeType.APPLE, 5);
        this.decorator.addTree(LOTRTreeType.PEAR, 5);
        this.decorator.addTree(LOTRTreeType.PLUM, 5);
        this.decorator.addTree(LOTRTreeType.OLIVE, 1);
        this.decorator.addTree(LOTRTreeType.ALMOND, 1);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhoth, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.brethil, LOTREventSpawner.EventChance.COMMON);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
        this.decorator.clearRandomStructures();
        //this.decorator.addRandomStructure(new LOTRWorldGenBrethilMerchantStall(false, 0, LOTRFABlocks.craftingTableBrethil), 100);
        //this.decorator.addRandomStructure(new LOTRWorldGenBrethilMerchantStall(false, 1, LOTRFABlocks.craftingTableBrethil), 100);
        this.decorator.addRandomStructure(new LOTRWorldGenBrethilTower(false), 250);
        //this.decorator.addRandomStructure(new LOTRWorldGenBrethilHouse(false), 100);
    }

    @Override
    protected void addFiefdomStructures() {
        // this.decorator.addRandomStructure(new LOTRWorldGenBlackrootWatchfort(false),
        // 1000);
        // this.decorator.addVillage(new LOTRVillageGenEdain(this,
        // LOTRWorldGenGondorStructure.GondorFiefdom.BLACKROOT_VALE, 0.6f));
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.brethil;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterBrethil;
    }
    
}
