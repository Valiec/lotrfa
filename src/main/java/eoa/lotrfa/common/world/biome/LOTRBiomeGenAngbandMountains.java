package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenMordorMountains;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenAngbandMountains extends LOTRBiomeGenMordorMountains {

    public LOTRBiomeGenAngbandMountains(int i, boolean major) {
        super(i, major);
        // this.spawnableCaveCreatureList.addAll(LOTRSpawnEntry.chanceList(1000, new LOTRSpawnEntry[] {
        // new LOTRSpawnEntry(LOTREntityAngbandTormentedElf.class, 4, 2, 4)}));
        // this.spawnableCaveCreatureList.add(new
        // LOTRSpawnEntry(LOTREntityTormentedElfSmith.class, 5, 1, 2));
        this.topBlock = LOTRFABlocks.rockSarllith;
        this.topBlockMeta = 0;
        this.fillerBlock = LOTRFABlocks.rockSarllith;
        this.fillerBlockMeta = 0;
        this.biomeColors.setGrass(0x5b4e29);
        this.biomeColors.setFoliage(0x636129);
        this.biomeColors.setSky(0x5f343c);
        this.biomeColors.setClouds(0x483333);
        this.biomeColors.setFog(0x432f25);
        this.biomeColors.setWater(0x1e2623);
        this.decorator.clearRandomStructures();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcBombardier.class, 5, 1, 2));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothWarg.class, 30, 4, 4));
        this.decorator.treesPerChunk = 0;
        this.decorator.flowersPerChunk = 0;
        this.decorator.treesPerChunk = 0;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.ardGalen;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterDorDaedeloth;
    }

}
