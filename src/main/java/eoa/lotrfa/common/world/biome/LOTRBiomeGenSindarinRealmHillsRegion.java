package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRMod;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;

public class LOTRBiomeGenSindarinRealmHillsRegion extends LOTRBiomeGenSindarinRealmRegion {
    public LOTRBiomeGenSindarinRealmHillsRegion(int i, boolean major) {
        super(i, major);
        this.clearBiomeVariants();
        this.decorator.treesPerChunk = 4;
        this.decorator.grassPerChunk = 10;
        this.decorator.addTree(LOTRTreeType.MALLORN_EXTREME, 250);
        this.decorator.addTree(LOTRTreeType.GREEN_OAK_LARGE, 250);
        this.spawnableEvilList.clear();
        this.setGoodEvilWeight(100, 0);
        this.addFlower(LOTRMod.elanor, 0, 20);
        this.addFlower(LOTRMod.niphredil, 0, 14);
        this.decorator.generateAthelas = true;
        this.enableRocky = false;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.doriath;
    }
}
