package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRAchievement;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;

public class LOTRBiomeGenEredLomin extends LOTRBiomeGenGenericMountains{

	public LOTRBiomeGenEredLomin(int i, boolean major, int achieve) {
		super(i, major, achieve);
    	this.biomeColors.setGrass(0x74883d);
    	this.biomeColors.setFoliage(0x74883d);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.dorDaidelos, LOTREventSpawner.EventChance.RARE);
	}
	
    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.hithlum;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterEredLomin;
    }

}
