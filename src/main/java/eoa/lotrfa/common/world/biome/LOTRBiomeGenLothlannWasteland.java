package eoa.lotrfa.common.world.biome;

import java.util.Random;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaedelothOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaedelothOrcArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaedelothOrcBombardier;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaedelothWarg;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangAxeThrower;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenDDCamp2;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangHillFort;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUlfangWatchtower;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.feature.LOTRWorldGenBlastedLand;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRInvasions;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.NoiseGeneratorPerlin;

public class LOTRBiomeGenLothlannWasteland extends LOTRBiome {

    private NoiseGeneratorPerlin noiseDirt = new NoiseGeneratorPerlin(new Random(42956029606L), 1);
    private NoiseGeneratorPerlin noiseGravel = new NoiseGeneratorPerlin(new Random(7185609602367L), 1);
    private NoiseGeneratorPerlin noiseMordorGravel = new NoiseGeneratorPerlin(new Random(12480634985056L), 1);
    
    public LOTRBiomeGenLothlannWasteland(int i, boolean major) {
        super(i, major);
        this.topBlock = Blocks.grass;
        this.topBlockMeta = 0;
        this.fillerBlock = Blocks.dirt;
        this.fillerBlockMeta = 0;
        this.biomeColors.setGrass(0x576a34);
        this.biomeColors.setSky(0x465258);
        this.biomeColors.setClouds(0x6a6868);
        this.biomeColors.setFog(0x777067);
        this.biomeColors.setWater(0x283440);
        this.spawnableCreatureList.clear();
        this.spawnableWaterCreatureList.clear();
        this.spawnableLOTRAmbientList.clear();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcBombardier.class, 5, 1, 2));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothWarg.class, 30, 4, 4));
        this.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(500, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityUlfangWarrior.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityUlfangArcher.class, 2, 4, 6), new LOTRSpawnEntry(LOTREntityUlfangAxeThrower.class, 2, 4, 6)}));
        this.clearBiomeVariants();
        this.addBiomeVariant(LOTRBiomeVariant.STEPPE);
        this.addBiomeVariant(LOTRBiomeVariant.STEPPE_BARREN);
        this.addBiomeVariant(LOTRBiomeVariant.DEADFOREST_SPRUCE);
        this.decorator.addTree(LOTRTreeType.SPRUCE_DEAD, 100);
        this.decorator.addTree(LOTRTreeType.CHARRED, 1000);
        this.decorator.treesPerChunk = 1;
        this.decorator.grassPerChunk = 6;
        this.decorator.flowersPerChunk = 0;
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenDDCamp2(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenBlastedLand(false), 30);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangHillFort(false), 1200);
        this.decorator.addRandomStructure(new LOTRWorldGenUlfangWatchtower(false), 500);
        this.setBanditChance(LOTREventSpawner.EventChance.UNCOMMON);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseUlfang, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.feanorians, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angband, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRInvasions.BLUE_MOUNTAINS, LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        double d1 = this.noiseDirt.func_151601_a(i * 0.09, k * 0.09);
        double d2 = this.noiseDirt.func_151601_a(i * 0.6, k * 0.6);
        double d3 = this.noiseGravel.func_151601_a(i * 0.09, k * 0.09);
        double d4 = this.noiseGravel.func_151601_a(i * 0.6, k * 0.6);
        double d5 = this.noiseMordorGravel.func_151601_a(i * 0.09, k * 0.09);
        double d6 = this.noiseMordorGravel.func_151601_a(i * 0.6, k * 0.6);
        if (d5 + d6 > 0.5) {
            this.topBlock = LOTRFABlocks.anfauglithAsh;
            this.topBlockMeta = 0;
        }
        else if (d3 + d4 > 0.96) {
            this.topBlock = LOTRMod.mordorDirt;
            this.topBlockMeta = 0;
        }
        else if (d1 + d2 > 0.7) {
            this.topBlock = Blocks.dirt;
            this.topBlockMeta = 1;
        }
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterLothlann;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.lothlann;
    }

    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.MORDOR.getSubregion("dagorlad");
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.MORDOR;
    }

}
