package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityFalathrimSailor;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.entity.animal.LOTREntityMidges;
import lotr.common.entity.animal.LOTREntitySwan;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.LOTRBiomeGenMidgewater;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.world.biome.BiomeGenBase;

public class LOTRBiomeGenLisgardh extends LOTRBiomeGenMidgewater {

    public LOTRBiomeGenLisgardh(int i, boolean major) {
        super(i, major);
        this.decorator.generateOrcDungeon = false;
        this.biomeColors.resetGrass();
        this.biomeColors.resetFoliage();
        this.biomeColors.resetWater();
        this.spawnableLOTRAmbientList.clear();
        this.spawnableLOTRAmbientList.add(new BiomeGenBase.SpawnListEntry(LOTREntityMidges.class, 4, 2, 3));
        this.spawnableLOTRAmbientList.add(new BiomeGenBase.SpawnListEntry(LOTREntitySwan.class, 12, 4, 8));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFalathrimSailor.class, 10, 1, 3));
        this.spawnableEvilList.clear();
        this.decorator.waterlilyPerChunk = 7;
        this.decorator.canePerChunk = 10;
        this.decorator.reedPerChunk = 10;
        this.decorator.clearRandomStructures();
        this.invasionSpawns.clearInvasions();
        this.spawnableEvilList.clear();
        this.spawnableGoodList.clear();
        this.decorator.clearRandomStructures();
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.westBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterLisgardh;
    }

}
