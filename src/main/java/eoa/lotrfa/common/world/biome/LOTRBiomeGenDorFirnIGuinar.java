package eoa.lotrfa.common.world.biome;

import lotr.common.world.biome.LOTRBiomeGenErynVorn;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;

public class LOTRBiomeGenDorFirnIGuinar extends LOTRBiomeGenErynVorn {

    public LOTRBiomeGenDorFirnIGuinar(int i, boolean major) {
        super(i, major);
        this.spawnableEvilList.clear();
        this.decorator.generateOrcDungeon = false;
        this.decorator.flowersPerChunk = 8;
        this.decorator.doubleFlowersPerChunk = 2;
        this.decorator.doubleGrassPerChunk = 2;
        this.decorator.clearTrees();
        this.decorator.clearRandomStructures();

        this.decorator.addTree(LOTRTreeType.OAK, 1000);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.BIRCH_LARGE, 10);
        this.decorator.addTree(LOTRTreeType.SPRUCE, 200);
        this.decorator.addTree(LOTRTreeType.BEECH_LARGE, 2);
        this.decorator.addTree(LOTRTreeType.CHESTNUT, 100);
        this.decorator.addTree(LOTRTreeType.CHESTNUT_LARGE, 10);
        this.decorator.addTree(LOTRTreeType.ASPEN_LARGE, 5);
        this.decorator.addTree(LOTRTreeType.PINE, 200);
        this.decorator.addTree(LOTRTreeType.MALLORN, 200);
        this.decorator.addTree(LOTRTreeType.MALLORN_BOUGHS, 200);
        this.decorator.addTree(LOTRTreeType.BIRCH, 200);
        this.decorator.addTree(LOTRTreeType.BEECH, 200);
        this.decorator.addTree(LOTRTreeType.ASPEN, 200);
        this.decorator.addTree(LOTRTreeType.GREEN_OAK, 200);
        this.decorator.addTree(LOTRTreeType.APPLE, 5);
        this.decorator.addTree(LOTRTreeType.PEAR, 5);
        this.decorator.addTree(LOTRTreeType.PLUM, 5);
        this.decorator.addTree(LOTRTreeType.CHERRY, 2);

        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LIGHT, 0.4f);
        this.addBiomeVariant(LOTRBiomeVariant.SCRUBLAND, 0.4f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_ASPEN, 0.2f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BEECH, 0.2f);
        // TODO Auto-generated constructor stub
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRWaypoint.Region.LOTHLORIEN;
    }

}
