package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothOrcArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothWarg;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenBrethilEdge extends LOTRBiomeGenBrethil {

    public LOTRBiomeGenBrethilEdge(int i, boolean major) {
        super(i, major);
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothWarg.class, 10, 4, 10));
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhoth, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhothWarg, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.brethil, LOTREventSpawner.EventChance.COMMON);
        this.setBanditChance(LOTREventSpawner.EventChance.UNCOMMON);
        this.decorator.treesPerChunk = 5;

    }

    @Override
    protected void addFiefdomStructures() {
        // this.decorator.addRandomStructure(new LOTRWorldGenBlackrootWatchfort(false),
        // 1000);
        // this.decorator.addVillage(new LOTRVillageGenEdain(this,
        // LOTRWorldGenGondorStructure.GondorFiefdom.BLACKROOT_VALE, 0.6f));
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

}
