package eoa.lotrfa.common.world.biome;

import lotr.common.entity.npc.LOTREntityGaladhrimTrader;
import lotr.common.world.biome.LOTRBiomeGenLothlorien;

public class LOTRBiomeGenEresseaWoods extends LOTRBiomeGenLothlorien {

    public LOTRBiomeGenEresseaWoods(int i, boolean major) {
        super(i, major);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityGaladhrimTrader.class);
        // TODO Auto-generated constructor stub
    }

}
