package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianWarrior;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRAchievement;
import lotr.common.entity.animal.LOTREntityBear;
import lotr.common.entity.animal.LOTREntityHorse;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenBurntHouse;
import lotr.common.world.structure2.LOTRWorldGenRottenHouse;
import lotr.common.world.structure2.LOTRWorldGenRuinedHouse;
import lotr.common.world.structure2.LOTRWorldGenSmallStoneRuin;
import lotr.common.world.structure2.LOTRWorldGenStoneRuin;
import net.minecraft.world.biome.BiomeGenBase;

public class LOTRBiomeGenEastBeleriand extends LOTRBiomeGenGenericGrassland {
    public LOTRBiomeGenEastBeleriand(int i, boolean major) {
        super(i, major);
        this.spawnableCreatureList.add(new BiomeGenBase.SpawnListEntry(LOTREntityHorse.class, 6, 2, 6));
        this.spawnableCreatureList.add(new BiomeGenBase.SpawnListEntry(LOTREntityBear.class, 4, 1, 4));
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(200, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityFeanorianWarrior.class, 10, 1, 6)}));
        this.spawnableEvilList.clear();
        this.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(1000, new LOTRSpawnEntry[] { new LOTRSpawnEntry(LOTREntityUlfangWarrior.class, 10, 3, 6), new LOTRSpawnEntry(LOTREntityUlfangArcher.class, 5, 3, 6) }));
        this.decorator.generateOrcDungeon = false;
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenRuinedHouse(false), 2000);
        this.decorator.addRandomStructure(new LOTRWorldGenBurntHouse(false), 3000);
        this.decorator.addRandomStructure(new LOTRWorldGenRottenHouse(false), 3000);
        this.decorator.addRandomStructure(new LOTRWorldGenSmallStoneRuin(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenStoneRuin.STONE(1, 3), 800);
        this.invasionSpawns.clearInvasions();
        this.decorator.clearRandomStructures();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.feanorians, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseBor, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseUlfang, LOTREventSpawner.EventChance.COMMON);
        this.setBanditChance(LOTREventSpawner.EventChance.COMMON);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.eastBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterEastBeleriand;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.COBBLESTONE;
    }
}
