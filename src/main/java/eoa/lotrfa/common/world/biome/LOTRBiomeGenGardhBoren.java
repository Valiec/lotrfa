package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenGardhBorenCamp;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenGardhBorenWatchtower;
import lotr.common.LOTRAchievement;
import lotr.common.entity.animal.LOTREntityShirePony;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.LOTRBiomeGenDale;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.*;
import net.minecraft.world.biome.BiomeGenBase;

public class LOTRBiomeGenGardhBoren extends LOTRBiomeGenDale {

    public LOTRBiomeGenGardhBoren(int i, boolean major) {
        super(i, major);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(100, new LOTRSpawnEntry[] { new LOTRSpawnEntry(LOTREntityBorMan.class, 20, 2, 4), new LOTRSpawnEntry(LOTREntityBorLevyman.class, 10, 3, 8), new LOTRSpawnEntry(LOTREntityBorSoldier.class, 10, 3, 8), new LOTRSpawnEntry(LOTREntityBorArcher.class, 5, 3, 8) }));
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 12, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 12, 4, 6));
        this.spawnableCreatureList.add(new BiomeGenBase.SpawnListEntry(LOTREntityShirePony.class, 12, 2, 6));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFeanorianWarrior.class, 5, 2, 4));
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseUlfang, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, LOTREventSpawner.EventChance.RARE);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenRuinedHouse(false), 4000);
        this.decorator.addRandomStructure(new LOTRWorldGenBurntHouse(false), 4000);
        this.decorator.addRandomStructure(new LOTRWorldGenRottenHouse(false), 4000);
        this.decorator.addRandomStructure(new LOTRWorldGenStoneRuin.STONE(1, 4), 1000);
        this.decorator.addRandomStructure(new LOTRWorldGenGardhBorenWatchtower(false), 500);
        this.decorator.addRandomStructure(new LOTRWorldGenGardhBorenCamp(false), 1500);
        //this.decorator.addRandomStructure(new LOTRWorldGenGardhBorenFortress(false), 800);
        //this.decorator.addRandomStructure(new LOTRWorldGenDaleVillage(false), 400);
        //this.decorator.addRandomStructure(new LOTRWorldGenHouseBorTavern(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenSmallStoneRuin(false), 500);
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.gardhBoren;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterGardhBoren;
    }

}
