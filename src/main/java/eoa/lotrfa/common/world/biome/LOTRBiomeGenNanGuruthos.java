package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUtumnoRemnantCamp;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenUtumnoRemnantWargPit;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenNanCurunir;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRWorldGenBoulder;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class LOTRBiomeGenNanGuruthos extends LOTRBiomeGenNanCurunir {

    protected WorldGenerator boulderGen = new LOTRWorldGenBoulder(Blocks.packed_ice, 0, 2, 8);

    public LOTRBiomeGenNanGuruthos(int i, boolean major) {
        super(i, major);
        this.setEnableSnow();
        this.topBlock = Blocks.snow;
        this.fillerBlock = Blocks.dirt;
        this.addBiomeVariant(LOTRBiomeVariant.WASTELAND);
        this.addBiomeVariant(LOTRBiomeVariant.MOUNTAIN);
        this.addBiomeVariant(LOTRBiomeVariant.HILLS);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
        this.biomeColors.setGrass(0x6eab77);
        this.biomeColors.setFoliage(0x6eab77);
        this.biomeColors.setSky(0x304b5f);
        this.biomeColors.setClouds(0x334448);
        this.biomeColors.setFog(0x6d4c3c);
        this.biomeColors.setWater(0x273a38);
        this.decorator.treesPerChunk = 0;
        this.decorator.willowPerChunk = 0;
        this.decorator.flowersPerChunk = 0;
        this.decorator.grassPerChunk = 10;
        this.decorator.doubleGrassPerChunk = 6;
        this.spawnableGoodList.clear();
        this.spawnableLOTRAmbientList.clear();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaidelosOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityUtumnoRemnantOrc.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityUtumnoRemnantBerserker.class, 5, 1, 2));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaidelosSnowTroll.class, 5, 1, 3));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaidelosWarg.class, 13, 2, 3));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityUtumnoRemnantSpider.class, 8, 1, 2));
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenUtumnoRemnantCamp(false), 100);
        this.decorator.addRandomStructure(new LOTRWorldGenUtumnoRemnantWargPit(false), 300);
    }

    @Override
    public void decorate(World world, Random random, int i, int k) {
    super.decorate(world, random, i, k);
    if (random.nextInt(6) == 0) {
    int i1 = i + random.nextInt(16) + 8;
    int k1 = k + random.nextInt(16) + 8;
    this.boulderGen.generate(world, random, i1, world.getHeightValue(i1, k1), k1);
    }
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.nanGuruthos;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNanGuruthos;
    }

    protected boolean hasMordorSoils() {
        return false;
    }

}
