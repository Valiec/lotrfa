package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityHithlumWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenHithlumTower;
import lotr.common.LOTRAchievement;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenMithrimMountains extends LOTRBiomeGenEredWethrin {

	public LOTRBiomeGenMithrimMountains(int i, boolean major, int achieve) {
		super(i, major, achieve);
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityHithlumWarrior.class, 2, 4, 4));
        this.decorator.addRandomStructure(new LOTRWorldGenHithlumTower(false), 400);
        this.biomeColors.setFoggy(true);
        this.biomeColors.setGrass(0x3c7a39);
        this.biomeColors.setFoliage(0x31642e);
        this.biomeColors.setFog(0x678277);
        this.biomeColors.setWater(0x2a3850);
	}
	
    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.hithlum;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterMithrimMountains;
    }

}
