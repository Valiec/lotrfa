package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityNargothrondElf;
import eoa.lotrfa.common.entity.npc.LOTREntityNargothrondWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.LOTRAchievement;
import lotr.common.entity.animal.LOTREntityFlamingo;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenNargothrond extends LOTRBiomeGenTaurEnFaroth {
    public LOTRBiomeGenNargothrond(int i, boolean major) {
        super(i, major);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityNargothrondElf.class, 10, 4, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityNargothrondWarrior.class, 2, 4, 4));
        this.spawnableCreatureList.add(new LOTRSpawnEntry(LOTREntityFlamingo.class, 1, 1, 2));
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenNargothrondHouse(false), 50);
        this.decorator.addRandomStructure(new LOTRWorldGenNargothrondHall(false), 75);
        this.decorator.addRandomStructure(new LOTRWorldGenNargothrondForge(false), 100);
        this.decorator.addRandomStructure(new LOTRWorldGenNargothrondMerchantStall(false), 100);
        this.decorator.addRandomStructure(new LOTRWorldGenNargothrondMerchantStall(false), 100);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.nargothrond;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNargothrond;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.DORWINION;
    }

}
