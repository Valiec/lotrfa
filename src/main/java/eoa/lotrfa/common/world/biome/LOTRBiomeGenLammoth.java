package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenTaiga;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenLammoth extends LOTRBiomeGenTaiga {

    public LOTRBiomeGenLammoth(int i, boolean major) {
        super(i, major);
        this.setEnableSnow();
        this.spawnableGoodList.clear();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(1000, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDorDaidelosOrc.class, 20, 4, 6), new LOTRSpawnEntry(LOTREntityDorDaidelosOrcArcher.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityDorDaidelosWarg.class, 15, 2, 4)}));
        this.clearTravellingTraders();
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.hithlum, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.dorDaidelos, LOTREventSpawner.EventChance.UNCOMMON);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.lammoth;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterLammoth;
    }

}
