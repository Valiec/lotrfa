package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenHighElvenTower;

public class LOTRBiomeGenEredWethrin extends LOTRBiomeGenGenericMountains {

    public LOTRBiomeGenEredWethrin(int i, boolean major, int achieve) {
        super(i, major, achieve);
        this.biomeColors.setFoggy(true);
        this.biomeColors.setSky(0x65615d);
        this.biomeColors.setClouds(0x4b4747);
        this.biomeColors.setFog(0x4e473f);
        this.biomeColors.setWater(0x3b4150);
        this.biomeColors.setGrass(0x5fa35c);
        this.biomeColors.setFoliage(0x576a34);
        this.clearBiomeVariants();
        this.setGoodEvilWeight(50, 50);
        this.addBiomeVariant(LOTRBiomeVariant.MOUNTAIN);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenHighElvenTower(false), 1000);
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityHithlumWarrior.class, 2, 4, 4));
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothWarg.class, 6, 4, 4));
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
        this.clearTravellingTraders();
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.hithlum;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterEredWethrin;
    }

}