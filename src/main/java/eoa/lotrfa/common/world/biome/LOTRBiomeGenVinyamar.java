package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.LOTRAchievement;
import lotr.common.world.map.LOTRWaypoint;

public class LOTRBiomeGenVinyamar extends LOTRBiomeGenEredWethrinFoothills {

    public LOTRBiomeGenVinyamar(int i, boolean major) {
        super(i, major, i);
        this.spawnableGoodList.clear();
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenVinyamarHouse(false), 100);
        this.decorator.addRandomStructure(new LOTRWorldGenVinyamarHall(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenVinyamarForge(false), 200);
        this.clearTravellingTraders();
        this.invasionSpawns.clearInvasions();
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.hithlum;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterVinyamar;
    }
}
