package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenHithlumForge;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenHithlumHall;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTREventSpawner.EventChance;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenBaradEithel extends LOTRBiomeGenEredWethrin {

    public LOTRBiomeGenBaradEithel(int i, boolean major) {
        super(i, major, 0);
        this.biomeColors.setGrass(0x50b54a);
        this.setGoodEvilWeight(90, 10);
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityHithlumWarrior.class, 10, 4, 4));
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(100, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDorLominLevyman.class, 20, 2, 4), new LOTRSpawnEntry(LOTREntityDorLominSoldier.class, 10, 3, 8), new LOTRSpawnEntry(LOTREntityDorLominArcher.class, 5, 3, 8)}));
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LIGHT);
        this.decorator.addRandomStructure(new LOTRWorldGenHithlumForge(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenHithlumHall(false), 200);
        this.clearTravellingTraders();
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhoth, EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhothWarg, EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, EventChance.COMMON);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.hithlum;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterBaradEithel;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.HIGH_ELVEN_RUINED;
    }

}
