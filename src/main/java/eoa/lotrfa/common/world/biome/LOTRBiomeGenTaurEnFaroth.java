package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityNargothrondRanger;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenTaurEnFaroth extends LOTRBiomeGenGenericMountainFoothills {

    public LOTRBiomeGenTaurEnFaroth(int i, boolean major) {
        super(i, major, 0);
        this.clearBiomeVariants();
        this.addBiomeVariant(LOTRBiomeVariant.HILLS_FOREST);
        this.decorator.treesPerChunk = 10;
        this.decorator.flowersPerChunk = 4;
        this.decorator.doubleFlowersPerChunk = 1;
        this.decorator.doubleGrassPerChunk = 2;
        this.decorator.addTree(LOTRTreeType.PINE, 1000);
        this.decorator.addTree(LOTRTreeType.FIR, 200);
        this.decorator.addTree(LOTRTreeType.SPRUCE, 200);
        this.spawnableEvilList.clear();
        this.decorator.generateOrcDungeon = false;
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(5000, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityNargothrondRanger.class, 10, 1, 3)}));
        this.decorator.clearRandomStructures();
        this.clearTravellingTraders();
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.nargothrond;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterTaurEnFaroth;
    }

}
