package eoa.lotrfa.common.world.biome;

import java.util.Random;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaedelothOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaedelothOrcArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothOrcArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothWarg;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenDDCamp2;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothAltar;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothCamp;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothTower;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenTolInGaurhothWargPit;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.LOTRBiomeGenAnduinVale;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.feature.LOTRWorldGenBlastedLand;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenSmallStoneRuin;
import lotr.common.world.structure2.LOTRWorldGenStoneRuin;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRBiomeGenPassSirion extends LOTRBiomeGenAnduinVale {

    public LOTRBiomeGenPassSirion(int i, boolean major) {
        super(i, major);
        this.decorator.generateOrcDungeon = true;
        this.spawnableCreatureList.clear();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTolInGaurhothWarg.class, 10, 4, 10));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 10, 4, 6));
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LIGHT);
        this.addBiomeVariant(LOTRBiomeVariant.STEPPE);
        this.addBiomeVariant(LOTRBiomeVariant.STEPPE_BARREN);
        this.biomeColors.resetSky();
        this.biomeColors.setFoggy(true);
        this.biomeColors.setFog(0x689480);
        this.biomeColors.setGrass(0x4e682b);
        this.biomeColors.setSky(0x608181);
        this.biomeColors.setWater(0x2f503b);
        this.decorator.logsPerChunk = 2;
        this.decorator.flowersPerChunk = 0;
        this.decorator.doubleFlowersPerChunk = 0;
        this.decorator.grassPerChunk = 10;
        this.decorator.doubleGrassPerChunk = 6;
        this.decorator.addTree(LOTRTreeType.OAK_DEAD, 500);
        this.decorator.addTree(LOTRTreeType.SPRUCE_DEAD, 500);
        this.decorator.addTree(LOTRTreeType.BEECH_DEAD, 500);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenBlastedLand(false), 30);
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothCamp(false), 40);
        this.decorator.addRandomStructure(new LOTRWorldGenDDCamp2(false), 70);
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothAltar(false), 70);
        this.decorator.addRandomStructure(new LOTRWorldGenSmallStoneRuin(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothTower(false), 300);
        this.decorator.addRandomStructure(new LOTRWorldGenTolInGaurhothWargPit(false), 100);
        this.decorator.addRandomStructure(new LOTRWorldGenStoneRuin.HIGH_ELVEN(1, 4), 300);
        this.decorator.addRandomStructure(new LOTRWorldGenStoneRuin.DOL_GULDUR(1, 4), 100);
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhoth, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.tolInGaurhothWarg, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.hithlum, LOTREventSpawner.EventChance.UNCOMMON);
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        double d1 = biomeTerrainNoise.func_151601_a(i * 0.08, k * 0.08);
        double d2 = biomeTerrainNoise.func_151601_a(i * 0.7, k * 0.7);
        if (d1 + d2 > 0.1) {
            this.topBlock = Blocks.dirt;
            this.topBlockMeta = 1;
            this.fillerBlock = this.topBlock;
            this.fillerBlockMeta = this.topBlockMeta;
        }
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.passSirion;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterPassSirion;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.COBBLESTONE;
    }
    
    @Override
    public LOTRRoadType.BridgeType getBridgeBlock() {
        return LOTRRoadType.BridgeType.CHARRED;
    }

}
