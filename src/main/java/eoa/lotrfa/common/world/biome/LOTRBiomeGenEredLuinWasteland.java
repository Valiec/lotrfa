package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.entity.npc.LOTREntityBlueDwarfWarrior;
import lotr.common.world.biome.LOTRBiomeGenBlueMountainsFoothills;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.feature.LOTRWorldGenBlastedLand;
import lotr.common.world.spawning.*;

public class LOTRBiomeGenEredLuinWasteland extends LOTRBiomeGenBlueMountainsFoothills {

    public LOTRBiomeGenEredLuinWasteland(int i, boolean major) {
        super(i, major);
        this.biomeColors.setGrass(0x707644);
        this.spawnableCreatureList.clear();
        this.spawnableWaterCreatureList.clear();
        this.spawnableLOTRAmbientList.clear();
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityBlueDwarfWarrior.class, 20, 4, 4));
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothWarg.class, 6, 4, 4));
        this.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(1000, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityUlfangWarrior.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityUlfangArcher.class, 2, 4, 6)}));
        this.addBiomeVariant(LOTRBiomeVariant.DEADFOREST_SPRUCE);
        this.decorator.addTree(LOTRTreeType.SPRUCE_DEAD, 100);
        this.decorator.addTree(LOTRTreeType.CHARRED, 1000);
        this.decorator.treesPerChunk = 1;
        this.decorator.grassPerChunk = 6;
        this.clearTravellingTraders();
        this.setBanditChance(LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRInvasions.BLUE_MOUNTAINS, LOTREventSpawner.EventChance.COMMON);
        this.decorator.addRandomStructure(new LOTRWorldGenBlastedLand(false), 30);

    }
}
