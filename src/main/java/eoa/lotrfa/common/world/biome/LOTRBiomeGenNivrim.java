package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenDoriathCamp;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.world.biome.LOTRBiomeGenLothlorienEdge;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class LOTRBiomeGenNivrim extends LOTRBiomeGenLothlorienEdge {
    public LOTRBiomeGenNivrim(int i, boolean major) {
        super(i, major);
        this.flowers.clear();
        this.registerForestFlowers();
        this.biomeColors.resetGrass();
        this.biomeColors.resetFog();
        this.spawnableEvilList.clear();
        this.setGoodEvilWeight(100, 0);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(50, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDoriathElf.class, 10, 2, 4), new LOTRSpawnEntry(LOTREntityDoriathWarrior.class, 2, 1, 4), new LOTRSpawnEntry(LOTREntityDoriathWarden.class, 1, 1, 4)}));
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.doriath, LOTREventSpawner.EventChance.UNCOMMON);
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.GREEN_OAK, 200);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 1070);
        this.decorator.addTree(LOTRTreeType.RED_OAK, 30);
        this.decorator.addTree(LOTRTreeType.OAK, 50);
        //this.decorator.addTree(LOTRTreeType.OAK_LARGE, 70);
        this.decorator.addTree(LOTRTreeType.OAK_PARTY, 200);
        this.decorator.addTree(LOTRTreeType.SPRUCE_THIN, 5);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA, 2);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA_THIN, 2);
        this.addFlower(LOTRMod.elanor, 0, 20);
        this.addFlower(LOTRMod.niphredil, 0, 14);
        this.decorator.addOre(new WorldGenMinable(LOTRMod.oreQuendite, 6), 6.0f, 0, 48);
        this.decorator.generateAthelas = true;
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenDoriathCamp(false), 1500);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.doriath;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterDoriath;
    }
    
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.GALADHRIM;
    }
}
