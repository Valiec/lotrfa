package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenGondolinTower;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenWindMountains;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;

public class LOTRBiomeGenEchoriath extends LOTRBiomeGenWindMountains {

    public LOTRBiomeGenEchoriath(int i, boolean major) {
        super(i, major);
        this.spawnableGoodList.clear();
        this.spawnableEvilList.clear();
        this.decorator.generateOrcDungeon = false;
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenGondolinTower(false), 300);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.taurNuFuin;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterEchoriath;
    }

}
