package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;

public class LOTRBiomeGenRegion extends LOTRBiomeGenNivrim {
    public LOTRBiomeGenRegion(int i, boolean major) {
        super(i, major);
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.HOLLY_LARGE, 1000);
        this.decorator.addTree(LOTRTreeType.HOLLY, 300);
        this.decorator.addTree(LOTRTreeType.GREEN_OAK_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.RED_OAK, 60);
        this.decorator.addTree(LOTRTreeType.RED_OAK_LARGE, 60);
        this.decorator.addTree(LOTRTreeType.OAK, 80);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.SPRUCE, 10);
        this.decorator.addTree(LOTRTreeType.SPRUCE_THIN, 5);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA, 2);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA_THIN, 2);
        this.decorator.addTree(LOTRTreeType.CHESTNUT, 2);
        this.decorator.addTree(LOTRTreeType.CHESTNUT_LARGE, 5);
        this.decorator.addTree(LOTRTreeType.LARCH, 20);
        this.decorator.addTree(LOTRTreeType.FIR, 20);
        this.decorator.addTree(LOTRTreeType.PINE, 40);
        this.decorator.addTree(LOTRTreeType.ASPEN, 50);
        this.decorator.addTree(LOTRTreeType.ASPEN_LARGE, 10);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.doriath;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterDoriath;
    }

}
