package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.entity.npc.LOTREntityNargothrondWarrior;
import lotr.common.world.biome.LOTRBiomeGenRivendellHills;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenNargothrondHills extends LOTRBiomeGenRivendellHills {
    public LOTRBiomeGenNargothrondHills(int i, boolean major) {
        super(i, major);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(500, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityNargothrondWarrior.class, 10, 2, 4)}));
        this.spawnableEvilList.clear();
        this.decorator.generateOrcDungeon = false;
        this.invasionSpawns.clearInvasions();
    }

}
