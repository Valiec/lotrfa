package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRBiomeGenThargelionWoodland extends LOTRBiomeGenThargelion {

    public LOTRBiomeGenThargelionWoodland(int i, boolean major) {
        super(i, major);
        this.clearBiomeVariants();
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.CHARRED, 1000);
        this.decorator.addTree(LOTRTreeType.OAK, 200);
        this.decorator.addTree(LOTRTreeType.SPRUCE, 300);
        this.addBiomeVariant(LOTRBiomeVariant.WASTELAND);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LIGHT);
        this.addBiomeVariant(LOTRBiomeVariant.HILLS_FOREST);
        this.addBiomeVariant(LOTRBiomeVariant.DEADFOREST_OAK);
        this.decorator.whiteSand = false;
        this.decorator.clearRandomStructures();
        this.spawnableGoodList.clear();
        this.spawnableEvilList.clear();
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseUlfang, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.feanorians, LOTREventSpawner.EventChance.RARE);
        this.setBanditChance(LOTREventSpawner.EventChance.COMMON);
        this.clearTravellingTraders();
        // this.biomeColors.setGrass(10708034);
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        double d1 = biomeTerrainNoise.func_151601_a(i * 0.08, k * 0.08);
        double d2 = biomeTerrainNoise.func_151601_a(i * 0.7, k * 0.7);
        if (d1 + d2 > 0.1) {
            this.topBlock = Blocks.dirt;
            this.topBlockMeta = 1;
            this.fillerBlock = this.topBlock;
            this.fillerBlockMeta = this.topBlockMeta;
        }
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    public LOTRBiome.GrassBlockAndMeta getRandomGrass(Random random) {
        if (random.nextInt(3) == 0) {
            return new LOTRBiome.GrassBlockAndMeta(Blocks.tallgrass, 1);
        }
        return new LOTRBiome.GrassBlockAndMeta(LOTRMod.tallGrass, 0);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRWaypoint.Region.WILDERLAND;
    }

}
