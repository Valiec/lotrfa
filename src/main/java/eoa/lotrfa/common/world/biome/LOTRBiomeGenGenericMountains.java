package eoa.lotrfa.common.world.biome;

import java.util.Random;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.spawning.LOTREventSpawner;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRBiomeGenGenericMountains extends LOTRBiome {

    public int biomeAchieveId;

    public LOTRBiomeGenGenericMountains(int i, boolean major, int achieve) {
        super(i, major);
        this.addBiomeVariantSet(LOTRBiomeVariant.SET_MOUNTAINS);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LARCH, 0.2F);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_PINE, 0.2F);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_ASPEN, 0.2F);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_MAPLE, 0.2F);
        this.decorator.biomeGemFactor = 1.0F;
        this.decorator.flowersPerChunk = 0;
        this.decorator.grassPerChunk = 0;
        this.decorator.doubleGrassPerChunk = 0;
        this.decorator.addTree(LOTRTreeType.SPRUCE, 400);
        this.decorator.addTree(LOTRTreeType.SPRUCE_THIN, 400);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA, 50);
        this.decorator.addTree(LOTRTreeType.SPRUCE_MEGA_THIN, 10);
        this.decorator.addTree(LOTRTreeType.LARCH, 500);
        this.decorator.addTree(LOTRTreeType.FIR, 500);
        this.decorator.addTree(LOTRTreeType.PINE, 500);
        this.registerMountainsFlowers();
        this.biomeColors.setSky(10862798);
        this.decorator.generateOrcDungeon = false;
        this.spawnableEvilList.clear();
        this.invasionSpawns.clearInvasions();
        this.decorator.clearRandomStructures();
        this.biomeAchieveId = achieve;
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
    }
    
    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.GREY_MOUNTAINS.getSubregion("greyMountains");
    }
    
    @Override
    public boolean getEnableRiver() {
        return false;
    }

    //@Override
    //public LOTRAchievement getBiomeAchievement() {
    //    LOTRAchievement[] biomeAchieves = {LOTRFAAchievements.enterAndram, LOTRFAAchievements.enterNevrast, LOTRFAAchievements.enterEredLomin};
    //    return biomeAchieves[this.biomeAchieveId];
    //}

    //@Override
    //public LOTRWaypoint.Region getBiomeWaypoints() {
    //    LOTRWaypoint.Region[] biomeRegions = {LOTRWaypoint.Region.MISTY_MOUNTAINS, LOTRWaypoint.Region.ERIADOR, LOTRWaypoint.Region.MISTY_MOUNTAINS, LOTRWaypoint.Region.MISTY_MOUNTAINS, LOTRWaypoint.Region.ERIADOR};
    //    return biomeRegions[this.biomeAchieveId];
    //}
    
    @Override
    protected void generateMountainTerrain(final World world, final Random random, final Block[] blocks, final byte[] meta, final int i, final int k, final int xzIndex, final int ySize, final int height, final int rockDepth, final LOTRBiomeVariant variant) {
        final int snowHeight = 150 - rockDepth;
        for (int stoneHeight = snowHeight - 40, j = ySize - 1; j >= stoneHeight; --j) {
            final int index = xzIndex * ySize + j;
            final Block block = blocks[index];
            if (j >= snowHeight && block == this.topBlock) {
                blocks[index] = Blocks.snow;
                meta[index] = 0;
            }
            else if (block == this.topBlock || block == this.fillerBlock) {
                blocks[index] = Blocks.stone;
                meta[index] = 0;
            }
        }
    }
    
    @Override
    public void decorate(final World world, final Random random, final int i, final int k) {
        super.decorate(world, random, i, k);
        for (int count = 0; count < 3; ++count) {
            final int i2 = i + random.nextInt(16) + 8;
            final int k2 = k + random.nextInt(16) + 8;
            final int j1 = world.getHeightValue(i2, k2);
            if (j1 < 100) {
                this.decorator.genTree(world, random, i2, j1, k2);
            }
        }
        for (int count = 0; count < 2; ++count) {
            final int i2 = i + random.nextInt(16) + 8;
            final int j2 = random.nextInt(128);
            final int k3 = k + random.nextInt(16) + 8;
            if (j2 < 100) {
                this.getRandomWorldGenForGrass(random).generate(world, random, i2, j2, k3);
            }
        }
    }
    
    @Override
    public float getChanceToSpawnAnimals() {
        return 0.0f;
    }
    
    @Override
    public float getTreeIncreaseChance() {
        return 0.0f;
    }

}
