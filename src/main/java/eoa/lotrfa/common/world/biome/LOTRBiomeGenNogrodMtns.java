package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenBlueMountains;
import lotr.common.world.map.LOTRRoadType;

public class LOTRBiomeGenNogrodMtns extends LOTRBiomeGenBlueMountains {

    public LOTRBiomeGenNogrodMtns(int i, boolean major) {
        super(i, major);
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.DWARVEN;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNogrodMtns;
    }

}
