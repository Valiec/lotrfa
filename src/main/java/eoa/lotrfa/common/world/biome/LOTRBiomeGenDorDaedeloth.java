package eoa.lotrfa.common.world.biome;

import java.util.Random;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.*;
import eoa.lotrfa.common.world.structure.nature.LOTRWorldGenDDBlastedLand;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiomeGenMordor;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.feature.LOTRWorldGenBoulder;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenStoneRuin;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.gen.NoiseGeneratorPerlin;

public class LOTRBiomeGenDorDaedeloth extends LOTRBiomeGenMordor {

    private NoiseGeneratorPerlin noiseDirt = new NoiseGeneratorPerlin(new Random(42956029606L), 1);
    private NoiseGeneratorPerlin noiseGravel = new NoiseGeneratorPerlin(new Random(7185609602367L), 1);
    private NoiseGeneratorPerlin noiseMordorGravel = new NoiseGeneratorPerlin(new Random(12480634985056L), 1);
    protected boolean enableMordorBoulders;

    public LOTRBiomeGenDorDaedeloth(int i, boolean major) {
        super(i, major);
        // this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityMiniBalrog.class, 6, 1, 2));

        this.setDisableRain();
        this.boulderGen = new LOTRWorldGenBoulder(LOTRMod.rock, 0, 1, 2);
        this.enableMordorBoulders = true;
        this.topBlock = LOTRFABlocks.anfauglithAsh;
        this.topBlockMeta = 0;
        this.fillerBlock = LOTRMod.rock;
        this.fillerBlockMeta = 0;
        this.biomeColors.setFoggy(true);
        this.decorator.grassPerChunk = 4;
        this.decorator.doubleGrassPerChunk = 2;
        this.decorator.clearTrees();
        this.decorator.treesPerChunk = 0;
        this.decorator.willowPerChunk = 0;
        this.clearBiomeVariants();
        this.addBiomeVariant(LOTRBiomeVariant.HILLS);
        this.decorator.clearTrees();
        this.decorator.addTree(LOTRTreeType.CHARRED, 10);
        this.biomeColors.setFoliage(0x657c4f);
        this.biomeColors.setGrass(0x657c4f);
        this.biomeColors.setSky(5523773);
        this.biomeColors.setClouds(3355443);
        this.biomeColors.setFog(0x4e473f);
        this.biomeColors.setWater(2498845);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenDDWatchtower(false), 300);
        this.decorator.addRandomStructure(new LOTRWorldGenStoneRuin.MORDOR(1, 4), 40);
        this.decorator.addRandomStructure(new LOTRWorldGenDDBlastedLand(false), 20);
        this.decorator.addRandomStructure(new LOTRWorldGenDDWargPit(false), 200); // !
        this.decorator.addRandomStructure(new LOTRWorldGenDDCamp(false), 30); // !
        this.spawnableCreatureList.clear();
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcBombardier.class, 5, 1, 2));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothWarg.class, 30, 4, 4));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityAngbandOrc.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityAngbandOrcArcher.class, 5, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityTormentedElfSmith.class, 5, 2, 3));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothTroll.class, 15, 1, 3));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothMountainTroll.class, 10, 1, 3));
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, LOTREventSpawner.EventChance.COMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angband, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandWarg, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.hithlum, LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.ardGalen;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterDorDaedeloth;
    }

    @Override
    public void generateBiomeTerrain(World world, Random random, Block[] blocks, byte[] meta, int i, int k, double stoneNoise, int height, LOTRBiomeVariant variant) {
        Block topBlock_pre = this.topBlock;
        int topBlockMeta_pre = this.topBlockMeta;
        Block fillerBlock_pre = this.fillerBlock;
        int fillerBlockMeta_pre = this.fillerBlockMeta;
        double d1 = this.noiseDirt.func_151601_a(i * 0.09, k * 0.09);
        double d2 = this.noiseDirt.func_151601_a(i * 0.6, k * 0.6);
        double d3 = this.noiseGravel.func_151601_a(i * 0.09, k * 0.09);
        double d4 = this.noiseGravel.func_151601_a(i * 0.6, k * 0.6);
        double d5 = this.noiseMordorGravel.func_151601_a(i * 0.09, k * 0.09);
        double d6 = this.noiseMordorGravel.func_151601_a(i * 0.6, k * 0.6);
        
        if (d5 + d6 > 0.5) {
            this.topBlock = LOTRMod.mordorGravel;
            this.topBlockMeta = 0;
        }
        else if (d3 + d4 > 0.96) {
            this.topBlock = LOTRMod.mordorDirt;
            this.topBlockMeta = 0;
        }
        else if (d1 + d2 > 0.7) {
            this.topBlock = LOTRMod.rock;
            this.topBlockMeta = 0;
        }
        super.generateBiomeTerrain(world, random, blocks, meta, i, k, stoneNoise, height, variant);
        this.topBlock = topBlock_pre;
        this.topBlockMeta = topBlockMeta_pre;
        this.fillerBlock = fillerBlock_pre;
        this.fillerBlockMeta = fillerBlockMeta_pre;
    }

    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.ANGMAR.getSubregion("angmar");
    }

    @Override
    public boolean canSpawnHostilesInDay() {
        return true;
    }

}
