package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenArthorien extends LOTRBiomeGenRegion {

    public LOTRBiomeGenArthorien(int i, boolean major) {
        super(i, major);
        this.spawnableGoodList.clear();
        this.spawnableGoodList.addAll(LOTRSpawnEntry.chanceList(50, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDoriathWarrior.class, 2, 1, 4), new LOTRSpawnEntry(LOTREntityDoriathWarden.class, 1, 1, 4)}));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityLaegrimElf.class, 5, 2, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityLaegrimScout.class, 1, 2, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityLaegrimWarrior.class, 1, 1, 2));
        this.decorator.addTree(LOTRTreeType.GREEN_OAK_LARGE, 10);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenLaegrimHouse(false), 16);
        this.decorator.addRandomStructure(new LOTRWorldGenLaegrimHideout(false), 40);
        this.decorator.addRandomStructure(new LOTRWorldGenLaegrimForge(false), 80);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.laegrim, LOTREventSpawner.EventChance.UNCOMMON);

    }
}
