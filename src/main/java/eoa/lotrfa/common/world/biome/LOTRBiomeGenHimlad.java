package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianElf;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianWarrior;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.*;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRMusicRegion;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.feature.LOTRWorldGenBlastedLand;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenRuinedHouse;

public class LOTRBiomeGenHimlad extends LOTRBiome {

    public LOTRBiomeGenHimlad(int i, boolean major) {
        super(i, major);
        this.spawnableLOTRAmbientList.clear();
        this.spawnableEvilList.clear();
        this.spawnableGoodList.clear();
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFeanorianElf.class, 10, 4, 4));
        this.spawnableGoodList.add(new LOTRSpawnEntry(LOTREntityFeanorianWarrior.class, 2, 4, 4));
        this.addBiomeVariant(LOTRBiomeVariant.FLOWERS);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LIGHT);
        this.addBiomeVariant(LOTRBiomeVariant.STEPPE);
        this.addBiomeVariant(LOTRBiomeVariant.HILLS);
        this.addBiomeVariant(LOTRBiomeVariant.HILLS_FOREST);
        this.addBiomeVariant(LOTRBiomeVariant.DENSEFOREST_OAK);
        this.addBiomeVariant(LOTRBiomeVariant.DENSEFOREST_BIRCH);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BEECH, 0.2f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BIRCH, 0.5f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_ASPEN, 0.5f);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_APPLE_PEAR, 1.0f);
        this.decorator.setTreeCluster(10, 20);
        this.decorator.treesPerChunk = 0;
        this.decorator.willowPerChunk = 1;
        this.decorator.flowersPerChunk = 3;
        this.decorator.grassPerChunk = 8;
        this.decorator.doubleGrassPerChunk = 1;
        this.decorator.addTree(LOTRTreeType.OAK, 100);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 25);
        this.decorator.addTree(LOTRTreeType.BIRCH, 500);
        this.decorator.addTree(LOTRTreeType.BIRCH_TALL, 500);
        this.decorator.addTree(LOTRTreeType.BIRCH_LARGE, 200);
        this.decorator.addTree(LOTRTreeType.BIRCH_PARTY, 50);
        this.decorator.addTree(LOTRTreeType.BEECH, 100);
        this.decorator.addTree(LOTRTreeType.BEECH_LARGE, 25);
        this.decorator.addTree(LOTRTreeType.CHESTNUT, 40);
        this.decorator.addTree(LOTRTreeType.CHESTNUT_LARGE, 10);
        this.decorator.addTree(LOTRTreeType.ASPEN, 300);
        this.decorator.addTree(LOTRTreeType.ASPEN_LARGE, 100);
        this.decorator.addTree(LOTRTreeType.APPLE, 2);
        this.decorator.addTree(LOTRTreeType.PEAR, 2);
        this.registerPlainsFlowers();
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenFeanorianHouse(false), 200);
        this.decorator.addRandomStructure(new LOTRWorldGenFeanorianHall(false), 400);
        this.decorator.addRandomStructure(new LOTRWorldGenFeanorianStable(false), 600);
        this.decorator.addRandomStructure(new LOTRWorldGenFeanorianForge(false), 300);
        this.decorator.addRandomStructure(new LOTRWorldGenFeanorianTower(false), 600);
        this.decorator.addRandomStructure(new LOTRWorldGenFeanorianCamp(false), 600);
        this.decorator.addRandomStructure(new LOTRWorldGenBlastedLand(false), 800);
        this.decorator.addRandomStructure(new LOTRWorldGenRuinedHouse(false), 300);
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseUlfang, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, LOTREventSpawner.EventChance.RARE);
        this.setBanditChance(LOTREventSpawner.EventChance.RARE);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterHimlad;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.feanor;
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.COBBLESTONE;
    }

    @Override
    public LOTRMusicRegion.Sub getBiomeMusic() {
        return LOTRMusicRegion.LINDON.getSubregion("lindon");
    }

}
