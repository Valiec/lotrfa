package eoa.lotrfa.common.world.biome;

import lotr.common.world.biome.LOTRBiomeGenFangornClearing;
import lotr.common.world.biome.variant.LOTRBiomeVariant;

public class LOTRBiomeGenEntwifeGardens extends LOTRBiomeGenFangornClearing {

    public LOTRBiomeGenEntwifeGardens(int i, boolean major) {
        super(i, major);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_PLUM);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_APPLE_PEAR);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_LEMON);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_ORANGE);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_OLIVE);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_DATE);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_LIME);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_POMEGRANATE);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_ALMOND);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_LIGHT);
        this.decorator.flowersPerChunk = 16;
        this.decorator.logsPerChunk = 0;
        this.decorator.doubleFlowersPerChunk = 3;
        this.decorator.grassPerChunk = 12;
        this.decorator.doubleGrassPerChunk = 3;
        this.registerPlainsFlowers();
        this.decorator.clearTrees();
        // TODO Auto-generated constructor stub
    }

}
