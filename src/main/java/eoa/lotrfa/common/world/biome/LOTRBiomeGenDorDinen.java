package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;

public class LOTRBiomeGenDorDinen extends LOTRBiomeGenGenericGrassland{

	public LOTRBiomeGenDorDinen(int i, boolean major) {
		super(i, major);
        this.setBanditChance(LOTREventSpawner.EventChance.UNCOMMON);
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
	}
	
    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.eastBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterDorDinen;
    }
	
    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

}
