package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenAngbandTower;
import eoa.lotrfa.common.world.structure.nature.LOTRWorldGenAngbandBoulder;
import eoa.lotrfa.common.world.structure.nature.LOTRWorldGenDDBlastedLand;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiomeGenMordorMountains;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRSpawnEntry;

public class LOTRBiomeGenThangorodrim extends LOTRBiomeGenMordorMountains {

    public LOTRBiomeGenThangorodrim(int i, boolean major) {
        super(i, major);
        this.boulderGen = new LOTRWorldGenAngbandBoulder(LOTRMod.rock, 0, 2, 8);
        this.biomeColors.setFog(0x600e00);
        this.biomeColors.setFoggy(true);
        this.spawnableEvilList.clear();
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenAngbandTower(false), 300);
        this.decorator.addRandomStructure(new LOTRWorldGenDDBlastedLand(false), 40);
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityAngbandOrc.class, 20, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityAngbandOrcArcher.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityAngbandWarg.class, 20, 1, 3));
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.angband;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterAngband;
    }

}
