package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.nature.LOTRWorldGenDDBlastedLand;
import lotr.common.LOTRAchievement;
import lotr.common.world.biome.LOTRBiomeGenEttenmoors;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenSmallStoneRuin;
import net.minecraft.world.gen.NoiseGeneratorPerlin;

public class LOTRBiomeGenNanDuaith extends LOTRBiomeGenEttenmoors {

    protected static NoiseGeneratorPerlin noiseSnow;

    public LOTRBiomeGenNanDuaith(int i, boolean major) {
        super(i, major);
        this.setEnableSnow();
        this.biomeColors.setFoggy(true);
        this.biomeColors.setSky(5523773);
        this.biomeColors.setClouds(3355443);
        this.biomeColors.setFog(0x4e473f);
        this.biomeColors.setWater(2498845);
        this.biomeColors.setFoliage(0x657c4f);
        this.biomeColors.setGrass(0x657c4f);
        this.decorator.clearRandomStructures();
        this.decorator.addRandomStructure(new LOTRWorldGenDDBlastedLand(false), 40);
        this.decorator.addRandomStructure(new LOTRWorldGenSmallStoneRuin(false), 400);
        this.setBanditChance(LOTREventSpawner.EventChance.NEVER);
        this.clearTravellingTraders();
        this.spawnableEvilList.clear();
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothTroll.class, 30, 1, 3));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothMountainTroll.class, 20, 1, 3));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrc.class, 10, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 5, 4, 6));
        this.spawnableEvilList.add(new LOTRSpawnEntry(LOTREntityDorDaedelothWarg.class, 5, 2, 6));
        this.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(300, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDorDaidelosOrc.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityDorDaidelosOrcArcher.class, 5, 4, 6)}));
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.dorDaidelos, LOTREventSpawner.EventChance.RARE);
    }

    @Override
    public boolean canSpawnHostilesInDay() {
        return true;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.nanDuaith;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterNanDuaith;
    }

}
