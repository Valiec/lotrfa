package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.entity.npc.LOTREntityDorDaidelosOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaidelosOrcArcher;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.builds.LOTRWorldGenDorDaidelosCamp;
import lotr.common.LOTRDimension;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.LOTRBiomeGenAnduinVale;
import lotr.common.world.spawning.LOTREventSpawner;
import lotr.common.world.spawning.LOTRSpawnEntry;
import lotr.common.world.structure2.LOTRWorldGenBlueMountainsSmithy;
import lotr.common.world.structure2.LOTRWorldGenSmallStoneRuin;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class LOTRFABiomes {

    public static LOTRBiome nargothrond;
    public static LOTRBiome nargothrondPlateau;
    public static LOTRBiome baradEithel;
    public static LOTRBiome arthorien;
    public static LOTRBiome tumladen;
    public static LOTRBiome hithlum;
    public static LOTRBiome mithrim;
    public static LOTRBiome beleriandWest;
    public static LOTRBiome beleriandEast;
    public static LOTRBiome talathDirnen;
    public static LOTRBiome beleriandDowns;
    public static LOTRBiome doriath;
    public static LOTRBiome taurNuFuin;
    public static LOTRBiome taurNuFuinFoothills;
    public static LOTRBiome nanDungortheb;
    public static LOTRBiome baradNimras;
    public static LOTRBiome taurEnFaroth;
    public static LOTRBiome dryRiver;
    public static LOTRBiome ossiriand;
    public static LOTRBiome ossiriandWoodlands;
    public static LOTRBiome neldoreth;
    public static LOTRBiome region;
    public static LOTRBiome belegost;
    public static LOTRBiome helcaraxe;
    public static LOTRBiome ivrin;
    public static LOTRBiome nanTathren;
    public static LOTRBiome vinyamar;
    public static LOTRBiome tolInGaurhoth;
    public static LOTRBiome nanElmoth;
    public static LOTRBiome theSea;
    public static LOTRBiome echoriath;
    public static LOTRBiome eredWethrin;
    public static LOTRBiome eredLomin;
    public static LOTRBiome eredNevrast;
    public static LOTRBiome andram;
    public static LOTRBiome eredWethrinFoothills;
    public static LOTRBiome eredLominFoothills;
    public static LOTRBiome eredNevrastFoothills;
    public static LOTRBiome andramMountains;
    public static LOTRBiome amonRudh;
    public static LOTRBiome nibinNoeg;
    public static LOTRBiome taurImDuinath;
    public static LOTRBiome brethil;
    public static LOTRBiome brethilEdge;
    public static LOTRBiome himlad;
    public static LOTRBiome dorLomin;
    public static LOTRBiome angband;
    public static LOTRBiome fenSerech;
    public static LOTRBiome nanGuruthos;
    public static LOTRBiome lammoth;
    public static LOTRBiome angbandMountains;
    public static LOTRBiome dorDaedeloth;
    public static LOTRBiome nimbrethil;
    public static LOTRBiome thangorodrim;
    public static LOTRBiome forodwaithMtns;
    public static LOTRBiome himringMtns;
    public static LOTRBiome himringFoothills;
    public static LOTRBiome falas;
    public static LOTRBiome lisgardh;
    public static LOTRBiome estolad;
    public static LOTRBiome gardhBoren;
    public static LOTRBiome balar;
    public static LOTRBiome balarHills;
    public static LOTRBiome taurdru;
    public static LOTRBiome aelinUial;
    public static LOTRBiome taurNuFuinMtns;
    public static LOTRBiome passSirion;
    public static LOTRBiome thargelion;
    public static LOTRBiome thargelionWoodland;
    public static LOTRBiome woodsNuath;
    public static LOTRBiome ladrosRuins;
    public static LOTRBiome anfauglith;
    public static LOTRBiome forodwaithFoothills;
    public static LOTRBiome nanDuaith;
    public static LOTRBiome lothlannWasteland;
    public static LOTRBiome dimbar;
    public static LOTRBiome dorDinen;
    public static LOTRBiome tumhalad;
    public static LOTRBiome eredEngrin;
    public static LOTRBiome eredEngrinFoothills;
    public static LOTRBiome beleriandSouthWoodlands;
    public static LOTRBiome sirionVale;
    public static LOTRBiome arvernien;
    public static LOTRBiome eredLuinWasteland;
    public static LOTRBiome nogrod;
    public static LOTRBiome eredGorgoroth;
    public static LOTRBiome mithrimMountains;
    public static LOTRBiome marchMaedhros;
    public static LOTRBiome amonEreb;
    public static LOTRBiome girdleMelian;
    public static LOTRBiome falasHills;
    public static LOTRBiome nevrast;
    public static LOTRBiome nevrastMarshes;
    public static LOTRBiome highFaroth;
    public static LOTRBiome ramdal;
    public static LOTRBiome tolGalen;
    public static LOTRBiome beleriandSouth;
    public static LOTRBiome frozenTundra;
    
    public static LOTRBiome locked;
    public static LOTRBiome locked1;
    public static LOTRBiome locked2;
    public static LOTRBiome locked3;
    public static LOTRBiome locked4;
    public static LOTRBiome locked5;
    public static LOTRBiome locked6;
    public static LOTRBiome locked7;
    public static LOTRBiome locked8;
    public static LOTRBiome locked9;
    public static LOTRBiome locked10;
    public static LOTRBiome locked11;
    public static LOTRBiome locked12;
    public static LOTRBiome locked13;
    public static LOTRBiome locked14;
    public static LOTRBiome locked15;
    public static LOTRBiome locked16;
    public static LOTRBiome locked17;
    public static LOTRBiome locked18;
    public static LOTRBiome locked19;

    //TODO cleanup bottom of this class
    /*
     * Unused Biome IDs. If you use an ID, please remove it from the list: 1, 2, 3,
     * 4, 5, 8, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 28, 29, 30,
     * 32, 33, 34, 35, 36, 37, 38, 39, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 53,
     * 55, 56, 58, 59, 60, 61, 62, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76,
     * 78, 79, 80, 81, 82, 83, 85, 86, 90, 91, 93, 94, 95, 96, 97, 98, 100, 101,
     * 104, 105, 108, 109, 110, 112, 114, 115, 116, 117, 118, 119, 120, 121, 122,
     * 123, 124, 125, 126, 127, 128, 131, 132, 133, 134, 135, 136, 137, 138, 139,
     * 140
     * 
     */

    public static void setupBiomes() {
        LOTRBiome.eregion.decorator.clearRandomStructures();
        LOTRBiome.eregion.decorator.addRandomStructure(new LOTRWorldGenSmallStoneRuin(false), 300);
        int[] overrides = {105, 51, 15, 179, 48, 102, 6, 142, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 180, 181, 182, 183, 234, 184, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 111, 232, 233, 236, 237, 238, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 145, 149, 10, 7, 20, 54, 9, 103, 185, 146, 107, 235, 106, 99, 239, 199, 130, 166, 164, 165, 163, 162, 161, 160, 159, 158, 157, 156, 155, 152, 151, 150, 148, 147, 143, 153, 154, 141};
        for (int i : overrides) {
            LOTRDimension.MIDDLE_EARTH.biomeList[i] = null;
        }

        LOTRFABiomes.angbandMountains = new LOTRBiomeGenAngbandMountains(6, true).setTemperatureRainfall(2.0F, 0.0F).setMinMaxHeight(2.0F, 3.0F).setColor(7630194).setBiomeName("angbandMountains");
        LOTRFABiomes.aelinUial = new LOTRBiomeGenAelinUial(10, true).setTemperatureRainfall(0.8f, 1.0f).setMinMaxHeight(0.0f, 0.1f).setColor(0x319c5c).setBiomeName("aelinUial");
        LOTRFABiomes.taurNuFuinFoothills = new LOTRBiomeGenTaurNuFuinFoothills(15, true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.82f, 0.95f).setColor(0x242f25).setBiomeName("taurNuFuinHills");
        LOTRFABiomes.arthorien = new LOTRBiomeGenArthorien(20, true).setTemperatureRainfall(0.7F, 0.7F).setMinMaxHeight(0.2F, 0.4F).setColor(0x395524).setBiomeName("arthorien");
        LOTRFABiomes.nanDuaith = new LOTRBiomeGenNanDuaith(99, true).setTemperatureRainfall(0.2f, 0.6f).setMinMaxHeight(0.5f, 0.9f).setColor(0x858a79).setBiomeName("nanDuaith");
        LOTRFABiomes.forodwaithFoothills = new LOTRBiomeGenForodwaithMtns(102, true).setTemperatureRainfall(0.0f, 0.2f).setMinMaxHeight(0.5f, 0.9f).setColor(0xcecfcf).setBiomeName("forodwaithFoothills");
        LOTRFABiomes.ladrosRuins = new LOTRBiomeGenLadrosRuins(103, true).setTemperatureRainfall(0.6f, 0.4f).setMinMaxHeight(0.6f, 0.9f).setColor(0x394a39).setBiomeName("ladrosRuins");
        LOTRFABiomes.eredLuinWasteland = new LOTRBiomeGenEredLuinWasteland(105, true).setTemperatureRainfall(0.7f, 0.4f).setMinMaxHeight(0.5f, 0.9f).setColor(0x9eaca7).setBiomeName("eredLuinWasteland");
        LOTRFABiomes.woodsNuath = new LOTRBiomeGenNuath(106, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.2f, 0.5f).setColor(0x498025).setBiomeName("woodsNuath");
        LOTRFABiomes.nibinNoeg = new LOTRBiomeGenAmonRudh(111, true).setTemperatureRainfall(0.9f, 1.1f).setMinMaxHeight(0.2f, 0.5f).setColor(0x8f9d84).setBiomeName("nibinNoeg");
        LOTRFABiomes.eredGorgoroth = (new LOTRBiomeGenEredGorgoroth(130, true)).setTemperatureRainfall(0.28F, 0.9F).setMinMaxHeight(1.82F, 2.05F).setColor(0x606c5e).setBiomeName("eredGorgoroth");
        LOTRFABiomes.sirionVale = new LOTRBiomeGenAnduinVale(141, true).setTemperatureRainfall(0.9f, 1.1f).setMinMaxHeight(0.05f, 0.05f).setColor(0x60a543).setBiomeName("sirionVale");
        LOTRFABiomes.lammoth = new LOTRBiomeGenLammoth(142, true).setTemperatureRainfall(0.1f, 0.7f).setMinMaxHeight(0.1f, 0.5f).setColor(0x79a568).setBiomeName("lammoth");
        LOTRFABiomes.nogrod = new LOTRBiomeGenNogrodMtns(143, true).setTemperatureRainfall(0.3f, 0.4f).setMinMaxHeight(1.0f, 2.5f).setColor(0xc7d3d6).setBiomeName("nogrod");
        LOTRFABiomes.taurNuFuinMtns = new LOTRBiomeGenTaurNuFuinMtns(145, true).setTemperatureRainfall(0.28f, 0.9f).setMinMaxHeight(1.2f, 1.5f).setColor(0x485248).setBiomeName("taurNuFuinMtns");
        LOTRFABiomes.thargelion = new LOTRBiomeGenThargelion(146, true).setTemperatureRainfall(0.6f, 0.4f).setMinMaxHeight(0.15f, 0.2f).setColor(0x81ad55).setBiomeName("thargelion");
        LOTRFABiomes.lothlannWasteland = new LOTRBiomeGenLothlannWasteland(147, true).setTemperatureRainfall(1.1f, 1.1f).setMinMaxHeight(0.1f, 0.05f).setColor(0x606a4d).setBiomeName("lothlannWasteland");
        LOTRFABiomes.tumhalad = new LOTRBiomeGenWestBeleriand(148, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.4f).setColor(0x6ead2f).setBiomeName("tumhalad");
        LOTRFABiomes.passSirion = new LOTRBiomeGenPassSirion(149, true).setTemperatureRainfall(0.9f, 1.1f).setMinMaxHeight(0.05f, 0.05f).setColor(0x717f48).setBiomeName("passSirion");
        LOTRFABiomes.frozenTundra = new LOTRBiomeGenFrozenTundra(150, true).setTemperatureRainfall(0.1f, 0.3f).setMinMaxHeight(0.1f, 0.2f).setColor(0xd3cfb6).setBiomeName("frozenTundra");
        LOTRFABiomes.tolGalen = new LOTRBiomeGenOssiriandWoodlands(151, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.2f, 0.3f).setColor(0x255e2f).setBiomeName("tolGalen");
        LOTRFABiomes.arvernien = new LOTRBiomeGenWestBeleriand(152, true).setTemperatureRainfall(0.9f, 0.9f).setMinMaxHeight(0.07f, 0.2f).setColor(0x61a51b).setBiomeName("arvernien");
        LOTRFABiomes.eredEngrinFoothills = new LOTRBiomeGenEredEngrinFoothills(153, true).setTemperatureRainfall(0.0f, 0.2f).setMinMaxHeight(0.5f, 0.9f).setColor(0xcbc7ca).setBiomeName("eredEngrinFoothills");
        LOTRFABiomes.eredEngrin = new LOTRBiomeGenEredEngrin(154, true).setTemperatureRainfall(0.0f, 0.2f).setMinMaxHeight(2.0f, 3.0f).setColor(0xe3dee2).setBiomeName("eredEngrin");
        LOTRFABiomes.ramdal = new LOTRBiomeGenAndramFoothills(155, true, 1).setTemperatureRainfall(0.5f, 0.7f).setMinMaxHeight(0.5f, 0.9f).setColor(0x728a61).setBiomeName("ramdal");
        LOTRFABiomes.beleriandSouthWoodlands = new LOTRBiomeGenGenericForest(156, false).setTemperatureRainfall(0.9f, 0.5f).setMinMaxHeight(0.2f, 0.4f).setColor(0x438536).setBiomeName("southBeleriandWoodlands");
        LOTRFABiomes.beleriandSouth = new LOTRBiomeGenGenericGrassland(157, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.4f).setColor(0x6ea54a).setBiomeName("southBeleriand");
        LOTRFABiomes.dorDinen = new LOTRBiomeGenDorDinen(158, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.4f).setColor(0x7faf51).setBiomeName("dorDinen");
        LOTRFABiomes.marchMaedhros = new LOTRBiomeGenMarchMaedhros(159, true).setTemperatureRainfall(0.9f, 0.9f).setMinMaxHeight(0.15f, 0.2f).setColor(0x607340).setBiomeName("marchMaedhros");

        LOTRFABiomes.highFaroth = new LOTRBiomeGenTaurEnFaroth(160, true).setTemperatureRainfall(0.28f, 0.2f).setMinMaxHeight(1.8f, 2.0f).setColor(0x779267).setBiomeName("highFaroth");
        LOTRFABiomes.mithrimMountains = new LOTRBiomeGenMithrimMountains(161, true, 3).setTemperatureRainfall(0.28f, 0.2f).setMinMaxHeight(1.8f, 2.0f).setColor(0xb4bfad).setBiomeName("mithrimMountains");
        LOTRFABiomes.nevrastMarshes = new LOTRBiomeGenNevrastMarshes(162, true).setTemperatureRainfall(0.8f, 1.0f).setMinMaxHeight(0.0f, 0.1f).setColor(0x50b152).setBiomeName("nevrastMarshes");
        LOTRFABiomes.nevrast = new LOTRBiomeGenNevrast(163, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.4f).setColor(0x4ca64e).setBiomeName("nevrast");

        LOTRFABiomes.dimbar = new LOTRBiomeGenDimbar(164, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.4f).setColor(0x779a56).setBiomeName("dimbar");
        LOTRFABiomes.falasHills = new LOTRBiomeGenFalasHills(165, true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.5f, 0.5f).setColor(0x6b8c3c).setBiomeName("falasHills");
        LOTRFABiomes.eredNevrast = new LOTRBiomeGenGenericMountains(166, true, 0).setTemperatureRainfall(0.28f, 0.2f).setMinMaxHeight(1.8f, 2.0f).setColor(0xb4c0ad).setBiomeName("nevrastMountains");
        LOTRFABiomes.nargothrond = new LOTRBiomeGenNargothrond(168, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(2.2f, 2.2f).setColor(0x6d9557).setBiomeName("nargothrond");
        LOTRFABiomes.nargothrondPlateau = new LOTRBiomeGenNargothrondPlateau(169, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(1.6f, 1.6f).setColor(0x5b8d3f).setBiomeName("nargothrondPlateau");
        LOTRFABiomes.tumladen = new LOTRBiomeGenTumladen(170, true).setTemperatureRainfall(1.1f, 1.1f).setMinMaxHeight(0.1f, 0.05f).setColor(0x76b56a).setBiomeName("tumladen");
        LOTRFABiomes.girdleMelian = new LOTRBiomeGenGirdleMelian(171, true).setTemperatureRainfall(0.5f, 1.0f).setMinMaxHeight(0.2f, 0.3f).setColor(0x406128).setBiomeName("girdleMelian");
        LOTRFABiomes.hithlum = new LOTRBiomeGenHithlum(173, true).setTemperatureRainfall(0.5f, 0.6f).setMinMaxHeight(0.1f, 0.4f).setColor(0x73b158).setBiomeName("hithlum");
        LOTRFABiomes.beleriandWest = new LOTRBiomeGenWestBeleriand(174, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.4f).setColor(0x60af2c).setBiomeName("westBeleriand");
        LOTRFABiomes.talathDirnen = new LOTRBiomeGenTalathDirnen(175, true).setTemperatureRainfall(0.9f, 1.1f).setMinMaxHeight(0.05f, 0.05f).setColor(0x76af3c).setBiomeName("talathDirnen");
        LOTRFABiomes.beleriandDowns = new LOTRBiomeGenBeleriandDowns(177, true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.5f, 0.5f).setColor(0x61933f).setBiomeName("beleriandDowns");
        LOTRFABiomes.doriath = new LOTRBiomeGenNivrim(178, true).setTemperatureRainfall(0.7f, 0.7f).setMinMaxHeight(0.2f, 0.4f).setColor(0x314f1b).setBiomeName("doriath");
        LOTRFABiomes.taurNuFuin = new LOTRBiomeGenTaurNuFuin(179, true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.82f, 0.95f).setColor(0x253725).setBiomeName("taurNuFuin");
        LOTRFABiomes.nanDungortheb = new LOTRBiomeGenNanDungortheb(180, true).setTemperatureRainfall(2.0f, 0.0f).setMinMaxHeight(0.1f, 0.4f).setColor(0x5f8558).setBiomeName("nanDungortheb");
        LOTRFABiomes.balar = new LOTRBiomeGenBalar(181, true).setTemperatureRainfall(0.9f, 0.9f).setMinMaxHeight(0.07f, 0.2f).setColor(0x8bba5b).setBiomeName("balar");
        LOTRFABiomes.baradNimras = new LOTRBiomeGenBaradNimras(182, true).setTemperatureRainfall(0.9f, 0.9f).setMinMaxHeight(0.2f, 0.35f).setColor(0x67b137).setBiomeName("baradNimras");
        LOTRFABiomes.taurEnFaroth = new LOTRBiomeGenTaurEnFaroth(183, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.5f, 0.9f).setColor(0x638b4b).setBiomeName("taurEnFaroth");
        LOTRFABiomes.dryRiver = new LOTRBiomeGenDryRiver(184, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.01f, 0.02f).setColor(0x8c8e5a).setBiomeName("dryRiver");
        LOTRFABiomes.anfauglith = new LOTRBiomeGenAnfauglith(185, true).setTemperatureRainfall(1.1f, 1.1f).setMinMaxHeight(0.1f, 0.05f).setColor(0x626055).setBiomeName("anfauglith");
        LOTRFABiomes.ossiriand = new LOTRBiomeGenOssiriand(188, true).setTemperatureRainfall(0.7f, 0.7f).setMinMaxHeight(0.2f, 0.4f).setColor(0x58a745).setBiomeName("ossiriand");
        LOTRFABiomes.ossiriandWoodlands = new LOTRBiomeGenOssiriandWoodlands(189, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.2f, 0.3f).setColor(0x2e6f3a).setBiomeName("ossiriandWoodlands");
        LOTRFABiomes.gardhBoren = new LOTRBiomeGenGardhBoren(193, true).setTemperatureRainfall(0.8f, 0.7f).setMinMaxHeight(0.1f, 0.2f).setColor(0x6da046).setBiomeName("gardhBoren");
        LOTRFABiomes.estolad = new LOTRBiomeGenEstolad(194, true).setTemperatureRainfall(0.9f, 0.3f).setMinMaxHeight(0.2f, 0.3f).setColor(0x76b04a).setBiomeName("estolad");
        LOTRFABiomes.taurdru = new LOTRBiomeGenTaurdru(195, true).setTemperatureRainfall(0.35f, 1.1f).setMinMaxHeight(0.1f, 0.05f).setColor(0x687750).setBiomeName("taurdru");
        LOTRFABiomes.neldoreth = new LOTRBiomeGenNeldoreth(196, true).setTemperatureRainfall(0.7F, 0.7F).setMinMaxHeight(0.2F, 0.4F).setColor(0x3c5429).setBiomeName("neldoreth");
        LOTRFABiomes.region = new LOTRBiomeGenRegion(197, true).setTemperatureRainfall(0.7F, 0.7F).setMinMaxHeight(0.2F, 0.4F).setColor(0x36541f).setBiomeName("region");
        LOTRFABiomes.belegost = new LOTRBiomeGenBelegost(198, true).setTemperatureRainfall(0.3f, 0.4f).setMinMaxHeight(1.0f, 2.5f).setColor(0xbfd0d7).setBiomeName("belegost");
        LOTRFABiomes.helcaraxe = new LOTRBiomeGenHelcaraxe(206, true).setTemperatureRainfall(0.0f, 0.8f).setMinMaxHeight(0.2f, 1.2f).setColor(0xdde4ec).setBiomeName("helcaraxe");
        LOTRFABiomes.ivrin = new LOTRBiomeGenIvrin(207, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(-0.1f, 0.1f).setColor(0x4eb250).setBiomeName("ivrin");
        LOTRFABiomes.nanTathren = new LOTRBiomeGenNanTathren(208, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.0f, 0.1f).setColor(0x4a8422).setBiomeName("nanTathren");
        LOTRFABiomes.vinyamar = new LOTRBiomeGenVinyamar(209, true).setTemperatureRainfall(0.9f, 1.0f).setMinMaxHeight(0.15f, 0.3f).setColor(0x97ad82).setBiomeName("vinyamar");
        LOTRFABiomes.tolInGaurhoth = new LOTRBiomeGenTolInGaurhoth(210, true).setTemperatureRainfall(0.9f, 1.0f).setMinMaxHeight(0.1f, 0.4f).setColor(0x16301b).setBiomeName("tolInGaurhoth");
        LOTRFABiomes.falas = new LOTRBiomeGenFalas(211, true).setTemperatureRainfall(0.9f, 0.9f).setMinMaxHeight(0.07f, 0.2f).setColor(0x7bb849).setBiomeName("falathrimHeartland");
        LOTRFABiomes.taurImDuinath = new LOTRBiomeGenTaurImDuinath(213, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.1f, 0.4f).setColor(0x3d7732).setBiomeName("taurImDuinath");
        LOTRFABiomes.nanElmoth = new LOTRBiomeGenNanElmoth(216, true).setTemperatureRainfall(0.7f, 0.7f).setMinMaxHeight(0.2f, 0.4f).setColor(0x2c5a24).setBiomeName("nanElmoth");
        LOTRFABiomes.eredLomin = new LOTRBiomeGenEredLomin(218, true, 3).setTemperatureRainfall(0.28f, 0.2f).setMinMaxHeight(1.8f, 2.0f).setColor(0xbcc2b8).setBiomeName("eredLomin");
        LOTRFABiomes.eredLominFoothills = new LOTRBiomeGenEredLomin(219, true, 3).setTemperatureRainfall(0.5f, 0.7f).setMinMaxHeight(0.5f, 0.9f).setColor(0x7da15d).setBiomeName("eredLominFoothills");
        LOTRFABiomes.baradEithel = new LOTRBiomeGenBaradEithel(220, true).setTemperatureRainfall(0.5f, 0.7f).setMinMaxHeight(1.8f, 2.0f).setColor(0x96a78b).setBiomeName("baradEithel");
        LOTRFABiomes.lisgardh = new LOTRBiomeGenLisgardh(221, true).setTemperatureRainfall(0.8f, 1.0f).setMinMaxHeight(0.0f, 0.1f).setColor(0x409d49).setBiomeName("lisgardh");
        LOTRFABiomes.theSea = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x0055a5).setBiomeName("theSea");
        LOTRFABiomes.andramMountains = new LOTRBiomeGenAndram(223, true, 1).setTemperatureRainfall(0.28f, 0.2f).setMinMaxHeight(1.8f, 2.0f).setColor(0x7d976b).setBiomeName("andramMountains");
        LOTRFABiomes.andram = new LOTRBiomeGenAndramFoothills(224, true, 1).setTemperatureRainfall(0.5f, 0.7f).setMinMaxHeight(0.5f, 0.9f).setColor(0x6a8c56).setBiomeName("andram");
        LOTRFABiomes.eredNevrastFoothills = new LOTRBiomeGenEredNevrast(226, true, 2).setTemperatureRainfall(0.5f, 0.7f).setMinMaxHeight(0.5f, 0.9f).setColor(0x74955d).setBiomeName("nevrastMountainFoothills");
        LOTRFABiomes.eredWethrin = new LOTRBiomeGenEredWethrin(227, true, 0).setTemperatureRainfall(0.28f, 0.2f).setMinMaxHeight(1.8f, 2.0f).setColor(0xb2b8ae).setBiomeName("hithlumMountains");
        LOTRFABiomes.eredWethrinFoothills = new LOTRBiomeGenEredWethrinFoothills(228, true, 0).setTemperatureRainfall(0.5f, 0.7f).setMinMaxHeight(0.5f, 0.9f).setColor(0x789a59).setBiomeName("hithlumMountainFoothills");
        LOTRFABiomes.amonRudh = new LOTRBiomeGenAmonRudh(231, true).setTemperatureRainfall(0.9f, 1.1f).setMinMaxHeight(0.5f, 0.7f).setColor(0x878977).setBiomeName("amonRudh");
        LOTRFABiomes.beleriandEast = new LOTRBiomeGenEastBeleriand(232, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.4f).setColor(0x65a63a).setBiomeName("eastBeleriand");
        LOTRFABiomes.mithrim = new LOTRBiomeGenMithrim(233, true).setTemperatureRainfall(0.5f, 0.6f).setMinMaxHeight(0.1f, 0.4f).setColor(0x6db94c).setBiomeName("mithrim");
        LOTRFABiomes.thargelionWoodland = new LOTRBiomeGenThargelionWoodland(235, true).setTemperatureRainfall(0.6f, 0.4f).setMinMaxHeight(0.5f, 0.5f).setColor(0x3c7546).setBiomeName("thargelionWoodland");
        LOTRFABiomes.brethil = new LOTRBiomeGenBrethil(236, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.2f, 0.12f).setColor(0x3c5d12).setBiomeName("brethil");
        LOTRFABiomes.brethilEdge = new LOTRBiomeGenBrethilEdge(237, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.2f, 0.12f).setColor(0x43641b).setBiomeName("brethilEdge");
        LOTRFABiomes.himlad = new LOTRBiomeGenHimlad(238, true).setTemperatureRainfall(0.9f, 0.9f).setMinMaxHeight(0.15f, 0.2f).setColor(0x7fbc45).setBiomeName("himlad");
        LOTRFABiomes.amonEreb = new LOTRBiomeGenAmonEreb(239, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(0.5f, 0.5f).setColor(0x709756).setBiomeName("amonEreb");
        LOTRFABiomes.echoriath = new LOTRBiomeGenEchoriath(242, true).setTemperatureRainfall(0.6f, 0.8f).setMinMaxHeight(1.8f, 2.4f).setColor(0x747c72).setBiomeName("echoriath");
        LOTRFABiomes.dorLomin = new LOTRBiomeGenDorLomin(244, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(0.5f, 0.5f).setColor(0x69af4a).setBiomeName("dorlomin");
        LOTRFABiomes.angband = new LOTRBiomeGenAngband(245, true).setTemperatureRainfall(1.5f, 0.0f).setMinMaxHeight(0.2f, 0.7f).setColor(0x4f484b).setBiomeName("angband");
        LOTRFABiomes.balarHills = new LOTRBiomeGenBalarHills(247, true).setTemperatureRainfall(0.8f, 0.7f).setMinMaxHeight(0.5f, 0.5f).setColor(0x7ca350).setBiomeName("balarHills");
        LOTRFABiomes.fenSerech = new LOTRBiomeGenFenSerech(248, true).setTemperatureRainfall(0.8f, 1.0f).setMinMaxHeight(0.0f, 0.1f).setColor(0x5c7950).setBiomeName("fenSerech");
        LOTRFABiomes.nanGuruthos = new LOTRBiomeGenNanGuruthos(249, true).setTemperatureRainfall(0.0f, 0.2f).setMinMaxHeight(0.1f, 0.1f).setColor(0xb3b3a7).setBiomeName("angbandHeartland");
        LOTRFABiomes.dorDaedeloth = new LOTRBiomeGenDorDaedeloth(250, true).setTemperatureRainfall(1.1f, 1.1f).setMinMaxHeight(0.1f, 0.05f).setColor(0x433f41).setBiomeName("dorDaedeloth");
        LOTRFABiomes.nimbrethil = new LOTRBiomeGenNimbrethil(251, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.1f, 0.4f).setColor(0x508519).setBiomeName("nimbrethil");
        LOTRFABiomes.thangorodrim = new LOTRBiomeGenThangorodrim(252, true).setTemperatureRainfall(2.0f, 0.0f).setMinMaxHeight(3.0F, 3.0F).setColor(0x625d60).setBiomeName("thangorodrim");
        LOTRFABiomes.forodwaithMtns = new LOTRBiomeGenForodwaithMtns(253, true).setTemperatureRainfall(0.0f, 0.2f).setMinMaxHeight(2.0f, 2.0f).setColor(0xd7f0f7).setBiomeName("forodwaithMtns");
        LOTRFABiomes.himringMtns = new LOTRBiomeGenHimringMtns(254, true).setTemperatureRainfall(0.6f, 0.8f).setMinMaxHeight(1.5f, 2.0f).setColor(0xdfe0cc).setBiomeName("himringMtns");
        LOTRFABiomes.himringFoothills = new LOTRBiomeGenHimringFoothills(255, true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.5f, 0.9f).setColor(0xb8c4af).setBiomeName("himringFoothills");
        
        /*LOTRFABiomes.locked = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x757575).setBiomeName("locked"); //Eriador
        LOTRFABiomes.locked1 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0xd4d4d4).setBiomeName("locked"); //Forodwaith
        LOTRFABiomes.locked2 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0xededed).setBiomeName("locked"); //Forodwaith Mountains
        LOTRFABiomes.locked3 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0xcecece).setBiomeName("locked"); //Forodwaith Mountains Foothills
        LOTRFABiomes.locked4 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0xe0e0e0).setBiomeName("locked"); //Ered Engrin
        LOTRFABiomes.locked5 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0xc9c9c9).setBiomeName("locked"); //Ered Engrin Foothills
        LOTRFABiomes.locked6 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0xd3d3d3).setBiomeName("locked"); //Frozen Tundra
        LOTRFABiomes.locked7 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0xa9a9a9).setBiomeName("locked"); //Tundra
        LOTRFABiomes.locked8 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x727272).setBiomeName("locked"); //Taiga
        LOTRFABiomes.locked9 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x696969).setBiomeName("locked"); //Eriador Downs
        LOTRFABiomes.locked10 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x5d5d5d).setBiomeName("locked"); //Eriador Woodlands
        LOTRFABiomes.locked11 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x757575).setBiomeName("locked"); //Eriador
        LOTRFABiomes.locked12 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x757575).setBiomeName("locked"); //Eriador
        LOTRFABiomes.locked13 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x757575).setBiomeName("locked"); //Eriador
        LOTRFABiomes.locked14 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x757575).setBiomeName("locked"); //Eriador
        LOTRFABiomes.locked15 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x757575).setBiomeName("locked"); //Eriador
        LOTRFABiomes.locked16 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x757575).setBiomeName("locked"); //Eriador
        LOTRFABiomes.locked17 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x757575).setBiomeName("locked"); //Eriador
        LOTRFABiomes.locked18 = new LOTRBiomeGenTheSea(222, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(-1.0f, 0.3f).setColor(0x757575).setBiomeName("locked"); //Eriador*/

        // LOTRFABiomes.nargothrond = new LOTRBiomeGenNargothrond(168, true).setTemperatureRainfall(0.9f, 1.0f).setMinMaxHeight(0.15f, 0.3f).setColor(0x42a006).setBiomeName("nargothrond");
        // LOTRFABiomes.sindarinRealmHills = new LOTRBiomeGenSindarinRealmHills(172, true).setTemperatureRainfall(0.8f, 0.6f).setMinMaxHeight(0.9f, 0.7f).setColor(0x448734).setBiomeName("sindarinRealmHills");
        // LOTRFABiomes.talathDirnenHills = new LOTRBiomeGenTalathDirnenHills(176, true).setTemperatureRainfall(0.7f, 0.7f).setMinMaxHeight(0.6f, 0.4f).setColor(0x8ec156).setBiomeName("talathDirnenHills");
        // LOTRFABiomes.dorFirnIGuinar = new LOTRBiomeGenDorFirnIGuinar(234, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.1f, 0.4f).setColor(0x4e8728).setBiomeName("dorFirnIGuinar");
        // this.ardGalen = new LOTRBiomeGenArdGalen(185,
        // true).setTemperatureRainfall(1.1f, 1.1f).setMinMaxHeight(0.1f,
        // 0.05f).setColor(0x70ae54).setBiomeName("ardGalen");
        // this.guldar = new LOTRBiomeGenCorruptedWoods(187, true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.95f, 1.08f).setColor(0x0b100a).setBiomeName("guldar");
        // LOTRFABiomes.ossiriandHills = new LOTRBiomeGenOssiriandHills(190, true).setTemperatureRainfall(0.8f, 0.6f).setMinMaxHeight(0.9f, 0.7f).setColor(0x3f6b41).setBiomeName("ossiriandHills");
        // LOTRFABiomes.ossiriandRiverVales = new LOTRBiomeGenOssiriandRiverVales(191, true).setTemperatureRainfall(0.9f, 1.1f).setMinMaxHeight(0.05f, 0.05f).setColor(0x386c42).setBiomeName("ossiriandRiverVales");
        // LOTRFABiomes.ossiriandRiverValesHills = new LOTRBiomeGenOssiriandRiverValesHills(192, true).setTemperatureRainfall(0.7f, 0.7f).setMinMaxHeight(0.6f, 0.4f).setColor(0x4a8f57).setBiomeName("ossiriandRiverValesHills");
        // LOTRFABiomes.belegostFoothills = new LOTRBiomeGenBelegostFoothills(105, true).setTemperatureRainfall(0.7f, 0.4f).setMinMaxHeight(0.5f, 0.9f).setColor(0x9eaca7).setBiomeName("belegostFoothills");
        // LOTRFABiomes.sindarinRealmNeldoreth = new LOTRBiomeGenSindarinRealmNeldoreth(200, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.2f, 0.3f).setColor(0x3c6915).setBiomeName("sindarinRealmNeldoreth");
        // LOTRFABiomes.sindarinRealmRegion = new LOTRBiomeGenSindarinRealmRegion(201, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.2f, 0.3f).setColor(0x24692d).setBiomeName("sindarinRealmRegion");
        // LOTRFABiomes.sindarinRealmHillsNeldoreth = new LOTRBiomeGenSindarinRealmHillsNeldoreth(202, true).setTemperatureRainfall(0.8f, 0.6f).setMinMaxHeight(0.9f, 0.7f).setColor(0x5c8734).setBiomeName("sindarinRealmHillsNeldoreth");
        // LOTRFABiomes.sindarinRealmHillsRegion = new LOTRBiomeGenSindarinRealmHillsRegion(203, true).setTemperatureRainfall(0.8f, 0.6f).setMinMaxHeight(0.9f, 0.7f).setColor(0x44874c).setBiomeName("sindarinRealmHillsRegion");
        // this.estoldWoodlands = new LOTRBiomeGenEstoladWoodlands(204, false).setTemperatureRainfall(0.9f, 0.5f).setMinMaxHeight(0.2f, 0.4f).setColor(0x789227).setBiomeName("estoladWoodlands");
        // this.estoladFields = new LOTRBiomeGenEstoladFields(205, true).setTemperatureRainfall(0.9f, 0.1f).setMinMaxHeight(0.4f, 0.3f).setColor(0x829b4b).setBiomeName("estoladFields");
        // this.falathrimHills = new LOTRBiomeGenFalathrimHills(212, true).setTemperatureRainfall(0.8f, 0.7f).setMinMaxHeight(0.5f, 0.5f).setColor(0x6a9f3f).setBiomeName("falathrimHills");
        // this.nogrodMtns = new LOTRBiomeGenNogrodMtns(214, true).setTemperatureRainfall(0.22f, 0.8f).setMinMaxHeight(1.0f, 2.5f).setColor(0xc8d9e0).setBiomeName("nogrodMountains");
        // this.eredLuinFoothills = new LOTRBiomeGenEredLuinFoothills(215, true).setTemperatureRainfall(0.5f, 0.8f).setMinMaxHeight(0.5f, 0.9f).setColor(0xaeb7b4).setBiomeName("eredLuinFoothills");
        // LOTRFABiomes.nanElmothRealm = new LOTRBiomeGenEolsRealm(217, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.2f, 0.3f).setColor(0x1e4616).setBiomeName("nanElmothRealm");
        // this.feanorianMtns = new LOTRBiomeGenFeanorianHills(225, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(0.5f, 0.5f).setColor(0x709756).setBiomeName("feanorianMtns");
        // LOTRFABiomes.lothlann = new LOTRBiomeGenLothlann(229, true).setTemperatureRainfall(0.8f, 1.1f).setMinMaxHeight(0.1f, 0.05f).setColor(0xff9000).setBiomeName("lothlann");
        // LOTRFABiomes.talathdru = new LOTRBiomeGenTalathdru(230, true).setTemperatureRainfall(0.35f, 1.1f).setMinMaxHeight(0.1f, 0.05f).setColor(0x6a954d).setBiomeName("talathdru");
        // this.dorthonionBarren = new LOTRBiomeGenDorthonionBarren(235,
        // true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.95f,
        // 1.08f).setColor(0xd4d6b5).setBiomeName("dorthonionBarren");
        // this.ulfangLands = new LOTRBiomeGenTalathdru(240, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.4f).setColor(0xa0b283).setBiomeName("ulfangLands");
        // LOTRFABiomes.feanorianWoods = new LOTRBiomeGenFeanorianWoods(241, true).setTemperatureRainfall(0.8f, 0.8f).setMinMaxHeight(0.5f, 0.5f).setColor(0x60d151).setBiomeName("feanorianWoods");
        // LOTRFABiomes.ladros = new LOTRBiomeGenLadros(243, true).setTemperatureRainfall(0.6f, 0.4f).setMinMaxHeight(0.6f, 0.9f).setColor(0x284628).setBiomeName("ladros");
        // LOTRFABiomes.angbandHighlands = new LOTRBiomeGenAngbandHighlands(246, true).setTemperatureRainfall(2.0f, 0.0f).setMinMaxHeight(0.6f, 0.2f).setColor(0x1a1a1a).setBiomeName("angbandHighlands");
        // this.echoriathFoothills = new LOTRBiomeGenEchoriathFoothills(102,
        // true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.5f,
        // 0.9f).setColor(0xc4d1ba).setBiomeName("echoriathFoothills");
        // this.taurNuFuinFoothills = new LOTRBiomeGenTaurNuFuinFoothills(146,
        // true).setTemperatureRainfall(0.28f, 0.9f).setMinMaxHeight(0.5f,
        // 0.9f).setColor(0x1f2a12).setBiomeName("taurNuFuinFoothills");
        // this.taurNuFuinFoothills = new LOTRBiomeGenMirkwoodCorrupted(146,
        // true).setTemperatureRainfall(0.28f, 0.9f).setMinMaxHeight(0.5f,
        // 0.9f).setColor(0x1f2a12).setBiomeName("taurNuFuin");
        // 151
        // this.greenwood = new LOTRBiomeGenGreenwood(10,
        // true).setTemperatureRainfall(0.7f, 0.7f).setMinMaxHeight(0.2f,
        // 0.4f).setColor(0x1e3a1a).setBiomeName("greenwood");
        // this.greenwoodRealm = new LOTRBiomeGenGreenwoodRealm(79,
        // true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.2f,
        // 0.3f).setColor(0x1f481c).setBiomeName("greenwoodRealm");
        // this.greenwoodHills = new LOTRBiomeGenGreenwoodHills(81,
        // true).setTemperatureRainfall(0.8f, 0.6f).setMinMaxHeight(0.9f,
        // 0.7f).setColor(0x375e2e).setBiomeName("greenwoodHills");
        // LOTRFABiomes.entwifeGardens = new LOTRBiomeGenEntwifeGardens(7, true).setTemperatureRainfall(0.7f, 0.8f).setMinMaxHeight(0.2f, 0.1f).setColor(0x2c972d).setBiomeName("entwifeGardens");
        // this.wildWood = new LOTRBiomeGenWildWood(84,
        // true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.3f,
        // 0.5f).setColor(0x606e2e).setBiomeName("wildWood");
        // this.barrowDowns = new LOTRBiomeGenBarrows(166,
        // true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.3f,
        // 0.4f).setColor(0x75964f).setBiomeName("barrowDownsFA");
        // this.cuivienen = new LOTRBiomeGenCuivienen(167,
        // true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.3f,
        // 0.4f).setColor(0x7bae46).setBiomeName("cuivienen");
        // this.hildorien = new LOTRBiomeGenHildorien(14,
        // true).setTemperatureRainfall(0.6f, 0.7f).setMinMaxHeight(0.3f,
        // 0.4f).setColor(0xb9c45b).setBiomeName("hildorien");
        // this.blackMountains = new LOTRBiomeGenBlackMountains(39,
        // true).setTemperatureRainfall(0.28f, 0.9f).setMinMaxHeight(1.2f,
        // 1.5f).setColor(0x394a35).setBiomeName("blackMountains");
        // LOTRFABiomes.arthorienRealm = new LOTRBiomeGenArthorienRealm(54, true).setTemperatureRainfall(0.8f, 0.9f).setMinMaxHeight(0.2f, 0.3f).setColor(0x24693a).setBiomeName("arthorienRealm");
        // LOTRFABiomes.arthorienRealmHills = new LOTRBiomeGenArthorienRealmHills(9, true).setTemperatureRainfall(0.8f, 0.6f).setMinMaxHeight(0.9f, 0.7f).setColor(0x448759).setBiomeName("arthorienRealmHills");
        /*
         * this.angbandHeartland.spawnableEvilList.clear();
         * this.angbandHeartland.spawnableEvilList.add(new
         * LOTRSpawnEntry(LOTREntityMordorOrc.class, 10, 4, 6));
         * this.angbandHeartland.spawnableEvilList.add(new
         * LOTRSpawnEntry(LOTREntityMordorOrcArcher.class, 5, 4, 6));
         * this.angbandHeartland.spawnableEvilList.add(new
         * LOTRSpawnEntry(LOTREntityMordorOrcBombardier.class, 2, 1, 2));
         * this.angbandHeartland.spawnableEvilList.add(new
         * LOTRSpawnEntry(LOTREntityMordorWarg.class, 40, 1, 3));
         * this.angbandHeartland.spawnableEvilList.add(new
         * LOTRSpawnEntry(LOTREntityBlackUruk.class, 5, 4, 6));
         * this.angbandHeartland.spawnableEvilList.add(new
         * LOTRSpawnEntry(LOTREntityBlackUrukArcher.class, 2, 4, 6));
         */
        // this.thargelionEdge = new LOTRBiomeGenThargelionEdge(107, true).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.4f).setColor(0x80b85b).setBiomeName("thargelionEdge");
        // this.southAnfauglith = new LOTRBiomeGenAnfauglith(239, true).setTemperatureRainfall(1.1f, 1.1f).setMinMaxHeight(0.1f, 0.05f).setColor(0x637054).setBiomeName("southAnfauglith");
        // this.southTaurdru = new LOTRBiomeGenTaurdru(199, true).setTemperatureRainfall(0.35f, 1.1f).setMinMaxHeight(0.1f, 0.05f).setColor(0x5d8e52).setBiomeName("southTaurdru");

        LOTRBiome.tundra.biomeColors.setSky(5523773);
        LOTRBiome.tundra.biomeColors.setClouds(3355443);
        LOTRBiome.tundra.biomeColors.setFog(0x4e473f);
        LOTRBiome.tundra.biomeColors.setWater(2498845);
        LOTRBiome.tundra.biomeColors.setGrass(0x5a6643);
        LOTRBiome.tundra.decorator.addRandomStructure(new LOTRWorldGenDorDaidelosCamp(false), 200);
        LOTRBiome.tundra.spawnableEvilList.clear();
        LOTRBiome.tundra.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(200, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDorDaidelosOrc.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityDorDaidelosOrcArcher.class, 5, 4, 6)}));
        LOTRBiome.tundra.invasionSpawns.clearInvasions();
        LOTRBiome.tundra.invasionSpawns.addInvasion(LOTRFAInvasions.dorDaidelos, LOTREventSpawner.EventChance.UNCOMMON);
        
        LOTRBiome.forodwaithMountains.decorator.addOre(new WorldGenMinable(LOTRMod.oreMorgulIron, 8), 20.0f, 0, 64);
        LOTRBiome.forodwaithMountains.decorator.addOre(new WorldGenMinable(LOTRMod.oreGulduril, 8), 8.0f, 0, 32);
        
        LOTRBiome.forodwaith.decorator.addOre(new WorldGenMinable(LOTRMod.oreMorgulIron, 8), 20.0f, 0, 64);
        LOTRBiome.forodwaith.decorator.addOre(new WorldGenMinable(LOTRMod.oreGulduril, 8), 8.0f, 0, 32);
        LOTRBiome.forodwaith.decorator.addRandomStructure(new LOTRWorldGenDorDaidelosCamp(false), 700);
        LOTRBiome.forodwaith.spawnableEvilList.clear();
        LOTRBiome.forodwaith.spawnableEvilList.addAll(LOTRSpawnEntry.chanceList(500, new LOTRSpawnEntry[] {new LOTRSpawnEntry(LOTREntityDorDaidelosOrc.class, 10, 4, 6), new LOTRSpawnEntry(LOTREntityDorDaidelosOrcArcher.class, 5, 4, 6)}));
        LOTRBiome.forodwaith.invasionSpawns.addInvasion(LOTRFAInvasions.dorDaidelos, LOTREventSpawner.EventChance.COMMON);
        

        //LOTRBiome.blueMountains.decorator.clearRandomStructures();
        LOTRBiome.blueMountains.spawnableEvilList.clear();
        // LOTRBiome.blueMountains.decorator.addRandomStructure(new LOTRWorldGenNogrodStronghold(false), 400);
        LOTRBiome.blueMountains.decorator.addRandomStructure(new LOTRWorldGenBlueMountainsSmithy(false), 150);

        LOTRFAReflectionHelper.getLOTRBiome("taiga").invasionSpawns.clearInvasions();
        //TODO also needs to be set?
        
        LOTRFABiomes.doriath.addFlower(LOTRMod.blackroot, 0, 60);
    }
}
