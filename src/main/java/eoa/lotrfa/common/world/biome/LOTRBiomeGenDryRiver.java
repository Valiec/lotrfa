package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRAchievement;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;

public class LOTRBiomeGenDryRiver extends LOTRBiomeGenWestBeleriand {

    public LOTRBiomeGenDryRiver(int i, boolean major) {
        super(i, major);
        this.decorator.treesPerChunk = 0;
        this.decorator.willowPerChunk = 0;
        this.decorator.generateWater = false;
        this.biomeColors.setGrass(0xd6c75c);
        this.decorator.clearRandomStructures();
        this.setBanditChance(LOTREventSpawner.EventChance.UNCOMMON);
        this.clearTravellingTraders();
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.westBeleriand;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterDryRiver;
    }

}
