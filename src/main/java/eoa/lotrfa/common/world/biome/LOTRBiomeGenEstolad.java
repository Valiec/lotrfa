package eoa.lotrfa.common.world.biome;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRAchievement;
import lotr.common.entity.animal.LOTREntityBear;
import lotr.common.entity.npc.LOTREntityBlueDwarfMerchant;
import lotr.common.entity.npc.LOTREntityScrapTrader;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRRoadType;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTREventSpawner;
import net.minecraft.world.biome.BiomeGenBase;

public class LOTRBiomeGenEstolad extends LOTRBiomeGenEastBeleriand {

    public LOTRBiomeGenEstolad(int i, boolean major) {
        super(i, major);
        this.spawnableCreatureList.add(new BiomeGenBase.SpawnListEntry(LOTREntityBear.class, 3, 1, 4));
        this.spawnableGoodList.clear();
        this.spawnableEvilList.clear();
        this.invasionSpawns.clearInvasions();
        this.decorator.generateOrcDungeon = false;
        this.variantChance = 0.3f;
        this.addBiomeVariantSet(LOTRBiomeVariant.SET_NORMAL_OAK);
        this.addBiomeVariant(LOTRBiomeVariant.BOULDERS_ROHAN);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BEECH, 0.5f);
        this.addBiomeVariant(LOTRBiomeVariant.FOREST_BIRCH, 0.5f);
        this.addBiomeVariant(LOTRBiomeVariant.ORCHARD_APPLE_PEAR, 0.5f);
        this.decorator.setTreeCluster(12, 30);
        this.decorator.willowPerChunk = 1;
        this.decorator.flowersPerChunk = 4;
        this.decorator.grassPerChunk = 15;
        this.decorator.doubleGrassPerChunk = 5;
        this.decorator.addTree(LOTRTreeType.OAK, 400);
        this.decorator.addTree(LOTRTreeType.OAK_LARGE, 800);
        this.decorator.addTree(LOTRTreeType.BIRCH, 20);
        this.decorator.addTree(LOTRTreeType.BIRCH_LARGE, 10);
        this.decorator.addTree(LOTRTreeType.BEECH, 20);
        this.decorator.addTree(LOTRTreeType.BEECH_LARGE, 10);
        this.decorator.addTree(LOTRTreeType.PINE, 20);
        this.decorator.addTree(LOTRTreeType.APPLE, 2);
        this.decorator.addTree(LOTRTreeType.PEAR, 2);
        this.registerPlainsFlowers();
        this.clearTravellingTraders();
        this.invasionSpawns.clearInvasions();
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseBor, LOTREventSpawner.EventChance.UNCOMMON);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedeloth, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.angbandDaedelothWarg, LOTREventSpawner.EventChance.RARE);
        this.invasionSpawns.addInvasion(LOTRFAInvasions.houseUlfang, LOTREventSpawner.EventChance.UNCOMMON);
        this.decorator.clearVillages();
        this.decorator.clearRandomStructures();
        this.setBanditChance(LOTREventSpawner.EventChance.COMMON);
        this.registerTravellingTrader(LOTREntityScrapTrader.class);
        this.registerTravellingTrader(LOTREntityBlueDwarfMerchant.class);
    }

    @Override
    public LOTRRoadType getRoadBlock() {
        return LOTRRoadType.PATH;
    }

    @Override
    public LOTRWaypoint.Region getBiomeWaypoints() {
        return LOTRFAWaypoints.Regions.estolad;
    }

    @Override
    public LOTRAchievement getBiomeAchievement() {
        return LOTRFAAchievements.enterEstolad;
    }

}
