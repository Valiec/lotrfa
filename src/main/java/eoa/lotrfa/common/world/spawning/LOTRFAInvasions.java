package eoa.lotrfa.common.world.spawning;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.reflection.LOTRFAEnumHelper;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.LOTRMod;
import lotr.common.world.spawning.LOTRInvasions;
import lotr.common.world.spawning.LOTRInvasions.InvasionSpawnEntry;

public class LOTRFAInvasions {
    public static LOTRInvasions angband;
    public static LOTRInvasions angbandWarg;
    public static LOTRInvasions angbandDaedeloth;
    public static LOTRInvasions angbandDaedelothWarg;
    public static LOTRInvasions tolInGaurhoth;
    public static LOTRInvasions tolInGaurhothWarg;
    public static LOTRInvasions falathrim;
    public static LOTRInvasions feanorians;
    public static LOTRInvasions gondolin;
    public static LOTRInvasions nargothrond;
    public static LOTRInvasions pettyDwarf;
    public static LOTRInvasions dorDaidelosUtumnoRemnant;
    public static LOTRInvasions dorDaidelos;
    public static LOTRInvasions brethil;
    public static LOTRInvasions dorLomin;
    public static LOTRInvasions hithlum;
    public static LOTRInvasions doriath;
    public static LOTRInvasions laegrim;
    public static LOTRInvasions houseBor;
    public static LOTRInvasions houseUlfang;

    public static void init() throws SecurityException, IllegalArgumentException {
        // TODO move to biomes?
        // for(LOTRBiome biome : LOTRDimension.MIDDLE_EARTH.biomeList) biome.invasionSpawns.clearInvasions();

        setupInvasions();
        addInvasionIcons();
        addInvasionMobs();

        editLOTRBiomeInvasions();
    }

    private static void editLOTRBiomeInvasions() {
        // TODO add BM invasions
        // LOTRBiome.blueMountains.invasionSpawns.addInvasion(invasion, chance);
    }

    private static void setupInvasions() {
    	dorDaidelos = LOTRFAEnumHelper.addInvasion("DOR_DAIDELOS", LOTRFAFactions.dorDaidelos);
    	brethil = LOTRFAEnumHelper.addInvasion("BRETHIL", LOTRFAFactions.brethil);
    	houseUlfang = LOTRFAEnumHelper.addInvasion("HOUSE_ULFANG", LOTRFAFactions.houseUlfang);
    	houseBor = LOTRFAEnumHelper.addInvasion("HOUSE_BOR", LOTRFAFactions.houseBor);
    	laegrim = LOTRFAEnumHelper.addInvasion("LAEGRIM", LOTRFAFactions.laegrim);
    	doriath = LOTRFAEnumHelper.addInvasion("DORIATH", LOTRFAFactions.doriath);
    	hithlum = LOTRFAEnumHelper.addInvasion("HITHLUM", LOTRFAFactions.hithlum);
    	dorLomin = LOTRFAEnumHelper.addInvasion("DOR_LOMIN", LOTRFAFactions.dorLomin);
        angband = LOTRFAEnumHelper.addInvasion("ANGBAND", LOTRFAFactions.angband);
        angbandWarg = LOTRFAEnumHelper.addInvasion("ANGBAND_WARG", LOTRFAFactions.angband, "angbandWarg");
        angbandDaedeloth = LOTRFAEnumHelper.addInvasion("ANGBAND_DAEDELOTH", LOTRFAFactions.angband, "daedeloth");
        angbandDaedelothWarg = LOTRFAEnumHelper.addInvasion("ANGBAND_DAEDELOTH_WARG", LOTRFAFactions.angband, "daedelothWarg");
        tolInGaurhoth = LOTRFAEnumHelper.addInvasion("TOL_IN_GAURHOTH", LOTRFAFactions.tolInGaurhoth);
        tolInGaurhothWarg = LOTRFAEnumHelper.addInvasion("TOL_IN_GAURHOTH_WARG", LOTRFAFactions.tolInGaurhoth, "tolInGaurhothWarg");
        dorDaidelosUtumnoRemnant = LOTRFAEnumHelper.addInvasion("DOR_DAIDELOS_UTUMNO_REMNANT", LOTRFAFactions.dorDaidelos, "utumnoRemnant");
        falathrim = LOTRFAEnumHelper.addInvasion("FALATHRIM", LOTRFAFactions.falathrim);
        feanorians = LOTRFAEnumHelper.addInvasion("FEANORIANS", LOTRFAFactions.feanorians);
        gondolin = LOTRFAEnumHelper.addInvasion("GONDOLIN", LOTRFAFactions.gondolin);
        nargothrond = LOTRFAEnumHelper.addInvasion("NARGOTHROND", LOTRFAFactions.nargothrond);
        pettyDwarf = LOTRFAEnumHelper.addInvasion("PETTY_DWARF", LOTRFAFactions.pettyDwarf);
    }

    private static void addInvasionIcons() throws SecurityException, IllegalArgumentException {
    	LOTRFAReflectionHelper.changeInvasionIcon(houseUlfang, LOTRFAItems.ulfangBattleaxe);
    	LOTRFAReflectionHelper.changeInvasionIcon(houseBor, LOTRFAItems.borGreatsword);
    	LOTRFAReflectionHelper.changeInvasionIcon(dorDaidelos, LOTRMod.spearUruk);
    	LOTRFAReflectionHelper.changeInvasionIcon(brethil, LOTRFAItems.brethilSword);
    	LOTRFAReflectionHelper.changeInvasionIcon(laegrim, LOTRMod.swordWoodElven);
    	LOTRFAReflectionHelper.changeInvasionIcon(hithlum, LOTRMod.swordHighElven);
    	LOTRFAReflectionHelper.changeInvasionIcon(doriath, LOTRMod.swordElven);
    	LOTRFAReflectionHelper.changeInvasionIcon(dorLomin, LOTRFAItems.dorLominSword);
    	LOTRFAReflectionHelper.changeInvasionIcon(angband, LOTRMod.scimitarBlackUruk);
        LOTRFAReflectionHelper.changeInvasionIcon(angbandWarg, LOTRMod.wargBone);
        LOTRFAReflectionHelper.changeInvasionIcon(angbandDaedeloth, LOTRMod.scimitarOrc);
        LOTRFAReflectionHelper.changeInvasionIcon(angbandDaedelothWarg, LOTRMod.wargBone);
        LOTRFAReflectionHelper.changeInvasionIcon(tolInGaurhoth, LOTRMod.swordDolGuldur);
        LOTRFAReflectionHelper.changeInvasionIcon(tolInGaurhothWarg, LOTRMod.wargBone);
        LOTRFAReflectionHelper.changeInvasionIcon(dorDaidelosUtumnoRemnant, LOTRFAItems.utumnoRemnantBerserkerCleaver);
        LOTRFAReflectionHelper.changeInvasionIcon(falathrim, LOTRFAItems.falathrimBow);
        LOTRFAReflectionHelper.changeInvasionIcon(feanorians, LOTRFAItems.feanorianSword);
        LOTRFAReflectionHelper.changeInvasionIcon(gondolin, LOTRMod.swordGondolin);
        LOTRFAReflectionHelper.changeInvasionIcon(nargothrond, LOTRMod.swordRivendell);
        LOTRFAReflectionHelper.changeInvasionIcon(pettyDwarf, LOTRFAItems.pettyDwarfWarhammer);
    }

    private static void addInvasionMobs() {
        angband.invasionMobs.add(new InvasionSpawnEntry(LOTREntityAngbandOrc.class, 10));
        angband.invasionMobs.add(new InvasionSpawnEntry(LOTREntityAngbandOrcArcher.class, 5));
        angband.invasionMobs.add(new InvasionSpawnEntry(LOTREntityAngbandOrcBombardier.class, 2));
        angband.invasionMobs.add(new InvasionSpawnEntry(LOTREntityAngbandBannerBearer.class, 2));
        angband.invasionMobs.add(new InvasionSpawnEntry(LOTREntityAngbandOrcFireThrower.class, 3));
        angband.invasionMobs.add(new InvasionSpawnEntry(LOTREntityAngbandWarTroll.class, 3));

        angbandWarg.invasionMobs.add(new InvasionSpawnEntry(LOTREntityAngbandWarg.class, 10));

        angbandDaedeloth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaedelothOrc.class, 10));
        angbandDaedeloth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaedelothOrcArcher.class, 5));
        angbandDaedeloth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaedelothOrcBombardier.class, 2));
        angbandDaedeloth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaedelothBannerBearer.class, 2));
        angbandDaedeloth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaedelothWarg.class, 10));

        angbandDaedelothWarg.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaedelothWarg.class, 10));

        tolInGaurhoth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityTolInGaurhothOrc.class, 10));
        tolInGaurhoth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityTolInGaurhothOrcArcher.class, 5));
        tolInGaurhoth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityTolInGaurhothBerserker.class, 2));
        tolInGaurhoth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityTolInGaurhothBannerBearer.class, 2));
        tolInGaurhoth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityTolInGaurhothWraith.class, 3));
        tolInGaurhoth.invasionMobs.add(new InvasionSpawnEntry(LOTREntityTolInGaurhothWarg.class, 10));

        tolInGaurhothWarg.invasionMobs.add(new InvasionSpawnEntry(LOTREntityTolInGaurhothWarg.class, 10));

        falathrim.invasionMobs.add(new InvasionSpawnEntry(LOTREntityFalathrimSailor.class, 10));
        falathrim.invasionMobs.add(new InvasionSpawnEntry(LOTREntityFalathrimArcher.class, 5));
        falathrim.invasionMobs.add(new InvasionSpawnEntry(LOTREntityFalathrimMariner.class, 5));
        falathrim.invasionMobs.add(new InvasionSpawnEntry(LOTREntityFalathrimBannerBearer.class, 2));

        feanorians.invasionMobs.add(new InvasionSpawnEntry(LOTREntityFeanorianWarrior.class, 15));
        feanorians.invasionMobs.add(new InvasionSpawnEntry(LOTREntityFeanorianBannerBearer.class, 2));

        gondolin.invasionMobs.add(new InvasionSpawnEntry(LOTREntityGondolinWarrior.class, 15));
        gondolin.invasionMobs.add(new InvasionSpawnEntry(LOTREntityGondolinBannerBearer.class, 2));

        nargothrond.invasionMobs.add(new InvasionSpawnEntry(LOTREntityNargothrondWarrior.class, 15));
        nargothrond.invasionMobs.add(new InvasionSpawnEntry(LOTREntityNargothrondBannerBearer.class, 2));

        pettyDwarf.invasionMobs.add(new InvasionSpawnEntry(LOTREntityPettyDwarfWarrior.class, 10));
        pettyDwarf.invasionMobs.add(new InvasionSpawnEntry(LOTREntityPettyDwarfAxeThrower.class, 5));
        pettyDwarf.invasionMobs.add(new InvasionSpawnEntry(LOTREntityPettyDwarfBannerBearer.class, 2));

        dorDaidelosUtumnoRemnant.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaidelosOrc.class, 5));
        dorDaidelosUtumnoRemnant.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaidelosOrcArcher.class, 5));
        dorDaidelosUtumnoRemnant.invasionMobs.add(new InvasionSpawnEntry(LOTREntityUtumnoRemnantOrc.class, 10));
        dorDaidelosUtumnoRemnant.invasionMobs.add(new InvasionSpawnEntry(LOTREntityUtumnoRemnantCrossbower.class, 5));
        dorDaidelosUtumnoRemnant.invasionMobs.add(new InvasionSpawnEntry(LOTREntityUtumnoRemnantBerserker.class, 5));
        dorDaidelosUtumnoRemnant.invasionMobs.add(new InvasionSpawnEntry(LOTREntityUtumnoRemnantBannerBearer.class, 2));
        dorDaidelosUtumnoRemnant.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaidelosWarg.class, 10));
        
        dorLomin.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorLominLevyman.class, 5));
        dorLomin.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorLominSoldier.class, 10));
        dorLomin.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorLominArcher.class, 5));
        dorLomin.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorLominBannerBearer.class, 2));
        
        hithlum.invasionMobs.add(new InvasionSpawnEntry(LOTREntityHithlumWarrior.class, 15));
        hithlum.invasionMobs.add(new InvasionSpawnEntry(LOTREntityHithlumBannerBearer.class, 2));
        
        doriath.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDoriathWarrior.class, 15));
        doriath.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDoriathBannerBearer.class, 2));
        
        laegrim.invasionMobs.add(new InvasionSpawnEntry(LOTREntityLaegrimWarrior.class, 15));
        laegrim.invasionMobs.add(new InvasionSpawnEntry(LOTREntityLaegrimBannerBearer.class, 2));
        
        houseBor.invasionMobs.add(new InvasionSpawnEntry(LOTREntityBorLevyman.class, 5));
        houseBor.invasionMobs.add(new InvasionSpawnEntry(LOTREntityBorSoldier.class, 10));
        houseBor.invasionMobs.add(new InvasionSpawnEntry(LOTREntityBorArcher.class, 5));
        houseBor.invasionMobs.add(new InvasionSpawnEntry(LOTREntityBorBannerBearer.class, 1));
        
        brethil.invasionMobs.add(new InvasionSpawnEntry(LOTREntityBrethilLevyman.class, 5));
        brethil.invasionMobs.add(new InvasionSpawnEntry(LOTREntityBrethilSoldier.class, 10));
        brethil.invasionMobs.add(new InvasionSpawnEntry(LOTREntityBrethilArcher.class, 5));
        brethil.invasionMobs.add(new InvasionSpawnEntry(LOTREntityBrethilBannerBearer.class, 1));
        
        houseUlfang.invasionMobs.add(new InvasionSpawnEntry(LOTREntityUlfangWarrior.class, 10));
        houseUlfang.invasionMobs.add(new InvasionSpawnEntry(LOTREntityUlfangArcher.class, 5));
        houseUlfang.invasionMobs.add(new InvasionSpawnEntry(LOTREntityUlfangAxeThrower.class, 3));
        houseUlfang.invasionMobs.add(new InvasionSpawnEntry(LOTREntityUlfangBerserker.class, 2));
        houseUlfang.invasionMobs.add(new InvasionSpawnEntry(LOTREntityUlfangBannerBearer.class, 2));
        
        dorDaidelos.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaidelosOrc.class, 20));
        dorDaidelos.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaidelosOrcArcher.class, 10));
        dorDaidelos.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaidelosWarg.class, 20));
        dorDaidelos.invasionMobs.add(new InvasionSpawnEntry(LOTREntityDorDaidelosBannerBearer.class, 5));
    }
}
