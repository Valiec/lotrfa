package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaedelothOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityDorDaedelothWarg;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.world.structure.LOTRChestContents;
import lotr.common.world.structure2.LOTRWorldGenMordorWargPit;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTRWorldGenDDWargPit extends LOTRWorldGenMordorWargPit {

    public LOTRWorldGenDDWargPit(boolean flag) {
        super(flag);
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        int radius = 8;
        int radiusPlusOne = radius + 1;
        int wallThresholdMin = (int) ((radius - 0.25) * (radius - 0.25));
        int wallThresholdMax = radiusPlusOne * radiusPlusOne;
        int baseLevel = -8;
        this.setOriginAndRotation(world, i, j, k, rotation, radiusPlusOne);
        this.setupRandomBlocks(random);
        if (this.restrictions) {
            int minHeight = 0;
            int maxHeight = 0;
            for (int i1 = -radiusPlusOne; i1 <= radiusPlusOne; ++i1) {
                for (int k1 = -radiusPlusOne; k1 <= radiusPlusOne; ++k1) {
                    int distSq = i1 * i1 + k1 * k1;
                    if (distSq >= wallThresholdMax) continue;
                    int j1 = this.getTopBlock(world, i1, k1) - 1;
                    Block block = this.getBlock(world, i1, j1, k1);
                    if (!this.isSurface(world, i1, j1, k1) && block != Blocks.dirt && block != Blocks.gravel && block != Blocks.stone && block != LOTRMod.mordorDirt && block != LOTRMod.mordorGravel) {
                        return false;
                    }
                    if (j1 < minHeight) {
                        minHeight = j1;
                    }
                    if (j1 <= maxHeight) continue;
                    maxHeight = j1;
                }
            }
            if (maxHeight - minHeight > 5) {
                return false;
            }
            this.originY = this.getY(maxHeight);
        }
        for (int i1 = -radiusPlusOne; i1 <= radiusPlusOne; ++i1) {
            for (int k1 = -radiusPlusOne; k1 <= radiusPlusOne; ++k1) {
                int j1;
                int distSq = i1 * i1 + k1 * k1;
                if (distSq >= wallThresholdMax) continue;
                if (distSq >= wallThresholdMin) {
                    this.setBlockAndMetadata(world, i1, 3, k1, this.fenceBlock, this.fenceMeta);
                    this.setBlockAndMetadata(world, i1, 2, k1, this.plankBlock, this.plankMeta);
                    for (j1 = 1; !(j1 < baseLevel && this.isOpaque(world, i1, j1, k1) || this.getY(j1) < 0); --j1) {
                        this.setBlockAndMetadata(world, i1, j1, k1, this.brickBlock, this.brickMeta);
                        this.setGrassToDirt(world, i1, j1 - 1, k1);
                    }
                    continue;
                }
                for (j1 = 3; !(j1 < baseLevel && this.isOpaque(world, i1, j1, k1) || this.getY(j1) < 0); --j1) {
                    if (j1 <= baseLevel) {
                        this.setPitFloor(world, random, i1, j1, k1);
                        this.setGrassToDirt(world, i1, j1 - 1, k1);
                        continue;
                    }
                    this.setAir(world, i1, j1, k1);
                }
            }
        }
        this.placeFenceAndTorch(world, -1, -3, -radius + 1);
        this.placeFenceAndTorch(world, 1, -3, -radius + 1);
        this.placeFenceAndTorch(world, -1, -3, radius - 1);
        this.placeFenceAndTorch(world, 1, -3, radius - 1);
        this.placeFenceAndTorch(world, -radius + 1, -3, -1);
        this.placeFenceAndTorch(world, -radius + 1, -3, 1);
        this.placeFenceAndTorch(world, radius - 1, -3, -1);
        this.placeFenceAndTorch(world, radius - 1, -3, 1);
        this.setBlockAndMetadata(world, 0, 3, -radius, this.fenceGateBlock, 0);
        int j1 = 2;
        while (!this.isOpaque(world, 0, j1, -radius - 1) && this.getY(j1) >= 0) {
            this.setBlockAndMetadata(world, 0, j1, -radius - 1, Blocks.ladder, 2);
            --j1;
        }
        j1 = 2;
        while (!this.isOpaque(world, 0, j1, -radius + 1) && this.getY(j1) >= 0) {
            this.setBlockAndMetadata(world, 0, j1, -radius + 1, Blocks.ladder, 3);
            --j1;
        }
        this.placeChest(world, random, 0, baseLevel + 1, radius - 1, 2, LOTRChestContents.WARG_PIT);
        int wargs = 2 + random.nextInt(5);
        for (int l = 0; l < wargs; ++l) {
            LOTREntityNPC warg = this.getWarg(world);
            this.spawnNPCAndSetHome(warg, world, 0, baseLevel + 1, 0, 8);
        }
        LOTREntityNPC orc = this.getOrc(world);
        orc.setCurrentItemOrArmor(0, new ItemStack(Items.lead));
        this.spawnNPCAndSetHome(orc, world, 0, baseLevel + 1, 0, 8);
        return true;
    }

    private void placeFenceAndTorch(World world, int i, int j, int k) {
        this.setBlockAndMetadata(world, i, j, k, this.fenceBlock, this.fenceMeta);
        this.placeOrcTorch(world, i, j + 1, k);
    }

    @Override
    protected LOTREntityNPC getOrc(final World world) {
        return new LOTREntityDorDaedelothOrc(world);
    }

    @Override
    protected LOTREntityNPC getWarg(final World world) {
        return new LOTREntityDorDaedelothWarg(world);
    }

}
