package eoa.lotrfa.common.world.structure.builds;

import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.LOTREntityFalathrimSmith;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityElf;
import lotr.common.world.structure2.LOTRWorldGenHighElvenForge;
import net.minecraft.world.World;

public class LOTRWorldGenFalathrimSmithy extends LOTRWorldGenHighElvenForge {

    public LOTRWorldGenFalathrimSmithy(boolean flag) {
        super(flag);
        this.brickBlock = LOTRFABlocks.brick;
        this.brickMeta = 5;
        this.pillarBlock = LOTRFABlocks.pillar;
        this.pillarMeta = 6;
        this.slabBlock = LOTRFABlocks.slabSingle2;
        this.slabMeta = 2;
        this.carvedBrickBlock = LOTRFABlocks.carvedBrickFalas;
        this.carvedBrickMeta = 0;
        this.wallBlock = LOTRFABlocks.wall;
        this.wallMeta = 5;
        this.stairBlock = LOTRFABlocks.stairsFalas;
        this.torchBlock = LOTRMod.highElvenTorch;
        this.tableBlock = LOTRFABlocks.craftingTableFalathrim;
        this.barsBlock = LOTRMod.silverBars;
        this.woodBarsBlock = LOTRMod.galadhrimWoodBars;
        this.roofBlock = LOTRMod.clayTileDyed;
        this.roofMeta = 11;
        this.roofStairBlock = LOTRMod.stairsClayTileDyedBlue;
        // TODO Auto-generated constructor stub
    }

    @Override
    protected LOTREntityElf getElf(World world) {
        return new LOTREntityFalathrimSmith(world);
    }

}
