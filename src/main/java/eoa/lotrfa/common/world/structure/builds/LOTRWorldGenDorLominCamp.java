package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.world.structure2.LOTRWorldGenCampBase;
import lotr.common.world.structure2.LOTRWorldGenStructureBase2;
import net.minecraft.world.World;

public class LOTRWorldGenDorLominCamp extends LOTRWorldGenCampBase{
	
	public LOTRWorldGenDorLominCamp(final boolean flag) {
        super(flag);
    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        this.tableBlock = LOTRFABlocks.craftingTableDorLomin;
    }
    
    @Override
    protected LOTRWorldGenStructureBase2 createTent(final boolean flag, final Random random) {
        return new LOTRWorldGenDorLominTent(false);
    }
    
    @Override
    protected LOTREntityNPC getCampCaptain(World world, Random random) {
        switch (random.nextInt(2)) {
            case 0:
                return new LOTREntityDorLominSmith(world);
            case 1:
                return new LOTREntityDorLominCaptain(world);
        }
        return null;
    }
    
    @Override
    protected void placeNPCRespawner(final World world, final Random random, final int i, final int j, final int k) {
        final LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClass(LOTREntityDorLominSoldier.class);
        respawner.setCheckRanges(24, -12, 12, 12);
        respawner.setSpawnRanges(8, -4, 4, 16);
        this.placeNPCRespawner(respawner, world, i, j, k);
    }

}
