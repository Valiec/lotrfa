package eoa.lotrfa.common.world.structure.builds;

import eoa.lotrfa.common.entity.npc.LOTREntityDorDaidelosWarg;
import eoa.lotrfa.common.entity.npc.LOTREntityUtumnoRemnantOrc;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.world.structure2.LOTRWorldGenUrukWargPit;
import net.minecraft.world.World;

public class LOTRWorldGenUtumnoRemnantWargPit extends LOTRWorldGenUrukWargPit{

	public LOTRWorldGenUtumnoRemnantWargPit(boolean flag) {
		super(flag);
	}
	
    @Override
    protected LOTREntityNPC getOrc(World world) {
        return new LOTREntityUtumnoRemnantOrc(world);
    }

    @Override
    protected LOTREntityNPC getWarg(World world) {
        return new LOTREntityDorDaidelosWarg(world);
    }

}
