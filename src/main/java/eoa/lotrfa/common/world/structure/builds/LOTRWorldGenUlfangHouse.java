package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangMan;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRFoods;
import lotr.common.LOTRMod;
import lotr.common.world.structure2.LOTRWorldGenDunlendingHouse;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRWorldGenUlfangHouse extends LOTRWorldGenDunlendingHouse {

    private Block plankBlock;
    private int plankMeta;
    private Block woodBlock;
    private int woodMeta;
    private Block floorBlock;
    private int floorMeta;

    public LOTRWorldGenUlfangHouse(boolean flag) {
        super(flag);
    }

    @Override
    protected void setupRandomBlocks(Random random) {
        int randomFloor = random.nextInt(5);
        if (randomFloor == 0) {
            this.floorBlock = Blocks.cobblestone;
            this.floorMeta = 0;
        }
        else if (randomFloor == 1) {
            this.floorBlock = Blocks.hardened_clay;
            this.floorMeta = 0;
        }
        else if (randomFloor == 2) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 7;
        }
        else if (randomFloor == 3) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 12;
        }
        else if (randomFloor == 4) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 15;
        }
        if (random.nextBoolean()) {
            this.woodBlock = Blocks.log;
            this.woodMeta = 1;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 1;
            this.plankSlabBlock = Blocks.wooden_slab;
            this.plankSlabMeta = 1;
            this.plankStairBlock = Blocks.spruce_stairs;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 1;
            this.fenceGateBlock = LOTRMod.fenceGateSpruce;
            this.doorBlock = LOTRMod.doorSpruce;
        }
        else {
            int randomWood = random.nextInt(2);
            if (randomWood == 0) {
                this.woodBlock = Blocks.log;
                this.woodMeta = 0;
                this.plankBlock = Blocks.planks;
                this.plankMeta = 0;
                this.plankSlabBlock = Blocks.wooden_slab;
                this.plankSlabMeta = 0;
                this.plankStairBlock = Blocks.oak_stairs;
                this.fenceBlock = Blocks.fence;
                this.fenceMeta = 0;
                this.fenceGateBlock = Blocks.fence_gate;
                this.doorBlock = Blocks.wooden_door;
            }
            else if (randomWood == 1) {
                this.woodBlock = LOTRMod.wood5;
                this.woodMeta = 0;
                this.plankBlock = LOTRMod.planks2;
                this.plankMeta = 4;
                this.plankSlabBlock = LOTRMod.woodSlabSingle3;
                this.plankSlabMeta = 4;
                this.plankStairBlock = LOTRMod.stairsPine;
                this.fenceBlock = LOTRMod.fence2;
                this.fenceMeta = 4;
                this.fenceGateBlock = LOTRMod.fenceGatePine;
                this.doorBlock = LOTRMod.doorPine;
            }
        }
        this.roofBlock = LOTRMod.thatch;
        this.roofMeta = 0;
        this.roofSlabBlock = LOTRMod.slabSingleThatch;
        this.roofSlabMeta = 0;
        this.roofStairBlock = LOTRMod.stairsThatch;
        if (random.nextBoolean()) {
            this.barsBlock = Blocks.iron_bars;
            this.barsMeta = 0;
        }
        else {
            this.barsBlock = LOTRMod.bronzeBars;
            this.barsMeta = 0;
        }
        this.bedBlock = random.nextBoolean() ? LOTRMod.furBed : LOTRMod.strawBed;
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        int j1;
        this.setOriginAndRotation(world, i, j, k, rotation, 6);
        this.setupRandomBlocks(random);
        if (this.restrictions) {
            int minHeight = 0;
            int maxHeight = 0;
            for (int i1 = -6; i1 <= 6; ++i1) {
                for (int k1 = -7; k1 <= 7; ++k1) {
                    j1 = this.getTopBlock(world, i1, k1) - 1;
                    if (!this.isSurface(world, i1, j1, k1)) {
                        return false;
                    }
                    if (j1 < minHeight) {
                        minHeight = j1;
                    }
                    if (j1 > maxHeight) {
                        maxHeight = j1;
                    }
                    if (maxHeight - minHeight <= 8) continue;
                    return false;
                }
            }
        }
        for (int i1 = -4; i1 <= 4; ++i1) {
            for (int k1 = -6; k1 <= 5; ++k1) {
                if (k1 >= -5) {
                    j1 = -1;
                    while (!this.isOpaque(world, i1, j1, k1) && this.getY(j1) >= 0) {
                        this.setBlockAndMetadata(world, i1, j1, k1, this.floorBlock, this.floorMeta);
                        this.setGrassToDirt(world, i1, j1 - 1, k1);
                        --j1;
                    }
                }
                for (j1 = 1; j1 <= 6; ++j1) {
                    this.setAir(world, i1, j1, k1);
                }
            }
        }
        this.loadStrScan("dunland_house");
        this.associateBlockMetaAlias("FLOOR", this.floorBlock, this.floorMeta);
        this.associateBlockMetaAlias("WOOD", this.woodBlock, this.woodMeta);
        this.associateBlockMetaAlias("WOOD|8", this.woodBlock, this.woodMeta | 8);
        this.associateBlockMetaAlias("PLANK", this.plankBlock, this.plankMeta);
        this.associateBlockMetaAlias("PLANK_SLAB", this.plankSlabBlock, this.plankSlabMeta);
        this.associateBlockMetaAlias("PLANK_SLAB_INV", this.plankSlabBlock, this.plankSlabMeta | 8);
        this.associateBlockAlias("PLANK_STAIR", this.plankStairBlock);
        this.associateBlockMetaAlias("FENCE", this.fenceBlock, this.fenceMeta);
        this.associateBlockAlias("DOOR", this.doorBlock);
        this.associateBlockMetaAlias("ROOF", this.roofBlock, this.roofMeta);
        this.associateBlockMetaAlias("ROOF_SLAB", this.roofSlabBlock, this.roofSlabMeta);
        this.associateBlockMetaAlias("ROOF_SLAB_INV", this.roofSlabBlock, this.roofSlabMeta | 8);
        this.associateBlockAlias("ROOF_STAIR", this.roofStairBlock);
        this.associateBlockMetaAlias("BARS", this.barsBlock, this.barsMeta);
        this.generateStrScan(world, random, 0, 1, 0);
        this.setBlockAndMetadata(world, 0, 1, 3, this.bedBlock, 0);
        this.setBlockAndMetadata(world, 0, 1, 4, this.bedBlock, 8);
        this.placeChest(world, random, -2, 1, 4, LOTRMod.chestBasket, 2, LOTRFAChestContents.house_ulfang_house);
        this.placeChest(world, random, 2, 1, 4, LOTRMod.chestBasket, 2, LOTRFAChestContents.house_ulfang_house);
        this.placeBarrel(world, random, -3, 2, -3, 4, LOTRFoods.DUNLENDING_DRINK);
        this.placePlate(world, random, -3, 2, -2, LOTRMod.woodPlateBlock, LOTRFoods.DUNLENDING);
        this.placePlate(world, random, -3, 2, -1, LOTRMod.woodPlateBlock, LOTRFoods.DUNLENDING);
        this.placeMug(world, random, 3, 2, -3, 1, LOTRFoods.DUNLENDING_DRINK);
        this.placePlate(world, random, 3, 2, -2, LOTRMod.woodPlateBlock, LOTRFoods.DUNLENDING);
        this.placePlate(world, random, 3, 2, -1, LOTRMod.woodPlateBlock, LOTRFoods.DUNLENDING);
        this.placeFlowerPot(world, -3, 2, 1, this.getRandomFlower(world, random));
        this.placeWeaponRack(world, 0, 3, -4, 4, this.getRandomDunlandWeapon(random));
        this.placeDunlandItemFrame(world, random, -2, 2, -5, 0);
        this.placeDunlandItemFrame(world, random, 2, 2, -5, 0);
        this.placeDunlandItemFrame(world, random, -2, 2, 5, 2);
        this.placeDunlandItemFrame(world, random, 2, 2, 5, 2);
        this.placeWallBanner(world, -2, 4, -6, LOTRFAItemBanner.FABannerTypes.houseUlfang, 2);
        this.placeWallBanner(world, 2, 4, -6, LOTRFAItemBanner.FABannerTypes.houseUlfang, 2);
        LOTREntityUlfangMan dunlending = new LOTREntityUlfangMan(world);
        this.spawnNPCAndSetHome(dunlending, world, 0, 1, 0, 16);
        return true;
    }

}
