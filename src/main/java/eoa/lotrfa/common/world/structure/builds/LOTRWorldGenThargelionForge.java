package eoa.lotrfa.common.world.structure.builds;

import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianSmith;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityElf;
import lotr.common.world.structure2.LOTRWorldGenHighElvenForge;
import net.minecraft.world.World;

public class LOTRWorldGenThargelionForge extends LOTRWorldGenHighElvenForge {

    public LOTRWorldGenThargelionForge(boolean flag) {
        super(flag);
        this.roofStairBlock = LOTRMod.stairsClayTileDyedRed;
        this.roofMeta = 14;
        this.tableBlock = LOTRFABlocks.craftingTableFeanorian;
        this.ruined = true;
        // TODO Auto-generated constructor stub
    }

    @Override
    protected LOTREntityElf getElf(World world) {
        return new LOTREntityFeanorianSmith(world);
    }

}
