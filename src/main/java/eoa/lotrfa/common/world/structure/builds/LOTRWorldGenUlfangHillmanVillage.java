package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import lotr.common.world.structure2.LOTRWorldGenAngmarHillmanVillage;
import lotr.common.world.structure2.LOTRWorldGenStructureBase2;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class LOTRWorldGenUlfangHillmanVillage extends LOTRWorldGenAngmarHillmanVillage {

    private static int VILLAGE_SIZE = 16;

    public LOTRWorldGenUlfangHillmanVillage(boolean flag) {
        super(flag);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        int l;
        this.setOriginAndRotation(world, i, j, k, rotation, 0);
        
        int houses = MathHelper.getRandomIntegerInRange(random, 3, 6);
        int chiefainHouses = MathHelper.getRandomIntegerInRange(random, 0, 1);
        for (l = 0; l < chiefainHouses; ++l) {
            LOTRWorldGenUlfangHillmanChieftainHouse structure = new LOTRWorldGenUlfangHillmanChieftainHouse(this.notifyChanges);
            this.attemptHouseSpawn(structure, world, random);
        }
        for (l = 0; l < houses; ++l) {
            LOTRWorldGenUlfangHillmanHouse structure = new LOTRWorldGenUlfangHillmanHouse(this.notifyChanges);
            this.attemptHouseSpawn(structure, world, random);
        }
        return true;
    }

    private boolean attemptHouseSpawn(LOTRWorldGenStructureBase2 structure, World world, Random random) {
        structure.restrictions = this.restrictions;
        structure.usingPlayer = this.usingPlayer;
        for (int l = 0; l < 16; ++l) {
            int rotation = random.nextInt(4);
            int x = MathHelper.getRandomIntegerInRange(random, (-VILLAGE_SIZE), VILLAGE_SIZE);
            int z = MathHelper.getRandomIntegerInRange(random, (-VILLAGE_SIZE), VILLAGE_SIZE);
            int spawnX = this.getX(x, z);
            int spawnZ = this.getZ(x, z);
            int spawnY = this.getY(this.getTopBlock(world, x, z));
            if (!structure.generateWithSetRotation(world, random, spawnX, spawnY, spawnZ, rotation)) continue;
            return true;
        }
        return false;
    }

}
