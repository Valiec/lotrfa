package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRMod;
import lotr.common.world.structure2.LOTRWorldGenTentBase;
import net.minecraft.init.Blocks;

public class LOTRWorldGenDorDaidelosTent extends LOTRWorldGenTentBase
{
    public LOTRWorldGenDorDaidelosTent(final boolean flag) {
        super(flag);
    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        final int randomWool = random.nextInt(3);
        if (randomWool == 0) {
            this.tentBlock = Blocks.wool;
            this.tentMeta = 15;
        }
        else if (randomWool == 1) {
            this.tentBlock = Blocks.wool;
            this.tentMeta = 12;
        }
        else if (randomWool == 2) {
            this.tentBlock = Blocks.wool;
            this.tentMeta = 7;
        }
        this.fenceBlock = LOTRMod.fence;
        this.fenceMeta = 3;
        this.chestContents = LOTRFAChestContents.dor_daidelos_tent;
        this.hasOrcTorches = true;
    }
}
