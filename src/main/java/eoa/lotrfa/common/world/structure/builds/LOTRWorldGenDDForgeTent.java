package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import lotr.common.LOTRMod;

public class LOTRWorldGenDDForgeTent extends LOTRWorldGenDDTent {

    public LOTRWorldGenDDForgeTent(boolean flag) {
        super(flag);
    }

    @Override
    protected void setupRandomBlocks(Random random) {
        super.setupRandomBlocks(random);
        this.tentBlock = LOTRMod.brick;
        this.tentMeta = 0;
        this.fenceBlock = LOTRMod.wall;
        this.fenceMeta = 1;
        this.hasOrcForge = true;
    }

}
