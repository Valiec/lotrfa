package eoa.lotrfa.common.world.structure.builds;

import lotr.common.LOTRMod;

public class LOTRWorldGenFeanorianHillTower extends LOTRWorldGenFeanorianTower {

    public LOTRWorldGenFeanorianHillTower(boolean flag) {
        super(flag);
        this.brickBlock = LOTRMod.brick4;
        this.brickMeta = 15;
        this.brickSlabBlock = LOTRMod.slabSingle9;
        this.brickSlabMeta = 0;
        this.brickStairBlock = LOTRMod.stairsChalkBrick;
        this.brickWallBlock = LOTRMod.wall3;
        this.brickWallMeta = 6;
        this.pillarBlock = LOTRMod.pillar2;
        this.pillarMeta = 1;
        this.floorBlock = LOTRMod.slabDouble8;
        this.floorMeta = 7;
        // this.roofBlock = this.brickBlock;
        // this.roofMeta = this.brickMeta;
        this.roofMeta = 14;
        this.roofSlabBlock = LOTRMod.slabClayTileDyedSingle2;
        this.roofSlabMeta = 6;
        // this.roofSlabBlock = this.brickSlabBlock;
        // this.roofSlabMeta = this.brickSlabMeta;
        this.roofStairBlock = LOTRMod.stairsClayTileDyedRed;
        // TODO Auto-generated constructor stub
    }

}
