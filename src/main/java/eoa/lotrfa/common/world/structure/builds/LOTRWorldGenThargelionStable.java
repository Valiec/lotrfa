package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import com.google.common.math.IntMath;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianStablemaster;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import lotr.common.LOTRMod;
import lotr.common.world.structure.LOTRChestContents;
import lotr.common.world.structure2.LOTRWorldGenDolAmrothStables;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRWorldGenThargelionStable extends LOTRWorldGenDolAmrothStables {

    protected Block roofStairBlock = LOTRMod.stairsClayTileDyedRed;
    protected int roofMeta = 14;
    protected Block roofSlabBlock = LOTRMod.slabClayTileDyedSingle2;
    protected int roofSlabMeta = 6;
    protected Block brickBlock = LOTRMod.brick3;
    protected int brickMeta = 2;
    protected Block brickSlabBlock = LOTRMod.slabSingle5;
    protected int brickSlabMeta = 5;
    protected Block brickStairBlock = LOTRMod.stairsHighElvenBrick;
    protected Block brickWallBlock = LOTRMod.wall2;
    protected int brickWallMeta = 11;
    protected Block pillarBlock = LOTRMod.pillar;
    protected int pillarMeta = 10;
    protected Block floorBlock = Blocks.double_stone_slab;
    protected int floorMeta = 0;
    protected Block plankBlock = Blocks.planks;
    protected int plankMeta = 2;
    protected Block plankSlabBlock = Blocks.wooden_slab;
    protected int plankSlabMeta = 2;
    protected Block plankStairBlock = Blocks.birch_stairs;
    protected Block fenceBlock = Blocks.fence;
    protected int fenceMeta = 2;
    protected Block leafBlock = Blocks.leaves;
    protected int leafMeta = 6;
    private int rockSlabMeta = 5;
    private Block rockSlabBlock = LOTRMod.slabSingle5;
    private Block doubleRockSlabBlock = LOTRMod.slabDouble5;
    private Block stairBlock = LOTRMod.stairsHighElvenBrick;
    private Block wallBlock = LOTRMod.wall2;
    private int wallMeta = 11;
    private Block roofBlock = LOTRMod.clayTileDyed;
    private int slabMeta = 5;
    private Block slabBlock = LOTRMod.slabSingle5;
    private Block logBlock = Blocks.log;
    private int logMeta = 0;
    private Block gateBlock = Blocks.fence_gate;
    private Block woodBeamBlock = LOTRMod.woodBeamV1;
    private int woodBeamMeta = 0;

    public LOTRWorldGenThargelionStable(boolean flag) {
        super(flag);

        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        int i1;
        int j1;
        int i12;
        int i132;
        int k1;
        int k122;
        int j12;
        int k13;
        int i14;
        int j13;
        int k14;
        int k15;
        int i15;
        int i16;
        int i22;
        int i17;
        int j14;
        int i18;
        int k162;
        int k17;
        int j15;
        this.setOriginAndRotation(world, i, j, k, rotation, 2);
        if (this.restrictions) {
            int minHeight = 0;
            int maxHeight = 0;
            for (i17 = -9; i17 <= 9; ++i17) {
                for (k122 = -1; k122 <= 19; ++k122) {
                    j15 = this.getTopBlock(world, i17, k122);
                    Block block = this.getBlock(world, i17, j15 - 1, k122);
                    if (block != Blocks.grass) {
                        return false;
                    }
                    if (j15 < minHeight) {
                        minHeight = j15;
                    }
                    if (j15 > maxHeight) {
                        maxHeight = j15;
                    }
                    if (maxHeight - minHeight <= 7) continue;
                    return false;
                }
            }
        }
        for (int segment = 0; segment <= 2; ++segment) {
            int i19;
            int k18;
            int i1102;
            int kz = segment * 4;
            for (i17 = -8; i17 <= 8; ++i17) {
                for (k122 = kz; k122 <= kz + 3; ++k122) {
                    for (j15 = 0; !(j15 != 0 && this.isOpaque(world, i17, j15, k122) || this.getY(j15) < 0); --j15) {
                        this.setBlockAndMetadata(world, i17, j15, k122, this.brickBlock, this.brickMeta);
                        this.setGrassToDirt(world, i17, j15 - 1, k122);
                    }
                    for (j15 = 1; j15 <= 7; ++j15) {
                        this.setAir(world, i17, j15, k122);
                    }
                }
            }
            this.placeWoodPillar(world, -8, kz);
            this.placeWoodPillar(world, 8, kz);
            int[] i111 = new int[] {-3, 3};
            k122 = i111.length;
            for (j15 = 0; j15 < k122; ++j15) {
                i1102 = i111[j15];
                if (segment == 0) {
                    this.placeWoodPillar(world, i1102, kz);
                    continue;
                }
                for (j1 = 1; j1 <= 3; ++j1) {
                    this.setBlockAndMetadata(world, i1102, j1, kz, this.logBlock, this.logMeta);
                }
            }
            for (i19 = -7; i19 <= -4; ++i19) {
                this.setBlockAndMetadata(world, i19, 1, kz, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, i19, 2, kz, this.wallBlock, this.wallMeta);
                this.setBlockAndMetadata(world, i19, 4, kz, this.brickBlock, this.brickMeta);
            }
            for (i19 = 4; i19 <= 7; ++i19) {
                this.setBlockAndMetadata(world, i19, 1, kz, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, i19, 2, kz, this.wallBlock, this.wallMeta);
                this.setBlockAndMetadata(world, i19, 4, kz, this.brickBlock, this.brickMeta);
            }
            this.setBlockAndMetadata(world, -4, 2, kz, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, -4, 3, kz, this.wallBlock, this.wallMeta);
            this.setBlockAndMetadata(world, 4, 2, kz, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, 4, 3, kz, this.wallBlock, this.wallMeta);
            for (k18 = kz + 1; k18 <= kz + 3; ++k18) {
                this.setBlockAndMetadata(world, -8, 1, k18, this.doubleRockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, -8, 2, k18, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, -8, 3, k18, this.rockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, -8, 4, k18, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, 8, 1, k18, this.doubleRockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, 8, 2, k18, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, 8, 3, k18, this.rockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, 8, 4, k18, this.brickBlock, this.brickMeta);
            }
            for (int j16 = 1; j16 <= 3; ++j16) {
                // this.setBlockAndMetadata(world, -2, j16, kz, this.fenceBlock, this.brickMeta);
                // this.setBlockAndMetadata(world, 2, j16, kz, this.fenceBlock, this.brickMeta);
            }
            for (k18 = kz + 1; k18 <= kz + 3; ++k18) {
                for (i132 = -7; i132 <= -3; ++i132) {
                    this.setBlockAndMetadata(world, i132, 0, k18, this.doubleRockSlabBlock, this.rockSlabMeta);
                    if (random.nextInt(3) == 0) continue;
                    this.setBlockAndMetadata(world, i132, 1, k18, LOTRMod.thatchFloor, 0);
                }
                for (i132 = 3; i132 <= 7; ++i132) {
                    this.setBlockAndMetadata(world, i132, 0, k18, this.doubleRockSlabBlock, this.rockSlabMeta);
                    if (random.nextInt(3) == 0) continue;
                    this.setBlockAndMetadata(world, i132, 1, k18, LOTRMod.thatchFloor, 0);
                }
            }
            for (int i11021 : new int[] {-7, 7}) {
                this.setBlockAndMetadata(world, i11021, 1, kz + 1, Blocks.hay_block, 0);
                this.setBlockAndMetadata(world, i11021, 1, kz + 2, Blocks.hay_block, 0);
                // this.setBlockAndMetadata(world, i11021, 1, kz + 3, this.fenceBlock, this.fenceMeta);
            }
            for (int i11021 : new int[] {-3, 3}) {
                this.setBlockAndMetadata(world, i11021, 1, kz + 1, this.gateBlock, 1);
                this.setBlockAndMetadata(world, i11021, 1, kz + 2, this.gateBlock, 1);
                // this.setBlockAndMetadata(world, i11021, 1, kz + 3, this.fenceBlock, this.fenceMeta);
                // this.setBlockAndMetadata(world, i11021, 2, kz + 3, Blocks.torch, 5);
            }
            int[] k19 = new int[] {-1, 1};
            i132 = k19.length;
            for (int i112 = -7; i112 <= 7; ++i112) {
                for (k122 = kz + 1; k122 <= kz + 3; ++k122) {
                    this.setBlockAndMetadata(world, i112, 4, k122, this.rockSlabBlock, this.rockSlabMeta | 8);
                }
                if (segment <= 0) continue;
                if (Math.abs(i112) <= 1) {
                    this.setBlockAndMetadata(world, i112, 4, kz, this.rockSlabBlock, this.rockSlabMeta | 8);
                    continue;
                }
                this.setBlockAndMetadata(world, i112, 4, kz, this.brickBlock, this.brickMeta);
            }
            for (int i11021 : new int[] {-3, 3}) {
                this.setBlockAndMetadata(world, i11021, 4, kz + 1, this.doubleRockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, i11021, 4, kz + 3, this.doubleRockSlabBlock, this.rockSlabMeta);
            }
            for (int i113 = -8; i113 <= 8; ++i113) {
                this.setBlockAndMetadata(world, i113, 5, kz, this.brickBlock, this.brickMeta);
            }
            for (int i11021 : new int[] {-7, 4}) {
                this.setBlockAndMetadata(world, i11021, 6, kz, this.stairBlock, 1);
                this.setBlockAndMetadata(world, i11021 + 1, 6, kz, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, i11021 + 1, 7, kz, this.stairBlock, 1);
                this.setBlockAndMetadata(world, i11021 + 2, 6, kz, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, i11021 + 2, 7, kz, this.stairBlock, 0);
                this.setBlockAndMetadata(world, i11021 + 3, 6, kz, this.stairBlock, 0);
                for (k13 = kz + 1; k13 <= kz + 3; ++k13) {
                    this.setBlockAndMetadata(world, i11021, 5, k13, this.roofStairBlock, 1);
                    this.setBlockAndMetadata(world, i11021 + 1, 5, k13, this.brickBlock, this.brickMeta);
                    this.setBlockAndMetadata(world, i11021 + 1, 6, k13, this.roofStairBlock, 1);
                    this.setBlockAndMetadata(world, i11021 + 2, 5, k13, this.brickBlock, this.brickMeta);
                    this.setBlockAndMetadata(world, i11021 + 2, 6, k13, this.roofStairBlock, 0);
                    this.setBlockAndMetadata(world, i11021 + 3, 5, k13, this.roofStairBlock, 0);
                }
            }
            this.setBlockAndMetadata(world, -2, 6, kz, this.stairBlock, 1);
            this.setBlockAndMetadata(world, -1, 6, kz, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, -1, 7, kz, this.stairBlock, 1);
            this.setBlockAndMetadata(world, 0, 6, kz, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, 0, 7, kz, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, 0, 8, kz, this.slabBlock, this.slabMeta);
            this.setBlockAndMetadata(world, 1, 6, kz, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, 1, 7, kz, this.stairBlock, 0);
            this.setBlockAndMetadata(world, 2, 6, kz, this.stairBlock, 0);
            for (int k110 = kz + 1; k110 <= kz + 3; ++k110) {
                this.setBlockAndMetadata(world, -2, 5, k110, this.roofStairBlock, 1);
                this.setBlockAndMetadata(world, -1, 5, k110, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, -1, 6, k110, this.roofStairBlock, 1);
                this.setBlockAndMetadata(world, 0, 5, k110, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, 0, 6, k110, this.roofBlock, this.roofMeta);
                this.setBlockAndMetadata(world, 0, 7, k110, this.roofSlabBlock, this.roofSlabMeta);
                this.setBlockAndMetadata(world, 1, 5, k110, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, 1, 6, k110, this.roofStairBlock, 0);
                this.setBlockAndMetadata(world, 2, 5, k110, this.roofStairBlock, 0);
            }
            int[] k110 = new int[] {-8, -3, 3, 8};
            k122 = k110.length;
            for (j15 = 0; j15 < k122; ++j15) {
                i1102 = k110[j15];
                for (k13 = kz + 1; k13 <= kz + 3; ++k13) {
                    this.setBlockAndMetadata(world, i1102, 5, k13, this.wallBlock, this.wallMeta);
                }
            }
        }
        for (i12 = -9; i12 <= 9; ++i12) {
            i22 = Math.abs(i12);
            if (i22 <= 2) {
                this.setBlockAndMetadata(world, i12, 0, -1, this.doubleRockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, i12, 0, -2, this.doubleRockSlabBlock, this.rockSlabMeta);
                continue;
            }
            this.placeGrassFoundation(world, i12, -1);
            if (i22 == 3 || i22 == 8) {
                for (j13 = 1; j13 <= 4; ++j13) {
                    this.setBlockAndMetadata(world, i12, j13, -1, this.wallBlock, this.wallMeta);
                }
                continue;
            }
            // this.setBlockAndMetadata(world, i12, 1, -1, this.leafBlock, this.leafMeta);
        }
        for (int k111 = 0; k111 <= 11; ++k111) {
            int[] i221 = new int[] {-9, 9};
            j13 = i221.length;
            for (k122 = 0; k122 < j13; ++k122) {
                i15 = i221[k122];
                this.placeGrassFoundation(world, i15, k111);
                if (k111 % 4 == 0) {
                    for (j12 = 1; j12 <= 4; ++j12) {
                        this.setBlockAndMetadata(world, i15, j12, k111, this.wallBlock, this.wallMeta);
                    }
                    continue;
                }
                // this.setBlockAndMetadata(world, i15, 1, k111, this.leafBlock, this.leafMeta);
            }
            if (k111 % 4 != 0) continue;
            this.setBlockAndMetadata(world, -9, 5, k111, this.stairBlock, 1);
            this.setBlockAndMetadata(world, 9, 5, k111, this.stairBlock, 0);
        }
        this.setBlockAndMetadata(world, -1, 5, 0, this.stairBlock, 4);
        this.setBlockAndMetadata(world, 0, 5, 0, this.stairBlock, 6);
        this.setBlockAndMetadata(world, 1, 5, 0, this.stairBlock, 5);
        this.setBlockAndMetadata(world, 0, 6, 0, LOTRMod.brick2, 13);
        for (i12 = -1; i12 <= 1; ++i12) {
            this.setBlockAndMetadata(world, i12, 0, 0, this.doubleRockSlabBlock, this.rockSlabMeta);
            for (j14 = 1; j14 <= 4; ++j14) {
                this.setBlockAndMetadata(world, i12, j14, 0, LOTRMod.gateHighElven, 2);
            }
        }
        for (int i1321 : new int[] {-2, 2}) {
            for (j15 = 1; j15 <= 4; ++j15) {
                this.setBlockAndMetadata(world, i1321, j15, 0, this.brickBlock, this.brickMeta);
            }
            this.placeWallBanner(world, i1321, 4, 0, LOTRFAItemBanner.FABannerTypes.feanorian, 2);
        }
        this.setBlockAndMetadata(world, -8, 5, -1, this.stairBlock, 1);
        this.setBlockAndMetadata(world, 8, 5, -1, this.stairBlock, 0);
        int[] i114 = new int[] {-7, 4};
        j14 = i114.length;
        for (j13 = 0; j13 < j14; ++j13) {
            i132 = i114[j13];
            this.setBlockAndMetadata(world, i132 + 0, 5, -1, this.stairBlock, 4);
            this.setBlockAndMetadata(world, i132 + 0, 6, -1, this.stairBlock, 1);
            this.setBlockAndMetadata(world, i132 + 1, 6, -1, this.stairBlock, 4);
            this.setBlockAndMetadata(world, i132 + 1, 7, -1, this.stairBlock, 1);
            this.setBlockAndMetadata(world, i132 + 2, 6, -1, this.stairBlock, 5);
            this.setBlockAndMetadata(world, i132 + 2, 7, -1, this.stairBlock, 0);
            this.setBlockAndMetadata(world, i132 + 3, 5, -1, this.stairBlock, 5);
            this.setBlockAndMetadata(world, i132 + 3, 6, -1, this.stairBlock, 0);
            this.setBlockAndMetadata(world, i132 + 1, 5, 0, this.stairBlock, 4);
            this.setBlockAndMetadata(world, i132 + 2, 5, 0, this.stairBlock, 5);
        }
        this.setBlockAndMetadata(world, -3, 5, -1, this.brickBlock, this.brickMeta);
        this.setBlockAndMetadata(world, -2, 5, -1, this.stairBlock, 4);
        this.setBlockAndMetadata(world, -2, 6, -1, this.stairBlock, 1);
        this.setBlockAndMetadata(world, -1, 6, -1, this.stairBlock, 4);
        this.setBlockAndMetadata(world, -1, 7, -1, this.stairBlock, 1);
        this.setBlockAndMetadata(world, 0, 7, -1, this.brickBlock, this.brickMeta);
        this.setBlockAndMetadata(world, 0, 8, -1, this.slabBlock, this.slabMeta);
        this.setBlockAndMetadata(world, 1, 6, -1, this.stairBlock, 5);
        this.setBlockAndMetadata(world, 1, 7, -1, this.stairBlock, 0);
        this.setBlockAndMetadata(world, 2, 5, -1, this.stairBlock, 5);
        this.setBlockAndMetadata(world, 2, 6, -1, this.stairBlock, 0);
        this.setBlockAndMetadata(world, 3, 5, -1, this.brickBlock, this.brickMeta);
        for (k14 = 1; k14 <= 3; ++k14) {
            for (i1 = -1; i1 <= 1; ++i1) {
                if (k14 == 3 && Math.abs(i1) >= 1) continue;
                this.setAir(world, i1, 4, k14);
            }
        }
        for (k14 = 1; k14 <= 11; ++k14) {
            this.setBlockAndMetadata(world, 0, 0, k14, this.doubleRockSlabBlock, this.rockSlabMeta);
        }
        for (int i115 = -9; i115 <= 9; ++i115) {
            for (k162 = 12; k162 <= 18; ++k162) {
                for (j13 = 0; !(j13 != 0 && this.isOpaque(world, i115, j13, k162) || this.getY(j13) < 0); --j13) {
                    this.setBlockAndMetadata(world, i115, j13, k162, this.brickBlock, this.brickMeta);
                    this.setGrassToDirt(world, i115, j13 - 1, k162);
                }
                for (j13 = 1; j13 <= 9; ++j13) {
                    this.setAir(world, i115, j13, k162);
                }
            }
        }
        int[] i115 = new int[] {12, 18};
        k162 = i115.length;
        for (j13 = 0; j13 < k162; ++j13) {
            k122 = i115[j13];
            for (i15 = -9; i15 <= 9; ++i15) {
                this.setBlockAndMetadata(world, i15, 1, k122, this.doubleRockSlabBlock, this.rockSlabMeta);
                for (j12 = 2; j12 <= 6; ++j12) {
                    this.setBlockAndMetadata(world, i15, j12, k122, this.brickBlock, this.brickMeta);
                }
            }
            int[] i116 = new int[] {-9, 9};
            j12 = i116.length;
            for (k13 = 0; k13 < j12; ++k13) {
                int i117 = i116[k13];
                this.placeWoodPillar(world, i117, k122);
                this.setBlockAndMetadata(world, i117, 4, k122, this.doubleRockSlabBlock, this.rockSlabMeta);
                for (int j17 = 5; j17 <= 7; ++j17) {
                    this.setBlockAndMetadata(world, i117, j17, k122, this.pillarBlock, this.pillarMeta);
                }
                this.setBlockAndMetadata(world, i117, 8, k122, this.rockSlabBlock, this.rockSlabMeta);
            }
            for (i15 = -8; i15 <= 8; ++i15) {
                int i23 = Math.abs(i15);
                if (i23 >= 5) {
                    if (i23 % 2 == 0) {
                        this.setBlockAndMetadata(world, i15, 7, k122, this.slabBlock, this.slabMeta);
                    }
                    else {
                        this.setBlockAndMetadata(world, i15, 7, k122, this.brickBlock, this.brickMeta);
                    }
                }
                if (i23 == 4) {
                    for (j1 = 5; j1 <= 10; ++j1) {
                        this.setBlockAndMetadata(world, i15, j1, k122, this.pillarBlock, this.pillarMeta);
                    }
                    this.setBlockAndMetadata(world, i15, 11, k122, this.slabBlock, this.slabMeta);
                }
                if (i23 > 3) continue;
                for (j1 = 7; j1 <= 8; ++j1) {
                    this.setBlockAndMetadata(world, i15, j1, k122, this.brickBlock, this.brickMeta);
                }
                this.setBlockAndMetadata(world, i15, 9, k122, this.doubleRockSlabBlock, this.rockSlabMeta);
                if (i23 >= 1) {
                    this.setBlockAndMetadata(world, i15, 10, k122, this.brickBlock, this.brickMeta);
                    if (i23 % 2 == 0) {
                        this.setBlockAndMetadata(world, i15, 11, k122, this.slabBlock, this.slabMeta);
                    }
                }
                if (i23 != 0) continue;
                this.setBlockAndMetadata(world, i15, 10, k122, this.slabBlock, this.slabMeta);
            }
        }
        for (k17 = 13; k17 <= 17; ++k17) {
            int[] k1621 = new int[] {-4, 4};
            j13 = k1621.length;
            for (k122 = 0; k122 < j13; ++k122) {
                i15 = k1621[k122];
                this.setBlockAndMetadata(world, i15, 9, k17, this.doubleRockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, i15, 10, k17, this.brickBlock, this.brickMeta);
                if (k17 % 2 != 0) continue;
                this.setBlockAndMetadata(world, i15, 11, k17, this.slabBlock, this.slabMeta);
            }
        }
        for (int i118 = -3; i118 <= 3; ++i118) {
            i22 = Math.abs(i118);
            for (k1 = 13; k1 <= 17; ++k1) {
                this.setBlockAndMetadata(world, i118, 9, k1, this.doubleRockSlabBlock, this.rockSlabMeta);
            }
            for (k1 = 14; k1 <= 16; ++k1) {
                this.setBlockAndMetadata(world, i118, 10, k1, this.doubleRockSlabBlock, this.rockSlabMeta);
                if (i22 <= 2) {
                    this.setBlockAndMetadata(world, i118, 11, k1, this.brickBlock, this.brickMeta);
                }
                if (i22 > 1) continue;
                this.setBlockAndMetadata(world, i118, 12, k1, this.brickBlock, this.brickMeta);
            }
        }
        for (k17 = 13; k17 <= 17; ++k17) {
            this.setBlockAndMetadata(world, -3, 11, k17, this.roofStairBlock, 1);
            this.setBlockAndMetadata(world, -2, 12, k17, this.roofStairBlock, 1);
            this.setBlockAndMetadata(world, -1, 13, k17, this.roofStairBlock, 1);
            this.setBlockAndMetadata(world, 0, 13, k17, this.roofBlock, this.roofMeta);
            this.setBlockAndMetadata(world, 0, 14, k17, this.roofSlabBlock, this.roofSlabMeta);
            this.setBlockAndMetadata(world, 1, 13, k17, this.roofStairBlock, 0);
            this.setBlockAndMetadata(world, 2, 12, k17, this.roofStairBlock, 0);
            this.setBlockAndMetadata(world, 3, 11, k17, this.roofStairBlock, 0);
        }
        int[] k112 = new int[] {13, 17};
        i22 = k112.length;
        for (k1 = 0; k1 < i22; ++k1) {
            k122 = k112[k1];
            this.setBlockAndMetadata(world, -3, 10, k122, this.roofStairBlock, 4);
            this.setBlockAndMetadata(world, -2, 11, k122, this.roofStairBlock, 4);
            this.setBlockAndMetadata(world, -1, 12, k122, this.roofStairBlock, 4);
            this.setBlockAndMetadata(world, 1, 12, k122, this.roofStairBlock, 5);
            this.setBlockAndMetadata(world, 2, 11, k122, this.roofStairBlock, 5);
            this.setBlockAndMetadata(world, 3, 10, k122, this.roofStairBlock, 5);
        }
        this.placeWallBanner(world, 0, 12, 14, LOTRFAItemBanner.FABannerTypes.feanorian, 2);
        this.placeWallBanner(world, 0, 12, 16, LOTRFAItemBanner.FABannerTypes.feanorian, 0);
        this.placeWallBanner(world, -4, 9, 12, LOTRFAItemBanner.FABannerTypes.feanorian, 2);
        this.setBlockAndMetadata(world, -3, 9, 12, this.doubleRockSlabBlock, this.rockSlabMeta);
        this.setBlockAndMetadata(world, -2, 9, 12, this.stairBlock, 6);
        this.setBlockAndMetadata(world, -2, 8, 12, LOTRMod.stainedGlassPane, 14);
        this.setBlockAndMetadata(world, -1, 9, 12, this.doubleRockSlabBlock, this.rockSlabMeta);
        this.setBlockAndMetadata(world, 0, 9, 12, this.stairBlock, 6);
        this.setBlockAndMetadata(world, 0, 8, 12, LOTRMod.stainedGlassPane, 0);
        this.setBlockAndMetadata(world, 1, 9, 12, this.doubleRockSlabBlock, this.rockSlabMeta);
        this.setBlockAndMetadata(world, 2, 9, 12, this.stairBlock, 6);
        this.setBlockAndMetadata(world, 2, 8, 12, LOTRMod.stainedGlassPane, 14);
        this.setBlockAndMetadata(world, 3, 9, 12, this.doubleRockSlabBlock, this.rockSlabMeta);
        this.placeWallBanner(world, 4, 9, 12, LOTRFAItemBanner.FABannerTypes.feanorian, 2);
        this.placeWoodPillar(world, -3, 12);
        this.placeWoodPillar(world, 3, 12);
        this.placeWoodPillar(world, -6, 18);
        this.placeWoodPillar(world, -2, 18);
        this.placeWoodPillar(world, 2, 18);
        this.placeWoodPillar(world, 6, 18);
        for (k15 = 13; k15 <= 17; ++k15) {
            int[] i24 = new int[] {-9, 9};
            k1 = i24.length;
            for (k122 = 0; k122 < k1; ++k122) {
                i15 = i24[k122];
                this.setBlockAndMetadata(world, i15, 1, k15, this.doubleRockSlabBlock, this.rockSlabMeta);
                for (j12 = 2; j12 <= 3; ++j12) {
                    this.setBlockAndMetadata(world, i15, j12, k15, this.brickBlock, this.brickMeta);
                }
                this.setBlockAndMetadata(world, i15, 4, k15, this.doubleRockSlabBlock, this.rockSlabMeta);
                for (j12 = 5; j12 <= 6; ++j12) {
                    this.setBlockAndMetadata(world, i15, j12, k15, this.brickBlock, this.brickMeta);
                }
                if (k15 % 2 == 1) {
                    this.setBlockAndMetadata(world, i15, 7, k15, this.slabBlock, this.slabMeta);
                    continue;
                }
                this.setBlockAndMetadata(world, i15, 7, k15, this.brickBlock, this.brickMeta);
            }
        }
        for (k15 = 13; k15 <= 17; ++k15) {
            for (int step = 0; step <= 4; ++step) {
                this.setBlockAndMetadata(world, -8 + step, 4 + step, k15, this.stairBlock, 4);
                this.setBlockAndMetadata(world, 8 - step, 4 + step, k15, this.stairBlock, 5);
            }
            this.setBlockAndMetadata(world, -8, 5, k15, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, -8, 6, k15, Blocks.grass, 0);
            this.setBlockAndMetadata(world, -7, 6, k15, Blocks.grass, 0);
            this.setBlockAndMetadata(world, 8, 5, k15, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, 8, 6, k15, Blocks.grass, 0);
            this.setBlockAndMetadata(world, 7, 6, k15, Blocks.grass, 0);
        }
        for (i16 = -8; i16 <= 8; ++i16) {
            i22 = Math.abs(i16);
            if (i22 == 8) {
                for (k1 = 13; k1 <= 17; ++k1) {
                    // this.setBlockAndMetadata(world, i16, 7, k1, this.leafBlock, this.leafMeta);
                }
                for (k1 = 14; k1 <= 16; ++k1) {
                    // this.setBlockAndMetadata(world, i16, 8, k1, this.leafBlock, this.leafMeta);
                }
                continue;
            }
            if (i22 == 7) {
                for (k1 = 13; k1 <= 17; ++k1) {
                    // this.setBlockAndMetadata(world, i16, 7, k1, this.leafBlock, this.leafMeta);
                }
                // this.setBlockAndMetadata(world, i16, 8, 13, this.leafBlock, this.leafMeta);
                // this.setBlockAndMetadata(world, i16, 8, 17, this.leafBlock, this.leafMeta);
                this.setBlockAndMetadata(world, i16, 7, 15, this.rockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, i16, 6, 15, this.brickBlock, this.brickMeta);
                continue;
            }
            if (i22 == 6) {
                // this.setBlockAndMetadata(world, i16, 7, 13, this.leafBlock, this.leafMeta);
                // this.setBlockAndMetadata(world, i16, 7, 17, this.leafBlock, this.leafMeta);
                // this.setBlockAndMetadata(world, i16, 8, 13, this.leafBlock, this.leafMeta);
                // this.setBlockAndMetadata(world, i16, 8, 17, this.leafBlock, this.leafMeta);
                this.setBlockAndMetadata(world, i16, 7, 14, this.rockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, i16, 7, 15, this.rockSlabBlock, this.rockSlabMeta);
                this.setBlockAndMetadata(world, i16, 7, 16, this.rockSlabBlock, this.rockSlabMeta);
                continue;
            }
            if (i22 != 5) continue;
            // this.setBlockAndMetadata(world, i16, 8, 13, this.leafBlock, this.leafMeta);
            // this.setBlockAndMetadata(world, i16, 8, 14, this.leafBlock, this.leafMeta);
            // this.setBlockAndMetadata(world, i16, 8, 16, this.leafBlock, this.leafMeta);
            // this.setBlockAndMetadata(world, i16, 8, 17, this.leafBlock, this.leafMeta);
        }
        for (i16 = -8; i16 <= 8; ++i16) {
            for (k162 = 13; k162 <= 17; ++k162) {
                this.setBlockAndMetadata(world, i16, 0, k162, this.doubleRockSlabBlock, this.rockSlabMeta);
            }
        }
        for (i16 = -2; i16 <= 2; ++i16) {
            this.setBlockAndMetadata(world, i16, 0, 12, this.doubleRockSlabBlock, this.rockSlabMeta);
            for (j14 = 1; j14 <= 3; ++j14) {
                this.setAir(world, i16, j14, 12);
            }
        }
        for (int j18 = 1; j18 <= 3; ++j18) {
            int[] j19 = new int[] {-2, 2};
            k1 = j19.length;
            for (k122 = 0; k122 < k1; ++k122) {
                i15 = j19[k122];
                this.setBlockAndMetadata(world, i15, j18, 12, this.brickBlock, this.brickMeta);
            }
            for (int i119 = -1; i119 <= 1; ++i119) {
                this.setBlockAndMetadata(world, i119, j18, 12, LOTRMod.gateHighElven, 2);
            }
        }
        for (int f : new int[] {-1, 1}) {
            this.setBlockAndMetadata(world, 4 * f, 4, 13, this.slabBlock, this.slabMeta | 8);
            this.setBlockAndMetadata(world, 3 * f, 5, 13, this.slabBlock, this.slabMeta);
            this.setBlockAndMetadata(world, 2 * f, 5, 13, this.slabBlock, this.slabMeta | 8);
            this.setBlockAndMetadata(world, 1 * f, 6, 13, this.slabBlock, this.slabMeta);
            this.setBlockAndMetadata(world, 0 * f, 6, 13, this.slabBlock, this.slabMeta);
            this.placeWallBanner(world, 3 * f, 4, 12, LOTRFAItemBanner.FABannerTypes.feanorian, 0);
            // this.setBlockAndMetadata(world, 6 * f, 3, 13, Blocks.torch, 3);
            for (j15 = 5; j15 <= 6; ++j15) {
                this.setBlockAndMetadata(world, 6 * f, j15, 18, this.woodBeamBlock, this.woodBeamMeta);
            }
            for (j15 = 5; j15 <= 7; ++j15) {
                this.setBlockAndMetadata(world, 2 * f, j15, 18, this.woodBeamBlock, this.woodBeamMeta);
            }
            this.placeWallBanner(world, 6 * f, 5, 18, LOTRFAItemBanner.FABannerTypes.feanorian, 0);
            this.placeWallBanner(world, 6 * f, 5, 18, LOTRFAItemBanner.FABannerTypes.feanorian, 2);
            this.placeWallBanner(world, 2 * f, 6, 18, LOTRFAItemBanner.FABannerTypes.feanorian, 0);
            this.placeWallBanner(world, 2 * f, 6, 18, LOTRFAItemBanner.FABannerTypes.feanorian, 2);
            // this.setBlockAndMetadata(world, 6 * f, 3, 17, Blocks.torch, 4);
            for (int k113 : new int[] {13, 17}) {
                // this.setBlockAndMetadata(world, 4 * f, 1, k113, this.plankBlock,
                // this.plankMeta);
                // this.setBlockAndMetadata(world, 5 * f, 1, k113, this.plankSlabBlock,
                // this.plankSlabMeta | 8);
                // this.setBlockAndMetadata(world, 7 * f, 1, k113, this.plankSlabBlock,
                // this.plankSlabMeta | 8);
                this.placeChest(world, random, 6 * f, 1, k113, 0, LOTRChestContents.HIGH_ELVEN_HALL);
            }
        }
        this.setBlockAndMetadata(world, -8, 1, 13, Blocks.crafting_table, 0);
        this.setBlockAndMetadata(world, -8, 1, 17, LOTRMod.elvenForge, 2);
        this.setBlockAndMetadata(world, 8, 1, 13, LOTRFABlocks.craftingTableFeanorian, 0);
        this.setBlockAndMetadata(world, 8, 1, 17, LOTRFABlocks.craftingTableFeanorian, 0);
        for (int i1321 : new int[] {-9, 9}) {
            for (int k114 = 14; k114 <= 16; ++k114) {
                this.setBlockAndMetadata(world, i1321, 1, k114, this.doubleRockSlabBlock, this.rockSlabMeta);
                this.setAir(world, i1321, 2, k114);
            }
            this.setBlockAndMetadata(world, i1321, 3, 14, this.stairBlock, 7);
            this.setBlockAndMetadata(world, i1321, 3, 15, this.slabBlock, this.slabMeta | 8);
            this.setBlockAndMetadata(world, i1321, 3, 16, this.stairBlock, 6);
        }
        int[] j18 = new int[] {-8, 7};
        i1 = j18.length;
        for (k1 = 0; k1 < i1; ++k1) {
            for (int i25 = i132 = j18[k1]; i25 <= i132 + 1; ++i25) {
                this.setBlockAndMetadata(world, i25, 1, 18, this.doubleRockSlabBlock, this.rockSlabMeta);
                this.setAir(world, i25, 2, 18);
            }
            this.setBlockAndMetadata(world, i132, 3, 18, this.stairBlock, 4);
            this.setBlockAndMetadata(world, i132 + 1, 3, 18, this.stairBlock, 5);
        }
        for (i14 = -8; i14 <= 8; ++i14) {
            this.setBlockAndMetadata(world, i14, 1, 15, Blocks.carpet, 11);
        }
        for (i14 = -2; i14 <= 2; ++i14) {
            this.setBlockAndMetadata(world, i14, 1, 14, Blocks.carpet, 11);
            this.setBlockAndMetadata(world, i14, 1, 16, Blocks.carpet, 11);
        }
        this.generateWindow(world, -4, 3, 18);
        this.generateWindow(world, 4, 3, 18);
        for (int k1221 : new int[] {14, 16}) {
            this.setBlockAndMetadata(world, -1, 9, k1221, Blocks.stained_hardened_clay, 3);
            this.setBlockAndMetadata(world, 0, 9, k1221, Blocks.stained_hardened_clay, 11);
            this.setBlockAndMetadata(world, 1, 9, k1221, Blocks.stained_hardened_clay, 3);
        }
        this.setBlockAndMetadata(world, -2, 9, 15, Blocks.stained_hardened_clay, 3);
        this.setBlockAndMetadata(world, -1, 9, 15, Blocks.stained_hardened_clay, 11);
        this.setBlockAndMetadata(world, 0, 9, 15, Blocks.stained_hardened_clay, 11);
        this.setBlockAndMetadata(world, 1, 9, 15, Blocks.stained_hardened_clay, 11);
        this.setBlockAndMetadata(world, 2, 9, 15, Blocks.stained_hardened_clay, 3);
        // this.setBlockAndMetadata(world, 0, 8, 15, this.fenceBlock, this.fenceMeta);
        // this.setBlockAndMetadata(world, 0, 7, 15, LOTRMod.chandelier, 2);
        for (i18 = -1; i18 <= 1; ++i18) {
            this.setBlockAndMetadata(world, i18, 3, 18, this.doubleRockSlabBlock, this.rockSlabMeta);
            this.setBlockAndMetadata(world, i18, 7, 18, this.doubleRockSlabBlock, this.rockSlabMeta);
            for (j14 = 4; j14 <= 6; ++j14) {
                if (IntMath.mod(i18 + j14, 2) == 0) {
                    this.setBlockAndMetadata(world, i18, j14, 18, LOTRMod.stainedGlassPane, 0);
                    continue;
                }
                this.setBlockAndMetadata(world, i18, j14, 18, LOTRMod.stainedGlassPane, 11);
            }
        }
        this.setBlockAndMetadata(world, 0, 8, 18, this.doubleRockSlabBlock, this.rockSlabMeta);
        for (i18 = -6; i18 <= 6; ++i18) {
            i22 = Math.abs(i18);
            this.placeGrassFoundation(world, i18, 19);
            if (i22 % 4 == 2) {
                this.setBlockAndMetadata(world, i18, 1, 19, this.stairBlock, 3);
                this.setGrassToDirt(world, i18, 0, 19);
            }
            else {
                // this.setBlockAndMetadata(world, i18, 1, 19, this.leafBlock, this.leafMeta);
            }
            if (i22 >= 6) {
                this.setBlockAndMetadata(world, i18, 6, 19, this.slabBlock, this.slabMeta | 8);
                continue;
            }
            if (i22 >= 3) {
                this.setBlockAndMetadata(world, i18, 7, 19, this.slabBlock, this.slabMeta);
                continue;
            }
            if (i22 >= 2) {
                this.setBlockAndMetadata(world, i18, 7, 19, this.slabBlock, this.slabMeta | 8);
                continue;
            }
            this.setBlockAndMetadata(world, i18, 8, 19, this.slabBlock, this.slabMeta);
        }
        LOTREntityFeanorianStablemaster captain = new LOTREntityFeanorianStablemaster(world);
        captain.spawnRidingHorse = false;
        // this.spawnNPCAndSetHome(captain, world, 0, 1, 15, 8);
        return true;
    }

    private void placeWoodPillar(World world, int i, int k) {
        int j = 0;
        while ((!this.isOpaque(world, i, j, k) || this.getBlock(world, i, j, k) == this.brickBlock && this.getMeta(world, i, j, k) == this.brickMeta) && this.getY(j) >= 0) {
            this.setBlockAndMetadata(world, i, j, k, this.woodBeamBlock, this.woodBeamMeta);
            this.setGrassToDirt(world, i, j - 1, k);
            --j;
        }
        for (j = 1; j <= 4; ++j) {
            this.setBlockAndMetadata(world, i, j, k, this.woodBeamBlock, this.woodBeamMeta);
        }
    }

    private void generateWindow(World world, int i, int j, int k) {
        this.setBlockAndMetadata(world, i - 1, j, k, this.stairBlock, 0);
        this.setBlockAndMetadata(world, i, j, k, this.slabBlock, this.slabMeta);
        this.setBlockAndMetadata(world, i + 1, j, k, this.stairBlock, 1);
        for (int i1 = i - 1; i1 <= i + 1; ++i1) {
            this.setAir(world, i1, j + 1, k);
            this.setAir(world, i1, j + 2, k);
        }
        this.setBlockAndMetadata(world, i - 1, j + 3, k, this.stairBlock, 4);
        this.setBlockAndMetadata(world, i, j + 3, k, this.slabBlock, this.slabMeta | 8);
        this.setBlockAndMetadata(world, i + 1, j + 3, k, this.stairBlock, 5);
    }

    private void placeGrassFoundation(World world, int i, int k) {
        for (int j1 = 6; !(j1 < 0 && this.isOpaque(world, i, j1, k) || this.getY(j1) < 0); --j1) {
            if (j1 > 0) {
                this.setAir(world, i, j1, k);
                continue;
            }
            if (j1 == 0) {
                this.setBlockAndMetadata(world, i, j1, k, Blocks.grass, 0);
                this.setGrassToDirt(world, i, j1 - 1, k);
                continue;
            }
            this.setBlockAndMetadata(world, i, j1, k, Blocks.dirt, 0);
            this.setGrassToDirt(world, i, j1 - 1, k);
        }
    }

}
