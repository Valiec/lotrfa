package eoa.lotrfa.common.world.structure.strbuilds;

import java.util.Random;
import lotr.common.world.structure2.LOTRWorldGenStructureBase2;
import net.minecraft.world.World;

public class LOTRFAWorldGenTestHouse extends LOTRWorldGenStructureBase2 {

	public LOTRFAWorldGenTestHouse(boolean flag) {
		super(flag);
	}

	@Override
	public boolean generateWithSetRotation(World world, Random rand, int x, int y, int z, int rotation) {
		rotation +=2;
		if(rotation >= 4) rotation -=4;
		
        this.setOriginAndRotation(world, x + 1, y, z + 1, rotation, 0);
        if (this.restrictions) {
            int minHeight = 0;
            int maxHeight = 0;
            for (int loopX = -2; loopX <= 2; ++loopX) {
                for (int loopZ = -2; loopZ <= 2; ++loopZ) {
                    int loopY = this.getTopBlock(world, loopX, loopZ) - 1;
                    if (!this.isSurface(world, loopX, loopY, loopZ)) {
                        return false;
                    }
                    if (loopY < minHeight) {
                        minHeight = loopY;
                    }
                    if (loopY > maxHeight) {
                        maxHeight = loopY;
                    }
                    if (maxHeight - minHeight <= 6) continue;
                    return false;
                }
            }
        }
        for (int loopX = -3; loopX <= 1; ++loopX) {
            for (int loopZ = -3; loopZ <= 1; ++loopZ) {
                for (int loopY = 1; loopY <= 5; ++loopY) {
                    this.setAir(world, loopX, loopY, loopZ);
                }
            }
        }
        
        this.loadStrScan("test_house");
        this.generateStrScan(world, rand, 0, 0, 0);
        
        return true;
    
	}

}
