package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityTolInGaurhothWarg;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.world.structure2.LOTRWorldGenAngmarWargPit;
import net.minecraft.world.World;

public class LOTRWorldGenTolInGaurhothWargPit extends LOTRWorldGenAngmarWargPit {

    public LOTRWorldGenTolInGaurhothWargPit(boolean flag) {
        super(flag);

    }

    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        this.brickBlock = LOTRMod.brick2;
        this.brickMeta = 8;
    }

    @Override
    protected LOTREntityNPC getOrc(final World world) {
        return new LOTREntityTolInGaurhothOrc(world);
    }

    @Override
    protected LOTREntityNPC getWarg(final World world) {
        return new LOTREntityTolInGaurhothWarg(world);
    }

}
