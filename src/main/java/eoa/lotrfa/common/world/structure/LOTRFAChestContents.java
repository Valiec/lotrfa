package eoa.lotrfa.common.world.structure;

import eoa.lotrfa.common.LOTRFALore;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.LOTRFoods;
import lotr.common.LOTRLore.LoreCategory;
import lotr.common.LOTRMod;
import lotr.common.world.structure.LOTRChestContents;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandomChestContent;

public class LOTRFAChestContents {
	
	public static LOTRChestContents tol_in_gaurhoth_tent;
	public static LOTRChestContents tol_in_gaurhoth_tower;
	public static LOTRChestContents angband_tent;
	public static LOTRChestContents dor_daedeloth_tent;
	public static LOTRChestContents dor_daidelos_tent;
	public static LOTRChestContents utumno_remnant_tent;
	
	public static LOTRChestContents house_ulfang_house;
	public static LOTRChestContents house_bor_house;
	public static LOTRChestContents dor_lomin_house;
	public static LOTRChestContents brethil_house;
	
	public static LOTRChestContents petty_dwarf_drops;
	public static LOTRChestContents petty_dwarf_house;
	
	public static LOTRChestContents wraith_drops;
	public static LOTRChestContents boldog_drops;

	public static LOTRChestContents gondolin_house;
	public static LOTRChestContents nargothrond_house;
	public static LOTRChestContents doriath_house;
	public static LOTRChestContents hithlum_house;
	public static LOTRChestContents feanorian_house;
	public static LOTRChestContents falathrim_house;
	public static LOTRChestContents laegrim_house;
	
	public static LOTRChestContents feanorian_forge;
	public static LOTRChestContents falathrim_forge;
	public static LOTRChestContents nargothrond_forge;
	public static LOTRChestContents hithlum_forge;
	public static LOTRChestContents laegrim_forge;

    
    public static void init() {
    	
    	laegrim_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mirkwoodBow), 1, 1, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.swordWoodElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmWoodElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.longspearWoodElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerWoodElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.axeWoodElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 6, 100), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.sapling7, 1, 1), 1, 3, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugRedWine), 1, 1, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lembas), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletWood), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.wineGlass), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.glass_bottle), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.string), 1, 3, 100) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.laegrim_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.laegrim_house, LOTRFoods.WOOD_ELF_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.laegrim_house, 16, LOTRFALore.LoreCategories.ossiriand);

    	
    	gondolin_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.book), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.wineGlass), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletGold), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletSilver), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.glass_bottle), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugMiruvor), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 6, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.gondolinBow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.gondolinSword), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.gondolinLongspear), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.gondolinWarhammer), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.gondolinDagger), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.gondolinSpear), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lembas), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.apple), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.appleGreen), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.pear), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lettuce), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.carrot), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leek), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnip), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin), 1, 10, 10) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.gondolin_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.gondolin_house, LOTRFoods.ELF_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.gondolin_house, 16, LOTRFALore.LoreCategories.gondolin);
    	
    	falathrim_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.book), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.wineGlass), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletGold), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletSilver), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.glass_bottle), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugMiruvor), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 6, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.falathrimBow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.falathrimSword), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.falathrimLongspear), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.falathrimBattlestaff), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.falathrimDagger), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.falathrimSpear), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lembas), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.apple), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.appleGreen), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.pear), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lettuce), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.carrot), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leek), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnip), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin), 1, 10, 10) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.falathrim_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.falathrim_house, LOTRFoods.ELF_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.falathrim_house, 16, LOTRFALore.LoreCategories.falas);
    	
    	feanorian_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.book), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.wineGlass), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletGold), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletSilver), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.glass_bottle), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugMiruvor), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 6, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.feanorianBow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.feanorianSword), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.feanorianLongspear), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.feanorianBattlestaff), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.feanorianDagger), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.feanorianSpear), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lembas), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.apple), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.appleGreen), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.pear), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lettuce), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.carrot), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leek), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnip), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin), 1, 10, 10) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.feanorian_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.feanorian_house, LOTRFoods.ELF_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.feanorian_house, 16, LOTRFALore.LoreCategories.marchMaedhros);
    	
    	doriath_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.book), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.wineGlass), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletGold), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletSilver), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.glass_bottle), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugMiruvor), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 6, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.elvenBow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.swordElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.longspearElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.spearElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lembas), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.apple), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.appleGreen), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.pear), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lettuce), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.carrot), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leek), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnip), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin), 1, 10, 10) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.doriath_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.doriath_house, LOTRFoods.ELF_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.doriath_house, 16, LOTRFALore.LoreCategories.doriath);
    	
    	nargothrond_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.book), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.wineGlass), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletGold), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletSilver), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.glass_bottle), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugMiruvor), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 6, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.rivendellBow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.swordRivendell), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmRivendell), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.longspearRivendell), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerRivendell), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.spearRivendell), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lembas), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.apple), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.appleGreen), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.pear), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lettuce), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.carrot), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leek), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnip), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin), 1, 10, 10) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.nargothrond_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.nargothrond_house, LOTRFoods.ELF_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.nargothrond_house, 16, LOTRFALore.LoreCategories.nargothrond);
    	
    	hithlum_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.book), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.wineGlass), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletGold), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletSilver), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.glass_bottle), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugMiruvor), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 6, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.highElvenBow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.swordHighElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmHighElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.longspearHighElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerHighElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.spearHighElven), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lembas), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.apple), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.appleGreen), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.pear), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lettuce), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.carrot), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leek), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnip), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin), 1, 10, 10) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.hithlum_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.hithlum_house, LOTRFoods.ELF_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.hithlum_house, 16, LOTRFALore.LoreCategories.hithlum);

    	
    	petty_dwarf_drops = new LOTRChestContents(3, 4, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfHelmet), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfChestplate), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfLeggings), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfBoots), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfSword), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfSpear), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfBattleaxe), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfDagger), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfWarhammer), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfPike), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfPickaxe), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfMattock), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfAxe), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfBoarArmor), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.coal), 1, 3, 100), 
    			new WeightedRandomChestContent(new ItemStack(Blocks.torch), 2, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_ingot), 1, 1, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.pettyDwarfSteel), 1, 1, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.aleHorn), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletWood), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_porkchop), 1, 4, 75), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_chicken), 1, 4, 75), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_beef), 1, 4, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.muttonCooked), 1, 4, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gammon), 1, 4, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.deerCooked), 1, 4, 75), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.cram), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.compass), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.dwarvenRing), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mithrilBook), 1, 1, 15) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.petty_dwarf_drops);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.petty_dwarf_drops, LOTRFoods.DWARF_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.petty_dwarf_drops, 16, LOTRFALore.LoreCategories.amonRudh);

    	
    	petty_dwarf_house = new LOTRChestContents(2, 5, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_porkchop), 1, 2, 75), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_beef), 1, 2, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.muttonCooked), 1, 2, 75), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_chicken), 1, 3, 75), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_fished), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.deerCooked), 1, 2, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gammon), 1, 2, 100), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 2, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletWood), 1, 2, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletGold), 1, 2, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletSilver), 1, 2, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletCopper), 1, 2, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.aleHorn), 1, 2, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.aleHornGold), 1, 2, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.ceramicPlate), 1, 2, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.woodPlate), 1, 2, 50) 
    		});


    	
    	brethil_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.leather), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_ingot), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.bronze), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.coal), 1, 3, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.coal, 1, 1), 1, 3, 100),
    			new WeightedRandomChestContent(new ItemStack(Items.flint), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.flint_and_steel), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.fishing_rod), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.string), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.feather), 1, 4, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.stick), 1, 8, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.wheat), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.wheat_seeds), 1, 6, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.potato), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.carrot), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lettuce), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leek), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnip), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnipCooked), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.apple), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.appleGreen), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.pear), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.plum), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_porkchop), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_fished), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_beef), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_chicken), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.muttonCooked), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.rabbitCooked), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.deerCooked), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_hoe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.hoeBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.stone_hoe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.wooden_hoe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_axe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.axeBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.stone_axe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.wooden_axe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_shovel), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.shovelBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.stone_shovel), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.wooden_shovel), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerIron), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.brethilHelmet), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.brethilChestplate), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.brethilLeggings), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.brethilBoots), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leatherHat), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.ceramicMug), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletWood), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.waterskin), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugAle), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugCider), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugPerry), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugMead), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.bow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.brethilBow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 100) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.brethil_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.brethil_house, LOTRFoods.RANGER_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.brethil_house, 16, LOTRFALore.LoreCategories.brethil);

    	
    	dor_lomin_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.leather), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_ingot), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.bronze), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.silver), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.silverNugget), 1, 6, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.gold_ingot), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.gold_nugget), 1, 6, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.coal), 1, 3, 75), 
    			new WeightedRandomChestContent(new ItemStack(Items.string), 1, 3, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.feather), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.wheat), 1, 3, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.potato), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.carrot), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leek), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnip), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnipCooked), 1, 3, 25),
    			new WeightedRandomChestContent(new ItemStack(Items.apple), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.appleGreen), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_porkchop), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_fished), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_beef), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_chicken), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.muttonCooked), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.rabbitCooked), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.deerCooked), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_hoe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.hoeBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.stone_hoe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.wooden_hoe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_axe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.axeBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.stone_axe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.wooden_axe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_shovel), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.shovelBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.stone_shovel), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.wooden_shovel), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerIron), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominDagger), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leatherHat), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.ceramicMug), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletGold), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletSilver), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletCopper), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletWood), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.wineGlass), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.aleHorn), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.aleHornGold), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugAle), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugCider), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugPerry), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugMead), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugRedWine), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugWhiteWine), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.bow), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominBow), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 100) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.dor_lomin_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.dor_lomin_house, LOTRFoods.DALE_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.dor_lomin_house, 16, LOTRFALore.LoreCategories.dorLomin);

    	
    	house_bor_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.leather), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_ingot), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.bronze), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.coal), 1, 3, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.coal, 1, 1), 1, 3, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.flint), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.flint_and_steel), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.fishing_rod), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.string), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.feather), 1, 4, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.horn), 1, 2, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.stick), 1, 8, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.wheat), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.wheat_seeds), 1, 6, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.potato), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.carrot), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.lettuce), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leek), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnip), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.turnipCooked), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.apple), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.appleGreen), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.pear), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.plum), 1, 3, 50), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_porkchop), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_fished), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_beef), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.cooked_chicken), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.muttonCooked), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.rabbitCooked), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.deerCooked), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_hoe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.hoeBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.stone_hoe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.wooden_hoe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_axe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.axeBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.stone_axe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.wooden_axe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_shovel), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.shovelBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.stone_shovel), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.wooden_shovel), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerIron), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerBronze), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.borDagger), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.borGreatsword), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.borSpear), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.borBattleaxe), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.borHelmet), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.borChestplate), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.borLeggings), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.borBoots), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.leatherHat), 1, 1, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.ceramicMug), 1, 3, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletWood), 1, 3, 25),
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletCopper), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.aleHorn), 1, 3, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugAle), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugCider), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugPerry), 1, 1, 10), 
    			new WeightedRandomChestContent(new ItemStack(Items.bow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(LOTRFAItems.borBow), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 100) 
    		});
    	LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.house_bor_house);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.house_bor_house, LOTRFoods.ROHAN_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.house_bor_house, 16, LOTRFALore.LoreCategories.gardhBoren);

    	
    	house_ulfang_house = new LOTRChestContents(4, 6, new WeightedRandomChestContent[] { 
    			 new WeightedRandomChestContent(new ItemStack(Items.paper), 2, 8, 50), 
    			 new WeightedRandomChestContent(new ItemStack(Items.book), 1, 3, 100), 
    			 new WeightedRandomChestContent(new ItemStack(Items.bucket), 1, 3, 25), 
    			 new WeightedRandomChestContent(new ItemStack(Items.feather), 1, 2, 50), 
    			 new WeightedRandomChestContent(new ItemStack(Items.leather), 1, 4, 100), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.bronze), 1, 3, 50), 
    			 new WeightedRandomChestContent(new ItemStack(Items.iron_ingot), 1, 3, 50), 
    			 new WeightedRandomChestContent(new ItemStack(Items.coal), 1, 3, 75), 
    			 new WeightedRandomChestContent(new ItemStack(Items.string), 1, 3, 100), 
    			 new WeightedRandomChestContent(new ItemStack(Items.wheat), 1, 3, 100), 
    			 new WeightedRandomChestContent(new ItemStack(Items.bread), 1, 3, 100), 
    			 new WeightedRandomChestContent(new ItemStack(Items.cooked_fished), 1, 3, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.rabbitCooked), 1, 3, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.deerCooked), 1, 3, 10), 
    			 new WeightedRandomChestContent(new ItemStack(Items.iron_hoe), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.hoeBronze), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(Items.stone_hoe), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(Items.wooden_hoe), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(Items.iron_axe), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.axeBronze), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(Items.stone_axe), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(Items.wooden_axe), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(Items.iron_shovel), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.shovelBronze), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(Items.stone_shovel), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(Items.wooden_shovel), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerIron), 1, 1, 25), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerBronze), 1, 1, 25), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRFAItems.ulfangDagger), 1, 1, 25),
    			 new WeightedRandomChestContent(new ItemStack(LOTRFAItems.ulfangDaggerPoisoned), 1, 1, 25), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRFAItems.ulfangSword), 1, 1, 25), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRFAItems.ulfangBattleaxe), 1, 1, 25), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRFAItems.ulfangSpear), 1, 1, 25), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetDunlending), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyDunlending), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.legsDunlending), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsDunlending), 1, 1, 10), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.leatherHat), 1, 1, 50), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.mug), 1, 3, 25), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.ceramicMug), 1, 3, 25), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.waterskin), 1, 3, 25), 
    			 new WeightedRandomChestContent(new ItemStack(Items.glass_bottle), 1, 2, 50), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.mugSourMilk), 1, 1, 50), 
    			 new WeightedRandomChestContent(new ItemStack(Items.milk_bucket), 1, 1, 50), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRMod.mugAle), 1, 1, 25), 
    			 new WeightedRandomChestContent(new ItemStack(Items.bow), 1, 1, 25), 
    			 new WeightedRandomChestContent(new ItemStack(LOTRFAItems.ulfangBow), 1, 1, 25), 
    			 new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 50) });
         LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.house_ulfang_house);
     	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.house_ulfang_house, LOTRFoods.DUNLENDING_DRINK);
         LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.house_ulfang_house, 16, LOTRFALore.LoreCategories.taurdru);

    	
    	LOTRChestContents.ORC_DUNGEON = new LOTRChestContents(6, 8, new WeightedRandomChestContent[] { 
    			new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 1, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.skull), 1, 1, 25), 
    			new WeightedRandomChestContent(new ItemStack(Items.iron_ingot), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.orcSteel), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.urukSteel), 1, 4, 100), 
    			new WeightedRandomChestContent(new ItemStack(Items.gold_ingot), 1, 2, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.silver), 1, 2, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mithrilNugget), 1, 4, 10), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.guldurilCrystal), 1, 4, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.orcBomb), 1, 2, 50), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.orcTorchItem), 1, 4, 75), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetOrc), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyOrc), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.legsOrc), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsOrc), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.scimitarOrc), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmOrc), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerOrc), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerOrcPoisoned), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.battleaxeOrc), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.hammerOrc), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetDolGuldur), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyDolGuldur), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.legsDolGuldur), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsDolGuldur), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.swordDolGuldur), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerDolGuldur), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerDolGuldurPoisoned), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.battleaxeDolGuldur), 1, 1, 15), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.hammerDolGuldur), 1, 1, 15),  
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.orcBow), 1, 1, 75), 
    			new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 7, 100), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.maggotyBread), 1, 3, 100), 
    			new WeightedRandomChestContent(new ItemStack(LOTRMod.mugOrcDraught), 1, 1, 200) 
    		});
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRChestContents.ORC_DUNGEON);

    	
    	tol_in_gaurhoth_tent = new LOTRChestContents(1, 2, new WeightedRandomChestContent[] { 
        		new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.rotten_flesh), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.stick), 8, 16, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.guldurilCrystal), 1, 2, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.orcSteel), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.skullCup), 1, 3, 50),
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.waterskin), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.mugOrcDraught), 1, 1, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerDolGuldurPoisoned), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.swordDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.pickaxeDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.axeDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.maggotyBread), 1, 3, 200) 
        });
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.tol_in_gaurhoth_tent);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.tol_in_gaurhoth_tent, LOTRFoods.ORC_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.tol_in_gaurhoth_tent, 16, LOTRFALore.LoreCategories.tolInGaurhoth);
        
        tol_in_gaurhoth_tower = new LOTRChestContents(6, 8, new WeightedRandomChestContent[] { 
        		new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(Items.rotten_flesh), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.orcSteel), 1, 4, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.guldurilCrystal), 1, 3, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.skullCup), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.waterskin), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.mugOrcDraught), 1, 1, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.orcBow), 1, 1, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerDolGuldurPoisoned), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.swordDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.spearDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.pickaxeDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.axeDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.battleaxeDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.hammerDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.pikeDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.legsDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsDolGuldur), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.maggotyBread), 1, 4, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.bottlePoison), 1, 1, 25) 
        	});
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.tol_in_gaurhoth_tower);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.tol_in_gaurhoth_tower, LOTRFoods.ORC_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.tol_in_gaurhoth_tower, 16, LOTRFALore.LoreCategories.tolInGaurhoth);
    	
        dor_daedeloth_tent = new LOTRChestContents(1, 2, new WeightedRandomChestContent[] { 
        		new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.rotten_flesh), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.stick), 8, 16, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.nauriteGem), 1, 4, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.orcSteel), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.skullCup), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.waterskin), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.mugOrcDraught), 1, 1, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerOrc), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerOrcPoisoned), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.scimitarOrc), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmOrc), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.pickaxeOrc), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.axeOrc), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(Items.saddle), 1, 1, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.maggotyBread), 1, 3, 200) 
        	});
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.dor_daedeloth_tent);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.dor_daedeloth_tent, LOTRFoods.ORC_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.dor_daedeloth_tent, 16, LOTRFALore.LoreCategories.angband);
    	
        angband_tent = new LOTRChestContents(1, 2, new WeightedRandomChestContent[] { 
        		new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.rotten_flesh), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.stick), 8, 16, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.nauriteGem), 1, 4, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.blackUrukSteel), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.skullCup), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.waterskin), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.mugOrcDraught), 1, 1, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerBlackUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerBlackUrukPoisoned), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.scimitarBlackUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.spearBlackUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.battleaxeBlackUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(Items.saddle), 1, 1, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.maggotyBread), 1, 3, 200) 
        	});
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.angband_tent);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.angband_tent, LOTRFoods.ORC_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.angband_tent, 16, LOTRFALore.LoreCategories.angband);
        
        boldog_drops = new LOTRChestContents(1, 2, new WeightedRandomChestContent[] { 
        		new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.rotten_flesh), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.stick), 8, 16, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.nauriteGem), 1, 4, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.blackUrukSteel), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.skullCup), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.waterskin), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.mugOrcDraught), 1, 1, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.scimitarBlackUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.spearBlackUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.battleaxeBlackUruk), 1, 1, 25),
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetHalfTroll), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyHalfTroll), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.legsHalfTroll), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsHalfTroll), 1, 1, 25),
        		new WeightedRandomChestContent(new ItemStack(Items.saddle), 1, 1, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.maggotyBread), 1, 3, 200) 
        	});
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.boldog_drops);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.boldog_drops, 16, LOTRFALore.LoreCategories.angband);
        
        dor_daidelos_tent = new LOTRChestContents(1, 2, new WeightedRandomChestContent[] { 
        		new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.rotten_flesh), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.stick), 8, 16, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.coal), 1, 4, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.orcSteel), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.urukSteel), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.skullCup), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.waterskin), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.mugOrcDraught), 1, 1, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.arrow), 2, 8, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerOrc), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerOrcPoisoned), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.scimitarOrc), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmOrc), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.pickaxeOrc), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.axeOrc), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerUruk), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerUrukPoisoned), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.scimitarUruk), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.pickaxeUruk), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.axeUruk), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerIron), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerIronPoisoned), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(Items.iron_sword), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(Items.iron_pickaxe), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(Items.iron_axe), 1, 1, 10), 
        		new WeightedRandomChestContent(new ItemStack(Items.saddle), 1, 1, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.maggotyBread), 1, 3, 200), 
        	});
        
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.dor_daidelos_tent);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.dor_daidelos_tent, LOTRFoods.ORC_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.dor_daidelos_tent, 16, LOTRFALore.LoreCategories.nanGuruthos);
        
        utumno_remnant_tent = new LOTRChestContents(1, 2, new WeightedRandomChestContent[] { 
        		new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.rotten_flesh), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.stick), 8, 16, 100), 
        		new WeightedRandomChestContent(new ItemStack(Items.coal), 1, 4, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.urukSteel), 1, 3, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.skullCup), 1, 3, 50),
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.waterskin), 1, 3, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.mugOrcDraught), 1, 1, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.crossbowBolt), 2, 8, 100), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerUrukPoisoned), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.scimitarUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.pikeUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.hammerUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.battleaxeUruk), 1, 1, 25), 
        		new WeightedRandomChestContent(new ItemStack(Items.saddle), 1, 1, 50), 
        		new WeightedRandomChestContent(new ItemStack(LOTRMod.maggotyBread), 1, 3, 200) 
        	});
        
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.utumno_remnant_tent);
    	LOTRFAReflectionHelper.setChestContentVessels(LOTRFAChestContents.utumno_remnant_tent, LOTRFoods.ORC_DRINK);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.utumno_remnant_tent, 16, LOTRFALore.LoreCategories.nanGuruthos);
        
        LOTRChestContents.DUNEDAIN_TOWER = new LOTRChestContents(5, 7, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 3, 100), 
                new WeightedRandomChestContent(new ItemStack(Items.skull), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_ingot), 2, 5, 100), 
                new WeightedRandomChestContent(new ItemStack(Items.gold_ingot), 1, 3, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silver), 2, 3, 100), 
                new WeightedRandomChestContent(new ItemStack(Items.gold_nugget), 2, 9, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverNugget), 2, 9, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin), 2, 20, 100), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_sword), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerIron), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetGondor), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyGondor), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsGondor), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsGondor), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordGondor), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerGondor), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.spearGondor), 1, 1, 25)
        });
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRChestContents.DUNEDAIN_TOWER);
        LOTRFAReflectionHelper.setChestContentLore(LOTRChestContents.DUNEDAIN_TOWER, 16, LoreCategory.ERIADOR);
        
        
        wraith_drops = new LOTRChestContents(4, 8, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin, 1, 0), 1, 20, 200), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin, 1, 1), 1, 8, 20), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin, 1, 2), 1, 2, 5), 
                new WeightedRandomChestContent(new ItemStack(Items.gold_nugget), 1, 5, 100), 
                new WeightedRandomChestContent(new ItemStack(Items.gold_ingot), 1, 4, 75), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverNugget), 1, 5, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silver), 1, 4, 75), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerBarrow), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerBronze), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordBronze), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetBronze), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyBronze), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsBronze), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsBronze), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerIron), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_sword), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_helmet), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_chestplate), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_leggings), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_boots), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordHighElven), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmHighElven), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.longspearHighElven), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerHighElven), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetHighElven), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyHighElven), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsHighElven), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsHighElven), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominHelmet), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominChestplate), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominLeggings), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominBoots), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominSword), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominDagger), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominSpear), 1, 1, 10),
                new WeightedRandomChestContent(new ItemStack(Items.arrow), 1, 8, 100), 
                new WeightedRandomChestContent(new ItemStack(Items.skull), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 4, 100), 
                new WeightedRandomChestContent(new ItemStack(Items.glass_bottle), 1, 3, 20), 
                new WeightedRandomChestContent(new ItemStack(Items.book), 1, 2, 20), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.goldRing), 1, 1, 20), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverRing), 1, 1, 20), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletGold), 1, 3, 20), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletSilver), 1, 3, 20), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.gobletCopper), 1, 3, 20)
        });
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRFAChestContents.wraith_drops);
        LOTRFAReflectionHelper.setChestContentLore(LOTRFAChestContents.wraith_drops, 16, LOTRFALore.LoreCategories.tolInGaurhoth);
                
        LOTRChestContents.ANCIENT_SWORD = new LOTRChestContents(1, 1, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(LOTRMod.scimitarOrc), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordHighElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordRivendell), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordWoodElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordDwarven), 1, 1, 50)
        });
        
        LOTRChestContents.ANCIENT_DAGGER = new LOTRChestContents(1, 1, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerOrc), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerHighElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerRivendell), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerWoodElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerDwarven), 1, 1, 50)
        });
        
        LOTRChestContents.ANCIENT_HELMET = new LOTRChestContents(1, 1, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetOrc), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetHighElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetRivendell), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetWoodElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetDwarven), 1, 1, 50)
        });
        
        LOTRChestContents.ANCIENT_BODY = new LOTRChestContents(1, 1, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyOrc), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyHighElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyRivendell), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyWoodElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyDwarven), 1, 1, 50)
        });
        
        LOTRChestContents.ANCIENT_LEGS = new LOTRChestContents(1, 1, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsOrc), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsHighElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsRivendell), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsWoodElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsDwarven), 1, 1, 50)
        });
        
        LOTRChestContents.ANCIENT_BOOTS = new LOTRChestContents(1, 1, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsOrc), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsHighElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsRivendell), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsWoodElven), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsGondor), 1, 1, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsDwarven), 1, 1, 50)
        });

        LOTRChestContents.TROLL_HOARD = new LOTRChestContents(1, 4, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(Items.bone), 1, 1, 75), 
                new WeightedRandomChestContent(new ItemStack(Items.gold_ingot), 1, 2, 50), 
                new WeightedRandomChestContent(new ItemStack(Items.gold_nugget), 1, 4, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silver), 1, 2, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverNugget), 1, 4, 50), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverCoin), 1, 30, 75), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.goldRing), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.silverRing), 1, 1, 25),  
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordHighElven), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmHighElven), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.longspearHighElven), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerHighElven), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetHighElven), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyHighElven), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsHighElven), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsHighElven), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.swordRivendell), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmRivendell), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.longspearRivendell), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerRivendell), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetRivendell), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyRivendell), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsRivendell), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsRivendell), 1, 1, 5), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.scimitarOrc), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.polearmOrc), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerOrc), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.helmetOrc), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bodyOrc), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.legsOrc), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.bootsOrc), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_sword), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.daggerIron), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_helmet), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_chestplate), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_leggings), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(Items.iron_boots), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominHelmet), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominChestplate), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominLeggings), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominBoots), 1, 1, 25), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominSword), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominDagger), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(LOTRFAItems.dorLominSpear), 1, 1, 10), 
                new WeightedRandomChestContent(new ItemStack(Items.compass), 1, 1, 25)
        });
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRChestContents.TROLL_HOARD);
        LOTRFAReflectionHelper.setChestContentLore(LOTRChestContents.TROLL_HOARD, 12, LoreCategory.RUINS, LOTRFALore.LoreCategories.hithlum);
                
        LOTRChestContents.TROLL_HOARD_ETTENMOORS = new LOTRChestContents(1, 1, new WeightedRandomChestContent[] {
                new WeightedRandomChestContent(new ItemStack(LOTRMod.trollTotem, 1, 0), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.trollTotem, 1, 1), 1, 1, 100), 
                new WeightedRandomChestContent(new ItemStack(LOTRMod.trollTotem, 1, 2), 1, 1, 100)
        });
        LOTRFAReflectionHelper.enableChestContentPouches(LOTRChestContents.TROLL_HOARD_ETTENMOORS);
        LOTRFAReflectionHelper.setChestContentLore(LOTRChestContents.TROLL_HOARD_ETTENMOORS, 12, LoreCategory.RUINS, LOTRFALore.LoreCategories.hithlum);
    }

}
