package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import com.google.common.math.IntMath;
import eoa.lotrfa.common.entity.npc.LOTREntityHithlumVintnerElf;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import lotr.common.LOTRFoods;
import lotr.common.LOTRMod;
import lotr.common.item.LOTRItemMug;
import lotr.common.world.structure2.LOTRWorldGenDorwinionHouse;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class LOTRWorldGenHithlumBrewery extends LOTRWorldGenDorwinionHouse {
	

    public LOTRWorldGenHithlumBrewery(boolean flag) {
        super(flag);
    }

    @Override
    protected void setupRandomBlocks(Random random) {
        int randomWood = random.nextInt(3);
        if (randomWood == 0) {
            this.woodBeamBlock = LOTRMod.woodBeamV1;
            this.woodBeamMeta = 0;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 0;
            this.plankSlabBlock = Blocks.wooden_slab;
            this.plankSlabMeta = 0;
            this.plankStairBlock = Blocks.oak_stairs;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 0;
        }
        else if (randomWood == 1) {
            this.woodBeamBlock = LOTRMod.woodBeam6;
            this.woodBeamMeta = 2;
            this.plankBlock = LOTRMod.planks2;
            this.plankMeta = 10;
            this.plankSlabBlock = LOTRMod.woodSlabSingle4;
            this.plankSlabMeta = 2;
            this.plankStairBlock = LOTRMod.stairsCypress;
            this.fenceBlock = LOTRMod.fence2;
            this.fenceMeta = 10;
        }
        else if (randomWood == 2) {
            this.woodBeamBlock = LOTRMod.woodBeam6;
            this.woodBeamMeta = 3;
            this.plankBlock = LOTRMod.planks2;
            this.plankMeta = 11;
            this.plankSlabBlock = LOTRMod.woodSlabSingle4;
            this.plankSlabMeta = 3;
            this.plankStairBlock = LOTRMod.stairsOlive;
            this.fenceBlock = LOTRMod.fence2;
            this.fenceMeta = 11;
        }
        int randomFloor = random.nextInt(2);
        if (randomFloor == 0) {
            this.floorBlock = Blocks.double_stone_slab;
            this.floorMeta = 0;
        }
        else if (randomFloor == 1) {
            this.floorBlock = LOTRMod.brick3;
            this.floorMeta = 2;
        }

        int randomWall = random.nextInt(3);
        if (randomWall == 0) {
            this.wallBlock = Blocks.stonebrick;
            this.wallMeta = 0;
        }
        else if (randomWall == 1) {
            this.wallBlock = Blocks.stained_hardened_clay;
            this.wallMeta = 0;
        }
        else if (randomWall == 2) {
            this.wallBlock = Blocks.stained_hardened_clay;
            this.wallMeta = 4;
        }
        this.brickBlock = LOTRMod.brick3;
        this.brickMeta = 2;
        this.brickSlabBlock = LOTRMod.slabSingle5;
        this.brickSlabMeta = 5;
        this.brickStairBlock = LOTRMod.stairsHighElvenBrick;
        this.brickWallBlock = LOTRMod.wall2;
        this.brickWallMeta = 11;
        this.pillarBlock = LOTRMod.pillar;
        this.pillarMeta = 10;
        int randomClay = random.nextInt(5);
        if (randomClay == 0) {
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 11;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle2;
            this.claySlabMeta = 3;
            this.clayStairBlock = LOTRMod.stairsClayTileDyedBlue;
        }
        else if (randomClay == 1) {
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 3;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle;
            this.claySlabMeta = 3;
            this.clayStairBlock = LOTRMod.stairsClayTileDyedLightBlue;
        }
        else if (randomClay == 2) {
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 9;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle2;
            this.claySlabMeta = 1;
            this.clayStairBlock = LOTRMod.stairsClayTileDyedCyan;
        }
        else if (randomClay == 3) {
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 8;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle2;
            this.claySlabMeta = 0;
            this.clayStairBlock = LOTRMod.stairsClayTileDyedLightGray;
        }
        else if (randomClay == 4) {
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 7;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle;
            this.claySlabMeta = 7;
            this.clayStairBlock = LOTRMod.stairsClayTileDyedGray;
        }
        this.leafBlock = LOTRMod.leaves6;
        this.leafMeta = 6;
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        int i1;
        int i12;
        int j1;
        int k1;
        int k12;
        int k13;
        int i13;
        this.setOriginAndRotation(world, i, j, k, rotation, 1);
        this.setupRandomBlocks(random);
        if (this.restrictions) {
            for (i12 = -6; i12 <= 6; ++i12) {
                for (k12 = 0; k12 <= 19; ++k12) {
                    j1 = this.getTopBlock(world, i12, k12) - 1;
                    Block block = this.getBlock(world, i12, j1, k12);
                    if (block == Blocks.grass) continue;
                    return false;
                }
            }
        }
        for (i12 = -6; i12 <= 6; ++i12) {
            for (k12 = 0; k12 <= 19; ++k12) {
                this.setBlockAndMetadata(world, i12, 0, k12, Blocks.grass, 0);
                j1 = -1;
                while (!this.isOpaque(world, i12, j1, k12) && this.getY(j1) >= 0) {
                    this.setBlockAndMetadata(world, i12, j1, k12, Blocks.dirt, 0);
                    this.setGrassToDirt(world, i12, j1 - 1, k12);
                    --j1;
                }
                for (j1 = 1; j1 <= 10; ++j1) {
                    this.setAir(world, i12, j1, k12);
                }
                if (i12 < -5 || i12 > 5 || k12 < 1 || k12 > 18) continue;
                if (!(i12 != -5 && i12 != 5 || k12 != 1 && k12 != 18)) {
                    for (j1 = 0; j1 <= 5; ++j1) {
                        this.setBlockAndMetadata(world, i12, j1, k12, this.woodBeamBlock, this.woodBeamMeta);
                    }
                    continue;
                }
                if (i12 == -5 || i12 == 5 || k12 == 1 || k12 == 18) {
                    for (j1 = 0; j1 <= 5; ++j1) {
                        this.setBlockAndMetadata(world, i12, j1, k12, this.brickBlock, this.brickMeta);
                    }
                    continue;
                }
                if (i12 >= -2 && i12 <= 2) {
                    this.setBlockAndMetadata(world, i12, 0, k12, this.floorBlock, this.floorMeta);
                    continue;
                }
                this.setBlockAndMetadata(world, i12, 0, k12, this.plankBlock, this.plankMeta);
            }
        }
        for (i12 = -1; i12 <= 1; ++i12) {
            this.setBlockAndMetadata(world, i12, 0, 1, this.floorBlock, this.floorMeta);
            for (int j12 = 1; j12 <= 3; ++j12) {
                this.setBlockAndMetadata(world, i12, j12, 1, LOTRMod.gateWooden, 2);
            }
        }
        for (k1 = 2; k1 <= 17; ++k1) {
            if (k1 % 3 == 2) {
                this.setBlockAndMetadata(world, -6, 1, k1, this.brickStairBlock, 1);
                this.setGrassToDirt(world, -6, 0, k1);
                this.setBlockAndMetadata(world, 6, 1, k1, this.brickStairBlock, 0);
                this.setGrassToDirt(world, 6, 0, k1);
                continue;
            }
            this.setBlockAndMetadata(world, -6, 1, k1, this.leafBlock, this.leafMeta);
            this.setBlockAndMetadata(world, 6, 1, k1, this.leafBlock, this.leafMeta);
        }
        for (i12 = -4; i12 <= 4; ++i12) {
            if (Math.abs(i12) == 4) {
                this.setBlockAndMetadata(world, i12, 1, 19, this.brickStairBlock, 3);
                this.setGrassToDirt(world, i12, 0, 19);
                continue;
            }
            this.setBlockAndMetadata(world, i12, 1, 19, this.leafBlock, this.leafMeta);
        }
        for (i12 = -4; i12 <= 4; ++i12) {
            if (Math.abs(i12) == 4 || Math.abs(i12) == 2) {
                this.setBlockAndMetadata(world, i12, 1, 0, this.brickStairBlock, 2);
                this.setGrassToDirt(world, i12, 0, 0);
                continue;
            }
            if (Math.abs(i12) != 3) continue;
            this.setBlockAndMetadata(world, i12, 1, 0, this.leafBlock, this.leafMeta);
        }
        for (i12 = -5; i12 <= 5; ++i12) {
            this.setBlockAndMetadata(world, i12, 5, 0, this.brickStairBlock, 6);
            this.setBlockAndMetadata(world, i12, 5, 19, this.brickStairBlock, 7);
        }
        for (k1 = 0; k1 <= 19; ++k1) {
            if (k1 >= 3 && k1 <= 16) {
                if (k1 % 3 == 0) {
                    this.setAir(world, -5, 3, k1);
                    this.setBlockAndMetadata(world, -5, 4, k1, this.brickStairBlock, 7);
                    this.setAir(world, 5, 3, k1);
                    this.setBlockAndMetadata(world, 5, 4, k1, this.brickStairBlock, 7);
                }
                else if (k1 % 3 == 1) {
                    this.setAir(world, -5, 3, k1);
                    this.setBlockAndMetadata(world, -5, 4, k1, this.brickStairBlock, 6);
                    this.setAir(world, 5, 3, k1);
                    this.setBlockAndMetadata(world, 5, 4, k1, this.brickStairBlock, 6);
                }
            }
            this.setBlockAndMetadata(world, -6, 5, k1, this.brickStairBlock, 5);
            this.setBlockAndMetadata(world, 6, 5, k1, this.brickStairBlock, 4);
            if (k1 <= 7 && k1 % 2 == 0 || k1 >= 12 && k1 % 2 == 1) {
                this.setBlockAndMetadata(world, -6, 6, k1, this.brickSlabBlock, this.brickSlabMeta);
                this.setBlockAndMetadata(world, 6, 6, k1, this.brickSlabBlock, this.brickSlabMeta);
            }
            if (k1 == 8 || k1 == 11) {
                this.setBlockAndMetadata(world, -6, 4, k1, this.brickSlabBlock, this.brickSlabMeta | 8);
                this.setBlockAndMetadata(world, -6, 5, k1, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, -6, 6, k1, this.brickSlabBlock, this.brickSlabMeta);
                this.setBlockAndMetadata(world, 6, 4, k1, this.brickSlabBlock, this.brickSlabMeta | 8);
                this.setBlockAndMetadata(world, 6, 5, k1, this.brickBlock, this.brickMeta);
                this.setBlockAndMetadata(world, 6, 6, k1, this.brickSlabBlock, this.brickSlabMeta);
                this.placeWallBanner(world, -5, 3, k1, LOTRFAItemBanner.FABannerTypes.hithlum, 3);
                this.placeWallBanner(world, 5, 3, k1, LOTRFAItemBanner.FABannerTypes.hithlum, 1);
            }
            if (k1 != 9 && k1 != 10) continue;
            this.setBlockAndMetadata(world, -6, 6, k1, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, 6, 6, k1, this.brickBlock, this.brickMeta);
        }
        for (i12 = -3; i12 <= 3; ++i12) {
            if (Math.abs(i12) == 3) {
                this.setBlockAndMetadata(world, i12, 2, 1, this.brickSlabBlock, this.brickSlabMeta);
                this.setAir(world, i12, 3, 1);
            }
            if (Math.abs(i12) == 2) {
                this.placeWallBanner(world, i12, 4, 1, LOTRFAItemBanner.FABannerTypes.hithlum, 2);
            }
            if (IntMath.mod(i12, 2) != 1) continue;
            this.setBlockAndMetadata(world, i12, 2, 18, this.brickSlabBlock, this.brickSlabMeta);
            this.setAir(world, i12, 3, 18);
        }
        for (int k14 : new int[] {1, 18}) {
            int i14;
            this.setBlockAndMetadata(world, -4, 6, k14, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, -3, 6, k14, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, -2, 6, k14, this.brickSlabBlock, this.brickSlabMeta);
            this.setBlockAndMetadata(world, -1, 6, k14, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, 0, 6, k14, this.brickSlabBlock, this.brickSlabMeta);
            this.setBlockAndMetadata(world, 1, 6, k14, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, 2, 6, k14, this.brickSlabBlock, this.brickSlabMeta);
            this.setBlockAndMetadata(world, 3, 6, k14, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, 4, 6, k14, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, -3, 7, k14, this.brickBlock, this.brickMeta);
            this.setAir(world, -2, 7, k14);
            this.setBlockAndMetadata(world, -1, 7, k14, this.brickBlock, this.brickMeta);
            this.setAir(world, 0, 7, k14);
            this.setBlockAndMetadata(world, 1, 7, k14, this.brickBlock, this.brickMeta);
            this.setAir(world, 2, 7, k14);
            this.setBlockAndMetadata(world, 3, 7, k14, this.brickBlock, this.brickMeta);
            for (i14 = -2; i14 <= 2; ++i14) {
                this.setBlockAndMetadata(world, i14, 8, k14, this.brickBlock, this.brickMeta);
            }
            for (i14 = -1; i14 <= 1; ++i14) {
                this.setBlockAndMetadata(world, i14, 9, k14, this.brickBlock, this.brickMeta);
            }
            this.setBlockAndMetadata(world, 0, 10, k14, this.brickBlock, this.brickMeta);
        }
        for (k13 = 2; k13 <= 17; ++k13) {
            this.setBlockAndMetadata(world, -4, 6, k13, this.plankStairBlock, 4);
            this.setBlockAndMetadata(world, -3, 7, k13, this.plankStairBlock, 4);
            this.setBlockAndMetadata(world, -2, 8, k13, this.plankStairBlock, 4);
            this.setBlockAndMetadata(world, -1, 9, k13, this.plankStairBlock, 4);
            this.setBlockAndMetadata(world, 0, 10, k13, this.plankBlock, this.plankMeta);
            this.setBlockAndMetadata(world, 1, 9, k13, this.plankStairBlock, 5);
            this.setBlockAndMetadata(world, 2, 8, k13, this.plankStairBlock, 5);
            this.setBlockAndMetadata(world, 3, 7, k13, this.plankStairBlock, 5);
            this.setBlockAndMetadata(world, 4, 6, k13, this.plankStairBlock, 5);
        }
        for (k13 = 0; k13 <= 19; ++k13) {
            this.setBlockAndMetadata(world, -5, 6, k13, this.clayStairBlock, 1);
            this.setBlockAndMetadata(world, -4, 7, k13, this.clayStairBlock, 1);
            this.setBlockAndMetadata(world, -3, 8, k13, this.clayStairBlock, 1);
            this.setBlockAndMetadata(world, -2, 9, k13, this.clayStairBlock, 1);
            this.setBlockAndMetadata(world, -1, 10, k13, this.clayStairBlock, 1);
            this.setBlockAndMetadata(world, 0, 11, k13, this.claySlabBlock, this.claySlabMeta);
            this.setBlockAndMetadata(world, 1, 10, k13, this.clayStairBlock, 0);
            this.setBlockAndMetadata(world, 2, 9, k13, this.clayStairBlock, 0);
            this.setBlockAndMetadata(world, 3, 8, k13, this.clayStairBlock, 0);
            this.setBlockAndMetadata(world, 4, 7, k13, this.clayStairBlock, 0);
            this.setBlockAndMetadata(world, 5, 6, k13, this.clayStairBlock, 0);
        }
        for (int k14 : new int[] {0, 19}) {
            this.setBlockAndMetadata(world, -4, 6, k14, this.clayStairBlock, 4);
            this.setBlockAndMetadata(world, -3, 7, k14, this.clayStairBlock, 4);
            this.setBlockAndMetadata(world, -2, 8, k14, this.clayStairBlock, 4);
            this.setBlockAndMetadata(world, -1, 9, k14, this.clayStairBlock, 4);
            this.setBlockAndMetadata(world, 0, 10, k14, this.clayBlock, this.clayMeta);
            this.setBlockAndMetadata(world, 1, 9, k14, this.clayStairBlock, 5);
            this.setBlockAndMetadata(world, 2, 8, k14, this.clayStairBlock, 5);
            this.setBlockAndMetadata(world, 3, 7, k14, this.clayStairBlock, 5);
            this.setBlockAndMetadata(world, 4, 6, k14, this.clayStairBlock, 5);
        }
        for (int k15 = 2; k15 <= 17; ++k15) {
            if (k15 % 3 != 2) continue;
            for (i13 = -4; i13 <= 4; ++i13) {
                this.setBlockAndMetadata(world, i13, 6, k15, this.woodBeamBlock, this.woodBeamMeta | 4);
            }
            this.setBlockAndMetadata(world, -4, 5, k15, Blocks.torch, 2);
            this.setBlockAndMetadata(world, 4, 5, k15, Blocks.torch, 1);
        }
        this.setBlockAndMetadata(world, -2, 5, 2, Blocks.torch, 3);
        this.setBlockAndMetadata(world, 2, 5, 2, Blocks.torch, 3);
        this.setBlockAndMetadata(world, -2, 5, 17, Blocks.torch, 4);
        this.setBlockAndMetadata(world, 2, 5, 17, Blocks.torch, 4);
        this.placeWallBanner(world, 0, 5, 1, LOTRFAItemBanner.FABannerTypes.hithlum, 0);
        this.placeWallBanner(world, 0, 5, 18, LOTRFAItemBanner.FABannerTypes.hithlum, 2);
        ItemStack drink = LOTRFoods.DORWINION_DRINK.getRandomBrewableDrink(random);
        for (k12 = 2; k12 <= 17; ++k12) {
            for (i1 = -4; i1 <= 4; ++i1) {
                if (Math.abs(i1) < 3) continue;
                if (k12 == 2 || k12 == 17) {
                    this.setBlockAndMetadata(world, i1, 1, k12, this.plankBlock, this.plankMeta);
                    continue;
                }
                if (k12 % 3 == 0) {
                    this.setBlockAndMetadata(world, i1, 1, k12, Blocks.spruce_stairs, 6);
                    this.setBlockAndMetadata(world, i1, 2, k12, Blocks.spruce_stairs, 2);
                    continue;
                }
                if (k12 % 3 != 1) continue;
                this.setBlockAndMetadata(world, i1, 1, k12, Blocks.spruce_stairs, 7);
                this.setBlockAndMetadata(world, i1, 2, k12, Blocks.spruce_stairs, 3);
            }
            if (k12 < 5 || k12 > 15 || k12 % 3 != 2) continue;
            this.setBlockAndMetadata(world, -4, 1, k12, this.plankBlock, this.plankMeta);
            this.placeBarrel(world, random, -4, 2, k12, 4, drink);
            this.setBlockAndMetadata(world, 4, 1, k12, this.plankBlock, this.plankMeta);
            this.placeBarrel(world, random, 4, 2, k12, 5, drink);
        }
        for (k12 = 8; k12 <= 11; ++k12) {
            for (i1 = -1; i1 <= 1; ++i1) {
                if (Math.abs(i1) == 1 && (k12 == 8 || k12 == 11)) {
                    this.setBlockAndMetadata(world, i1, 1, k12, this.plankBlock, this.plankMeta);
                    continue;
                }
                this.setBlockAndMetadata(world, i1, 1, k12, this.plankSlabBlock, this.plankSlabMeta | 8);
            }
            this.placeMug(world, random, -1, 2, k12, 1, drink, LOTRFoods.ELF_DRINK);
            this.placeMug(world, random, 1, 2, k12, 3, drink, LOTRFoods.ELF_DRINK);
        }
        this.setBlockAndMetadata(world, 0, 1, 17, Blocks.crafting_table, 0);
        for (i13 = -2; i13 <= 2; ++i13) {
            if (Math.abs(i13) < 1) continue;
            this.setBlockAndMetadata(world, i13, 1, 17, Blocks.chest, 2);
            TileEntity tileentity = this.getTileEntity(world, i13, 1, 17);
            if (!(tileentity instanceof TileEntityChest)) continue;
            TileEntityChest chest = (TileEntityChest) tileentity;
            int wines = MathHelper.getRandomIntegerInRange(random, 3, 6);
            for (int l = 0; l < wines; ++l) {
                ItemStack chestDrinkItem = drink.copy();
                chestDrinkItem.stackSize = 1;
                LOTRItemMug.setStrengthMeta(chestDrinkItem, MathHelper.getRandomIntegerInRange(random, 1, 4));
                LOTRItemMug.Vessel[] chestVessels = LOTRFoods.ELF_DRINK.getDrinkVessels();
                LOTRItemMug.Vessel v = chestVessels[random.nextInt(chestVessels.length)];
                LOTRItemMug.setVessel(chestDrinkItem, v, true);
                chest.setInventorySlotContents(random.nextInt(chest.getSizeInventory()), chestDrinkItem);
            }
        }
        LOTREntityHithlumVintnerElf vintner = new LOTREntityHithlumVintnerElf(world);
        this.spawnNPCAndSetHome(vintner, world, 0, 1, 13, 16);
        return true;
    }
}
