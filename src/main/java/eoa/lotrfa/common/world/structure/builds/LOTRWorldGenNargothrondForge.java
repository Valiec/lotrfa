package eoa.lotrfa.common.world.structure.builds;

import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.LOTREntityNargothrondSmith;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityElf;
import lotr.common.world.structure2.LOTRWorldGenRivendellForge;
import net.minecraft.world.World;

public class LOTRWorldGenNargothrondForge extends LOTRWorldGenRivendellForge {

    public LOTRWorldGenNargothrondForge(boolean flag) {
        super(flag);
        this.brickBlock = LOTRMod.brick5;
        this.brickMeta = 2;
        this.pillarBlock = LOTRMod.pillar2;
        this.pillarMeta = 6;
        this.slabBlock = LOTRMod.slabSingle9;
        this.slabMeta = 7;
        this.carvedBrickBlock = LOTRMod.brick5;
        this.carvedBrickMeta = 6;
        this.wallBlock = LOTRMod.wall3;
        this.wallMeta = 10;
        this.stairBlock = LOTRMod.stairsDorwinionBrick;
        this.roofStairBlock = LOTRMod.stairsClayTileDyedGreen;
        this.roofMeta = 13;
        this.tableBlock = LOTRFABlocks.craftingTableNargothrond;
        // TODO Auto-generated constructor stub
    }

    @Override
    protected LOTREntityElf getElf(World world) {
        return new LOTREntityNargothrondSmith(world);
    }

}
