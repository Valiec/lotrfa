package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangArcher;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangWarrior;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRMod;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.world.structure2.LOTRWorldGenStructureBase2;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTRWorldGenUlfangWatchtower extends LOTRWorldGenStructureBase2{
	
    protected Block woodBlock;
    protected int woodMeta;
    protected Block plankBlock;
    protected int plankMeta;
    protected Block plankSlabBlock;
    protected int plankSlabMeta;
    protected Block plankStairBlock;
    protected Block fenceBlock;
    protected int fenceMeta;
    protected Block fenceGateBlock;
    protected Block doorBlock;
    protected Block roofBlock;
    protected int roofMeta;
    protected Block plank2Block;
    protected int plank2Meta;
    protected Block plank2SlabBlock;
    protected int plank2SlabMeta;
    protected Block plank2StairBlock;
    protected Block boneBlock;
    protected int boneMeta;
    protected Block bedBlock;

	
    public LOTRWorldGenUlfangWatchtower(final boolean flag) {
        super(flag);
    }
    
    protected boolean isRuined() {
        return false;
    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        this.woodBlock = Blocks.log;
        this.woodMeta = 1;
        this.plankBlock = Blocks.planks;
        this.plankMeta = 1;
        this.plankSlabBlock = Blocks.wooden_slab;
        this.plankSlabMeta = 1;
        this.plankStairBlock = Blocks.spruce_stairs;
        this.fenceBlock = Blocks.fence;
        this.fenceMeta = 1;
        this.fenceGateBlock = LOTRMod.fenceGateSpruce;
        this.doorBlock = LOTRMod.doorSpruce;
        final int randomWool = random.nextInt(3);
        if (randomWool == 0) {
            this.roofBlock = Blocks.wool;
            this.roofMeta = 7;
        }
        else if (randomWool == 1) {
            this.roofBlock = Blocks.wool;
            this.roofMeta = 12;
        }
        else if (randomWool == 2) {
            this.roofBlock = Blocks.wool;
            this.roofMeta = 15;
        }
        final int randomFloorWood = random.nextInt(2);
        if (randomFloorWood == 0) {
            this.plank2Block = LOTRMod.planks2;
            this.plank2Meta = 11;
            this.plank2SlabBlock = LOTRMod.woodSlabSingle4;
            this.plank2SlabMeta = 3;
            this.plank2StairBlock = LOTRMod.stairsOlive;
        }
        else if (randomFloorWood == 1) {
            this.plank2Block = LOTRMod.planks3;
            this.plank2Meta = 0;
            this.plank2SlabBlock = LOTRMod.woodSlabSingle5;
            this.plank2SlabMeta = 0;
            this.plank2StairBlock = LOTRMod.stairsPlum;
        }
        this.boneBlock = LOTRMod.boneBlock;
        this.boneMeta = 0;
        this.bedBlock = LOTRMod.strawBed;
        if (this.isRuined()) {
            if (random.nextBoolean()) {
                this.woodBlock = LOTRMod.wood;
                this.woodMeta = 3;
                this.plankBlock = LOTRMod.planks;
                this.plankMeta = 3;
                this.plankSlabBlock = LOTRMod.woodSlabSingle;
                this.plankSlabMeta = 3;
                this.plankStairBlock = LOTRMod.stairsCharred;
                this.fenceBlock = LOTRMod.fence;
                this.fenceMeta = 3;
                this.fenceGateBlock = LOTRMod.fenceGateCharred;
                this.doorBlock = LOTRMod.doorCharred;
            }
            if (random.nextBoolean()) {
                this.plank2Block = LOTRMod.planks;
                this.plank2Meta = 3;
                this.plank2SlabBlock = LOTRMod.woodSlabSingle;
                this.plank2SlabMeta = 3;
                this.plank2StairBlock = LOTRMod.stairsCharred;
            }
        }
    }
    
    protected ItemStack getRandomHarnedorWeapon(final Random random) {
        final ItemStack[] items = { new ItemStack(LOTRFAItems.ulfangSword), new ItemStack(LOTRFAItems.ulfangDagger), new ItemStack(LOTRFAItems.ulfangSpear), new ItemStack(LOTRFAItems.ulfangPike) };
        return items[random.nextInt(items.length)].copy();
    }
    
    protected ItemStack getHarnedorFramedItem(final Random random) {
        final ItemStack[] items = { new ItemStack(LOTRMod.helmetDunlending), new ItemStack(LOTRMod.bodyDunlending), new ItemStack(LOTRMod.legsDunlending), new ItemStack(LOTRMod.bootsDunlending), new ItemStack(LOTRFAItems.ulfangDagger), new ItemStack(LOTRFAItems.ulfangSword), new ItemStack(LOTRFAItems.ulfangSpear), new ItemStack(LOTRFAItems.ulfangPike), new ItemStack(LOTRFAItems.ulfangBow), new ItemStack(Items.arrow), new ItemStack(Items.skull), new ItemStack(Items.bone), new ItemStack(LOTRMod.gobletGold), new ItemStack(LOTRMod.gobletSilver), new ItemStack(LOTRMod.mug), new ItemStack(LOTRMod.ceramicMug), new ItemStack(LOTRMod.goldRing), new ItemStack(LOTRMod.silverRing), new ItemStack(LOTRMod.doubleFlower, 1, 2), new ItemStack(LOTRMod.doubleFlower, 1, 3) };
        return items[random.nextInt(items.length)].copy();
    }
    
    @Override
    public boolean generateWithSetRotation(final World world, final Random random, final int i, final int j, final int k, final int rotation) {
        this.setOriginAndRotation(world, i, j, k, rotation, 0);
        this.setupRandomBlocks(random);
        if (this.restrictions) {
            int minHeight = 0;
            int maxHeight = 0;
            for (int i2 = -3; i2 <= 3; ++i2) {
                for (int k2 = -3; k2 <= 3; ++k2) {
                    final int j2 = this.getTopBlock(world, i2, k2) - 1;
                    if (!this.isSurface(world, i2, j2, k2)) {
                        return false;
                    }
                    if (j2 < minHeight) {
                        minHeight = j2;
                    }
                    if (j2 > maxHeight) {
                        maxHeight = j2;
                    }
                    if (maxHeight - minHeight > 8) {
                        return false;
                    }
                }
            }
        }
        for (int i3 = -3; i3 <= 3; ++i3) {
            for (int k3 = -3; k3 <= 3; ++k3) {
                for (int j3 = 6; j3 <= 16; ++j3) {
                    this.setAir(world, i3, j3, k3);
                }
            }
        }
        this.loadStrScan("harnedor_tower");
        this.associateBlockMetaAlias("PLANK", this.plankBlock, this.plankMeta);
        this.associateBlockMetaAlias("PLANK_SLAB", this.plankSlabBlock, this.plankSlabMeta);
        this.associateBlockMetaAlias("FENCE", this.fenceBlock, this.fenceMeta);
        this.associateBlockMetaAlias("ROOF", this.roofBlock, this.roofMeta);
        this.generateStrScan(world, random, 0, 1, 0);
        this.placeSkull(world, random, -3, 5, -3);
        this.placeSkull(world, random, 3, 6, -3);
        this.placeSkull(world, random, 3, 6, 3);
        this.placeSkull(world, random, -3, 7, -2);
        this.placeSkull(world, random, -3, 7, 2);
        this.placeSkull(world, random, 0, 8, 3);
        this.placeSkull(world, random, -3, 10, 3);
        this.placeSkull(world, random, -3, 12, -3);
        this.placeSkull(world, random, 3, 13, 2);
        this.placeChest(world, random, -2, 11, 2, Blocks.chest, 2, LOTRFAChestContents.house_ulfang_house);
        for (int warriors = 1 + random.nextInt(2), l = 0; l < warriors; ++l) {
            final LOTREntityUlfangWarrior warrior = (random.nextInt(3) == 0) ? new LOTREntityUlfangArcher(world) : new LOTREntityUlfangWarrior(world);
            warrior.spawnRidingHorse = false;
            this.spawnNPCAndSetHome(warrior, world, 0, 13, 0, 8);
        }
        final LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClasses(LOTREntityUlfangWarrior.class, LOTREntityUlfangArcher.class);
        respawner.setCheckRanges(6, -16, 4, 4);
        respawner.setSpawnRanges(2, -1, 1, 8);
        this.placeNPCRespawner(respawner, world, 0, 13, 0);
        return true;
    }

}
