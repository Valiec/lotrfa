package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import lotr.common.LOTRMod;

public class LOTRWorldGenAngbandForgeTent extends LOTRWorldGenAngbandTent {
    public LOTRWorldGenAngbandForgeTent(boolean b) {
        super(b);
    }

    @Override
    protected void setupRandomBlocks(Random random) {
        super.setupRandomBlocks(random);
        this.tentBlock = LOTRMod.brick;
        this.tentMeta = 0;
        this.fenceBlock = LOTRMod.wall;
        this.fenceMeta = 1;
        this.hasOrcForge = true;
    }
}
