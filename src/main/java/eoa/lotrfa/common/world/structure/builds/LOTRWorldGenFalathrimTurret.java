package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.LOTREntityFalathrimMariner;
import eoa.lotrfa.common.entity.npc.LOTREntityFalathrimMarinerCaptain;
import lotr.common.LOTRMod;
import lotr.common.world.structure.LOTRWorldGenHighElvenTurret;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRWorldGenFalathrimTurret extends LOTRWorldGenHighElvenTurret {

    public LOTRWorldGenFalathrimTurret(boolean flag) {
        super(flag);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean generate(World world, Random random, int i, int j, int k) {
        int j1;
        int j12;
        int k1;
        Block block;
        int i1;
        int k12;
        int j13;
        if (this.restrictions && (block = world.getBlock(i, j - 1, k)) != Blocks.grass && block != Blocks.dirt && block != Blocks.stone) {
            return false;
        }
        --j;
        int rotation = random.nextInt(4);
        if (!this.restrictions && this.usingPlayer != null) {
            rotation = this.usingPlayerRotation();
        }
        switch (rotation) {
            case 0: {
                k += 6;
                break;
            }
            case 1: {
                i -= 6;
                break;
            }
            case 2: {
                k -= 6;
                break;
            }
            case 3: {
                i += 6;
            }
        }
        for (i1 = i - 4; i1 <= i + 4; ++i1) {
            for (k12 = k - 4; k12 <= k + 4; ++k12) {
                for (j12 = j; !(j12 != j && LOTRMod.isOpaque(world, i1, j12, k12) || j12 < 0); --j12) {
                    this.setBlockAndNotifyAdequately(world, i1, j12, k12, LOTRFABlocks.brick, 5);
                    this.setGrassToDirt(world, i1, j12 - 1, k12);
                }
                for (j12 = j + 1; j12 <= j + 7; ++j12) {
                    if (Math.abs(i1 - i) == 4 || Math.abs(k12 - k) == 4) {
                        this.setBlockAndNotifyAdequately(world, i1, j12, k12, LOTRFABlocks.brick, 5);
                        continue;
                    }
                    this.setBlockAndNotifyAdequately(world, i1, j12, k12, Blocks.air, 0);
                }
            }
        }
        for (i1 = i - 3; i1 <= i + 3; ++i1) {
            for (k12 = k - 3; k12 <= k + 3; ++k12) {
                if (Math.abs(i1 - i) % 2 == Math.abs(k12 - k) % 2) {
                    this.setBlockAndNotifyAdequately(world, i1, j, k12, LOTRFABlocks.pillar, 6);
                    continue;
                }
                this.setBlockAndNotifyAdequately(world, i1, j, k12, Blocks.double_stone_slab, 0);
            }
        }
        for (j1 = j + 1; j1 <= j + 7; ++j1) {
            this.setBlockAndNotifyAdequately(world, i - 3, j1, k - 3, LOTRFABlocks.pillar, 6);
            this.setBlockAndNotifyAdequately(world, i - 3, j1, k + 3, LOTRFABlocks.pillar, 6);
            this.setBlockAndNotifyAdequately(world, i + 3, j1, k - 3, LOTRFABlocks.pillar, 6);
            this.setBlockAndNotifyAdequately(world, i + 3, j1, k + 3, LOTRFABlocks.pillar, 6);
        }
        for (i1 = i - 4; i1 <= i + 4; ++i1) {
            this.setBlockAndNotifyAdequately(world, i1, j + 7, k - 4, LOTRFABlocks.stairsFalas, 2);
            this.setBlockAndNotifyAdequately(world, i1, j + 7, k + 4, LOTRFABlocks.stairsFalas, 3);
        }
        for (k1 = k - 3; k1 <= k + 3; ++k1) {
            this.setBlockAndNotifyAdequately(world, i - 4, j + 7, k1, LOTRFABlocks.stairsFalas, 0);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 7, k1, LOTRFABlocks.stairsFalas, 1);
        }
        for (i1 = i - 3; i1 <= i + 3; ++i1) {
            for (k12 = k - 3; k12 <= k + 3; ++k12) {
                for (j12 = j + 7; j12 <= j + 15; ++j12) {
                    if (Math.abs(i1 - i) == 3 || Math.abs(k12 - k) == 3) {
                        if (j12 - j >= 10 && j12 - j <= 14 && Math.abs(i1 - i) >= 3 && Math.abs(k12 - k) >= 3) continue;
                        this.setBlockAndNotifyAdequately(world, i1, j12, k12, LOTRFABlocks.brick, 5);
                        continue;
                    }
                    this.setBlockAndNotifyAdequately(world, i1, j12, k12, Blocks.air, 0);
                }
            }
        }
        for (i1 = i - 4; i1 <= i + 4; ++i1) {
            for (k12 = k - 4; k12 <= k + 4; ++k12) {
                for (j12 = j + 16; j12 <= j + 18; ++j12) {
                    if (j12 - j == 16 || Math.abs(i1 - i) == 4 || Math.abs(k12 - k) == 4) {
                        if (j12 - j == 18 && (Math.abs(i1 - i) % 2 == 1 || Math.abs(k12 - k) % 2 == 1)) {
                            this.setBlockAndNotifyAdequately(world, i1, j12, k12, Blocks.air, 0);
                            continue;
                        }
                        this.setBlockAndNotifyAdequately(world, i1, j12, k12, LOTRFABlocks.brick, 5);
                        continue;
                    }
                    this.setBlockAndNotifyAdequately(world, i1, j12, k12, Blocks.air, 0);
                }
            }
        }
        for (i1 = i - 4; i1 <= i + 4; ++i1) {
            this.setBlockAndNotifyAdequately(world, i1, j + 16, k - 4, LOTRFABlocks.stairsFalas, 6);
            this.setBlockAndNotifyAdequately(world, i1, j + 16, k + 4, LOTRFABlocks.stairsFalas, 7);
        }
        for (k1 = k - 3; k1 <= k + 3; ++k1) {
            this.setBlockAndNotifyAdequately(world, i - 4, j + 16, k1, LOTRFABlocks.stairsFalas, 4);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 16, k1, LOTRFABlocks.stairsFalas, 5);
        }
        if (rotation == 0) {
            for (i1 = i - 1; i1 <= i + 1; ++i1) {
                this.setBlockAndNotifyAdequately(world, i1, j, k - 5, Blocks.double_stone_slab, 0);
                this.setBlockAndNotifyAdequately(world, i1, j, k - 4, Blocks.double_stone_slab, 0);
            }
            for (j1 = j + 1; j1 <= j + 2; ++j1) {
                this.setBlockAndNotifyAdequately(world, i - 1, j1, k - 5, LOTRFABlocks.brick, 5);
                this.setBlockAndNotifyAdequately(world, i, j1, k - 5, Blocks.air, 0);
                this.setBlockAndNotifyAdequately(world, i, j1, k - 4, Blocks.air, 0);
                this.setBlockAndNotifyAdequately(world, i + 1, j1, k - 5, LOTRFABlocks.brick, 5);
            }
            this.setBlockAndNotifyAdequately(world, i - 1, j + 3, k - 5, LOTRFABlocks.stairsFalas, 0);
            this.setBlockAndNotifyAdequately(world, i, j + 3, k - 5, LOTRFABlocks.brick, 5);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 3, k - 5, LOTRFABlocks.stairsFalas, 1);
            for (i1 = i + 1; i1 <= i + 2; ++i1) {
                for (j13 = j + 1; j13 <= j + 7; ++j13) {
                    this.setBlockAndNotifyAdequately(world, i1, j13, k + 3, LOTRFABlocks.brick, 5);
                }
                for (j13 = j + 1; j13 <= j + 16; ++j13) {
                    this.setBlockAndNotifyAdequately(world, i1, j13, k + 2, Blocks.ladder, 2);
                }
            }
            this.setBlockAndNotifyAdequately(world, i, j + 1, k - 5, Blocks.wooden_door, 1);
            this.setBlockAndNotifyAdequately(world, i, j + 2, k - 5, Blocks.wooden_door, 8);
        }
        else if (rotation == 1) {
            for (k1 = k - 1; k1 <= k + 1; ++k1) {
                this.setBlockAndNotifyAdequately(world, i + 5, j, k1, Blocks.double_stone_slab, 0);
                this.setBlockAndNotifyAdequately(world, i + 4, j, k1, Blocks.double_stone_slab, 0);
            }
            for (j1 = j + 1; j1 <= j + 2; ++j1) {
                this.setBlockAndNotifyAdequately(world, i + 5, j1, k - 1, LOTRFABlocks.brick, 5);
                this.setBlockAndNotifyAdequately(world, i + 5, j1, k, Blocks.air, 0);
                this.setBlockAndNotifyAdequately(world, i + 4, j1, k, Blocks.air, 0);
                this.setBlockAndNotifyAdequately(world, i + 5, j1, k + 1, LOTRFABlocks.brick, 5);
            }
            this.setBlockAndNotifyAdequately(world, i + 5, j + 3, k - 1, LOTRFABlocks.stairsFalas, 2);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 3, k, LOTRFABlocks.brick, 5);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 3, k + 1, LOTRFABlocks.stairsFalas, 3);
            for (k1 = k - 1; k1 >= k - 2; --k1) {
                for (j13 = j + 1; j13 <= j + 7; ++j13) {
                    this.setBlockAndNotifyAdequately(world, i - 3, j13, k1, LOTRFABlocks.brick, 5);
                }
                for (j13 = j + 1; j13 <= j + 16; ++j13) {
                    this.setBlockAndNotifyAdequately(world, i - 2, j13, k1, Blocks.ladder, 5);
                }
            }
            this.setBlockAndNotifyAdequately(world, i + 5, j + 1, k, Blocks.wooden_door, 1);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 2, k, Blocks.wooden_door, 8);
        }
        else if (rotation == 2) {
            for (i1 = i - 1; i1 <= i + 1; ++i1) {
                this.setBlockAndNotifyAdequately(world, i1, j, k + 5, Blocks.double_stone_slab, 0);
                this.setBlockAndNotifyAdequately(world, i1, j, k + 4, Blocks.double_stone_slab, 0);
            }
            for (j1 = j + 1; j1 <= j + 2; ++j1) {
                this.setBlockAndNotifyAdequately(world, i - 1, j1, k + 5, LOTRFABlocks.brick, 5);
                this.setBlockAndNotifyAdequately(world, i, j1, k + 5, Blocks.air, 0);
                this.setBlockAndNotifyAdequately(world, i, j1, k + 4, Blocks.air, 0);
                this.setBlockAndNotifyAdequately(world, i + 1, j1, k + 5, LOTRFABlocks.brick, 5);
            }
            this.setBlockAndNotifyAdequately(world, i - 1, j + 3, k + 5, LOTRFABlocks.stairsFalas, 0);
            this.setBlockAndNotifyAdequately(world, i, j + 3, k + 5, LOTRFABlocks.brick, 5);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 3, k + 5, LOTRFABlocks.stairsFalas, 1);
            for (i1 = i - 1; i1 >= i - 2; --i1) {
                for (j13 = j + 1; j13 <= j + 7; ++j13) {
                    this.setBlockAndNotifyAdequately(world, i1, j13, k - 3, LOTRFABlocks.brick, 5);
                }
                for (j13 = j + 1; j13 <= j + 16; ++j13) {
                    this.setBlockAndNotifyAdequately(world, i1, j13, k - 2, Blocks.ladder, 3);
                }
            }
            this.setBlockAndNotifyAdequately(world, i, j + 1, k + 5, Blocks.wooden_door, 1);
            this.setBlockAndNotifyAdequately(world, i, j + 2, k + 5, Blocks.wooden_door, 8);
        }
        else if (rotation == 3) {
            for (k1 = k - 1; k1 <= k + 1; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 5, j, k1, Blocks.double_stone_slab, 0);
                this.setBlockAndNotifyAdequately(world, i - 4, j, k1, Blocks.double_stone_slab, 0);
            }
            for (j1 = j + 1; j1 <= j + 2; ++j1) {
                this.setBlockAndNotifyAdequately(world, i - 5, j1, k - 1, LOTRFABlocks.brick, 5);
                this.setBlockAndNotifyAdequately(world, i - 5, j1, k, Blocks.air, 0);
                this.setBlockAndNotifyAdequately(world, i - 4, j1, k, Blocks.air, 0);
                this.setBlockAndNotifyAdequately(world, i - 5, j1, k + 1, LOTRFABlocks.brick, 5);
            }
            this.setBlockAndNotifyAdequately(world, i - 5, j + 3, k - 1, LOTRFABlocks.stairsFalas, 2);
            this.setBlockAndNotifyAdequately(world, i - 5, j + 3, k, LOTRFABlocks.brick, 5);
            this.setBlockAndNotifyAdequately(world, i - 5, j + 3, k + 1, LOTRFABlocks.stairsFalas, 3);
            for (k1 = k + 1; k1 <= k + 2; ++k1) {
                for (j13 = j + 1; j13 <= j + 7; ++j13) {
                    this.setBlockAndNotifyAdequately(world, i + 3, j13, k1, LOTRFABlocks.brick, 5);
                }
                for (j13 = j + 1; j13 <= j + 16; ++j13) {
                    this.setBlockAndNotifyAdequately(world, i + 2, j13, k1, Blocks.ladder, 4);
                }
            }
            this.setBlockAndNotifyAdequately(world, i - 5, j + 1, k, Blocks.wooden_door, 1);
            this.setBlockAndNotifyAdequately(world, i - 5, j + 2, k, Blocks.wooden_door, 8);
        }
        this.setBlockAndNotifyAdequately(world, i - 3, j + 3, k, LOTRMod.highElvenTorch, 1);
        this.setBlockAndNotifyAdequately(world, i + 3, j + 3, k, LOTRMod.highElvenTorch, 2);
        this.setBlockAndNotifyAdequately(world, i, j + 3, k - 3, LOTRMod.highElvenTorch, 3);
        this.setBlockAndNotifyAdequately(world, i, j + 3, k + 3, LOTRMod.highElvenTorch, 4);
        this.setBlockAndNotifyAdequately(world, i - 3, j + 18, k, LOTRMod.highElvenTorch, 1);
        this.setBlockAndNotifyAdequately(world, i + 3, j + 18, k, LOTRMod.highElvenTorch, 2);
        this.setBlockAndNotifyAdequately(world, i, j + 18, k - 3, LOTRMod.highElvenTorch, 3);
        this.setBlockAndNotifyAdequately(world, i, j + 18, k + 3, LOTRMod.highElvenTorch, 4);
        LOTREntityFalathrimMarinerCaptain captain = new LOTREntityFalathrimMarinerCaptain(world);
        captain.setLocationAndAngles((i), j + 17, (k), 0.0f, 0.0f);
        captain.spawnRidingHorse = false;
        captain.onSpawnWithEgg(null);
        captain.isNPCPersistent = true;
        world.spawnEntityInWorld(captain);
        captain.setHomeArea(i, j + 17, k, 24);

        LOTREntityFalathrimMariner elf1 = new LOTREntityFalathrimMariner(world);
        elf1.setLocationAndAngles((i), j + 1, k + 1, 0.0f, 0.0f);
        elf1.spawnRidingHorse = false;
        elf1.onSpawnWithEgg(null);
        elf1.isNPCPersistent = true;
        world.spawnEntityInWorld(elf1);
        elf1.setHomeArea(i, j + 1, k, 24);

        LOTREntityFalathrimMariner elf2 = new LOTREntityFalathrimMariner(world);
        elf2.setLocationAndAngles((i), j + 1, (k), 0.0f, 0.0f);
        elf2.spawnRidingHorse = false;
        elf2.onSpawnWithEgg(null);
        elf2.isNPCPersistent = true;
        world.spawnEntityInWorld(elf2);
        elf2.setHomeArea(i, j + 1, k + 1, 24);

        return true;
    }

}
