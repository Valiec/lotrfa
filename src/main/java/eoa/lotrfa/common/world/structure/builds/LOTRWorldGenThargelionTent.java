package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import lotr.common.LOTRMod;
import lotr.common.world.structure.LOTRChestContents;
import lotr.common.world.structure2.LOTRWorldGenDorwinionTent;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRWorldGenThargelionTent extends LOTRWorldGenDorwinionTent {

    private Block woolBlock;
    private int woolMeta;
    private int clayMeta;
    private Block clayBlock;
    private int claySlabMeta;
    private Block claySlabBlock;
    private Block clayStairBlock;

    public LOTRWorldGenThargelionTent(boolean flag) {
        super(flag);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void setupRandomBlocks(Random random) {
        int randomWood = random.nextInt(4);
        if (randomWood == 0) {
            this.woodBeamBlock = LOTRMod.woodBeamV1;
            this.woodBeamMeta = 0;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 0;
            this.plankSlabBlock = Blocks.wooden_slab;
            this.plankSlabMeta = 0;
            this.plankStairBlock = Blocks.oak_stairs;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 0;
        }
        else if (randomWood == 1) {
            this.woodBeamBlock = LOTRMod.woodBeamV1;
            this.woodBeamMeta = 2;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 2;
            this.plankSlabBlock = Blocks.wooden_slab;
            this.plankSlabMeta = 2;
            this.plankStairBlock = Blocks.birch_stairs;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 2;
        }
        else if (randomWood == 2) {
            this.woodBeamBlock = LOTRMod.woodBeam2;
            this.woodBeamMeta = 1;
            this.plankBlock = LOTRMod.planks;
            this.plankMeta = 9;
            this.plankSlabBlock = LOTRMod.woodSlabSingle2;
            this.plankSlabMeta = 1;
            this.plankStairBlock = LOTRMod.stairsBeech;
            this.fenceBlock = LOTRMod.fence;
            this.fenceMeta = 9;
        }
        else if (randomWood == 3) {
            this.woodBeamBlock = LOTRMod.woodBeamFruit;
            this.woodBeamMeta = 0;
            this.plankBlock = LOTRMod.planks;
            this.plankMeta = 4;
            this.plankSlabBlock = LOTRMod.woodSlabSingle;
            this.plankSlabMeta = 4;
            this.plankStairBlock = LOTRMod.stairsApple;
            this.fenceBlock = LOTRMod.fence;
            this.fenceMeta = 4;
        }
        int randomFloor = random.nextInt(4);
        if (randomFloor == 0) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 1;
        }
        else if (randomFloor == 1) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 7;
        }
        else if (randomFloor == 2) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 8;
        }
        else if (randomFloor == 3) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 14;
        }
        int randomWool = random.nextInt(4);
        if (randomWool == 0) {
            this.woolBlock = Blocks.wool;
            this.woolMeta = 8;
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 8;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle2;
            this.claySlabMeta = 0;
            this.clayStairBlock = LOTRMod.stairsClayTileDyedLightGray;
        }
        if (randomWool == 1) {
            this.woolBlock = Blocks.wool;
            this.woolMeta = 7;
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 7;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle;
            this.claySlabMeta = 7;
            this.clayStairBlock = LOTRMod.stairsClayTileDyedGray;
        }
        else if (randomWool == 2) {
            this.woolBlock = Blocks.wool;
            this.woolMeta = 1;
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 1;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle;
            this.claySlabMeta = 1;
            this.clayStairBlock = LOTRMod.stairsClayTileDyedOrange;
        }
        else if (randomWool == 3) {
            this.woolBlock = Blocks.wool;
            this.woolMeta = 14;
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 14;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle2;
            this.claySlabMeta = 6;
            this.clayStairBlock = LOTRMod.stairsClayTileDyedRed;
        }
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        int k1;
        int i1;
        this.setOriginAndRotation(world, i, j, k, rotation, 3);
        this.setupRandomBlocks(random);
        if (this.restrictions) {
            for (i1 = -2; i1 <= 2; ++i1) {
                for (k1 = -3; k1 <= 3; ++k1) {
                    int j1 = this.getTopBlock(world, i1, k1) - 1;
                    if (this.isSurface(world, i1, j1, k1)) continue;
                    return false;
                }
            }
        }
        for (i1 = -2; i1 <= 2; ++i1) {
            for (k1 = -2; k1 <= 2; ++k1) {
                int j1;
                int i2 = Math.abs(i1);
                int k2 = Math.abs(k1);
                for (j1 = 0; !(j1 != 0 && this.isOpaque(world, i1, j1, k1) || this.getY(j1) < 0); --j1) {
                    this.setBlockAndMetadata(world, i1, j1, k1, this.floorBlock, this.floorMeta);
                    this.setGrassToDirt(world, i1, j1 - 1, k1);
                }
                for (j1 = 1; j1 <= 4; ++j1) {
                    this.setAir(world, i1, j1, k1);
                }
                if (i2 == 2 && k2 == 2) {
                    for (j1 = 1; j1 <= 2; ++j1) {
                        this.setBlockAndMetadata(world, i1, j1, k1, this.woodBeamBlock, this.woodBeamMeta);
                    }
                }
                else if (i2 == 2) {
                    for (j1 = 1; j1 <= 2; ++j1) {
                        if (k1 % 2 == 0) {
                            this.setBlockAndMetadata(world, i1, j1, k1, this.woolBlock, this.woolMeta);
                            continue;
                        }
                        this.setBlockAndMetadata(world, i1, j1, k1, this.woolBlock, this.woolMeta);
                    }
                }
                if (i2 != 0 || k2 != 2) continue;
                for (j1 = 1; j1 <= 3; ++j1) {
                    this.setBlockAndMetadata(world, i1, j1, k1, this.fenceBlock, this.fenceMeta);
                }
            }
        }
        for (int k12 = -2; k12 <= 2; ++k12) {
            if (k12 % 2 == 0) {
                this.setBlockAndMetadata(world, -2, 3, k12, this.clayStairBlock, 1);
                this.setBlockAndMetadata(world, 2, 3, k12, this.clayStairBlock, 0);
                this.setBlockAndMetadata(world, -1, 3, k12, this.clayStairBlock, 4);
                this.setBlockAndMetadata(world, 1, 3, k12, this.clayStairBlock, 5);
                this.setBlockAndMetadata(world, -1, 4, k12, this.clayStairBlock, 1);
                this.setBlockAndMetadata(world, 1, 4, k12, this.clayStairBlock, 0);
                this.setBlockAndMetadata(world, 0, 4, k12, this.clayBlock, this.clayMeta);
                this.setBlockAndMetadata(world, 0, 5, k12, this.claySlabBlock, this.claySlabMeta);
                continue;
            }
            this.setBlockAndMetadata(world, -2, 3, k12, this.clayStairBlock, 1);
            this.setBlockAndMetadata(world, 2, 3, k12, this.clayStairBlock, 0);
            this.setBlockAndMetadata(world, -1, 3, k12, this.clayStairBlock, 4);
            this.setBlockAndMetadata(world, 1, 3, k12, this.clayStairBlock, 5);
            this.setBlockAndMetadata(world, -1, 4, k12, this.clayStairBlock, 1);
            this.setBlockAndMetadata(world, 1, 4, k12, this.clayStairBlock, 0);
            this.setBlockAndMetadata(world, 0, 4, k12, this.clayBlock, this.clayMeta);
            this.setBlockAndMetadata(world, 0, 5, k12, this.claySlabBlock, this.claySlabMeta);
        }
        if (random.nextBoolean()) {
            this.placeChest(world, random, -1, 1, 0, 4, LOTRChestContents.HIGH_ELVEN_HALL);
            // this.setBlockAndMetadata(world, 1, 2, 0, Blocks.torch, 1);
        }
        else {
            this.placeChest(world, random, 1, 1, 0, 4, LOTRChestContents.HIGH_ELVEN_HALL);
            // this.setBlockAndMetadata(world, -1, 2, 0, Blocks.torch, 2);
        }
        return true;
    }

}
