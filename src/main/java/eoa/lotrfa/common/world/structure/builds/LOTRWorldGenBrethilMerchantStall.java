package eoa.lotrfa.common.world.structure.builds;

import eoa.lotrfa.common.block.LOTRFABlocks;
import lotr.common.LOTRMod;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;

public class LOTRWorldGenBrethilMerchantStall extends LOTRWorldGenEdainMerchantStall {
    public LOTRWorldGenBrethilMerchantStall(boolean b, int i, Block brethilct) {
        super(b, i, brethilct);
        this.elf = i;
        this.brickBlock = LOTRMod.brick2;
        this.brickMeta = 3;
        this.pillarBlock = LOTRMod.pillar2;
        this.pillarMeta = 2;
        this.slabBlock = LOTRMod.slabSingle4;
        this.slabMeta = 1;
        this.carvedBrickBlock = LOTRMod.brick2;
        this.carvedBrickMeta = 6;
        this.wallBlock = LOTRMod.wall2;
        this.wallMeta = 4;
        this.stairBlock = LOTRMod.stairsGondorBrick;
        this.torchBlock = Blocks.torch;
        this.tableBlock = LOTRFABlocks.craftingTableBrethil;
        this.barsBlock = LOTRMod.silverBars;
        this.woodBarsBlock = Blocks.fence;
        this.roofBlock = LOTRMod.thatch;
        this.roofMeta = 0;
        this.roofStairBlock = LOTRMod.stairsThatch;
        this.tableBlock = brethilct;
    }
}
