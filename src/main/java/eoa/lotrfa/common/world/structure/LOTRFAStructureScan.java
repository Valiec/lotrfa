package eoa.lotrfa.common.world.structure;

import java.util.*;
import java.util.Map.Entry;
import cpw.mods.fml.common.FMLCommonHandler;
import eoa.lotrfa.common.LOTRFA;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import eoa.lotrfa.common.util.StringBanks;
import lotr.common.world.structure2.scan.LOTRStructureScan;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;

public class LOTRFAStructureScan extends LOTRStructureScan {
    private static final String ALIAS_PREFIX = "#";
    private static final String ALIAS_META_PREFIX = "~";
    private static final String BLOCK_ID_PREFIX = "\"";
    private static final String SPECIAL_PREFIX = "/";
    private static final String FILL_DOWN_SUFFIX = "v";
    private static final String FIND_LOWEST_SUFFIX = "_";
    private static final String SEPARATOR = ".";
    private static final String REGEX_SEPARATOR = "\\.";

    public LOTRFAStructureScan(String name) {
        super(name);
    }
    
    public static void loadAllScans(Object mod) {
        Map<String, String[]> scanMap = StringBanks.getStructureScans(mod);
        Map<String, LOTRStructureScan> scans = new HashMap<String, LOTRStructureScan>();
        
        for (Entry<String, String[]> entry : scanMap.entrySet()) {
            String strName = entry.getKey();
            String[] lines = entry.getValue();
            int lineNumber = 0;
            
            LOTRFAStructureScan scan = new LOTRFAStructureScan(strName);
            HashSet<String> blockAliases = new HashSet<String>();
            HashSet<String> blockMetaAliases = new HashSet<String>();
            
            for (String line : lines) {
                lineNumber++;
                if (line.length() == 0) continue;
                
                if (line.startsWith(ALIAS_PREFIX)) {
                    blockAliases.add(line.replace(ALIAS_PREFIX, ""));
                    continue;
                }
                else if (line.startsWith(ALIAS_META_PREFIX)) {
                    blockMetaAliases.add(line.replace(ALIAS_META_PREFIX, ""));
                    continue;
                }

                ScanStepBase step = null;
                int x;
                int y;
                int z;
                boolean fillDown = false;
                boolean findLowest = false;
                
                String[] lineArr = line.split(REGEX_SEPARATOR, 4);
                x = Integer.parseInt(lineArr[0]);
                z = Integer.parseInt(lineArr[2]);
                
                String stringY = lineArr[1];
                if(stringY.endsWith(FILL_DOWN_SUFFIX)) {
                    fillDown = true;
                    stringY = stringY.replace(FILL_DOWN_SUFFIX, "");         
                }
                else if(stringY.endsWith(FIND_LOWEST_SUFFIX)) {
                    findLowest = true;
                    stringY = stringY.replace(FIND_LOWEST_SUFFIX, "");
                }
                y = Integer.parseInt(stringY);
                
                String stringBlock = lineArr[3];
                if(stringBlock.startsWith(BLOCK_ID_PREFIX)) {
                    String[] blockIDArr = stringBlock.split(BLOCK_ID_PREFIX);
                    
                    Block block = Block.getBlockFromName(blockIDArr[1]);
                    if (block == null) {
                        LOTRFA.logger.error("LOTRFAStrScan: Block " + blockIDArr[1] + " does not exist!");
                        block = Blocks.stone;
                    }
                    
                    int meta = Integer.parseInt(blockIDArr[2].replace(SEPARATOR, ""));

                    step = new ScanStep(x, y, z, block, meta);
                }
                else if(stringBlock.startsWith(ALIAS_PREFIX)) {
                    String[] blockAliasArr = stringBlock.split(REGEX_SEPARATOR);
                    String alias = blockAliasArr[0].replace(ALIAS_PREFIX, "");
                    int meta = Integer.parseInt(blockAliasArr[1]);
                    

                    step = new ScanStepBlockAlias(x, y, z, alias, meta);
                }
                else if(stringBlock.startsWith(ALIAS_META_PREFIX)) {
                    String alias = stringBlock.replace(ALIAS_META_PREFIX, "");
                    
                    step = new ScanStepBlockMetaAlias(x, y, z, alias);
                }
                else if(stringBlock.startsWith(SPECIAL_PREFIX)) {
                    String code = stringBlock.replace(SPECIAL_PREFIX, "");
                    
                    if(code.equals("SKULL")) {
                        step = new ScanStepSkull(x, y, z);
                    }  
                }
            
                if (step != null) {
                    step.fillDown = fillDown;
                    step.findLowest = findLowest;
                    scan.addScanStep(step);
                }
                else throw new IllegalArgumentException("Invalid scan instruction on line " + lineNumber + " in " + strName);
            }
            
            scans.put(scan.scanName, scan);
        }
        
        LOTRFA.logger.info("Created " + scans.size() + " scans for " + FMLCommonHandler.instance().findContainerFor(mod).getModId());
        LOTRFAReflectionHelper.addSTRScans(scans);
    }
}
