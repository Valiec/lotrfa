package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.entity.npc.LOTREntityAngbandOrc;
import eoa.lotrfa.common.entity.npc.LOTREntityAngbandOrcCaptain;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import lotr.common.LOTRFoods;
import lotr.common.LOTRMod;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.world.structure.LOTRWorldGenStructureBase;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRWorldGenAngbandTower extends LOTRWorldGenStructureBase {
    public LOTRWorldGenAngbandTower(boolean flag) {
        super(flag);
    }

    @Override
    public boolean generate(World world, Random random, int i, int j, int k) {
        int rotation;
        System.out.println("GENERATING!");
        int j1;
        int k1;
        int j12;
        int k12;
        int i1;
        int sections;
        int equipmentSection;

        --j;
        rotation = random.nextInt(4);
        if (!this.restrictions && this.usingPlayer != null) {
            rotation = this.usingPlayerRotation();
        }

        switch (rotation) {
            case 0:
                k += 7;
                break;
            case 1:
                i -= 7;
                break;
            case 2:
                k -= 7;
                break;
            case 3:
                i += 7;
        }

        sections = 2 + random.nextInt(3);
        equipmentSection = 1 + random.nextInt(sections);
        if (this.restrictions) {
            j1 = world.getHeightValue(i, k) - 1;
            Block block = world.getBlock(i, j1, k);
            int meta = world.getBlockMetadata(i, j1, k);
            if (block != LOTRMod.mordorDirt && block != LOTRMod.mordorGravel && (block != LOTRMod.rock || meta != 0) && block != Blocks.stone && block != Blocks.gravel && block != Blocks.dirt) {
                System.out.println(block.getUnlocalizedName());
                return false;
            }
        }

        for (k1 = k - 2; k1 <= k + 2; ++k1) {
            for (j12 = j; !LOTRMod.isOpaque(world, i - 6, j12, k1) && j12 >= 0; --j12) {
                this.setBlockAndNotifyAdequately(world, i - 6, j12, k1, LOTRMod.brick, 0);
            }
            for (j12 = j; !LOTRMod.isOpaque(world, i + 6, j12, k1) && j12 >= 0; --j12) {
                this.setBlockAndNotifyAdequately(world, i + 6, j12, k1, LOTRMod.brick, 0);
            }
        }
        for (k1 = k - 4; k1 <= k + 4; ++k1) {
            for (j12 = j; !LOTRMod.isOpaque(world, i - 5, j12, k1) && j12 >= 0; --j12) {
                this.setBlockAndNotifyAdequately(world, i - 5, j12, k1, LOTRMod.brick, 0);
            }
            for (j12 = j; !LOTRMod.isOpaque(world, i + 5, j12, k1) && j12 >= 0; --j12) {
                this.setBlockAndNotifyAdequately(world, i + 5, j12, k1, LOTRMod.brick, 0);
            }
        }
        for (k1 = k - 5; k1 <= k + 5; ++k1) {
            for (i1 = i - 4; i1 <= i - 3; ++i1) {
                for (j1 = j; !LOTRMod.isOpaque(world, i1, j1, k1) && j1 >= 0; --j1) {
                    this.setBlockAndNotifyAdequately(world, i1, j1, k1, LOTRMod.brick, 0);
                }
            }
            for (i1 = i + 3; i1 <= i + 4; ++i1) {
                for (j1 = j; !LOTRMod.isOpaque(world, i1, j1, k1) && j1 >= 0; --j1) {
                    this.setBlockAndNotifyAdequately(world, i1, j1, k1, LOTRMod.brick, 0);
                }
            }
        }
        for (k1 = k - 6; k1 <= k + 6; ++k1) {
            for (i1 = i - 2; i1 <= i + 2; ++i1) {
                for (j1 = j; !LOTRMod.isOpaque(world, i1, j1, k1) && j1 >= 0; --j1) {
                    this.setBlockAndNotifyAdequately(world, i1, j1, k1, LOTRMod.brick, 0);
                }
            }
        }
        for (int l = 0; l <= sections; ++l) {
            this.generateTowerSection(world, random, i, j, k, l, false, l == equipmentSection);
        }
        this.generateTowerSection(world, random, i, j, k, sections + 1, true, false);
        LOTREntityAngbandOrcCaptain trader = new LOTREntityAngbandOrcCaptain(world);
        trader.setLocationAndAngles(i + 0.5, j + (sections + 1) * 8 + 1, k - 4 + 0.5, world.rand.nextFloat() * 360.0f, 0.0f);
        trader.onSpawnWithEgg(null);
        trader.setHomeArea(i, j + (sections + 1) * 8, k, 24);
        world.spawnEntityInWorld(trader);
        switch (rotation) {
            case 0: {
                for (i1 = i - 1; i1 <= i + 1; ++i1) {
                    this.setBlockAndNotifyAdequately(world, i1, j, k - 6, LOTRMod.slabDouble, 0);
                    for (j1 = j + 1; j1 <= j + 4; ++j1) {
                        this.setBlockAndNotifyAdequately(world, i1, j1, k - 6, LOTRMod.gateOrc, 3);
                    }
                }
                this.placeWallBanner(world, i, j + 7, k - 6, 2, LOTRFAItemBanner.FABannerTypes.angband);
                break;
            }
            case 1: {
                for (k12 = k - 1; k12 <= k + 1; ++k12) {
                    this.setBlockAndNotifyAdequately(world, i + 6, j, k12, LOTRMod.slabDouble, 0);
                    for (j1 = j + 1; j1 <= j + 4; ++j1) {
                        this.setBlockAndNotifyAdequately(world, i + 6, j1, k12, LOTRMod.gateOrc, 4);
                    }
                }
                this.placeWallBanner(world, i + 6, j + 7, k, 3, LOTRFAItemBanner.FABannerTypes.angband);
                break;
            }
            case 2: {
                for (i1 = i - 1; i1 <= i + 1; ++i1) {
                    this.setBlockAndNotifyAdequately(world, i1, j, k + 6, LOTRMod.slabDouble, 0);
                    for (j1 = j + 1; j1 <= j + 4; ++j1) {
                        this.setBlockAndNotifyAdequately(world, i1, j1, k + 6, LOTRMod.gateOrc, 2);
                    }
                }
                this.placeWallBanner(world, i, j + 7, k + 6, 0, LOTRFAItemBanner.FABannerTypes.angband);
                break;
            }
            case 3: {
                for (k12 = k - 1; k12 <= k + 1; ++k12) {
                    this.setBlockAndNotifyAdequately(world, i - 6, j, k12, LOTRMod.slabDouble, 0);
                    for (j1 = j + 1; j1 <= j + 4; ++j1) {
                        this.setBlockAndNotifyAdequately(world, i - 6, j1, k12, LOTRMod.gateOrc, 5);
                    }
                }
                this.placeWallBanner(world, i - 6, j + 7, k, 1, LOTRFAItemBanner.FABannerTypes.angband);
            }
        }
        LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClass(LOTREntityAngbandOrc.class);
        respawner.setCheckRanges(12, -8, 50, 20);
        respawner.setSpawnRanges(5, 1, 40, 16);
        this.placeNPCRespawner(respawner, world, i, j, k);
        return true;
    }

    private void generateTowerSection(World world, Random random, int i, int j, int k, int section, boolean isTop, boolean isEquipmentSection) {
        int j1;
        int i1;
        for (j1 = section == 0 ? j : (j += section * 8) + 1; j1 <= (isTop ? j + 10 : j + 8); ++j1) {
            int i12;
            int k1;
            Block fillBlock = Blocks.air;
            int fillMeta = 0;
            if (j1 == j) {
                fillBlock = LOTRMod.slabDouble;
                fillMeta = 0;
            }
            else if (j1 == j + 8 && !isTop) {
                fillBlock = LOTRMod.slabSingle;
                fillMeta = 8;
            }
            else {
                fillBlock = Blocks.air;
                fillMeta = 0;
            }
            for (k1 = k - 2; k1 <= k + 2; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 5, j1, k1, fillBlock, fillMeta);
                this.setBlockAndNotifyAdequately(world, i + 5, j1, k1, fillBlock, fillMeta);
            }
            for (k1 = k - 4; k1 <= k + 4; ++k1) {
                for (i12 = i - 4; i12 <= i - 3; ++i12) {
                    this.setBlockAndNotifyAdequately(world, i12, j1, k1, fillBlock, fillMeta);
                }
                for (i12 = i + 3; i12 <= i + 4; ++i12) {
                    this.setBlockAndNotifyAdequately(world, i12, j1, k1, fillBlock, fillMeta);
                }
            }
            for (k1 = k - 5; k1 <= k + 5; ++k1) {
                for (i12 = i - 2; i12 <= i + 2; ++i12) {
                    this.setBlockAndNotifyAdequately(world, i12, j1, k1, fillBlock, fillMeta);
                }
            }
        }
        for (j1 = j + 1; j1 <= (isTop ? j + 1 : j + 8); ++j1) {
            for (int k1 = k - 2; k1 <= k + 2; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 6, j1, k1, LOTRMod.brick, 0);
                this.setBlockAndNotifyAdequately(world, i + 6, j1, k1, LOTRMod.brick, 0);
            }
            for (int i13 = i - 2; i13 <= i + 2; ++i13) {
                this.setBlockAndNotifyAdequately(world, i13, j1, k - 6, LOTRMod.brick, 0);
                this.setBlockAndNotifyAdequately(world, i13, j1, k + 6, LOTRMod.brick, 0);
            }
            this.setBlockAndNotifyAdequately(world, i - 5, j1, k - 4, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i - 5, j1, k - 3, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i - 5, j1, k + 3, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i - 5, j1, k + 4, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i - 4, j1, k - 5, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i - 4, j1, k + 5, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i - 3, j1, k - 5, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i - 3, j1, k + 5, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i + 3, j1, k - 5, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i + 3, j1, k + 5, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i + 4, j1, k - 5, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i + 4, j1, k + 5, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i + 5, j1, k - 4, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i + 5, j1, k - 3, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i + 5, j1, k + 3, LOTRMod.brick, 0);
            this.setBlockAndNotifyAdequately(world, i + 5, j1, k + 4, LOTRMod.brick, 0);
        }
        this.placeOrcTorch(world, i - 5, j + 1, k - 2);
        this.placeOrcTorch(world, i - 5, j + 1, k + 2);
        this.placeOrcTorch(world, i + 5, j + 1, k - 2);
        this.placeOrcTorch(world, i + 5, j + 1, k + 2);
        this.placeOrcTorch(world, i - 2, j + 1, k - 5);
        this.placeOrcTorch(world, i + 2, j + 1, k - 5);
        this.placeOrcTorch(world, i - 2, j + 1, k + 5);
        this.placeOrcTorch(world, i + 2, j + 1, k + 5);
        if (!isTop) {
            for (j1 = j + 2; j1 <= j + 4; ++j1) {
                for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                    this.setBlockAndNotifyAdequately(world, i - 6, j1, k1, LOTRMod.orcSteelBars, 0);
                    this.setBlockAndNotifyAdequately(world, i + 6, j1, k1, LOTRMod.orcSteelBars, 0);
                }
                for (int i14 = i - 1; i14 <= i + 1; ++i14) {
                    this.setBlockAndNotifyAdequately(world, i14, j1, k - 6, LOTRMod.orcSteelBars, 0);
                    this.setBlockAndNotifyAdequately(world, i14, j1, k + 6, LOTRMod.orcSteelBars, 0);
                }
            }
            for (i1 = i - 2; i1 <= i + 2; ++i1) {
                for (int k1 = k - 2; k1 <= k + 2; ++k1) {
                    this.setBlockAndNotifyAdequately(world, i1, j + 8, k1, Blocks.air, 0);
                }
            }
            this.setBlockAndNotifyAdequately(world, i - 2, j + 1, k + 1, LOTRMod.slabSingle, 0);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 1, k + 2, LOTRMod.slabSingle, 8);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 2, k + 2, LOTRMod.slabSingle, 0);
            this.setBlockAndNotifyAdequately(world, i, j + 2, k + 2, LOTRMod.slabSingle, 8);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 3, k + 2, LOTRMod.slabSingle, 0);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 3, k + 2, LOTRMod.slabSingle, 8);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 4, k + 1, LOTRMod.slabSingle, 0);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 4, k, LOTRMod.slabSingle, 8);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 5, k - 1, LOTRMod.slabSingle, 0);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 5, k - 2, LOTRMod.slabSingle, 8);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 6, k - 2, LOTRMod.slabSingle, 0);
            this.setBlockAndNotifyAdequately(world, i, j + 6, k - 2, LOTRMod.slabSingle, 8);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 7, k - 2, LOTRMod.slabSingle, 0);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 7, k - 2, LOTRMod.slabSingle, 8);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 8, k - 1, LOTRMod.slabSingle, 0);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 8, k, LOTRMod.slabSingle, 8);
        }
        for (i1 = i - 1; i1 <= i + 1; ++i1) {
            for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                for (int j12 = j + 1; j12 <= (isTop ? j + 3 : j + 8); ++j12) {
                    this.setBlockAndNotifyAdequately(world, i1, j12, k1, LOTRMod.brick, 0);
                }
            }
        }
        if (isEquipmentSection) {
            int l = random.nextInt(4);
            switch (l) {
                case 0: {
                    for (int i15 = i - 1; i15 <= i + 1; ++i15) {
                        this.setBlockAndNotifyAdequately(world, i15, j + 1, k - 5, LOTRMod.orcBomb, 0);
                        this.setBlockAndNotifyAdequately(world, i15, j + 1, k + 5, LOTRMod.slabSingle, 9);
                        this.placeBarrel(world, random, i15, j + 2, k + 5, 2, LOTRFoods.ORC_DRINK);
                    }
                    break;
                }
                case 1: {
                    for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                        this.setBlockAndNotifyAdequately(world, i + 5, j + 1, k1, LOTRMod.orcBomb, 0);
                        this.setBlockAndNotifyAdequately(world, i - 5, j + 1, k1, LOTRMod.slabSingle, 9);
                        this.placeBarrel(world, random, i - 5, j + 2, k1, 5, LOTRFoods.ORC_DRINK);
                    }
                    break;
                }
                case 2: {
                    for (int i16 = i - 1; i16 <= i + 1; ++i16) {
                        this.setBlockAndNotifyAdequately(world, i16, j + 1, k + 5, LOTRMod.orcBomb, 0);
                        this.setBlockAndNotifyAdequately(world, i16, j + 1, k - 5, LOTRMod.slabSingle, 9);
                        this.placeBarrel(world, random, i16, j + 2, k - 5, 3, LOTRFoods.ORC_DRINK);
                    }
                    break;
                }
                case 3: {
                    for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                        this.setBlockAndNotifyAdequately(world, i - 5, j + 1, k1, LOTRMod.orcBomb, 0);
                        this.setBlockAndNotifyAdequately(world, i + 5, j + 1, k1, LOTRMod.slabSingle, 9);
                        this.placeBarrel(world, random, i + 5, j + 2, k1, 4, LOTRFoods.ORC_DRINK);
                    }
                    break;
                }
            }
        }
        if (isTop) {
            for (j1 = j + 1; j1 <= j + 8; ++j1) {
                for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                    this.setBlockAndNotifyAdequately(world, i - 7, j1, k1, LOTRMod.brick, 0);
                    this.setBlockAndNotifyAdequately(world, i + 7, j1, k1, LOTRMod.brick, 0);
                }
                for (int i17 = i - 1; i17 <= i + 1; ++i17) {
                    this.setBlockAndNotifyAdequately(world, i17, j1, k - 7, LOTRMod.brick, 0);
                    this.setBlockAndNotifyAdequately(world, i17, j1, k + 7, LOTRMod.brick, 0);
                }
            }
            for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 7, j, k1, LOTRMod.stairsMordorBrick, 4);
                this.setBlockAndNotifyAdequately(world, i - 6, j + 2, k1, LOTRMod.stairsMordorBrick, 1);
                this.setBlockAndNotifyAdequately(world, i - 7, j + 9, k1, LOTRMod.stairsMordorBrick, 0);
                this.setBlockAndNotifyAdequately(world, i - 6, j + 9, k1, LOTRMod.stairsMordorBrick, 5);
                this.setBlockAndNotifyAdequately(world, i - 6, j + 10, k1, LOTRMod.stairsMordorBrick, 0);
                this.setBlockAndNotifyAdequately(world, i + 7, j, k1, LOTRMod.stairsMordorBrick, 5);
                this.setBlockAndNotifyAdequately(world, i + 6, j + 2, k1, LOTRMod.stairsMordorBrick, 0);
                this.setBlockAndNotifyAdequately(world, i + 7, j + 9, k1, LOTRMod.stairsMordorBrick, 1);
                this.setBlockAndNotifyAdequately(world, i + 6, j + 9, k1, LOTRMod.stairsMordorBrick, 4);
                this.setBlockAndNotifyAdequately(world, i + 6, j + 10, k1, LOTRMod.stairsMordorBrick, 1);
            }
            for (i1 = i - 1; i1 <= i + 1; ++i1) {
                this.setBlockAndNotifyAdequately(world, i1, j, k - 7, LOTRMod.stairsMordorBrick, 6);
                this.setBlockAndNotifyAdequately(world, i1, j + 2, k - 6, LOTRMod.stairsMordorBrick, 3);
                this.setBlockAndNotifyAdequately(world, i1, j + 9, k - 7, LOTRMod.stairsMordorBrick, 2);
                this.setBlockAndNotifyAdequately(world, i1, j + 9, k - 6, LOTRMod.stairsMordorBrick, 7);
                this.setBlockAndNotifyAdequately(world, i1, j + 10, k - 6, LOTRMod.stairsMordorBrick, 2);
                this.setBlockAndNotifyAdequately(world, i1, j, k + 7, LOTRMod.stairsMordorBrick, 7);
                this.setBlockAndNotifyAdequately(world, i1, j + 2, k + 6, LOTRMod.stairsMordorBrick, 2);
                this.setBlockAndNotifyAdequately(world, i1, j + 9, k + 7, LOTRMod.stairsMordorBrick, 3);
                this.setBlockAndNotifyAdequately(world, i1, j + 9, k + 6, LOTRMod.stairsMordorBrick, 6);
                this.setBlockAndNotifyAdequately(world, i1, j + 10, k + 6, LOTRMod.stairsMordorBrick, 3);
            }
            for (j1 = j; j1 <= j + 4; ++j1) {
                this.setBlockAndNotifyAdequately(world, i - 5, j1, k - 5, LOTRMod.brick, 0);
                this.setBlockAndNotifyAdequately(world, i - 5, j1, k + 5, LOTRMod.brick, 0);
                this.setBlockAndNotifyAdequately(world, i + 5, j1, k - 5, LOTRMod.brick, 0);
                this.setBlockAndNotifyAdequately(world, i + 5, j1, k + 5, LOTRMod.brick, 0);
            }
            this.placeBanner(world, i - 5, j + 5, k - 5, 0, LOTRFAItemBanner.FABannerTypes.angband);
            this.placeBanner(world, i - 5, j + 5, k + 5, 0, LOTRFAItemBanner.FABannerTypes.angband);
            this.placeBanner(world, i + 5, j + 5, k - 5, 0, LOTRFAItemBanner.FABannerTypes.angband);
            this.placeBanner(world, i + 5, j + 5, k + 5, 0, LOTRFAItemBanner.FABannerTypes.angband);
            this.setBlockAndNotifyAdequately(world, i - 5, j + 2, k - 4, LOTRMod.stairsMordorBrick, 3);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 2, k - 5, LOTRMod.stairsMordorBrick, 1);
            this.setBlockAndNotifyAdequately(world, i - 5, j + 2, k + 4, LOTRMod.stairsMordorBrick, 2);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 2, k + 5, LOTRMod.stairsMordorBrick, 1);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 2, k - 4, LOTRMod.stairsMordorBrick, 3);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 2, k - 5, LOTRMod.stairsMordorBrick, 0);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 2, k + 4, LOTRMod.stairsMordorBrick, 2);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 2, k + 5, LOTRMod.stairsMordorBrick, 0);
        }
    }
}
