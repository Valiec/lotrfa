package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRFoods;
import lotr.common.LOTRMod;
import lotr.common.entity.LOTREntityNPCRespawner;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.Direction;
import net.minecraft.world.World;

public class LOTRWorldGenFeanorianCaptainTent extends LOTRWorldGenFeanorianTent {

    private Block woolBlock;
    private int woolMeta;
    private int clayMeta;
    private Block clayBlock;
    private int claySlabMeta;
    private Block claySlabBlock;

    public LOTRWorldGenFeanorianCaptainTent(boolean flag) {
        super(flag);
    }

    @Override
    protected void setupRandomBlocks(Random random) {
        int randomWood = random.nextInt(4);
        if (randomWood == 0) {
            this.woodBeamBlock = LOTRMod.woodBeamV1;
            this.woodBeamMeta = 0;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 0;
            this.plankSlabBlock = Blocks.wooden_slab;
            this.plankSlabMeta = 0;
            this.plankStairBlock = Blocks.oak_stairs;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 0;
        }
        else if (randomWood == 1) {
            this.woodBeamBlock = LOTRMod.woodBeamV1;
            this.woodBeamMeta = 2;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 2;
            this.plankSlabBlock = Blocks.wooden_slab;
            this.plankSlabMeta = 2;
            this.plankStairBlock = Blocks.birch_stairs;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 2;
        }
        else if (randomWood == 2) {
            this.woodBeamBlock = LOTRMod.woodBeam2;
            this.woodBeamMeta = 1;
            this.plankBlock = LOTRMod.planks;
            this.plankMeta = 9;
            this.plankSlabBlock = LOTRMod.woodSlabSingle2;
            this.plankSlabMeta = 1;
            this.plankStairBlock = LOTRMod.stairsBeech;
            this.fenceBlock = LOTRMod.fence;
            this.fenceMeta = 9;
        }
        else if (randomWood == 3) {
            this.woodBeamBlock = LOTRMod.woodBeamFruit;
            this.woodBeamMeta = 0;
            this.plankBlock = LOTRMod.planks;
            this.plankMeta = 4;
            this.plankSlabBlock = LOTRMod.woodSlabSingle;
            this.plankSlabMeta = 4;
            this.plankStairBlock = LOTRMod.stairsApple;
            this.fenceBlock = LOTRMod.fence;
            this.fenceMeta = 4;
        }
        int randomFloor = random.nextInt(4);
        if (randomFloor == 0) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 1;
        }
        else if (randomFloor == 1) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 7;
        }
        else if (randomFloor == 2) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 8;
        }
        else if (randomFloor == 3) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 14;
        }
        int randomWool = random.nextInt(4);
        if (randomWool == 0) {
            this.woolBlock = Blocks.wool;
            this.woolMeta = 8;
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 8;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle2;
            this.claySlabMeta = 0;
        }
        if (randomWool == 1) {
            this.woolBlock = Blocks.wool;
            this.woolMeta = 7;
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 7;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle;
            this.claySlabMeta = 7;
        }
        else if (randomWool == 2) {
            this.woolBlock = Blocks.wool;
            this.woolMeta = 1;
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 1;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle;
            this.claySlabMeta = 1;
        }
        else if (randomWool == 3) {
            this.woolBlock = Blocks.wool;
            this.woolMeta = 14;
            this.clayBlock = LOTRMod.clayTileDyed;
            this.clayMeta = 14;
            this.claySlabBlock = LOTRMod.slabClayTileDyedSingle2;
            this.claySlabMeta = 6;
        }
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        int j1;
        int i2;
        int k1;
        int i1;
        int k2;
        this.setOriginAndRotation(world, i, j, k, rotation, 7);
        this.setupRandomBlocks(random);
        if (this.restrictions) {
            int minHeight = 0;
            int maxHeight = 0;
            int maxEdgeHeight = 0;
            for (int i12 = -15; i12 <= 15; ++i12) {
                for (int k12 = -15; k12 <= 15; ++k12) {
                    int j12 = this.getTopBlock(world, i12, k12);
                    Block block = this.getBlock(world, i12, j12 - 1, k12);
                    if (block != Blocks.grass) {
                        return false;
                    }
                    if (j12 < minHeight) {
                        minHeight = j12;
                    }
                    if (j12 > maxHeight) {
                        maxHeight = j12;
                    }
                    if (maxHeight - minHeight > 6) {
                        return false;
                    }
                    if (Math.abs(i12) != 8 && Math.abs(k12) != 8 || j12 <= maxEdgeHeight) continue;
                    maxEdgeHeight = j12;
                }
            }
            this.originY = this.getY(maxEdgeHeight);
        }
        int r = 35;
        int h = 7;
        for (i1 = -r; i1 <= r; ++i1) {
            for (k1 = -r; k1 <= r; ++k1) {
                i2 = Math.abs(i1);
                k2 = Math.abs(k1);
                boolean within = false;
                for (j1 = 0; j1 >= -h; --j1) {
                    int j2 = j1 + r - 3;
                    int d = i2 * i2 + j2 * j2 + k2 * k2;
                    if (d >= r * r) continue;
                    boolean grass = !this.isOpaque(world, i1, j1 + 1, k1);
                    this.setBlockAndMetadata(world, i1, j1, k1, grass ? Blocks.grass : Blocks.dirt, 0);
                    this.setGrassToDirt(world, i1, j1 - 1, k1);
                    within = true;
                }
                if (!within) continue;
                j1 = -h - 1;
                while (!this.isOpaque(world, i1, j1, k1) && this.getY(j1) >= 0) {
                    this.setBlockAndMetadata(world, i1, j1, k1, Blocks.dirt, 0);
                    this.setGrassToDirt(world, i1, j1 - 1, k1);
                    --j1;
                }
            }
        }
        for (i1 = -6; i1 <= 6; ++i1) {
            for (k1 = -6; k1 <= 6; ++k1) {
                i2 = Math.abs(i1);
                k2 = Math.abs(k1);
                boolean inside = false;
                if (i2 <= 3 && k2 <= 3) {
                    inside = true;
                }
                if (i2 == 4 && k2 <= 3 || k2 == 4 && i2 <= 3) {
                    inside = true;
                }
                if (i2 == 5 && k2 <= 2 || k2 == 5 && i2 <= 2) {
                    inside = true;
                }
                if (inside) {
                    this.setBlockAndMetadata(world, i1, 0, k1, this.floorBlock, this.floorMeta);
                    for (j1 = 1; j1 <= 4; ++j1) {
                        this.setAir(world, i1, j1, k1);
                    }
                }
                if (i2 == 6 && k2 == 2 || k2 == 6 && i2 == 2 || i2 == 4 && k2 == 4) {
                    this.setGrassToDirt(world, i1, 0, k1);
                    for (j1 = 1; j1 <= 3; ++j1) {
                        this.setBlockAndMetadata(world, i1, j1, k1, this.woodBeamBlock, this.woodBeamMeta);
                    }
                    this.setBlockAndMetadata(world, i1, 4, k1, this.claySlabBlock, this.claySlabMeta);
                }
                if (i2 == 5 && k2 == 3 || k2 == 5 && i2 == 3) {
                    this.setGrassToDirt(world, i1, 0, k1);
                    this.setBlockAndMetadata(world, i1, 1, k1, this.woolBlock, this.woolMeta);
                    this.setBlockAndMetadata(world, i1, 2, k1, this.woolBlock, this.woolMeta);
                    this.setBlockAndMetadata(world, i1, 3, k1, this.woolBlock, this.woolMeta);
                    this.setBlockAndMetadata(world, i1, 4, k1, this.claySlabBlock, this.claySlabMeta);
                }
                if (i2 == 5 && k2 == 4 || k2 == 5 && i2 == 4) {
                    for (j1 = 1; j1 <= 2; ++j1) {
                        this.setBlockAndMetadata(world, i1, j1, k1, this.fenceBlock, this.fenceMeta);
                    }
                    this.setBlockAndMetadata(world, i1, 3, k1, this.clayBlock, this.clayMeta);
                }
                if (i2 == 6 && k2 == 3 || k2 == 6 && i2 == 3) {
                    this.setBlockAndMetadata(world, i1, 3, k1, this.claySlabBlock, this.claySlabMeta | 8);
                }
                if (i2 == 6 && k2 <= 1 || k2 == 6 && i2 <= 1) {
                    this.setBlockAndMetadata(world, i1, 0, k1, this.floorBlock, this.floorMeta);
                    int gateMeta = Direction.directionToFacing[Direction.getMovementDirection(i1, k1)];
                    for (int j13 = 1; j13 <= 3; ++j13) {
                        this.setBlockAndMetadata(world, i1, j13, k1, LOTRMod.gateWooden, gateMeta);
                    }
                    this.setBlockAndMetadata(world, i1, 4, k1, this.claySlabBlock, this.claySlabMeta);
                }
                if (i2 == 5 && k2 == 2 || k2 == 5 && i2 == 2) {
                    this.setBlockAndMetadata(world, i1, 1, k1, this.plankBlock, this.plankMeta);
                    this.setBlockAndMetadata(world, i1, 2, k1, this.fenceBlock, this.fenceMeta);
                    this.setBlockAndMetadata(world, i1, 3, k1, this.fenceBlock, this.fenceMeta);
                    this.setBlockAndMetadata(world, i1, 4, k1, this.clayBlock, this.clayMeta);
                }
                if (i2 == 5 && k2 <= 1 || k2 == 5 && i2 <= 1) {
                    this.setBlockAndMetadata(world, i1, 4, k1, this.claySlabBlock, this.claySlabMeta | 8);
                }
                if (i2 == 4 && k2 == 3 || k2 == 4 && i2 == 3) {
                    this.setBlockAndMetadata(world, i1, 4, k1, this.claySlabBlock, this.claySlabMeta | 8);
                }
                if (i2 == 4 && k2 <= 2 || k2 == 4 && i2 <= 2 || i2 == 3 && k2 == 3) {
                    this.setBlockAndMetadata(world, i1, 5, k1, this.claySlabBlock, this.claySlabMeta);
                }
                if (i2 == 3 && k2 <= 2 || k2 == 3 && i2 <= 2 || i2 == 2 && k2 == 2) {
                    this.setBlockAndMetadata(world, i1, 5, k1, this.claySlabBlock, this.claySlabMeta);
                }
                if (i2 == 2 && k2 == 1 || k2 == 2 && i2 == 1) {
                    this.setBlockAndMetadata(world, i1, 5, k1, this.claySlabBlock, this.claySlabMeta);
                }
                if (i2 == 0 && k2 == 2 || k2 == 0 && i2 == 2 || i2 == 1 && k2 == 1) {
                    this.setBlockAndMetadata(world, i1, 5, k1, this.clayBlock, this.clayMeta);
                }
                if (i2 == 0 && k2 == 1 || k2 == 0 && i2 == 1 || i2 == 0 && k2 == 0) {
                    this.setBlockAndMetadata(world, i1, 5, k1, LOTRMod.highElfBars, 0);
                }
                if (i2 == 2 && k2 <= 1 || k2 == 2 && i2 <= 1 || i2 <= 1 && k2 <= 1) {
                    this.setBlockAndMetadata(world, i1, 0, k1, this.plankBlock, this.plankMeta);
                }
                if ((i2 != 2 || k2 != 0) && (k2 != 2 || i2 != 0)) continue;
                for (j1 = 1; j1 <= 4; ++j1) {
                    this.setBlockAndMetadata(world, i1, j1, k1, this.woodBeamBlock, this.woodBeamMeta);
                }
            }
        }
        this.setBlockAndMetadata(world, 0, 1, 0, LOTRMod.commandTable, 0);
        this.setBlockAndMetadata(world, 0, 3, -3, Blocks.torch, 4);
        this.setBlockAndMetadata(world, 0, 3, 3, Blocks.torch, 3);
        this.setBlockAndMetadata(world, -3, 3, 0, Blocks.torch, 1);
        this.setBlockAndMetadata(world, 3, 3, 0, Blocks.torch, 2);
        this.setBlockAndMetadata(world, -3, 1, -4, this.plankBlock, this.plankMeta);
        this.setBlockAndMetadata(world, -3, 1, -3, this.plankBlock, this.plankMeta);
        this.setBlockAndMetadata(world, -4, 1, -3, this.plankBlock, this.plankMeta);
        this.setBlockAndMetadata(world, -3, 1, -2, this.plankSlabBlock, this.plankSlabMeta);
        this.setBlockAndMetadata(world, -4, 1, -2, this.plankSlabBlock, this.plankSlabMeta);
        this.setBlockAndMetadata(world, -3, 2, -3, Blocks.bed, 2);
        this.setBlockAndMetadata(world, -3, 2, -4, Blocks.bed, 10);
        this.setBlockAndMetadata(world, 3, 1, -4, Blocks.crafting_table, 0);
        this.setBlockAndMetadata(world, 4, 1, -3, LOTRFABlocks.craftingTableFeanorian, 0);
        this.placeChest(world, random, -4, 1, 3, 2, LOTRFAChestContents.feanorian_house);
        this.placeChest(world, random, -3, 1, 4, 4, LOTRFAChestContents.feanorian_house);
        this.setBlockAndMetadata(world, 2, 1, 4, this.plankSlabBlock, this.plankSlabMeta | 8);
        this.placeMug(world, random, 2, 2, 4, 1, LOTRFoods.DORWINION_DRINK);
        this.setBlockAndMetadata(world, 3, 1, 3, this.plankSlabBlock, this.plankSlabMeta | 8);
        this.placeMug(world, random, 3, 2, 3, 0, LOTRFoods.DORWINION_DRINK);
        this.setBlockAndMetadata(world, 4, 1, 2, this.plankSlabBlock, this.plankSlabMeta | 8);
        this.placeMug(world, random, 4, 2, 2, 1, LOTRFoods.DORWINION_DRINK);
        this.setBlockAndMetadata(world, 3, 1, 4, this.plankBlock, this.plankMeta);
        this.placeBarrel(world, random, 3, 2, 4, 5, LOTRFoods.DORWINION_DRINK);
        this.setBlockAndMetadata(world, 4, 1, 3, this.plankBlock, this.plankMeta);
        this.placeBarrel(world, random, 4, 2, 3, 2, LOTRFoods.DORWINION_DRINK);
        for (int i13 : new int[] {-2, 2}) {
            this.placeWallBanner(world, i13, 3, -6, LOTRFAItemBanner.FABannerTypes.feanorian, 2);
            this.placeWallBanner(world, i13, 3, 6, LOTRFAItemBanner.FABannerTypes.feanorian, 0);
        }
        for (int k13 : new int[] {-2, 2}) {
            this.placeWallBanner(world, -6, 3, k13, LOTRFAItemBanner.FABannerTypes.feanorian, 3);
            this.placeWallBanner(world, 6, 3, k13, LOTRFAItemBanner.FABannerTypes.feanorian, 1);
        }
        LOTREntityFeanorianLord captain = new LOTREntityFeanorianLord(world);
        this.spawnNPCAndSetHome(captain, world, 0, 1, -1, 16);

        LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClasses(LOTREntityFeanorianWarrior.class, LOTREntityFeanorianElf.class);
        respawner.setCheckRanges(24, -12, 12, 12);
        respawner.setSpawnRanges(12, -2, 2, 16);
        this.placeNPCRespawner(respawner, world, 0, 0, 0);
        return true;
    }

}
