package eoa.lotrfa.common.world.structure.builds;

import eoa.lotrfa.common.entity.npc.LOTREntityHithlumSmith;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityElf;
import lotr.common.world.structure2.LOTRWorldGenHighElvenForge;
import net.minecraft.world.World;

public class LOTRWorldGenHithlumForge extends LOTRWorldGenHighElvenForge {
    public LOTRWorldGenHithlumForge(boolean b) {
        super(b);
        this.brickBlock = LOTRMod.brick3;
        this.brickMeta = 2;
        this.pillarBlock = LOTRMod.pillar;
        this.pillarMeta = 10;
        this.slabBlock = LOTRMod.slabSingle5;
        this.slabMeta = 5;
        this.carvedBrickBlock = LOTRMod.brick2;
        this.carvedBrickMeta = 13;
        this.wallBlock = LOTRMod.wall2;
        this.wallMeta = 11;
        this.stairBlock = LOTRMod.stairsHighElvenBrick;
        this.torchBlock = LOTRMod.highElvenTorch;
        this.tableBlock = LOTRMod.highElvenTable;
        this.barsBlock = LOTRMod.highElfBars;
        this.woodBarsBlock = LOTRMod.highElfWoodBars;
        this.roofBlock = LOTRMod.clayTileDyed;
        this.roofMeta = 11;
        this.roofStairBlock = LOTRMod.stairsClayTileDyedBlue;
    }
    
    @Override
    protected LOTREntityElf getElf(final World world) {
        return new LOTREntityHithlumSmith(world);
    }
}
