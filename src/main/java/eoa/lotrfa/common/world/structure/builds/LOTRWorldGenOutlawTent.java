package eoa.lotrfa.common.world.structure.builds;

import lotr.common.world.structure2.LOTRWorldGenRangerTent;

public class LOTRWorldGenOutlawTent extends LOTRWorldGenRangerTent {
    public LOTRWorldGenOutlawTent(boolean b) {
        super(b);
    }
}
