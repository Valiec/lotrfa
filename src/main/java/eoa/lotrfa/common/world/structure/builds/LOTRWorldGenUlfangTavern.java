package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangBartender;
import eoa.lotrfa.common.entity.npc.LOTREntityUlfangMan;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import lotr.common.LOTRFoods;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTRNames;
import lotr.common.world.structure.LOTRChestContents;
import lotr.common.world.structure2.LOTRWorldGenDunlendingTavern;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class LOTRWorldGenUlfangTavern extends LOTRWorldGenDunlendingTavern {

    private Block plankBlock;
    private int plankMeta;
    private Block woodBlock;
    private int woodMeta;
    private Block floorBlock;
    private int floorMeta;

    public LOTRWorldGenUlfangTavern(boolean flag) {
        super(flag);
    }

    @Override
    protected void setupRandomBlocks(Random random) {
        int randomFloor = random.nextInt(5);
        if (randomFloor == 0) {
            this.floorBlock = Blocks.cobblestone;
            this.floorMeta = 0;
        }
        else if (randomFloor == 1) {
            this.floorBlock = Blocks.hardened_clay;
            this.floorMeta = 0;
        }
        else if (randomFloor == 2) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 7;
        }
        else if (randomFloor == 3) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 12;
        }
        else if (randomFloor == 4) {
            this.floorBlock = Blocks.stained_hardened_clay;
            this.floorMeta = 15;
        }
        if (random.nextBoolean()) {
            this.woodBlock = Blocks.log;
            this.woodMeta = 1;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 1;
            this.plankSlabBlock = Blocks.wooden_slab;
            this.plankSlabMeta = 1;
            this.plankStairBlock = Blocks.spruce_stairs;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 1;
            this.fenceGateBlock = LOTRMod.fenceGateSpruce;
            this.doorBlock = LOTRMod.doorSpruce;
        }
        else {
            int randomWood = random.nextInt(2);
            if (randomWood == 0) {
                this.woodBlock = Blocks.log;
                this.woodMeta = 0;
                this.plankBlock = Blocks.planks;
                this.plankMeta = 0;
                this.plankSlabBlock = Blocks.wooden_slab;
                this.plankSlabMeta = 0;
                this.plankStairBlock = Blocks.oak_stairs;
                this.fenceBlock = Blocks.fence;
                this.fenceMeta = 0;
                this.fenceGateBlock = Blocks.fence_gate;
                this.doorBlock = Blocks.wooden_door;
            }
            else if (randomWood == 1) {
                this.woodBlock = LOTRMod.wood5;
                this.woodMeta = 0;
                this.plankBlock = LOTRMod.planks2;
                this.plankMeta = 4;
                this.plankSlabBlock = LOTRMod.woodSlabSingle3;
                this.plankSlabMeta = 4;
                this.plankStairBlock = LOTRMod.stairsPine;
                this.fenceBlock = LOTRMod.fence2;
                this.fenceMeta = 4;
                this.fenceGateBlock = LOTRMod.fenceGatePine;
                this.doorBlock = LOTRMod.doorPine;
            }
        }
        this.roofBlock = LOTRMod.thatch;
        this.roofMeta = 0;
        this.roofSlabBlock = LOTRMod.slabSingleThatch;
        this.roofSlabMeta = 0;
        this.roofStairBlock = LOTRMod.stairsThatch;
        if (random.nextBoolean()) {
            this.barsBlock = Blocks.iron_bars;
            this.barsMeta = 0;
        }
        else {
            this.barsBlock = LOTRMod.bronzeBars;
            this.barsMeta = 0;
        }
        this.bedBlock = random.nextBoolean() ? LOTRMod.furBed : LOTRMod.strawBed;
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        int j1;
        this.setOriginAndRotation(world, i, j, k, rotation, 8);
        this.setupRandomBlocks(random);
        if (this.restrictions) {
            int minHeight = 0;
            int maxHeight = 0;
            for (int i1 = -11; i1 <= 11; ++i1) {
                for (int k1 = -8; k1 <= 8; ++k1) {
                    j1 = this.getTopBlock(world, i1, k1) - 1;
                    if (!this.isSurface(world, i1, j1, k1)) {
                        return false;
                    }
                    if (j1 < minHeight) {
                        minHeight = j1;
                    }
                    if (j1 > maxHeight) {
                        maxHeight = j1;
                    }
                    if (maxHeight - minHeight <= 12) continue;
                    return false;
                }
            }
        }
        for (int i1 = -9; i1 <= 9; ++i1) {
            for (int k1 = -7; k1 <= 7; ++k1) {
                int i2 = Math.abs(i1);
                int k2 = Math.abs(k1);
                for (j1 = 1; j1 <= 7; ++j1) {
                    this.setAir(world, i1, j1, k1);
                }
                j1 = -1;
                while (!this.isOpaque(world, i1, j1, k1) && this.getY(j1) >= 0) {
                    this.setBlockAndMetadata(world, i1, j1, k1, this.floorBlock, this.floorMeta);
                    this.setGrassToDirt(world, i1, j1 - 1, k1);
                    --j1;
                }
                if (random.nextInt(4) != 0 || i2 <= 3 && k2 <= 2) continue;
                this.setBlockAndMetadata(world, i1, 1, k1, LOTRMod.thatchFloor, 0);
            }
        }
        this.loadStrScan("dunland_tavern");
        this.associateBlockMetaAlias("FLOOR", this.floorBlock, this.floorMeta);
        this.associateBlockMetaAlias("WOOD", this.woodBlock, this.woodMeta);
        this.associateBlockMetaAlias("WOOD|4", this.woodBlock, this.woodMeta | 4);
        this.associateBlockMetaAlias("WOOD|8", this.woodBlock, this.woodMeta | 8);
        this.associateBlockMetaAlias("PLANK", this.plankBlock, this.plankMeta);
        this.associateBlockMetaAlias("PLANK_SLAB", this.plankSlabBlock, this.plankSlabMeta);
        this.associateBlockMetaAlias("PLANK_SLAB_INV", this.plankSlabBlock, this.plankSlabMeta | 8);
        this.associateBlockAlias("PLANK_STAIR", this.plankStairBlock);
        this.associateBlockMetaAlias("FENCE", this.fenceBlock, this.fenceMeta);
        this.associateBlockAlias("FENCE_GATE", this.fenceGateBlock);
        this.associateBlockAlias("DOOR", this.doorBlock);
        this.associateBlockMetaAlias("ROOF", this.roofBlock, this.roofMeta);
        this.associateBlockMetaAlias("ROOF_SLAB", this.roofSlabBlock, this.roofSlabMeta);
        this.associateBlockMetaAlias("ROOF_SLAB_INV", this.roofSlabBlock, this.roofSlabMeta | 8);
        this.associateBlockAlias("ROOF_STAIR", this.roofStairBlock);
        this.associateBlockMetaAlias("BARS", this.barsBlock, this.barsMeta);
        this.generateStrScan(world, random, 0, 1, 0);
        this.placeFlowerPot(world, 8, 2, -5, this.getRandomFlower(world, random));
        this.placeFlowerPot(world, 8, 2, 5, this.getRandomFlower(world, random));
        this.placeFlowerPot(world, -8, 2, -4, this.getRandomFlower(world, random));
        this.placeFlowerPot(world, -8, 2, 4, this.getRandomFlower(world, random));
        this.placeChest(world, random, 7, 1, -5, LOTRMod.chestBasket, 5, LOTRChestContents.DUNLENDING_HOUSE);
        this.placeBarrel(world, random, 7, 2, 6, 2, LOTRFoods.DUNLENDING_DRINK);
        this.placeBarrel(world, random, 4, 2, 6, 2, LOTRFoods.DUNLENDING_DRINK);
        this.placeFoodOrDrink(world, random, -6, 2, -6);
        this.placeFoodOrDrink(world, random, -5, 2, -6);
        this.placeFoodOrDrink(world, random, -6, 2, -1);
        this.placeFoodOrDrink(world, random, -5, 2, -1);
        this.placeFoodOrDrink(world, random, -6, 2, 0);
        this.placeFoodOrDrink(world, random, -5, 2, 0);
        this.placeFoodOrDrink(world, random, -6, 2, 1);
        this.placeFoodOrDrink(world, random, -5, 2, 1);
        this.placeFoodOrDrink(world, random, -6, 2, 6);
        this.placeFoodOrDrink(world, random, -5, 2, 6);
        this.placeFoodOrDrink(world, random, -1, 2, 6);
        this.placeFoodOrDrink(world, random, 0, 2, 6);
        this.placeFoodOrDrink(world, random, 1, 2, 6);
        this.placeFoodOrDrink(world, random, 5, 2, 3);
        this.placeFoodOrDrink(world, random, 6, 2, 3);
        this.placeFoodOrDrink(world, random, 8, 2, 4);
        this.placeFoodOrDrink(world, random, 5, 2, -3);
        this.placeFoodOrDrink(world, random, 6, 2, -3);
        this.placeFoodOrDrink(world, random, 8, 2, -4);
        this.placeFoodOrDrink(world, random, 4, 2, -6);
        this.placeFoodOrDrink(world, random, 7, 2, -6);
        String[] tavernName = LOTRNames.getDunlendingTavernName(random);
        String tavernNameNPC = tavernName[0] + " " + tavernName[1];
        this.placeSign(world, 0, 3, -8, Blocks.wall_sign, 2, new String[] {"", tavernName[0], tavernName[1], ""});
        this.placeWallBanner(world, -8, 6, 0, LOTRFAItemBanner.FABannerTypes.houseUlfang, 1);
        this.placeWallBanner(world, 8, 6, 0, LOTRFAItemBanner.FABannerTypes.houseUlfang, 3);
        for (int k1 : new int[] {-3, 3}) {
            this.placeDunlandItemFrame(world, random, -3, 2, k1, 1);
            this.placeDunlandItemFrame(world, random, 3, 2, k1, 3);
        }
        LOTREntityUlfangBartender bartender = new LOTREntityUlfangBartender(world);
        bartender.setSpecificLocationName(tavernNameNPC);
        if (random.nextBoolean()) {
            this.spawnNPCAndSetHome(bartender, world, 5, 1, -4, 2);
        }
        else {
            this.spawnNPCAndSetHome(bartender, world, 5, 1, 4, 2);
        }
        int dunlendings = MathHelper.getRandomIntegerInRange(random, 3, 8);
        for (int l = 0; l < dunlendings; ++l) {
            LOTREntityUlfangMan dunlending = new LOTREntityUlfangMan(world);
            this.spawnNPCAndSetHome(dunlending, world, 0, 1, 0, 16);
        }
        return true;
    }

    private void placeFoodOrDrink(World world, Random random, int i, int j, int k) {
        if (random.nextBoolean()) {
            if (random.nextBoolean()) {
                this.placeMug(world, random, i, j, k, random.nextInt(4), LOTRFoods.DUNLENDING_DRINK);
            }
            else {
                Block plateBlock = random.nextBoolean() ? LOTRMod.woodPlateBlock : LOTRMod.ceramicPlateBlock;
                
                if (random.nextBoolean()) {
                    this.setBlockAndMetadata(world, i, j, k, plateBlock, 0);
                }
                else {
                    this.placePlateWithCertainty(world, random, i, j, k, plateBlock, LOTRFoods.DUNLENDING);
                }
            }
        }
    }

}
