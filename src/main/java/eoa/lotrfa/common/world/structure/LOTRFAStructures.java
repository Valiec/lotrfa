package eoa.lotrfa.common.world.structure;

import java.util.*;
import eoa.lotrfa.common.world.structure.builds.*;
import eoa.lotrfa.common.world.structure.strbuilds.LOTRFAWorldGenTestHouse;
import net.minecraft.world.gen.feature.WorldGenerator;

public class LOTRFAStructures {
    private static Map<Integer, Class<? extends WorldGenerator>> idToClassMapping = new HashMap<Integer, Class<? extends WorldGenerator>>();
    private static Map<Integer, String> idToStringMapping = new HashMap<Integer, String>();
    public static Map<Integer, StructureInfo> structureSpawners = new LinkedHashMap<Integer, StructureInfo>();

    private static void registerStructure(int id, Class<? extends WorldGenerator> structureClass, String name, int background, int foreground) {
        idToClassMapping.put(id, structureClass);
        idToStringMapping.put(id, name);
        structureSpawners.put(id, new StructureInfo(id, background, foreground));
    }

    public static WorldGenerator getStructureFromID(int id) {
        WorldGenerator generator = null;
        try {
            Class<? extends WorldGenerator> structureClass = idToClassMapping.get(id);
            if (structureClass != null) {
                generator = structureClass.getConstructor(new Class[] {boolean.class}).newInstance(new Object[] {true});
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return generator;
    }

    public static String getNameFromID(int id) {
        return idToStringMapping.get(id);
    }

    public static void registerStructures() {
        registerStructure(2000, LOTRWorldGenTolInGaurhothWargPit.class, "TolInGaurhothWargPit", 0x515647, 0x2c3b2d);
        registerStructure(2001, LOTRWorldGenDorDaidelosCamp.class, "DorDaidelosCamp", 0x50565d, 0x36393c);
        registerStructure(2002, LOTRWorldGenAngbandCamp.class, "AngbandCamp", 0x1e181d, 0xab1900);
        registerStructure(2003, LOTRWorldGenAngbandWargPit.class, "AngbandWargPit", 0x1e181d, 0xab1900);
        registerStructure(2004, LOTRWorldGenDDWatchtower.class, "DDWatchtower", 0x1e181d, 0xab1900);
        registerStructure(2005, LOTRWorldGenDDWargPit.class, "DDWargPit", 0x1e181d, 0xab1900);
        registerStructure(2006, LOTRWorldGenDDCamp.class, "DDCamp", 0x1e181d, 0xab1900);
        registerStructure(2007, LOTRWorldGenAngbandTower.class, "AngbandTower", 0x1e181d, 0xab1900);
        // replace
        registerStructure(2008, LOTRWorldGenFeanorianCamp.class, "FeanorianCamp", 0xe3e3e8, 0xe00000);
        registerStructure(2009, LOTRWorldGenFeanorianForge.class, "FeanorianForge", 0xe3e3e8, 0xe00000);
        registerStructure(2010, LOTRWorldGenFeanorianHall.class, "FeanorianHall", 0xe3e3e8, 0xe00000);
        registerStructure(2010, LOTRWorldGenFeanorianHillTower.class, "FeanorianHillTower", 0xe3e3e8, 0xe00000);
        registerStructure(2011, LOTRWorldGenFeanorianTower.class, "FeanorianTower", 0xe3e3e8, 0xe00000);
        registerStructure(2012, LOTRWorldGenNargothrondHall.class, "NargothrondHall", 0x22511d, 0xc2a70e);
        registerStructure(2013, LOTRWorldGenNargothrondHouse.class, "NargothrondHouse", 0x22511d, 0xc2a70e);

        registerStructure(2014, LOTRWorldGenHithlumTower.class, "HithlumTower", 0xc5c4c4, 0x27295e);
        registerStructure(2015, LOTRWorldGenHithlumHall.class, "HithlumHall", 0xc5c4c4, 0x27295e);
        registerStructure(2016, LOTRWorldGenHithlumForge.class, "HithlumForge", 0xc5c4c4, 0x27295e);

        registerStructure(2017, LOTRWorldGenFeanorianHouse.class, "FeanorianHouse", 0xe3e3e8, 0xe00000);

        registerStructure(2018, LOTRWorldGenNargothrondForge.class, "NargothrondForge", 0x22511d, 0xc2a70e);
        
        registerStructure(2019, LOTRWorldGenUtumnoRemnantCamp.class, "UtumnoRemnantCamp", 0x50565d, 0x36393c);
        registerStructure(2020, LOTRWorldGenUtumnoRemnantWargPit.class, "UtumnoRemnantWargPit", 0x50565d, 0x36393c);
        
        registerStructure(2021, LOTRWorldGenFalathrimMerchantStall.class, "FalathrimMerchantStall", 0xe3e3e8, 0x08566e);
        registerStructure(2022, LOTRWorldGenFalathrimSmithy.class, "FalathrimSmithy", 0xe3e3e8, 0x08566e);
        registerStructure(2023, LOTRWorldGenFalathrimHouse.class, "FalathrimHouse", 0xe3e3e8, 0x08566e);
        registerStructure(2024, LOTRWorldGenFalathrimTurret.class, "FalathrimTurret", 0xe3e3e8, 0x08566e);
        
        registerStructure(2025, LOTRWorldGenNargothrondMerchantStall.class, "NargothrondMerchantStall", 0x22511d, 0xc2a70e);
        
        registerStructure(2026, LOTRWorldGenTolInGaurhothAltar.class, "TolInGaurhothAltar", 0x515647, 0x2c3b2d);
        registerStructure(2027, LOTRWorldGenTolInGaurhothCamp.class, "TolInGaurhothCamp", 0x515647, 0x2c3b2d);
        registerStructure(2028, LOTRWorldGenTolInGaurhothTower.class, "TolInGaurhothTower", 0x515647, 0x2c3b2d);
        
        registerStructure(2029, LOTRWorldGenHithlumBrewery.class, "HithlumBrewery", 0xc5c4c4, 0x27295e);
        registerStructure(2030, LOTRWorldGenHithlumMerchantStall.class, "HithlumMerchantStall", 0xc5c4c4, 0x27295e);
        registerStructure(2031, LOTRWorldGenHithlumHouse.class, "HithlumHouse", 0xc5c4c4, 0x27295e);
        
        registerStructure(2032, LOTRWorldGenGondolinTower.class, "GondolinTower", 0x75a4ce, 0xcb9a15);
        
        //registerStructure(2034, LOTRWorldGenHouseBorTavern.class, "HouseBorTavern", 0xCE4942, 0x1B1919);
        registerStructure(2035, LOTRWorldGenGardhBorenWatchtower.class, "GardhBorenWatchtower", 0x3c2a1e, 0x7f7f7f);
        //registerStructure(2036, LOTRWorldGenGardhBorenFortress.class, "GardhBorenFortress", 0xCE4942, 0x1B1919);
        
        registerStructure(2037, LOTRWorldGenUlfangCampfire.class, "UlfangCampfire", 0x241915, 0x555555);
        registerStructure(2038, LOTRWorldGenUlfangHillFort.class, "UlfangHillFort", 0x241915, 0x555555);
        //registerStructure(2039, LOTRWorldGenUlfangHillmanChieftainHouse.class, "UlfangHillmanChieftainHouse", 0xCE4942, 0x1B1919);
        registerStructure(2040, LOTRWorldGenUlfangHouse.class, "UlfangHouse", 0x241915, 0x555555);
        //registerStructure(2041, LOTRWorldGenUlfangHillmanVillage.class, "UlfangHillmanVillage", 0xCE4942, 0x1B1919);
        registerStructure(2042, LOTRWorldGenUlfangMerchantStall.class, "UlfangMerchantStall", 0x241915, 0x555555);
        registerStructure(2043, LOTRWorldGenUlfangWatchtower.class, "UlfangWatchtower", 0x241915, 0x555555);
        
        registerStructure(2044, LOTRWorldGenBrethilTower.class, "BrethilTower", 0x4f3f32, 0x566c1f);
        //registerStructure(2045, LOTRWorldGenBrethilMerchantStall.class, "BrethilMerchantStall", 0xCE4942, 0x1B1919);
        //registerStructure(2046, LOTRWorldGenBrethilHouse.class, "BrethilHouse", 0xCE4942, 0x1B1919);

        registerStructure(2047, LOTRWorldGenLaegrimHouse.class, "LaegrimHouse", 0x4f5328, 0x524733);
        registerStructure(2048, LOTRWorldGenLaegrimForge.class, "LaegrimForge", 0x4f5328, 0x524733);
        
        registerStructure(2050, LOTRWorldGenPettyDwarfCamp.class, "PettyDwarfCamp", 0x545454, 0x3b3535);
        registerStructure(2051, LOTRWorldGenDorLominCamp.class, "DorLominCamp", 0x2f313e, 0xa29d9d);
        //registerStructure(2052, LOTRWorldGenFalathrimCamp.class, "FalathrimCamp", 0xCE4942, 0x1B1919);
        registerStructure(2053, LOTRWorldGenGardhBorenCamp.class, "GardhBorenCamp", 0x3c2a1e, 0x7f7f7f);
        //registerStructure(2054, LOTRWorldGenNargothrondCamp.class, "NargothrondCamp", 0xCE4942, 0x1B1919);
        registerStructure(2055, LOTRWorldGenDoriathCamp.class, "DoriathCamp", 0xd6e8d9, 0xbfbdb8);
        // end list

        registerStructure(2056, LOTRFAWorldGenTestHouse.class, "TestHouse", 0x00FF00, 0xFF0000);
    }

    public static class StructureInfo {
        public final int spawnedID;
        public final int primaryColor;
        public final int secondaryColor;

        public StructureInfo(int ID, int primaryColor, int secondaryColor) {
            spawnedID = ID;
            this.primaryColor = primaryColor;
            this.secondaryColor = secondaryColor;  
        }
    }
}