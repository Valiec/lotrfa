package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import lotr.common.LOTRMod;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.world.structure2.LOTRWorldGenCampBase;
import lotr.common.world.structure2.LOTRWorldGenStructureBase2;
import net.minecraft.world.World;

public class LOTRWorldGenDDCamp extends LOTRWorldGenCampBase {

    public LOTRWorldGenDDCamp(boolean flag) {
        super(flag);

    }
    
    @Override
    protected LOTRWorldGenStructureBase2 createTent(final boolean flag, final Random random) {
        if (random.nextInt(6) == 0) {
            return new LOTRWorldGenDDForgeTent(false);
        }
        return new LOTRWorldGenDDTent(false);
    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        this.tableBlock = LOTRFABlocks.craftingTableAngband;
        this.brickBlock = LOTRMod.brick;
        this.brickMeta = 0;
        this.brickSlabBlock = LOTRMod.slabSingle;
        this.brickSlabMeta = 1;
        this.fenceBlock = LOTRMod.fence;
        this.fenceMeta = 3;
        this.fenceGateBlock = LOTRMod.fenceGateCharred;
        this.farmBaseBlock = LOTRMod.rock;
        this.farmBaseMeta = 0;
        this.farmCropBlock = LOTRMod.morgulShroom;
        this.farmCropMeta = 0;
        this.hasOrcTorches = true;
        this.hasSkulls = true;
    }

    @Override
    protected LOTREntityNPC getCampCaptain(World world, Random random) {
        switch (random.nextInt(3)) {
            case 0:
                return new LOTREntityDorDaedelothTrader(world);
            case 1:
                return new LOTREntityDorDaedelothHuntsman(world);
            case 2:
                return new LOTREntityDorDaedelothScavenger(world);
        }
        return null;
    }

    @Override
    protected void placeNPCRespawner(final World world, final Random random, final int i, final int j, final int k) {
        final LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClasses(LOTREntityDorDaedelothOrc.class, LOTREntityDorDaedelothOrcArcher.class);
        respawner.setCheckRanges(24, -12, 12, 12);
        respawner.setSpawnRanges(8, -4, 4, 16);
        this.placeNPCRespawner(respawner, world, i, j, k);
    }

}
