package eoa.lotrfa.common.world.structure.nature;

import java.util.Random;
import lotr.common.LOTRMod;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class LOTRWorldGenAngbandBoulder extends WorldGenerator {
    private Block id;
    private int meta;
    private int minWidth;
    private int maxWidth;
    private int heightCheck = 3;

    public LOTRWorldGenAngbandBoulder(Block i, int j, int k, int l) {
        super(false);
        this.id = i;
        this.meta = j;
        this.minWidth = k;
        this.maxWidth = l;
    }

    public LOTRWorldGenAngbandBoulder setHeightCheck(int i) {
        this.heightCheck = i;
        return this;
    }

    @Override
    public boolean generate(World world, Random random, int i, int j, int k) {
        // System.out.println("BOULDER");
        world.getBiomeGenForCoords(i, k);
        int j0 = world.getHeightValue(i, k) - 1;
        Block block = world.getBlock(i, j0, k);
        int meta = world.getBlockMetadata(i, j0, k);
        if (block != LOTRMod.mordorDirt && block != LOTRMod.mordorGravel && (block != LOTRMod.rock || meta != 0) && block != Blocks.stone && block != Blocks.gravel && block != Blocks.dirt) {
            // System.out.println(block.getUnlocalizedName());
            return false;
        }
        else {
            int boulderWidth = MathHelper.getRandomIntegerInRange(random, this.minWidth, this.maxWidth);
            int highestHeight = j;
            int lowestHeight = j;

            int spheres;
            int l;
            int posX;
            for (spheres = i - boulderWidth; spheres <= i + boulderWidth; ++spheres) {
                for (l = k - boulderWidth; l <= k + boulderWidth; ++l) {
                    posX = world.getHeightValue(spheres, l) - 1;
                    block = world.getBlock(spheres, posX, l);
                    meta = world.getBlockMetadata(spheres, posX, l);
                    if (block != LOTRMod.mordorDirt && block != LOTRMod.mordorGravel && (block != LOTRMod.rock || meta != 0) && block != Blocks.stone && block != Blocks.gravel && block != Blocks.dirt) {
                        // System.out.println(block.getUnlocalizedName());
                        // System.out.println(posX);
                        return false;
                    }

                    if (posX > highestHeight) {
                        highestHeight = posX;
                    }

                    if (posX < lowestHeight) {
                        lowestHeight = posX;
                    }
                }
            }

            if (highestHeight - lowestHeight > this.heightCheck) {
                return false;
            }
            else {
                spheres = 1 + random.nextInt(boulderWidth + 1);

                for (l = 0; l < spheres; ++l) {
                    posX = i + MathHelper.getRandomIntegerInRange(random, -boulderWidth, boulderWidth);
                    int posZ = k + MathHelper.getRandomIntegerInRange(random, -boulderWidth, boulderWidth);
                    int posY = world.getTopSolidOrLiquidBlock(posX, posZ);
                    int sphereWidth = MathHelper.getRandomIntegerInRange(random, this.minWidth, this.maxWidth);

                    for (int i1 = posX - sphereWidth; i1 <= posX + sphereWidth; ++i1) {
                        for (int j1 = posY - sphereWidth; j1 <= posY + sphereWidth; ++j1) {
                            for (int k1 = posZ - sphereWidth; k1 <= posZ + sphereWidth; ++k1) {
                                int i2 = i1 - posX;
                                int j2 = j1 - posY;
                                int k2 = k1 - posZ;
                                int dist = i2 * i2 + j2 * j2 + k2 * k2;
                                if (dist < sphereWidth * sphereWidth || dist < (sphereWidth + 1) * (sphereWidth + 1) && random.nextInt(3) == 0) {
                                    int j3;
                                    for (j3 = j1; j3 >= 0 && !world.getBlock(i1, j3 - 1, k1).isOpaqueCube(); --j3) {

                                    }

                                    this.setBlockAndNotifyAdequately(world, i1, j3, k1, this.id, this.meta);
                                    world.getBlock(i1, j3 - 1, k1).onPlantGrow(world, i1, j3 - 1, k1, i1, j3 - 1, k1);
                                }
                            }
                        }
                    }
                }

                return true;
            }
        }
    }
}
