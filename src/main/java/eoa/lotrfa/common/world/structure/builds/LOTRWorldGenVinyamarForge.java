package eoa.lotrfa.common.world.structure.builds;

import eoa.lotrfa.common.block.LOTRFABlocks;
import lotr.common.LOTRMod;
import lotr.common.world.structure2.LOTRWorldGenRivendellForge;
import net.minecraft.entity.EntityCreature;
import net.minecraft.world.World;

public class LOTRWorldGenVinyamarForge extends LOTRWorldGenRivendellForge {

    public LOTRWorldGenVinyamarForge(boolean flag) {
        super(flag);
        this.ruined = true;
        this.tableBlock = LOTRFABlocks.craftingTableGondolin;
        this.roofMeta = 0;
        this.roofStairBlock = LOTRMod.stairsClayTileDyedWhite;
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void spawnNPCAndSetHome(EntityCreature entity, World world, int i, int j, int k, int homeDistance) {
        return;
    }

}
