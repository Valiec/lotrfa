package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.world.structure2.LOTRWorldGenTentBase;
import net.minecraft.init.Blocks;

public class LOTRWorldGenDorLominTent extends LOTRWorldGenTentBase{
	
    public LOTRWorldGenDorLominTent(final boolean flag) {
        super(flag);
    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        final int randomWool = random.nextInt(3);
        if (randomWool == 0) {
            this.tentBlock = Blocks.wool;
            this.tentMeta = 11;
        }
        else if (randomWool == 1) {
            this.tentBlock = Blocks.wool;
            this.tentMeta = 8;
        }
        else if (randomWool == 2) {
            this.tentBlock = Blocks.wool;
            this.tentMeta = 7;
        }
        this.fenceBlock = Blocks.fence;
        this.fenceMeta = 0;
        this.chestContents = LOTRFAChestContents.dor_lomin_house;
    }

}
