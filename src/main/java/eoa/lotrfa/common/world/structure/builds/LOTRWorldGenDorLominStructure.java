package eoa.lotrfa.common.world.structure.builds; 
import java.util.Random;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.item.LOTRItemBanner;
import lotr.common.world.structure.LOTRChestContents;
import lotr.common.world.structure2.LOTRWorldGenStructureBase2;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public abstract class LOTRWorldGenDorLominStructure extends LOTRWorldGenStructureBase2
{
    protected Block rockBlock;
    protected int rockMeta;
    protected Block rockSlabBlock;
    protected int rockSlabMeta;
    protected Block rockSlabDoubleBlock;
    protected int rockSlabDoubleMeta;
    protected Block rockStairBlock;
    protected Block rockWallBlock;
    protected int rockWallMeta;
    protected Block brickBlock;
    protected int brickMeta;
    protected Block brickSlabBlock;
    protected int brickSlabMeta;
    protected Block brickStairBlock;
    protected Block brickWallBlock;
    protected int brickWallMeta;
    protected Block brickMossyBlock;
    protected int brickMossyMeta;
    protected Block brickMossySlabBlock;
    protected int brickMossySlabMeta;
    protected Block brickMossyStairBlock;
    protected Block brickMossyWallBlock;
    protected int brickMossyWallMeta;
    protected Block brickCrackedBlock;
    protected int brickCrackedMeta;
    protected Block brickCrackedSlabBlock;
    protected int brickCrackedSlabMeta;
    protected Block brickCrackedStairBlock;
    protected Block brickCrackedWallBlock;
    protected int brickCrackedWallMeta;
    protected Block pillarBlock;
    protected int pillarMeta;
    protected Block brick2Block;
    protected int brick2Meta;
    protected Block brick2SlabBlock;
    protected int brick2SlabMeta;
    protected Block brick2StairBlock;
    protected Block brick2WallBlock;
    protected int brick2WallMeta;
    protected Block pillar2Block;
    protected int pillar2Meta;
    protected Block cobbleBlock;
    protected int cobbleMeta;
    protected Block cobbleSlabBlock;
    protected int cobbleSlabMeta;
    protected Block cobbleStairBlock;
    protected Block plankBlock;
    protected int plankMeta;
    protected Block plankSlabBlock;
    protected int plankSlabMeta;
    protected Block plankStairBlock;
    protected Block fenceBlock;
    protected int fenceMeta;
    protected Block fenceGateBlock;
    protected Block woodBeamBlock;
    protected int woodBeamMeta;
    protected Block doorBlock;
    protected Block wallBlock;
    protected int wallMeta;
    protected Block roofBlock;
    protected int roofMeta;
    protected Block roofSlabBlock;
    protected int roofSlabMeta;
    protected Block roofStairBlock;
    protected Block barsBlock;
    protected Block tableBlock;
    protected Block bedBlock;
    protected Block gateBlock;
    protected Block plateBlock;
    protected Block cropBlock;
    protected int cropMeta;
    protected Item seedItem;
    protected LOTRItemBanner.BannerType bannerType;
    protected LOTRItemBanner.BannerType bannerType2;
    protected LOTRChestContents chestContents;
    
    public LOTRWorldGenDorLominStructure(final boolean flag) {
        super(flag);
    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        this.rockBlock = Blocks.stone;
        this.rockMeta = 0;
        this.rockSlabBlock = Blocks.stone_slab;
        this.rockSlabMeta = 0;
        this.rockSlabDoubleBlock = Blocks.double_stone_slab;
        this.rockSlabDoubleMeta = 0;
        this.rockStairBlock = Blocks.stone_stairs;
        this.rockWallBlock = Blocks.cobblestone_wall;
        this.rockWallMeta = 0;
        this.brickBlock = LOTRMod.brick5;
        this.brickMeta = 1;
        this.brickSlabBlock = LOTRMod.slabSingle9;
        this.brickSlabMeta = 6;
        this.brickStairBlock = LOTRMod.stairsDaleBrick;
        this.brickWallBlock = LOTRMod.wall3;
        this.brickWallMeta = 9;
        //
        this.brickMossyBlock = LOTRMod.brick;
        this.brickMossyMeta = 2;
        this.brickMossySlabBlock = LOTRMod.slabSingle;
        this.brickMossySlabMeta = 4;
        this.brickMossyStairBlock = LOTRMod.stairsGondorBrickMossy;
        this.brickMossyWallBlock = LOTRMod.wall;
        this.brickMossyWallMeta = 4;
        this.brickCrackedBlock = LOTRMod.brick;
        this.brickCrackedMeta = 3;
        this.brickCrackedSlabBlock = LOTRMod.slabSingle;
        this.brickCrackedSlabMeta = 5;
        this.brickCrackedStairBlock = LOTRMod.stairsGondorBrickCracked;
        this.brickCrackedWallBlock = LOTRMod.wall;
        this.brickCrackedWallMeta = 5;
        //
        this.pillarBlock = LOTRMod.pillar2;
        this.pillarMeta = 5;
        //
        this.brick2Block = LOTRMod.whiteSandstone;
        this.brick2Meta = 0;
        this.brick2SlabBlock = LOTRMod.slabSingle10;
        this.brick2SlabMeta = 6;
        this.brick2StairBlock = LOTRMod.stairsWhiteSandstone;
        this.brick2WallBlock = LOTRMod.wall3;
        this.brick2WallMeta = 14;
        
        this.pillar2Block = LOTRMod.pillar;
        this.pillar2Meta = 9;
        
        this.cobbleBlock = Blocks.cobblestone;
        this.cobbleMeta = 0;
        this.cobbleSlabBlock = Blocks.stone_slab;
        this.cobbleSlabMeta = 3;
        this.cobbleStairBlock = Blocks.stone_stairs;
        if (random.nextBoolean()) {
            this.wallBlock = LOTRMod.daub;
            this.wallMeta = 0;
        }
        else {
            this.wallBlock = this.plankBlock;
            this.wallMeta = this.plankMeta;
        }
        this.roofBlock = LOTRMod.thatch;
        this.roofMeta = 0;
        this.roofSlabBlock = LOTRMod.slabSingleThatch;
        this.roofSlabMeta = 0;
        this.roofStairBlock = LOTRMod.stairsThatch;
        this.barsBlock = Blocks.iron_bars;
        this.tableBlock = LOTRFABlocks.craftingTableDorLomin;
        this.bedBlock = LOTRMod.strawBed;
        this.gateBlock = LOTRMod.gateWooden;
        this.plateBlock = LOTRMod.ceramicPlateBlock;
        if (random.nextBoolean()) {
            this.cropBlock = Blocks.wheat;
            this.cropMeta = 7;
            this.seedItem = Items.wheat_seeds;
        }
        else {
            final int randomCrop = random.nextInt(6);
            if (randomCrop == 0) {
                this.cropBlock = Blocks.carrots;
                this.cropMeta = 7;
                this.seedItem = Items.carrot;
            }
            else if (randomCrop == 1) {
                this.cropBlock = Blocks.potatoes;
                this.cropMeta = 7;
                this.seedItem = Items.potato;
            }
            else if (randomCrop == 2) {
                this.cropBlock = LOTRMod.lettuceCrop;
                this.cropMeta = 7;
                this.seedItem = LOTRMod.lettuce;
            }
            else if (randomCrop == 3) {
                this.cropBlock = LOTRMod.cornStalk;
                this.cropMeta = 0;
                this.seedItem = Item.getItemFromBlock(LOTRMod.cornStalk);
            }
            else if (randomCrop == 4) {
                this.cropBlock = LOTRMod.leekCrop;
                this.cropMeta = 7;
                this.seedItem = LOTRMod.leek;
            }
            else if (randomCrop == 5) {
                this.cropBlock = LOTRMod.turnipCrop;
                this.cropMeta = 7;
                this.seedItem = LOTRMod.turnip;
            }
        }
        this.bannerType2 = LOTRFAItemBanner.FABannerTypes.dorLomin;
        this.chestContents = LOTRChestContents.GONDOR_HOUSE;
    }
    
    protected ItemStack getGondorFramedItem(final Random random) {
        final ItemStack[] items = { new ItemStack(LOTRMod.helmetPinnathGelin), new ItemStack(LOTRMod.bodyPinnathGelin), new ItemStack(LOTRFAItems.dorLominDagger), new ItemStack(LOTRFAItems.dorLominSpear), new ItemStack(LOTRFAItems.dorLominBow), new ItemStack(Items.arrow), new ItemStack(Items.iron_sword), new ItemStack(Items.iron_axe), new ItemStack(LOTRMod.daggerIron), new ItemStack(LOTRMod.pikeIron), new ItemStack(LOTRMod.ironCrossbow), new ItemStack(LOTRMod.goldRing), new ItemStack(LOTRMod.silverRing) };
        return items[random.nextInt(items.length)].copy();
    }
    
    public enum DorLominFiefdom
    {
        DOR_LOMIN;
        
        public LOTREntityDorLominMan createCaptain(final World world) {
        	return new LOTREntityDorLominCaptain(world);
        }
        
        public Class[] getSoldierClasses() {
        	return new Class[] { LOTREntityDorLominSoldier.class, LOTREntityDorLominArcher.class };
        }
        
        public Class[] getLevyClasses() {
        	return new Class[] { LOTREntityDorLominLevyman.class, LOTREntityDorLominLevyman.class };
        }
        
        protected ItemStack[] getFiefdomArmor() {
        	return new ItemStack[] { new ItemStack(LOTRMod.helmetPinnathGelin), new ItemStack(LOTRMod.bodyPinnathGelin), new ItemStack(LOTRMod.legsPinnathGelin), new ItemStack(LOTRMod.bootsPinnathGelin) };
        }
    }
}
