package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.LOTREntityBorSoldier;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRMod;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.world.structure2.LOTRWorldGenStructureBase2;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRWorldGenGardhBorenWatchtower extends LOTRWorldGenStructureBase2{

    private Block woodBlock;
    private int woodMeta;
    private Block plankBlock;
    private int plankMeta;
    private Block fenceBlock;
    private int fenceMeta;
    private Block stairBlock;
    
    public LOTRWorldGenGardhBorenWatchtower(final boolean flag) {
        super(flag);
    }
    
    @Override
    public boolean generateWithSetRotation(final World world, final Random random, final int i, final int j, final int k, final int rotation) {
        this.setOriginAndRotation(world, i, j, k, rotation, 0);
        if (this.restrictions) {
            for (int i2 = -4; i2 <= 4; ++i2) {
                for (int k2 = -4; k2 <= 4; ++k2) {
                    final int j2 = this.getTopBlock(world, i2, k2);
                    final Block block = this.getBlock(world, i2, j2 - 1, k2);
                    if (block != Blocks.grass) {
                        return false;
                    }
                }
            }
        }
        final int randomWood = random.nextInt(4);
        if (randomWood == 0) {
            this.woodBlock = Blocks.log;
            this.woodMeta = 0;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 0;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 0;
            this.stairBlock = Blocks.oak_stairs;
        }
        else if (randomWood == 1) {
            this.woodBlock = Blocks.log;
            this.woodMeta = 1;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 1;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 0;
            this.stairBlock = Blocks.spruce_stairs;
        }
        else if (randomWood == 2) {
            this.woodBlock = Blocks.log;
            this.woodMeta = 5;
            this.plankBlock = Blocks.planks;
            this.plankMeta = 5;
            this.fenceBlock = Blocks.fence;
            this.fenceMeta = 5;
            this.stairBlock = Blocks.dark_oak_stairs;
        }
        else if (randomWood == 3) {
            this.woodBlock = LOTRMod.wood4;
            this.woodMeta = 3;
            this.plankBlock = LOTRMod.planks2;
            this.plankMeta = 3;
            this.fenceBlock = LOTRMod.fence2;
            this.fenceMeta = 3;
            this.stairBlock = LOTRMod.stairsFir;
        }
        this.generateSupportPillar(world, -3, 4, -3);
        this.generateSupportPillar(world, -3, 4, 3);
        this.generateSupportPillar(world, 3, 4, -3);
        this.generateSupportPillar(world, 3, 4, 3);
        for (int i3 = -2; i3 <= 2; ++i3) {
            for (int k3 = -2; k3 <= 2; ++k3) {
                for (int j3 = 5; j3 <= 19; ++j3) {
                    this.setAir(world, i3, j3, k3);
                }
            }
        }
        for (int j4 = 6; j4 <= 19; ++j4) {
            this.setBlockAndMetadata(world, -2, j4, -2, this.woodBlock, this.woodMeta);
            this.setBlockAndMetadata(world, -2, j4, 2, this.woodBlock, this.woodMeta);
            this.setBlockAndMetadata(world, 2, j4, -2, this.woodBlock, this.woodMeta);
            this.setBlockAndMetadata(world, 2, j4, 2, this.woodBlock, this.woodMeta);
        }
        for (int j4 = 5; j4 <= 10; j4 += 5) {
            for (int i4 = -3; i4 <= 3; ++i4) {
                for (int k4 = -3; k4 <= 3; ++k4) {
                    this.setBlockAndMetadata(world, i4, j4, k4, this.plankBlock, this.plankMeta);
                }
            }
            for (int i4 = -4; i4 <= 4; ++i4) {
                this.setBlockAndMetadata(world, i4, j4, -4, this.stairBlock, 2);
                this.setBlockAndMetadata(world, i4, j4, 4, this.stairBlock, 3);
            }
            for (int k3 = -3; k3 <= 3; ++k3) {
                this.setBlockAndMetadata(world, -4, j4, k3, this.stairBlock, 1);
                this.setBlockAndMetadata(world, 4, j4, k3, this.stairBlock, 0);
            }
            for (int i4 = -2; i4 <= 2; ++i4) {
                this.setBlockAndMetadata(world, i4, j4 + 1, -3, this.fenceBlock, this.fenceMeta);
                this.setBlockAndMetadata(world, i4, j4 + 1, 3, this.fenceBlock, this.fenceMeta);
            }
            for (int k3 = -2; k3 <= 2; ++k3) {
                this.setBlockAndMetadata(world, -3, j4 + 1, k3, this.fenceBlock, this.fenceMeta);
                this.setBlockAndMetadata(world, 3, j4 + 1, k3, this.fenceBlock, this.fenceMeta);
            }
            this.setBlockAndMetadata(world, 0, j4 + 2, -3, Blocks.torch, 5);
            this.setBlockAndMetadata(world, 0, j4 + 2, 3, Blocks.torch, 5);
            this.setBlockAndMetadata(world, -3, j4 + 2, 0, Blocks.torch, 5);
            this.setBlockAndMetadata(world, 3, j4 + 2, 0, Blocks.torch, 5);
            LOTREntityBorSoldier soldier = new LOTREntityBorSoldier(world);
            soldier.spawnRidingHorse = false;
            this.spawnNPCAndSetHome(soldier, world, -1, j4 + 1, 0, 8);
        }
        for (int i3 = -2; i3 <= 2; ++i3) {
            for (int k3 = -2; k3 <= 2; ++k3) {
                final int i5 = Math.abs(i3);
                final int k5 = Math.abs(k3);
                if (i5 < 2 || k5 < 2) {
                    this.setBlockAndMetadata(world, i3, 15, k3, this.plankBlock, this.plankMeta);
                    if ((i5 < 2 && k5 == 2) || (i5 == 2 && k5 < 2)) {
                        this.setBlockAndMetadata(world, i3, 16, k3, this.fenceBlock, this.fenceMeta);
                    }
                }
            }
        }
        this.setGrassToDirt(world, 0, 0, 0);
        for (int j4 = 1; j4 <= 25; ++j4) {
            this.setBlockAndMetadata(world, 0, j4, 0, this.woodBlock, this.woodMeta);
            if (j4 <= 15) {
                this.setBlockAndMetadata(world, 0, j4, -1, Blocks.ladder, 2);
            }
        }
        this.setBlockAndMetadata(world, 0, 6, -1, Blocks.trapdoor, 0);
        this.setBlockAndMetadata(world, 0, 11, -1, Blocks.trapdoor, 0);
        this.setBlockAndMetadata(world, 0, 17, -2, Blocks.torch, 5);
        this.setBlockAndMetadata(world, 0, 17, 2, Blocks.torch, 5);
        this.setBlockAndMetadata(world, -2, 17, 0, Blocks.torch, 5);
        this.setBlockAndMetadata(world, 2, 17, 0, Blocks.torch, 5);
        this.placeChest(world, random, 0, 16, 1, 0, LOTRFAChestContents.house_bor_house);
        this.setBlockAndMetadata(world, 0, 11, 1, LOTRFABlocks.craftingTableHouseBor, 0);
        for (int j4 = 17; j4 <= 18; ++j4) {
            this.setBlockAndMetadata(world, -2, j4, -2, this.fenceBlock, this.fenceMeta);
            this.setBlockAndMetadata(world, -2, j4, 2, this.fenceBlock, this.fenceMeta);
            this.setBlockAndMetadata(world, 2, j4, -2, this.fenceBlock, this.fenceMeta);
            this.setBlockAndMetadata(world, 2, j4, 2, this.fenceBlock, this.fenceMeta);
        }
        for (int step = 0; step <= 1; ++step) {
            for (int i4 = -2 + step; i4 <= 2 - step; ++i4) {
                this.setBlockAndMetadata(world, i4, 20 + step, -2 + step, this.stairBlock, 2);
                this.setBlockAndMetadata(world, i4, 20 + step, 2 - step, this.stairBlock, 3);
            }
            for (int k3 = -1 + step; k3 <= 1 - step; ++k3) {
                this.setBlockAndMetadata(world, -2 + step, 20 + step, k3, this.stairBlock, 1);
                this.setBlockAndMetadata(world, 2 - step, 20 + step, k3, this.stairBlock, 0);
            }
        }
        this.placeWallBanner(world, -2, 15, 0, LOTRFAItemBanner.FABannerTypes.houseBor, 3);
        this.placeWallBanner(world, 2, 15, 0, LOTRFAItemBanner.FABannerTypes.houseBor, 1);
        this.placeWallBanner(world, 0, 15, -2, LOTRFAItemBanner.FABannerTypes.houseBor, 2);
        this.placeWallBanner(world, 0, 15, 2, LOTRFAItemBanner.FABannerTypes.houseBor, 0);
        for (int j4 = 24; j4 <= 25; ++j4) {
            this.setBlockAndMetadata(world, 1, j4, 0, Blocks.wool, 11);
            this.setBlockAndMetadata(world, 2, j4, 1, Blocks.wool, 11);
            this.setBlockAndMetadata(world, 2, j4, 2, Blocks.wool, 11);
            this.setBlockAndMetadata(world, 3, j4, 3, Blocks.wool, 11);
        }
        final LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClass(LOTREntityBorSoldier.class);
        respawner.setCheckRanges(24, -12, 20, 8);
        respawner.setSpawnRanges(4, -4, 4, 16);
        this.placeNPCRespawner(respawner, world, 0, 0, 0);
        return true;
    }
    
    private void generateSupportPillar(final World world, final int i, final int j, final int k) {
        for (int j2 = j; !this.isOpaque(world, i, j2, k) && this.getY(j2) >= 0; --j2) {
            this.setBlockAndMetadata(world, i, j2, k, this.woodBlock, this.woodMeta);
            this.setGrassToDirt(world, i, j2 - 1, i);
        }
    }

}
