package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRMod;
import lotr.common.world.biome.LOTRBiomeGenMordor;
import lotr.common.world.structure2.LOTRWorldGenTentBase;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;

public class LOTRWorldGenAngbandTent extends LOTRWorldGenTentBase {
    public LOTRWorldGenAngbandTent(boolean flag) {
        super(flag);
    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        final int randomWool = random.nextInt(3);
        if (randomWool == 0) {
            this.tentBlock = Blocks.wool;
            this.tentMeta = 15;
        }
        else if (randomWool == 1) {
            this.tentBlock = Blocks.wool;
            this.tentMeta = 12;
        }
        else if (randomWool == 2) {
            this.tentBlock = Blocks.wool;
            this.tentMeta = 7;
        }
        this.fenceBlock = LOTRMod.fence;
        this.fenceMeta = 3;
        this.chestContents = LOTRFAChestContents.angband_tent;
        this.hasOrcTorches = true;
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        BiomeGenBase biome;
        int j1;
        int k1;
        int i1;
        this.setOriginAndRotation(world, i, j, k, rotation, 4);
        this.setupRandomBlocks(random);
        if (this.restrictions) {
            System.out.println("ATTEMPT");
            for (i1 = -2; i1 <= 2; ++i1) {
                for (k1 = -3; k1 <= 3; ++k1) {
                    j1 = this.getTopBlock(world, i1, k1) - 1;
                    Block block = this.getBlock(world, i1, j1, k1);
                    if (this.isSurface(world, i1, j1, k1) || block == Blocks.dirt || block == Blocks.gravel || block == Blocks.stone || block == LOTRMod.mordorDirt || block == LOTRMod.mordorGravel) continue;
                    System.out.println(block.getUnlocalizedName());
                    System.out.print(i1);
                    System.out.print(", ");
                    System.out.print(j1);
                    System.out.print(", ");
                    System.out.println(k1);
                    return false;
                }
            }
        }
        for (i1 = -2; i1 <= 2; ++i1) {
            for (k1 = -3; k1 <= 3; ++k1) {
                for (j1 = 0; !(j1 < 0 && this.isOpaque(world, i1, j1, k1) || this.getY(j1) < 0); --j1) {
                    int randomGround;
                    biome = this.getBiome(world, i1, k1);
                    if (biome instanceof LOTRBiomeGenMordor) {
                        randomGround = random.nextInt(3);
                        if (randomGround == 0) {
                            this.setBlockAndMetadata(world, i1, j1, k1, LOTRMod.rock, 0);
                        }
                        else if (randomGround == 1) {
                            this.setBlockAndMetadata(world, i1, j1, k1, LOTRMod.mordorDirt, 0);
                        }
                        else if (randomGround == 2) {
                            this.setBlockAndMetadata(world, i1, j1, k1, LOTRMod.mordorGravel, 0);
                        }
                    }
                    else {
                        randomGround = random.nextInt(3);
                        if (randomGround == 0) {
                            if (j1 == 0) {
                                this.setBiomeTop(world, i1, j1, k1);
                            }
                            else {
                                this.setBiomeFiller(world, i1, j1, k1);
                            }
                        }
                        else if (randomGround == 1) {
                            this.setBlockAndMetadata(world, i1, j1, k1, LOTRMod.mordorDirt, 0);
                        }
                        else if (randomGround == 2) {
                            this.setBlockAndMetadata(world, i1, j1, k1, LOTRMod.mordorGravel, 0);
                        }
                    }
                    this.setGrassToDirt(world, i1, j1 - 1, k1);
                }
                for (j1 = 1; j1 <= 3; ++j1) {
                    this.setAir(world, i1, j1, k1);
                }
            }
        }
        for (int k12 = -3; k12 <= 3; ++k12) {
            int[] k13 = new int[] {-2, 2};
            j1 = k13.length;
            for (int biome_ = 0; biome_ < j1; ++biome_) {
                int i12 = k13[biome_];
                for (int j12 = 1; j12 <= 2; ++j12) {
                    this.setBlockAndMetadata(world, i12, j12, k12, this.tentBlock, this.tentMeta);
                }
                this.setGrassToDirt(world, i12, 0, k12);
            }
            this.setBlockAndMetadata(world, -1, 3, k12, this.tentBlock, this.tentMeta);
            this.setBlockAndMetadata(world, 1, 3, k12, this.tentBlock, this.tentMeta);
            this.setBlockAndMetadata(world, 0, 4, k12, this.tentBlock, this.tentMeta);
        }
        for (int j13 = 1; j13 <= 3; ++j13) {
            this.setBlockAndMetadata(world, 0, j13, -3, this.fenceBlock, this.fenceMeta);
            this.setBlockAndMetadata(world, 0, j13, 3, this.fenceBlock, this.fenceMeta);
        }
        if (this.hasOrcTorches) {
            this.placeOrcTorch(world, -1, 1, -3);
            this.placeOrcTorch(world, 1, 1, -3);
            this.placeOrcTorch(world, -1, 1, 3);
            this.placeOrcTorch(world, 1, 1, 3);
        }
        else {
            this.setBlockAndMetadata(world, -1, 2, -3, Blocks.torch, 2);
            this.setBlockAndMetadata(world, 1, 2, -3, Blocks.torch, 1);
            this.setBlockAndMetadata(world, -1, 2, 3, Blocks.torch, 2);
            this.setBlockAndMetadata(world, 1, 2, 3, Blocks.torch, 1);
        }
        if (random.nextBoolean()) {
            if (this.hasOrcForge) {
                this.setBlockAndMetadata(world, -1, 1, 0, LOTRMod.orcForge, 4);
                this.setGrassToDirt(world, -1, 0, 0);
                this.setBlockAndMetadata(world, -1, 1, -1, this.fenceBlock, this.fenceMeta);
                this.setBlockAndMetadata(world, -1, 1, 1, this.fenceBlock, this.fenceMeta);
            }
            else {
                this.placeChest(world, random, -1, 1, 0, 4, this.chestContents);
            }
        }
        else if (this.hasOrcForge) {
            this.setBlockAndMetadata(world, 1, 1, 0, LOTRMod.orcForge, 5);
            this.setGrassToDirt(world, 1, 0, 0);
            this.setBlockAndMetadata(world, 1, 1, -1, this.fenceBlock, this.fenceMeta);
            this.setBlockAndMetadata(world, 1, 1, 1, this.fenceBlock, this.fenceMeta);
        }
        else {
            this.placeChest(world, random, 1, 1, 0, 5, this.chestContents);
        }
        return true;
    }
}
