package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRFoods;
import lotr.common.LOTRMod;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.world.structure.LOTRChestContents;
import lotr.common.world.structure.LOTRWorldGenMordorTower;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class LOTRWorldGenBrethilTower extends LOTRWorldGenMordorTower {
    public LOTRWorldGenBrethilTower(boolean b) {
        super(b);
    }

    @Override
    public boolean generate(World world, Random random, int i, int j, int k) {
        int i1;
        int k1;
        int j1;
        int k12;
        int j12;

        --j;
        int rotation = random.nextInt(4);
        if (!this.restrictions && this.usingPlayer != null) {
            rotation = this.usingPlayerRotation();
        }
        switch (rotation) {
            case 0: {
                k += 7;
                break;
            }
            case 1: {
                i -= 7;
                break;
            }
            case 2: {
                k -= 7;
                break;
            }
            case 3: {
                i += 7;
            }
        }
        int sections = 2 + random.nextInt(3);
        int equipmentSection = 1 + random.nextInt(sections);
        if (this.restrictions) {
            for (int i12 = i - 7; i12 <= i + 7; ++i12) {
                for (k12 = k - 7; k12 <= k + 7; ++k12) {
                    j1 = world.getHeightValue(i12, k12) - 1;
                    Block block = world.getBlock(i12, j1, k12);
                    if (block != Blocks.water && block != LOTRMod.thatch && block != LOTRMod.brick2 && block != Blocks.stonebrick) continue;
                    return false;
                }
            }
        }
        for (k1 = k - 2; k1 <= k + 2; ++k1) {
            for (j12 = j; !LOTRMod.isOpaque(world, i - 6, j12, k1) && j12 >= 0; --j12) {
                this.setBlockAndNotifyAdequately(world, i - 6, j12, k1, LOTRMod.brick2, 3); // a
            }
            for (j12 = j; !LOTRMod.isOpaque(world, i + 6, j12, k1) && j12 >= 0; --j12) {
                this.setBlockAndNotifyAdequately(world, i + 6, j12, k1, LOTRMod.brick2, 3); // b
            }
        }
        for (k1 = k - 4; k1 <= k + 4; ++k1) {
            for (j12 = j; !LOTRMod.isOpaque(world, i - 5, j12, k1) && j12 >= 0; --j12) {
                this.setBlockAndNotifyAdequately(world, i - 5, j12, k1, LOTRMod.brick2, 3); // c
            }
            for (j12 = j; !LOTRMod.isOpaque(world, i + 5, j12, k1) && j12 >= 0; --j12) {
                this.setBlockAndNotifyAdequately(world, i + 5, j12, k1, LOTRMod.brick2, 3); // d
            }
        }
        for (k1 = k - 5; k1 <= k + 5; ++k1) {
            for (i1 = i - 4; i1 <= i - 3; ++i1) {
                for (j1 = j; !LOTRMod.isOpaque(world, i1, j1, k1) && j1 >= 0; --j1) {
                    this.setBlockAndNotifyAdequately(world, i1, j1, k1, LOTRMod.brick2, 3); // e
                }
            }
            for (i1 = i + 3; i1 <= i + 4; ++i1) {
                for (j1 = j; !LOTRMod.isOpaque(world, i1, j1, k1) && j1 >= 0; --j1) {
                    this.setBlockAndNotifyAdequately(world, i1, j1, k1, LOTRMod.brick2, 3); // f
                }
            }
        }
        for (k1 = k - 6; k1 <= k + 6; ++k1) {
            for (i1 = i - 2; i1 <= i + 2; ++i1) {
                for (j1 = j; !LOTRMod.isOpaque(world, i1, j1, k1) && j1 >= 0; --j1) {
                    this.setBlockAndNotifyAdequately(world, i1, j1, k1, LOTRMod.brick2, 3); // g
                }
            }
        }
        for (int l = 0; l <= sections; ++l) {
            this.generateTowerSection(world, random, i, j, k, l, false, l == equipmentSection);
        }
        this.generateTowerSection(world, random, i, j, k, sections + 1, true, false);
        LOTREntityBrethilCaptain trader = new LOTREntityBrethilCaptain(world);
        trader.setLocationAndAngles(i + 0.5, j + (sections + 1) * 8 + 1, k - 4 + 0.5, world.rand.nextFloat() * 360.0f, 0.0f);
        trader.onSpawnWithEgg(null);
        trader.setHomeArea(i, j + (sections + 1) * 8, k, 24);
        world.spawnEntityInWorld(trader);
        switch (rotation) {
            case 0: {
                for (j1 = j + 1; j1 <= j + 4; ++j1) {
                    this.setBlockAndNotifyAdequately(world, i - 2, j1, k - 6, LOTRMod.woodBeam2, 1);
                    this.setBlockAndNotifyAdequately(world, i + 2, j1, k - 6, LOTRMod.woodBeam2, 1);
                }
                for (i1 = i - 1; i1 <= i + 1; ++i1) {
                    this.setBlockAndNotifyAdequately(world, i1, j, k - 6, LOTRMod.planks, 9);
                    for (j1 = j + 1; j1 <= j + 4; ++j1) {
                        this.setBlockAndNotifyAdequately(world, i1, j1, k - 6, LOTRMod.gateWooden, 3);
                    }
                    this.setBlockAndNotifyAdequately(world, i1, j + 5, k - 6, LOTRMod.woodBeam2, 5);
                }
                this.placeWallBanner(world, i - 2, j + 4, k - 6, 2, LOTRFAItemBanner.FABannerTypes.brethil);
                this.placeWallBanner(world, i + 2, j + 4, k - 6, 2, LOTRFAItemBanner.FABannerTypes.brethil);
                break;
            }
            case 1: {
                for (j1 = j + 1; j1 <= j + 4; ++j1) {
                    this.setBlockAndNotifyAdequately(world, i + 6, j1, k - 2, LOTRMod.woodBeam2, 1);
                    this.setBlockAndNotifyAdequately(world, i + 6, j1, k + 2, LOTRMod.woodBeam2, 1);
                }
                for (k12 = k - 1; k12 <= k + 1; ++k12) {
                    this.setBlockAndNotifyAdequately(world, i + 6, j, k12, LOTRMod.planks, 9);
                    for (j1 = j + 1; j1 <= j + 4; ++j1) {
                        this.setBlockAndNotifyAdequately(world, i + 6, j1, k12, LOTRMod.gateWooden, 4);
                    }
                    this.setBlockAndNotifyAdequately(world, i + 6, j + 5, k12, LOTRMod.woodBeam2, 9);
                }
                this.placeWallBanner(world, i + 6, j + 4, k - 2, 3, LOTRFAItemBanner.FABannerTypes.brethil);
                this.placeWallBanner(world, i + 6, j + 4, k + 2, 3, LOTRFAItemBanner.FABannerTypes.brethil);
                break;
            }
            case 2: {
                for (j1 = j + 1; j1 <= j + 4; ++j1) {
                    this.setBlockAndNotifyAdequately(world, i - 2, j1, k + 6, LOTRMod.woodBeam2, 1);
                    this.setBlockAndNotifyAdequately(world, i + 2, j1, k + 6, LOTRMod.woodBeam2, 1);
                }
                for (i1 = i - 1; i1 <= i + 1; ++i1) {
                    this.setBlockAndNotifyAdequately(world, i1, j, k + 6, LOTRMod.planks, 9);
                    for (j1 = j + 1; j1 <= j + 4; ++j1) {
                        this.setBlockAndNotifyAdequately(world, i1, j1, k + 6, LOTRMod.gateWooden, 2);
                    }
                    this.setBlockAndNotifyAdequately(world, i1, j + 5, k + 6, LOTRMod.woodBeam2, 5);
                }
                this.placeWallBanner(world, i - 2, j + 4, k + 6, 0, LOTRFAItemBanner.FABannerTypes.brethil);
                this.placeWallBanner(world, i + 2, j + 4, k + 6, 0, LOTRFAItemBanner.FABannerTypes.brethil);
                break;
            }
            case 3: {
                for (j1 = j + 1; j1 <= j + 4; ++j1) {
                    this.setBlockAndNotifyAdequately(world, i - 6, j1, k - 2, LOTRMod.woodBeam2, 1);
                    this.setBlockAndNotifyAdequately(world, i - 6, j1, k + 2, LOTRMod.woodBeam2, 1);
                }
                for (k12 = k - 1; k12 <= k + 1; ++k12) {
                    this.setBlockAndNotifyAdequately(world, i - 6, j, k12, LOTRMod.planks, 9);
                    for (j1 = j + 1; j1 <= j + 4; ++j1) {
                        this.setBlockAndNotifyAdequately(world, i - 6, j1, k12, LOTRMod.gateWooden, 5);
                    }
                    this.setBlockAndNotifyAdequately(world, i - 6, j1, k12, LOTRMod.woodBeam2, 9);
                }
                this.placeWallBanner(world, i - 6, j + 4, k - 2, 1, LOTRFAItemBanner.FABannerTypes.brethil);
                this.placeWallBanner(world, i - 6, j + 4, k + 2, 1, LOTRFAItemBanner.FABannerTypes.brethil);
            }
        }
        LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClasses(LOTREntityBrethilSoldier.class, LOTREntityBrethilArcher.class);
        respawner.setCheckRanges(12, -8, 50, 20);
        respawner.setSpawnRanges(5, 1, 40, 16);
        this.placeNPCRespawner(respawner, world, i, j, k);
        return true;
    }

    private void generateTowerSection(World world, Random random, int i, int j, int k, int section, boolean isTop, boolean isEquipmentSection) {
        int j1;
        int i1;
        for (j1 = section == 0 ? j : (j += section * 8) + 1; j1 <= (isTop ? j + 10 : j + 8); ++j1) {
            int i12;
            int k1;
            Block fillBlock = Blocks.air;
            int fillMeta = 0;
            if (j1 == j) {
                fillBlock = LOTRMod.planks;
                fillMeta = 9;
            }
            else if (j1 == j + 8 && !isTop) {
                fillBlock = LOTRMod.woodSlabSingle2;
                fillMeta = 9;
            }
            else {
                fillBlock = Blocks.air;
                fillMeta = 0;
            }
            for (k1 = k - 2; k1 <= k + 2; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 5, j1, k1, fillBlock, fillMeta);
                this.setBlockAndNotifyAdequately(world, i + 5, j1, k1, fillBlock, fillMeta);
            }
            for (k1 = k - 4; k1 <= k + 4; ++k1) {
                for (i12 = i - 4; i12 <= i - 3; ++i12) {
                    this.setBlockAndNotifyAdequately(world, i12, j1, k1, fillBlock, fillMeta);
                }
                for (i12 = i + 3; i12 <= i + 4; ++i12) {
                    this.setBlockAndNotifyAdequately(world, i12, j1, k1, fillBlock, fillMeta);
                }
            }
            for (k1 = k - 5; k1 <= k + 5; ++k1) {
                for (i12 = i - 2; i12 <= i + 2; ++i12) {
                    this.setBlockAndNotifyAdequately(world, i12, j1, k1, fillBlock, fillMeta);
                }
            }
        }
        for (j1 = j + 1; j1 <= (isTop ? j : j + 8); ++j1) {
            for (int k1 = k - 2; k1 <= k + 2; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 6, j1, k1, LOTRMod.brick2, 3); // ff
                this.setBlockAndNotifyAdequately(world, i + 6, j1, k1, LOTRMod.brick2, 3); // gg
            }
            for (int i13 = i - 2; i13 <= i + 2; ++i13) {
                this.setBlockAndNotifyAdequately(world, i13, j1, k - 6, LOTRMod.brick2, 3); // h
                this.setBlockAndNotifyAdequately(world, i13, j1, k + 6, LOTRMod.brick2, 3); // i
            }
            this.setBlockAndNotifyAdequately(world, i - 5, j1, k - 4, LOTRMod.brick2, 6); // j
            this.setBlockAndNotifyAdequately(world, i - 5, j1, k - 3, LOTRMod.brick2, 3); // k
            this.setBlockAndNotifyAdequately(world, i - 5, j1, k + 3, LOTRMod.brick2, 3); // l
            this.setBlockAndNotifyAdequately(world, i - 5, j1, k + 4, LOTRMod.brick2, 6); // m
            this.setBlockAndNotifyAdequately(world, i - 4, j1, k - 5, LOTRMod.brick2, 6); // n
            this.setBlockAndNotifyAdequately(world, i - 4, j1, k + 5, LOTRMod.brick2, 6); // o
            this.setBlockAndNotifyAdequately(world, i - 3, j1, k - 5, LOTRMod.brick2, 3); // p
            this.setBlockAndNotifyAdequately(world, i - 3, j1, k + 5, LOTRMod.brick2, 3); // q
            this.setBlockAndNotifyAdequately(world, i + 3, j1, k - 5, LOTRMod.brick2, 3); // r
            this.setBlockAndNotifyAdequately(world, i + 3, j1, k + 5, LOTRMod.brick2, 3); // s
            this.setBlockAndNotifyAdequately(world, i + 4, j1, k - 5, LOTRMod.brick2, 6); // t
            this.setBlockAndNotifyAdequately(world, i + 4, j1, k + 5, LOTRMod.brick2, 6); // u
            this.setBlockAndNotifyAdequately(world, i + 5, j1, k - 4, LOTRMod.brick2, 6); // v
            this.setBlockAndNotifyAdequately(world, i + 5, j1, k - 3, LOTRMod.brick2, 3); // w
            this.setBlockAndNotifyAdequately(world, i + 5, j1, k + 3, LOTRMod.brick2, 3); // x
            this.setBlockAndNotifyAdequately(world, i + 5, j1, k + 4, LOTRMod.brick2, 6); // y
        }
        this.placeOrcTorch(world, i - 5, j + 1, k - 2);
        this.placeOrcTorch(world, i - 5, j + 1, k + 2);
        this.placeOrcTorch(world, i + 5, j + 1, k - 2);
        this.placeOrcTorch(world, i + 5, j + 1, k + 2);
        this.placeOrcTorch(world, i - 2, j + 1, k - 5);
        this.placeOrcTorch(world, i + 2, j + 1, k - 5);
        this.placeOrcTorch(world, i - 2, j + 1, k + 5);
        this.placeOrcTorch(world, i + 2, j + 1, k + 5);
        if (!isTop) {
            this.setBlockAndNotifyAdequately(world, i - 6, j + 1, k - 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 6, j + 1, k - 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 1, k - 6, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 1, k + 6, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i - 6, j + 5, k - 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 6, j + 5, k - 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 5, k - 6, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 5, k + 6, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i - 6, j + 1, k - 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 6, j + 1, k + 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 1, k - 6, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 1, k + 6, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i - 6, j + 5, k + 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 6, j + 5, k + 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 5, k - 6, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 5, k + 6, LOTRMod.brick2, 6);
            for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 6, j + 1, k1, LOTRMod.woodBeam2, 9);
                this.setBlockAndNotifyAdequately(world, i + 6, j + 1, k1, LOTRMod.woodBeam2, 9);
            }
            for (int i14 = i - 1; i14 <= i + 1; ++i14) {
                this.setBlockAndNotifyAdequately(world, i14, j + 1, k - 6, LOTRMod.woodBeam2, 5);
                this.setBlockAndNotifyAdequately(world, i14, j + 1, k + 6, LOTRMod.woodBeam2, 5);
            }
            for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 6, j + 5, k1, LOTRMod.woodBeam2, 9);
                this.setBlockAndNotifyAdequately(world, i + 6, j + 5, k1, LOTRMod.woodBeam2, 9);
            }
            for (int i14 = i - 1; i14 <= i + 1; ++i14) {
                this.setBlockAndNotifyAdequately(world, i14, j + 5, k - 6, LOTRMod.woodBeam2, 5);
                this.setBlockAndNotifyAdequately(world, i14, j + 5, k + 6, LOTRMod.woodBeam2, 5);
            }
            for (j1 = j + 2; j1 <= j + 4; ++j1) {
                for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                    this.setBlockAndNotifyAdequately(world, i - 6, j1, k1, LOTRMod.bronzeBars, 0);
                    this.setBlockAndNotifyAdequately(world, i + 6, j1, k1, LOTRMod.bronzeBars, 0);
                }
                for (int i14 = i - 1; i14 <= i + 1; ++i14) {
                    this.setBlockAndNotifyAdequately(world, i14, j1, k - 6, LOTRMod.bronzeBars, 0);
                    this.setBlockAndNotifyAdequately(world, i14, j1, k + 6, LOTRMod.bronzeBars, 0);
                }
            }
            for (i1 = i - 2; i1 <= i + 2; ++i1) {
                for (int k1 = k - 2; k1 <= k + 2; ++k1) {
                    this.setBlockAndNotifyAdequately(world, i1, j + 8, k1, Blocks.air, 0);
                }
            }
            this.setBlockAndNotifyAdequately(world, i - 2, j + 1, k + 1, LOTRMod.woodSlabSingle2, 1);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 1, k + 2, LOTRMod.woodSlabSingle2, 9);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 2, k + 2, LOTRMod.woodSlabSingle2, 1);
            this.setBlockAndNotifyAdequately(world, i, j + 2, k + 2, LOTRMod.woodSlabSingle2, 9);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 3, k + 2, LOTRMod.woodSlabSingle2, 1);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 3, k + 2, LOTRMod.woodSlabSingle2, 9);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 4, k + 1, LOTRMod.woodSlabSingle2, 1);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 4, k, LOTRMod.woodSlabSingle2, 9);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 5, k - 1, LOTRMod.woodSlabSingle2, 1);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 5, k - 2, LOTRMod.woodSlabSingle2, 9);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 6, k - 2, LOTRMod.woodSlabSingle2, 1);
            this.setBlockAndNotifyAdequately(world, i, j + 6, k - 2, LOTRMod.woodSlabSingle2, 9);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 7, k - 2, LOTRMod.woodSlabSingle2, 1);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 7, k - 2, LOTRMod.woodSlabSingle2, 9);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 8, k - 1, LOTRMod.woodSlabSingle2, 1);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 8, k, LOTRMod.woodSlabSingle2, 9);
        }
        for (i1 = i - 1; i1 <= i + 1; ++i1) {
            for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                for (int j12 = j + 1; j12 <= (isTop ? j + 3 : j + 8); ++j12) {
                    this.setBlockAndNotifyAdequately(world, i1, j12, k1, LOTRMod.brick2, 3); // z
                }
            }
        }
        if (isEquipmentSection) {
            int l = random.nextInt(4);
            System.out.println(l);
            switch (l) {
                case 0: {
                    this.setBlockAndNotifyAdequately(world, i, j + 2, k - 5, Blocks.chest, 3);
                    LOTRChestContents.fillChest(world, random, i, j + 2, k - 5, LOTRFAChestContents.brethil_house);
                    for (int i15 = i - 1; i15 <= i + 1; ++i15) {
                        this.setBlockAndNotifyAdequately(world, i15, j + 1, k + 5, LOTRMod.slabSingle4, 9);
                        this.setBlockAndNotifyAdequately(world, i15, j + 1, k - 5, LOTRMod.slabSingle4, 9);
                        this.placeBarrel(world, random, i15, j + 2, k + 5, 2, LOTRFoods.GONDOR_DRINK);

                    }
                    this.setBlockMetadata(world, i, j + 2, k - 5, 3);
                    break;
                }
                case 1: {
                    this.setBlockAndNotifyAdequately(world, i + 5, j + 2, k, Blocks.chest, 4);
                    LOTRChestContents.fillChest(world, random, i + 5, j + 2, k, LOTRFAChestContents.brethil_house);
                    for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                        this.setBlockAndNotifyAdequately(world, i + 5, j + 1, k1, LOTRMod.slabSingle4, 9);
                        this.setBlockAndNotifyAdequately(world, i - 5, j + 1, k1, LOTRMod.slabSingle4, 9);
                        this.placeBarrel(world, random, i - 5, j + 2, k1, 5, LOTRFoods.GONDOR_DRINK);
                    }
                    this.setBlockMetadata(world, i + 5, j + 2, k, 4);
                    break;
                }
                case 2: {
                    this.setBlockAndNotifyAdequately(world, i, j + 2, k + 5, Blocks.chest, 2);
                    LOTRChestContents.fillChest(world, random, i, j + 2, k + 5, LOTRChestContents.GONDOR_FORTRESS_DRINKS);
                    for (int i16 = i - 1; i16 <= i + 1; ++i16) {
                        this.setBlockAndNotifyAdequately(world, i16, j + 1, k - 5, LOTRMod.slabSingle4, 9);
                        this.setBlockAndNotifyAdequately(world, i16, j + 1, k + 5, LOTRMod.slabSingle4, 9);
                        this.placeBarrel(world, random, i16, j + 2, k - 5, 3, LOTRFoods.GONDOR_DRINK);
                    }
                    this.setBlockMetadata(world, i, j + 2, k + 5, 2);
                    break;
                }
                case 3: {
                    this.setBlockAndNotifyAdequately(world, i - 5, j + 2, k, Blocks.chest, 5);
                    LOTRChestContents.fillChest(world, random, i - 5, j + 2, k, LOTRChestContents.GONDOR_FORTRESS_DRINKS);
                    for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                        this.setBlockAndNotifyAdequately(world, i + 5, j + 1, k1, LOTRMod.slabSingle4, 9);
                        this.setBlockAndNotifyAdequately(world, i - 5, j + 1, k1, LOTRMod.slabSingle4, 9);
                        this.placeBarrel(world, random, i + 5, j + 2, k1, 4, LOTRFoods.GONDOR_DRINK);
                    }
                    this.setBlockMetadata(world, i - 5, j + 2, k, 5);
                    break;
                }
            }
        }
        if (isTop) {
            /*
             * for (j1 = j + 1; j1 <= j + 8; ++j1) { for (int k1 = k - 1; k1 <= k + 1; ++k1)
             * { this.setBlockAndNotifyAdequately(world, i - 7, j1, k1,
             * Blocks.mossy_cobblestone, 3); //aa this.setBlockAndNotifyAdequately(world, i
             * + 7, j1, k1, Blocks.nether_brick, 3); //ab } for (int i17 = i - 1; i17 <= i +
             * 1; ++i17) { this.setBlockAndNotifyAdequately(world, i17, j1, k - 7,
             * Blocks.quartz_ore, 3); //ac this.setBlockAndNotifyAdequately(world, i17, j1,
             * k + 7, Blocks.sandstone, 3); //ad } }
             */
            for (int k1 = k - 2; k1 <= k + 2; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 7, j, k1, LOTRMod.stairsArnorBrick, 4);
                this.setBlockAndNotifyAdequately(world, i - 7, j + 1, k1, LOTRMod.stairsArnorBrick, 0);
                this.setBlockAndNotifyAdequately(world, i + 7, j, k1, LOTRMod.stairsArnorBrick, 5);
                this.setBlockAndNotifyAdequately(world, i + 7, j + 1, k1, LOTRMod.stairsArnorBrick, 1);
            }
            for (i1 = i - 2; i1 <= i + 2; ++i1) {
                this.setBlockAndNotifyAdequately(world, i1, j, k - 7, LOTRMod.stairsArnorBrick, 6);
                this.setBlockAndNotifyAdequately(world, i1, j + 1, k - 7, LOTRMod.stairsArnorBrick, 2);
                this.setBlockAndNotifyAdequately(world, i1, j, k + 7, LOTRMod.stairsArnorBrick, 7);
                this.setBlockAndNotifyAdequately(world, i1, j + 1, k + 7, LOTRMod.stairsArnorBrick, 3);
            }
            this.setBlockAndNotifyAdequately(world, i - 2, j + 1, k + 6, LOTRMod.stairsArnorBrick, 0);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 1, k + 6, LOTRMod.stairsArnorBrick, 1);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 1, k - 6, LOTRMod.stairsArnorBrick, 0);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 1, k - 6, LOTRMod.stairsArnorBrick, 1);
            this.setBlockAndNotifyAdequately(world, i - 6, j + 1, k + 2, LOTRMod.stairsArnorBrick, 3);
            this.setBlockAndNotifyAdequately(world, i - 6, j + 1, k - 2, LOTRMod.stairsArnorBrick, 2);
            this.setBlockAndNotifyAdequately(world, i + 6, j + 1, k + 2, LOTRMod.stairsArnorBrick, 3);
            this.setBlockAndNotifyAdequately(world, i + 6, j + 1, k - 2, LOTRMod.stairsArnorBrick, 2);

            this.setBlockAndNotifyAdequately(world, i + 3, j + 1, k - 5, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 1, k - 5, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 1, k - 3, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 1, k - 4, LOTRMod.fence, 9);

            this.setBlockAndNotifyAdequately(world, i - 5, j + 1, k - 3, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i - 5, j + 1, k - 4, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 1, k - 5, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 1, k - 5, LOTRMod.fence, 9);

            this.setBlockAndNotifyAdequately(world, i + 5, j + 1, k + 3, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 1, k + 4, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 1, k + 5, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 1, k + 5, LOTRMod.fence, 9);

            this.setBlockAndNotifyAdequately(world, i - 5, j + 1, k + 3, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i - 5, j + 1, k + 4, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 1, k + 5, LOTRMod.fence, 9);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 1, k + 5, LOTRMod.fence, 9);

            for (j1 = j + 1; j1 <= j + 4; ++j1) {
                this.setBlockAndNotifyAdequately(world, i + 4, j1, k - 4, LOTRMod.woodBeam2, 1);
                this.setBlockAndNotifyAdequately(world, i - 4, j1, k - 4, LOTRMod.woodBeam2, 1);
                this.setBlockAndNotifyAdequately(world, i + 4, j1, k + 4, LOTRMod.woodBeam2, 1);
                this.setBlockAndNotifyAdequately(world, i - 4, j1, k + 4, LOTRMod.woodBeam2, 1);

                this.setBlockAndNotifyAdequately(world, i - 2, j1, k + 5, LOTRMod.brick2, 6);
                this.setBlockAndNotifyAdequately(world, i + 2, j1, k + 5, LOTRMod.brick2, 6);
                this.setBlockAndNotifyAdequately(world, i - 2, j1, k - 5, LOTRMod.brick2, 6);
                this.setBlockAndNotifyAdequately(world, i + 2, j1, k - 5, LOTRMod.brick2, 6);
                this.setBlockAndNotifyAdequately(world, i - 5, j1, k + 2, LOTRMod.brick2, 6);
                this.setBlockAndNotifyAdequately(world, i - 5, j1, k - 2, LOTRMod.brick2, 6);
                this.setBlockAndNotifyAdequately(world, i + 5, j1, k + 2, LOTRMod.brick2, 6);
                this.setBlockAndNotifyAdequately(world, i + 5, j1, k - 2, LOTRMod.brick2, 6);
            }

            for (int k1 = k - 2; k1 <= k + 2; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 6, j + 5, k1, LOTRMod.clayTileDyed, 13);
                this.setBlockAndNotifyAdequately(world, i + 6, j + 5, k1, LOTRMod.clayTileDyed, 13);
                this.setBlockAndNotifyAdequately(world, i - 5, j + 6, k1, LOTRMod.clayTileDyed, 13);
                this.setBlockAndNotifyAdequately(world, i + 5, j + 6, k1, LOTRMod.clayTileDyed, 13);
            }

            this.setBlockAndNotifyAdequately(world, i - 5, j + 5, k - 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 5, k + 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 5, k - 2, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i - 5, j + 5, k + 2, LOTRMod.brick2, 6);

            for (i1 = i - 2; i1 <= i + 2; ++i1) {
                this.setBlockAndNotifyAdequately(world, i1, j + 5, k - 6, LOTRMod.clayTileDyed, 13);
                this.setBlockAndNotifyAdequately(world, i1, j + 5, k + 6, LOTRMod.clayTileDyed, 13);
                this.setBlockAndNotifyAdequately(world, i1, j + 6, k - 5, LOTRMod.clayTileDyed, 13);
                this.setBlockAndNotifyAdequately(world, i1, j + 6, k + 5, LOTRMod.clayTileDyed, 13);
            }

            this.setBlockAndNotifyAdequately(world, i - 2, j + 5, k - 5, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 5, k + 5, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 5, k - 5, LOTRMod.brick2, 6);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 5, k + 5, LOTRMod.brick2, 6);

            for (int k1 = k - 1; k1 <= k + 1; ++k1) {
                this.setBlockAndNotifyAdequately(world, i - 4, j + 7, k1, LOTRMod.clayTileDyed, 13);
                this.setBlockAndNotifyAdequately(world, i + 4, j + 7, k1, LOTRMod.clayTileDyed, 13);
            }
            for (i1 = i - 1; i1 <= i + 1; ++i1) {
                this.setBlockAndNotifyAdequately(world, i1, j + 7, k - 4, LOTRMod.clayTileDyed, 13);
                this.setBlockAndNotifyAdequately(world, i1, j + 7, k + 4, LOTRMod.clayTileDyed, 13);
            }

            this.setBlockAndNotifyAdequately(world, i + 3, j + 5, k - 5, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 5, k - 4, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 5, j + 5, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 5, k - 3, LOTRMod.clayTileDyed, 13);

            this.setBlockAndNotifyAdequately(world, i - 5, j + 5, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 5, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 5, k - 5, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 5, k - 4, LOTRMod.clayTileDyed, 13);

            this.setBlockAndNotifyAdequately(world, i + 5, j + 5, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 5, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 5, k + 5, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 5, k + 4, LOTRMod.clayTileDyed, 13);

            this.setBlockAndNotifyAdequately(world, i - 5, j + 5, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 5, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 5, k + 5, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 5, k + 4, LOTRMod.clayTileDyed, 13);

            this.setBlockAndNotifyAdequately(world, i + 4, j + 5, k - 4, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 5, k - 4, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 5, k + 4, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 5, k + 4, LOTRMod.clayTileDyed, 13);

            this.setBlockAndNotifyAdequately(world, i + 1, j + 3, k, LOTRMod.brick2, 3);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 3, k, LOTRMod.brick2, 3);
            this.setBlockAndNotifyAdequately(world, i, j + 3, k + 1, LOTRMod.brick2, 3);
            this.setBlockAndNotifyAdequately(world, i, j + 3, k - 1, LOTRMod.brick2, 3);

            this.placeWallBanner(world, i, j + 3, k + 1, 0, LOTRFAItemBanner.FABannerTypes.brethil);
            this.placeWallBanner(world, i - 1, j + 3, k, 1, LOTRFAItemBanner.FABannerTypes.brethil);
            this.placeWallBanner(world, i, j + 3, k - 1, 2, LOTRFAItemBanner.FABannerTypes.brethil);
            this.placeWallBanner(world, i + 1, j + 3, k, 3, LOTRFAItemBanner.FABannerTypes.brethil);

            this.setBlockAndNotifyAdequately(world, i, j + 11, k, LOTRMod.clayTileDyed, 13);

            this.placeBanner(world, i, j + 12, k, 0, LOTRFAItemBanner.FABannerTypes.brethil);

            this.setBlockAndNotifyAdequately(world, i + 1, j + 10, k, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 10, k, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i, j + 10, k - 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i, j + 10, k + 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 9, k - 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 9, k + 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 9, k - 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 9, k + 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 9, k, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 9, k, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i, j + 9, k - 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i, j + 9, k + 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 8, k, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 8, k, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i, j + 8, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i, j + 8, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 8, k - 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 8, k + 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 8, k - 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 8, k + 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 8, k - 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 8, k + 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 8, k - 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 8, k + 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 8, k - 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 8, k + 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 8, k - 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 8, k + 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 7, k - 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 7, k + 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 7, k - 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 7, k + 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 7, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 7, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 7, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 7, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 7, k - 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 7, k + 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 7, k - 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 7, k + 1, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 7, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 7, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 1, j + 7, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 1, j + 7, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 6, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 6, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 3, j + 6, k - 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 3, j + 6, k + 3, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 6, k - 4, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 6, k + 4, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 2, j + 6, k - 4, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 2, j + 6, k + 4, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 6, k - 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 6, k + 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i + 4, j + 6, k - 2, LOTRMod.clayTileDyed, 13);
            this.setBlockAndNotifyAdequately(world, i - 4, j + 6, k + 2, LOTRMod.clayTileDyed, 13);

            /*
             * for (j1 = j; j1 <= j + 4; ++j1) { this.setBlockAndNotifyAdequately(world, i -
             * 5, j1, k - 5, Blocks.snow, 3); //ae this.setBlockAndNotifyAdequately(world, i
             * - 5, j1, k + 5, Blocks.soul_sand, 3); //af
             * this.setBlockAndNotifyAdequately(world, i + 5, j1, k - 5,
             * Blocks.stained_hardened_clay, 3); //ag
             * this.setBlockAndNotifyAdequately(world, i + 5, j1, k + 5, Blocks.sponge, 3);
             * //ah }
             */
            // this.placeBanner(world, i - 5, j + 5, k - 5, 0,
            // LOTRFABanners.brethil);
            // this.placeBanner(world, i - 5, j + 5, k + 5, 0,
            // LOTRFABanners.brethil);
            // this.placeBanner(world, i + 5, j + 5, k - 5, 0,
            // LOTRFABanners.brethil);
            // this.placeBanner(world, i + 5, j + 5, k + 5, 0,
            // LOTRFABanners.brethil);
            // this.setBlockAndNotifyAdequately(world, i - 5, j + 2, k - 4,
            // LOTRMod.stairsArnorBrick, 3);
            // this.setBlockAndNotifyAdequately(world, i - 4, j + 2, k - 5,
            // LOTRMod.stairsArnorBrick, 1);
            // this.setBlockAndNotifyAdequately(world, i - 5, j + 2, k + 4,
            // LOTRMod.stairsArnorBrick, 2);
            // this.setBlockAndNotifyAdequately(world, i - 4, j + 2, k + 5,
            // LOTRMod.stairsArnorBrick, 1);
            // this.setBlockAndNotifyAdequately(world, i + 5, j + 2, k - 4,
            // LOTRMod.stairsArnorBrick, 3);
            // this.setBlockAndNotifyAdequately(world, i + 4, j + 2, k - 5,
            // LOTRMod.stairsArnorBrick, 0);
            // this.setBlockAndNotifyAdequately(world, i + 5, j + 2, k + 4,
            // LOTRMod.stairsArnorBrick, 2);
            // this.setBlockAndNotifyAdequately(world, i + 4, j + 2, k + 5,
            // LOTRMod.stairsArnorBrick, 0);
        }
    }

    @Override
    protected void placeOrcTorch(World world, int i, int j, int k) {
        this.setBlockAndNotifyAdequately(world, i, j, k, Blocks.torch, 0);

    }

}
