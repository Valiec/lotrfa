package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import lotr.common.LOTRMod;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.world.structure2.LOTRWorldGenCampBase;
import lotr.common.world.structure2.LOTRWorldGenTentBase;
import net.minecraft.world.World;

public class LOTRWorldGenAngbandCamp extends LOTRWorldGenCampBase {

    public LOTRWorldGenAngbandCamp(boolean flag) {
        super(flag);

    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        this.tableBlock = LOTRFABlocks.craftingTableAngband;
        this.brickBlock = LOTRMod.brick;
        this.brickMeta = 0;
        this.brickSlabBlock = LOTRMod.slabSingle;
        this.brickSlabMeta = 1;
        this.fenceBlock = LOTRMod.fence;
        this.fenceMeta = 3;
        this.fenceGateBlock = LOTRMod.fenceGateCharred;
        this.farmBaseBlock = LOTRMod.rock;
        this.farmBaseMeta = 0;
        this.farmCropBlock = LOTRMod.morgulShroom;
        this.farmCropMeta = 0;
        this.hasOrcTorches = true;
        this.hasSkulls = true;
    }

    @Override
    protected LOTRWorldGenTentBase createTent(boolean flag, Random random) {
        if (random.nextInt(6) == 0) {
            return new LOTRWorldGenAngbandForgeTent(false);
        }
        return new LOTRWorldGenAngbandTent(false);
    }

    @Override
    protected void placeNPCRespawner(final World world, final Random random, final int i, final int j, final int k) {
        final LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClasses(LOTREntityAngbandOrc.class, LOTREntityAngbandOrcArcher.class);
        respawner.setCheckRanges(24, -12, 12, 12);
        respawner.setSpawnRanges(8, -4, 4, 16);
        this.placeNPCRespawner(respawner, world, i, j, k);
    }

    @Override
    protected LOTREntityNPC getCampCaptain(final World world, final Random random) {
        if (random.nextBoolean()) {
            return new LOTREntityAngbandTrader(world);
        }
        return null;
    }

}
