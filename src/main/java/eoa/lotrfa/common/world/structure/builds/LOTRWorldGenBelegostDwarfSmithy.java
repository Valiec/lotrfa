package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.tile.LOTRTileEntityPettyDwarvenForge;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityBlueMountainsSmith;
import lotr.common.entity.npc.LOTREntityDwarf;
import lotr.common.world.structure2.LOTRWorldGenDwarfSmithy;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class LOTRWorldGenBelegostDwarfSmithy extends LOTRWorldGenDwarfSmithy {

    protected Block baseBrickBlock = LOTRMod.brick;
    protected int baseBrickMeta = 14;
    protected Block brickBlock = LOTRMod.brick;
    protected int brickMeta = 6;
    protected Block brickSlabBlock = LOTRMod.slabSingle;
    protected int brickSlabMeta = 7;
    protected Block brickStairBlock = LOTRMod.stairsDwarvenBrick;
    protected Block pillarBlock = LOTRMod.pillar;
    protected int pillarMeta = 3;
    protected Block carvedBrickBlock = LOTRMod.brick3;
    protected int carvedBrickMeta = 0;
    protected Block barsBlock = LOTRMod.blueDwarfBars;
    protected Block tableBlock = LOTRMod.blueDwarvenTable;

    public LOTRWorldGenBelegostDwarfSmithy(boolean flag) {
        super(flag);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected LOTREntityDwarf createSmith(World world) {
        return new LOTREntityBlueMountainsSmith(world);
    }

    @Override
    protected void setupRandomBlocks(Random random) {
        int randomWood = random.nextInt(4);
        if (randomWood == 0) {
            this.plankBlock = Blocks.planks;
            this.plankMeta = 1;
            this.gateBlock = Blocks.fence_gate;
        }
        else if (randomWood == 1) {
            this.plankBlock = LOTRMod.planks;
            this.plankMeta = 13;
            this.gateBlock = LOTRMod.fenceGateLarch;
        }
        else if (randomWood == 2) {
            this.plankBlock = LOTRMod.planks2;
            this.plankMeta = 4;
            this.gateBlock = LOTRMod.fenceGatePine;
        }
        else if (randomWood == 3) {
            this.plankBlock = LOTRMod.planks2;
            this.plankMeta = 3;
            this.gateBlock = LOTRMod.fenceGateFir;
        }
    }

    @Override
    public boolean generateWithSetRotation(World world, Random random, int i, int j, int k, int rotation) {
        int j1;
        int i1;
        int k2;
        int i2;
        int k1;
        int k12;
        // System.out.println("DWARF SMITHY CHECK:"+i+","+j+","+k);
        this.setOriginAndRotation(world, i, j, k, rotation, 5);
        this.setupRandomBlocks(random);
        if (this.restrictions) {
            int minHeight = 0;
            int maxHeight = 0;
            for (int i12 = -4; i12 <= 4; ++i12) {
                for (int k13 = -4; k13 <= 4; ++k13) {
                    j1 = this.getTopBlock(world, i12, k13);
                    Block block = this.getBlock(world, i12, j1 - 1, k13);
                    if (block != Blocks.grass && block != LOTRMod.rock && block != Blocks.dirt && block != Blocks.stone) {
                        // System.out.println("DWARF SMITHY FAIL (BLOCK):"+i+","+j+","+k);
                        return false;
                    }
                    if (j1 < minHeight) {
                        minHeight = j1;
                    }
                    if (j1 > maxHeight) {
                        maxHeight = j1;
                    }
                    if (maxHeight - minHeight <= 5) continue;
                    // System.out.println("DWARF SMITHY FAIL (NONFLAT):"+i+","+j+","+k);
                    return false;
                }
            }
        }
        // System.out.println("DWARF SMITHY:"+i+","+j+","+k);
        for (i1 = -4; i1 <= 4; ++i1) {
            for (k12 = -4; k12 <= 4; ++k12) {
                i2 = Math.abs(i1);
                if (i2 + (k2 = Math.abs(k12)) > 6) continue;
                this.layFoundation(world, i1, k12);
                for (j1 = 1; j1 <= 5; ++j1) {
                    this.setAir(world, i1, j1, k12);
                }
                if (i2 == 4 || k2 == 4) {
                    this.setBlockAndMetadata(world, i1, 1, k12, this.baseBrickBlock, this.baseBrickMeta);
                    this.setBlockAndMetadata(world, i1, 2, k12, this.plankBlock, this.plankMeta);
                    this.setBlockAndMetadata(world, i1, 3, k12, this.brickBlock, this.brickMeta);
                }
                if (i2 != 3 || k2 != 3) continue;
                for (j1 = 1; j1 <= 3; ++j1) {
                    this.setBlockAndMetadata(world, i1, j1, k12, this.pillarBlock, this.pillarMeta);
                }
            }
        }
        for (i1 = -2; i1 <= 2; ++i1) {
            this.setBlockAndMetadata(world, i1, 3, -3, this.brickStairBlock, 7);
            this.setBlockAndMetadata(world, i1, 3, 3, this.brickStairBlock, 6);
        }
        for (k1 = -2; k1 <= 2; ++k1) {
            this.setBlockAndMetadata(world, -3, 3, k1, this.brickStairBlock, 4);
            this.setBlockAndMetadata(world, 3, 3, k1, this.brickStairBlock, 5);
        }
        for (i1 = -3; i1 <= 3; ++i1) {
            for (k12 = -3; k12 <= 3; ++k12) {
                this.setBlockAndMetadata(world, i1, 4, k12, this.brickBlock, this.brickMeta);
            }
        }
        for (i1 = -2; i1 <= 2; ++i1) {
            this.setBlockAndMetadata(world, i1, 4, -4, this.brickStairBlock, 2);
            this.setBlockAndMetadata(world, i1, 4, 4, this.brickStairBlock, 3);
        }
        for (k1 = -2; k1 <= 2; ++k1) {
            this.setBlockAndMetadata(world, -4, 4, k1, this.brickStairBlock, 1);
            this.setBlockAndMetadata(world, 4, 4, k1, this.brickStairBlock, 0);
        }
        this.setBlockAndMetadata(world, -4, 4, 2, this.brickStairBlock, 3);
        this.setBlockAndMetadata(world, -3, 4, 2, this.brickStairBlock, 1);
        this.setBlockAndMetadata(world, -3, 4, 3, this.brickStairBlock, 3);
        this.setBlockAndMetadata(world, -2, 4, 3, this.brickStairBlock, 1);
        this.setBlockAndMetadata(world, 4, 4, 2, this.brickStairBlock, 3);
        this.setBlockAndMetadata(world, 3, 4, 2, this.brickStairBlock, 0);
        this.setBlockAndMetadata(world, 3, 4, 3, this.brickStairBlock, 3);
        this.setBlockAndMetadata(world, 2, 4, 3, this.brickStairBlock, 0);
        this.setBlockAndMetadata(world, -4, 4, -2, this.brickStairBlock, 2);
        this.setBlockAndMetadata(world, -3, 4, -2, this.brickStairBlock, 1);
        this.setBlockAndMetadata(world, -3, 4, -3, this.brickStairBlock, 2);
        this.setBlockAndMetadata(world, -2, 4, -3, this.brickStairBlock, 1);
        this.setBlockAndMetadata(world, 4, 4, -2, this.brickStairBlock, 2);
        this.setBlockAndMetadata(world, 3, 4, -2, this.brickStairBlock, 0);
        this.setBlockAndMetadata(world, 3, 4, -3, this.brickStairBlock, 2);
        this.setBlockAndMetadata(world, 2, 4, -3, this.brickStairBlock, 0);
        for (i1 = -1; i1 <= 1; ++i1) {
            for (k12 = 2; k12 <= 4; ++k12) {
                i2 = Math.abs(i1 - 0);
                k2 = Math.abs(k12 - 3);
                if (i2 == 1 && k2 == 1) {
                    this.setBlockAndMetadata(world, i1, 5, k12, this.brickSlabBlock, this.brickSlabMeta);
                    continue;
                }
                if (i2 == 1 || k2 == 1) {
                    this.setBlockAndMetadata(world, i1, 5, k12, this.brickBlock, this.brickMeta);
                    continue;
                }
                if (i2 != 0 || k2 != 0) continue;
                this.setAir(world, i1, 3, k12);
                this.setAir(world, i1, 4, k12);
            }
            this.setBlockAndMetadata(world, i1, 4, 4, this.brickBlock, this.brickMeta);
            for (int j12 = 1; j12 <= 2; ++j12) {
                this.setBlockAndMetadata(world, i1, j12, 4, this.brickBlock, this.brickMeta);
            }
        }
        this.setBlockAndMetadata(world, 0, 6, 2, this.brickStairBlock, 2);
        this.setBlockAndMetadata(world, -1, 6, 3, this.brickStairBlock, 1);
        this.setBlockAndMetadata(world, 1, 6, 3, this.brickStairBlock, 0);
        this.setBlockAndMetadata(world, 0, 6, 4, this.brickStairBlock, 3);
        this.setBlockAndMetadata(world, 0, 1, -4, this.gateBlock, 0);
        this.setAir(world, 0, 2, -4);
        this.setBlockAndMetadata(world, -2, 2, -3, Blocks.torch, 2);
        this.setBlockAndMetadata(world, 2, 2, -3, Blocks.torch, 1);
        this.setBlockAndMetadata(world, 0, 1, -1, Blocks.anvil, 1);
        for (int i13 : new int[] {-3, 3}) {
            this.setBlockAndMetadata(world, i13, 1, -1, Blocks.anvil, 0);
            this.setBlockAndMetadata(world, i13, 1, 0, this.tableBlock, 0);
            this.setBlockAndMetadata(world, i13, 1, 2, Blocks.crafting_table, 0);
        }
        this.setBlockAndMetadata(world, -3, 1, -2, LOTRMod.unsmeltery, 4);
        this.setBlockAndMetadata(world, 3, 1, -2, LOTRMod.unsmeltery, 5);
        this.placeChest(world, random, -3, 1, 1, 4, this.getChestContents());
        this.placeChest(world, random, 3, 1, 1, 5, this.getChestContents());
        this.placeDwarfForge(world, random, 0, 1, 2, 2);
        this.placeDwarfForge(world, random, -1, 1, 3, 5);
        this.placeDwarfForge(world, random, 1, 1, 3, 4);
        for (int i13 : new int[] {-1, 1}) {
            this.setBlockAndMetadata(world, i13, 1, 2, this.brickBlock, this.brickMeta);
            this.setBlockAndMetadata(world, i13, 2, 2, this.carvedBrickBlock, this.carvedBrickMeta);
            this.setBlockAndMetadata(world, i13, 3, 2, this.brickStairBlock, 2);
            this.setBlockAndMetadata(world, i13, 2, 3, this.barsBlock, 0);
            this.setBlockAndMetadata(world, i13, 3, 3, this.brickBlock, this.brickMeta);
        }
        this.setBlockAndMetadata(world, 0, 2, 2, this.barsBlock, 0);
        this.setBlockAndMetadata(world, 0, 3, 2, this.brickStairBlock, 2);
        this.setBlockAndMetadata(world, 0, 1, 3, Blocks.lava, 0);
        LOTREntityDwarf smith = this.createSmith(world);
        this.spawnNPCAndSetHome(smith, world, 0, 1, 0, 8);
        return true;
    }

    @Override
    protected void placeDwarfForge(World world, Random random, int i, int j, int k, int meta) {
        this.setBlockAndMetadata(world, i, j, k, LOTRMod.dwarvenForge, meta);
        TileEntity tileentity = this.getTileEntity(world, i, j, k);
        if (tileentity instanceof LOTRTileEntityPettyDwarvenForge) {
            LOTRTileEntityPettyDwarvenForge forge = (LOTRTileEntityPettyDwarvenForge) tileentity;
            int fuelAmount = MathHelper.getRandomIntegerInRange(random, 0, 4);
            if (fuelAmount > 0) {
                ItemStack fuel = new ItemStack(Items.coal, fuelAmount, 0);
                forge.setInventorySlotContents(forge.fuelSlot, fuel);
            }
        }
    }

}
