package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.world.structure2.LOTRWorldGenCampBase;
import lotr.common.world.structure2.LOTRWorldGenStructureBase2;
import net.minecraft.world.World;

public class LOTRWorldGenGardhBorenCamp extends LOTRWorldGenCampBase{
	
	public LOTRWorldGenGardhBorenCamp(final boolean flag) {
        super(flag);
    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        this.tableBlock = LOTRFABlocks.craftingTableHouseBor;
    }
    
    @Override
    protected LOTRWorldGenStructureBase2 createTent(final boolean flag, final Random random) {
        return new LOTRWorldGenGardhBorenTent(false);
    }
    
    @Override
    protected LOTREntityNPC getCampCaptain(World world, Random random) {
        switch (random.nextInt(2)) {
            case 0:
                return new LOTREntityBorBlacksmith(world);
            case 1:
                return new LOTREntityBorCaptain(world);
        }
        return null;
    }
    
    @Override
    protected void placeNPCRespawner(final World world, final Random random, final int i, final int j, final int k) {
        final LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClass(LOTREntityBorSoldier.class);
        respawner.setCheckRanges(24, -12, 12, 12);
        respawner.setSpawnRanges(8, -4, 4, 16);
        this.placeNPCRespawner(respawner, world, i, j, k);
    }

}
