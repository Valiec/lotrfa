package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.*;
import lotr.common.LOTRMod;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.world.structure2.*;
import net.minecraft.world.World;

public class LOTRWorldGenTolInGaurhothCamp extends LOTRWorldGenCampBase{
	
	public LOTRWorldGenTolInGaurhothCamp(final boolean flag) {
        super(flag);
    }
    
    @Override
    protected void setupRandomBlocks(final Random random) {
        super.setupRandomBlocks(random);
        this.tableBlock = LOTRFABlocks.craftingTableTolInGaurhoth;
        this.brickBlock = LOTRMod.brick2;
        this.brickMeta = 8;
        this.brickSlabBlock = LOTRMod.slabSingle4;
        this.brickSlabMeta = 5;
        this.fenceBlock = LOTRMod.fence;
        this.fenceMeta = 3;
        this.fenceGateBlock = LOTRMod.fenceGateCharred;
        this.hasOrcTorches = true;
        this.hasSkulls = true;
    }
    
    @Override
    protected LOTRWorldGenStructureBase2 createTent(final boolean flag, final Random random) {
        if (random.nextInt(6) == 0) {
            return new LOTRWorldGenDolGuldurForgeTent(false);
        }
        return new LOTRWorldGenTolInGaurhothTent(false);
    }
    
    @Override
    protected LOTREntityNPC getCampCaptain(final World world, final Random random) {
        if (random.nextBoolean()) {
            return new LOTREntityTolInGaurhothTrader(world);
        }
        return null;
    }
    
    @Override
    protected void placeNPCRespawner(final World world, final Random random, final int i, final int j, final int k) {
        final LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClasses(LOTREntityTolInGaurhothOrc.class, LOTREntityTolInGaurhothOrcArcher.class);
        respawner.setCheckRanges(24, -12, 12, 12);
        respawner.setSpawnRanges(8, -4, 4, 16);
        this.placeNPCRespawner(respawner, world, i, j, k);
    }

}
