package eoa.lotrfa.common.world.structure.builds;

import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianSmith;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityElf;
import lotr.common.world.structure2.LOTRWorldGenHighElvenForge;
import net.minecraft.world.World;

public class LOTRWorldGenFeanorianForge extends LOTRWorldGenHighElvenForge {

    public LOTRWorldGenFeanorianForge(boolean flag) {
        super(flag);
        this.brickBlock = LOTRFABlocks.brick;
        this.brickMeta = 4;
        this.pillarBlock = LOTRFABlocks.pillar;
        this.pillarMeta = 8;
        this.slabBlock = LOTRFABlocks.slabSingle2;
        this.slabMeta = 1;
        this.carvedBrickBlock = LOTRFABlocks.carvedBrickFeanorian;
        this.carvedBrickMeta = 0;
        this.wallBlock = LOTRFABlocks.wall;
        this.wallMeta = 4;
        this.stairBlock = LOTRFABlocks.stairsFeanorian;
        this.roofStairBlock = LOTRMod.stairsClayTileDyedRed;
        this.roofMeta = 14;
        this.tableBlock = LOTRFABlocks.craftingTableFeanorian;
    }

    @Override
    protected LOTREntityElf getElf(World world) {
        return new LOTREntityFeanorianSmith(world);
    }

}
