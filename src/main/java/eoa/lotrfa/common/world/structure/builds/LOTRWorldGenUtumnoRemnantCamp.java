package eoa.lotrfa.common.world.structure.builds;

import java.util.Random;
import eoa.lotrfa.common.entity.npc.*;
import lotr.common.entity.LOTREntityNPCRespawner;
import lotr.common.entity.npc.LOTREntityNPC;
import net.minecraft.world.World;

public class LOTRWorldGenUtumnoRemnantCamp extends LOTRWorldGenDorDaidelosCamp{

	public LOTRWorldGenUtumnoRemnantCamp(boolean flag) {
		super(flag);
	}
	
    @Override
    protected LOTREntityNPC getCampCaptain(World world, Random random) {
        switch (random.nextInt(2)) {
            case 0:
                return new LOTREntityDorDaidelosTrader(world);
            case 1:
                return new LOTREntityUtumnoRemnantOrcCaptain(world);
        }
        return null;
    }

    @Override
    protected void placeNPCRespawner(final World world, final Random random, final int i, final int j, final int k) {
        final LOTREntityNPCRespawner respawner = new LOTREntityNPCRespawner(world);
        respawner.setSpawnClasses(LOTREntityUtumnoRemnantOrc.class, LOTREntityUtumnoRemnantCrossbower.class);
        respawner.setCheckRanges(24, -12, 12, 12);
        respawner.setSpawnRanges(8, -4, 4, 16);
        this.placeNPCRespawner(respawner, world, i, j, k);
    }

}
