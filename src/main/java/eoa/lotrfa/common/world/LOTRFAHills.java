package eoa.lotrfa.common.world;

import eoa.lotrfa.common.reflection.LOTRFAEnumHelper;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;

public class LOTRFAHills {

    public static void setupHills() {
        //TODO store these
        LOTRFAEnumHelper.addMountain("AMON_GWARETH", 621.0f, 642.0f, 2.5f, 800);
        LOTRFAEnumHelper.addMountain("HIMRING", LOTRFAWaypoints.himring.getX(), LOTRFAWaypoints.himring.getY(), 2.5f, 200);
        LOTRFAEnumHelper.addMountain("THANGORODRIM1", LOTRFAWaypoints.angband.getX() - 1, LOTRFAWaypoints.angband.getY() - 2, 5.6f, 120, 25);
        LOTRFAEnumHelper.addMountain("THANGORODRIM2", LOTRFAWaypoints.angband.getX(), LOTRFAWaypoints.angband.getY() - 1, 5.6f, 120, 25);
        LOTRFAEnumHelper.addMountain("THANGORODRIM3", LOTRFAWaypoints.angband.getX() + 1, LOTRFAWaypoints.angband.getY() - 2, 5.6f, 120, 25);
        LOTRFAEnumHelper.addMountain("AMON_ETHIR", LOTRFAWaypoints.amonEthir.getX(), LOTRFAWaypoints.amonEthir.getY(), 1.8f, 120);
        LOTRFAEnumHelper.addMountain("AMON_OBEL", LOTRFAWaypoints.amonObel.getX() + 1, LOTRFAWaypoints.amonObel.getY(), 2.0f, 150);
        LOTRFAEnumHelper.addMountain("AMON_RUDH", LOTRFAWaypoints.amonRudh.getX(), LOTRFAWaypoints.amonRudh.getY(), 2.3f, 150);
        LOTRFAEnumHelper.addMountain("AMON_EREB_FA", LOTRFAWaypoints.amonErebFA.getX(), LOTRFAWaypoints.amonErebFA.getY(), 2.5f, 220);
        LOTRFAEnumHelper.addMountain("MOUNT_DOLMED_FA", LOTRFAWaypoints.mountDolmedFA.getX(), LOTRFAWaypoints.mountDolmedFA.getY(), 4.5f, 350);
        LOTRFAEnumHelper.addMountain("MOUNT_RERIR_FA", LOTRFAWaypoints.mountRerirFA.getX(), LOTRFAWaypoints.mountRerirFA.getY(), 4.0f, 300);
        LOTRFAEnumHelper.addMountain("BELEGOST", LOTRFAWaypoints.belegostFA.getX() - 2, LOTRFAWaypoints.belegostFA.getY() - 2, 5.0f, 300);
        LOTRFAEnumHelper.addMountain("NOGROD", LOTRFAWaypoints.nogrodFA.getX(), LOTRFAWaypoints.nogrodFA.getY() + 2, 5.0f, 300);
        LOTRFAEnumHelper.addMountain("AMON_DARTHIR", LOTRFAWaypoints.amonDarthir.getX(), LOTRFAWaypoints.amonDarthir.getY(), 3.2f, 250);
        LOTRFAEnumHelper.addMountain("FOEN", LOTRFAWaypoints.foen.getX(), LOTRFAWaypoints.foen.getY(), 2.0f, 120);
        LOTRFAEnumHelper.addMountain("MOUNT_TARAS", 271.0f, 707.0f, 3.0f, 150);
    }
}
