package eoa.lotrfa.common;

import java.util.EnumSet;
import eoa.lotrfa.common.reflection.LOTRFAEnumHelper;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import lotr.common.LOTRDimension;
import lotr.common.LOTRDimension.DimensionRegion;
import lotr.common.LOTRFaction;
import lotr.common.LOTRFaction.*;
import lotr.common.world.map.LOTRWaypoint;

public class LOTRFAFactions {
    public static LOTRFaction angband;
    public static LOTRFaction brethil;
    public static LOTRFaction dorDaidelos;
    public static LOTRFaction dorLomin;
    public static LOTRFaction doriath;
    public static LOTRFaction falathrim;
    public static LOTRFaction feanorians;
    public static LOTRFaction gondolin;
    public static LOTRFaction hithlum;
    public static LOTRFaction houseBor;
    public static LOTRFaction houseUlfang;
    public static LOTRFaction laegrim;
    public static LOTRFaction nanDungortheb;
    public static LOTRFaction nargothrond;
    public static LOTRFaction pettyDwarf;
    public static LOTRFaction tolInGaurhoth;

    public static void initPreWaypoints() {
        DimensionRegions.init();

        removeAllFactionsExcluding(LOTRFaction.BLUE_MOUNTAINS, LOTRFaction.FANGORN, LOTRFaction.UTUMNO, LOTRFaction.HOSTILE, LOTRFaction.UNALIGNED);
        editFactions();

        setupFactions();
        setupRelations();
    }

    public static void initPostWaypoints() {
        setupMapInfo();
        setupControlZones();
        setupAchievements();
    }

    private static void removeAllFactionsExcluding(LOTRFaction... factions) {
        factions: for (LOTRFaction faction : LOTRFaction.values()) {
            for (LOTRFaction excludedFaction : factions) {
                if (excludedFaction == faction) continue factions;
            }
            clearFaction(faction);
        }
    }

    private static void editFactions() {
        changeRegion(LOTRFaction.BLUE_MOUNTAINS, DimensionRegions.good);
        LOTRFAReflectionHelper.clearControlZones(LOTRFaction.BLUE_MOUNTAINS);

        changeRegion(LOTRFaction.FANGORN, DimensionRegions.neutral);
        LOTRFAReflectionHelper.clearControlZones(LOTRFaction.FANGORN);
    }

    private static void setupFactions() {
        EnumSet<FactionType> enumSetFreeElf = EnumSet.of(FactionType.TYPE_FREE, FactionType.TYPE_ELF);
        EnumSet<FactionType> enumSetFreeMan = EnumSet.of(FactionType.TYPE_FREE, FactionType.TYPE_MAN);

        angband = LOTRFAEnumHelper.addFaction("ANGBAND", 0x2e282b, DimensionRegions.evil, EnumSet.of(FactionType.TYPE_ORC));
        brethil = LOTRFAEnumHelper.addFaction("BRETHIL", 0x3c5d12, DimensionRegions.good, enumSetFreeMan);
        dorDaidelos = LOTRFAEnumHelper.addFaction("DOR_DAIDELOS", 0x777e82, DimensionRegions.evil, EnumSet.of(FactionType.TYPE_ORC));
        dorLomin = LOTRFAEnumHelper.addFaction("DOR_LOMIN", 0xe4dbbf, DimensionRegions.good, enumSetFreeMan);
        doriath = LOTRFAEnumHelper.addFaction("DORIATH", 0x98c139, DimensionRegions.good, enumSetFreeElf);
        falathrim = LOTRFAEnumHelper.addFaction("FALATHRIM", 0xc6e5ff, DimensionRegions.good, enumSetFreeElf);
        feanorians = LOTRFAEnumHelper.addFaction("FEANORIANS", 0xe05e4b, DimensionRegions.good, enumSetFreeElf);
        gondolin = LOTRFAEnumHelper.addFaction("GONDOLIN", 0x6072c6, DimensionRegions.good, enumSetFreeElf);
        hithlum = LOTRFAEnumHelper.addFaction("HITHLUM", 0x80b3ff, DimensionRegions.good, enumSetFreeElf);
        houseBor = LOTRFAEnumHelper.addFaction("HOUSE_BOR", 0xce875f, DimensionRegions.good, EnumSet.of(FactionType.TYPE_MAN));
        houseUlfang = LOTRFAEnumHelper.addFaction("HOUSE_ULFANG", 0xa8948f, DimensionRegions.evil, EnumSet.of(FactionType.TYPE_MAN));
        laegrim = LOTRFAEnumHelper.addFaction("LAEGRIM", 0x39964e, DimensionRegions.good, enumSetFreeElf);
        nanDungortheb = LOTRFAEnumHelper.addFaction("NAN_DUNGORTHEB", 0, null, null, true, true, -1, null, null);
        nargothrond = LOTRFAEnumHelper.addFaction("NARGOTHROND", 0x74ab97, DimensionRegions.good, enumSetFreeElf);
        pettyDwarf = LOTRFAEnumHelper.addFaction("PETTY_DWARF", 0x827e58, DimensionRegions.neutral, EnumSet.of(FactionType.TYPE_FREE, FactionType.TYPE_DWARF));
        tolInGaurhoth = LOTRFAEnumHelper.addFaction("TOL_IN_GAURHOTH", 0x2e3b35, DimensionRegions.evil, EnumSet.of(FactionType.TYPE_ORC));

        pettyDwarf.isolationist = true;

        // TODO
        // DORIATH LOTRFaction.GALADHRIM;
        // HITHLUM LOTRFaction.HIGH_ELF
        // ANGBAND LOTRFaction.MORDOR
        // UTUMNO REMNANT LOTRFaction.URUK_HAI
        // TOL-IN-GAURHOTH LOTRFaction.DOL_GULDUR
        // LAEGRIM LOTRFaction.WOOD_ELF
        // HOUSE_BOR LOTRFaction.DALE
        // BARAHIRS_OUTLAWS LOTRFaction.RANGER_NORTH
        // ESTOLAD LOTRFaction.ROHAN
        // HOUSE_ULFANG LOTRFaction.DUNLAND
        // ENTS = LOTRFaction.FANGORN
    }

    private static void setupMapInfo() {
        angband.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.dorDaedeloth, 200);
        LOTRFaction.BLUE_MOUNTAINS.factionMapInfo = new FactionMapInfo(1080, 785, 150);
        brethil.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.amonObel, 50);
        dorDaidelos.factionMapInfo = new FactionMapInfo(1124, 374, 50);
        dorLomin.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.dorLomin, 50);
        doriath.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.menegroth, 100);
        LOTRFaction.FANGORN.factionMapInfo = new FactionMapInfo(1080, 785, 150);
        falathrim.factionMapInfo = new FactionMapInfo(621, 642, 100);
        feanorians.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.himring, 100);
        gondolin.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.amonGwareth, 50);
        hithlum.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.northHithlum, 150);
        houseBor.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.gardhBoren, 50);
        houseUlfang.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.talathdru, 50);
        laegrim.factionMapInfo = new FactionMapInfo(1022, 723, 150);
        nanDungortheb.factionMapInfo = new FactionMapInfo(1, 1, 100);
        nargothrond.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.nargothrond, 150);
        pettyDwarf.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.amonRudh, 50);
        tolInGaurhoth.factionMapInfo = getMapInfoFromWaypoint(LOTRFAWaypoints.tolSirion, 50);
    }

    private static void setupAchievements() {
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(angband, LOTRFAAchievements.alignment_angband);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(brethil, LOTRFAAchievements.alignment_brethil);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(dorDaidelos, LOTRFAAchievements.alignment_dorDaidelos);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(dorLomin, LOTRFAAchievements.alignment_dorLomin);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(doriath, LOTRFAAchievements.alignment_doriath);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(falathrim, LOTRFAAchievements.alignment_falathrim);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(feanorians, LOTRFAAchievements.alignment_feanorians);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(gondolin, LOTRFAAchievements.alignment_gondolin);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(hithlum, LOTRFAAchievements.alignment_hithlum);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(houseBor, LOTRFAAchievements.alignment_houseBor);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(houseUlfang, LOTRFAAchievements.alignment_houseUlfang);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(laegrim, LOTRFAAchievements.alignment_laegrim);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(nargothrond, LOTRFAAchievements.alignment_nargothrond);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(pettyDwarf, LOTRFAAchievements.alignment_pettyDwarf);
        LOTRFAReflectionHelper.setFactionAlignmentAchievements(tolInGaurhoth, LOTRFAAchievements.alignment_tolInGaurhoth);
    }

    private static void setupControlZones() {
    	//ORCS
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.angband, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 220));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.angband, new ControlZone(LOTRFAWaypoints.nargothrond, 130));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.angband, new ControlZone(LOTRFAWaypoints.sarnAthrad, 80));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.tolInGaurhoth, new ControlZone(LOTRFAWaypoints.tolSirion, 160));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.tolInGaurhoth, new ControlZone(LOTRFAWaypoints.angband, 100));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.dorDaidelos, new ControlZone(LOTRFAWaypoints.angband, 50));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.dorDaidelos, new ControlZone(LOTRFAWaypoints.northHithlum, 120));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.dorDaidelos, new ControlZone(1124, 374, 75));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.dorDaidelos, new ControlZone(LOTRFAWaypoints.himring, 50));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.dorDaidelos, new ControlZone(LOTRFAWaypoints.maglorsGap, 50));
        
        //MEN
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.houseUlfang, new ControlZone(LOTRFAWaypoints.thargelion, 120));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.houseUlfang, new ControlZone(LOTRFAWaypoints.lothlann, 100));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.houseUlfang, new ControlZone(LOTRFAWaypoints.angband, 80));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.brethil, new ControlZone(LOTRFAWaypoints.amonObel, 60));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.brethil, new ControlZone(LOTRFAWaypoints.foen, 65));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.brethil, new ControlZone(LOTRFAWaypoints.tolSirion, 45));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.brethil, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 35));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.dorLomin, new ControlZone(LOTRFAWaypoints.dorLomin, 120));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.dorLomin, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 50));

        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.houseBor, new ControlZone(LOTRFAWaypoints.gardhBoren, 70));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.houseBor, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 35));
        
        //DWARVES AND OTHERS
        LOTRFAReflectionHelper.addControlZone(LOTRFaction.BLUE_MOUNTAINS, new ControlZone(LOTRFAWaypoints.nogrodFA, 120));
        LOTRFAReflectionHelper.addControlZone(LOTRFaction.BLUE_MOUNTAINS, new ControlZone(LOTRFAWaypoints.lakeHelevorn, 120));
        LOTRFAReflectionHelper.addControlZone(LOTRFaction.BLUE_MOUNTAINS, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 35));
        LOTRFAReflectionHelper.addControlZone(LOTRFaction.BLUE_MOUNTAINS, new ControlZone(LOTRFAWaypoints.himring, 45));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.pettyDwarf, new ControlZone(LOTRFAWaypoints.amonRudh, 30));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFaction.FANGORN, new ControlZone(LOTRFAWaypoints.lantInOnodrim, 90));
        LOTRFAReflectionHelper.addControlZone(LOTRFaction.FANGORN, new ControlZone(LOTRFAWaypoints.nanTathren, 30));

        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.nanDungortheb, new ControlZone(1, 1, 80));
        
        //ELVES
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.doriath, new ControlZone(LOTRFAWaypoints.amonObel, 55));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.doriath, new ControlZone(LOTRFAWaypoints.menegroth, 120));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.doriath, new ControlZone(LOTRFAWaypoints.nanElmoth, 30));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.doriath, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 50));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.doriath, new ControlZone(LOTRFAWaypoints.tolSirion, 45));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.falathrim, new ControlZone(LOTRFAWaypoints.brithombar, 120));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.falathrim, new ControlZone(LOTRFAWaypoints.eglarest, 120));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.falathrim, new ControlZone(LOTRFAWaypoints.havensOfSirion, 80));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.falathrim, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 35));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.falathrim, new ControlZone(LOTRFAWaypoints.tolSirion, 45));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.feanorians, new ControlZone(LOTRFAWaypoints.lothlann, 100));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.feanorians, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 80));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.feanorians, new ControlZone(LOTRFAWaypoints.himring, 70));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.feanorians, new ControlZone(LOTRFAWaypoints.amonErebFA, 45));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.feanorians, new ControlZone(LOTRFAWaypoints.thargelion, 120));

        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.gondolin, new ControlZone(LOTRFAWaypoints.amonGwareth, 65));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.gondolin, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 65));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.hithlum, new ControlZone(LOTRFAWaypoints.northHithlum, 100));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.hithlum, new ControlZone(LOTRFAWaypoints.baradEithel, 140));

        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.laegrim, new ControlZone(LOTRFAWaypoints.haudaliebar, 150));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.laegrim, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 35));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.laegrim, new ControlZone(LOTRFAWaypoints.himring, 50));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.laegrim, new ControlZone(LOTRFAWaypoints.maglorsGap, 50));
        
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.nargothrond, new ControlZone(LOTRFAWaypoints.nargothrond, 170));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.nargothrond, new ControlZone(LOTRFAWaypoints.tolSirion, 65));
        LOTRFAReflectionHelper.addControlZone(LOTRFAFactions.nargothrond, new ControlZone(LOTRFAWaypoints.dorDaedeloth, 35));
    }

    private static void setupRelations() {
        LOTRFAReflectionHelper.clearRelations(LOTRFaction.BLUE_MOUNTAINS);

        addAllies(angband, houseUlfang, tolInGaurhoth, dorDaidelos);
        addEnemies(angband, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, doriath, LOTRFaction.FANGORN, falathrim, feanorians, gondolin, hithlum, houseBor, laegrim, nargothrond, pettyDwarf, LOTRFaction.UTUMNO);

        addAllies(brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, doriath, falathrim, feanorians, gondolin, hithlum, houseBor, laegrim, nargothrond);
        addEnemies(brethil, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addAllies(LOTRFaction.BLUE_MOUNTAINS, brethil, dorLomin, doriath, falathrim, feanorians, gondolin, hithlum, houseBor, laegrim, nargothrond);
        addEnemies(LOTRFaction.BLUE_MOUNTAINS, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);
        
        addAllies(dorDaidelos, angband, houseUlfang, tolInGaurhoth);
        addEnemies(dorDaidelos, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, doriath, LOTRFaction.FANGORN, falathrim, feanorians, gondolin, hithlum, houseBor, laegrim, nargothrond, pettyDwarf, LOTRFaction.UTUMNO);

        addAllies(dorLomin, brethil, LOTRFaction.BLUE_MOUNTAINS, doriath, falathrim, feanorians, gondolin, hithlum, houseBor, laegrim, nargothrond);
        addEnemies(dorLomin, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addAllies(doriath, brethil, LOTRFaction.BLUE_MOUNTAINS, LOTRFaction.FANGORN, falathrim, houseBor, laegrim, nargothrond);
        addEnemies(doriath, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addAllies(LOTRFaction.FANGORN, doriath, laegrim);
        addEnemies(LOTRFaction.FANGORN, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos);

        addAllies(falathrim, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, doriath, feanorians, gondolin, hithlum, houseBor, laegrim, nargothrond);
        addEnemies(falathrim, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addAllies(feanorians, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, falathrim, gondolin, hithlum, houseBor, laegrim, nargothrond);
        addEnemies(feanorians, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addAllies(gondolin, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, falathrim, feanorians, hithlum, houseBor, laegrim, nargothrond);
        addEnemies(gondolin, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addAllies(hithlum, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, falathrim, feanorians, gondolin, houseBor, laegrim, nargothrond);
        addEnemies(hithlum, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addAllies(houseBor, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, doriath, falathrim, feanorians, gondolin, hithlum, laegrim, nargothrond);
        addEnemies(houseBor, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addAllies(houseUlfang, angband, tolInGaurhoth, dorDaidelos);
        addEnemies(houseUlfang, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, doriath, LOTRFaction.FANGORN, falathrim, feanorians, gondolin, hithlum, houseBor, laegrim, nargothrond, LOTRFaction.UTUMNO);

        addAllies(laegrim, doriath, LOTRFaction.FANGORN, falathrim, feanorians, nargothrond);
        addEnemies(laegrim, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addEnemies(nanDungortheb, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, doriath, LOTRFaction.FANGORN, falathrim, feanorians, gondolin, hithlum, houseBor, laegrim, nargothrond, LOTRFaction.UTUMNO);

        addAllies(nargothrond, brethil, dorLomin, doriath, falathrim, LOTRFaction.BLUE_MOUNTAINS, feanorians, gondolin, hithlum, houseBor, laegrim);
        addEnemies(nargothrond, angband, houseUlfang, nanDungortheb, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addEnemies(pettyDwarf, angband, tolInGaurhoth, dorDaidelos, LOTRFaction.UTUMNO);

        addAllies(tolInGaurhoth, angband, houseUlfang, dorDaidelos);
        addEnemies(tolInGaurhoth, brethil, LOTRFaction.BLUE_MOUNTAINS, dorLomin, doriath, LOTRFaction.FANGORN, falathrim, feanorians, gondolin, hithlum, houseBor, laegrim, nargothrond, pettyDwarf, LOTRFaction.UTUMNO);
        
        
        addEnemies(LOTRFaction.UTUMNO, angband, brethil, dorLomin, doriath, falathrim, feanorians, gondolin, hithlum, houseBor, houseUlfang, laegrim, nanDungortheb, nargothrond, pettyDwarf, tolInGaurhoth, dorDaidelos);
    }

    private static FactionMapInfo getMapInfoFromWaypoint(LOTRWaypoint waypoint, int radius) {
        return new FactionMapInfo(waypoint.getX(), waypoint.getY(), radius);
    }

    private static void addAllies(LOTRFaction faction, LOTRFaction... allies) {
        for (LOTRFaction ally : allies) LOTRFAReflectionHelper.addAlly(faction, ally);
    }

    private static void addEnemies(LOTRFaction faction, LOTRFaction... enemies) {
        for (LOTRFaction enemy : enemies) LOTRFAReflectionHelper.addEnemy(faction, enemy);
    }

    private static void changeRegion(LOTRFaction faction, DimensionRegion newRegion) {
        faction.factionRegion.factionList.remove(faction);
        newRegion.factionList.add(faction);
        faction.factionRegion = newRegion;
    }

    private static void clearFaction(LOTRFaction faction) {
        faction.allowPlayer = false;
        faction.hasFixedAlignment = true;
        faction.fixedAlignment = 0;

        if (faction.factionDimension != null) faction.factionDimension.factionList.remove(faction);
        if (faction.factionRegion != null) faction.factionRegion.factionList.remove(faction);
        faction.factionDimension = null;
        faction.factionRegion = null;
        LOTRFAReflectionHelper.clearControlZones(faction);
    }

    public static class DimensionRegions {
        public static DimensionRegion good;
        public static DimensionRegion neutral;
        public static DimensionRegion evil;

        private static void init() {
            removeDimensionRegions();

            good = LOTRFAEnumHelper.addDimensionRegion("GOOD", "good");
            neutral = LOTRFAEnumHelper.addDimensionRegion("NEUTRAL", "neutral");
            evil = LOTRFAEnumHelper.addDimensionRegion("EVIL", "evil");

            setDimension(good, LOTRDimension.MIDDLE_EARTH);
            setDimension(neutral, LOTRDimension.MIDDLE_EARTH);
            setDimension(evil, LOTRDimension.MIDDLE_EARTH);
        }

        private static void removeDimensionRegions() {
            LOTRDimension.MIDDLE_EARTH.dimensionRegions.clear();
        }

        private static void setDimension(DimensionRegion region, LOTRDimension dimension) {
            region.setDimension(dimension);
            dimension.dimensionRegions.add(region);
        }
    }
}
