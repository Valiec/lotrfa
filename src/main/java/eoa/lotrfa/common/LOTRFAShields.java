package eoa.lotrfa.common;

import eoa.lotrfa.common.reflection.LOTRFAEnumHelper;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import eoa.lotrfa.common.util.URLHelper;
import lotr.common.LOTRFaction;
import lotr.common.LOTRShields;
import lotr.common.LOTRShields.ShieldType;
import net.minecraft.util.ResourceLocation;

public class LOTRFAShields {
    public static LOTRShields alignmentAngband;
    //public static LOTRShields alignmentBelegost;
    public static LOTRShields alignmentBrethil;
    public static LOTRShields alignmentDorDaidelos;
    public static LOTRShields alignmentDorLomin;
    public static LOTRShields alignmentDoriath;
    public static LOTRShields alignmentFalathrim;
    public static LOTRShields alignmentFeanorians;
    public static LOTRShields alignmentGondolin;
    public static LOTRShields alignmentHithlum;
    public static LOTRShields alignmentHouseBeor;
    public static LOTRShields alignmentHouseBor;
    public static LOTRShields alignmentHouseUlfang;
    public static LOTRShields alignmentLaegrim;
    public static LOTRShields alignmentNargothrond;
    //public static LOTRShields alignmentNogrod;
    public static LOTRShields alignmentPettyDwarf;
    public static LOTRShields alignmentTolInGaurhoth;
    public static LOTRShields alignmentUtumnoRemnant;

    public static LOTRShields supporter;

    public static void init() {
        hideAlignmentShieldExcluding(LOTRShields.ALIGNMENT_BLUE_MOUNTAINS);

        alignmentAngband = addAlignmentShield("ALIGNMENT_ANGBAND", LOTRFAFactions.angband);
        alignmentBrethil = addAlignmentShield("ALIGNMENT_BRETHIL", LOTRFAFactions.brethil);
        //alignmentBelegost = addAlignmentShield("ALIGNMENT_BELEGOST", LOTRFaction.BLUE_MOUNTAINS);
        alignmentDorDaidelos = addAlignmentShield("ALIGNMENT_DOR_DAIDELOS", LOTRFAFactions.dorDaidelos);
        alignmentDorLomin = addAlignmentShield("ALIGNMENT_DOR_LOMIN", LOTRFAFactions.dorLomin);
        alignmentDoriath = addAlignmentShield("ALIGNMENT_DORIATH", LOTRFAFactions.doriath);
        alignmentFalathrim = addAlignmentShield("ALIGNMENT_FALAS", LOTRFAFactions.falathrim);
        alignmentFeanorians = addAlignmentShield("ALIGNMENT_FEANORIANS", LOTRFAFactions.feanorians);
        alignmentGondolin = addAlignmentShield("ALIGNMENT_GONDOLIN", LOTRFAFactions.gondolin);
        alignmentHithlum = addAlignmentShield("ALIGNMENT_HITHLUM", LOTRFAFactions.hithlum);
        alignmentHouseBeor = addAlignmentShield("ALIGNMENT_HOUSE_BEOR", LOTRFAFactions.dorLomin);
        alignmentHouseBor = addAlignmentShield("ALIGNMENT_HOUSE_BOR", LOTRFAFactions.houseBor);
        alignmentHouseUlfang = addAlignmentShield("ALIGNMENT_HOUSE_ULFANG", LOTRFAFactions.houseUlfang);
        alignmentLaegrim = addAlignmentShield("ALIGNMENT_LAEGRIM", LOTRFAFactions.laegrim);
        alignmentNargothrond = addAlignmentShield("ALIGNMENT_NARGOTHROND", LOTRFAFactions.nargothrond);
        //alignmentNogrod = addAlignmentShield("ALIGNMENT_NOGROD", LOTRFaction.BLUE_MOUNTAINS);
        alignmentPettyDwarf = addAlignmentShield("ALIGNMENT_PETTY_DWARF", LOTRFAFactions.pettyDwarf);
        alignmentTolInGaurhoth = addAlignmentShield("ALIGNMENT_TOL_IN_GAURHOTH", LOTRFAFactions.tolInGaurhoth);
        alignmentUtumnoRemnant = addAlignmentShield("ALIGNMENT_UTUMNO_REMNANT", LOTRFAFactions.dorDaidelos);

        String[] supporterList = URLHelper.getStringArrayFromURL("https://gitlab.com/eras-of-arda/EoA-public/raw/master/lotrfa/shield-uuids.txt");
        if(supporterList.length == 0) LOTRFA.logger.warn("Was unable to retrieve the shield supporters list. Probably caused by a issue in the internet connection.");
        supporter = addShield("SUPPORTER", false, supporterList);
    }
    
    private static LOTRShields addAlignmentShield(String enumName, LOTRFaction faction) {
        LOTRShields shield = LOTRFAEnumHelper.addAlignmentShield(enumName, faction);
        shield.shieldTexture = new ResourceLocation("lotrfa:shield/" + shield.name().toLowerCase() + ".png");
        
        return shield;
    }
    
    private static LOTRShields addShield(String enumName, boolean hidden, String[] uuids) {
        LOTRShields shield = LOTRFAEnumHelper.addShield(enumName, hidden, uuids);
        shield.shieldTexture = new ResourceLocation("lotrfa:shield/" + shield.name().toLowerCase() + ".png");
        
        return shield;
    }

    private static void hideAlignmentShieldExcluding(LOTRShields... shields) {
        shields: for (LOTRShields shield : ShieldType.ALIGNMENT.list) {
            for (LOTRShields excludedShield : shields) {
                if (excludedShield.shieldType != ShieldType.ALIGNMENT) throw new IllegalArgumentException("Excluding shields from hiding have to be alignment shields. " + shield.getShieldName());
                if (excludedShield == shield) continue shields;
            }

            LOTRFAReflectionHelper.hideShield(shield);
        }
    }
}
