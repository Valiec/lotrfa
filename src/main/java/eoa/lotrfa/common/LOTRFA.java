package eoa.lotrfa.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.*;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.entity.*;
import eoa.lotrfa.common.item.*;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import eoa.lotrfa.common.tile.LOTRTileEntityPettyDwarvenForge;
import eoa.lotrfa.common.util.*;
import eoa.lotrfa.common.world.LOTRFAHills;
import eoa.lotrfa.common.world.biome.LOTRFABiomes;
import eoa.lotrfa.common.world.map.LOTRFARoads;
import eoa.lotrfa.common.world.map.LOTRFAWaypoints;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import eoa.lotrfa.common.world.structure.*;
import lotr.common.LOTRDate;
import lotr.common.LOTRDate.ShireReckoning.Date;
import lotr.common.LOTRDate.ShireReckoning.Month;
import lotr.common.command.LOTRCommandStrScan;

@Mod(modid = LOTRFA.MODID, version = LOTRFA.VERSION, name = LOTRFA.MODNAME, dependencies="required-after:lotr")
public class LOTRFA {
    public static final String MODID = "lotrfa";
    public static final String MODNAME = "Lord of the Rings Mod: First Age";
    public static final String VERSION = "2.0.0-BETA.1";
    public static Logger logger = LogManager.getLogger("LOTRFA");

    @Mod.Instance(value = "lotrfa")
    public static LOTRFA instance;
    @SidedProxy(clientSide = "eoa.lotrfa.client.ClientProxy", serverSide = "eoa.lotrfa.common.CommonProxy")
    public static CommonProxy proxy;
    private static LOTRFAEventHandler eventHandler;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        NetworkRegistry.INSTANCE.registerGuiHandler(this, proxy);
        eventHandler = new LOTRFAEventHandler();
        LOTRFAMaterial.changeLOTRMaterials();

        proxy.preInit(event);
        LOTRFAEntities.registerEntities();
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        LOTRFAFactions.initPreWaypoints();
        LOTRFAWaypoints.setupWaypoints();
        proxy.editTickHanderClient();

        LOTRFAItemBanner.FABannerTypes.init();
        LOTRFACreativeTabs.init();
        LOTRFABlocks.init();
        LOTRFAItems.init();
        LOTRFAMaterial.setupCraftingItems();
        LOTRFABlocks.registerFireInfo();
        LOTRFACreativeTabs.setupIcons();
        LOTRFABlocks.initItemBlockHide();

        LOTRFAInvasions.init();
        LOTRFABiomes.setupBiomes();

        LOTRFAAchievements.init();
        LOTRFAShields.init();
        LOTRFACapes.init();
        LOTRFAFactions.initPostWaypoints();
        LOTRFATitle.init();

        GameRegistry.registerTileEntity(LOTRTileEntityPettyDwarvenForge.class, "pettyDwarvenForge");
        FARecipes.clearBaseRecipes();
        FARecipes.createRecipes();

        LOTRFARoads.init();
        LOTRFAHills.setupHills();

        LOTRFATrades.setupTrades();
        LOTRFAUnitTrades.setupTrades();
        
        proxy.registerRenderers();
        StringBanks.loadSpeechBanks(instance);
        StringBanks.loadNameBanks(instance);
        LOTRFALore.LoreCategories.init();
        LOTRFALore.loadLoreBanks(instance);
        LOTRFAStructureScan.loadAllScans(instance);
        LOTRFAStructures.registerStructures();
        
        LOTRFAMiniQuests.init();
        ResourceHelper.closeJarFile(instance);
        proxy.init(event);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        UpdateChecker.checkForUpdates();
        LOTRFAReflectionHelper.disableLOTRUpdateChecker();
        
        

        /*
         * Field f = LOTRMapGenDwarvenMine.class.getDeclaredField("spawnBiomes");
         * f.setAccessible(true); 
         * 
         * List l = (List)(f.get(LOTRMapGenDwarvenMine.class));
         * l.add(biomes.orocarni); 
         * l.add(biomes.orocarniFoothills);
         * 
         * f.set(LOTRMapGenDwarvenMine.class, l);
         */
        
        LOTRFAChestContents.init();
        

        LOTRDate.ShireReckoning.startDate = new Date(400, Month.AFTERYULE, 1);
        LOTRDate.SR_TO_TA = 0;
        LOTRDate.THIRD_AGE_CURRENT = 400;
    }

    @EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        event.registerServerCommand(new LOTRCommandStrScan());
    }

    @EventHandler
    public void serverStarted(FMLServerStartedEvent event) {
        if(UpdateChecker.isUpdateAbailable()) UpdateChecker.announceUpdateServerLogs();
    }
}
