package eoa.lotrfa.common;

import java.util.ArrayList;
import java.util.List;
import cpw.mods.fml.common.registry.GameRegistry;
import eoa.lotrfa.common.block.LOTRFABlock;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.item.equipment.*;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.LOTRMod;
import lotr.common.item.*;
import lotr.common.item.LOTRItemDagger.DaggerEffect;
import lotr.common.recipe.LOTRRecipePoisonWeapon;
import lotr.common.recipe.LOTRRecipes;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.*;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.oredict.*;

public class FARecipes {
	//Faction recipes
    public static List<IRecipe> angbandRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> brethilRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> dorDaidelosRecipes = new ArrayList<IRecipe>(); 
    public static List<IRecipe> doriathRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> dorLominRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> falathrimRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> feanorianRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> gondolinRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> hithlumRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> houseBorRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> houseUlfangRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> laegrimRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> nargothrondRecipes = new ArrayList<IRecipe>();
	public static List<IRecipe> pettyDwarvenRecipes = new ArrayList<IRecipe>();
    public static List<IRecipe> tolInGaurhothRecipes = new ArrayList<IRecipe>();
	
    //Common recipes
    @SuppressWarnings("unchecked")
    private static List<IRecipe>[] commonDwarfRecipes = new List[] {pettyDwarvenRecipes};
    @SuppressWarnings("unchecked")
    private static List<IRecipe>[] commonElfRecipes = new List[] {doriathRecipes, falathrimRecipes, feanorianRecipes, gondolinRecipes, nargothrondRecipes};
    @SuppressWarnings("unchecked")
    private static List<IRecipe>[] commonOrcsRecipes = new List[] {angbandRecipes, dorDaidelosRecipes, tolInGaurhothRecipes};
    @SuppressWarnings("unchecked")
    private static List<IRecipe>[] commonMannishRecipes = new List[] {houseUlfangRecipes};

    @SuppressWarnings("unchecked")
    public static void clearBaseRecipes() {
        List<IRecipe>[] recipesToClear = new List[] {LOTRRecipes.morgulRecipes, LOTRRecipes.elvenRecipes, LOTRRecipes.dwarvenRecipes, LOTRRecipes.urukRecipes, LOTRRecipes.woodElvenRecipes, LOTRRecipes.gondorianRecipes, LOTRRecipes.rohirricRecipes, LOTRRecipes.dunlendingRecipes, LOTRRecipes.angmarRecipes, LOTRRecipes.nearHaradRecipes, LOTRRecipes.highElvenRecipes, LOTRRecipes.rangerRecipes, LOTRRecipes.dolGuldurRecipes, LOTRRecipes.gundabadRecipes, LOTRRecipes.halfTrollRecipes, LOTRRecipes.dolAmrothRecipes, LOTRRecipes.moredainRecipes, LOTRRecipes.tauredainRecipes, LOTRRecipes.daleRecipes, LOTRRecipes.dorwinionRecipes, LOTRRecipes.hobbitRecipes, LOTRRecipes.rhunRecipes, LOTRRecipes.rivendellRecipes, LOTRRecipes.umbarRecipes, LOTRRecipes.gulfRecipes};
        for(List<IRecipe> recipe : recipesToClear) recipe.clear();
    }
    
    public static void createRecipes() {
        createVanillaRecipes();
        createSmeltingRecipes();
        
        //Faction recipes
        createAngbandRecipes();
        createBrethilRecipes();
        createDorDaidelosRecipes();
        createDoriathRecipes();
        createDorLominRecipes();
        createFalathrimRecipes();
        createFeanorianRecipes();
        createGondolinRecipes();
        createHithlumRecipes();
        createHouseBorRecipes();
        createHouseUlfangRecipes();
        createLaegrimRecipes();
        createNargothrondRecipes();
        createPettyDwarvenRecipes();
        createTolInGaurhothRecipes();
        createMiscFactionRecipes();
        
        //Common recipes
        createCommonOrcRecipes();
        createCommonDwarfRecipes();
        createCommonMannishRecipes();
    }
    
    private static void createSmeltingRecipes() {
        GameRegistry.addSmelting(new ItemStack(LOTRFABlocks.pillar, 1, 0), new ItemStack(LOTRFABlocks.pillar, 1, 1), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRMod.pillar2, 1, 5), new ItemStack(LOTRFABlocks.pillar, 1, 2), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRFABlocks.pillar, 1, 3), new ItemStack(LOTRFABlocks.pillar, 1, 4), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRFABlocks.pillar, 1, 6), new ItemStack(LOTRFABlocks.pillar, 1, 7), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRFABlocks.pillar, 1, 8), new ItemStack(LOTRFABlocks.pillar, 1, 9), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRFABlocks.pillar, 1, 10), new ItemStack(LOTRFABlocks.pillar, 1, 11), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRMod.pillar2, 1, 6), new ItemStack(LOTRFABlocks.pillar, 1, 12), 0.1f);
        
        GameRegistry.addSmelting(new ItemStack(LOTRFABlocks.brick, 1, 0), new ItemStack(LOTRFABlocks.brick, 1, 1), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRFABlocks.brick, 1, 2), new ItemStack(LOTRFABlocks.brick2, 1, 0), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRFABlocks.brick, 1, 4), new ItemStack(LOTRFABlocks.brick, 1, 7), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRFABlocks.brick, 1, 5), new ItemStack(LOTRFABlocks.brick, 1, 8), 0.1f);
        GameRegistry.addSmelting(new ItemStack(LOTRMod.brick5, 1, 5), new ItemStack(LOTRFABlocks.brick2, 1, 0), 0.1f);//check narg brick recipe
    }
    
    private static void createVanillaRecipes() {
        GameRegistry.addShapelessRecipe(new ItemStack(LOTRMod.bottlePoison, 1), new Object[] {Items.glass_bottle, Items.fermented_spider_eye, LOTRMod.sulfur});
        GameRegistry.addShapelessRecipe(new ItemStack(Items.dye, 1, 15), new Object[] {Blocks.dirt, Items.rotten_flesh, Items.spider_eye, LOTRMod.sulfur});
        GameRegistry.addShapelessRecipe(new ItemStack(LOTRMod.wood, 1, 3), new Object[] {LOTRMod.rottenLog, LOTRMod.sulfur});
        GameRegistry.addShapelessRecipe(new ItemStack(Blocks.dirt, 4, 1), new Object[] {Blocks.dirt, Blocks.dirt, Blocks.dirt, Blocks.dirt, LOTRMod.sulfur});
    }
    
    private static void createAngbandRecipes() {
        addArmorRecipes(angbandRecipes, LOTRMod.helmetOrc, LOTRMod.bodyOrc, LOTRMod.legsOrc, LOTRMod.bootsOrc);
        addMeleeWeaponRecipes(angbandRecipes, LOTRMod.scimitarOrc, LOTRMod.battleaxeOrc, LOTRMod.daggerOrc, LOTRMod.spearOrc, LOTRMod.hammerOrc, LOTRMod.polearmOrc);
        addToolRecipes(angbandRecipes, LOTRMod.shovelOrc, LOTRMod.pickaxeOrc, LOTRMod.axeOrc, LOTRMod.hoeOrc);
        
        addArmorRecipes(angbandRecipes, LOTRMod.helmetBlackUruk, LOTRMod.bodyBlackUruk, LOTRMod.legsBlackUruk, LOTRMod.bootsBlackUruk);
        addMeleeWeaponRecipes(angbandRecipes, LOTRMod.scimitarBlackUruk, LOTRMod.daggerBlackUruk, LOTRMod.spearBlackUruk, LOTRMod.battleaxeBlackUruk, LOTRMod.hammerBlackUruk);
        addRangedWeaponRecipes(angbandRecipes, LOTRMod.blackUrukBow);
        
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcSkullStaff), new Object[] { "X", "Y", "Y", 'X', Items.skull, 'Y', "stickWood" }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcBedItem), new Object[] { "XXX", "YYY", 'X', Blocks.wool, 'Y', "plankWood" }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.maggotyBread), new Object[] { "XXX", 'X', Items.wheat }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcSteelBars, 16), new Object[] { "XXX", "XXX", 'X', LOTRMod.orcSteel }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcBow), new Object[] { " XY", "X Y", " XY", 'X', LOTRMod.orcSteel, 'Y', Items.string }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcBomb, 4), new Object[] { "XYX", "YXY", "XYX", 'X', Items.gunpowder, 'Y', LOTRMod.orcSteel }));
        angbandRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.orcBomb, 1, 1), new Object[] { new ItemStack(LOTRMod.orcBomb, 1, 0), Items.gunpowder, LOTRMod.orcSteel }));
        angbandRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.orcBomb, 1, 2), new Object[] { new ItemStack(LOTRMod.orcBomb, 1, 1), Items.gunpowder, LOTRMod.orcSteel }));
        
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wargArmorMordor), new Object[] { "X  ", "XYX", "XXX", 'X', LOTRMod.orcSteel, 'Y', Items.leather }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableAngband), new Object[] { "XX", "YY", 'X', "plankWood", 'Y', new ItemStack(LOTRMod.rock, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcForge), new Object[] { "XXX", "X X", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.smoothStone, 2, 0), new Object[] { "X", "X", 'X', new ItemStack(LOTRMod.rock, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick, 4, 0), new Object[] { "XX", "XX", 'X', new ItemStack(LOTRMod.rock, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick2, 1, 10), new Object[] { "XX", "XX", 'X', new ItemStack(LOTRMod.brick, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.pillar, 3, 7), new Object[] { "X", "X", "X", 'X', new ItemStack(LOTRMod.rock, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle10, 6, 7), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.rock, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle, 6, 0), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.smoothStone, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle, 6, 1), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle2, 6, 2), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 7) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle5, 6, 1), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar, 1, 7) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsMordorBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsMordorBrickCracked, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 7) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsMordorRock, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.rock, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall, 6, 0), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.rock, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall, 6, 1), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 0) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall, 6, 9), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 7) }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcTorchItem, 2), new Object[] { "X", "Y", "Y", 'X', LOTRMod.nauriteGem, 'Y', "stickWood" }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.angband.bannerID), new Object[] { "X", "Y", "Z", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood" }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 7), new Object[] { " X ", "YZY", 'X', "stickWood", 'Y', LOTRMod.orcTorchItem, 'Z', LOTRMod.orcSteel }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateOrc, 4), new Object[] { "YYY", "YXY", "YYY", 'X', LOTRMod.gateGear, 'Y', LOTRMod.orcSteel }));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.rhunFireJar), new Object[] {"XYX", "YZY", "XYX", Character.valueOf('X'), LOTRMod.orcSteel, Character.valueOf('Y'), Items.gunpowder, Character.valueOf('Z'), LOTRMod.nauriteGem}));
        angbandRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.rhunFirePot, 4), new Object[] {"Z", "Y", "X", Character.valueOf('X'), LOTRMod.orcSteel, Character.valueOf('Y'), Items.gunpowder, Character.valueOf('Z'), LOTRMod.nauriteGem}));
    }
    
    private static void createBrethilRecipes() {
        addArmorRecipes(brethilRecipes, LOTRFAItems.brethilRangerHelmet, LOTRFAItems.brethilRangerChestplate, LOTRFAItems.brethilRangerLeggings, LOTRFAItems.brethilRangerBoots);
        addMeleeWeaponRecipes(brethilRecipes, "ingotIron", LOTRFAItems.brethilSword, LOTRFAItems.brethilSpear, LOTRFAItems.brethilDagger, LOTRFAItems.brethilPike, LOTRFAItems.brethilBattleaxe, LOTRMod.lanceGondor);
        GameRegistry.addRecipe(new LOTRRecipePoisonWeapon(LOTRFAItems.brethilDagger, LOTRFAItems.brethilDaggerPoisoned));
        addRangedWeaponRecipes(brethilRecipes, "stickWood", LOTRFAItems.brethilBow);
        
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chestLebethron), new Object[] {"XXX", "XYX", "XXX", 'X', new ItemStack(LOTRMod.planks, 1, 8), 'Y', "nuggetSilver"}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bodyGondorGambeson), new Object[] {"X X", "XXX", "XXX", 'X', Blocks.wool}));
        
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.brethilHelmet), new Object[] {"YYY", "X X", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.brethilChestplate), new Object[] {"X X", "YYY", "XXX", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.brethilLeggings), new Object[] {"YYY", "X X", "Y Y", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.brethilBoots), new Object[] {"Y Y", "X X", Character.valueOf('X'), Items.iron_ingot, Character.valueOf('Y'), Items.leather}));
        
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableBrethil), new Object[] {"XX", "XX", 'X', "plankWood"}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.brethil.bannerID), new Object[] {"XA", "Y ", "Z ", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood", 'A', LOTRMod.blackroot}));
      
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick2, 4, 3), new Object[] {"XX", "XX", 'X', Blocks.stone}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle4, 6, 1), new Object[] {"XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 3)}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsArnorBrick, 4), new Object[] {"X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 3)}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 4), new Object[] {"XXX", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 3)}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick2, 1, 6), new Object[] {"XX", "XX", 'X', new ItemStack(LOTRMod.brick2, 1, 3)}));
        brethilRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.brick2, 1, 4), new Object[] {new ItemStack(LOTRMod.brick2, 1, 3), "vine"}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle4, 6, 2), new Object[] {"XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 4)}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsArnorBrickMossy, 4), new Object[] {"X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 4)}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 5), new Object[] {"XXX", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 4)}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle4, 6, 3), new Object[] {"XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 5)}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsArnorBrickCracked, 4), new Object[] {"X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 5)}));
        brethilRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 6), new Object[] {"XXX", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 5)}));
    }
    
    private static void createDorDaidelosRecipes() {
        addArmorRecipes(dorDaidelosRecipes, LOTRMod.helmetUruk, LOTRMod.bodyUruk, LOTRMod.legsUruk, LOTRMod.bootsUruk);
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.helmetUrukBerserker), new Object[] { "XYX", " Z ", 'X', LOTRMod.urukSteel, 'Y', "dyeWhite", 'Z', new ItemStack(LOTRMod.helmetUruk, 1, 0) }));
        addMeleeWeaponRecipes(dorDaidelosRecipes, LOTRMod.scimitarUruk, LOTRMod.daggerUruk, LOTRMod.battleaxeUruk, LOTRMod.hammerUruk, LOTRMod.spearUruk, LOTRMod.pikeUruk, LOTRFAItems.utumnoRemnantBerserkerCleaver);
        addRangedWeaponRecipes(dorDaidelosRecipes, LOTRMod.urukCrossbow);
        addToolRecipes(dorDaidelosRecipes, LOTRMod.shovelUruk, LOTRMod.pickaxeUruk, LOTRMod.axeUruk, LOTRMod.hoeUruk);
        
        
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcSkullStaff), new Object[] { "X", "Y", "Y", 'X', Items.skull, 'Y', "stickWood" }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcBedItem), new Object[] { "XXX", "YYY", 'X', Blocks.wool, 'Y', "plankWood" }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.maggotyBread), new Object[] { "XXX", 'X', Items.wheat }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcSteelBars, 16), new Object[] { "XXX", "XXX", 'X', LOTRMod.orcSteel }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcBow), new Object[] { " XY", "X Y", " XY", 'X', LOTRMod.orcSteel, 'Y', Items.string }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wargArmorUruk), new Object[] { "X  ", "XYX", "XXX", 'X', LOTRMod.urukSteel, 'Y', Items.leather }));
        
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableDorDaidelos), new Object[] { "XX", "YY", 'X', "plankWood", 'Y', new ItemStack(LOTRMod.brick2, 1, 7) }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcTorchItem, 2), new Object[] { "X", "Y", "Y", 'X', new ItemStack(Items.coal, 1, 32767), 'Y', "stickWood" }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 9), new Object[] { " X ", "YZY", 'X', "stickWood", 'Y', LOTRMod.orcTorchItem, 'Z', LOTRMod.urukSteel }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcForge), new Object[] { "XXX", "X X", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 7) }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcBomb, 4), new Object[] { "XYX", "YXY", "XYX", 'X', Items.gunpowder, 'Y', LOTRMod.urukSteel }));
        dorDaidelosRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.orcBomb, 1, 1), new Object[] { new ItemStack(LOTRMod.orcBomb, 1, 0), Items.gunpowder, LOTRMod.urukSteel }));
        dorDaidelosRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.orcBomb, 1, 2), new Object[] { new ItemStack(LOTRMod.orcBomb, 1, 1), Items.gunpowder, LOTRMod.urukSteel }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.dorDaidelos.bannerID), new Object[] { "X", "Y", "Z", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood" }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.utumnoRemnant.bannerID), new Object[] { "XA", "Y ", "Z ", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood", 'A', LOTRMod.urukSteel }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick2, 4, 7), new Object[] { "XX", "XX", 'X', Blocks.stone }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle4, 6, 4), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 7) }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsUrukBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 7) }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 7), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 7) }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.urukBars, 16), new Object[] { "XXX", "XXX", 'X', LOTRMod.urukSteel }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateUruk, 4), new Object[] { "YYY", "YXY", "YYY", 'X', LOTRMod.gateGear, 'Y', LOTRMod.urukSteel }));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.rhunFireJar), new Object[] {"XYX", "YZY", "XYX", Character.valueOf('X'), LOTRMod.orcSteel, Character.valueOf('Y'), Items.gunpowder, Character.valueOf('Z'), LOTRMod.nauriteGem}));
        dorDaidelosRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.rhunFirePot, 4), new Object[] {"Z", "Y", "X", Character.valueOf('X'), LOTRMod.orcSteel, Character.valueOf('Y'), Items.gunpowder, Character.valueOf('Z'), LOTRMod.nauriteGem}));
    }
    
    private static void createDoriathRecipes() {
        addArmorRecipes(doriathRecipes, LOTRMod.helmetElven, LOTRMod.bodyElven, LOTRMod.legsElven, LOTRMod.bootsElven);
        addArmorRecipes(doriathRecipes, LOTRMod.helmetHithlain, LOTRMod.bodyHithlain, LOTRMod.legsHithlain, LOTRMod.bootsHithlain);
        addMeleeWeaponRecipes(doriathRecipes, LOTRMod.swordElven, LOTRMod.spearElven, LOTRMod.daggerElven, LOTRMod.polearmElven, LOTRMod.longspearElven);
        addRangedWeaponRecipes(doriathRecipes, LOTRMod.elvenBow);
        addToolRecipes(doriathRecipes, LOTRMod.shovelElven, LOTRMod.pickaxeElven, LOTRMod.axeElven, LOTRMod.hoeElven);
        
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableDoriath), new Object[] { "XX", "XX", 'X', "plankWood"}));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.elvenBedItem), new Object[] { "XXX", "YYY", 'X', LOTRMod.hithlain, 'Y', new ItemStack(LOTRMod.planks, 1, 1) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.fence, 3, 1), new Object[] { "XYX", "XYX", 'X', new ItemStack(LOTRMod.planks, 1, 1), 'Y', "stickWood" }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.doriath.bannerID), new Object[] { "X", "Y", "Z", 'X', Blocks.wool, 'Y', "stickWood", 'Z', new ItemStack(LOTRMod.planks, 1, 1) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.horseArmorGaladhrim), new Object[] { "X  ", "XYX", "XXX", 'X', LOTRMod.elfSteel, 'Y', Items.leather }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick, 4, 11), new Object[] { "XX", "XX", 'X', Blocks.stone }));
        doriathRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.brick, 1, 12), new Object[] { new ItemStack(LOTRMod.brick, 1, 11), "vine" }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle2, 6, 3), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick, 4, 11) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle2, 6, 4), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick, 4, 12) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle2, 6, 5), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick, 4, 13) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsElvenBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 11) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsElvenBrickMossy, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 12) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsElvenBrickCracked, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 13) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall, 6, 10), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 11) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall, 6, 11), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 12) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall, 6, 12), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 13) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.pillar, 3, 1), new Object[] { "X", "X", "X", 'X', Blocks.stone }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle2, 6, 6), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar, 1, 1) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle2, 6, 7), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar, 1, 2) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick2, 1, 15), new Object[] { "XX", "XX", 'X', new ItemStack(LOTRMod.brick, 1, 11) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.elvenForge), new Object[] { "XXX", "X X", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 11) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.galadhrimBars, 16), new Object[] { "XXX", "XXX", 'X', LOTRMod.elfSteel }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.galadhrimWoodBars, 16), new Object[] { "XXX", "XXX", 'X', "plankWood" }));	
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.hithlain, 3), new Object[] { "XXX", 'X', Items.string }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.hithlainLadder, 3), new Object[] { "X", "X", "X", 'X', LOTRMod.hithlain }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick4, 1, 9), new Object[] { " X ", "XYX", " X ", 'X', "nuggetSilver", 'Y', new ItemStack(LOTRMod.brick, 1, 11) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick4, 1, 12), new Object[] { " X ", "XYX", " X ", 'X', "nuggetGold", 'Y', new ItemStack(LOTRMod.brick, 1, 11) }));
        doriathRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateElven, 4), new Object[] { "ZYZ", "YXY", "ZYZ", 'X', LOTRMod.gateGear, 'Y', new ItemStack(LOTRMod.planks, 1, 1), 'Z', LOTRMod.elfSteel }));
    }
    
    private static void createDorLominRecipes() {
        addMeleeWeaponRecipes(dorLominRecipes, "ingotIron", LOTRFAItems.dorLominSword, LOTRFAItems.dorLominSpear, LOTRFAItems.dorLominDagger, LOTRFAItems.dorLominPike, LOTRFAItems.dorLominBattleaxe, LOTRMod.lanceGondor);
        GameRegistry.addRecipe(new LOTRRecipePoisonWeapon(LOTRFAItems.dorLominDagger, LOTRFAItems.dorLominDaggerPoisoned));
        addRangedWeaponRecipes(dorLominRecipes, "stickWood", LOTRFAItems.dorLominBow);
        
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.horseArmorGondor), new Object[] {"X  ", "XYX", "XXX", 'X', "ingotIron", 'Y', Items.leather}));
        
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.dorLominHelmet), new Object[] {"XXX", "X X", Character.valueOf('X'), Items.iron_ingot}));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.dorLominChestplate), new Object[] {"X X", "YYY", "XXX", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.dorLominLeggings), new Object[] {"YYY", "X X", "Y Y", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.dorLominBoots), new Object[] {"Y Y", "X X", Character.valueOf('X'), Items.iron_ingot, Character.valueOf('Y'), Items.leather}));
        
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chestLebethron), new Object[] {"XXX", "XYX", "XXX", 'X', new ItemStack(LOTRMod.planks, 1, 8), 'Y', "nuggetSilver"}));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bodyGondorGambeson), new Object[] {"X X", "XXX", "XXX", 'X', Blocks.wool}));
        
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableDorLomin), new Object[] {"XX", "YY", 'X', "plankWood", 'Y', new ItemStack(LOTRMod.rock, 1, 1)}));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.dorLomin.bannerID), new Object[] { "X", "Y", "Z", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood" }));
        
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick5, 4, 1), new Object[] { "XX", "XX", 'X', Blocks.stone }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle9, 6, 6), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 1) }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDaleBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 1) }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall3, 6, 9), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 1) }));
        
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateGondor, 4), new Object[] {"ZYZ", "YXY", "ZYZ", 'X', LOTRMod.gateGear, 'Y', "plankWood", 'Z', "ingotIron"}));

        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.pillar2, 3, 5), new Object[] { "X", "X", "X", 'X', Blocks.stone }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle10, 6, 0), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar2, 1, 5) }));
        
        dorLominRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.brick6, 1, 3), new Object[] { new ItemStack(LOTRMod.brick5, 1, 1), "vine" }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle13, 6, 0), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick6, 1, 3) }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDaleBrickMossy, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick6, 1, 3) }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall4, 6, 14), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick6, 1, 3) }));
        
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle13, 6, 1), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick6, 1, 4) }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDaleBrickCracked, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick6, 1, 4) }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall4, 6, 15), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick6, 1, 4) }));
        dorLominRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick6, 1, 5), new Object[] { "XX", "XX", 'X', new ItemStack(LOTRMod.brick5, 1, 1) }));

    }
    
    private static void createFalathrimRecipes() {
        addArmorRecipes(falathrimRecipes, LOTRMod.bodyDolAmrothGambeson, LOTRMod.legsDolAmrothGambeson);
        addArmorRecipes(falathrimRecipes, new ItemStack(LOTRMod.elfSteel), LOTRMod.bodyDolAmroth, LOTRMod.legsDolAmroth, LOTRMod.bootsDolAmroth);
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.helmetDolAmroth), new Object[] {"Y Y", "XXX", "X X", Character.valueOf('X'), LOTRMod.elfSteel, Character.valueOf('Y'), LOTRMod.swanFeather}));
        addMeleeWeaponRecipes(falathrimRecipes, new ItemStack(LOTRMod.elfSteel), LOTRFAItems.falathrimSword, LOTRFAItems.falathrimDagger, LOTRFAItems.falathrimSpear, LOTRFAItems.falathrimBattlestaff, LOTRFAItems.falathrimLongspear, LOTRMod.lanceDolAmroth);
        falathrimRecipes.add(new LOTRRecipePoisonWeapon(LOTRFAItems.falathrimDagger, LOTRFAItems.falathrimDaggerPoisoned, new ItemStack(LOTRMod.bottlePoison, 1, 0)));
        addRangedWeaponRecipes(falathrimRecipes, new ItemStack(LOTRMod.elfSteel), LOTRFAItems.falathrimBow);
        addToolRecipes(falathrimRecipes, new ItemStack(LOTRMod.elfSteel), LOTRFAItems.falathrimShovel, LOTRFAItems.falathrimPickaxe, LOTRFAItems.falathrimAxe, LOTRFAItems.falathrimHoe);

        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.falasTorch), new Object[] {"X", "Y", Character.valueOf('X'), Items.coal, Character.valueOf('Y'), Items.stick}));


        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.horseArmorDolAmroth), new Object[] {"X  ", "XYX", "XXX", Character.valueOf('X'), LOTRMod.elfSteel, Character.valueOf('Y'), Items.leather}));
        
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenTorch, 4), new Object[] {"X", "Y", Character.valueOf('X'), LOTRMod.quenditeCrystal, Character.valueOf('Y'), "stickWood"}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 10), new Object[] {" X ", "YZY", Character.valueOf('X'), "stickWood", Character.valueOf('Y'), LOTRMod.highElvenTorch, Character.valueOf('Z'), LOTRMod.elfSteel}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenBedItem), new Object[] {"XXX", "YYY", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "plankWood"}));
        
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateDolAmroth, 4), new Object[] {"ZYZ", "YXY", "ZYZ", Character.valueOf('X'), LOTRMod.gateGear, Character.valueOf('Y'), "plankWood", Character.valueOf('Z'), LOTRMod.elfSteel}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableFalathrim), new Object[] {"XX", "YY", Character.valueOf('X'), "plankWood", Character.valueOf('Y'), new ItemStack(LOTRMod.rock, 1, 1)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.falathrim.bannerID), new Object[] {"X", "Y", "Z", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "stickWood", Character.valueOf('Z'), "plankWood"}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 4, 5), new Object[] {"XX", "XX", Character.valueOf('X'), Blocks.stone}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle2, 6, 2), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsFalas, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 14), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.elvenForge), new Object[] {"XXX", "X X", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        
        falathrimRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRFABlocks.brick, 1, 6), new Object[] { new ItemStack(LOTRFABlocks.brick, 1, 5), "vine" }));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle3, 6, 0), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsFalasMossy, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 7), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle2, 6, 4), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsFalasCracked, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 9), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
        
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.pillar, 3, 6), new Object[] { "X", "X", "X", 'X', Blocks.stone }));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle2, 6, 5), new Object[] { "XXX", 'X', new ItemStack(LOTRFABlocks.pillar, 1, 6) }));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle2, 6, 6), new Object[] { "XXX", 'X', new ItemStack(LOTRFABlocks.pillar, 1, 7) }));

        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 1, 13), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetSilver", Character.valueOf('Y'), new ItemStack(LOTRFABlocks.brick, 1, 6)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 1, 12), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetGold", Character.valueOf('Y'), new ItemStack(LOTRFABlocks.brick, 1, 6)}));
        falathrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.carvedBrickFalas, 1), new Object[] {"XX", "XX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 5)}));
    }
    
    private static void createFeanorianRecipes() {
        addArmorRecipes(feanorianRecipes, LOTRFAItems.feanorianHelmet, LOTRFAItems.feanorianChestplate, LOTRFAItems.feanorianLeggings, LOTRFAItems.feanorianBoots);
        addMeleeWeaponRecipes(feanorianRecipes, LOTRFAItems.feanorianSword, LOTRFAItems.feanorianDagger, LOTRFAItems.feanorianSpear, LOTRFAItems.feanorianBattlestaff, LOTRFAItems.feanorianLongspear, LOTRFAItems.feanorianLance);
        GameRegistry.addRecipe(new LOTRRecipePoisonWeapon(LOTRFAItems.feanorianDagger, LOTRFAItems.feanorianDaggerPoisoned, new ItemStack(LOTRMod.bottlePoison, 1, 0)));
        addRangedWeaponRecipes(feanorianRecipes, LOTRFAItems.feanorianBow);
        addToolRecipes(feanorianRecipes, LOTRFAItems.feanorianShovel, LOTRFAItems.feanorianPickaxe, LOTRFAItems.feanorianAxe, LOTRFAItems.feanorianHoe);

        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.feanorianHorseArmor), new Object[] {"X  ", "XYX", "XXX", Character.valueOf('X'), LOTRMod.elfSteel, Character.valueOf('Y'), Items.leather}));

        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.feanorianTorch), new Object[] {"X", "Y", Character.valueOf('X'), Items.coal, Character.valueOf('Y'), Items.stick}));


        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenTorch, 4), new Object[] {"X", "Y", Character.valueOf('X'), LOTRMod.quenditeCrystal, Character.valueOf('Y'), "stickWood"}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 10), new Object[] {" X ", "YZY", Character.valueOf('X'), "stickWood", Character.valueOf('Y'), LOTRMod.highElvenTorch, Character.valueOf('Z'), LOTRMod.elfSteel}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenBedItem), new Object[] {"XXX", "YYY", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "plankWood"}));
        
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 4, 4), new Object[] {"XX", "XX", Character.valueOf('X'), Blocks.stone}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle2, 6, 1), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 4)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle3, 6, 1), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 11)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle2, 6, 3), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 7)}));
        
        feanorianRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRFABlocks.brick, 1, 11), new Object[] { new ItemStack(LOTRFABlocks.brick, 1, 4), "vine" }));
        
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsFeanorian, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 4)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsFeanorianCracked, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 7)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsFeanorianMossy, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 11)}));
        
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 4), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 4)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 8), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 7)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 10), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 11)}));
        
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.pillar, 3, 8), new Object[] {"X", "X", "X", Character.valueOf('X'), Blocks.stone}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle2, 6, 7), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.pillar, 1, 8)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle3, 6, 4), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.pillar, 1, 9)}));
        
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.carvedBrickFeanorian, 1), new Object[] {"XX", "XX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 4)}));
        
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.elvenForge), new Object[] {"XXX", "X X", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 4)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElfBars, 16), new Object[] {"XXX", "XXX", Character.valueOf('X'), LOTRMod.elfSteel}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElfWoodBars, 16), new Object[] {"XXX", "XXX", Character.valueOf('X'), "plankWood"}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 1, 10), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetSilver", Character.valueOf('Y'), new ItemStack(LOTRFABlocks.brick, 1, 4)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 1, 9), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetGold", Character.valueOf('Y'), new ItemStack(LOTRFABlocks.brick, 1, 4)}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateHighElven, 4), new Object[] {"ZYZ", "YXY", "ZYZ", Character.valueOf('X'), LOTRMod.gateGear, Character.valueOf('Y'), "plankWood", Character.valueOf('Z'), LOTRMod.elfSteel}));

        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableFeanorian), new Object[] {"XX", "XX", Character.valueOf('X'), "plankWood"}));
        feanorianRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.feanorian.bannerID), new Object[] {"X", "Y", "Z", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "stickWood", Character.valueOf('Z'), "plankWood"}));
    }
    
    private static void createGondolinRecipes() {
        addArmorRecipes(gondolinRecipes, LOTRMod.helmetGondolin, LOTRMod.bodyGondolin, LOTRMod.legsGondolin, LOTRMod.bootsGondolin);
        addMeleeWeaponRecipes(gondolinRecipes, LOTRMod.swordGondolin, LOTRFAItems.gondolinLongspear, LOTRFAItems.gondolinSpear, LOTRFAItems.gondolinDagger);
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.gondolinGlaive), new Object[] {"X  ", " Y ", "  X", Character.valueOf('X'), LOTRMod.elfSteel, Character.valueOf('Y'), "stickWood"}));
        GameRegistry.addRecipe(new LOTRRecipePoisonWeapon(LOTRFAItems.gondolinDagger, LOTRFAItems.gondolinDaggerPoisoned, new ItemStack(LOTRMod.bottlePoison, 1, 0)));
        addRangedWeaponRecipes(gondolinRecipes, LOTRFAItems.gondolinBow);
        addToolRecipes(gondolinRecipes, LOTRFAItems.gondolinShovel, LOTRFAItems.gondolinPickaxe, LOTRFAItems.gondolinAxe, LOTRFAItems.gondolinHoe);

        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.gondolinTorch), new Object[] {"X", "Y", Character.valueOf('X'), Items.coal, Character.valueOf('Y'), Items.stick}));


        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.gondolinHorseArmor), new Object[] {"X  ", "XYX", "XXX", Character.valueOf('X'), LOTRMod.elfSteel, Character.valueOf('Y'), Items.leather}));
        
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableGondolin), new Object[] {"XX", "XX", Character.valueOf('X'), "plankWood"}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.gondolin.bannerID), new Object[] {"X", "Y", "Z", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "stickWood", Character.valueOf('Z'), "plankWood"}));
       
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 4, 2), new Object[] {"XX", "XX", Character.valueOf('X'), Blocks.stone}));
        gondolinRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRFABlocks.brick2, 1, 1), new Object[] { new ItemStack(LOTRFABlocks.brick, 1, 2), "vine" }));
        
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle, 6, 4), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 2)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle3, 6, 5), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick2, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle3, 6, 6), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick2, 1, 1)}));
        
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsGondolin, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 2)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsGondolinCracked, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick2, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsGondolinMossy, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick2, 1, 1)}));
        
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 2), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 2)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 12), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick2, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 11), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick2, 1, 1)}));

        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick2, 1, 3), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetSilver", Character.valueOf('Y'), new ItemStack(LOTRFABlocks.brick, 1, 2)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick2, 1, 2), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetGold", Character.valueOf('Y'), new ItemStack(LOTRFABlocks.brick, 1, 2)}));
        
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.pillar, 3, 10), new Object[] { "X", "X", "X", 'X', Blocks.stone }));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle3, 6, 7), new Object[] { "XXX", 'X', new ItemStack(LOTRFABlocks.pillar, 1, 10) }));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle4, 6, 0), new Object[] { "XXX", 'X', new ItemStack(LOTRFABlocks.pillar, 1, 11) }));
        
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingleSarnlum, 6, 0), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.rockSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingleSarnlum, 6, 1), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brickSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingleSarnlum, 6, 2), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.pillarSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingleSarnlum, 6, 3), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.smoothStoneSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brickSarnlum, 4, 0), new Object[] {"XX", "XX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.rockSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wallSarnlum, 6, 0), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.rockSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsSarnlum, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.rockSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsSarnlumBrick, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brickSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wallSarnlum, 6, 1), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brickSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.pillarSarnlum, 3, 0), new Object[] {"X", "X", "X", Character.valueOf('X'), new ItemStack(LOTRFABlocks.rockSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.smoothStoneSarnlum, 2, 0), new Object[] {"X", "X", Character.valueOf('X'), new ItemStack(LOTRFABlocks.rockSarnlum, 1, 0)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.carvedBrickSarnlum, 1, 0), new Object[] {"XX", "XX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brickSarnlum, 1, 0)}));
        
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.carvedBrickGondolin, 1), new Object[] {"XX", "XX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 2)}));
        
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenTorch, 4), new Object[] {"X", "Y", Character.valueOf('X'), LOTRMod.quenditeCrystal, Character.valueOf('Y'), "stickWood"}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 10), new Object[] {" X ", "YZY", Character.valueOf('X'), "stickWood", Character.valueOf('Y'), LOTRMod.highElvenTorch, Character.valueOf('Z'), LOTRMod.elfSteel}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenBedItem), new Object[] {"XXX", "YYY", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "plankWood"}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick3, 4, 2), new Object[] {"XX", "XX", Character.valueOf('X'), Blocks.stone}));
        //gondolinRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.brick3, 1, 3), new Object[] {new ItemStack(LOTRMod.brick3, 1, 2), "vine"}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle5, 6, 5), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 2)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle5, 6, 6), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 3)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle5, 6, 7), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 4)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsHighElvenBrick, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 2)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsHighElvenBrickMossy, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 3)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsHighElvenBrickCracked, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 4)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 11), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 2)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 12), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 3)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 13), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 4)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.pillar, 3, 10), new Object[] {"X", "X", "X", Character.valueOf('X'), Blocks.stone}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle6, 6, 0), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRMod.pillar, 1, 10)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle6, 6, 1), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRMod.pillar, 1, 11)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick2, 1, 13), new Object[] {"XX", "XX", Character.valueOf('X'), new ItemStack(LOTRMod.brick3, 1, 2)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.elvenForge), new Object[] {"XXX", "X X", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 2)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElfBars, 16), new Object[] {"XXX", "XXX", Character.valueOf('X'), LOTRMod.elfSteel}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElfWoodBars, 16), new Object[] {"XXX", "XXX", Character.valueOf('X'), "plankWood"}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick4, 1, 8), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetSilver", Character.valueOf('Y'), new ItemStack(LOTRMod.brick3, 1, 2)}));
        //gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick4, 1, 11), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetGold", Character.valueOf('Y'), new ItemStack(LOTRMod.brick3, 1, 2)}));
        gondolinRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateHighElven, 4), new Object[] {"ZYZ", "YXY", "ZYZ", Character.valueOf('X'), LOTRMod.gateGear, Character.valueOf('Y'), "plankWood", Character.valueOf('Z'), LOTRMod.elfSteel}));
    }
    
    private static void createHithlumRecipes() {
        addArmorRecipes(hithlumRecipes, LOTRMod.helmetHighElven, LOTRMod.bodyHighElven, LOTRMod.legsHighElven, LOTRMod.bootsHighElven);
        addMeleeWeaponRecipes(hithlumRecipes, LOTRMod.swordHighElven, LOTRMod.daggerHighElven, LOTRMod.spearHighElven, LOTRMod.polearmHighElven, LOTRMod.longspearHighElven);
        addRangedWeaponRecipes(hithlumRecipes, LOTRMod.highElvenBow);
        addToolRecipes(hithlumRecipes, LOTRMod.shovelHighElven, LOTRMod.pickaxeHighElven, LOTRMod.axeHighElven, LOTRMod.hoeHighElven);


        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenTorch), new Object[] {"X", "Y", Character.valueOf('X'), Items.coal, Character.valueOf('Y'), Items.stick}));


        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableHithlum), new Object[] { "XX", "XX", 'X', "plankWood" }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.hithlum.bannerID), new Object[] { "X", "Y", "Z", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood" }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.horseArmorHighElven), new Object[] { "X  ", "XYX", "XXX", 'X', LOTRMod.elfSteel, 'Y', Items.leather }));
        
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenTorch, 4), new Object[] { "X", "Y", 'X', LOTRMod.quenditeCrystal, 'Y', "stickWood" }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 10), new Object[] { " X ", "YZY", 'X', "stickWood", 'Y', LOTRMod.highElvenTorch, 'Z', LOTRMod.elfSteel }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenBedItem), new Object[] { "XXX", "YYY", 'X', Blocks.wool, 'Y', "plankWood" }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick3, 4, 2), new Object[] { "XX", "XX", 'X', Blocks.stone }));
        hithlumRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.brick3, 1, 3), new Object[] { new ItemStack(LOTRMod.brick3, 1, 2), "vine" }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle5, 6, 5), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick3, 4, 2) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle5, 6, 6), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick3, 4, 3) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle5, 6, 7), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick3, 4, 4) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsHighElvenBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 2) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsHighElvenBrickMossy, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 3) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsHighElvenBrickCracked, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 4) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 11), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 2) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 12), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 3) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 13), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 4) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.pillar, 3, 10), new Object[] { "X", "X", "X", 'X', Blocks.stone }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle6, 6, 0), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar, 1, 10) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle6, 6, 1), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar, 1, 11) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick2, 1, 13), new Object[] { "XX", "XX", 'X', new ItemStack(LOTRMod.brick3, 1, 2) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.elvenForge), new Object[] { "XXX", "X X", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 2) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElfBars, 16), new Object[] { "XXX", "XXX", 'X', LOTRMod.elfSteel }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElfWoodBars, 16), new Object[] { "XXX", "XXX", 'X', "plankWood" }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick4, 1, 8), new Object[] { " X ", "XYX", " X ", 'X', "nuggetSilver", 'Y', new ItemStack(LOTRMod.brick3, 1, 2) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick4, 1, 11), new Object[] { " X ", "XYX", " X ", 'X', "nuggetGold", 'Y', new ItemStack(LOTRMod.brick3, 1, 2) }));
        hithlumRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateHighElven, 4), new Object[] { "ZYZ", "YXY", "ZYZ", 'X', LOTRMod.gateGear, 'Y', "plankWood", 'Z', LOTRMod.elfSteel }));
        
    }
    
    private static void createHouseBorRecipes() {
        addArmorRecipes(houseBorRecipes, "ingotIron", LOTRFAItems.borCaptainHelmet, LOTRFAItems.borCaptainChestplate, LOTRFAItems.borCaptainLeggings, LOTRFAItems.borCaptainBoots);
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bodyDaleGambeson), new Object[] { "X X", "XXX", "XXX", 'X', Blocks.wool }));
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.borHelmet), new Object[] {"YYY", "X X", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.borChestplate), new Object[] {"X X", "YYY", "XXX", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.borLeggings), new Object[] {"YYY", "X X", "Y Y", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.borBoots), new Object[] {"Y Y", "X X", Character.valueOf('X'), Items.iron_ingot, Character.valueOf('Y'), Items.leather}));
        addMeleeWeaponRecipes(houseBorRecipes, "ingotIron", LOTRFAItems.borGreatsword, LOTRFAItems.borSword, LOTRFAItems.borDagger, LOTRFAItems.borSpear, LOTRFAItems.borBattleaxe, LOTRFAItems.borPike);
        addRangedWeaponRecipes(houseBorRecipes, "ingotIron", LOTRFAItems.borThrowingKnife);
        addRangedWeaponRecipes(houseBorRecipes, "stickWood", LOTRFAItems.borBow);
        
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableHouseBor), new Object[] { "XX", "XX", 'X', "plankWood" }));
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick, 4, 4), new Object[] { "XX", "XX", 'X', Blocks.stone}));
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle, 6, 6), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 4) }));
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsRohanBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 4) }));
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall, 6, 6), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick, 1, 4) }));
        houseBorRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.houseBor.bannerID), new Object[] { "X", "Y", "Z", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood" }));
    }
    
    private static void createHouseUlfangRecipes() {
        addArmorRecipes(houseUlfangRecipes, "ingotIron", LOTRMod.helmetGundabadUruk, LOTRMod.bodyGundabadUruk, LOTRMod.legsGundabadUruk, LOTRMod.bootsGundabadUruk);
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.helmetDunlending), new Object[] {"YYY", "X X", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bodyDunlending), new Object[] {"X X", "YYY", "XXX", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.legsDunlending), new Object[] {"YYY", "X X", "Y Y", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot}));
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bootsDunlending), new Object[] {"Y Y", "X X", Character.valueOf('X'), Items.iron_ingot, Character.valueOf('Y'), Items.leather}));
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.helmetMoredainLion), new Object[] {"XXX", "X X", Character.valueOf('X'), LOTRMod.fur}));
        addMeleeWeaponRecipes(houseUlfangRecipes, "ingotIron", LOTRFAItems.ulfangBattleaxe, LOTRFAItems.ulfangPike, LOTRFAItems.ulfangSword, LOTRFAItems.ulfangDagger);
        GameRegistry.addRecipe(new LOTRRecipePoisonWeapon(LOTRFAItems.ulfangDagger, LOTRFAItems.ulfangDaggerPoisoned, new ItemStack(LOTRMod.bottlePoison, 1, 0)));
        addRangedWeaponRecipes(houseUlfangRecipes, LOTRFAItems.ulfangThrowingAxe);
        addRangedWeaponRecipes(houseUlfangRecipes, "ingotIron", LOTRFAItems.ulfangBow);
        
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableHouseUlfang), new Object[] { "XX", "YY", 'X', "plankWood", 'Y', new ItemStack(LOTRMod.brick3, 1, 10)}));
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.houseUlfang.bannerID), new Object[] { "X", "Y", "Z", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood" }));
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick3, 4, 10), new Object[] { "XX", "XX", 'X', Blocks.stone}));
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle7, 6, 0), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick3, 4, 10) }));
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsMoredainBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick3, 4, 10) }));
        houseUlfangRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 15), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick3, 4, 10) }));
    }
    
    private static void createLaegrimRecipes() {
        addArmorRecipes(laegrimRecipes, LOTRMod.helmetWoodElven, LOTRMod.bodyWoodElven, LOTRMod.legsWoodElven, LOTRMod.bootsWoodElven);
        addArmorRecipes(laegrimRecipes, LOTRMod.helmetWoodElvenScout, LOTRMod.bodyWoodElvenScout, LOTRMod.legsWoodElvenScout, LOTRMod.bootsWoodElvenScout);
        addMeleeWeaponRecipes(laegrimRecipes, LOTRMod.swordWoodElven, LOTRMod.daggerWoodElven, LOTRMod.spearWoodElven, LOTRMod.longspearWoodElven);
        addRangedWeaponRecipes(laegrimRecipes, "stickWood", LOTRMod.mirkwoodBow);
        addToolRecipes(laegrimRecipes, LOTRMod.shovelWoodElven, LOTRMod.pickaxeWoodElven, LOTRMod.axeWoodElven, LOTRMod.hoeWoodElven);
        
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableLaegrim), new Object[] { "XX", "XX", 'X', "plankWood" }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.woodElvenBedItem), new Object[] { "XXX", "YYY", 'X', Blocks.wool, 'Y', "plankWood" }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.woodElvenTorch, 4), new Object[] { "X", "Y", "Z", 'X', new ItemStack(LOTRMod.leaves, 1, 3), 'Y', new ItemStack(Items.coal, 1, 32767), 'Z', "stickWood" }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 6), new Object[] { " X ", "YZY", 'X', "stickWood", 'Y', LOTRMod.woodElvenTorch, 'Z', "plankWood" }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.laegrim.bannerID), new Object[] { "X", "Y", "Z", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood" }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick3, 4, 5), new Object[] { "XX", "XX", 'X', Blocks.stone }));
        laegrimRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.brick3, 1, 6), new Object[] { new ItemStack(LOTRMod.brick3, 1, 5), "vine" }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle6, 6, 2), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick3, 4, 5) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle6, 6, 3), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick3, 4, 6) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle6, 6, 4), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick3, 4, 7) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsWoodElvenBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 5) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsWoodElvenBrickMossy, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 6) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsWoodElvenBrickCracked, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 7) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall3, 6, 0), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 5) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall3, 6, 1), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 6) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall3, 6, 2), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 7) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.pillar, 3, 12), new Object[] { "X", "X", "X", 'X', Blocks.stone }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle6, 6, 5), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar, 1, 12) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle6, 6, 6), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar, 1, 13) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick2, 1, 14), new Object[] { "XX", "XX", 'X', new ItemStack(LOTRMod.brick3, 1, 5) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.elvenForge), new Object[] { "XXX", "X X", "XXX", 'X', new ItemStack(LOTRMod.brick3, 1, 5) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.woodElfBars, 16), new Object[] { "XXX", "XXX", 'X', LOTRMod.elfSteel }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.woodElfWoodBars, 16), new Object[] { "XXX", "XXX", 'X', "plankWood" }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.polearmWoodElven), new Object[] { "  X", " Y ", "X  ", 'X', LOTRMod.elfSteel, 'Y', "stickWood" }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick4, 1, 10), new Object[] { " X ", "XYX", " X ", 'X', "nuggetSilver", 'Y', new ItemStack(LOTRMod.brick3, 1, 5) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick4, 1, 13), new Object[] { " X ", "XYX", " X ", 'X', "nuggetGold", 'Y', new ItemStack(LOTRMod.brick3, 1, 5) }));
        laegrimRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateWoodElven, 4), new Object[] { "ZYZ", "YXY", "ZYZ", 'X', LOTRMod.gateGear, 'Y', "plankWood", 'Z', LOTRMod.elfSteel }));
    }
    
    private static void createNargothrondRecipes() {
        addArmorRecipes(nargothrondRecipes, LOTRFAItems.nargothrondRangerHelmet, LOTRFAItems.nargothrondRangerChestplate, LOTRFAItems.nargothrondRangerLeggings, LOTRFAItems.nargothrondRangerBoots);
        addArmorRecipes(nargothrondRecipes, LOTRMod.helmetRivendell, LOTRMod.bodyRivendell, LOTRMod.legsRivendell, LOTRMod.bootsRivendell);
        addMeleeWeaponRecipes(nargothrondRecipes, LOTRMod.swordRivendell, LOTRMod.daggerRivendell, LOTRMod.spearRivendell, LOTRMod.polearmRivendell, LOTRMod.longspearRivendell);
        GameRegistry.addRecipe(new LOTRRecipePoisonWeapon(LOTRMod.daggerRivendell, LOTRMod.daggerRivendellPoisoned, new ItemStack(LOTRMod.bottlePoison, 1, 0)));
        addRangedWeaponRecipes(nargothrondRecipes, LOTRMod.rivendellBow);
        addToolRecipes(nargothrondRecipes, LOTRMod.shovelRivendell, LOTRMod.pickaxeRivendell, LOTRMod.axeRivendell, LOTRMod.hoeRivendell);
        
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.horseArmorRivendell), new Object[] {"X  ", "XYX", "XXX", Character.valueOf('X'), LOTRMod.elfSteel, Character.valueOf('Y'), Items.leather}));

        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.nargothrondTorch), new Object[] {"X", "Y", Character.valueOf('X'), Items.coal, Character.valueOf('Y'), Items.stick}));

        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTableNargothrond), new Object[] {"XX", "XX", Character.valueOf('X'), "plankWood"}));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.nargothrond.bannerID), new Object[] {"X", "Y", "Z", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "stickWood", Character.valueOf('Z'), "plankWood"}));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenTorch, 4), new Object[] {"X", "Y", Character.valueOf('X'), LOTRMod.quenditeCrystal, Character.valueOf('Y'), "stickWood"}));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 10), new Object[] {" X ", "YZY", Character.valueOf('X'), "stickWood", Character.valueOf('Y'), LOTRMod.highElvenTorch, Character.valueOf('Z'), LOTRMod.elfSteel}));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElvenBedItem), new Object[] {"XXX", "YYY", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "plankWood"}));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick5, 4, 2), new Object[] { "XX", "XX", 'X', new ItemStack(Blocks.stone) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle9, 6, 7), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 2) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDorwinionBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 2) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall3, 6, 10), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 2) }));
        nargothrondRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.brick5, 1, 4), new Object[] { new ItemStack(LOTRMod.brick5, 1, 2), "vine" }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle10, 6, 1), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 4) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDorwinionBrickMossy, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 4) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall3, 6, 11), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 4) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle10, 6, 2), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 5) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDorwinionBrickCracked, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 5) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall3, 6, 12), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 5) }));
        //nargothrondRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.brick5, 1, 7), new Object[] { new ItemStack(LOTRMod.brick5, 1, 2), new ItemStack(LOTRMod.rhunFlower, 1, 2) }));
        //nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle10, 6, 3), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 7) }));
        //nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDorwinionBrickFlowers, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 7) }));
        //nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall3, 6, 13), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick5, 1, 7) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick5, 1, 6), new Object[] { "XX", "XX", 'X', new ItemStack(LOTRMod.brick5, 1, 2) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.pillar2, 3, 6), new Object[] { "X", "X", "X", 'X', new ItemStack(Blocks.stone) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle10, 6, 4), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar2, 1, 6) }));
        nargothrondRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.pillar2, 1, 7), new Object[] { new ItemStack(LOTRMod.pillar2, 1, 6), "vine" }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle10, 6, 5), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.pillar2, 1, 7) }));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.elvenForge), new Object[] {"XXX", "X X", "XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick5, 1, 2)}));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElfBars, 16), new Object[] {"XXX", "XXX", Character.valueOf('X'), LOTRMod.elfSteel}));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.highElfWoodBars, 16), new Object[] {"XXX", "XXX", Character.valueOf('X'), "plankWood"}));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateHighElven, 4), new Object[] {"ZYZ", "YXY", "ZYZ", Character.valueOf('X'), LOTRMod.gateGear, Character.valueOf('Y'), "plankWood", Character.valueOf('Z'), LOTRMod.elfSteel}));   
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle4, 6, 1), new Object[] { "XXX", 'X', new ItemStack(LOTRFABlocks.pillar, 1, 12) }));
        
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 1, 15), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetSilver", Character.valueOf('Y'), new ItemStack(LOTRMod.brick5, 1, 2)}));
        nargothrondRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 1, 14), new Object[] {" X ", "XYX", " X ", Character.valueOf('X'), "nuggetGold", Character.valueOf('Y'), new ItemStack(LOTRMod.brick5, 1, 2)}));
    }
    
    private static void createPettyDwarvenRecipes() {
        addArmorRecipes(pettyDwarvenRecipes, LOTRFAItems.pettyDwarfHelmet, LOTRFAItems.pettyDwarfChestplate, LOTRFAItems.pettyDwarfLeggings, LOTRFAItems.pettyDwarfBoots);
        addMeleeWeaponRecipes(pettyDwarvenRecipes, LOTRFAItems.pettyDwarfSword, LOTRFAItems.pettyDwarfDagger, LOTRFAItems.pettyDwarfBattleaxe, LOTRFAItems.pettyDwarfWarhammer, LOTRFAItems.pettyDwarfPike, LOTRFAItems.pettyDwarfSpear);
        GameRegistry.addRecipe(new LOTRRecipePoisonWeapon(LOTRFAItems.pettyDwarfDagger, LOTRFAItems.pettyDwarfDaggerPoisoned));
        addRangedWeaponRecipes(pettyDwarvenRecipes, LOTRFAItems.pettyDwarfThrowingAxe);
        addToolRecipes(pettyDwarvenRecipes, LOTRFAItems.pettyDwarfShovel, LOTRFAItems.pettyDwarfPickaxe, LOTRFAItems.pettyDwarfAxe, LOTRFAItems.pettyDwarfHoe, LOTRFAItems.pettyDwarfMattock);
        
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.pettyDwarfBoarArmor), new Object[] {"X  ", "XYX", "XXX", Character.valueOf('X'), LOTRFAItems.pettyDwarfSteel, Character.valueOf('Y'), Items.leather}));
        
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.craftingTablePettyDwarf), new Object[] {"XX", "YY", Character.valueOf('X'), "plankWood", Character.valueOf('Y'), new ItemStack(LOTRFABlocks.brick, 1, 0)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 8), new Object[] {" X ", "YZY", Character.valueOf('X'), "stickWood", Character.valueOf('Y'), Blocks.torch, Character.valueOf('Z'), LOTRMod.dwarfSteel}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.dwarfBars, 16), new Object[] {"XXX", "XXX", Character.valueOf('X'), LOTRMod.dwarfSteel}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateDwarven, 4), new Object[] {"ZYZ", "YXY", "ZYZ", Character.valueOf('X'), LOTRMod.gateGear, Character.valueOf('Y'), new ItemStack(LOTRFABlocks.brick, 1, 0), Character.valueOf('Z'), LOTRMod.dwarfSteel}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDwarvenBrick, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick, 1, 6)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle, 6, 7), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick, 1, 6)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.forgePettyDwarf), new Object[] {"XXX", "X X", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 0)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.dwarvenDoor), new Object[] {"XX", "XX", "XX", Character.valueOf('X'), Blocks.stone}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.dwarvenDoorIthildin), new Object[] {"XX", "XY", "XX", Character.valueOf('X'), Blocks.stone, Character.valueOf('Y'), new ItemStack(LOTRMod.ithildin, 1, 0)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.dwarvenBedItem), new Object[] {"XXX", "YYY", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "plankWood"}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDwarvenBrickCracked, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRMod.brick4, 1, 5)}));
        pettyDwarvenRecipes.add(new LOTRRecipePoisonWeapon(LOTRMod.chisel, LOTRMod.chiselIthildin, new ItemStack(LOTRMod.ithildin, 1, 0)));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.pettyDwarves.bannerID), new Object[] {"X", "Y", "Z", Character.valueOf('X'), Blocks.wool, Character.valueOf('Y'), "stickWood", Character.valueOf('Z'), "plankWood"}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle, 6, 0), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 0)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle, 6, 2), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 1)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle, 6, 1), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.pillar, 1, 0)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle, 6, 3), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.pillar, 1, 1)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.brick, 4, 0), new Object[] {"XX", "XX", Character.valueOf('X'), new ItemStack(Blocks.stone, 1, 0)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 0), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 0)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsPettyDwarf, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 0)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsPettyDwarfCracked, 4), new Object[] {"X  ", "XX ", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 1)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 1), new Object[] {"XXX", "XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 1)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.pillar, 3, 4), new Object[] {"X", "X", "X", Character.valueOf('X'), new ItemStack(Blocks.stone, 1, 4)}));
        pettyDwarvenRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.carvedBrickPettyDwarf, 1, 0), new Object[] {"XX", "XX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 0)}));
    }
    
    private static void createTolInGaurhothRecipes() {
        addArmorRecipes(tolInGaurhothRecipes, LOTRMod.helmetDolGuldur, LOTRMod.bodyDolGuldur, LOTRMod.legsDolGuldur, LOTRMod.bootsDolGuldur);
        addMeleeWeaponRecipes(tolInGaurhothRecipes, LOTRMod.swordDolGuldur, LOTRMod.battleaxeDolGuldur, LOTRMod.daggerDolGuldur, LOTRMod.spearDolGuldur, LOTRMod.hammerDolGuldur, LOTRMod.pikeDolGuldur, LOTRFAItems.tolInGaurhothCleaver);
        addToolRecipes(tolInGaurhothRecipes, LOTRMod.shovelDolGuldur, LOTRMod.pickaxeDolGuldur, LOTRMod.axeDolGuldur, LOTRMod.hoeDolGuldur);
        
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcSkullStaff), new Object[] { "X", "Y", "Y", 'X', Items.skull, 'Y', "stickWood" }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcBedItem), new Object[] { "XXX", "YYY", 'X', Blocks.wool, 'Y', "plankWood" }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.maggotyBread), new Object[] { "XXX", 'X', Items.wheat }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcSteelBars, 16), new Object[] { "XXX", "XXX", 'X', LOTRMod.orcSteel }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcBow), new Object[] { " XY", "X Y", " XY", 'X', LOTRMod.orcSteel, 'Y', Items.string }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcBomb, 4), new Object[] { "XYX", "YXY", "XYX", 'X', Items.gunpowder, 'Y', LOTRMod.orcSteel }));
        tolInGaurhothRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.orcBomb, 1, 1), new Object[] { new ItemStack(LOTRMod.orcBomb, 1, 0), Items.gunpowder, LOTRMod.orcSteel }));
        tolInGaurhothRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRMod.orcBomb, 1, 2), new Object[] { new ItemStack(LOTRMod.orcBomb, 1, 1), Items.gunpowder, LOTRMod.orcSteel }));
        
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wargArmorAngmar), new Object[] { "X  ", "XYX", "XXX", 'X', LOTRMod.orcSteel, 'Y', Items.leather }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.brick2, 4, 8), new Object[] { "XX", "XX", 'X', Blocks.stone }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.dolGuldurTable), new Object[] { "XX", "YY", 'X', "plankWood", 'Y', new ItemStack(LOTRMod.brick2, 1, 8) }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle4, 6, 5), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 8) }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDolGuldurBrick, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 8) }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 8), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 8) }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcTorchItem, 2), new Object[] { "X", "Y", "Y", 'X', new ItemStack(Items.coal, 1, 32767), 'Y', "stickWood" }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.slabSingle4, 6, 6), new Object[] { "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 9) }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.stairsDolGuldurBrickCracked, 4), new Object[] { "X  ", "XX ", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 9) }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.wall2, 6, 9), new Object[] { "XXX", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 9) }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.orcForge), new Object[] { "XXX", "X X", "XXX", 'X', new ItemStack(LOTRMod.brick2, 1, 8) }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFAItems.banner, 1, LOTRFAItemBanner.FABannerTypes.tolInGaurhoth.bannerID), new Object[] { "X", "Y", "Z", 'X', Blocks.wool, 'Y', "stickWood", 'Z', "plankWood" }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.chandelier, 2, 7), new Object[] { " X ", "YZY", 'X', "stickWood", 'Y', LOTRMod.orcTorchItem, 'Z', LOTRMod.orcSteel }));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.gateOrc, 4), new Object[] { "YYY", "YXY", "YYY", 'X', LOTRMod.gateGear, 'Y', LOTRMod.orcSteel }));
        tolInGaurhothRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRFABlocks.brick, 1, 3), new Object[] {new ItemStack(LOTRMod.brick2, 1, 8), "vine"}));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.carvedBrickTolInGaurhoth, 1), new Object[] {"XX", "XX", 'X', new ItemStack(LOTRMod.brick2, 1, 8)}));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.pillar, 3, 3), new Object[] {"X", "X", "X", 'X', Blocks.stone}));
        tolInGaurhothRecipes.add(new ShapelessOreRecipe(new ItemStack(LOTRFABlocks.pillar, 1, 5), new Object[] {new ItemStack(LOTRFABlocks.pillar, 1, 3), "vine"}));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle, 6, 6), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.pillar, 1, 3)}));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.slabSingle, 6, 7), new Object[] {"XXX", Character.valueOf('X'), new ItemStack(LOTRFABlocks.brick, 1, 3)}));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.wall, 6, 3), new Object[] {"XXX", "XXX", 'X', new ItemStack(LOTRFABlocks.brick, 1, 3)}));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRFABlocks.stairsTolInGaurhothMossy, 4), new Object[] {"X  ", "XX ", "XXX", 'X', new ItemStack(LOTRFABlocks.brick, 1, 3)}));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.rhunFireJar), new Object[] {"XYX", "YZY", "XYX", Character.valueOf('X'), LOTRMod.orcSteel, Character.valueOf('Y'), Items.gunpowder, Character.valueOf('Z'), LOTRMod.nauriteGem}));
        tolInGaurhothRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.rhunFirePot, 4), new Object[] {"Z", "Y", "X", Character.valueOf('X'), LOTRMod.orcSteel, Character.valueOf('Y'), Items.gunpowder, Character.valueOf('Z'), LOTRMod.nauriteGem}));
    }
    
    private static void createMiscFactionRecipes() {
        GameRegistry.addRecipe(new LOTRRecipePoisonWeapon(LOTRFAItems.houseBeorDagger, LOTRFAItems.houseBeorDaggerPoisoned));
        
        GameRegistry.addRecipe(new ItemStack(LOTRFABlocks.slabSingle3, 6, 0), new Object[] { "XXX", 'X', LOTRFABlocks.rockSarllith});
        GameRegistry.addRecipe(new ItemStack(LOTRFABlocks.wall, 6, 6), new Object[] { "XXX", "XXX", 'X', LOTRFABlocks.rockSarllith});
        GameRegistry.addRecipe(new ItemStack(LOTRFABlocks.stairsSarllith, 4), new Object[] { "X  ", "XX ", "XXX", 'X', LOTRFABlocks.rockSarllith});

        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.helmetDwarvenSilver), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), "nuggetSilver", Character.valueOf('Y'), LOTRMod.helmetBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bodyDwarvenSilver), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), "nuggetSilver", Character.valueOf('Y'), LOTRMod.bodyBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.legsDwarvenSilver), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), "nuggetSilver", Character.valueOf('Y'), LOTRMod.legsBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bootsDwarvenSilver), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), "nuggetSilver", Character.valueOf('Y'), LOTRMod.bootsBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.helmetDwarvenGold), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), "nuggetGold", Character.valueOf('Y'), LOTRMod.helmetBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bodyDwarvenGold), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), "nuggetGold", Character.valueOf('Y'), LOTRMod.bodyBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.legsDwarvenGold), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), "nuggetGold", Character.valueOf('Y'), LOTRMod.legsBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bootsDwarvenGold), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), "nuggetGold", Character.valueOf('Y'), LOTRMod.bootsBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.helmetDwarvenMithril), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), LOTRMod.mithrilNugget, Character.valueOf('Y'), LOTRMod.helmetBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bodyDwarvenMithril), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), LOTRMod.mithrilNugget, Character.valueOf('Y'), LOTRMod.bodyBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.legsDwarvenMithril), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), LOTRMod.mithrilNugget, Character.valueOf('Y'), LOTRMod.legsBlueDwarven}));
        LOTRRecipes.blueMountainsRecipes.add(new ShapedOreRecipe(new ItemStack(LOTRMod.bootsDwarvenMithril), new Object[] {"XXX", "XYX", "XXX", Character.valueOf('X'), LOTRMod.mithrilNugget, Character.valueOf('Y'), LOTRMod.bootsBlueDwarven}));
    }

    
    private static void createCommonOrcRecipes() {
        
    }
    
    private static void createCommonDwarfRecipes() {
   
    }
    
    private static void createCommonMannishRecipes() {
        
    }
    
    
    
    private static void addToolRecipes(List<IRecipe> recipeList, Item... tools) {
        addToolRecipes(recipeList, null, tools);
    }
    
    private static void addToolRecipes(List<IRecipe> recipeList, Object material, Item... tools) {
        addToolRecipes(recipeList, "stickWood", material, tools);
    }
    
    private static void addToolRecipes(List<IRecipe> recipeList, Object handle, Object material, Item... tools) {
        for(Item item : tools) {
            if(!(item instanceof ItemTool || item instanceof ItemHoe)) {
                LOTRFA.logger.error("Tied to create weapon recipe for item " + item.getUnlocalizedName() + " but this item isn't instance of ItemTool or ItemHoe.");
                continue;
            }
            
            if(material == null) {
                if(item instanceof ItemTool) material = ((ItemTool) item).func_150913_i().getRepairItemStack();
                else material = ToolMaterial.valueOf(((ItemHoe) item).getToolMaterialName()).getRepairItemStack();
            }
            
            addToolRecipe(recipeList, handle, material, item);
        }
    }

    private static void addToolRecipe(List<IRecipe> recipeList, Object handle, Object material, Item tool) {
        if(!isValidObject(handle) || !isValidObject(material)) {
            LOTRFA.logger.error("Tied to create tool recipe for item " + tool.getUnlocalizedName() + " but the material or handle isn't valid.");
            return;
        }
            
        if(tool instanceof LOTRItemMattock) recipeList.add(new ShapedOreRecipe(tool, new Object[] {"XXX", "XY ", " Y ", 'X', material, 'Y', handle}));
        else if(tool instanceof ItemPickaxe) recipeList.add(new ShapedOreRecipe(tool, new Object[] {"XXX", " Y ", " Y ", 'X', material, 'Y', handle}));
        else if(tool instanceof ItemAxe) recipeList.add(new ShapedOreRecipe(tool, new Object[] {"XX", "XY", " Y", 'X', material, 'Y', handle}));
        else if(tool instanceof ItemSpade) recipeList.add(new ShapedOreRecipe(tool, new Object[] {"X", "Y", "Y", 'X', material, 'Y', handle}));
        else if(tool instanceof ItemHoe) recipeList.add(new ShapedOreRecipe(tool, new Object[] {"XX", " Y", " Y", 'X', material, 'Y', handle}));
        else LOTRFA.logger.error("Tied to create tool recipe for item " + tool.getUnlocalizedName() + " but the tool type wasn't found.");
    }
    
    private static void addArmorRecipes(List<IRecipe> recipeList, Item... armors) {
        addArmorRecipes(recipeList, null, armors);
    }
    
    private static void addArmorRecipes(List<IRecipe> recipeList, Object material, Item... armors) {
        for(Item item : armors) {
            if(!(item instanceof ItemArmor)) {
                LOTRFA.logger.error("Tied to create armor recipe for item " + item.getUnlocalizedName() + " but this item isn't instance of ItemArmor.");
                continue;
            }
            
            ItemArmor armor = (ItemArmor) item;
            
            if(material == null) material = new ItemStack(armor.getArmorMaterial().customCraftingMaterial);
            
            addArmorRecipe(recipeList, material, armor);
        }
    }
    
    private static void addArmorRecipe(List<IRecipe> recipeList, Object material, ItemArmor armor) {
        if(!isValidObject(material)) {
            LOTRFA.logger.error("Tied to create armor recipe for item " + armor.getUnlocalizedName() + " but the material isn't valid.");
            return;
        }
        
        int type = armor.armorType;

        if(type == 0) recipeList.add(new ShapedOreRecipe(armor, new Object[] {"XXX", "X X", 'X', material,}));
        else if(type == 1) recipeList.add(new ShapedOreRecipe(armor, new Object[] {"X X", "XXX", "XXX", 'X', material}));
        else if(type == 2) recipeList.add(new ShapedOreRecipe(armor, new Object[] {"XXX", "X X", "X X", 'X', material}));
        else if(type == 3) recipeList.add(new ShapedOreRecipe(armor, new Object[] {"X X", "X X", 'X', material}));
    }    

    private static void addMeleeWeaponRecipes(List<IRecipe> recipeList, Item... weapons) {
        addMeleeWeaponRecipes(recipeList, null, weapons);
    }
    
    private static void addMeleeWeaponRecipes(List<IRecipe> recipeList, Object material, Item... weapons) {
        addMeleeWeaponRecipes(recipeList, "stickWood", material, weapons);
    }
    
    private static void addMeleeWeaponRecipes(List<IRecipe> recipeList, Object handle, Object material, Item... weapons) {
        for(Item item : weapons) {
            if(!(item instanceof ItemSword)) {
                LOTRFA.logger.error("Tied to create melee weapon recipe for item " + item.getUnlocalizedName() + " but this item isn't instance of ItemSword.");
                continue;
            }
            ItemSword weapon = (ItemSword) item;
            
            if(material == null) material = ToolMaterial.valueOf(weapon.getToolMaterialName()).getRepairItemStack();
            
            addMeleeWeaponRecipe(recipeList, handle, material, weapon);
        }
    }
    
    private static void addMeleeWeaponRecipe(List<IRecipe> recipeList, Object handle, Object material, ItemSword weapon) {
        if(!isValidObject(handle) || !isValidObject(material)) {
            LOTRFA.logger.error("Tied to create melee weapon recipe for item " + weapon.getUnlocalizedName() + " but the material or handle isn't valid.");
            return;
        }
            
        if(weapon instanceof LOTRItemLance) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"  X", " X ", "Y  ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRItemPike) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"  X", " YX", "Y  ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRItemPolearmLong) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"  X", " YX", "Y  ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRItemPolearm) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {" XX", " YX", "Y  ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRItemSpear) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"  X", " Y ", "Y  ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRItemBattleaxe) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"XXX", "XYX", " Y ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRItemHammer) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"XYX", "XYX", " Y ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRFAItemGreatsword) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"  X", " X ", "Y  ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRFAItemCleaver) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"  X", " X ", "Y  ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRItemDagger && ((LOTRItemDagger) weapon).getDaggerEffect() == DaggerEffect.NONE) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"X", "Y", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRItemSword) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"X", "X", "Y", 'X', material, 'Y', handle}));
        else LOTRFA.logger.error("Tied to create melee weapon recipe for item " + weapon.getUnlocalizedName() + " but the weapon type wasn't found.");
    }
    
    private static void addRangedWeaponRecipes(List<IRecipe> recipeList, Item... weapons) {
        addRangedWeaponRecipes(recipeList, null, weapons);
    }
    
    private static void addRangedWeaponRecipes(List<IRecipe> recipeList, Object material, Item... weapons) {
        addRangedWeaponRecipes(recipeList, "stickWood", material, weapons);
    }

    private static void addRangedWeaponRecipes(List<IRecipe> recipeList, Object handle, Object material, Item... weapons) {
        for(Item weapon : weapons) {
            if(!(weapon instanceof ItemBow || weapon instanceof LOTRItemCrossbow || weapon instanceof LOTRItemThrowingAxe)) {
                LOTRFA.logger.error("Tied to create ranged weapon recipe for item " + weapon.getUnlocalizedName() + " but this item isn't instance of a ranged weapon.");
                continue;
            }
            
            if(material == null) {
                if(weapon instanceof LOTRItemBow) material  = LOTRFAReflectionHelper.getBowMaterial((LOTRItemBow) weapon).getRepairItemStack();
                else if(weapon instanceof LOTRItemCrossbow) material  = ((LOTRItemCrossbow) weapon).getCrossbowMaterial().getRepairItemStack();
                else if(weapon instanceof LOTRItemThrowingAxe) material  = ((LOTRItemThrowingAxe) weapon).getAxeMaterial().getRepairItemStack();
            }
            
            addRangedWeaponRecipe(recipeList, handle, material, weapon);
        }
    }
    
    private static void addRangedWeaponRecipe(List<IRecipe> recipeList, Object handle, Object material, Item weapon) {
        if(!isValidObject(handle) || !isValidObject(material)) {
            LOTRFA.logger.error("Tied to create ranged weapon recipe for item " + weapon.getUnlocalizedName() + " but the material or handle isn't valid.");
            return;
        }
           
        if(weapon instanceof LOTRItemBow) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {" XY", "X Y", " XY", 'X', material, 'Y', Items.string}));
        else if(weapon instanceof LOTRItemCrossbow) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {"XXY", "ZYX", "YZX", 'X', material, 'Y', handle, 'Z', Items.string}));
        else if(weapon instanceof LOTRFAItemThrowingKnife) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {" X", "Y ", 'X', material, 'Y', handle}));
        else if(weapon instanceof LOTRItemThrowingAxe) recipeList.add(new ShapedOreRecipe(weapon, new Object[] {" X ", " YX", "Y  ", 'X', material, 'Y', handle}));
        else LOTRFA.logger.error("Tied to create ranged weapon recipe for item " + weapon.getUnlocalizedName() + " but the weapon type wasn't found.");
    }

    private static boolean isValidObject(Object obj) {
        if(obj != null && (obj instanceof Item || obj instanceof ItemStack || obj instanceof Block || isOreDictString(obj))) {
            return true;
        }
        
        LOTRFA.logger.error("Tied to use " + obj.toString() + " as recipe part but it isn't valid.");
        return false;
    }
    
    private static boolean isOreDictString(Object obj) {
        if(obj instanceof String && OreDictionary.doesOreNameExist((String) obj)) return true;
        
        return false;
    }
}
