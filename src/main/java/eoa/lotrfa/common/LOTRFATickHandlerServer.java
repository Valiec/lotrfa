package eoa.lotrfa.common;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.PlayerTickEvent;
import eoa.lotrfa.common.world.biome.*;
import lotr.common.LOTRLevelData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.MinecraftForge;

public class LOTRFATickHandlerServer {

    public LOTRFATickHandlerServer() {
        MinecraftForge.EVENT_BUS.register(this);
        FMLCommonHandler.instance().bus().register(this);
    }
    
    
    @SubscribeEvent
    public void onPlayerTick(PlayerTickEvent event) {
        EntityPlayer player = event.player;
        World world = player.worldObj;
        
        if(world.isRemote || !player.isEntityAlive() || event.phase != Phase.END) return;
        
        LOTRFAAchievements.runAchievementCheck(player);

        if (player.ticksExisted % 10 == 0 && !player.capabilities.isCreativeMode) {
            //TODO make helper method for biome gen at entity
            BiomeGenBase biomeGen = world.getBiomeGenForCoords((int) player.posX, (int) player.posZ);
            
            if(player.isInWater() && player.ridingEntity == null) {
                //TODO make LOTRFABiome and have it have water potion effects
                if (biomeGen instanceof LOTRBiomeGenIvrin) {
                    player.addPotionEffect(new PotionEffect(Potion.regeneration.id, 200, 1));
                }
                else if(biomeGen instanceof LOTRBiomeGenIvrin) {
                    player.addPotionEffect(new PotionEffect(Potion.regeneration.id, 200, 1));
                }
                else if(biomeGen instanceof LOTRBiomeGenTaurNuFuin) {
                    player.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 600, 1));
                    player.addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 600, 1));
                    player.addPotionEffect(new PotionEffect(Potion.weakness.id, 600));
                    player.addPotionEffect(new PotionEffect(Potion.blindness.id, 600));
                }
                else if(biomeGen instanceof LOTRBiomeGenTolInGaurhoth && world.rand.nextInt(100) > LOTRLevelData.getData(player).getAlignment(LOTRFAFactions.angband)) {
                    player.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 600, 1));
                    player.addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 600, 1));
                    player.addPotionEffect(new PotionEffect(Potion.weakness.id, 600));
                    player.addPotionEffect(new PotionEffect(Potion.poison.id, 100));
                }
            }

            int doriathAlignment = LOTRLevelData.getData(player).getAlignment(LOTRFAFactions.doriath);
            if (biomeGen instanceof LOTRBiomeGenGirdleMelian && doriathAlignment < 50) {
                player.addPotionEffect(new PotionEffect(Potion.blindness.id, 200, 1));
                player.addPotionEffect(new PotionEffect(Potion.confusion.id, 200, 0));
                player.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 200, 1));
            }
            else if (biomeGen instanceof LOTRBiomeGenAelinUial && doriathAlignment < 25) {
                player.addPotionEffect(new PotionEffect(Potion.blindness.id, 200, 1));
            }
        }
    }
}
