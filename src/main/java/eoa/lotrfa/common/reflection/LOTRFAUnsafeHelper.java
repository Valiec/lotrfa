package eoa.lotrfa.common.reflection;

import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.lotrfa.common.LOTRFA;
import eoa.lotrfa.common.world.map.LOTRFARoadType;
import lotr.common.world.map.LOTRRoadType;
import sun.misc.Unsafe;

public class LOTRFAUnsafeHelper {
    private static Unsafe unsafe;
    
    private static void initUnsafe() {
        if(unsafe == null) unsafe = ReflectionHelper.getPrivateValue(Unsafe.class, null, "theUnsafe");
    }
    
    private static long getNativeTypeLong(Class c){
        try {
            Object object = unsafe.allocateInstance(c);
            
            return getNativeTypeLong(object);
        } catch (InstantiationException e) {
            LOTRFA.logger.error("Was unable to get the native type long from class " + c.getName());
            e.printStackTrace();
        }
        return 0L;
    }
    
    private static int getNativeTypeInt(Class c){
        try {
            Object object = unsafe.allocateInstance(c);
            
            return getNativeTypeInt(object);
        } catch (InstantiationException e) {
            LOTRFA.logger.error("Was unable to get the native type int from class " + c.getName());
            e.printStackTrace();
        }
        return 0;
    }
    
    private static long getNativeTypeLong(Object object) {
        return unsafe.getLong(object, 8L);
    }
    
    private static int getNativeTypeInt(Object object) {
        return unsafe.getInt(object, 4L);
    }
    
    @SuppressWarnings("unchecked")
    private static <T> T changeNativeTypeLong(Object target, long nativeType) {
        unsafe.putLong(target, 8L, nativeType);
        return (T) target;
    }

    @SuppressWarnings("unchecked")
    private static <T> T changeNativeTypeInt(Object target, int nativeType) {
        unsafe.putInt(target, 4L, nativeType);
        return (T) target;
    }
    
    private static <T> T changeObjectType(Object object, Class newType) {
        initUnsafe();
        
        int size = unsafe.addressSize();
        
        if(size == 4) {
            return changeNativeTypeInt(object, getNativeTypeInt(newType));
        }
        else {
            return changeNativeTypeLong(object, getNativeTypeLong(newType));
        }
    }
    
    private static <T> T changeObjectType(Object object, Object newType) {
        initUnsafe();
        
        int size = unsafe.addressSize();
        
        if(size == 4) {
            return changeNativeTypeInt(object, getNativeTypeInt(newType));
        }
        else {
            return changeNativeTypeLong(object, getNativeTypeLong(newType));
        }
    }
    
    public static class RoadType {
        public static <T> T changeToRoadType(LOTRRoadType road, LOTRFARoadType type) {            
            return changeObjectType(road, type);
        }
    }
}
