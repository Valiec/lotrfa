package eoa.lotrfa.common.reflection;

import java.util.EnumSet;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.*;
import lotr.common.LOTRAchievement.Category;
import lotr.common.LOTRDimension.DimensionRegion;
import lotr.common.LOTRFaction.FactionMapInfo;
import lotr.common.LOTRFaction.FactionType;
import lotr.common.LOTRLore.LoreCategory;
import lotr.common.LOTRShields.ShieldType;
import lotr.common.item.LOTRItemBanner.BannerType;
import lotr.common.quest.LOTRMiniQuestFactory;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.map.LOTRMountains;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.map.LOTRWaypoint.Region;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraftforge.common.util.EnumHelper;

public class LOTRFAEnumHelper {

    public static LOTRMountains addMountain(String name, float x, float z, float h, int r) {
        return addMountain(name, x, z, h, r, 0);
    }

    public static LOTRMountains addMountain(String name, float x, float z, float h, int r, int lava) {
        Class<?>[] classArr = {float.class, float.class, float.class, int.class, int.class};
        Object[] args = {x, z, h, r, lava};

        return EnumHelper.addEnum(LOTRMountains.class, name, classArr, args);
    }

    public static LOTRWaypoint addWaypoint(String name, Region region, LOTRFaction faction, int x, int z) {
        return addWaypoint(name, region, faction, x, z, false);
    }

    public static LOTRWaypoint addWaypoint(String name, Region region, LOTRFaction faction, int x, int z, boolean hidden) {
        Class<?>[] classArr = {LOTRWaypoint.Region.class, LOTRFaction.class, int.class, int.class, boolean.class};
        Object[] args = {region, faction, x, z, hidden};

        return EnumHelper.addEnum(LOTRWaypoint.class, name, classArr, args);
    }

    public static Region addWaypointRegion(String name) {
        Class<?>[] classArr = {};
        Object[] args = {};

        return EnumHelper.addEnum(Region.class, name, classArr, args);
    }

    public static BannerType addBannerType(String enumName, int id, String bannerName, LOTRFaction faction) {
        Class<?>[] classArr = {int.class, String.class, LOTRFaction.class};
        Object[] args = {id, bannerName, faction};

        BannerType banner = EnumHelper.addEnum(BannerType.class, enumName, classArr, args);
        LOTRFAReflectionHelper.addBannerToIDMap(banner);
        return banner;
    }

    public static LOTRFaction addFaction(String enumName, int color, DimensionRegion region, EnumSet<FactionType> types) {
        return addFaction(enumName, color, LOTRDimension.MIDDLE_EARTH, region, true, true, Integer.MIN_VALUE, null, types);
    }

    public static LOTRFaction addFaction(String enumName, int color, LOTRDimension dim, LOTRDimension.DimensionRegion region, boolean player, boolean registry, int alignment, FactionMapInfo mapInfo, EnumSet<FactionType> types) {
        Class<?>[] classArr = {int.class, LOTRDimension.class, DimensionRegion.class, boolean.class, boolean.class, int.class, FactionMapInfo.class, EnumSet.class};
        Object[] args = {color, dim, region, player, registry, alignment, mapInfo, types};

        return EnumHelper.addEnum(LOTRFaction.class, enumName, classArr, args);
    }

    public static DimensionRegion addDimensionRegion(String enumName, String regionName) {
        Class<?>[] classArr = {String.class};
        Object[] args = {regionName};

        return EnumHelper.addEnum(DimensionRegion.class, enumName, classArr, args);
    }

    public static Category addAchievementCategory(String enumName, LOTRFaction faction) {
        Class<?>[] classArr = {LOTRFaction.class};
        Object[] args = {faction};

        return EnumHelper.addEnum(Category.class, enumName, classArr, args);
    }

    public static Category addAchievementCategory(String enumName, LOTRBiome biome) {
        Class<?>[] classArr = {LOTRBiome.class};
        Object[] args = {biome};

        return EnumHelper.addEnum(Category.class, enumName, classArr, args);
    }

    public static LOTRMapLabels addMapLabel(String enumName, LOTRBiome biomeLabel, int x, int y, float scale, int angle, float zoomMin, float zoomMan) {
        return addMapLabel(enumName, (Object) biomeLabel, x, y, scale, angle, zoomMin, zoomMan);
    }

    public static LOTRMapLabels addMapLabel(String enumName, String stringLabel, int x, int y, float scale, int angle, float zoomMin, float zoomMan) {
        return addMapLabel(enumName, (Object) stringLabel, x, y, scale, angle, zoomMin, zoomMan);
    }

    private static LOTRMapLabels addMapLabel(String enumName, Object label, int x, int y, float scale, int angle, float zoomMin, float zoomMan) {
        Class<?>[] classArr = {Object.class, int.class, int.class, float.class, int.class, float.class, float.class};
        Object[] args = {label, x, y, scale, angle, zoomMin, zoomMan};

        return EnumHelper.addEnum(LOTRMapLabels.class, enumName, classArr, args);
    }

    public static LOTRInvasions addInvasion(String enumName, LOTRFaction faction) {
        return addInvasion(enumName, faction, null);
    }

    public static LOTRInvasions addInvasion(String enumName, LOTRFaction faction, String subfaction) {
        Class<?>[] classArr = {LOTRFaction.class, String.class};
        Object[] args = {faction, subfaction};

        return EnumHelper.addEnum(LOTRInvasions.class, enumName, classArr, args);
    }

    public static LOTRShields addAlignmentShield(String enumName, LOTRFaction faction) {
        Class<?>[] classArr = {LOTRFaction.class};
        Object[] args = {faction};

        return EnumHelper.addEnum(LOTRShields.class, enumName, classArr, args);
    }

    public static LOTRShields addShield(String enumName, boolean hidden, String... players) {
        return addShield(enumName, ShieldType.EXCLUSIVE, hidden, players);
    }

    public static LOTRShields addShield(String enumName, ShieldType type, boolean hidden, String... players) {
        Class<?>[] classArr = {ShieldType.class, boolean.class, String[].class};
        Object[] args = {type, hidden, players};

        return EnumHelper.addEnum(LOTRShields.class, enumName, classArr, args);
    }
    
    public static LoreCategory addLoreCategory(String enumName, String name) {
        Class<?>[] classArr = {String.class};
        Object[] args = {name};

        return EnumHelper.addEnum(LoreCategory.class, enumName, classArr, args);
    }
    
    public static LOTRMiniQuestFactory addMiniQuestFactory(String enumName, String name) {
        Class<?>[] classArr = {String.class};
        Object[] args = {name};

        return EnumHelper.addEnum(LOTRMiniQuestFactory.class, enumName, classArr, args);
    }
}
