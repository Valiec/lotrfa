package eoa.lotrfa.common.reflection;

import java.lang.reflect.*;
import java.util.*;
import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.lotrfa.common.LOTRFA;
import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.*;
import lotr.common.LOTRFaction.ControlZone;
import lotr.common.LOTRLore.LoreCategory;
import lotr.common.LOTRTitle.TitleType;
import lotr.common.entity.item.LOTREntityBanner;
import lotr.common.entity.item.LOTREntityBannerWall;
import lotr.common.entity.npc.*;
import lotr.common.item.*;
import lotr.common.item.LOTRItemBanner.BannerType;
import lotr.common.item.LOTRItemMountArmor.Mount;
import lotr.common.item.LOTRItemMug.Vessel;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuest.QuestFactoryBase;
import lotr.common.quest.LOTRMiniQuestFactory;
import lotr.common.util.LOTRVersionChecker;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.map.LOTRRoads;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.spawning.LOTRInvasions;
import lotr.common.world.structure.LOTRChestContents;
import lotr.common.world.structure2.scan.LOTRStructureScan;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class LOTRFAReflectionHelper {
    /**
     * Returns the {@link LOTRCreativeTabs} with that given field name.
     * This shouldn't be used for {@link LOTRCreativeTabs#tabStory} use LOTRCreativeTabs.tabStory instead.
     * 
     * @param name Name
     * @return {@link LOTRCreativeTabs}
     */
    public static LOTRCreativeTabs getLOTRCreativeTab(String name) {
        return ReflectionHelper.getPrivateValue(LOTRCreativeTabs.class, null, name);
    }

    // Some biomes have the same names as vanilla ones and the same for as the creative tabs happen
    /**
     * Returns the {@link LOTRBiome} with that given field name.
     * This should only be used for biomes where you can't do LOTRBiome.biomeName.
     * 
     * @param name Name
     * @return {@link LOTRBiome}
     */
    public static LOTRBiome getLOTRBiome(String name) {
        return ReflectionHelper.getPrivateValue(LOTRBiome.class, null, name);
    }

    public static void disableWaypoint(LOTRWaypoint wp) {
        ReflectionHelper.setPrivateValue(LOTRWaypoint.class, wp, true, "isHidden");
        ReflectionHelper.setPrivateValue(LOTRWaypoint.class, wp, LOTRFaction.HOSTILE, "faction");
    }

    public static void addBannerToIDMap(BannerType banner) {
        HashMap<Integer, BannerType> bannerForIDMap = ReflectionHelper.getPrivateValue(LOTRItemBanner.BannerType.class, null, "bannerForID");
        bannerForIDMap.put(banner.bannerID, banner);
        ReflectionHelper.setPrivateValue(LOTRItemBanner.BannerType.class, null, bannerForIDMap, "bannerForID");
    }

    public static List<Item> getAllLOTRFAItems() {
        List<Item> items = new ArrayList<Item>();
        try {
            for (Field field : LOTRFAItems.class.getFields()) {
                if (field != null && field.get(null) instanceof Item) items.add((Item) field.get(null));
            }
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            LOTRFA.logger.error("Errored when getting all LOTRFA fields.", e);
        }
        return items;
    }

    public static void addAlly(LOTRFaction faction, LOTRFaction ally) {
        if (faction == ally) {
            throw new IllegalArgumentException("Cannot ally a faction with itself! Base faction " + faction.factionName());
        }
        if (faction.isEnemy(ally) || ally.isEnemy(faction)) {
            throw new IllegalArgumentException("Cannot ally two enemy factions! Base faction " + faction.factionName() + " ally faction " + ally.factionName());
        }
        if (faction.isAllied(ally)) {
            throw new IllegalArgumentException("Already allies! Base faction " + faction.factionName() + " ally faction " + ally.factionName());
        }

        List<LOTRFaction> allies = faction.getAllies();
        allies.add(ally);
        ReflectionHelper.setPrivateValue(LOTRFaction.class, faction, allies, "allies");
    }

    public static void addEnemy(LOTRFaction faction, LOTRFaction enemy) {
        if (faction == enemy) {
            throw new IllegalArgumentException("Cannot enemy a faction with itself! Base faction " + faction.factionName());
        }
        if (faction.isAllied(enemy) || enemy.isAllied(faction)) {
            throw new IllegalArgumentException("Cannot enemy two allied factions! Base faction " + faction.factionName() + " enemy faction " + enemy.factionName());
        }
        if (faction.isEnemy(enemy)) {
            throw new IllegalArgumentException("Already enemies! Base faction " + faction.factionName() + " enemy faction " + enemy.factionName());
        }

        List<LOTRFaction> enemies = faction.getEnemies();
        enemies.add(enemy);
        ReflectionHelper.setPrivateValue(LOTRFaction.class, faction, enemies, "enemies");
    }

    public static void clearAllies(LOTRFaction faction) {
        ReflectionHelper.setPrivateValue(LOTRFaction.class, faction, new ArrayList<LOTRFaction>(), "allies");
    }

    public static void clearEnemies(LOTRFaction faction) {
        ReflectionHelper.setPrivateValue(LOTRFaction.class, faction, new ArrayList<LOTRFaction>(), "enemies");
    }

    public static void clearRelations(LOTRFaction faction) {
        clearAllies(faction);
        clearEnemies(faction);
    }

    public static void addControlZone(LOTRFaction faction, ControlZone zone) {
        findAndInvokeMethod(zone, LOTRFaction.class, faction, "addControlZone", ControlZone.class);
    }

    public static void clearControlZones(LOTRFaction faction) {
        ReflectionHelper.setPrivateValue(LOTRFaction.class, faction, new ArrayList<ControlZone>(), "controlZones");
    }

    public static void setFactionAlignmentAchievements(LOTRFaction faction, LOTRAchievement[] achievements) {
        if (achievements.length != 3) throw new IllegalArgumentException("The achievement array has to contain 3 achievements.");
        setFactionAlignmentAchievements(faction, 10, achievements[0]);
        setFactionAlignmentAchievements(faction, 100, achievements[1]);
        setFactionAlignmentAchievements(faction, 1000, achievements[2]);
    }

    public static void setFactionAlignmentAchievements(LOTRFaction faction, int alignmentLevel, LOTRAchievement achievement) {
        findAndInvokeMethod(new Object[] {alignmentLevel, achievement}, LOTRFaction.class, faction, "addAlignmentAchievement", int.class, LOTRAchievement.class);
    }
    
    public static List<LOTRFaction> getLOTRFAFactions() {
    	List<LOTRFaction> factions = new ArrayList<LOTRFaction>();
        try {
            for (Field field : LOTRFAFactions.class.getDeclaredFields()) {
                if (field != null && field.get(null) instanceof LOTRFaction) factions.add((LOTRFaction) field.get(null));
            }
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            LOTRFA.logger.error("Errored when getting all LOTRFAFactions fields.", e);
        }
        return factions;
    }

    public static void clearRoadDataBase() {
        Object dataBase = findAndInvokeConstructor("lotr.common.world.map.LOTRRoads$RoadPointDatabase");
        ReflectionHelper.setPrivateValue(LOTRRoads.class, null, dataBase, "roadPointDatabase");
    }

    public static void registerRoad(String name, Object... waypoints) {
        findAndInvokeMethod(new Object[] {name, waypoints}, LOTRRoads.class, null, "registerRoad", String.class, Object[].class);
    }

    public static void removeMapLabel(LOTRMapLabels label) {
        ReflectionHelper.setPrivateValue(LOTRMapLabels.class, label, 0.0001f, "maxZoom");
        ReflectionHelper.setPrivateValue(LOTRMapLabels.class, label, 0, "minZoom");
    }

    public static void changeInvasionIcon(LOTRInvasions invasion, Item icon) {
        ReflectionHelper.setPrivateValue(LOTRInvasions.class, invasion, icon, "invasionIcon");
    }

    public static void hideShield(LOTRShields shield) {
        ReflectionHelper.setPrivateValue(LOTRShields.class, shield, true, "isHidden");
    }
    
    public static void addSpeechBanks(Map<String, String[]> speechBanks) {
        Map<String, String[]> allSpeechBanks = ReflectionHelper.getPrivateValue(LOTRSpeech.class, null, "allSpeechBanks");
        allSpeechBanks.putAll(speechBanks);
        ReflectionHelper.setPrivateValue(LOTRSpeech.class, null, allSpeechBanks, "allSpeechBanks");
    }
    
    public static void addNameBanks(Map<String, String[]> nameBanks) {
        Map<String, String[]> allNameBanks = ReflectionHelper.getPrivateValue(LOTRNames.class, null, "allNameBanks");
        allNameBanks.putAll(nameBanks);
        ReflectionHelper.setPrivateValue(LOTRNames.class, null, allNameBanks, "allNameBanks");
    }
    
    public static void addSTRScans(Map<String, LOTRStructureScan> scans) {
        Map<String, LOTRStructureScan> allScans = ReflectionHelper.getPrivateValue(LOTRStructureScan.class, null, "allLoadedScans");
        allScans.putAll(scans);
        ReflectionHelper.setPrivateValue(LOTRStructureScan.class, null, allScans, "allLoadedScans");
    }
    
    public static void disableLOTRUpdateChecker() {
        ReflectionHelper.setPrivateValue(LOTRVersionChecker.class, null, true, "checkedUpdate");
    }
    
    public static void setBannerWasEverProtecting(LOTREntityBanner banner, boolean bool) {
        ReflectionHelper.setPrivateValue(LOTREntityBanner.class, banner, bool, "wasEverProtecting");
    }
    
    public static boolean getBannerWasEverProtecting(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "wasEverProtecting");
    }
    
    public static void setBannerPlayerSpecificProtection(LOTREntityBanner banner, boolean bool) {
        ReflectionHelper.setPrivateValue(LOTREntityBanner.class, banner, bool, "playerSpecificProtection");
    }
    
    public static boolean getBannerPlayerSpecificProtection(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "playerSpecificProtection");
    }
    
    public static boolean getBannerSelfProtection(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "selfProtection");
    }
    
    public static boolean getBannerStructureProtection(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "structureProtection");
    }
    
    public static NBTTagCompound getBannerProtectData(LOTREntityBanner banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBanner.class, banner, "protectData");
    }
    
    public static void setBannerProtectData(LOTREntityBanner banner, NBTTagCompound tag) {
        ReflectionHelper.setPrivateValue(LOTREntityBanner.class, banner, tag, "protectData");
    }
    
    public static NBTTagCompound getWallBannerProtectData(LOTREntityBannerWall banner) {
        return ReflectionHelper.getPrivateValue(LOTREntityBannerWall.class, banner, "protectData");
    }
    
    public static boolean getTitleUseAchievementName(LOTRTitle title) {
        return ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "useAchievementName");
    }
    
    public static LOTRAchievement getTitleTitleAchievement(LOTRTitle title) {
        return ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleAchievement");
    }
    
    public static TitleType getTitleTitleType(LOTRTitle title) {
        return ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleType");
    }
    
    public static List<LOTRFaction> getTitleAlignmentFactions(LOTRTitle title) {
        return ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "alignmentFactions");
    }
    
    public static void removeTitles(List<LOTRTitle> titles) {
    	for(LOTRTitle title : titles) removeTitle(title);
    }
    
    public static void removeTitle(LOTRTitle title) {
    	if(getTitleTitleType(title) == TitleType.ACHIEVEMENT) {
    		LOTRAchievement achievement = ReflectionHelper.getPrivateValue(LOTRTitle.class, title, "titleAchievement");
    		ReflectionHelper.setPrivateValue(LOTRAchievement.class, achievement, null, "achievementTitle");
    		ReflectionHelper.setPrivateValue(LOTRTitle.class, title, null, "titleAchievement");
    	}
    	
        ReflectionHelper.setPrivateValue(LOTRTitle.class, title, true, "isHidden");
        LOTRTitle.allTitles.remove(title);
    }
    
    public static void addLoreToLoreCategory(LoreCategory category, LOTRLore lore) {
        List<LOTRLore> loreList = ReflectionHelper.getPrivateValue(LOTRLore.LoreCategory.class, category, "loreList");
        loreList.add(lore);
        ReflectionHelper.setPrivateValue(LOTRLore.LoreCategory.class, category, loreList, "loreList");
    }
    
    public static ToolMaterial getBowMaterial(LOTRItemBow bow) {
        return ReflectionHelper.getPrivateValue(LOTRItemBow.class, bow, "bowMaterial");
    }
    
    public static void setMaterialCraftingItem(LOTRMaterial material, Item item) {
        setMaterialCraftingItem(material, item, item);
    }
    
    public static void setMaterialCraftingItem(LOTRMaterial material, Item toolItem, Item armorItem) {
        findAndInvokeMethod(new Object[] {toolItem, armorItem}, LOTRMaterial.class, material, "setCraftingItems", Item.class, Item.class);
    }
    
    public static void addMiniQuest(LOTRMiniQuestFactory factory, QuestFactoryBase questFactory) {
        findAndInvokeMethod(questFactory, LOTRMiniQuestFactory.class, factory, "addQuest", QuestFactoryBase.class);
    }
    
    public static void setHiringQuestFactory(LOTRMiniQuestFactory factory, int alignment) {
        Map<Class<? extends LOTRMiniQuest>, List<LOTRMiniQuest.QuestFactoryBase>> questFactoriesMap = ReflectionHelper.getPrivateValue(LOTRMiniQuestFactory.class, factory, "questFactories");
        for(List<LOTRMiniQuest.QuestFactoryBase> questFactories : questFactoriesMap.values()) {
            for (LOTRMiniQuest.QuestFactoryBase questFactory : questFactories) {
                questFactory.setRewardFactor(0.0f);
                questFactory.setHiring(alignment);
            }
        }
        ReflectionHelper.setPrivateValue(LOTRMiniQuestFactory.class, factory, questFactoriesMap, "questFactories");
    }
    
    public static void setMiniQuestFactoryAchievement(LOTRMiniQuestFactory factory, LOTRAchievement achievement) {
        findAndInvokeMethod(achievement, LOTRMiniQuestFactory.class, factory, "setAchievement", LOTRAchievement.class);
    }
    
    public static void setMiniQuestFactoryLore(LOTRMiniQuestFactory factory, LoreCategory... lore) {
        findAndInvokeMethod(new Object[] {lore}, LOTRMiniQuestFactory.class, factory, "setLore", LoreCategory[].class);
    }
    
    public static void setMiniQuestBaseSpeechGroup(LOTRMiniQuestFactory factory, LOTRMiniQuestFactory quest) {
        ReflectionHelper.setPrivateValue(LOTRMiniQuestFactory.class, factory, quest, "baseSpeechGroup");
    }
    
    public static String getArmorName(LOTRItemArmor armor) {
        return findAndInvokeMethod(LOTRItemArmor.class, armor, "getArmorName");
    }
    
    public static Mount getMountArmorType(LOTRItemMountArmor armor) {
        return ReflectionHelper.getPrivateValue(LOTRItemMountArmor.class, armor, "mountType");
    }
    
    public static void setTradeVessels(LOTRTradeEntries trade, LOTRFoods food) {
        setTradeVessels(trade, food.getDrinkVessels());
    }
    
    public static void setTradeVessels(LOTRTradeEntries trade, Vessel... vessels) {
        findAndInvokeMethod(new Object[] {vessels}, LOTRTradeEntries.class, trade, "setVessels", Vessel[].class);
    }
    
    public static void enableChestContentPouches(LOTRChestContents contents) {
        findAndInvokeMethod(LOTRChestContents.class, contents, "enablePouches");
    }
    
    public static void setChestContentLore(LOTRChestContents contents, int chance, LoreCategory... categories) {
        findAndInvokeMethod(new Object[] {chance, categories}, LOTRChestContents.class, contents, "setLore", int.class, LoreCategory[].class);
    }
    public static void setChestContentVessels(LOTRChestContents contents, LOTRFoods food) {
        setChestContentVessels(contents, food.getDrinkVessels());
    }
    
    public static void setChestContentVessels(LOTRChestContents contents, Vessel... vessels) {
        findAndInvokeMethod(new Object[] {vessels}, LOTRChestContents.class, contents, "setDrinkVessels", Vessel[].class);
    }
    
    public static void setLOTRNPCSize(LOTREntityNPC entity, float width, float height) {
        findAndInvokeMethod(new Object[] {width, height}, LOTREntityNPC.class, entity, new String[] {"setSize", "func_70105_a", "a"}, float.class, float.class);
    }
    
    public static void setToolMaterialRepairItem(ToolMaterial material, ItemStack item) {
        ReflectionHelper.setPrivateValue(ToolMaterial.class, material, item, "repairMaterial");
    }

    private static <T, E> T findAndInvokeMethod(Class<? super E> clazz, E instance, String methodName) {
        return findAndInvokeMethod(new Object[] {}, clazz, instance, methodName);
    }

    private static <T, E> T findAndInvokeMethod(Object arg, Class<? super E> clazz, E instance, String methodName, Class<?>... methodTypes) {
        return findAndInvokeMethod(new Object[] {arg}, clazz, instance, new String[] {methodName}, methodTypes);
    }

    private static <T, E> T findAndInvokeMethod(Object[] arg, Class<? super E> clazz, E instance, String methodName, Class<?>... methodTypes) {
        return findAndInvokeMethod(arg, clazz, instance, new String[] {methodName}, methodTypes);
    }

    @SuppressWarnings("unchecked")
    private static <T, E> T findAndInvokeMethod(Object[] args, Class<? super E> clazz, E instance, String[] methodNames, Class<?>... methodTypes) {
        Method addControlZoneMethod = ReflectionHelper.findMethod(clazz, instance, methodNames, methodTypes);
        try {
            return (T) addControlZoneMethod.invoke(instance, args);
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            LOTRFA.logger.error("Error when getting method " + methodNames[0] + " from class " + clazz.getSimpleName());
            e.printStackTrace();
        }
        
        return null;
    }

    private static <T> T findAndInvokeConstructor(String className, Class<?>... parameterTypes) {
        return findAndInvokeConstructor(new Object[] {}, className, parameterTypes);
    }

    @SuppressWarnings("unchecked")
    private static <T> T findAndInvokeConstructor(Object[] args, String className, Class<?>... parameterTypes) {
        try {
            return findAndInvokeConstructor(args, (Class<? super Object>) Class.forName(className), parameterTypes);
        }
        catch (ClassNotFoundException e) {
            LOTRFA.logger.error("Error when finding class " + className + " for a constructor.");
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T, E> T findAndInvokeConstructor(Object[] args, Class<E> clazz, Class<?>... parameterTypes) {
        Constructor<E> constructor = findConstructor(clazz, parameterTypes);
        constructor.setAccessible(true);
        try {
            return (T) constructor.newInstance(args);
        }
        catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            LOTRFA.logger.error("Error when initializing constructor from class " + clazz.getSimpleName() + " with parameters " + args);
            e.printStackTrace();
        }
        return null;
    }

    private static <E> Constructor<E> findConstructor(Class<E> clazz, Class<?>... parameterTypes) {
        try {
            return clazz.getDeclaredConstructor(parameterTypes);
        }
        catch (NoSuchMethodException | SecurityException e) {
            LOTRFA.logger.error("Error when getting constructor from class " + clazz.getSimpleName() + " with parameters " + parameterTypes);
            e.printStackTrace();
        }
        return null;
    }
}
