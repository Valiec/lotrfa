package eoa.lotrfa.common;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import eoa.lotrfa.common.block.table.*;
import eoa.lotrfa.common.entity.npc.LOTREntityHithlumVintnerGuard;
import eoa.lotrfa.common.item.LOTRFAMaterial;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import eoa.lotrfa.common.util.UpdateChecker;
import eoa.lotrfa.common.world.biome.LOTRBiomeGenHelcaraxe;
import eoa.lotrfa.common.world.biome.LOTRBiomeGenNanGuruthos;
import lotr.common.*;
import lotr.common.block.LOTRBlockGrapevine;
import lotr.common.entity.npc.*;
import lotr.common.item.LOTRMaterial;
import lotr.common.world.biome.LOTRBiomeGenForodwaith;
import net.minecraft.block.Block;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.item.*;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;

public class LOTRFAEventHandler {

    public LOTRFAEventHandler() {
        MinecraftForge.EVENT_BUS.register(this);
        //TODO this one shouldn't be needed
        FMLCommonHandler.instance().bus().register(this);
    }

    @SubscribeEvent
    public void onLivingUpdate(LivingUpdateEvent event) {
        EntityLivingBase entity = event.entityLiving;
        World world = entity.worldObj;

        if (!world.isRemote && entity.isEntityAlive() && entity.ticksExisted % 10 == 0) {
            //TODO make nargothrond speed boost atribute
            IAttributeInstance speedAttribute = entity.getEntityAttribute(SharedMonsterAttributes.movementSpeed);
            if (speedAttribute.getModifier(LOTREntityWoodElfScout.scoutArmorSpeedBoost.getID()) != null) {
                speedAttribute.removeModifier(LOTREntityWoodElfScout.scoutArmorSpeedBoost);
            }
            
            ArmorMaterial material = LOTRFAMaterial.getFullArmorMaterial(entity);
            if(material == LOTRFAMaterial.NARGOTHROND_RANGER.toArmorMaterial() || material == LOTRMaterial.WOOD_ELVEN_SCOUT.toArmorMaterial()) {
                speedAttribute.applyModifier(LOTREntityWoodElfScout.scoutArmorSpeedBoost);
            }
        }
        
        if (!world.isRemote && entity.isEntityAlive() && entity.ticksExisted % 20 == 0) {
            boolean canTakeFrostDamage = true;
            if (entity instanceof LOTREntityNPC && ((LOTREntityNPC) entity).isImmuneToFrost) {
                canTakeFrostDamage = false;
            }
            if (entity instanceof EntityPlayer) {
                canTakeFrostDamage = !((EntityPlayer) entity).capabilities.isCreativeMode;
            }
            if (canTakeFrostDamage) {
                int i1 = MathHelper.floor_double(entity.posX);
                int j5 = MathHelper.floor_double(entity.boundingBox.minY);
                int k5 = MathHelper.floor_double(entity.posZ);
                BiomeGenBase biomeGen = world.getBiomeGenForCoords(i1, k5);
                
                //TODO make helper method for biome gen at entity
                if ((biomeGen instanceof LOTRBiomeGenForodwaith ||  biomeGen instanceof LOTRBiomeGenNanGuruthos)  && (world.canBlockSeeTheSky(i1, j5, k5) || entity.isInWater()) && world.getSavedLightValue(EnumSkyBlock.Block, i1, j5, k5) < 10) {
                    int frostProtection = 50;
                    
                    for (int i = 1; i < 4; i++) {
                        ItemStack armor = entity.getEquipmentInSlot(i);
                        if (armor != null && armor.getItem() instanceof ItemArmor) {
                            Item material = ((ItemArmor) armor.getItem()).getArmorMaterial().func_151685_b();

                            if (material == Items.leather) {
                                frostProtection += 50;
                            }
                            else if (material == LOTRMod.urukSteel) { //TODO why?
                                frostProtection += 100;
                            }
                            else if (material == LOTRMod.fur) {
                                frostProtection += 100;
                            }
                        }
                    }
                    
                    if (world.isRaining()) frostProtection /= 3;
                    if (entity.isInWater()) frostProtection /= 20;
                    if (biomeGen instanceof LOTRBiomeGenHelcaraxe) frostProtection /= 5;
                    
                    frostProtection = Math.max(frostProtection, 1);
                    
                    if (world.rand.nextInt(frostProtection) == 0) {
                        entity.attackEntityFrom(LOTRDamage.frost, 1.0f);

                        if (biomeGen instanceof LOTRBiomeGenHelcaraxe) entity.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 600, 1));
                        if( biomeGen instanceof LOTRBiomeGenNanGuruthos) entity.addPotionEffect(new PotionEffect(Potion.weakness.id, 600));
                        
                    }
                    if (world.rand.nextInt(frostProtection) == 0) {
                        entity.attackEntityFrom(LOTRDamage.frost, 1.0f);
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void onCrafting(ItemCraftedEvent event) {
        EntityPlayer player = event.player;
        World world = player.worldObj;
        
        if(world.isRemote) return;
        
        Container container = player.openContainer;
        
        
        if (container instanceof LOTRBlockTablePettyDwarven.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.usePettyDwarvenTable);
        }
        else if (container instanceof LOTRBlockTableAngband.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useAngbandTable);
        }
        else if (container instanceof LOTRBlockTableBrethil.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useBrethilTable);
        }
        else if (container instanceof LOTRBlockTableDorDaidelos.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useDorDaidelosTable);
        }
        else if (container instanceof LOTRBlockTableDoriath.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useDoriathTable);
        }
        else if (container instanceof LOTRBlockTableDorLomin.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useDorLominTable);
        }
        else if (container instanceof LOTRBlockTableFalathrim.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useFalasTable);
        }
        else if (container instanceof LOTRBlockTableFeanorian.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useFeanorTable);
        }
        else if (container instanceof LOTRBlockTableGondolin.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useGondolinTable);
        }
        else if (container instanceof LOTRBlockTableHithlum.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useHithlumTable);
        }
        else if (container instanceof LOTRBlockTableHouseBor.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useBorTable);
        }
        else if (container instanceof LOTRBlockTableHouseUlfang.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useUlfangTable);
        }
        else if (container instanceof LOTRBlockTableLaegrim.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useLaegrimTable);
        }
        else if (container instanceof LOTRBlockTableNargothrond.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useNargothrondTable);
        }
        else if (container instanceof LOTRBlockTolInGaurhothTable.Container) {
            LOTRLevelData.getData(player).addAchievement(LOTRFAAchievements.useTolInGaurhothTable);
        }
    }

    @SubscribeEvent
    public void onBlockInteract(PlayerInteractEvent event) {
        EntityPlayer player = event.entityPlayer;
        World world = player.worldObj;
        int posX = event.x;
        int posY = event.y;
        int posZ = event.z;
        
        if (!world.canMineBlock(player, posX, posY, posZ) || player.canPlayerEdit(posX, posY, posZ, event.face, player.inventory.getCurrentItem())) return;
        
        if (event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK) {
            Block block = world.getBlock(posX, posY, posZ);
            if (!player.worldObj.isRemote && block instanceof LOTRBlockGrapevine && ((LOTRBlockGrapevine) block).hasGrapes) {
                LOTREntityHithlumVintnerGuard.defendGrapevines(world, posX, posY, posZ, player);
            }
        }
    }

    @SubscribeEvent
    public void onPlayerLogin(PlayerLoggedInEvent event) {
        EntityPlayer player = event.player;
        
        if(!player.worldObj.isRemote) {
            if(UpdateChecker.isUpdateAbailable()) UpdateChecker.announceUpdateClient(player);
        }
    }

    @SubscribeEvent
    public void onLivingSpawn(EntityJoinWorldEvent event) {
        Entity entity = event.entity;
        World world = event.world;
        
        if(entity instanceof LOTREntityWarg) {
            LOTREntityWarg warg = (LOTREntityWarg) entity;
            LOTRFAReflectionHelper.setLOTRNPCSize(warg, 1.2f, 1.95f);
        }
    }
}
