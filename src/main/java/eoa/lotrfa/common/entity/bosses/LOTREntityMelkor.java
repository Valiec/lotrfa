package eoa.lotrfa.common.entity.bosses;

import lotr.common.entity.npc.LOTREntitySauron;
import net.minecraft.world.World;

public class LOTREntityMelkor extends LOTREntitySauron {

    public LOTREntityMelkor(World world) {
        super(world);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onLivingUpdate() {
        super.onLivingUpdate();
        if (!this.worldObj.isRemote && this.getHealth() < this.getMaxHealth() && this.ticksExisted % 10 == 0) {
            this.setHealth(this.getHealth() - 2.0f);
            this.heal(0.5f);
        }
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(npcAttackDamage).setBaseValue(10.0);
    }

}
