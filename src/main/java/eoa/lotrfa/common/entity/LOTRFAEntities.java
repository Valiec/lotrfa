package eoa.lotrfa.common.entity;

import cpw.mods.fml.common.registry.EntityRegistry;
import eoa.lotrfa.common.LOTRFA;
import eoa.lotrfa.common.entity.animal.LOTREntityBlackSwan;
import eoa.lotrfa.common.entity.bosses.LOTREntityMelkor;
import eoa.lotrfa.common.entity.item.LOTRFAEntityBanner;
import eoa.lotrfa.common.entity.item.LOTRFAEntityBannerWall;
import eoa.lotrfa.common.entity.npc.*;
import lotr.common.entity.LOTREntities;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;

public class LOTRFAEntities extends LOTREntities {

    public static void registerCreature(Class<? extends Entity> entityClass, int id, int eggBackground, int eggSpots) {
        registerCreature(entityClass, getNameFromClass(entityClass), id, eggBackground, eggSpots);
    }

    public static void registerCreature(Class<? extends Entity> entityClass, int id) {
        registerCreature(entityClass, getNameFromClass(entityClass), id);
    }

    public static void registerCreature(Class<? extends Entity> entityClass, String name, int id, int eggBackground, int eggSpots) {
        spawnEggs.put(id, new SpawnEggInfo(id, eggBackground, eggSpots));
        registerCreature(entityClass, name, id);
    }

    public static void registerCreature(Class<? extends Entity> entityClass, String name, int id) {
        registerEntity(entityClass, name, id, 80, 3, true);
    }

    public static void registerEntity(Class<? extends Entity> entityClass, String name, int id, int updateRange, int updateFreq, boolean sendVelocityUpdates) {
        EntityRegistry.registerModEntity(entityClass, name, id, LOTRFA.instance, updateRange, updateFreq, sendVelocityUpdates);
        String fullName = (String) EntityList.classToStringMapping.get(entityClass);
        stringToIDMapping.put(fullName, id);
        IDToStringMapping.put(id, fullName);
        classToIDMapping.put(entityClass, id);
    }

    public static void registerEntities() {
        registerCreature(LOTREntityPettyDwarfAxeThrower.class, 1010, 0x545454, 0x3b3535);
        registerCreature(LOTREntityPettyDwarfWarrior.class, 1011, 0x545454, 0x3b3535);
        registerCreature(LOTREntityPettyDwarfSmith.class, 1012, 0xfc876d, 0xed5620);
        registerCreature(LOTREntityPettyDwarfCommander.class, 1013, 0x545454, 0x3b3535);
        registerCreature(LOTREntityPettyDwarf.class, 1014, 0xfc876d, 0xed5620);
        registerCreature(LOTREntityPettyDwarfBannerBearer.class, 1015, 0x545454, 0x3b3535);
        // registerCreature(LOTREntityRogueDwarf.class, 1016, 0xf6876d, 0xe75620);
        
        registerCreature(LOTREntityMelkor.class, 1018, 988430, 2830632);

        registerCreature(LOTREntityNargothrondElf.class, 1019, 0x22511d, 0xc2a70e);
        registerCreature(LOTREntityNargothrondTrader.class, 1020, 0x22511d, 0xc2a70e);
        registerCreature(LOTREntityNargothrondSmith.class, 1021, 0x22511d, 0xc2a70e);
        registerCreature(LOTREntityNargothrondGoldsmith.class, 1114, 0x22511d, 0xc2a70e);
        registerCreature(LOTREntityNargothrondStonemason.class, 1115, 0x22511d, 0xc2a70e);
        registerCreature(LOTREntityNargothrondRanger.class, 1039, 0x708c17, 0x162a14);
        registerCreature(LOTREntityNargothrondWarrior.class, 1022, 0xe3e3e8, 0x0c7b0d);
        registerCreature(LOTREntityNargothrondLord.class, 1023, 0xe3e3e8, 0x0c7b0d);
        registerCreature(LOTREntityNargothrondBannerBearer.class, 1024, 0xe3e3e8, 0x0c7b0d);

        registerCreature(LOTREntityGondolinElf.class, 1025, 0xe3e3e8, 0xf0b81d);
        registerCreature(LOTREntityGondolinSmith.class, 1026, 0xe3e3e8, 0xf0b81d);
        registerCreature(LOTREntityGondolinFlorist.class, 1103, 0xe3e3e8, 0xf0b81d);
        registerCreature(LOTREntityGondolinStonemason.class, 1104, 0xe3e3e8, 0xf0b81d);
        registerCreature(LOTREntityGondolinWarrior.class, 1027, 0x75a4ce, 0xcb9a15);
        registerCreature(LOTREntityGondolinLord.class, 1028, 0x75a4ce, 0xcb9a15);
        registerCreature(LOTREntityGondolinBannerBearer.class, 1029, 0x75a4ce, 0xcb9a15);

        registerCreature(LOTREntityFeanorianElf.class, 1030, 0xdb7b27, 0xbd4a1a);
        registerCreature(LOTREntityFeanorianSmith.class, 1031, 0xdb7b27, 0xbd4a1a);
        registerCreature(LOTREntityFeanorianStablemaster.class, 1102, 0xdb7b27, 0xbd4a1a);
        registerCreature(LOTREntityFeanorianWarrior.class, 1032, 0xe3e3e8, 0xe00000);
        registerCreature(LOTREntityFeanorianCavalryman.class, 1083, 0xe3e3e8, 0xe00000);
        registerCreature(LOTREntityFeanorianMountedArcher.class, 1207, 0xe3e3e8, 0xe00000);
        registerCreature(LOTREntityFeanorianBannerBearer.class, 1034, 0xe3e3e8, 0xe00000);
        registerCreature(LOTREntityFeanorianLord.class, 1033, 0xe3e3e8, 0xe00000);

        registerCreature(LOTREntityFalathrimElf.class, 1041, 0x08566e, 0xe3e3e8);
        registerCreature(LOTREntityFalathrimSailor.class, 1042, 0x08566e, 0xe3e3e8);
        registerCreature(LOTREntityFalathrimMariner.class, 1043, 0xe3e3e8, 0x08566e);
        registerCreature(LOTREntityFalathrimMarinerCaptain.class, 1044, 0xe3e3e8, 0x08566e);
        registerCreature(LOTREntityFalathrimArcher.class, 1045, 0xe3e3e8, 0x08566e);
        registerCreature(LOTREntityFalathrimFishmonger.class, 1052, 0x08566e, 0xe3e3e8);
        registerCreature(LOTREntityFalathrimLumberman.class, 1055, 0x08566e, 0xe3e3e8);
        registerCreature(LOTREntityFalathrimSmith.class, 1057, 0x08566e, 0xe3e3e8);
        registerCreature(LOTREntityFalathrimBannerBearer.class, 1058, 0xe3e3e8, 0x08566e);

        registerCreature(LOTREntityUlfangMan.class, 1059, 0xc6754a, 0x241b15);
        registerCreature(LOTREntityUlfangArcher.class, 1060, 0x241915, 0x555555);
        registerCreature(LOTREntityUlfangAxeThrower.class, 1061, 0x241915, 0x555555);
        registerCreature(LOTREntityUlfangBannerBearer.class, 1062, 0x241915, 0x555555);
        registerCreature(LOTREntityUlfangBartender.class, 1063, 0xc6754a, 0x241b15);
        registerCreature(LOTREntityUlfangBerserker.class, 1064, 0x241915, 0x969696);
        registerCreature(LOTREntityUlfangWarlord.class, 1065, 0x241915, 0x555555);
        registerCreature(LOTREntityUlfangWarrior.class, 1066, 0x241915, 0x555555);
        registerCreature(LOTREntityUlfangSmith.class, 1082, 0xc6754a, 0x241b15);
        registerCreature(LOTREntityUlfangLumberman.class, 1117, 0xc6754a, 0x241b15);
        registerCreature(LOTREntityUlfangButcher.class, 1118, 0xc6754a, 0x241b15);
       // registerCreature(LOTREntityUlfangFarmer.class, 1119, 15897714, 3679258);
        registerCreature(LOTREntityUlfangHuntsman.class, 1120, 0xc6754a, 0x241b15);
        registerCreature(LOTREntityUlfangBrewer.class, 1123, 0xc6754a, 0x241b15);

        registerCreature(LOTREntityBlackSwan.class, 1072, 0x1b1b1b, 0xff5151);

        registerCreature(LOTREntityNanDungorthebSpider.class, 1076, 2630945, 1315088);

        //House Bor
        registerCreature(LOTREntityBorMan.class, 1197, 0xf5915b, 0x2b1d17);
        registerCreature(LOTREntityBorLevyman.class, 1198, 0x3c2a1e, 0x2b1d17);
        registerCreature(LOTREntityBorSoldier.class, 1199, 0x3c2a1e, 0x7f7f7f);
        registerCreature(LOTREntityBorArcher.class, 1200, 0x3c2a1e, 0x7f7f7f);
        registerCreature(LOTREntityBorBannerBearer.class, 1201, 0x3c2a1e, 0x7f7f7f);
        registerCreature(LOTREntityBorCaptain.class, 1084, 0x3c2a1e, 0x7f7f7f);
        registerCreature(LOTREntityBorBlacksmith.class, 1203, 0xf5915b, 0x2b1d17);
        registerCreature(LOTREntityBorBaker.class, 1204, 0xf5915b, 0x2b1d17);
        registerCreature(LOTREntityBorBartender.class, 1089, 0xf5915b, 0x2b1d17);
        registerCreature(LOTREntityBorFarmer.class, 1090, 0xf5915b, 0x2b1d17);
        registerCreature(LOTREntityBorGreengrocer.class, 1091, 0xf5915b, 0x2b1d17);
        registerCreature(LOTREntityBorFarmhand.class, 1208, 0xf5915b, 0x2b1d17);

        registerCreature(LOTREntityTormentedElfFarmer.class, 1085, 14079919, 4337710);
        registerCreature(LOTREntityTormentedElfSmith.class, 1086, 14079919, 4337710);
        registerCreature(LOTREntityAngbandTormentedElf.class, 1087, 14079919, 4337710);

        registerCreature(LOTREntityBelegostGoldsmith.class, 1088, 16353133, 15357472);

        //Doriath
        registerCreature(LOTREntityDoriathElf.class, 1185, 0x869a70, 0xa7a38c);
        registerCreature(LOTREntityDoriathWarrior.class, 1186, 0xd6e8d9, 0xbfbdb8);
        registerCreature(LOTREntityDoriathBannerBearer.class, 1187, 0xd6e8d9, 0xbfbdb8);
        registerCreature(LOTREntityDoriathLord.class, 1188, 0xd6e8d9, 0xbfbdb8);
        registerCreature(LOTREntityDoriathWarden.class, 1189, 0x3e3c3c, 0x979090);
        registerCreature(LOTREntityDoriathSmith.class, 1190, 0x869a70, 0xa7a38c);
        registerCreature(LOTREntityDoriathBaker.class, 1097, 0x869a70, 0xa7a38c);
        registerCreature(LOTREntityDoriathHuntsman.class, 1098, 0x869a70, 0xa7a38c);
        registerCreature(LOTREntityDoriathLumberman.class, 1099, 0x869a70, 0xa7a38c);

        //Laegrim
        registerCreature(LOTREntityLaegrimElf.class, 1191, 0x374419, 0xddbe75);
        registerCreature(LOTREntityLaegrimWarrior.class, 1192, 0x4f5328, 0x524733);
        registerCreature(LOTREntityLaegrimCaptain.class, 1193, 0x4f5328, 0x524733);
        registerCreature(LOTREntityLaegrimBannerBearer.class, 1194, 0x4f5328, 0x524733);
        registerCreature(LOTREntityLaegrimScout.class, 1195, 0x392517, 0x403922);
        registerCreature(LOTREntityLaegrimSmith.class, 1196, 0x374419, 0xddbe75);
        registerCreature(LOTREntityLaegrimFlorist.class, 1111, 0x374419, 0xddbe75);
        registerCreature(LOTREntityLaegrimHuntsman.class, 1112, 0x374419, 0xddbe75);
        registerCreature(LOTREntityLaegrimLumberman.class, 1113, 0x374419, 0xddbe75);

        registerCreature(LOTREntityNogrodStonemason.class, 1116, 16353133, 15357472);

        registerCreature(LOTREntityTormentedElfStonemason.class, 1121, 14079919, 4337710);
        
        //Brethil
        registerCreature(LOTREntityBrethilArcher.class, 1124, 0x4f3f32, 0x566c1f);
        registerCreature(LOTREntityBrethilCaptain.class, 1125, 0x4f3f32, 0x566c1f);
        registerCreature(LOTREntityBrethilLevyman.class, 1126, 0x8f7b63, 0x261409);
        registerCreature(LOTREntityBrethilMan.class, 1127, 0xc79468, 0x261409);
        registerCreature(LOTREntityBrethilSoldier.class, 1128, 0x4f3f32, 0x566c1f);
        registerCreature(LOTREntityBrethilBannerBearer.class, 1135, 0x4f3f32, 0x566c1f);
        registerCreature(LOTREntityBrethilButcher.class, 1092, 0xc79468, 0x261409);
        registerCreature(LOTREntityBrethilLumberman.class, 1093, 0xc79468, 0x261409);
        registerCreature(LOTREntityBrethilSmith.class, 1094, 0xc79468, 0x261409);

        // Hithlum
        registerCreature(LOTREntityHithlumElf.class, 1180, 0x1f214b, 0xc1b78a);
        registerCreature(LOTREntityHithlumWarrior.class, 1181, 0xc5c4c4, 0x27295e);
        registerCreature(LOTREntityHithlumLord.class, 1182, 0xc5c4c4, 0x27295e);
        registerCreature(LOTREntityHithlumBannerBearer.class, 1183, 0xc5c4c4, 0x27295e);
        registerCreature(LOTREntityHithlumSmith.class, 1184, 0x1f214b, 0xc1b78a);
        registerCreature(LOTREntityHithlumFlorist.class, 1106,0x1f214b, 0xc1b78a);
        registerCreature(LOTREntityHithlumGreengrocer.class, 1107, 0x1f214b, 0xc1b78a);
        registerCreature(LOTREntityHithlumVinekeeper.class, 1078, 0x1f214b, 0xc1b78a);
        registerCreature(LOTREntityHithlumVinehand.class, 1080, 0x1f214b, 0xc1b78a);
        registerCreature(LOTREntityHithlumVintnerElf.class, 1035, 0x1f214b, 0xc1b78a);
        registerCreature(LOTREntityHithlumVintnerGuard.class, 1036, 0x1f214b, 0xc1b78a);

        // Dor Lomin
        registerCreature(LOTREntityDorLominMan.class, 1172, 0xceb8a5, 0x564133);
        registerCreature(LOTREntityDorLominLevyman.class, 1173, 0xceb8a5, 0x564133);
        registerCreature(LOTREntityDorLominSoldier.class, 1174, 0x2f313e, 0xa29d9d);
        registerCreature(LOTREntityDorLominArcher.class, 1175, 0x2f313e, 0xa29d9d);
        registerCreature(LOTREntityDorLominCaptain.class, 1176, 0x2f313e, 0xa29d9d);
        registerCreature(LOTREntityDorLominBannerBearer.class, 1177, 0x2f313e, 0xa29d9d);
        registerCreature(LOTREntityDorLominFarmer.class, 1178, 0xceb8a5, 0x564133);
        registerCreature(LOTREntityDorLominFarmhand.class, 1209, 0xceb8a5, 0x564133);
        registerCreature(LOTREntityDorLominBartender.class, 1179, 0xceb8a5, 0x564133);
        registerCreature(LOTREntityDorLominBrewer.class, 1100, 0xceb8a5, 0x564133);
        registerCreature(LOTREntityDorLominSmith.class, 1101, 0xceb8a5, 0x564133);
        registerCreature(LOTREntityDorLominVinehand.class, 1081, 0xceb8a5, 0x564133);

        // Dor Daidelos
        registerCreature(LOTREntityDorDaidelosOrc.class, 1157, 0x565e64, 0xd4cdbf);
        registerCreature(LOTREntityDorDaidelosOrcArcher.class, 1158, 0x565e64, 0xd4cdbf);
        registerCreature(LOTREntityDorDaidelosOrcChieftain.class, 1205, 0x565e64, 0xd4cdbf);
        registerCreature(LOTREntityDorDaidelosBannerBearer.class, 1206, 0x565e64, 0xd4cdbf);
        registerCreature(LOTREntityDorDaidelosTrader.class, 1169, 5979436, 13421772);
        registerCreature(LOTREntityUtumnoRemnantOrc.class, 1159, 0x383d41, 0xb8b2a5);
        registerCreature(LOTREntityUtumnoRemnantCrossbower.class, 1168, 0x383d41, 0xb8b2a5);
        registerCreature(LOTREntityUtumnoRemnantOrcCaptain.class, 1170, 0x383d41, 0xb8b2a5);
        registerCreature(LOTREntityUtumnoRemnantBannerBearer.class, 1171, 0x383d41, 0xb8b2a5);
        registerCreature(LOTREntityDorDaidelosWarg.class, 1166, 4600617, 2694422);
        registerCreature(LOTREntityUtumnoRemnantBerserker.class, 1136, 0x383d41, 0xb8b2a5);
        registerCreature(LOTREntityUtumnoRemnantSpider.class, 1073, 15594495, 7697919);
        // registerCreature(LOTREntityUtumnoRemnantWarg.class, 1074, 15066080, 9348269);
        registerCreature(LOTREntityDorDaidelosSnowTroll.class, 1075, 14606046, 11059905);

        // Tol-in-Gaurhoth
        registerCreature(LOTREntityTolInGaurhothOrc.class, 1161, 0x334039, 0x222222);
        registerCreature(LOTREntityTolInGaurhothOrcArcher.class, 1162, 0x334039, 0x222222);
        registerCreature(LOTREntityTolInGaurhothLumberman.class, 1105, 0x334039, 0x222222);
        registerCreature(LOTREntityTolInGaurhothBannerBearer.class, 1163, 0x334039, 0x222222);
        registerCreature(LOTREntityTolInGaurhothBerserker.class, 1210, 0x334039, 0x222222);
        registerCreature(LOTREntityTolInGaurhothOrcCaptain.class, 1164, 0x334039, 0x222222);
        registerCreature(LOTREntityTolInGaurhothWarg.class, 1137, 4600617, 2694422);
        registerCreature(LOTREntityTolInGaurhothWraith.class, 1138, 0x26322c, 0x19221e);
        //registerCreature(LOTREntityTolInGaurhothTroll.class, 1165, 10848082, 4796702);
        registerCreature(LOTREntityTolInGaurhothTrader.class, 1167, 5979436, 13421772);

        // Dor Daedeloth
        registerCreature(LOTREntityDorDaedelothOrc.class, 1148, 0x222426, 0x101112);
        registerCreature(LOTREntityDorDaedelothOrcArcher.class, 1149, 0x222426, 0x101112);
        registerCreature(LOTREntityDorDaedelothBannerBearer.class, 1150, 0x222426, 0x101112);
        registerCreature(LOTREntityDorDaedelothOrcBombardier.class, 1156, 0x222426, 0x101112);
        registerCreature(LOTREntityDorDaedelothWarg.class, 1151, 4600617, 2694422);
        registerCreature(LOTREntityDorDaedelothOrcCaptain.class, 1152, 0x222426, 0x101112);
        registerCreature(LOTREntityDorDaedelothHuntsman.class, 1095, 0x222426, 0x101112);
        registerCreature(LOTREntityDorDaedelothScavenger.class, 1096, 0x222426, 0x101112);
        registerCreature(LOTREntityDorDaedelothTrader.class, 1153, 5979436, 13421772);
        registerCreature(LOTREntityDorDaedelothTroll.class, 1139, 10848082, 4796702);
        registerCreature(LOTREntityDorDaedelothMountainTroll.class, 1140, 9991001, 5651753);

        // Angband
        registerCreature(LOTREntityAngbandOrc.class, 1141, 0x191418, 0x37312e);
        registerCreature(LOTREntityAngbandOrcArcher.class, 1142, 0x191418, 0x37312e);
        registerCreature(LOTREntityAngbandOrcFireThrower.class, 1154, 0x191418, 0x37312e);
        registerCreature(LOTREntityAngbandOrcBombardier.class, 1155, 0x191418, 0x37312e);
        registerCreature(LOTREntityAngbandBannerBearer.class, 1143, 0x191418, 0x37312e);
        registerCreature(LOTREntityAngbandWarg.class, 1144, 4600617, 2694422);
        registerCreature(LOTREntityAngbandWarTroll.class, 1145, 0x37312e, 0x2c2725);
        registerCreature(LOTREntityAngbandTrader.class, 1146, 0x191418, 0x37312e);
        registerCreature(LOTREntityAngbandOrcCaptain.class, 1147, 0x191418, 0x37312e);
        
        registerCreature(LOTREntityAngbandBalrog.class, 1040, 1772037, 13009920);
        registerCreature(LOTREntityAngbandBoldog.class, 1202, 0x191418, 0x37312e);
        
        registerEntity(LOTRFAEntityBanner.class, "FABanner", 2050, 160, 3, true);
        registerEntity(LOTRFAEntityBannerWall.class, "FAWallBanner", 2051, 160, Integer.MAX_VALUE, false);
        

    }

    private static String getNameFromClass(Class<? extends Entity> entityClass) {
        String simpleClassName = entityClass.getSimpleName();
        String entityName = null;
        String LOTRFAPrefix = "LOTRFAEntity";
        String LOTRPrefix = "LOTREntity";

        if (simpleClassName.startsWith(LOTRFAPrefix)) entityName = simpleClassName.substring(LOTRFAPrefix.length());
        else if (simpleClassName.startsWith(LOTRPrefix)) entityName = simpleClassName.substring(LOTRPrefix.length());

        return entityName;
    }

}
