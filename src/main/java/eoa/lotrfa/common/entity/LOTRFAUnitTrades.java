package eoa.lotrfa.common.entity;

import eoa.lotrfa.common.entity.npc.*;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.entity.animal.LOTREntityHorse;
import lotr.common.entity.animal.LOTREntityWildBoar;
import lotr.common.entity.npc.*;

public class LOTRFAUnitTrades {
    public static LOTRUnitTradeEntries bor_farmer_units;
    public static LOTRUnitTradeEntries dor_lomin_farmer_units;
    public static LOTRUnitTradeEntries hithlum_vinekeeper_units;

    public static LOTRUnitTradeEntries angband_units;
    public static LOTRUnitTradeEntries bor_units;
    public static LOTRUnitTradeEntries brethil_units;
    public static LOTRUnitTradeEntries doriath_units;
    public static LOTRUnitTradeEntries dor_daedeloth_units;
    public static LOTRUnitTradeEntries dor_daidelos_units;
    public static LOTRUnitTradeEntries dor_lomin_units;
    public static LOTRUnitTradeEntries falathrim_units;
    public static LOTRUnitTradeEntries feanorian_units;
    public static LOTRUnitTradeEntries feanorian_stablemaster_units;
    public static LOTRUnitTradeEntries gondolin_units;
    public static LOTRUnitTradeEntries hithlum_units;
    public static LOTRUnitTradeEntries laegrim_units;
    public static LOTRUnitTradeEntries nargothrond_units;
    public static LOTRUnitTradeEntries petty_dwarf_units;
    public static LOTRUnitTradeEntries tol_in_gaurhoth_units;
    public static LOTRUnitTradeEntries ulfang_units;
    public static LOTRUnitTradeEntries utumno_remnant_units;

    public static void setupTrades() {
        //Farmer units
        bor_farmer_units = new LOTRUnitTradeEntries(100, 
                new LOTRUnitTradeEntry(LOTREntityBorFarmhand.class, 40, 0).setTask(LOTRHiredNPCInfo.Task.FARMER) 
        );
        
        dor_lomin_farmer_units = new LOTRUnitTradeEntries(100, 
        		new LOTRUnitTradeEntry(LOTREntityDorLominVinehand.class, 40, 50).setTask(LOTRHiredNPCInfo.Task.FARMER),
        		new LOTRUnitTradeEntry(LOTREntityDorLominFarmhand.class, 40, 0).setTask(LOTRHiredNPCInfo.Task.FARMER) 
        );
        
        hithlum_vinekeeper_units = new LOTRUnitTradeEntries(0, 
                new LOTRUnitTradeEntry(LOTREntityDorLominVinehand.class, 40, 50).setTask(LOTRHiredNPCInfo.Task.FARMER), 
                new LOTRUnitTradeEntry(LOTREntityHithlumVinehand.class, 40, 50).setTask(LOTRHiredNPCInfo.Task.FARMER)
        );

        // Commander units
        angband_units = new LOTRUnitTradeEntries(150, 
                new LOTRUnitTradeEntry(LOTREntityAngbandOrc.class, 40, 100),
                new LOTRUnitTradeEntry(LOTREntityAngbandOrcArcher.class, 60, 150),
                new LOTRUnitTradeEntry(LOTREntityAngbandOrcFireThrower.class, 60, 200),
                new LOTRUnitTradeEntry(LOTREntityAngbandBannerBearer.class, 60, 250),
                new LOTRUnitTradeEntry(LOTREntityAngbandWarg.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityAngbandOrc.class, LOTREntityAngbandWarg.class, "AngbandOrc_Warg", 60, 100).setMountArmor(LOTRMod.wargArmorMordor, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityAngbandOrcArcher.class, LOTREntityAngbandWarg.class, "AngbandOrcArcher_Warg", 80, 150).setMountArmor(LOTRMod.wargArmorMordor, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityAngbandBannerBearer.class, LOTREntityAngbandWarg.class, "AngbandBannerBearer_Warg", 80, 150).setMountArmor(LOTRMod.wargArmorMordor, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityAngbandWarTroll.class, 120, 350) 
        );

        bor_units = new LOTRUnitTradeEntries(100, 
                new LOTRUnitTradeEntry(LOTREntityBorLevyman.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityBorSoldier.class, 30, 50),
                new LOTRUnitTradeEntry(LOTREntityBorArcher.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityBorBannerBearer.class, 50, 200),
                new LOTRUnitTradeEntry(LOTREntityBorSoldier.class, LOTREntityHorse.class, "BorSoldier_Horse", 50, 150).setMountArmor(LOTRMod.horseArmorDale),
                new LOTRUnitTradeEntry(LOTREntityBorBannerBearer.class, LOTREntityHorse.class, "BorBannerBearer_Horse", 70, 200).setMountArmor(LOTRMod.horseArmorDale)  
        );
        
        brethil_units = new LOTRUnitTradeEntries(100, 
                new LOTRUnitTradeEntry(LOTREntityBrethilLevyman.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityBrethilSoldier.class, 30, 50),
                new LOTRUnitTradeEntry(LOTREntityBrethilArcher.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityBrethilBannerBearer.class, 50, 200)
        );
        
        doriath_units = new LOTRUnitTradeEntries(300, 
                new LOTRUnitTradeEntry(LOTREntityDoriathElf.class, 30, 0),
                new LOTRUnitTradeEntry(LOTREntityDoriathWarden.class, 40, 50),
                new LOTRUnitTradeEntry(LOTREntityDoriathWarrior.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityDoriathBannerBearer.class, 70, 250),
                new LOTRUnitTradeEntry(LOTREntityDoriathWarrior.class, LOTREntityHorse.class, "DoriathWarrior_Horse", 70, 200).setMountArmor(LOTRMod.horseArmorGaladhrim),
                new LOTRUnitTradeEntry(LOTREntityDoriathBannerBearer.class, LOTREntityHorse.class, "DoriathBannerBearer_Horse", 78, 300).setMountArmor(LOTRMod.horseArmorGaladhrim)
        );

        dor_daedeloth_units = new LOTRUnitTradeEntries(150, 
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothOrc.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothOrcArcher.class, 40, 50),
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothOrcBombardier.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothBannerBearer.class, 40, 150),
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothWarg.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothOrc.class, LOTREntityDorDaedelothWarg.class, "DorDaedelothOrc_Warg", 40, 100 ).setMountArmor(LOTRMod.wargArmorMordor, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothOrcArcher.class, LOTREntityDorDaedelothWarg.class, "DorDaedelothOrcArcher_Warg", 60, 150).setMountArmor(LOTRMod.wargArmorMordor, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothBannerBearer.class, LOTREntityDorDaedelothWarg.class, "DorDaedelothBannerBearer_Warg", 70, 200).setMountArmor(LOTRMod.wargArmorMordor, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothTroll.class, 100, 250),
                new LOTRUnitTradeEntry(LOTREntityDorDaedelothMountainTroll.class, 120, 350)    
        );
        
        dor_daidelos_units = new LOTRUnitTradeEntries(150, 
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosOrc.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosOrcArcher.class, 40, 50),
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosBannerBearer.class, 60, 150),
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosWarg.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosOrc.class, LOTREntityDorDaidelosWarg.class, "DorDaidelosOrc_Warg", 40, 100).setMountArmor(null),
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosOrcArcher.class, LOTREntityDorDaidelosWarg.class, "DorDaidelosOrcArcher_Warg", 60, 150).setMountArmor(null),
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosBannerBearer.class, LOTREntityDorDaidelosWarg.class, "DorDaidelosBannerBearer_Warg", 70, 200).setMountArmor(null)
        );
        
        dor_lomin_units = new LOTRUnitTradeEntries(200, 
                new LOTRUnitTradeEntry(LOTREntityDorLominLevyman.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityDorLominSoldier.class, 30, 50),
                new LOTRUnitTradeEntry(LOTREntityDorLominArcher.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityDorLominSoldier.class, LOTREntityHorse.class, "DorLominSoldier_Horse", 50, 150).setMountArmor(LOTRMod.horseArmorGondor),
                new LOTRUnitTradeEntry(LOTREntityDorLominBannerBearer.class, LOTREntityHorse.class, "DorLominBannerBearer_Horse", 50, 150).setMountArmor(LOTRMod.horseArmorGondor),
                new LOTRUnitTradeEntry(LOTREntityDorLominBannerBearer.class, 50, 200)
        );
        
        falathrim_units = new LOTRUnitTradeEntries(200, 
                new LOTRUnitTradeEntry(LOTREntityFalathrimElf.class, 30, 0),
                new LOTRUnitTradeEntry(LOTREntityFalathrimSailor.class, 30, 0),
                new LOTRUnitTradeEntry(LOTREntityFalathrimArcher.class, 50, 50),
                new LOTRUnitTradeEntry(LOTREntityFalathrimMariner.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityFalathrimBannerBearer.class, 50, 150),
                new LOTRUnitTradeEntry(LOTREntityFalathrimMariner.class, LOTREntityHorse.class, "FalathrimMariner_Horse", 70, 200).setMountArmor(LOTRMod.horseArmorDolAmroth),
                new LOTRUnitTradeEntry(LOTREntityFalathrimArcher.class, LOTREntityHorse.class, "FalathrimArcher_Horse", 65, 100).setMountArmor(LOTRMod.horseArmorDolAmroth),
                new LOTRUnitTradeEntry(LOTREntityFalathrimBannerBearer.class, LOTREntityHorse.class, "FalathrimBannerBearer_Horse", 78, 300).setMountArmor(LOTRMod.horseArmorDolAmroth)
        );
        
        feanorian_units = new LOTRUnitTradeEntries(300, 
                new LOTRUnitTradeEntry(LOTREntityFeanorianElf.class, 30, 0),
                new LOTRUnitTradeEntry(LOTREntityFeanorianWarrior.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityFeanorianBannerBearer.class, 70, 250),
                new LOTRUnitTradeEntry(LOTREntityFeanorianCavalryman.class, LOTREntityHorse.class, "FeanorianCavalryman_Horse", 70, 200).setMountArmor(LOTRFAItems.feanorianHorseArmor),
                new LOTRUnitTradeEntry(LOTREntityFeanorianMountedArcher.class, LOTREntityHorse.class, "FeanorianMountedArcher_Horse", 70, 200).setMountArmor(LOTRFAItems.feanorianHorseArmor),
                new LOTRUnitTradeEntry(LOTREntityFeanorianBannerBearer.class, LOTREntityHorse.class, "FeanorianBannerBearer_Horse", 78, 300).setMountArmor(LOTRFAItems.feanorianHorseArmor)
        );
        
        feanorian_stablemaster_units = new LOTRUnitTradeEntries(300, 
                new LOTRUnitTradeEntry(LOTREntityFeanorianCavalryman.class, LOTREntityHorse.class, "FeanorianCavalryman_Horse", 70, 200).setMountArmor(LOTRFAItems.feanorianHorseArmor),
                new LOTRUnitTradeEntry(LOTREntityFeanorianMountedArcher.class, LOTREntityHorse.class, "FeanorianMountedArcher_Horse", 70, 200).setMountArmor(LOTRFAItems.feanorianHorseArmor),
                new LOTRUnitTradeEntry(LOTREntityFeanorianBannerBearer.class, LOTREntityHorse.class, "FeanorianBannerBearer_Horse", 78, 300).setMountArmor(LOTRFAItems.feanorianHorseArmor)
        );
        
        gondolin_units = new LOTRUnitTradeEntries(200, 
                new LOTRUnitTradeEntry(LOTREntityGondolinElf.class, 30, 0),
                new LOTRUnitTradeEntry(LOTREntityGondolinWarrior.class, 50, 50),
                new LOTRUnitTradeEntry(LOTREntityGondolinBannerBearer.class, 50, 150),
                new LOTRUnitTradeEntry(LOTREntityGondolinWarrior.class, LOTREntityHorse.class, "GondolinWarrior_Horse", 70, 200).setMountArmor(LOTRFAItems.gondolinHorseArmor),
                new LOTRUnitTradeEntry(LOTREntityGondolinBannerBearer.class, LOTREntityHorse.class, "GondolinBannerBearer_Horse", 78, 300).setMountArmor(LOTRFAItems.gondolinHorseArmor)
        );

        hithlum_units = new LOTRUnitTradeEntries(300, 
                new LOTRUnitTradeEntry(LOTREntityHithlumElf.class, 30, 0),
                new LOTRUnitTradeEntry(LOTREntityHithlumWarrior.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityHithlumWarrior.class, LOTREntityHorse.class, "HithlumWarrior_Horse", 70, 200).setMountArmor(LOTRMod.horseArmorHighElven),
                new LOTRUnitTradeEntry(LOTREntityHithlumBannerBearer.class, LOTREntityHorse.class, "HithlumBannerBearer_Horse", 78, 300).setMountArmor(LOTRMod.horseArmorHighElven),
                new LOTRUnitTradeEntry(LOTREntityHithlumBannerBearer.class, 70, 250)
        );

        laegrim_units = new LOTRUnitTradeEntries(250, 
                new LOTRUnitTradeEntry(LOTREntityLaegrimElf.class, 30, 0),
                new LOTRUnitTradeEntry(LOTREntityLaegrimScout.class, 40, 50),
                new LOTRUnitTradeEntry(LOTREntityLaegrimWarrior.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityLaegrimBannerBearer.class, 70, 250)
        );
        
        nargothrond_units = new LOTRUnitTradeEntries(300, 
                new LOTRUnitTradeEntry(LOTREntityNargothrondElf.class, 30, 0),
                new LOTRUnitTradeEntry(LOTREntityNargothrondRanger.class, 40, 50),
                new LOTRUnitTradeEntry(LOTREntityNargothrondWarrior.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityNargothrondBannerBearer.class, 70, 250),
                new LOTRUnitTradeEntry(LOTREntityNargothrondWarrior.class, LOTREntityHorse.class, "NargothrondWarrior_Horse", 70, 200).setMountArmor(LOTRMod.horseArmorRivendell),
                new LOTRUnitTradeEntry(LOTREntityNargothrondBannerBearer.class, LOTREntityHorse.class, "NargothrondBannerBearer_Horse", 78, 300).setMountArmor(LOTRMod.horseArmorRivendell)
        );
        
        petty_dwarf_units = new LOTRUnitTradeEntries(200, 
                new LOTRUnitTradeEntry(LOTREntityPettyDwarf.class, 20, 0), 
                new LOTRUnitTradeEntry(LOTREntityPettyDwarfWarrior.class, 30, 50), 
                new LOTRUnitTradeEntry(LOTREntityPettyDwarfAxeThrower.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityPettyDwarfBannerBearer.class, 50, 200),
                new LOTRUnitTradeEntry(LOTREntityPettyDwarfWarrior.class, LOTREntityWildBoar.class, "DwarfWarrior_Boar", 50, 150).setMountArmor(LOTRFAItems.pettyDwarfBoarArmor), 
                new LOTRUnitTradeEntry(LOTREntityPettyDwarfAxeThrower.class, LOTREntityWildBoar.class, "DwarfAxeThrower_Boar", 70, 200).setMountArmor(LOTRFAItems.pettyDwarfBoarArmor),
                new LOTRUnitTradeEntry(LOTREntityPettyDwarfBannerBearer.class, LOTREntityWildBoar.class, "PettyDwarfBannerBearer_Boar", 78, 250).setMountArmor(LOTRFAItems.pettyDwarfBoarArmor)
        );
        
        tol_in_gaurhoth_units = new LOTRUnitTradeEntries(150, 
                new LOTRUnitTradeEntry(LOTREntityTolInGaurhothOrc.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityTolInGaurhothOrcArcher.class, 40, 50),
                new LOTRUnitTradeEntry(LOTREntityTolInGaurhothBannerBearer.class, 40, 0),
                new LOTRUnitTradeEntry(LOTREntityTolInGaurhothWarg.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityTolInGaurhothOrc.class, LOTREntityTolInGaurhothWarg.class, "TolInGaurhothOrc_Warg", 40, 100).setMountArmor(LOTRMod.wargArmorAngmar, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityTolInGaurhothOrcArcher.class, LOTREntityTolInGaurhothWarg.class, "TolInGaurhothOrcArcher_Warg", 60, 150).setMountArmor(LOTRMod.wargArmorAngmar, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityTolInGaurhothBerserker.class, 80, 300),
                new LOTRUnitTradeEntry(LOTREntityTolInGaurhothBannerBearer.class, LOTREntityTolInGaurhothWarg.class, "TolInGaurhothBannerBearer_Warg", 70, 200).setMountArmor(LOTRMod.wargArmorAngmar, 0.5f)
                //new LOTRUnitTradeEntry(LOTREntityTolInGaurhothTroll.class, 100, 350)
        );
        
        ulfang_units = new LOTRUnitTradeEntries(100, 
                new LOTRUnitTradeEntry(LOTREntityUlfangMan.class, 15, 0),
                new LOTRUnitTradeEntry(LOTREntityUlfangWarrior.class, 30, 50),
                new LOTRUnitTradeEntry(LOTREntityUlfangArcher.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityUlfangAxeThrower.class, 50, 100),
                new LOTRUnitTradeEntry(LOTREntityUlfangBannerBearer.class, 55, 200),
                new LOTRUnitTradeEntry(LOTREntityUlfangBerserker.class, 70, 200),
                new LOTRUnitTradeEntry(LOTREntityUlfangWarrior.class, LOTREntityHorse.class, "UlfangWarrior_Horse", 70, 200).setMountArmor(LOTRFAItems.ulfangHorseArmor),
                new LOTRUnitTradeEntry(LOTREntityUlfangArcher.class, LOTREntityHorse.class, "UlfangArcher_Horse", 70, 200).setMountArmor(LOTRFAItems.ulfangHorseArmor),
                new LOTRUnitTradeEntry(LOTREntityUlfangBannerBearer.class, LOTREntityHorse.class, "UlfangBannerBearer_Horse", 78, 250).setMountArmor(LOTRFAItems.ulfangHorseArmor)
        );
        
        utumno_remnant_units = new LOTRUnitTradeEntries(150, 
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosOrc.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosOrcArcher.class, 40, 50),
                new LOTRUnitTradeEntry(LOTREntityUtumnoRemnantOrc.class, 40, 0),
                new LOTRUnitTradeEntry(LOTREntityUtumnoRemnantCrossbower.class, 60, 50),
                new LOTRUnitTradeEntry(LOTREntityUtumnoRemnantBerserker.class, 60, 150),
                new LOTRUnitTradeEntry(LOTREntityUtumnoRemnantBannerBearer.class, 60, 150),
                new LOTRUnitTradeEntry(LOTREntityDorDaidelosWarg.class, 20, 0),
                new LOTRUnitTradeEntry(LOTREntityUtumnoRemnantOrc.class, LOTREntityDorDaidelosWarg.class, "UtumnoRemnantOrc_Warg", 40, 100).setMountArmor(LOTRMod.wargArmorUruk, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityUtumnoRemnantCrossbower.class, LOTREntityDorDaidelosWarg.class, "UtumnoRemnantOrcCrossbower_Warg", 60, 150).setMountArmor(LOTRMod.wargArmorUruk, 0.5f),
                new LOTRUnitTradeEntry(LOTREntityUtumnoRemnantBannerBearer.class, LOTREntityDorDaidelosWarg.class, "UtumnoRemnantBannerBearer_Warg", 70, 200).setMountArmor(LOTRMod.wargArmorUruk, 0.5f)
        );
    }
}
