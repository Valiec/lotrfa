package eoa.lotrfa.common.entity.item;

import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.LOTRBannerProtection;
import lotr.common.entity.item.LOTREntityBanner;
import lotr.common.item.LOTRItemBanner;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.world.World;

public class LOTRFAEntityBanner extends LOTREntityBanner {

    public LOTRFAEntityBanner(World world) {
        super(world);
    }
    
    @Override
    public void onUpdate() {
        super.onUpdate();
        boolean protecting = this.isProtectingTerritory();
        if (!this.worldObj.isRemote && protecting) {
            LOTRFAReflectionHelper.setBannerWasEverProtecting(this, true);
        }
        if (!this.worldObj.isRemote && this.getPlacingPlayer() == null &&LOTRFAReflectionHelper.getBannerPlayerSpecificProtection(this)) {
            LOTRFAReflectionHelper.setBannerPlayerSpecificProtection(this, false);
        }
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        this.func_145771_j(this.posX, (this.boundingBox.minY + this.boundingBox.maxY) / 2.0, this.posZ);
        this.motionZ = 0.0;
        this.motionX = 0.0;
        this.motionY = 0.0;
        this.moveEntity(this.motionX, this.motionY, this.motionZ);
        int i = MathHelper.floor_double(this.posX);
        int j = MathHelper.floor_double(this.boundingBox.minY);
        int k = MathHelper.floor_double(this.posZ);
        boolean onSolidBlock = World.doesBlockHaveSolidTopSurface(this.worldObj, i, j - 1, k) && this.boundingBox.minY == MathHelper.ceiling_double_int(this.boundingBox.minY);
        if (!this.worldObj.isRemote && !onSolidBlock) {
            this.dropAsItem(true);
        }
        this.ignoreFrustumCheck = protecting;
    }
    
    @Override
    public boolean attackEntityFrom(DamageSource damagesource, float f) {
        boolean isProtectionBanner = this.isProtectingTerritory();
        boolean isPlayerDamage = damagesource.getEntity() instanceof EntityPlayer;
        if (isProtectionBanner && !isPlayerDamage) {
            return false;
        }
        if (!this.isDead && !this.worldObj.isRemote) {
            if (isPlayerDamage) {
                EntityPlayer entityplayer = (EntityPlayer) damagesource.getEntity();
                if (LOTRBannerProtection.isProtectedByBanner(this.worldObj, this, LOTRBannerProtection.forPlayer(entityplayer), true)) {
                    if (isProtectionBanner) {
                        if (LOTRFAReflectionHelper.getBannerSelfProtection(this)) {
                            return false;
                        }
                        if (LOTRFAReflectionHelper.getBannerStructureProtection(this) && damagesource.getEntity() != damagesource.getSourceOfDamage()) {
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
                if (isProtectionBanner && LOTRFAReflectionHelper.getBannerSelfProtection(this) && !this.canPlayerEditBanner(entityplayer)) {
                    return false;
                }
            }
            this.setBeenAttacked();
            this.worldObj.playSoundAtEntity(this, Blocks.planks.stepSound.getBreakSound(), (Blocks.planks.stepSound.getVolume() + 1.0f) / 2.0f, Blocks.planks.stepSound.getPitch() * 0.8f);
            boolean drop = true;
            if (damagesource.getEntity() instanceof EntityPlayer && ((EntityPlayer) damagesource.getEntity()).capabilities.isCreativeMode) {
                drop = false;
            }
            this.dropAsItem(drop);
        }
        return true;
    }

    private void dropAsItem(boolean drop) {
        this.setDead();
        if (drop) {
        	this.worldObj.findNearestEntityWithinAABB(EntityItem.class, this.boundingBox, this).setDead();
            this.entityDropItem(this.getBannerItem(), 0.0f);
        }
    }

    @Override
    public ItemStack getPickedResult(MovingObjectPosition target) {
        return this.getBannerItem();
    }

    private int getBannerTypeID() {
        return this.dataWatcher.getWatchableObjectByte(18);
    }

    private ItemStack getBannerItem() {
        ItemStack item = new ItemStack(LOTRFAItems.banner, 1, getBannerTypeID());
        if (LOTRFAReflectionHelper.getBannerWasEverProtecting(this) && LOTRFAReflectionHelper.getBannerProtectData(this) == null) {
            LOTRFAReflectionHelper.setBannerProtectData(this, new NBTTagCompound());
        }
        if (LOTRFAReflectionHelper.getBannerProtectData(this) != null) {
            this.writeProtectionToNBT(LOTRFAReflectionHelper.getBannerProtectData(this));
            if (!LOTRFAReflectionHelper.getBannerStructureProtection(this)) {
                LOTRItemBanner.setProtectionData(item, LOTRFAReflectionHelper.getBannerProtectData(this));
            }
        }
        return item;
    }

}
