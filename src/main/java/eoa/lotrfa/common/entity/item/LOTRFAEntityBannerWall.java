package eoa.lotrfa.common.entity.item;

import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import lotr.common.entity.item.LOTREntityBannerWall;
import lotr.common.item.LOTRItemBanner;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class LOTRFAEntityBannerWall extends LOTREntityBannerWall {

    public LOTRFAEntityBannerWall(World world, int i, int j, int k, int dir) {
        super(world, i, j, k, dir);
    }
    
    public LOTRFAEntityBannerWall(World world) {
        super(world);
    }
    
    @Override
    public void onBroken(Entity entity) {
        this.worldObj.playSoundAtEntity(this, Blocks.planks.stepSound.getBreakSound(), (Blocks.planks.stepSound.getVolume() + 1.0f) / 2.0f, Blocks.planks.stepSound.getPitch() * 0.8f);
        boolean flag = true;
        if (entity instanceof EntityPlayer && ((EntityPlayer) entity).capabilities.isCreativeMode) {
            flag = false;
        }
        if (flag) {
            this.entityDropItem(this.getBannerItem(), 0.0f);
        }
    }

    @Override
    public ItemStack getPickedResult(MovingObjectPosition target) {
        return this.getBannerItem();
    }

    private ItemStack getBannerItem() {
        ItemStack item = new ItemStack(LOTRFAItems.banner, 1, this.getBannerType().bannerID);
        if (LOTRFAReflectionHelper.getWallBannerProtectData(this) != null) {
            LOTRItemBanner.setProtectionData(item, LOTRFAReflectionHelper.getWallBannerProtectData(this));
        }
        return item;
    }

}
