package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.*;
import lotr.common.entity.ai.*;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.npc.LOTREntityOlogHai;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class LOTREntityAngbandWarTroll extends LOTREntityOlogHai {

    public LOTREntityAngbandWarTroll(World world) {
        super(world);
        this.tasks.taskEntries.clear();
        this.targetTasks.taskEntries.clear();
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(1, new LOTREntityAIHiredRemainStill(this));
        this.tasks.addTask(2, new LOTREntityAIAttackOnCollide(this, 2.0, false));
        this.tasks.addTask(3, new LOTREntityAIFollowHiringPlayer(this));
        this.tasks.addTask(4, new EntityAIWander(this, 1.0));
        this.tasks.addTask(5, new EntityAIWatchClosest2(this, EntityPlayer.class, 12.0f, 0.02f));
        this.tasks.addTask(5, new EntityAIWatchClosest2(this, LOTREntityNPC.class, 8.0f, 0.02f));
        this.tasks.addTask(6, new EntityAIWatchClosest(this, EntityLiving.class, 12.0f, 0.01f));
        this.tasks.addTask(7, new EntityAILookIdle(this));
        this.addTargetTasks(true, LOTREntityAINearestAttackableTargetOrc.class);
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(80.0);
        this.getEntityAttribute(LOTREntityNPC.npcAttackDamage).setBaseValue(7.0);
    }

    @Override
    public float getTrollScale() {
        return 1.25f;
    }

    @Override
    public int getTotalArmorValue() {
        return 15;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.angband;
    }

    @Override
    protected boolean hasTrollName() {
        return false;
    }

    @Override
    protected boolean canTrollBeTickled(final EntityPlayer entityplayer) {
        return false;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
    }

    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRAchievement.killTroll;
    }

    @Override
    public int getAlignmentBonus() {
        return 4;
    }

    @Override
    public void dropTrollItems(final boolean flag, final int i) {
        if (flag) {
            int rareDropChance = 8 - i;
            if (rareDropChance < 1) {
                rareDropChance = 1;
            }
            if (this.rand.nextInt(rareDropChance) == 0) {
                for (int drops = 1 + this.rand.nextInt(2) + this.rand.nextInt(i + 1), j = 0; j < drops; ++j) {
                    this.dropItem(LOTRMod.blackUrukSteel, 1);
                }
            }
        }
    }

    @Override
    protected int getExperiencePoints(final EntityPlayer entityplayer) {
        return 5 + this.rand.nextInt(8);
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        return null;
    }
}
