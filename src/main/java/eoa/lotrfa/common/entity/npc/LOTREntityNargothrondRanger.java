package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.entity.item.LOTREntityArrowPoisoned;
import lotr.common.item.LOTRItemBow;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityNargothrondRanger extends LOTREntityNargothrondWarrior {

    public LOTREntityNargothrondRanger(World world) {
        super(world);
        this.spawnRidingHorse = false; // not mounted
        this.npcShield = null;
    }

    @Override
    public void attackEntityWithRangedAttack(EntityLivingBase target, float f) {
        this.npcArrowAttack(target, f);
    }

    @Override
    protected void npcArrowAttack(EntityLivingBase target, float f) {
        float accuracy = (float) this.getEntityAttribute(npcRangedAccuracy).getAttributeValue();
        LOTREntityArrowPoisoned arrow = new LOTREntityArrowPoisoned(this.worldObj, this, target, 1.3f + this.getDistanceToEntity(target) / 80.0f, accuracy);
        ItemStack heldItem = this.getHeldItem();
        if (heldItem != null) {
            LOTRItemBow.applyBowModifiers(arrow, heldItem);
        }
        this.playSound("random.bow", 1.0f, 1.0f / (this.rand.nextFloat() * 0.4f + 0.8f));
        this.worldObj.spawnEntityInWorld(arrow);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        int i = this.rand.nextInt(6);
        if (i == 0) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.polearmRivendell));
        }
        else if (i == 1) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.longspearRivendell));
        }
        else {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.swordRivendell));
        }
        this.npcItemsInv.setRangedWeapon(new ItemStack(LOTRMod.rivendellBow));
        if (this.rand.nextInt(5) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.spearRivendell));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.nargothrondRangerBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.nargothrondRangerLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.nargothrondRangerChestplate));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRFAItems.nargothrondRangerHelmet));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }

}
