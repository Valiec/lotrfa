package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItemBanner;
import lotr.common.entity.npc.LOTRBannerBearer;
import lotr.common.item.LOTRItemBanner;
import net.minecraft.world.World;

public class LOTREntityBorBannerBearer extends LOTREntityBorSoldier implements LOTRBannerBearer
{
    public LOTREntityBorBannerBearer(final World world) {
        super(world);
        this.spawnRidingHorse = (this.rand.nextInt(6) == 0);
    }
    
    @Override
    public LOTRItemBanner.BannerType getBannerType() {
        return LOTRFAItemBanner.FABannerTypes.houseBor;
    }

}
