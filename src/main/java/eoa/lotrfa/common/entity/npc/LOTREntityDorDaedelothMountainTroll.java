package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.LOTRFaction;
import lotr.common.entity.npc.LOTREntityMountainTroll;
import net.minecraft.world.World;

public class LOTREntityDorDaedelothMountainTroll extends LOTREntityMountainTroll {

    public LOTREntityDorDaedelothMountainTroll(World world) {
        super(world);

    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.angband;
    }

}
