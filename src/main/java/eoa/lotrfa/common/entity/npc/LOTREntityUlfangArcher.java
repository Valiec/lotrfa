package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIRangedAttack;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUlfangArcher extends LOTREntityUlfangWarrior {

    public LOTREntityUlfangArcher(World world) {
        super(world);
        this.npcShield = LOTRFAShields.alignmentHouseUlfang;
    }
    
    @Override
    public EntityAIBase getDunlendingAttackAI() {
        return new LOTREntityAIRangedAttack(this, 1.4, 30, 50, 16.0f);
    }
    
    @Override
    protected void onAttackModeChange(final AttackMode mode, final boolean mounted) {
        if (mode == AttackMode.IDLE) {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getIdleItem());
        }
        else {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getRangedWeapon());
        }
    }
    
    @Override
    protected void dropFewItems(final boolean flag, final int i) {
        super.dropFewItems(flag, i);
        this.dropNPCArrows(i);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setRangedWeapon(new ItemStack(LOTRFAItems.ulfangBow));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getRangedWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsDunlending));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsDunlending));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyDunlending));
        if (this.rand.nextInt(3) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetDunlending));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }
    
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.houseUlfang;
    }

}
