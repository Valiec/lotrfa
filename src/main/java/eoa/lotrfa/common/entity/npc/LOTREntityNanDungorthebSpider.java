package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRFaction;
import lotr.common.entity.ai.LOTREntityAINearestAttackableTargetWoodElf;
import lotr.common.entity.npc.LOTREntitySpiderBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.world.World;

public class LOTREntityNanDungorthebSpider extends LOTREntitySpiderBase {

    public LOTREntityNanDungorthebSpider(World world) {
        super(world);
        this.addTargetTasks(true, LOTREntityAINearestAttackableTargetWoodElf.class);
    }

    @Override
    protected int getRandomSpiderScale() {
        return 1 + this.rand.nextInt(3);
    }

    @Override
    protected int getRandomSpiderType() {
        return this.rand.nextBoolean() ? 0 : 1 + this.rand.nextInt(2);
    }

    @Override
    public IEntityLivingData initCreatureForHire(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        return data;
    }

    @Override
    protected boolean canRideSpider() {
        return false;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        return data;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.nanDungortheb;
    }

    @Override
    public int getAlignmentBonus() {
        return 1;
    }

    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killNanDungorthebSpider;
    }

}
