package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.entity.LOTRFATrades;
import lotr.common.*;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUlfangBrewer extends LOTREntityUlfangMan implements LOTRTradeable {

    public LOTREntityUlfangBrewer(World world) {
        super(world);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return this.isFriendly(entityplayer);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        // TODO Auto-generated method stub
        return LOTRFATrades.ulfang_brewer_buy;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setIdleItem(new ItemStack(LOTRMod.mugAle));
        // this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        return data;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        // TODO Auto-generated method stub
        return LOTRFATrades.ulfang_brewer_sell;
    }

    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeUlfangTrader);
    }

    @Override
    public boolean shouldTraderRespawn() {
        // TODO Auto-generated method stub
        return true;
    }
    
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.houseUlfang;
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "houseUlfang/trader/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "houseUlfang/brewer/friendly";
        }
        return "houseUlfang/brewer/neutral";
    }
}
