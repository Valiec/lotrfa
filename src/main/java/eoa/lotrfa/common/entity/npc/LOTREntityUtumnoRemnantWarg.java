package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.npc.LOTREntityUtumnoIceWarg;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class LOTREntityUtumnoRemnantWarg extends LOTREntityUtumnoIceWarg {

    public LOTREntityUtumnoRemnantWarg(World world) {
        super(world);
        this.isChilly = false;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.dorDaidelos;
    }

    @Override
    public boolean canWargBeRidden() {
        return true;
    }

    @Override
    public LOTREntityNPC createWargRider() {
        if (this.rand.nextBoolean()) {
            this.setWargArmor(new ItemStack(LOTRMod.wargArmorMordor));
        }
        return this.worldObj.rand.nextBoolean() ? new LOTREntityUtumnoRemnantCrossbower(this.worldObj) : new LOTREntityUtumnoRemnantOrc(this.worldObj);
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        if (!this.worldObj.isRemote && this.canWargBeRidden() && this.rand.nextInt(3) == 0) {
            // LOTREntityNPC rider = this.createWargRider();
            // rider.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw,
            // 0.0f);
            // rider.onSpawnWithEgg(null);
            // rider.isNPCPersistent = this.isNPCPersistent;
            // this.worldObj.spawnEntityInWorld((Entity)rider);
            // rider.mountEntity((Entity)this);
        }
        return data;
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(MathHelper.getRandomIntegerInRange(this.rand, 20, 32));
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(32.0);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.25);
        this.getEntityAttribute(npcAttackDamage).setBaseValue(MathHelper.getRandomIntegerInRange(this.rand, 3, 5));
    }

}
