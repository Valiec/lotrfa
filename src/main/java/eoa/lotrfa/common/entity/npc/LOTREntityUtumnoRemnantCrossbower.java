package eoa.lotrfa.common.entity.npc;

import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIRangedAttack;
import lotr.common.entity.projectile.LOTREntityCrossbowBolt;
import lotr.common.item.LOTRItemCrossbow;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUtumnoRemnantCrossbower extends LOTREntityUtumnoRemnantOrc {

    public LOTREntityUtumnoRemnantCrossbower(final World world) {
        super(world);
    }

    @Override
    public EntityAIBase createOrcAttackAI() {
        return new LOTREntityAIRangedAttack(this, 1.4, 30, 40, 16.0f);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setRangedWeapon(new ItemStack(LOTRMod.urukCrossbow));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getRangedWeapon());
        return data;
    }

    @Override
    protected void onAttackModeChange(final AttackMode mode, final boolean mounted) {
        if (mode == AttackMode.IDLE) {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getIdleItem());
        }
        else {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getRangedWeapon());
        }
    }

    @Override
    public void attackEntityWithRangedAttack(final EntityLivingBase target, final float f) {
        final double d = this.getDistanceToEntity(target);
        final LOTREntityCrossbowBolt bolt = new LOTREntityCrossbowBolt(this.worldObj, this, target, new ItemStack(LOTRMod.crossbowBolt), 1.0f + (float) d / 16.0f * 0.015f, 1.0f);
        final ItemStack heldItem = this.getHeldItem();
        if (heldItem != null) {
            LOTRItemCrossbow.applyCrossbowModifiers(bolt, heldItem);
        }
        this.playSound("lotr:item.crossbow", 1.0f, 1.0f / (this.rand.nextFloat() * 0.4f + 0.8f));
        this.worldObj.spawnEntityInWorld(bolt);
    }

    @Override
    protected void dropFewItems(final boolean flag, final int i) {
        super.dropFewItems(flag, i);
        this.dropNPCCrossbowBolts(i);
    }

}
