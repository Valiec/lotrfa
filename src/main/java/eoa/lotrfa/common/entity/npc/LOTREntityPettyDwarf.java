package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.*;
import lotr.common.entity.npc.LOTREntityDwarf;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import lotr.common.world.structure.LOTRChestContents;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityPettyDwarf extends LOTREntityDwarf {

    public LOTREntityPettyDwarf(World world) {
        super(world);
    }
    
    @Override
    protected void dropFewItems(final boolean flag, final int i) {
        super.dropFewItems(flag, i);
        for (int bones = this.rand.nextInt(2) + this.rand.nextInt(i + 1), l = 0; l < bones; ++l) {
            this.dropItem(LOTRMod.dwarfBone, 1);
        }
        if (this.rand.nextInt(4) == 0) {
            this.dropChestContents(this.getLarderDrops(), 1, 2 + i);
        }
        if (this.rand.nextInt(8) == 0) {
            this.dropChestContents(this.getGenericDrops(), 1, 2 + i);
        }
        if (flag) {
            int rareDropChance = 20 - i * 4;
            rareDropChance = Math.max(rareDropChance, 1);
            if (this.rand.nextInt(rareDropChance) == 0) {
                final int randDrop = this.rand.nextInt(4);
                switch (randDrop) {
                    case 0: {
                        this.entityDropItem(new ItemStack(Items.iron_ingot), 0.0f);
                        break;
                    }
                    case 1: {
                        this.entityDropItem(new ItemStack(this.getDwarfSteelDrop()), 0.0f);
                        break;
                    }
                    case 2: {
                        this.entityDropItem(new ItemStack(Items.gold_nugget, 1 + this.rand.nextInt(3)), 0.0f);
                        break;
                    }
                    case 3: {
                        this.entityDropItem(new ItemStack(LOTRMod.silverNugget, 1 + this.rand.nextInt(3)), 0.0f);
                        break;
                    }
                }
            }
            int mithrilBookChance = 40 - i * 5;
            mithrilBookChance = Math.max(mithrilBookChance, 1);
            if (this.rand.nextInt(mithrilBookChance) == 0) {
                this.entityDropItem(new ItemStack(LOTRMod.mithrilBook), 0.0f);
            }
        }
    }
    
    @Override
    protected Item getDwarfSteelDrop() {
        return LOTRFAItems.pettyDwarfSteel;
    }
    
    @Override
    protected LOTRChestContents getLarderDrops() {
        return LOTRFAChestContents.petty_dwarf_house;
    }
    
    @Override
    protected LOTRChestContents getGenericDrops() {
        return LOTRFAChestContents.petty_dwarf_drops;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.pettyDwarf;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killPettyDwarf;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.pettyDwarf.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.pettyDwarf;
    }

}
