package eoa.lotrfa.common.entity.npc;

import java.util.List;
import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.world.biome.LOTRBiomeGenMithrim;
import lotr.common.*;
import lotr.common.world.LOTRWorldChunkManager;
import lotr.common.world.LOTRWorldProvider;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import net.minecraft.block.Block;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.util.ForgeDirection;

public class LOTREntityHithlumVintnerGuard extends LOTREntityHithlumWarrior {

    public int grapeAlert;
    public static final int MAX_GRAPE_ALERT = 3;

    public LOTREntityHithlumVintnerGuard(World world) {
        super(world);
    }

    public static void defendGrapevines(World world, int i, int j, int k, EntityPlayer entityplayer) {
        if (!entityplayer.capabilities.isCreativeMode) {
            LOTRBiomeVariant variant = world.provider instanceof LOTRWorldProvider ? ((LOTRWorldChunkManager) world.provider.worldChunkMgr).getBiomeVariantAt(i, k) : null;
            BiomeGenBase biome = world.getBiomeGenForCoords(i, k);
            if (biome instanceof LOTRBiomeGenMithrim && variant == LOTRBiomeVariant.VINEYARD) {
                int alignment = LOTRLevelData.getData(entityplayer).getAlignment(LOTRFAFactions.hithlum);
                boolean evil = alignment < 0;
                int limit = 2000;
                float chance = (float) (limit - alignment) / (float) limit;
                chance = Math.max(chance, 0.0f);
                chance = Math.min(chance, 1.0f);
                chance *= chance;
                if ((evil || world.rand.nextFloat() < chance) && world.rand.nextInt(4) == 0) {
                    int nearbyGuards = 0;
                    int spawnRange = 8;
                    List<LOTREntityHithlumVintnerGuard> guardList = world.getEntitiesWithinAABB(LOTREntityHithlumVintnerGuard.class, entityplayer.boundingBox.expand(spawnRange, spawnRange, spawnRange));
                    for (LOTREntityHithlumVintnerGuard guard : guardList) {
                        if (guard.hiredNPCInfo.isActive) continue;
                        ++nearbyGuards;
                    }
                    if (nearbyGuards < 8) {
                        int guardSpawns = 1 + world.rand.nextInt(6);
                        block1: for (int l = 0; l < guardSpawns; ++l) {
                            LOTREntityHithlumVintnerGuard guard = new LOTREntityHithlumVintnerGuard(world);
                            int attempts = 16;
                            for (int a = 0; a < attempts; ++a) {
                                int i1 = i + MathHelper.getRandomIntegerInRange(world.rand, (-spawnRange), spawnRange);
                                int j1 = j + MathHelper.getRandomIntegerInRange(world.rand, (-spawnRange) / 2, spawnRange / 2);
                                int k1 = k + MathHelper.getRandomIntegerInRange(world.rand, (-spawnRange), spawnRange);
                                Block belowBlock = world.getBlock(i1, j1 - 1, k1);
                                boolean belowSolid = belowBlock.isSideSolid(world, i1, j1 - 1, k1, ForgeDirection.UP);
                                if (!belowSolid || world.getBlock(i1, j1, k1).isNormalCube() || world.getBlock(i1, j1 + 1, k1).isNormalCube()) continue;
                                guard.setLocationAndAngles(i1 + 0.5, j1, k1 + 0.5, world.rand.nextFloat() * 360.0f, 0.0f);
                                guard.liftSpawnRestrictions = true;
                                if (!guard.getCanSpawnHere()) continue;
                                guard.liftSpawnRestrictions = false;
                                world.spawnEntityInWorld(guard);
                                guard.spawnRidingHorse = false;
                                guard.onSpawnWithEgg(null);
                                continue block1;
                            }
                        }
                    }
                }
                int range = 16;
                List<LOTREntityHithlumVintnerGuard> guardList = world.getEntitiesWithinAABB(LOTREntityHithlumVintnerGuard.class, entityplayer.boundingBox.expand(range, range, range));
                boolean anyAlert = false;
                for (Object obj : guardList) {
                    LOTREntityHithlumVintnerGuard guard = (LOTREntityHithlumVintnerGuard) obj;
                    if (guard.hiredNPCInfo.isActive) continue;
                    if (evil) {
                        guard.setAttackTarget(entityplayer);
                        guard.sendSpeechBank(entityplayer, "dorwinion/guard/grapeAttack");
                        guard.grapeAlert = 3;
                        anyAlert = true;
                        continue;
                    }
                    if (world.rand.nextFloat() >= chance) continue;
                    ++guard.grapeAlert;
                    if (guard.grapeAlert >= 3) {
                        guard.setAttackTarget(entityplayer);
                        guard.sendSpeechBank(entityplayer, "dorwinion/guard/grapeAttack");
                    }
                    else {
                        guard.sendSpeechBank(entityplayer, "dorwinion/guard/grapeWarn");
                    }
                    anyAlert = true;
                }
                if (anyAlert && alignment >= 0) {
                    LOTRLevelData.getData(entityplayer).addAlignment(entityplayer, LOTRAlignmentValues.VINEYARD_STEAL_PENALTY, LOTRFAFactions.hithlum, i + 0.5, j + 0.5, k + 0.5);
                }
            }
        }

    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        nbt.setInteger("GrapeAlert", this.grapeAlert);
    }

    @Override
    public void onDeath(DamageSource damagesource) {
        super.onDeath(damagesource);
        if (!this.worldObj.isRemote && damagesource.getEntity() instanceof EntityPlayer) {
            EntityPlayer entityplayer = (EntityPlayer) damagesource.getEntity();
            if (this.grapeAlert >= 3) {
                LOTRLevelData.getData(entityplayer).addAchievement(LOTRAchievement.stealDorwinionGrapes);
            }
        }
    }

    @Override
    public void onLivingUpdate() {
        super.onLivingUpdate();
        if (!this.worldObj.isRemote && this.grapeAlert > 0 && this.ticksExisted % 600 == 0) {
            --this.grapeAlert;
        }
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.grapeAlert = nbt.getInteger("GrapeAlert");
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        int i = this.rand.nextInt(6);
        if (i == 0) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.polearmHighElven));
        }
        else if (i == 1) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.longspearHighElven));
        }
        else {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.swordHighElven));
        }
        this.npcItemsInv.setRangedWeapon(new ItemStack(LOTRMod.highElvenBow));
        if (this.rand.nextInt(5) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.spearHighElven));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsHighElven));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsHighElven));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyHighElven));
        return data;
    }

}
