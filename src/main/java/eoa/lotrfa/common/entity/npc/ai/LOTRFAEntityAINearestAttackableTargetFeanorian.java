package eoa.lotrfa.common.entity.npc.ai;

import eoa.lotrfa.common.entity.npc.LOTREntityFeanorianElf;
import eoa.lotrfa.common.item.story.LOTRFAItemSilmaril;
import lotr.common.entity.ai.LOTREntityAINearestAttackableTargetBasic;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.player.EntityPlayer;

public class LOTRFAEntityAINearestAttackableTargetFeanorian extends LOTREntityAINearestAttackableTargetBasic {
    public LOTRFAEntityAINearestAttackableTargetFeanorian(EntityCreature entity, Class targetClass, int chance, boolean flag) {
        super(entity, targetClass, chance, flag);
    }

    public LOTRFAEntityAINearestAttackableTargetFeanorian(EntityCreature entity, Class targetClass, int chance, boolean flag, IEntitySelector selector) {
        super(entity, targetClass, chance, flag, selector);
    }
    
    @Override
    protected boolean isPlayerSuitableTarget(EntityPlayer player) {
        //TODO check pledge
        if(this.taskOwner instanceof LOTREntityFeanorianElf) {
            LOTREntityFeanorianElf elf = (LOTREntityFeanorianElf) this.taskOwner;
            if(elf.isSilmarilTarget(player)) {
                if(player.getHeldItem() != null && player.getHeldItem().getItem() instanceof LOTRFAItemSilmaril) return true;
                
                return this.taskOwner.getRNG().nextInt(25) == 0;
            }
        }
        
        return super.isPlayerSuitableTarget(player);
    }
}
