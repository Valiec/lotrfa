package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityFalathrimSailor extends LOTREntityFalathrimElf {

    public LOTREntityFalathrimSailor(World world) {
        super(world);
        this.spawnRidingHorse = false;
        // TODO Auto-generated constructor stub
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.falathrim;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.falathrimSword));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(Items.leather_boots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsDolAmrothGambeson));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyDolAmrothGambeson));
        if (this.rand.nextInt(3) == 0) {
            this.setCurrentItemOrArmor(4, null);
        }
        else {
            int i = this.rand.nextInt(3);
            if (i == 0) {
                this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetDolAmroth));
            }
            else if (i == 1) {
                this.setCurrentItemOrArmor(4, new ItemStack(Items.iron_helmet));
            }
            else if (i == 2) {
                this.setCurrentItemOrArmor(4, new ItemStack(Items.leather_helmet));
            }
        }
        return data;
    }

}
