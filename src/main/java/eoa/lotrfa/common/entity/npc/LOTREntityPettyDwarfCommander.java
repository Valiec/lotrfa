package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.entity.LOTRFAUnitTrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRLevelData;
import lotr.common.entity.npc.LOTRUnitTradeEntries;
import lotr.common.entity.npc.LOTRUnitTradeable;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityPettyDwarfCommander extends LOTREntityPettyDwarfWarrior implements LOTRUnitTradeable{

    public LOTREntityPettyDwarfCommander(World world) {
        super(world);
        this.npcShield = LOTRFAShields.alignmentPettyDwarf;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.pettyDwarfWarhammer));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.pettyDwarfBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.pettyDwarfLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.pettyDwarfChestplate));
        this.setCurrentItemOrArmor(4, null);
        return data;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 5;
    }

    @Override
    public LOTRUnitTradeEntries getUnits() {
        return LOTRFAUnitTrades.petty_dwarf_units;
    }

    @Override
    public LOTRInvasions getConquestHorn() {
        return LOTRFAInvasions.pettyDwarf;
    }
    
    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 200 && this.isFriendly(entityplayer);
    }
    
    @Override
    public void onUnitTrade(final EntityPlayer entityplayer) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradePettyDwarfCommander);
    }
    
    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "dwarf/dwarf/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "dwarf/commander/friendly";
        }
        return "dwarf/commander/neutral";
    }

}
