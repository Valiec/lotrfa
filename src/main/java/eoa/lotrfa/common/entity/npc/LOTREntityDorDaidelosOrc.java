package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.*;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.npc.LOTREntityOrc;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDorDaidelosOrc extends LOTREntityOrc {

    private static ItemStack[] weapons;
    private static ItemStack[] spears;
    private static ItemStack[] helmets;
    private static ItemStack[] bodies;
    private static ItemStack[] legs;
    private static ItemStack[] boots;

    public LOTREntityDorDaidelosOrc(final World world) {
        super(world);
    }
    
    @Override
    protected void dropOrcItems(final boolean flag, final int i) {
        if (this.rand.nextInt(6) == 0) {
            this.dropChestContents(LOTRFAChestContents.dor_daidelos_tent, 1, 2 + i);
        }
    }

    @Override
    public EntityAIBase createOrcAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.4, false);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        int i = this.rand.nextInt(LOTREntityDorDaidelosOrc.weapons.length);
        this.npcItemsInv.setMeleeWeapon(LOTREntityDorDaidelosOrc.weapons[i].copy());
        if (this.rand.nextInt(6) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            i = this.rand.nextInt(LOTREntityDorDaidelosOrc.spears.length);
            this.npcItemsInv.setMeleeWeapon(LOTREntityDorDaidelosOrc.spears[i].copy());
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        i = this.rand.nextInt(LOTREntityDorDaidelosOrc.boots.length);
        this.setCurrentItemOrArmor(1, LOTREntityDorDaidelosOrc.boots[i].copy());
        i = this.rand.nextInt(LOTREntityDorDaidelosOrc.legs.length);
        this.setCurrentItemOrArmor(2, LOTREntityDorDaidelosOrc.legs[i].copy());
        i = this.rand.nextInt(LOTREntityDorDaidelosOrc.bodies.length);
        this.setCurrentItemOrArmor(3, LOTREntityDorDaidelosOrc.bodies[i].copy());
        if (this.rand.nextInt(3) != 0) {
            i = this.rand.nextInt(LOTREntityDorDaidelosOrc.helmets.length);
            this.setCurrentItemOrArmor(4, LOTREntityDorDaidelosOrc.helmets[i].copy());
        }
        return data;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.dorDaidelos;
    }

    @Override
    public int getAlignmentBonus() {
        return 1;
    }

    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killDorDaidelosOrc;
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "dorDaidelos/orc/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "dorDaidelos/orc/hired";
        }
        if (LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 100) {
            return "dorDaidelos/orc/friendly";
        }
        return "dorDaidelos/orc/neutral";
    }

    @Override
    protected String getOrcSkirmishSpeech() {
        return "dorDaidelos/orc/skirmish";
    }

    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.dorDaidelos.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.dorDaidelos;
    }

    static {
        LOTREntityDorDaidelosOrc.weapons = new ItemStack[] {new ItemStack(LOTRMod.scimitarBlackUruk), new ItemStack(LOTRMod.battleaxeBlackUruk), new ItemStack(LOTRMod.hammerBlackUruk), new ItemStack(Items.iron_sword), new ItemStack(Items.iron_axe), new ItemStack(Items.iron_pickaxe), new ItemStack(LOTRMod.daggerIron), new ItemStack(LOTRMod.daggerIronPoisoned), new ItemStack(LOTRMod.battleaxeIron), new ItemStack(LOTRMod.swordBronze), new ItemStack(LOTRMod.axeBronze), new ItemStack(LOTRMod.pickaxeBronze), new ItemStack(LOTRMod.daggerBronze), new ItemStack(LOTRMod.daggerBronzePoisoned), new ItemStack(LOTRMod.battleaxeBronze), new ItemStack(LOTRMod.scimitarUruk), new ItemStack(LOTRMod.axeUruk), new ItemStack(LOTRMod.pickaxeUruk), new ItemStack(LOTRMod.daggerUruk), new ItemStack(LOTRMod.daggerUrukPoisoned), new ItemStack(LOTRMod.battleaxeUruk), new ItemStack(LOTRMod.hammerUruk), new ItemStack(LOTRMod.pikeUruk)};
        LOTREntityDorDaidelosOrc.spears = new ItemStack[] {new ItemStack(LOTRMod.spearIron), new ItemStack(LOTRMod.spearStone), new ItemStack(LOTRMod.spearUruk)};
        LOTREntityDorDaidelosOrc.helmets = new ItemStack[] {new ItemStack(LOTRMod.helmetBlackUruk), new ItemStack(LOTRMod.helmetFur), new ItemStack(LOTRFAItems.dorDaidelosHelmet)};
        LOTREntityDorDaidelosOrc.bodies = new ItemStack[] {new ItemStack(LOTRMod.bodyBlackUruk), new ItemStack(LOTRMod.bodyFur), new ItemStack(LOTRFAItems.dorDaidelosChestplate), new ItemStack(LOTRMod.bodyUruk)};
        LOTREntityDorDaidelosOrc.legs = new ItemStack[] {new ItemStack(LOTRMod.legsBlackUruk), new ItemStack(LOTRMod.legsFur), new ItemStack(LOTRFAItems.dorDaidelosLeggings), new ItemStack(LOTRMod.legsUruk)};
        LOTREntityDorDaidelosOrc.boots = new ItemStack[] {new ItemStack(LOTRMod.bootsBlackUruk), new ItemStack(LOTRMod.bootsFur), new ItemStack(LOTRFAItems.dorDaidelosBoots), new ItemStack(LOTRMod.bootsUruk)};
    }
}
