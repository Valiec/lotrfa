package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.entity.ai.LOTREntityAIRangedAttack;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.projectile.LOTREntityThrowingAxe;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityPettyDwarfAxeThrower extends LOTREntityPettyDwarfWarrior {

    public LOTREntityPettyDwarfAxeThrower(World world) {
        super(world);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.pettyDwarfBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.pettyDwarfLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.pettyDwarfChestplate));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRFAItems.pettyDwarfHelmet));
        }
        this.npcItemsInv.setRangedWeapon(new ItemStack(LOTRFAItems.pettyDwarfThrowingAxe));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getRangedWeapon());
        return data;
    }

    @Override
    public EntityAIBase getDwarfAttackAI() {
        return new LOTREntityAIRangedAttack(this, 1.25, 40, 12.0f);
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(npcRangedAccuracy).setBaseValue(0.75);
    }

    @Override
    public void onAttackModeChange(LOTREntityNPC.AttackMode mode, boolean mounted) {
        if (mode == LOTREntityNPC.AttackMode.IDLE) {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getIdleItem());
        }
        else {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getRangedWeapon());
        }
    }

    @Override
    public void attackEntityWithRangedAttack(EntityLivingBase target, float f) {
        LOTREntityThrowingAxe axe = new LOTREntityThrowingAxe(this.worldObj, this, target, this.npcItemsInv.getRangedWeapon(), 1.0f, (float) this.getEntityAttribute(npcRangedAccuracy).getAttributeValue());
        this.playSound("random.bow", 1.0f, 1.0f / (this.rand.nextFloat() * 0.4f + 0.8f));
        this.worldObj.spawnEntityInWorld(axe);
        this.swingItem();
    }

}
