package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDorDaidelosTrader extends LOTREntityDorDaidelosOrc implements LOTRTradeable.Smith {

    public LOTREntityDorDaidelosTrader(final World world) {
        super(world);
        this.addTargetTasks(false);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.dor_daidelos_trader_buy;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRFATrades.dor_daidelos_trader_sell;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.orcSkullStaff));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.dorDaidelosBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.dorDaidelosLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.dorDaidelosChestplate));
        this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetFur));
        return data;
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }

    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 100 && this.isFriendly(entityplayer);
    }
    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeDorDaidelosOrcTrader);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "dorDaidelos/trader/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "dorDaidelos/trader/friendly";
        }
        return "dorDaidelos/trader/neutral";
    }
}
