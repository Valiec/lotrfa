package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import lotr.common.entity.npc.LOTRBannerBearer;
import lotr.common.item.LOTRItemBanner;
import net.minecraft.world.World;

public class LOTREntityPettyDwarfBannerBearer extends LOTREntityPettyDwarf implements LOTRBannerBearer {

    public LOTREntityPettyDwarfBannerBearer(World world) {
        super(world);
        this.npcShield = LOTRFAShields.alignmentPettyDwarf;
    }

    @Override
    public LOTRItemBanner.BannerType getBannerType() {
        return LOTRFAItemBanner.FABannerTypes.pettyDwarves;
    }
}
