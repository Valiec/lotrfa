package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.LOTRFaction;
import lotr.common.entity.npc.LOTREntityUtumnoIceSpider;
import net.minecraft.world.World;

public class LOTREntityUtumnoRemnantSpider extends LOTREntityUtumnoIceSpider {

    public LOTREntityUtumnoRemnantSpider(World world) {
        super(world);
        this.isChilly = false;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.dorDaidelos;
    }

    @Override
    protected boolean canRideSpider() {
        return this.getSpiderScale() > 0;
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }

}
