package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.*;
import lotr.common.entity.npc.LOTREntityDunlending;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUlfangMan extends LOTREntityDunlending {

    public LOTREntityUlfangMan(World world) {
        super(world);
    }
    
    @Override
    protected void dropFewItems(final boolean flag, final int i) {
        super.dropFewItems(flag, i);
        for (int bones = this.rand.nextInt(2) + this.rand.nextInt(i + 1), l = 0; l < bones; ++l) {
            this.dropItem(Items.bone, 1);
        }
        this.dropUlfangItems(flag, i);
    }
    
    protected void dropUlfangItems(final boolean flag, final int i) {
        if (this.rand.nextInt(6) == 0) {
            this.dropChestContents(LOTRFAChestContents.house_ulfang_house, 1, 2 + i);
        }
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        final int i = this.rand.nextInt(2);
        if (i == 0) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangSword));
        }
        else if (i == 1) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangDagger));
        }
        else if (i == 2) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangDaggerPoisoned));
        }
        this.npcItemsInv.setIdleItem(null);
        if (this.rand.nextInt(4) == 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetFur));
        }
        return data;
    }
    
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.houseUlfang;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killUlfangMan;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.houseUlfang.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.houseUlfang;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "houseUlfang/man/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "houseUlfang/man/hired";
        }
        return "houseUlfang/man/friendly";
    }
}
