package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityTolInGaurhothBerserker extends LOTREntityTolInGaurhothOrc{
	    
    public LOTREntityTolInGaurhothBerserker(final World world) {
        super(world);
        float scale = 1.15f;
        this.setSize(this.npcWidth * scale, this.npcHeight * scale);
        this.isWeakOrc = false;
        this.isImmuneToFire = true;
    }
    
    @Override
    public EntityAIBase createOrcAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.6, false);
    }
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(27.0);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(24.0);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.22);
        this.getEntityAttribute(LOTREntityTolInGaurhothBerserker.npcAttackDamageExtra).setBaseValue(1.5);
        this.getEntityAttribute(LOTREntityTolInGaurhothBerserker.npcRangedAccuracy).setBaseValue(0.75);
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.tolInGaurhothCleaver));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsDolGuldur));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsBone));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyDolGuldur));
        this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetBone));
        return data;
    }
    
    @Override
    protected float getSoundPitch() {
        return super.getSoundPitch() * 0.8f;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }
}

