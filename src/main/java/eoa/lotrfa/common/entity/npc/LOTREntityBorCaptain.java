package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFACapes;
import eoa.lotrfa.common.entity.LOTRFAUnitTrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRLevelData;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.npc.LOTRUnitTradeEntries;
import lotr.common.entity.npc.LOTRUnitTradeable;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityBorCaptain extends LOTREntityBorSoldier implements LOTRUnitTradeable {

    public LOTREntityBorCaptain(final World world) {
        super(world);
        this.addTargetTasks(false);
        this.npcCape = LOTRFACapes.houseBor;
    }
    
    @Override
    protected EntityAIBase createDaleAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.6, true);
    }
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(25.0);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.borGreatsword));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.borCaptainBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.borCaptainLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.borCaptainChestplate));
        this.setCurrentItemOrArmor(4, new ItemStack(LOTRFAItems.borCaptainHelmet));
        return data;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 5;
    }
    
    @Override
    public LOTRUnitTradeEntries getUnits() {
        return LOTRFAUnitTrades.bor_units;
    }
    
    @Override
    public LOTRInvasions getConquestHorn() {
        return LOTRFAInvasions.houseBor;
    }
    
    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 100 && this.isFriendly(entityplayer);
    }
    
    @Override
    public void onUnitTrade(final EntityPlayer entityplayer) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeBorCaptain);
    }
    
    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "houseBor/captain/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "houseBor/captain/friendly";
        }
        return "houseBor/captain/neutral";
    }
}
