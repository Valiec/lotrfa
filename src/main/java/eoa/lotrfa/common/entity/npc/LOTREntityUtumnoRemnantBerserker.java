package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.npc.LOTREntityUrukHaiBerserker;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUtumnoRemnantBerserker extends LOTREntityUtumnoRemnantOrc {
    
    public LOTREntityUtumnoRemnantBerserker(final World world) {
        super(world);
        float scale = 1.15f;
        this.setSize(this.npcWidth * scale, this.npcHeight * scale);
    }
    
    @Override
    public EntityAIBase createOrcAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.6, false);
    }
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(30.0);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(24.0);
        this.getEntityAttribute(LOTREntityUrukHaiBerserker.npcAttackDamageExtra).setBaseValue(1.0);
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.utumnoRemnantBerserkerCleaver));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetUrukBerserker));
        return data;
    }
    
    @Override
    protected float getSoundPitch() {
        return super.getSoundPitch() * 0.8f;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "utumnoRemnant/orc/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "utumnoRemnant/orc/friendly";
        }
        return "utumnoRemnant/orc/neutral";
    }
}
