package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.npc.LOTREntityWarg;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityTolInGaurhothWarg extends LOTREntityWarg {

    public LOTREntityTolInGaurhothWarg(World world) {
        super(world);
    }

    @Override
    public LOTREntityNPC createWargRider() {
        if (this.rand.nextBoolean()) {
            this.setWargArmor(new ItemStack(LOTRMod.wargArmorAngmar));
        }
        return this.worldObj.rand.nextBoolean() ? new LOTREntityTolInGaurhothOrcArcher(this.worldObj) : new LOTREntityTolInGaurhothOrc(this.worldObj);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.tolInGaurhoth;
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }
}
