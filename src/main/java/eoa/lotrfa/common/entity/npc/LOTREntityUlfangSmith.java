package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUlfangSmith extends LOTREntityUlfangMan implements LOTRTradeable.Smith {

    public LOTREntityUlfangSmith(World world) {
        super(world);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 50 && this.isFriendly(entityplayer);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        // TODO Auto-generated method stub
        return LOTRFATrades.ulfang_smith_buy;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.blacksmithHammer));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        return data;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        // TODO Auto-generated method stub
        return LOTRFATrades.ulfang_smith_sell;
    }

    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeUlfangTrader);
    }

    @Override
    public boolean shouldTraderRespawn() {
        // TODO Auto-generated method stub
        return true;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "houseUlfang/trader/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "houseUlfang/smith/friendly";
        }
        return "houseUlfang/smith/neutral";
    }
}
