package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import lotr.common.LOTRFaction;
import lotr.common.entity.npc.LOTRBannerBearer;
import lotr.common.item.LOTRItemBanner;
import net.minecraft.world.World;

public class LOTREntityUlfangBannerBearer extends LOTREntityUlfangWarrior implements LOTRBannerBearer {

    public LOTREntityUlfangBannerBearer(World world) {
        super(world);
        this.npcShield = LOTRFAShields.alignmentHouseUlfang;
    }
    
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.houseUlfang;
    }
    
    @Override
    public LOTRItemBanner.BannerType getBannerType() {
        return LOTRFAItemBanner.FABannerTypes.houseUlfang;
    }

}
