package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.entity.LOTRFAUnitTrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.*;
import lotr.common.entity.npc.LOTRUnitTradeEntries;
import lotr.common.entity.npc.LOTRUnitTradeable;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUlfangWarlord extends LOTREntityUlfangWarrior implements LOTRUnitTradeable{

    public LOTREntityUlfangWarlord(World world) {
        super(world);
        this.addTargetTasks(false);
        this.npcShield = LOTRFAShields.alignmentHouseUlfang;
    }
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(25.0);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        boolean i = this.rand.nextBoolean();
        if (i) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangBattleaxe));
        }
        else {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangSword));
        }
        if (this.rand.nextInt(5) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangSpear));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsGundabadUruk));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsGundabadUruk));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyGundabadUruk));
        this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetGundabadUruk));
        return data;
    }

    @Override
    public LOTRUnitTradeEntries getUnits() {
        return LOTRFAUnitTrades.ulfang_units;
    }
    
    @Override
    public LOTRInvasions getConquestHorn() {
        return LOTRFAInvasions.houseUlfang;
    }
    
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.houseUlfang;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 5;
    }
    
    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 100 && this.isFriendly(entityplayer);
    }
    
    @Override
    public void onUnitTrade(final EntityPlayer entityplayer) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeUlfangWarlord);
    }
    
    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "houseUlfang/warlord/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "houseUlfang/warlord/friendly";
        }
        return "houseUlfang/warlord/neutral";
    }
}
