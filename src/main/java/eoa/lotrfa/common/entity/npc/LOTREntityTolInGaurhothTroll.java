package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.LOTRFaction;
import lotr.common.entity.npc.LOTREntityTroll;
import net.minecraft.world.World;

public class LOTREntityTolInGaurhothTroll extends LOTREntityTroll {

    public LOTREntityTolInGaurhothTroll(World world) {
        super(world);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.tolInGaurhoth;
    }

}
