package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.ai.LOTREntityAIRangedAttack;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityFeanorianWarrior extends LOTREntityFeanorianElf {

    public LOTREntityFeanorianWarrior(World world) {
        super(world);        
        this.tasks.addTask(2, this.meleeAttackAI);
        this.spawnRidingHorse = false;
        this.npcShield = LOTRFAShields.alignmentFeanorians;
    }
    
    @Override
    protected EntityAIBase createElfRangedAttackAI() {
        return new LOTREntityAIRangedAttack(this, 1.25, 25, 40, 24.0f);
    }
    
    @Override
    protected EntityAIBase createElfMeleeAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.5, false);
    }
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(24.0);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.feanorians;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        int i = this.rand.nextInt(6);
        if (i == 0) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianBattlestaff));
        }
        else if (i == 1) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianLongspear));
        }
        else {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianSword));
        }
        this.npcItemsInv.setRangedWeapon(new ItemStack(LOTRFAItems.feanorianBow));
        if (this.rand.nextInt(5) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianSpear));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.feanorianBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.feanorianLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.feanorianChestplate));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRFAItems.feanorianHelmet));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }

}
