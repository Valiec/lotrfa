package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import lotr.common.LOTRLevelData;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityLaegrimSmith extends LOTREntityLaegrimElf implements LOTRTradeable.Smith
{
    public LOTREntityLaegrimSmith(final World world) {
        super(world);
        this.addTargetTasks(false);
    }
    
    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRTradeEntries.WOOD_ELF_SMITH_BUY;
    }
    
    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRTradeEntries.WOOD_ELF_SMITH_SELL;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 100 && this.isFriendly(entityplayer);
    }
    
    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeLaegrimTrader);
    }
    
    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }
    
    @Override
    public boolean shouldRenderNPCHair() {
        return false;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "woodElf/smith/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "woodElf/smith/friendly";
        }
        return "woodElf/smith/neutral";
    }
}

