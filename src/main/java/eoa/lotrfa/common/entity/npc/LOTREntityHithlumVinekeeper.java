package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import eoa.lotrfa.common.entity.LOTRFAUnitTrades;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.*;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityHithlumVinekeeper extends LOTREntityHithlumElf implements LOTRTradeable, LOTRUnitTradeable {

    public LOTREntityHithlumVinekeeper(World world) {
        super(world);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.hithlum_vinekeeper_buy;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRFATrades.hithlum_vinekeeper_sell;
    }

    @Override
    public LOTRInvasions getConquestHorn() {
        return null;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(Items.iron_hoe));
        if (this.rand.nextBoolean()) {
            this.npcItemsInv.setIdleItem(new ItemStack(LOTRMod.grapeRed));
        }
        else {
            this.npcItemsInv.setIdleItem(new ItemStack(LOTRMod.grapeWhite));
        }
        return data;
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 0 && this.isFriendly(entityplayer);
    }

    @Override
    public void onPlayerTrade(EntityPlayer entityplayer, LOTRTradeEntries.TradeType type, ItemStack itemstack) {
    }

    @Override
    public LOTRUnitTradeEntries getUnits() {
        return LOTRFAUnitTrades.hithlum_vinekeeper_units;
    }

    @Override
    public void onUnitTrade(EntityPlayer entityplayer) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.hireHithlumVinekeeper);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return false;
    }

    @Override
    public String getSpeechBank(EntityPlayer entityplayer) {
        if (this.isFriendly(entityplayer)) {
            return "dorwinion/vinekeeper/friendly";
        }
        return "dorwinion/vinekeeper/hostile";
    }

}
