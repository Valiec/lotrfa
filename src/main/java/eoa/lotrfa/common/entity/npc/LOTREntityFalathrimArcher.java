package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.ai.LOTREntityAIRangedAttack;
import lotr.common.entity.animal.LOTREntityHorse;
import lotr.common.entity.npc.LOTRNPCMount;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityFalathrimArcher extends LOTREntityFalathrimElf {

    public LOTREntityFalathrimArcher(World world) {
        super(world);        
        this.tasks.addTask(2, this.meleeAttackAI);
        this.spawnRidingHorse = this.rand.nextInt(4) == 0;
        this.npcShield = LOTRFAShields.alignmentFalathrim;
    }
    
    @Override
    protected EntityAIBase createElfRangedAttackAI() {
        return new LOTREntityAIRangedAttack(this, 1.25, 25, 40, 24.0f);
    }
    
    @Override
    protected EntityAIBase createElfMeleeAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.5, false);
    }
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(24.0);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.falathrim;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }

    @Override
    public LOTRNPCMount createMountToRide() {
        LOTREntityHorse horse = (LOTREntityHorse) super.createMountToRide();
        horse.setMountArmor(new ItemStack(LOTRMod.horseArmorDolAmroth));
        return horse;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getRangedWeapon());
        this.npcItemsInv.setRangedWeapon(new ItemStack(LOTRFAItems.falathrimBow));
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.falathrimDagger));
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsDolAmroth));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsDolAmroth));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyDolAmroth));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetDolAmroth));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "falathrim/elf/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "falathrim/archer/hired";
        }
        return "falathrim/archer/friendly";
    }
}