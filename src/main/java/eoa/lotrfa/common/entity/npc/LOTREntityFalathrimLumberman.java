package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import lotr.common.item.LOTRItemLeatherHat;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityFalathrimLumberman extends LOTREntityFalathrimElf implements LOTRTradeable {

    public LOTREntityFalathrimLumberman(World world) {
        super(world);
        this.addTargetTasks(false);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean shouldRenderNPCHair() {
        return false;
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.falathrim_lumberman_buy;
    }
    
    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRFATrades.falathrim_lumberman_sell;
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 0 && this.isFriendly(entityplayer);
    }

    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeFalathrimTrader);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(Items.iron_axe));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        ItemStack hat = new ItemStack(LOTRMod.leatherHat);
        LOTRItemLeatherHat.setHatColor(hat, 6834742);
        LOTRItemLeatherHat.setFeatherColor(hat, 3916082);
        this.setCurrentItemOrArmor(4, hat);
        return data;
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "falathrim/trader/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "falathrim/lumberman/friendly";
        }
        return "falathrim/lumberman/neutral";
    }
}
