package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFACapes;
import eoa.lotrfa.common.entity.LOTRFAUnitTrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRLevelData;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.npc.LOTRUnitTradeEntries;
import lotr.common.entity.npc.LOTRUnitTradeable;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDorLominCaptain extends LOTREntityDorLominSoldier implements LOTRUnitTradeable {

    public LOTREntityDorLominCaptain(World world) {
        super(world);
        this.addTargetTasks(false);
        this.npcCape = LOTRFACapes.dorLomin;
    }

    @Override
    public EntityAIBase createGondorAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.6, false);
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(25.0);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.dorLominSword));
        this.npcItemsInv.setMeleeWeaponMounted(this.npcItemsInv.getMeleeWeapon());
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.npcItemsInv.setIdleItemMounted(this.npcItemsInv.getMeleeWeaponMounted());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.dorLominBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.dorLominLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.dorLominChestplate));
        this.setCurrentItemOrArmor(4, (ItemStack) null);
        return data;
    }

    @Override
    public int getAlignmentBonus() {
        return 5;
    }

    @Override
    public LOTRUnitTradeEntries getUnits() {
        return LOTRFAUnitTrades.dor_lomin_units;
    }

    @Override
    public LOTRInvasions getConquestHorn() {
        return LOTRFAInvasions.dorLomin;
    }

    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 200 && this.isFriendly(entityplayer);
    }

    @Override
    public void onUnitTrade(final EntityPlayer entityplayer) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeDorLominCaptain);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "dorLomin/captain/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "dorLomin/captain/friendly";
        }
        return "dorLomin/captain/neutral";
    }
}
