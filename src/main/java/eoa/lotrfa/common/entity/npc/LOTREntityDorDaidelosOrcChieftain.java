package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.entity.LOTRFAUnitTrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTRUnitTradeEntries;
import lotr.common.entity.npc.LOTRUnitTradeable;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDorDaidelosOrcChieftain extends LOTREntityUtumnoRemnantOrcCaptain implements LOTRUnitTradeable {

    public LOTREntityDorDaidelosOrcChieftain(final World world) {
        super(world);
        this.addTargetTasks(false);
        this.npcShield = LOTRFAShields.alignmentDorDaidelos; // needs to wear shield on back
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(30.0);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.orcSkullStaff));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.dorDaidelosBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.dorDaidelosLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.dorDaidelosChestplate));
        this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetFur));
        return data;
    }

    @Override
    public int getAlignmentBonus() {
        return 5;
    }

    @Override
    public LOTRUnitTradeEntries getUnits() {
        return LOTRFAUnitTrades.dor_daidelos_units;
    }

    @Override
    public LOTRInvasions getConquestHorn() {
        return LOTRFAInvasions.dorDaidelos;
    }

    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 150 && this.isFriendly(entityplayer);
    }

    @Override
    public void onUnitTrade(final EntityPlayer entityplayer) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeDorDaidelosOrcCaptain);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "dorDaidelos/chieftain/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "dorDaidelos/chieftain/friendly";
        }
        return "dorDaidelos/chieftain/neutral";
    }
}
