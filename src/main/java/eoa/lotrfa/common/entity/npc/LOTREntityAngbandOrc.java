package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.*;
import lotr.common.entity.npc.LOTREntityBlackUruk;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityAngbandOrc extends LOTREntityBlackUruk {

    public LOTREntityAngbandOrc(World world) {
        super(world);
        this.npcShield = LOTRFAShields.alignmentAngband;
    }
    
    @Override
    protected void dropOrcItems(final boolean flag, final int i) {
        if (this.rand.nextInt(6) == 0) {
            this.dropChestContents(LOTRFAChestContents.angband_tent, 1, 2 + i);
        }
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(24.0);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(24.0);
        this.getEntityAttribute(LOTREntityNPC.npcRangedAccuracy).setBaseValue(0.5);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        final int i = this.rand.nextInt(7);
        if (i == 0 || i == 1 || i == 2) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.scimitarBlackUruk));
        }
        else if (i == 3) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.battleaxeBlackUruk));
        }
        else if (i == 4) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.daggerBlackUruk));
        }
        else if (i == 5) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.daggerBlackUrukPoisoned));
        }
        else if (i == 6) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.hammerBlackUruk));
        }
        if (this.rand.nextInt(6) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.spearBlackUruk));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsBlackUruk));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsBlackUruk));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyBlackUruk));
        if (this.rand.nextInt(10) != 0) {
        this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetBlackUruk));
		}
		else {
			this.setCurrentItemOrArmor(4, null);
		}
		return data;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.angband;
    }

    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killAngbandOrc;
    }

    @Override
    protected float getSoundPitch() {
        return super.getSoundPitch() * 0.75f;
    }

    @Override
    public boolean canOrcSkirmish() {
        return false;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.angband.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.angband;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "angband/orc/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "angband/orc/hired";
        }
        return "angband/orc/friendly";
    }
}
