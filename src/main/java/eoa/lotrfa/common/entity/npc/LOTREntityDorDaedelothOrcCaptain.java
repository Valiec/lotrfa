package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFAUnitTrades;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTRUnitTradeEntries;
import lotr.common.entity.npc.LOTRUnitTradeable;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDorDaedelothOrcCaptain extends LOTREntityDorDaedelothOrc implements LOTRUnitTradeable {

    public LOTREntityDorDaedelothOrcCaptain(World world) {
        super(world);
        this.setSize(0.6f, 1.8f);
        this.addTargetTasks(false);
        this.isWeakOrc = false;
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(24.0);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.22);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.orcSkullStaff));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsOrc));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsOrc));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyOrc));
        this.setCurrentItemOrArmor(4, (ItemStack) null);
        return data;
    }

    @Override
    public int getAlignmentBonus() {
        return 5;
    }

    @Override
    public LOTRUnitTradeEntries getUnits() {
        return LOTRFAUnitTrades.dor_daedeloth_units;
    }

    @Override
    public LOTRInvasions getConquestHorn() {
        return LOTRFAInvasions.angbandDaedeloth;
    }

    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 150 && this.isFriendly(entityplayer);
    }

    @Override
    public void onUnitTrade(final EntityPlayer entityplayer) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeDorDaedelothOrcCaptain);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "dorDaedeloth/chieftain/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "dorDaedeloth/chieftain/friendly";
        }
        return "dorDaedeloth/chieftain/neutral";
    }
}
