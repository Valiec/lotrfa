package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItems;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityPettyDwarfWarrior extends LOTREntityPettyDwarf {

    public LOTREntityPettyDwarfWarrior(World world) {
        super(world);
        this.npcShield = LOTRFAShields.alignmentPettyDwarf;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        int i = this.rand.nextInt(7);
        if (i == 0) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.pettyDwarfSword));
        }
        else if (i == 1 || i == 2) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.pettyDwarfBattleaxe));
        }
        else if (i == 3 || i == 4) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.pettyDwarfWarhammer));
        }
        else if (i == 5) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.pettyDwarfMattock));
        }
        else if (i == 6) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.pettyDwarfPike));
        }
        if (this.rand.nextInt(6) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.pettyDwarfSpear));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.pettyDwarfBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.pettyDwarfLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.pettyDwarfChestplate));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRFAItems.pettyDwarfHelmet));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }

}
