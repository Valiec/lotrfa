package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityLaegrimFlorist extends LOTREntityLaegrimElf implements LOTRTradeable {

    public LOTREntityLaegrimFlorist(World world) {
        super(world);
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 50 && this.isFriendly(entityplayer);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.laegrim_florist_buy;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setIdleItem(new ItemStack(LOTRMod.niphredil));
        return data;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRFATrades.laegrim_florist_sell;
    }

    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeLaegrimTrader);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

}
