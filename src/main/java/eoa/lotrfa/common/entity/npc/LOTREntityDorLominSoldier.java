package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.npc.LOTREntityNPC;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDorLominSoldier extends LOTREntityDorLominLevyman {

    public LOTREntityDorLominSoldier(final World world) {
        super(world);
        this.spawnRidingHorse = (this.rand.nextInt(6) == 0);
        this.npcShield = LOTRFAShields.alignmentDorLomin;
    }

    @Override
    public EntityAIBase createGondorAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.45, false);
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(LOTREntityNPC.npcRangedAccuracy).setBaseValue(0.75);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        final int i = this.rand.nextInt(5);
        if (i == 0 || i == 1 || i == 2 || i == 3) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.dorLominSword));
        }
        else if (i == 4) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.dorLominBattleaxe));
        }
        else if (i == 5) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.dorLominPike));
        }
        if (this.rand.nextInt(5) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.dorLominSpear));
        }
        this.npcItemsInv.setMeleeWeaponMounted(this.npcItemsInv.getMeleeWeapon());
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.npcItemsInv.setIdleItemMounted(this.npcItemsInv.getMeleeWeaponMounted());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.dorLominBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.dorLominLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.dorLominChestplate));
        if (this.rand.nextInt(5) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRFAItems.dorLominHelmet));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "dorLomin/man/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "dorLomin/man/hired";
        }
        return "dorLomin/soldier/friendly";
    }
}
