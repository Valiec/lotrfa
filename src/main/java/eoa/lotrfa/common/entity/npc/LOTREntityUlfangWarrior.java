package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.animal.LOTREntityHorse;
import lotr.common.entity.npc.LOTRNPCMount;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUlfangWarrior extends LOTREntityUlfangMan {

    public LOTREntityUlfangWarrior(World world) {
        super(world);
        this.spawnRidingHorse = this.rand.nextInt(4) == 0;
        this.npcShield = LOTRFAShields.alignmentHouseUlfang;
    }
    
    @Override
    public EntityAIBase getDunlendingAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.6, false);
    }
    
    @Override
    public void setupNPCGender() {
        this.familyInfo.setMale(true);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        final int i = this.rand.nextInt(5);
        if (i == 0 || i == 1 || i == 2) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangSword));
        }
        else if (i == 3) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangPoleaxe));
        }
        else if (i == 4) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangBattleaxe));
        }
        else if (i == 5) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangPike));
        }
        if (this.rand.nextInt(5) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangSpear));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.npcItemsInv.setIdleItemMounted(this.npcItemsInv.getMeleeWeaponMounted());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsDunlending));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsDunlending));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyDunlending));
        if (this.rand.nextInt(5) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetDunlending));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }

    @Override
    public LOTRNPCMount createMountToRide() {
        LOTREntityHorse horse = (LOTREntityHorse) super.createMountToRide();
        horse.setHealth(horse.getMaxHealth() * 1.2f);
        horse.setMountArmor(new ItemStack(LOTRFAItems.ulfangHorseArmor));
        return horse;
    }
    
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.houseUlfang;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "houseUlfang/man/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "houseUlfang/warrior/hired";
        }
        return "houseUlfang/warrior/friendly";
    }
}
