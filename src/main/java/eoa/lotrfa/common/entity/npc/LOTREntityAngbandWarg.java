package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.*;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.npc.LOTREntityWarg;
import lotr.common.entity.npc.LOTREntityWarg.WargType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.world.World;

public class LOTREntityAngbandWarg extends LOTREntityWarg {

    public LOTREntityAngbandWarg(World world) {
        super(world);
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        if (this.rand.nextInt(500) == 0) {
            this.setWargType(WargType.FIRE);
        }
        else if (this.rand.nextInt(20) == 0) {
            this.setWargType(WargType.OBSIDIAN);
        }
        else if (this.rand.nextInt(3) == 0) {
            this.setWargType(WargType.BLACK);
        }
        else {
            this.setWargType(WargType.GREY);
        }
    }

    @Override
    protected LOTRAchievement getKillAchievement() {
        if(getWargType() == WargType.FIRE) return LOTRFAAchievements.killAngbandFireWarg;
        
        return super.getKillAchievement();
    }  

    @Override
    public LOTREntityNPC createWargRider() {
        if (this.rand.nextBoolean()) {
            this.setWargArmor(new ItemStack(LOTRMod.wargArmorMordor));
        }
        return this.worldObj.rand.nextBoolean() ? new LOTREntityAngbandOrcArcher(this.worldObj) : new LOTREntityAngbandOrc(this.worldObj);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.angband;
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    public void onDeath(DamageSource damagesource) {
        super.onDeath(damagesource);
        if(this.getWargType() == WargType.FIRE && damagesource instanceof EntityDamageSource) {
            Entity entity = ((EntityDamageSource) damagesource).getEntity();
            if(entity.isEntityAlive() && entity instanceof EntityLivingBase) {
                EntityLivingBase livingEntity = (EntityLivingBase) entity;
                livingEntity.attackEntityFrom(DamageSource.onFire, 3);
            }
        }
    }
}
