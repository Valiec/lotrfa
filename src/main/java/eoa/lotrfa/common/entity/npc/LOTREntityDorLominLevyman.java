package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDorLominLevyman extends LOTREntityDorLominMan {

    private static ItemStack[] militiaWeapons;
    private static int[] leatherDyes;

    public LOTREntityDorLominLevyman(final World world) {
        super(world);
        this.addTargetTasks(true);
    }

    @Override
    protected EntityAIBase createGondorAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.4, true);
    }

    @Override
    public void setupNPCGender() {
        this.familyInfo.setMale(true);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        int i = this.rand.nextInt(LOTREntityDorLominLevyman.militiaWeapons.length);
        this.npcItemsInv.setMeleeWeapon(LOTREntityDorLominLevyman.militiaWeapons[i].copy());
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, this.dyeLeather(new ItemStack(Items.leather_boots)));
        this.setCurrentItemOrArmor(2, this.dyeLeather(new ItemStack(Items.leather_leggings)));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyGondorGambeson));
        if (this.rand.nextInt(3) != 0) {
            this.setCurrentItemOrArmor(4, (ItemStack) null);
        }
        else {
            i = this.rand.nextInt(3);
            if (i == 0) {
                this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetPinnathGelin));
            }
            else if (i == 1) {
                this.setCurrentItemOrArmor(4, new ItemStack(Items.iron_helmet));
            }
            else if (i == 2) {
                this.setCurrentItemOrArmor(4, this.dyeLeather(new ItemStack(Items.leather_helmet)));
            }
        }
        return data;
    }

    private ItemStack dyeLeather(final ItemStack itemstack) {
        final int i = this.rand.nextInt(LOTREntityDorLominLevyman.leatherDyes.length);
        final int color = LOTREntityDorLominLevyman.leatherDyes[i];
        ((ItemArmor) itemstack.getItem()).func_82813_b(itemstack, color);
        return itemstack;
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "dorLomin/man/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "dorLomin/man/hired";
        }
        return "dorLomin/levyman/friendly";
    }

    static {
        LOTREntityDorLominLevyman.militiaWeapons = new ItemStack[] {new ItemStack(LOTRFAItems.dorLominSword), new ItemStack(LOTRFAItems.dorLominBattleaxe), new ItemStack(LOTRFAItems.dorLominPike), new ItemStack(Items.iron_sword), new ItemStack(Items.iron_axe), new ItemStack(LOTRMod.battleaxeIron), new ItemStack(LOTRMod.pikeIron), new ItemStack(LOTRMod.swordBronze), new ItemStack(LOTRMod.axeBronze), new ItemStack(LOTRMod.battleaxeBronze)};
        LOTREntityDorLominLevyman.leatherDyes = new int[] {10855845, 8026746, 5526612, 3684408, 8350297, 10388590, 4799795, 5330539, 4211801, 2632504};
    }

}
