package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.animal.LOTREntityHorse;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.npc.LOTRNPCMount;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityFalathrimMariner extends LOTREntityFalathrimElf {

    public LOTREntityFalathrimMariner(World world) {
        super(world);
        this.addTargetTasks(true);
        this.spawnRidingHorse = this.rand.nextInt(4) == 0;
        this.npcShield = LOTRFAShields.alignmentFalathrim;
        this.tasks.addTask(2, this.meleeAttackAI);
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(30.0);
        this.getEntityAttribute(horseAttackSpeed).setBaseValue(2.0);
        this.getEntityAttribute(npcRangedAccuracy).setBaseValue(0.75);
    }
    
    @Override
    public LOTRNPCMount createMountToRide() {
        LOTREntityHorse horse = (LOTREntityHorse) super.createMountToRide();
        horse.setMountArmor(new ItemStack(LOTRMod.horseArmorDolAmroth));
        return horse;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        final int i = this.rand.nextInt(2);
        if (i == 0) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.falathrimSword));
        }
        else if (i == 1) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.falathrimBattlestaff));
        }
        else if (i == 2) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.falathrimLongspear));
        }
        if (this.rand.nextInt(5) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.falathrimSpear));
        }
        {
        	this.npcItemsInv.setMeleeWeaponMounted(this.npcItemsInv.getMeleeWeapon());
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.npcItemsInv.setIdleItemMounted(this.npcItemsInv.getMeleeWeaponMounted());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsDolAmroth));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsDolAmroth));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyDolAmroth));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetDolAmroth));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.falathrim;
    }

    @Override
    protected void onAttackModeChange(LOTREntityNPC.AttackMode mode, boolean mounted) {
        if (mode == LOTREntityNPC.AttackMode.IDLE) {
            if (mounted) {
                this.setCurrentItemOrArmor(0, this.npcItemsInv.getIdleItemMounted());
            }
            else {
                this.setCurrentItemOrArmor(0, this.npcItemsInv.getIdleItem());
            }
        }
        else if (mounted) {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getMeleeWeaponMounted());
        }
        else {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getMeleeWeapon());
        }
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "falathrim/elf/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "falathrim/warrior/friendly";
        }
        return "falathrim/warrior/neutral";
    }
}