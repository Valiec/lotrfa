package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.entity.LOTRFATrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.*;
import lotr.common.entity.npc.LOTRTradeEntries.TradeType;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityTormentedElfStonemason extends LOTREntityTormentedElf implements LOTRTradeable {

    public LOTREntityTormentedElfStonemason(World world) {
        super(world);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return this.isFriendly(entityplayer);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        // TODO Auto-generated method stub
        return LOTRFATrades.tormented_elf_stonemason_buy;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFaction.UNALIGNED;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        final int i = this.rand.nextInt(8);
        if (i == 0 || i == 1) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.pickaxeOrc));
        }
        else if (i == 2) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.pickaxeUruk));
        }
        else if (i == 3) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianPickaxe));
        }
        else if (i == 4) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.pickaxeHighElven));
        }
    	else if (i == 5) {
    		this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.daggerHighElven));
    	}
    	else if (i == 6) {
    		this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianDagger));
    	}
        else {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.daggerBlackUruk));
        }
        int e3 = this.rand.nextInt(10);
        if (e3 == 0) {
            this.npcItemsInv.setIdleItem(new ItemStack(Blocks.stone));
        }
        else {
            this.npcItemsInv.setIdleItem(new ItemStack(LOTRMod.rock));
        }
        return data;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        // TODO Auto-generated method stub
        return LOTRFATrades.tormented_elf_stonemason_sell;
    }

    @Override
    public void onPlayerTrade(EntityPlayer arg0, TradeType arg1, ItemStack arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean shouldTraderRespawn() {
        // TODO Auto-generated method stub
        return true;
    }
    
    @Override
    public String getSpeechBank(EntityPlayer entityplayer) {
        if (this.isFriendly(entityplayer)) {
            if (this.canTradeWith(entityplayer)) {
                return "angband/tormentedElf/trader/friendly";
            }
            return "angband/tormentedElf/trader/neutral";
        }
        return "angband/tormentedElf/hostile";
    }
}
