package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItemBanner;
import lotr.common.entity.npc.LOTRBannerBearer;
import lotr.common.item.LOTRItemBanner;
import net.minecraft.world.World;

public class LOTREntityUtumnoRemnantBannerBearer extends LOTREntityUtumnoRemnantOrc implements LOTRBannerBearer {
    public LOTREntityUtumnoRemnantBannerBearer(final World world) {
        super(world);
    }

    @Override
    public LOTRItemBanner.BannerType getBannerType() {
        return LOTRFAItemBanner.FABannerTypes.utumnoRemnant;
    }
}
