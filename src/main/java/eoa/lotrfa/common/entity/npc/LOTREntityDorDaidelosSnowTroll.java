package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.LOTRFaction;
import lotr.common.entity.npc.LOTREntitySnowTroll;
import net.minecraft.world.World;

public class LOTREntityDorDaidelosSnowTroll extends LOTREntitySnowTroll {

    public LOTREntityDorDaidelosSnowTroll(World world) {
        super(world);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.dorDaidelos;
    }

}
