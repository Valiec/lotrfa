package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityBorLevyman extends LOTREntityBorMan
{
    private static ItemStack[] militiaWeapons;
    private static int[] leatherDyes;
    
    public LOTREntityBorLevyman(final World world) {
        super(world);
        this.addTargetTasks(true);
    }
    
    @Override
    protected EntityAIBase createDaleAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.4, true);
    }
    
    @Override
    public void setupNPCGender() {
        this.familyInfo.setMale(true);
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        int i = this.rand.nextInt(LOTREntityBorLevyman.militiaWeapons.length);
        this.npcItemsInv.setMeleeWeapon(LOTREntityBorLevyman.militiaWeapons[i].copy());
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, this.dyeLeather(new ItemStack(Items.leather_boots)));
        this.setCurrentItemOrArmor(2, this.dyeLeather(new ItemStack(Items.leather_leggings)));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyDaleGambeson));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRFAItems.borHelmet));
        }
        else {
            this.setCurrentItemOrArmor(4, (ItemStack)null);
        }
        return data;
    }
    
    private ItemStack dyeLeather(final ItemStack itemstack) {
        final int i = this.rand.nextInt(LOTREntityBorLevyman.leatherDyes.length);
        final int color = LOTREntityBorLevyman.leatherDyes[i];
        ((ItemArmor)itemstack.getItem()).func_82813_b(itemstack, color);
        return itemstack;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "houseBor/man/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "houseBor/levyman/hired";
        }
        return "houseBor/levyman/friendly";
    }
    
    static {
        LOTREntityBorLevyman.militiaWeapons = new ItemStack[] { new ItemStack(LOTRFAItems.borSword), new ItemStack(LOTRFAItems.borBattleaxe), new ItemStack(LOTRFAItems.borPike), new ItemStack(Items.iron_sword), new ItemStack(Items.iron_axe), new ItemStack(LOTRMod.battleaxeIron), new ItemStack(LOTRMod.pikeIron), new ItemStack(LOTRMod.swordBronze), new ItemStack(LOTRMod.axeBronze), new ItemStack(LOTRMod.battleaxeBronze) };
        LOTREntityBorLevyman.leatherDyes = new int[] { 7034184, 5650986, 7039851, 5331051, 2305612, 2698291, 1973790 };
    }
}
