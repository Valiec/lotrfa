package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.*;
import lotr.common.entity.npc.LOTREntityWoodElf;
import lotr.common.item.LOTRItemMug;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityLaegrimElf extends LOTREntityWoodElf{

	public LOTREntityLaegrimElf(World world) {
		super(world);
	}
	
    @Override
    protected void dropElfItems(final boolean flag, final int i) {
        super.dropElfItems(flag, i);
        if (flag) {
            int dropChance = 20 - i * 4;
            dropChance = Math.max(dropChance, 1);
            if (this.rand.nextInt(dropChance) == 0) {
                final ItemStack elfDrink = new ItemStack(LOTRMod.mugMiruvor);
                elfDrink.setItemDamage(1 + this.rand.nextInt(3));
                LOTRItemMug.setVessel(elfDrink, LOTRFoods.ELF_DRINK.getRandomVessel(this.rand), true);
                this.entityDropItem(elfDrink, 0.0f);
            }
        }
        if (this.rand.nextInt(6) == 0) {
            this.dropChestContents(LOTRFAChestContents.laegrim_house, 1, 1 + i);
        }
    }
	
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.laegrim;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killLaegrimElf;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.laegrim.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.laegrim;
    }

}
