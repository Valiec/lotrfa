package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIFarm;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.npc.LOTRFarmhand;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;

public class LOTREntityDorLominFarmhand extends LOTREntityDorLominMan implements LOTRFarmhand{

    public LOTREntityDorLominFarmhand(World world) {
        super(world);
        this.tasks.addTask(3, new LOTREntityAIFarm(this, 1.0, 1.0f));
        this.targetTasks.taskEntries.clear();
        this.addTargetTasks(false);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        int e = this.rand.nextInt(10);
        if (e == 0) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(Items.iron_hoe));
        }
        else {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.hoeBronze));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        return data;
    }

    @Override
    public void onAttackModeChange(LOTREntityNPC.AttackMode mode, boolean mounted) {
        if (mode == LOTREntityNPC.AttackMode.IDLE) {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getIdleItem());
        }
        else {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getMeleeWeapon());
        }
    }

    @Override
    public IPlantable getUnhiredSeeds() {
        return (IPlantable) Items.wheat_seeds;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.dorLomin;
    }

    @Override
    public String getSpeechBank(EntityPlayer entityplayer) {
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "dorLomin/farmhand/hired";
        }
        return "dorLomin/farmhand/neutral";
    }

}
