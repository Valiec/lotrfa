package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.*;
import lotr.common.entity.npc.LOTREntityHalfTroll;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityAngbandBoldog extends LOTREntityHalfTroll{

	public LOTREntityAngbandBoldog(World world) {
		super(world);
	}
	
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.angband;
    }
    
    @Override
    public int getTotalArmorValue() {
        return 12;
    }
    
    @Override
    protected void dropFewItems(final boolean flag, final int i) {
        super.dropFewItems(flag, i);
        if (this.rand.nextInt(6) == 0) {
            this.dropChestContents(LOTRFAChestContents.boldog_drops, 1, 2 + i);
        }
    }
   
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(45.0);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(30.0);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.2);
        this.getEntityAttribute(LOTREntityAngbandBoldog.npcAttackDamageExtra).setBaseValue(2.5);
        this.getEntityAttribute(LOTREntityAngbandBoldog.npcRangedAccuracy).setBaseValue(0.75);
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        final int i = this.rand.nextInt(4);
        if (i == 0) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.battleaxeBlackUruk));
        }
        else if (i == 1) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.hammerBlackUruk));
        }
        else if (i == 2) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.spearBlackUruk));
        }
        else if (i == 3) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.scimitarBlackUruk));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsHalfTroll));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsHalfTroll));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyHalfTroll));
        if (this.rand.nextInt(4) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetHalfTroll));
        }
        return data;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 3;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.angband.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.angband;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killBoldog;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "angband/boldog/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "angband/boldog/hired";
        }
        return "angband/boldog/friendly";
    }
}
