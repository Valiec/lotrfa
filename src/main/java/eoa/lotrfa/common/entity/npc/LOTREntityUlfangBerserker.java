package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFACapes;
import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.npc.LOTREntityDunlendingBerserker;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUlfangBerserker extends LOTREntityUlfangWarrior {

    public LOTREntityUlfangBerserker(World world) {
        super(world);
        this.npcShield = null;
        this.spawnRidingHorse = false;
        this.npcCape = LOTRFACapes.houseUlfang;
    }
    
    @Override
    public EntityAIBase getDunlendingAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.7, false);
    }
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(30.0);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(24.0);
        this.getEntityAttribute(LOTREntityDunlendingBerserker.npcAttackDamageExtra).setBaseValue(2.0);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.ulfangBattleaxe));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsGundabadUruk));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsGundabadUruk));
        if(this.rand.nextBoolean()) this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyBone));
        else this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyGundabadUruk));
        this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetMoredainLion));
        return data;
    }
    
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.houseUlfang;
    }
}
