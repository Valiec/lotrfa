package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRFaction;
import lotr.common.entity.npc.LOTREntityGondorMan;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityBrethilMan extends LOTREntityGondorMan {

    public LOTREntityBrethilMan(World world) {
        super(world);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.brethil;
    }

    @Override
    protected void dropFewItems(final boolean flag, final int i) {
        super.dropFewItems(flag, i);
        for (int bones = this.rand.nextInt(2) + this.rand.nextInt(i + 1), l = 0; l < bones; ++l) {
            this.dropItem(Items.bone, 1);
        }
        this.dropGondorItems(flag, i);
    }

    @Override
    protected void dropGondorItems(final boolean flag, final int i) {
        if (this.rand.nextInt(6) == 0) {
            this.dropChestContents(LOTRFAChestContents.brethil_house, 1, 2 + i);
        }
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.brethilDagger));
        return data;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killBrethilMan;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.brethil.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.brethil;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "brethil/man/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "brethil/man/hired";
        }
        return "brethil/man/friendly";
    }
}
