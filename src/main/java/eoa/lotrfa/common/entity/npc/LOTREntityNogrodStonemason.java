package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.entity.LOTRFATrades;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.*;
import lotr.common.entity.npc.LOTRTradeEntries.TradeType;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityNogrodStonemason extends LOTREntityBlueDwarf implements LOTRTradeable {

    public LOTREntityNogrodStonemason(World world) {
        super(world);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return this.isFriendly(entityplayer);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.nogrod_stonemason_buy;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setIdleItem(new ItemStack(LOTRMod.rock, 1, 3));
        // this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        return data;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        // TODO Auto-generated method stub
        return LOTRFATrades.nogrod_stonemason_sell;
    }

    @Override
    public void onPlayerTrade(EntityPlayer arg0, TradeType arg1, ItemStack arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean shouldTraderRespawn() {
        // TODO Auto-generated method stub
        return true;
    }

}
