package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItemBanner;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.entity.animal.LOTREntityHorse;
import lotr.common.entity.npc.LOTRBannerBearer;
import lotr.common.entity.npc.LOTRNPCMount;
import lotr.common.item.LOTRItemBanner;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityFeanorianBannerBearer extends LOTREntityFeanorianWarrior implements LOTRBannerBearer {

    public LOTREntityFeanorianBannerBearer(World world) {
        super(world);
        this.npcShield = LOTRFAShields.alignmentFeanorians;
        this.spawnRidingHorse = this.rand.nextInt(4) == 0;
        this.tasks.addTask(2, this.meleeAttackAI);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.feanorians;
    }

    @Override
    public LOTRNPCMount createMountToRide() {
        LOTREntityHorse horse = (LOTREntityHorse) super.createMountToRide();
        horse.setMountArmor(new ItemStack(LOTRFAItems.feanorianHorseArmor));
        return horse;
    }

    @Override
    public LOTRItemBanner.BannerType getBannerType() {
        return LOTRFAItemBanner.FABannerTypes.feanorian;
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianBattlestaff));
        this.npcItemsInv.setMeleeWeaponMounted(new ItemStack(LOTRFAItems.feanorianBattlestaff));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.npcItemsInv.setIdleItemMounted(new ItemStack(LOTRFAItems.feanorianBattlestaff));
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRFAItems.feanorianBoots));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRFAItems.feanorianLeggings));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRFAItems.feanorianChestplate));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRFAItems.feanorianHelmet));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }
}
