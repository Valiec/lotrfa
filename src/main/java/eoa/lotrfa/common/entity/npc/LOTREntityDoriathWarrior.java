package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAShields;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.ai.LOTREntityAIRangedAttack;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDoriathWarrior extends LOTREntityDoriathElf{
    
    public LOTREntityDoriathWarrior(final World world) {
        super(world);
        this.tasks.addTask(2, this.meleeAttackAI);
        this.spawnRidingHorse = (this.rand.nextInt(4) == 0);
        this.npcShield = LOTRFAShields.alignmentDoriath;
    }
    
    @Override
    protected EntityAIBase createElfRangedAttackAI() {
        return new LOTREntityAIRangedAttack(this, 1.25, 30, 40, 24.0f);
    }
    
    @Override
    protected EntityAIBase createElfMeleeAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.4, false);
    }
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(24.0);
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        final int i = this.rand.nextInt(6);
        if (i == 0) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.polearmElven));
        }
        else if (i == 1) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.longspearElven));
        }
    	else if (i == 2 || i == 3 || i == 4) {
    		this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.doriathBattleaxe));
    	}
        else {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.swordElven));
        }
        this.npcItemsInv.setRangedWeapon(new ItemStack(LOTRMod.elvenBow));
        if (this.rand.nextInt(5) == 0) {
            this.npcItemsInv.setSpearBackup(this.npcItemsInv.getMeleeWeapon());
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.spearElven));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsElven));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsElven));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyElven));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetElven));
        }
        else {
            this.setCurrentItemOrArmor(4, null);
        }
        return data;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "doriath/elf/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "doriath/elf/hired";
        }
        return "doriath/warrior/friendly";
    }
}
