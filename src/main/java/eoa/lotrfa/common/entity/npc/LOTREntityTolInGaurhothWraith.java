package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRFaction;
import lotr.common.entity.npc.LOTREntityBarrowWight;
import lotr.common.quest.LOTRMiniQuest;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.init.Items;
import net.minecraft.world.World;

public class LOTREntityTolInGaurhothWraith extends LOTREntityBarrowWight {

    public LOTREntityTolInGaurhothWraith(World world) {
        super(world);
    }
    
    @Override
    protected void dropFewItems(final boolean flag, final int i) {
        super.dropFewItems(flag, i);
        for (int bones = 1 + this.rand.nextInt(3) + this.rand.nextInt(i + 1), l = 0; l < bones; ++l) {
            this.dropItem(Items.bone, 1);
        }
        if (this.rand.nextBoolean()) {
            this.dropChestContents(LOTRFAChestContents.wraith_drops, 1, 2 + i + 1);
        }
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(35.0);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(20.0);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.tolInGaurhoth;
    }

    @Override
    public int getAlignmentBonus() {
        return 3;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killWraith;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.tolInGaurhothWraith.createQuest(this);
    }
}
