package eoa.lotrfa.common.entity.npc;

import java.util.UUID;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIRangedAttack;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class LOTREntityLaegrimScout extends LOTREntityLaegrimElf{
	
	private static final UUID scoutArmorSpeedBoost_id;
    public static final AttributeModifier scoutArmorSpeedBoost;
    
    public LOTREntityLaegrimScout(final World world) {
        super(world);
        this.tasks.addTask(2, this.rangedAttackAI);
    }
    
    @Override
    protected EntityAIBase createElfRangedAttackAI() {
        return new LOTREntityAIRangedAttack(this, 1.25, 25, 35, 24.0f);
    }
    
    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(24.0);
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getRangedWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsWoodElvenScout));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsWoodElvenScout));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyWoodElvenScout));
        if (this.rand.nextInt(10) != 0) {
            this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetWoodElvenScout));
        }
        return data;
    }
    
    @Override
    public void onLivingUpdate() {
        super.onLivingUpdate();
        if (!this.worldObj.isRemote && this.isEntityAlive() && this.ridingEntity == null) {
            final ItemStack currentItem = this.getEquipmentInSlot(0);
            if (currentItem != null && currentItem.getItem() instanceof ItemBow) {
                final EntityLivingBase lastAttacker = this.getAITarget();
                if (lastAttacker != null && this.getDistanceSqToEntity(lastAttacker) < 16.0 && this.rand.nextInt(20) == 0) {
                    for (int l = 0; l < 32; ++l) {
                        final int i = MathHelper.floor_double(this.posX) - this.rand.nextInt(16) + this.rand.nextInt(16);
                        final int j = MathHelper.floor_double(this.posY) - this.rand.nextInt(3) + this.rand.nextInt(3);
                        final int k = MathHelper.floor_double(this.posZ) - this.rand.nextInt(16) + this.rand.nextInt(16);
                        if (this.getDistance(i, j, k) > 6.0 && this.worldObj.getBlock(i, j - 1, k).isNormalCube() && !this.worldObj.getBlock(i, j, k).isNormalCube() && !this.worldObj.getBlock(i, j + 1, k).isNormalCube()) {
                            final double d = i + 0.5;
                            final double d2 = j;
                            final double d3 = k + 0.5;
                            final AxisAlignedBB aabb = this.boundingBox.copy().offset(d - this.posX, d2 - this.posY, d3 - this.posZ);
                            if (this.worldObj.checkNoEntityCollision(aabb) && this.worldObj.getCollidingBoundingBoxes(this, aabb).isEmpty() && !this.worldObj.isAnyLiquid(aabb)) {
                                this.doTeleportEffects();
                                this.setPosition(d, d2, d3);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void doTeleportEffects() {
        this.worldObj.playSoundAtEntity(this, "lotr:elf.woodElf_teleport", this.getSoundVolume(), 0.5f + this.rand.nextFloat());
        this.worldObj.setEntityState(this, (byte)15);
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void handleHealthUpdate(final byte b) {
        if (b == 15) {
            for (int i = 0; i < 16; ++i) {
                final double d = this.posX + (this.rand.nextDouble() - 0.5) * this.width;
                final double d2 = this.posY + this.rand.nextDouble() * this.height;
                final double d3 = this.posZ + (this.rand.nextDouble() - 0.5) * this.width;
                final double d4 = -0.05 + this.rand.nextFloat() * 0.1f;
                final double d5 = -0.05 + this.rand.nextFloat() * 0.1f;
                final double d6 = -0.05 + this.rand.nextFloat() * 0.1f;
                LOTRMod.proxy.spawnParticle("leafGreen_" + (20 + this.rand.nextInt(30)), d, d2, d3, d4, d5, d6);
            }
        }
        else {
            super.handleHealthUpdate(b);
        }
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "woodElf/warrior/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "woodElf/elf/hired";
        }
        if (LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 50) {
            return "woodElf/warrior/friendly";
        }
        return "woodElf/elf/neutral";
    }
    
    static {
        scoutArmorSpeedBoost_id = UUID.fromString("cf0ceb91-0f13-4788-be0e-a6c67a830308");
        scoutArmorSpeedBoost = new AttributeModifier(LOTREntityLaegrimScout.scoutArmorSpeedBoost_id, "WE Scout armor speed boost", 0.3, 2).setSaved(false);
    }
}

