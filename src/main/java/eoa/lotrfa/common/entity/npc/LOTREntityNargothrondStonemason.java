package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import lotr.common.LOTRLevelData;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityNargothrondStonemason extends LOTREntityNargothrondElf implements LOTRTradeable {

    public LOTREntityNargothrondStonemason(World world) {
        super(world);
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return this.isFriendly(entityplayer);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.nargothrond_stonemason_buy;
    }

    @Override
    public boolean shouldRenderNPCHair() {
        return false;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setIdleItem(new ItemStack(Blocks.stone));
        return data;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRFATrades.nargothrond_stonemason_sell;
    }

    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeNargothrondTrader);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

}
