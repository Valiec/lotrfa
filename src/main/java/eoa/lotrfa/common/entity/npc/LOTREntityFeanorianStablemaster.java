package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import eoa.lotrfa.common.entity.LOTRFAUnitTrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRLevelData;
import lotr.common.entity.npc.*;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityFeanorianStablemaster extends LOTREntityFeanorianLord implements LOTRTradeable {

    public LOTREntityFeanorianStablemaster(World world) {
        super(world);
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return this.isFriendly(entityplayer);
    }

    @Override
    public boolean shouldRenderNPCHair() {
        return false;
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.feanorian_stablemaster_buy;
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianDagger));
        this.npcItemsInv.setIdleItem(new ItemStack(Items.lead));
        this.setCurrentItemOrArmor(1, null);
        this.setCurrentItemOrArmor(2, null);
        this.setCurrentItemOrArmor(3, null);
        this.setCurrentItemOrArmor(4, null);
        return data;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRFATrades.feanorian_stablemaster_sell;
    }
    
    @Override
    public LOTRUnitTradeEntries getUnits() {
        return LOTRFAUnitTrades.feanorian_stablemaster_units;
    }

    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeFeanorianTrader);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

}
