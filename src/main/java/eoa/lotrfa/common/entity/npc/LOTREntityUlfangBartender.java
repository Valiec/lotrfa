package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import lotr.common.*;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityUlfangBartender extends LOTREntityUlfangMan implements LOTRTradeable.Bartender
{
    public LOTREntityUlfangBartender(final World world) {
        super(world);
        this.addTargetTasks(false);
        this.npcLocationName = "entity.lotr.DunlendingBartender.locationName";
    }
    
    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRTradeEntries.DUNLENDING_BARTENDER_BUY;
    }
    
    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRTradeEntries.DUNLENDING_BARTENDER_SELL;
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setIdleItem(new ItemStack(LOTRMod.mug));
        return data;
    }
    
    @Override
    public void dropDunlendingItems(boolean flag, int i) {
        for (int j = this.rand.nextInt(3) + this.rand.nextInt(i + 1), k = 0; k < j; ++k) {
            int l = this.rand.nextInt(4);
            
            if(l == 0) {
                Item food = LOTRFoods.DUNLENDING.getRandomFood(this.rand).getItem();
                this.entityDropItem(new ItemStack(food), 0.0f);
                break;
                
            }
            else if(l == 1) {
                Item drink = LOTRFoods.DUNLENDING_DRINK.getRandomFood(this.rand).getItem();
                this.entityDropItem(new ItemStack(drink, 1, 1 + this.rand.nextInt(3)), 0.0f);
                
            }
            else if(l == 2) {
                this.entityDropItem(new ItemStack(LOTRMod.mug), 0.0f);
                
            }
            else if(l == 3) {
                this.entityDropItem(new ItemStack(Items.gold_nugget, 2 + this.rand.nextInt(3)), 0.0f);
            }
        }
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return this.isFriendly(entityplayer);
    }
    
    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeUlfangTrader);
    }
    
    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "houseUlfang/trader/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "houseUlfang/bartender/friendly";
        }
        return "houseUlfang/smith/neutral";
    }
}
