package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDorLominBrewer extends LOTREntityDorLominMan implements LOTRTradeable {

    public LOTREntityDorLominBrewer(World world) {
        super(world);
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return this.isFriendly(entityplayer);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.dor_lomin_brewer_buy;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        // this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.blacksmithHammer));
        this.npcItemsInv.setIdleItem(new ItemStack(LOTRMod.mugAle));
        return data;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRFATrades.dor_lomin_brewer_sell;
    }

    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeDorLominTrader);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "dorLomin/trader/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "dorLomin/brewer/friendly";
        }
        return "dorLomin/brewer/neutral";
    }
}
