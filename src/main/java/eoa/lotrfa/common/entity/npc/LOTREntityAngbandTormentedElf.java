package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAMiniQuests;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.*;
import lotr.common.entity.npc.LOTREntityTormentedElf;
import lotr.common.quest.LOTRMiniQuest;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityAngbandTormentedElf extends LOTREntityTormentedElf {

    public LOTREntityAngbandTormentedElf(World world) {
        super(world);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFaction.UNALIGNED;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRAchievement.killTormentedElf;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        final int i = this.rand.nextInt(7);
        if (i == 0 || i == 1) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.pickaxeOrc));
        }
        else if (i == 2) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.pickaxeUruk));
        }
        else if (i == 3) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianPickaxe));
        }
        else if (i == 4) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.pickaxeHighElven));
        }
    	else if (i == 5) {
    		this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.daggerHighElven));
    	}
    	else if (i == 6) {
    		this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianDagger));
    	}
        else if (i == 7) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.daggerBlackUruk));
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        return data;
    }

    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.angbandTormentedElves.createQuest(this);
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "angband/tormentedElf/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "angband/tormentedElf/hired";
        }
        return "angband/tormentedElf/friendly";
    }
}
