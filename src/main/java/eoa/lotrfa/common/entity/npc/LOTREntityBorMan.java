package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRFaction;
import lotr.common.entity.npc.LOTREntityDaleMan;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityBorMan extends LOTREntityDaleMan{

	public LOTREntityBorMan(World world) {
		super(world);
	}
	
    @Override
    protected void dropFewItems(final boolean flag, final int i) {
        super.dropFewItems(flag, i);
        for (int bones = this.rand.nextInt(2) + this.rand.nextInt(i + 1), l = 0; l < bones; ++l) {
            this.dropItem(Items.bone, 1);
        }
        this.dropDaleItems(flag, i);
    }
    
    @Override
    protected void dropDaleItems(final boolean flag, final int i) {
        if (this.rand.nextInt(6) == 0) {
            this.dropChestContents(LOTRFAChestContents.house_bor_house, 1, 2 + i);
        }
    }
	
    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.houseBor;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killBorMan;
    }
    
    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.borDagger));
        return data;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.houseBor.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.houseBor;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "houseBor/man/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "houseBor/man/hired";
        }
        return "houseBor/man/friendly";
    }
}
