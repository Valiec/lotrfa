package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAFactions;
import eoa.lotrfa.common.LOTRFAMiniQuests;
import lotr.common.*;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.ai.LOTREntityAIFollowHiringPlayer;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.world.LOTRWorldProviderUtumno;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class LOTREntityAngbandBalrog extends LOTREntityNPC {
    public LOTREntityAngbandBalrog(World world) {
        super(world);
        this.setSize(1.8f, 3.75f);
        this.getNavigator().setAvoidsWater(true);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(1, new LOTREntityAIAttackOnCollide(this, 1.6, false));
        this.tasks.addTask(2, new LOTREntityAIFollowHiringPlayer(this));
        this.tasks.addTask(3, new EntityAIWander(this, 1.0));
        this.tasks.addTask(4, new EntityAIWatchClosest2(this, EntityPlayer.class, 24.0f, 0.02f));
        this.tasks.addTask(4, new EntityAIWatchClosest2(this, LOTREntityNPC.class, 16.0f, 0.02f));
        this.tasks.addTask(5, new EntityAIWatchClosest(this, EntityLiving.class, 12.0f, 0.02f));
        this.tasks.addTask(6, new EntityAILookIdle(this));
        this.addTargetTasks(true);
        this.spawnsInDarkness = true;
        this.isImmuneToFire = true;
    }

    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.angbandBalrog.createQuest(this);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        if (this.rand.nextBoolean()) {
            this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.balrogWhip));
        }
        else {
            int i = this.rand.nextInt(3);
            if (i == 0) {
                this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.scimitarBlackUruk));
            }
            else if (i == 1) {
                this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.battleaxeBlackUruk));
            }
            else if (i == 2) {
                this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.hammerBlackUruk));
            }
        }
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        return data;
    }

    @Override
    protected void onAttackModeChange(LOTREntityNPC.AttackMode mode, boolean mounted) {
        if (mode == LOTREntityNPC.AttackMode.IDLE) {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getIdleItem());
        }
        else {
            this.setCurrentItemOrArmor(0, this.npcItemsInv.getMeleeWeapon());
        }
    }

    @Override
    public int getAlignmentBonus() {
        return 4;
    }

    @Override
    public void onLivingUpdate() {
        super.onLivingUpdate();
        if (this.getHealth() < this.getMaxHealth() && this.ticksExisted % 10 == 0) {
            this.heal(1.0f);
        }
        if (this.isWreathedInFlame()) {
            if (!this.worldObj.isRemote && this.rand.nextInt(80) == 0) {
                boolean isUtumno = this.worldObj.provider instanceof LOTRWorldProviderUtumno;
                for (int l = 0; l < 24; ++l) {
                    int i = MathHelper.floor_double(this.posX);
                    int j = MathHelper.floor_double(this.boundingBox.minY);
                    int k = MathHelper.floor_double(this.posZ);
                    Block block = this.worldObj.getBlock(i += MathHelper.getRandomIntegerInRange(this.rand, -8, 8), j += MathHelper.getRandomIntegerInRange(this.rand, -4, 8), k += MathHelper.getRandomIntegerInRange(this.rand, -8, 8));
                    float maxResistance = Blocks.obsidian.getExplosionResistance(this);
                    if (!block.isReplaceable(this.worldObj, i, j, k) && (!isUtumno || block.getExplosionResistance(this) > maxResistance) || !Blocks.fire.canPlaceBlockAt(this.worldObj, i, j, k)) continue;
                    this.worldObj.setBlock(i, j, k, Blocks.fire, 0, 3);
                }
            }
            for (int l = 0; l < 3; ++l) {
                String s = this.rand.nextInt(3) == 0 ? "flame" : "largesmoke";
                this.worldObj.spawnParticle(s, this.posX + (this.rand.nextDouble() - 0.5) * this.width * 2.0, this.posY + 0.5 + this.rand.nextDouble() * this.height * 1.5, this.posZ + (this.rand.nextDouble() - 0.5) * this.width * 2.0, 0.0, 0.0, 0.0);
            }
        }
    }

    public boolean isWreathedInFlame() {
        return this.isEntityAlive() && !this.isWet();
    }

    @Override
    public void knockBack(Entity entity, float f, double d, double d1) {
        super.knockBack(entity, f, d, d1);
        this.motionX /= 4.0;
        this.motionY /= 4.0;
        this.motionZ /= 4.0;
    }

    @Override
    public boolean attackEntityFrom(DamageSource damagesource, float f) {
        if (damagesource == DamageSource.fall) {
            return false;
        }
        return super.attackEntityFrom(damagesource, f);
    }

    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRAchievement.killBalrog;
    }

    @Override
    protected int getExperiencePoints(EntityPlayer entityplayer) {
        return 15 + this.rand.nextInt(10);
    }

    @Override
    public void dropNPCEquipment(boolean flag, int i) {
        ItemStack heldItem;
        if (flag && this.rand.nextInt(3) == 0 && (heldItem = this.getHeldItem()) != null) {
            this.entityDropItem(heldItem, 0.0f);
            this.setCurrentItemOrArmor(0, null);
        }
        super.dropNPCEquipment(flag, i);
    }

    @Override
    public void onDeath(DamageSource damagesource) {
        if (!this.worldObj.isRemote) {
            int exRange = 8;
            int i = MathHelper.floor_double(this.posX);
            int j = MathHelper.floor_double(this.posY);
            int k = MathHelper.floor_double(this.posZ);
            for (int i1 = i - exRange; i1 <= i + exRange; ++i1) {
                for (int j1 = j - exRange; j1 <= j + exRange; ++j1) {
                    for (int k1 = k - exRange; k1 <= k + exRange; ++k1) {
                        Block block = this.worldObj.getBlock(i1, j1, k1);
                        if (block.getMaterial() != Material.fire) continue;
                        this.worldObj.setBlockToAir(i1, j1, k1);
                    }
                }
            }
        }
        else {
            for (int l = 0; l < 100; ++l) {
                double d = this.posX + this.width * MathHelper.randomFloatClamp(this.rand, -1.0f, 1.0f);
                double d1 = this.posY + this.height * MathHelper.randomFloatClamp(this.rand, 0.0f, 1.0f);
                double d2 = this.posZ + this.width * MathHelper.randomFloatClamp(this.rand, -1.0f, 1.0f);
                double d3 = this.rand.nextGaussian() * 0.03;
                double d4 = this.rand.nextGaussian() * 0.03;
                double d5 = this.rand.nextGaussian() * 0.03;
                if (this.rand.nextInt(3) == 0) {
                    this.worldObj.spawnParticle("explode", d, d1, d2, d3, d4, d5);
                    continue;
                }
                this.worldObj.spawnParticle("flame", d, d1, d2, d3, d4, d5);
            }
        }
        super.onDeath(damagesource);
        this.playSound(this.getHurtSound(), this.getSoundVolume() * 2.0f, this.getSoundPitch() * 0.75f);
    }

    @Override
    public String getLivingSound() {
        return "lotr:troll.say";
    }

    @Override
    public String getHurtSound() {
        return "lotr:troll.say";
    }

    @Override
    protected float getSoundVolume() {
        return 1.5f;
    }

    @Override
    protected void func_145780_a(int i, int j, int k, Block block) {
        this.playSound("lotr:troll.step", 0.75f, this.getSoundPitch());
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(70.0);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(40.0);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.25);
        this.getEntityAttribute(npcAttackDamage).setBaseValue(5.0);
    }

    @Override
    public boolean attackEntityAsMob(Entity entity) {
        if (super.attackEntityAsMob(entity)) {
            if (this.getHeldItem() == null) {
                entity.setFire(3);
            }
            float attackDamage = (float) this.getEntityAttribute(LOTREntityNPC.npcAttackDamage).getAttributeValue();
            float knockbackModifier = 0.25f * attackDamage;
            entity.addVelocity((-MathHelper.sin(this.rotationYaw * 3.1415927f / 180.0f)) * knockbackModifier * 0.5f, knockbackModifier * 0.1, MathHelper.cos(this.rotationYaw * 3.1415927f / 180.0f) * knockbackModifier * 0.5f);
            return true;
        }
        return false;
    }

    @Override
    protected void dropFewItems(boolean flag, int i) {
        super.dropFewItems(flag, i);
        int coals = MathHelper.getRandomIntegerInRange(this.rand, 4, 16 + i * 8);
        for (int l = 0; l < coals; ++l) {
            this.dropItem(Items.coal, 1);
        }
    }

    @Override
    public String getSpeechBank(EntityPlayer entityplayer) {
        if (this.isFriendly(entityplayer)) {
            if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
                return "angband/balrog/hired";
            }
            if (LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 100) {
                return "angband/balrog/friendly";
            }
            return "angband/balrog/neutral";
        }
        return "angband/balrog/hostile";
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.angband;
    }
}
