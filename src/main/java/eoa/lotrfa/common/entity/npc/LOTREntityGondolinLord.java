package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFACapes;
import eoa.lotrfa.common.entity.LOTRFAUnitTrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.world.spawning.LOTRFAInvasions;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRMod;
import lotr.common.entity.animal.LOTREntityHorse;
import lotr.common.entity.npc.*;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityGondolinLord extends LOTREntityGondolinWarrior implements LOTRUnitTradeable{

    public LOTREntityGondolinLord(World world) {
        super(world);
        this.addTargetTasks(false);
        this.npcCape = LOTRFACapes.gondolin;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.swordGondolin));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, new ItemStack(LOTRMod.bootsGondolin));
        this.setCurrentItemOrArmor(2, new ItemStack(LOTRMod.legsGondolin));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyGondolin));
        this.setCurrentItemOrArmor(4, null);
        return data;
    }
    
    @Override
    public int getAlignmentBonus() {
        return 5;
    }

    @Override
    public LOTRUnitTradeEntries getUnits() {
        return LOTRFAUnitTrades.gondolin_units;
    }

    @Override
    public LOTRInvasions getConquestHorn() {
        return LOTRFAInvasions.gondolin;
    }
    
    @Override
    public LOTRNPCMount createMountToRide() {
        LOTREntityHorse horse = (LOTREntityHorse) super.createMountToRide();
        horse.setMountArmor(new ItemStack(LOTRFAItems.gondolinHorseArmor));
        return horse;
    }
    
    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 300 && this.isFriendly(entityplayer);
    }
    
    @Override
    public void onUnitTrade(final EntityPlayer entityplayer) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeGondolinLord);
    }
    
    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "highElf/warrior/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "highElf/lord/friendly";
        }
        return "highElf/lord/neutral";
    }

}
