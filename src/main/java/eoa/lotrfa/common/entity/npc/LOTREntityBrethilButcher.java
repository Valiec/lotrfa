package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import lotr.common.LOTRLevelData;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityBrethilButcher extends LOTREntityBrethilMan implements LOTRTradeable {

    public LOTREntityBrethilButcher(World world) {
        super(world);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return this.isFriendly(entityplayer);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        // TODO Auto-generated method stub
        return LOTRFATrades.brethil_butcher_buy;
    }
    
    @Override
    public LOTRTradeEntries getSellPool() {
        // TODO Auto-generated method stub
        return LOTRFATrades.brethil_butcher_sell;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.brethilDagger));
        this.npcItemsInv.setIdleItem(new ItemStack(Items.porkchop));
        return data;
    }

    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeBrethilTrader);
    }

    @Override
    public boolean shouldTraderRespawn() {
        // TODO Auto-generated method stub
        return true;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "brethil/butcher/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "brethil/butcher/friendly";
        }
        return "brethil/butcher/neutral";
    }
}
