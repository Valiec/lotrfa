package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityBrethilLevyman extends LOTREntityBrethilMan {

    private static ItemStack[] militiaWeapons;
    private static int[] leatherDyes;

    public LOTREntityBrethilLevyman(final World world) {
        super(world);
        this.addTargetTasks(true);
    }

    @Override
    protected EntityAIBase createGondorAttackAI() {
        return new LOTREntityAIAttackOnCollide(this, 1.4, true);
    }

    @Override
    public void setupNPCGender() {
        this.familyInfo.setMale(true);
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        int i = this.rand.nextInt(LOTREntityBrethilLevyman.militiaWeapons.length);
        this.npcItemsInv.setMeleeWeapon(LOTREntityBrethilLevyman.militiaWeapons[i].copy());
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        this.setCurrentItemOrArmor(1, this.dyeLeather(new ItemStack(Items.leather_boots)));
        this.setCurrentItemOrArmor(2, this.dyeLeather(new ItemStack(Items.leather_leggings)));
        this.setCurrentItemOrArmor(3, new ItemStack(LOTRMod.bodyGondorGambeson));
        if (this.rand.nextInt(3) != 0) {
            this.setCurrentItemOrArmor(4, (ItemStack) null);
        }
        else {
            i = this.rand.nextInt(3);
            if (i == 0) {
                this.setCurrentItemOrArmor(4, new ItemStack(LOTRMod.helmetBlackroot));
            }
            else if (i == 1) {
                this.setCurrentItemOrArmor(4, new ItemStack(Items.iron_helmet));
            }
            else if (i == 2) {
                this.setCurrentItemOrArmor(4, this.dyeLeather(new ItemStack(Items.leather_helmet)));
            }
        }
        return data;
    }

    private ItemStack dyeLeather(final ItemStack itemstack) {
        final int i = this.rand.nextInt(LOTREntityBrethilLevyman.leatherDyes.length);
        final int color = LOTREntityBrethilLevyman.leatherDyes[i];
        ((ItemArmor) itemstack.getItem()).func_82813_b(itemstack, color);
        return itemstack;
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }

    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "gondor/soldier/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "gondor/soldier/hired";
        }
        return "gondor/soldier/friendly";
    }

    static {
        LOTREntityBrethilLevyman.militiaWeapons = new ItemStack[] {new ItemStack(LOTRFAItems.brethilSword), new ItemStack(LOTRFAItems.brethilBattleaxe), new ItemStack(LOTRFAItems.brethilPike), new ItemStack(Items.iron_sword), new ItemStack(Items.iron_axe), new ItemStack(LOTRMod.battleaxeIron), new ItemStack(LOTRMod.pikeIron), new ItemStack(LOTRMod.swordBronze), new ItemStack(LOTRMod.axeBronze), new ItemStack(LOTRMod.battleaxeBronze)};
        LOTREntityBrethilLevyman.leatherDyes = new int[] {10855845, 8026746, 5526612, 3684408, 8350297, 10388590, 4799795, 5330539, 4211801, 2632504};
    }

}