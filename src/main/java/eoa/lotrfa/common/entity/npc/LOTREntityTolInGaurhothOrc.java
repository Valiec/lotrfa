package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRFaction;
import lotr.common.entity.npc.LOTREntityDolGuldurOrc;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class LOTREntityTolInGaurhothOrc extends LOTREntityDolGuldurOrc {

    public LOTREntityTolInGaurhothOrc(World world) {
        super(world);
        this.setSize(0.6f, 1.8f);
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.tolInGaurhoth;
    }
    
    @Override
    protected void dropOrcItems(final boolean flag, final int i) {
        if (this.rand.nextInt(6) == 0) {
            this.dropChestContents(LOTRFAChestContents.tol_in_gaurhoth_tent, 1, 2 + i);
        }
    }

    @Override
    public boolean canOrcSkirmish() {
        return false;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killTolInGaurhothOrc;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.tolInGaurhoth.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.tolInGaurhoth;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "tolInGaurhoth/orc/hostile";
        }
        if (this.hiredNPCInfo.getHiringPlayer() == entityplayer) {
            return "tolInGaurhoth/orc/hired";
        }
        return "tolInGaurhoth/orc/friendly";
    }

}
