package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.*;
import eoa.lotrfa.common.entity.npc.ai.LOTRFAEntityAINearestAttackableTargetFeanorian;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.item.story.LOTRFAItemSilmaril;
import eoa.lotrfa.common.world.structure.LOTRFAChestContents;
import lotr.common.*;
import lotr.common.entity.animal.LOTREntityHorse;
import lotr.common.entity.npc.LOTREntityHighElf;
import lotr.common.entity.npc.LOTRNPCMount;
import lotr.common.item.LOTRItemMug;
import lotr.common.quest.LOTRMiniQuest;
import lotr.common.quest.LOTRMiniQuestFactory;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityFeanorianElf extends LOTREntityHighElf {

    public LOTREntityFeanorianElf(World world) {
        super(world);
        this.addTargetTasks(true, LOTRFAEntityAINearestAttackableTargetFeanorian.class);
    }
    
    @Override
    protected void dropElfItems(final boolean flag, final int i) {
        super.dropElfItems(flag, i);
        if (flag) {
            int dropChance = 20 - i * 4;
            dropChance = Math.max(dropChance, 1);
            if (this.rand.nextInt(dropChance) == 0) {
                final ItemStack elfDrink = new ItemStack(LOTRMod.mugMiruvor);
                elfDrink.setItemDamage(1 + this.rand.nextInt(3));
                LOTRItemMug.setVessel(elfDrink, LOTRFoods.ELF_DRINK.getRandomVessel(this.rand), true);
                this.entityDropItem(elfDrink, 0.0f);
            }
        }
        if (this.rand.nextInt(6) == 0) {
            this.dropChestContents(LOTRFAChestContents.feanorian_house, 1, 1 + i);
        }
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.feanorians;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRFAItems.feanorianDagger));
        this.npcItemsInv.setRangedWeapon(new ItemStack(LOTRFAItems.feanorianBow));
        this.npcItemsInv.setIdleItem(null);
        return data;
    }

    @Override
    public LOTRNPCMount createMountToRide() {
        LOTREntityHorse horse = (LOTREntityHorse) super.createMountToRide();
        horse.setMountArmor(new ItemStack(LOTRFAItems.feanorianHorseArmor));
        return horse;
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        return LOTRFAAchievements.killFeanorianElf;
    }
    
    @Override
    public LOTRMiniQuest createMiniQuest() {
        return LOTRFAMiniQuests.feanorian.createQuest(this);
    }

    @Override
    public LOTRMiniQuestFactory getBountyHelpSpeechDir() {
        return LOTRFAMiniQuests.feanorian;
    }
    
    public boolean isSilmarilTarget(EntityPlayer player) {
        for(ItemStack item : player.inventory.mainInventory) {
            if(item != null && item.getItem() instanceof LOTRFAItemSilmaril && LOTRLevelData.getData(player).getAlignment(LOTRFAFactions.feanorians) <= 500) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "feanorian/elf/hostile";
        }
        if (isSilmarilTarget(entityplayer)) {
            return "feanorian/elf/silmaril";
        }
        return "feanorian/elf/neutral";
    }
}
