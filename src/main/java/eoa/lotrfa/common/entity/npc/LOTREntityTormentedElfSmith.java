package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.entity.LOTRFATrades;
import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRFaction;
import lotr.common.LOTRMod;
import lotr.common.entity.npc.LOTREntityTormentedElf;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeEntries.TradeType;
import lotr.common.entity.npc.LOTRTradeable.Smith;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityTormentedElfSmith extends LOTREntityTormentedElf implements Smith {

    public LOTREntityTormentedElfSmith(World world) {
        super(world);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean canTradeWith(EntityPlayer arg0) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.tormented_elf_smith_buy;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRFATrades.tormented_elf_smith_sell;
    }

    @Override
    public IEntityLivingData onSpawnWithEgg(IEntityLivingData data) {
        data = super.onSpawnWithEgg(data);
        this.npcItemsInv.setMeleeWeapon(new ItemStack(LOTRMod.blacksmithHammer));
        this.npcItemsInv.setIdleItem(this.npcItemsInv.getMeleeWeapon());
        return data;
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFaction.UNALIGNED;
    }

    @Override
    public void onPlayerTrade(EntityPlayer arg0, TradeType arg1, ItemStack arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

}
