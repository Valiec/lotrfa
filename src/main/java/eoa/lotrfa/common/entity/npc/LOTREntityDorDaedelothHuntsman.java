package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.entity.LOTRFATrades;
import lotr.common.LOTRLevelData;
import lotr.common.entity.npc.LOTRTradeEntries;
import lotr.common.entity.npc.LOTRTradeable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LOTREntityDorDaedelothHuntsman extends LOTREntityDorDaedelothOrc implements LOTRTradeable {

    public LOTREntityDorDaedelothHuntsman(World world) {
        super(world);
    }

    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRFATrades.dor_daedeloth_huntsman_buy;
    }

    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRFATrades.dor_daedeloth_huntsman_sell;
    }

    @Override
    public boolean canTradeWith(EntityPlayer entityplayer) {
        return this.isFriendly(entityplayer);
    }

    @Override
    public void onPlayerTrade(EntityPlayer entityplayer, LOTRTradeEntries.TradeType type, ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeDorDaedelothOrcTrader);
    }

    @Override
    public boolean shouldTraderRespawn() {
        return true;
    }

    @Override
    public String getSpeechBank(EntityPlayer entityplayer) {
        if (this.isFriendly(entityplayer)) {
            if (this.canTradeWith(entityplayer)) {
                return "dorDaedeloth/trader/friendly";
            }
            return "dorDaedeloth/trader/neutral";
        }
        return "dorDaedeloth/trader/hostile";
    }
}
