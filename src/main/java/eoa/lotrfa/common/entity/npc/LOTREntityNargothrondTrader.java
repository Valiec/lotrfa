package eoa.lotrfa.common.entity.npc;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFACapes;
import lotr.common.LOTRLevelData;
import lotr.common.entity.ai.LOTREntityAIAttackOnCollide;
import lotr.common.entity.npc.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class LOTREntityNargothrondTrader extends LOTREntityNargothrondElf implements LOTRTravellingTrader
{
    public LOTREntityNargothrondTrader(final World world) {
        super(world);
        this.tasks.addTask(2, new LOTREntityAIAttackOnCollide(this, 1.6, false));
        this.addTargetTasks(false);
        this.npcCape = LOTRFACapes.nargothrond;
    }
    
    @Override
    public LOTRTradeEntries getBuyPool() {
        return LOTRTradeEntries.RIVENDELL_TRADER_BUY;
    }
    
    @Override
    public LOTRTradeEntries getSellPool() {
        return LOTRTradeEntries.RIVENDELL_TRADER_SELL;
    }
    
    @Override
    public LOTREntityNPC createTravellingEscort() {
        return new LOTREntityRivendellElf(this.worldObj);
    }
    
    @Override
    public String getDepartureSpeech() {
        return "rivendell/trader/departure";
    }
    
    @Override
    public int getTotalArmorValue() {
        return 10;
    }
    
    @Override
    public void onLivingUpdate() {
        super.onLivingUpdate();
        if (!this.worldObj.isRemote && this.isEntityAlive() && this.travellingTraderInfo.timeUntilDespawn == 0) {
            this.worldObj.setEntityState(this, (byte)15);
        }
    }
    
    @Override
    public int getAlignmentBonus() {
        return 2;
    }
    
    @Override
    protected int getExperiencePoints(final EntityPlayer entityplayer) {
        return 5 + this.rand.nextInt(3);
    }
    
    @Override
    public void onDeath(final DamageSource damagesource) {
        super.onDeath(damagesource);
        if (!this.worldObj.isRemote) {
            this.worldObj.setEntityState(this, (byte)15);
        }
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void handleHealthUpdate(final byte b) {
        if (b != 15) {
            super.handleHealthUpdate(b);
        }
    }
    
    @Override
    public boolean canTradeWith(final EntityPlayer entityplayer) {
        return LOTRLevelData.getData(entityplayer).getAlignment(this.getFaction()) >= 75 && this.isFriendly(entityplayer);
    }
    
    @Override
    public void onPlayerTrade(final EntityPlayer entityplayer, final LOTRTradeEntries.TradeType type, final ItemStack itemstack) {
        LOTRLevelData.getData(entityplayer).addAchievement(LOTRFAAchievements.tradeNargothrondTrader);
    }
    
    @Override
    public boolean shouldTraderRespawn() {
        return false;
    }
    
    @Override
    public boolean shouldRenderNPCHair() {
        return false;
    }
    
    @Override
    public String getSpeechBank(final EntityPlayer entityplayer) {
        if (!this.isFriendly(entityplayer)) {
            return "rivendell/trader/hostile";
        }
        if (this.canTradeWith(entityplayer)) {
            return "rivendell/trader/friendly";
        }
        return "rivendell/trader/neutral";
    }
}
