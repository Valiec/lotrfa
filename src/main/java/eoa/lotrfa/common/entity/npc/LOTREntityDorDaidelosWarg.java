package eoa.lotrfa.common.entity.npc;

import eoa.lotrfa.common.LOTRFAAchievements;
import eoa.lotrfa.common.LOTRFAFactions;
import lotr.common.*;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.entity.npc.LOTREntityWarg;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.world.World;

public class LOTREntityDorDaidelosWarg extends LOTREntityWarg {

    public LOTREntityDorDaidelosWarg(World world) {
        super(world);
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        if (this.rand.nextInt(500) == 0) {
            this.setWargType(WargType.ICE);
        }
        else if (this.rand.nextInt(20) == 0) {
            this.setWargType(WargType.WHITE);
        }
        else if (this.rand.nextInt(3) == 0) {
            this.setWargType(WargType.GREY);
        }
        else {
            this.setWargType(WargType.BROWN);
        }
    }

    @Override
    public LOTRFaction getFaction() {
        return LOTRFAFactions.dorDaidelos;
    }

    @Override
    public int getAlignmentBonus() {
        return 2;
    }

    @Override
    public LOTREntityNPC createWargRider() {
        if (this.rand.nextBoolean()) {
            this.setWargArmor(new ItemStack(LOTRMod.wargArmorUruk));
        }
        return this.worldObj.rand.nextBoolean() ? new LOTREntityDorDaidelosOrcArcher(this.worldObj) : new LOTREntityDorDaidelosOrc(this.worldObj);
    }
    
    @Override
    protected LOTRAchievement getKillAchievement() {
        if(getWargType() == WargType.ICE) return LOTRFAAchievements.killDorDaidelosIceWarg;
        
        return super.getKillAchievement();
    }
    
    @Override
    public void onDeath(DamageSource damagesource) {
        super.onDeath(damagesource);
        if(this.getWargType() == WargType.ICE && damagesource instanceof EntityDamageSource) {
            Entity entity = ((EntityDamageSource) damagesource).getEntity();
            if(entity.isEntityAlive() && entity instanceof EntityLivingBase) {
                EntityLivingBase livingEntity = (EntityLivingBase) entity;
                livingEntity.attackEntityFrom(LOTRDamage.frost, 2);
                livingEntity.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 100));
            }
        }
    }
}
