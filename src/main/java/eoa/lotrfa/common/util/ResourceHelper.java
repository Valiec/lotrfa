package eoa.lotrfa.common.util;

import java.io.*;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import com.google.common.base.Charsets;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.ModContainer;
import eoa.lotrfa.common.LOTRFA;

public class ResourceHelper {
    private static Map<Object, JarFile> jars = new HashMap<Object, JarFile>();
    
    public static Map<String, BufferedReader> getNamedReaders(Object mod, String path) {
        return getNamedReaders(mod, path, null);
    } 
    
    public static Map<String, BufferedReader> getNamedReaders(Object mod, String path, String extension) {
        Map<String, BufferedReader> readers = new HashMap<String, BufferedReader>();
        
        ModContainer modContainer = FMLCommonHandler.instance().findContainerFor(mod);
        if(modContainer == null) throw new IllegalArgumentException("The object " + mod.toString() + " can't be indentified as a mod.");
        
        if (modContainer.getSource().isFile() || jars.containsKey(mod)) {
            try {
                JarFile jar = null;
                if(jars.containsKey(mod)) jar = jars.get(mod);
                else {
                    jar = new JarFile(modContainer.getSource());
                    jars.put(mod, jar);
                }
                
                Enumeration<JarEntry> entries = jar.entries();
                
                while(entries.hasMoreElements()) {
                    JarEntry entry = entries.nextElement();
                    String entryName = entry.getName();
                
                    if (!entryName.startsWith(path) || !entryName.contains(".")) continue;
                    if(extension != null && !entryName.endsWith("." + extension)) continue;
                    
                    String name = entryName.substring(path.length() + 1, entryName.indexOf("."));
                    BufferedReader reader = new BufferedReader(new InputStreamReader(jar.getInputStream(entry), Charsets.UTF_8));
                    
                    readers.put(name, reader);
                }
            }
            catch(IOException e) {
                LOTRFA.logger.error(("Failed to load string banks for the mod " + modContainer.getModId() + " from jar file"));
            }
        }
        else {
            readers.putAll(folderReaders(mod, path, extension, path));
        }
        
        
        return readers;
    }
    
    public static void closeJarFile(Object mod) {
        try {
            if(jars.containsKey(mod)) jars.get(mod).close();
        }
        catch (IOException e) {
            LOTRFA.logger.error("Failed to close jar file for mod " + mod.toString());
            e.printStackTrace();
        }
        finally {
            jars.remove(mod);
        }
    }
    
    private static Map<String, BufferedReader> folderReaders(Object mod, String path, String extension, String originalPath) {
        Map<String, BufferedReader> readers = new HashMap<String, BufferedReader>();
        
        BufferedReader folderReader = getReader(mod, path);
        String line;
        try {
            while((line = folderReader.readLine()) != null) {
                if(!line.contains(".")) {
                    readers.putAll(folderReaders(mod, path + "/" + line, extension, originalPath));
                    continue;
                }
                if(extension != null && !line.endsWith("." + extension)) continue;

                String name =  line.substring(0, line.indexOf("."));
                if(!path.equals(originalPath)) {
                    String namePath = path.replace(originalPath + "/", "");
                    name = namePath + "/" + name;
                }
                
                readers.put(name, getReader(mod, path + "/" + line));
            }
        }
        catch (IOException e) {
            LOTRFA.logger.error("Something went wrong while getting the resources from " + path);
            e.printStackTrace();
        }
        
        return readers;
    }
    
    public static BufferedReader getReader(Object mod, String path) {
        return new BufferedReader(new InputStreamReader(getInputStream(mod, path), Charsets.UTF_8));
    }
    
    public static InputStream getInputStream(Object mod, String path) {
        InputStream in = mod.getClass().getResourceAsStream("/" + path);
        if(in == null) LOTRFA.logger.fatal("Could not find the imput stream for path " + path + " from " + mod.toString());
        return in;
    }
}
