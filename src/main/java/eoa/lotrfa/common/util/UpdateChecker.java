package eoa.lotrfa.common.util;

import eoa.lotrfa.common.LOTRFA;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.ClickEvent.Action;
import net.minecraft.util.*;

public class UpdateChecker {
    private static String version = LOTRFA.VERSION;
    private static String link;
    
    public static boolean isUpdateAbailable() {
        return !version.equals(LOTRFA.VERSION);
    }
    
    public static void checkForUpdates() {
        String[] arr = URLHelper.getStringArrayFromURL("https://gitlab.com/eras-of-arda/EoA-public/raw/master/lotrfa/version.txt");
        if(arr.length == 0) {
            LOTRFA.logger.warn("Was unable to retrieve the update version. Probably caused by a issue in the internet connection.");
        }
        else if(!arr[0].equals(LOTRFA.VERSION)) {
            version = arr[0];
            link = arr[1];
        }
    }
    
    public static void announceUpdateClient(EntityPlayer player) {
        ChatComponentText prefix = new ChatComponentText("[" + EnumChatFormatting.YELLOW + LOTRFA.MODNAME + EnumChatFormatting.WHITE + "]:");
        
        ChatComponentText version = new ChatComponentText(UpdateChecker.version);
        version.getChatStyle().setChatClickEvent(new ClickEvent(Action.OPEN_URL, link));
        version.getChatStyle().setUnderlined(true);
        
        player.addChatMessage(new ChatComponentTranslation("chat.lotrfa.update.client", new Object[] {prefix, version}));
    }
    
    public static void announceUpdateServerLogs() {
        LOTRFA.logger.info("There is a new update LOTRFA available! Version: " + version + ", you can find the latest release at https://gitlab.com/eras-of-arda/lotrfa/-/releases");
    }
}
