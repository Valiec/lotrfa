package eoa.lotrfa.common.util;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.ModContainer;
import eoa.lotrfa.common.LOTRFA;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;

public class StringBanks {

	public static Map<String, String[]> loadStringBank(Object mod, String folderPath, String extension) {
		Map<String, String[]> stringBanks = new HashMap<String, String[]>();
        
        ModContainer modContainer = FMLCommonHandler.instance().findContainerFor(mod);
        if(modContainer == null) throw new IllegalArgumentException("The object " + mod.toString() + " can't be indentified as a mod.");
        
        String path = "assets/" + modContainer.getModId() + "/" + folderPath;
        Map<String, BufferedReader> readers = ResourceHelper.getNamedReaders(mod, path, extension);
        
        for(Entry<String, BufferedReader> entry : readers.entrySet()) {
            String name = entry.getKey();
            BufferedReader reader = entry.getValue();
              
            String[] lines = reader.lines().toArray(String[]::new);
                  
            if (lines.length == 0) {
                LOTRFA.logger.warn("String bank " + name + " from " + modContainer.getModId() + " is empty!");
                continue;
            }
                
            stringBanks.put(name, lines);
        }
        
        if(stringBanks.isEmpty()) LOTRFA.logger.error("Found no string bank in " + path + " from " + modContainer.getModId());
        else LOTRFA.logger.info("Loaded " + stringBanks.size() + " banks in " + path + " from " + modContainer.getModId());
        
        return stringBanks;
	}
	
	public static void loadSpeechBanks(Object mod) {
		Map<String, String[]> speechBanks = StringBanks.loadStringBank(mod, "speech", "txt");
		        
		LOTRFAReflectionHelper.addSpeechBanks(speechBanks);
	}
	
	public static void loadNameBanks(Object mod) {
		Map<String, String[]> nameBanks = StringBanks.loadStringBank(mod, "names", "txt");
		        
		LOTRFAReflectionHelper.addNameBanks(nameBanks);
	}
	
	public static Map<String, String[]> getLoreBanks(Object mod) {
	    return StringBanks.loadStringBank(mod, "lore", "txt");
	}
    
    public static Map<String, String[]> getStructureScans(Object mod) {
        return StringBanks.loadStringBank(mod, "strscan", "strscan");
    }
}
