package eoa.lotrfa.common.util;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.StringUtils;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.lotrfa.common.LOTRFA;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.util.ResourceLocation;

public class URLHelper {
    
    public static List<String> getStringListFromURL(String stringURL) {
        ArrayList<String> list = new ArrayList<String>();

        URL url = getURL(stringURL);
        if(url != null) {    
            BufferedReader reader = getBufferedReader(url);
            if(reader != null) {
                try {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        if (line.startsWith("#") || StringUtils.isEmpty(line)) continue;
                        list.add(line.trim());
                    }
                    reader.close();
                }
                catch (IOException e) {
                    LOTRFA.logger.error("Something went wrong when reading the lines from URL: " + url.toString());
                    e.printStackTrace();
                }
            }
        }
        
        return list;
    }
    
    public static String[] getStringArrayFromURL(String stringURL) {
        return getStringListFromURL(stringURL).toArray(new String[0]);
    }
    
    @SideOnly(Side.CLIENT)
    public static ResourceLocation getTextureResourceLocationFromURL(String textureName, String urlString) {
        ResourceLocation loc = null;
        URL url = getURL(urlString);
        if(url != null) {
            BufferedImage img = getBufferedImage(url);
            if(img != null) {
                loc = Minecraft.getMinecraft().getTextureManager().getDynamicTextureLocation(textureName, new DynamicTexture(img));
            }   
        }
        
        return loc;
    }
    
    public static BufferedImage getBufferedImage(URL url) {
        InputStream stream = getInputStream(url);
        if(stream == null) return null;
        try {
            return ImageIO.read(stream);
        }
        catch (IOException e) {
            LOTRFA.logger.error("Was unable to read image from URL: " + url.toString());
            e.printStackTrace();
        }
        return null;
    }
    
    public static BufferedReader getBufferedReader(URL url) {
        InputStream stream = getInputStream(url);
        if(stream == null) return null;
        
        return new BufferedReader(new InputStreamReader(stream));
    }
    
    public static InputStream getInputStream(URL url) {
        if(!pingURL(url)) return null;
        
        try {
            return url.openStream();
        }
        catch (IOException e) {
            LOTRFA.logger.error("Was unable to open read the stream from URL: " + url.toString());
            e.printStackTrace();
        }
        return null;
    }
    
    public static boolean pingURL(URL url) {
        return pingURL(url, 2000);
    }
    
    public static boolean pingURL(URL url, int timeOut) {
        try {
            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(timeOut);
            connection.connect();
            return true;
        }
        catch (IOException e) {
            LOTRFA.logger.error("Was unable to open the connection to the URL: " + url.toString());
            e.printStackTrace();
        }
        
        return false;
    }
    
    public static URL getURL(String url) {
        try {
            return new URL(url);
        }
        catch (MalformedURLException e) {
            LOTRFA.logger.error("Unable to convert the string " + url + " to a url.");
            e.printStackTrace();
        }
        return null;
    }
}
