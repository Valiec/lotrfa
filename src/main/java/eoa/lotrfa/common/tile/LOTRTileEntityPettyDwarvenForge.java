package eoa.lotrfa.common.tile;

import eoa.lotrfa.common.item.LOTRFAItems;
import lotr.common.LOTRMod;
import lotr.common.tileentity.LOTRTileEntityDwarvenForge;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

public class LOTRTileEntityPettyDwarvenForge extends LOTRTileEntityDwarvenForge {

    @Override
    public String getForgeName() {
        return StatCollector.translateToLocal("container.lotr.pettyDwarvenForge");
    }

    @Override
    protected ItemStack getAlloySmeltingResult(ItemStack itemstack, ItemStack alloyItem) {
        if (this.isIron(itemstack) && this.isCoal(alloyItem)) {
            return new ItemStack(LOTRMod.dwarfSteel);
        }
        if (this.isIron(itemstack) && alloyItem.getItem() == LOTRMod.quenditeCrystal) {
            return new ItemStack(LOTRMod.galvorn);
        }
        if (this.isIron(itemstack) && alloyItem.getItem() == Item.getItemFromBlock(LOTRMod.rock) && alloyItem.getItemDamage() == 3) {
            return new ItemStack(LOTRMod.blueDwarfSteel);
        }

        if (this.isIron(itemstack) && alloyItem.getItem() == Item.getItemFromBlock(Blocks.stone) && alloyItem.getItemDamage() == 0) {
            return new ItemStack(LOTRFAItems.pettyDwarfSteel);
        }
        return super.getAlloySmeltingResult(itemstack, alloyItem);
    }
}
