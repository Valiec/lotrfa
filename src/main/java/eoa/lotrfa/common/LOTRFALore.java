package eoa.lotrfa.common;

import java.util.*;
import java.util.Map.Entry;
import eoa.lotrfa.common.reflection.LOTRFAEnumHelper;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import eoa.lotrfa.common.util.StringBanks;
import lotr.common.LOTRFaction;
import lotr.common.LOTRLore;

public class LOTRFALore extends LOTRLore {
    private static final String newline = "\n";
    private static final String codeMetadata = "#";
    private static final String codeTitle = "title:";
    private static final String codeAuthor = "author:";
    private static final String codeCategory = "types:";
    private static final String codeCategorySeparator = ",";
    private static final String codeReward = "reward";

    public LOTRFALore(String name, String title, String auth, String text, Set<LoreCategory> categories, boolean reward) {
        super(name, title, auth, text, new ArrayList<LoreCategory>(categories), reward);
    }
    
    public static void loadLoreBanks(Object mod) {
        Map<String, String[]> loreBanks = StringBanks.getLoreBanks(mod);
        if(loreBanks.size() == 0) LOTRFA.logger.error("Found no lorebanks for " + mod.toString());
        
        for (Entry<String, String[]> entry : loreBanks.entrySet()) {
            String loreName = entry.getKey();
            String[] lines = entry.getValue();
            
            String title = "";
            String author = "";
            Set<LoreCategory> categories = new HashSet<LoreCategory>();
            String text = "";
            boolean reward = false;
            
            for(String line : lines) {
                if (line.startsWith(codeMetadata)) {
                    String metadata = line.substring(codeMetadata.length());
                    
                    if (metadata.startsWith(codeTitle)) {
                        title = metadata.substring(codeTitle.length());
                    }
                    else if (metadata.startsWith(codeAuthor)) {
                        author = metadata.substring(codeAuthor.length());
                    }
                    else if (metadata.startsWith(codeCategory)) {
                        String[] categoryStrings = metadata.substring(codeCategory.length()).trim().split(codeCategorySeparator);
                        
                        for(String categoryName : categoryStrings) {
                            if(categoryName.equals("all")) {
                                for (LoreCategory category : LoreCategory.values()) categories.add(category);
                                continue;
                            }
                            
                            LoreCategory category = LoreCategories.forName(categoryName);
                            if (category == null) {
                                LOTRFA.logger.warn("LOTRFALore: Loading lore " + loreName + ", no category exists for name " + categoryName);
                                continue;
                            }
                            
                            categories.add(category);
                        }
                    }
                    else if (metadata.startsWith(codeReward)) {
                        reward = true;
                    }
                }
                else text += line + newline;
            }
                
            LOTRLore lore = new LOTRFALore(loreName, title, author, text, categories, reward);
            for(LoreCategory category : categories) LOTRFAReflectionHelper.addLoreToLoreCategory(category, lore);
        }
    }
    
    public static class LoreCategories {
        public static LoreCategory amonRudh;
        public static LoreCategory angband;
        public static LoreCategory brethil;
        public static LoreCategory dorLomin;
        public static LoreCategory doriath;
        public static LoreCategory dorthonion;
        public static LoreCategory falas;
        public static LoreCategory gardhBoren;
        public static LoreCategory gondolin;
        public static LoreCategory hithlum;
        public static LoreCategory marchMaedhros;
        public static LoreCategory nanGuruthos;
        public static LoreCategory nargothrond;
        public static LoreCategory ossiriand;
        public static LoreCategory taurdru;
        public static LoreCategory tolInGaurhoth;
        public static LoreCategory eastBeleriand;
        public static LoreCategory westBeleriand;
        
        public static void init() {
            amonRudh = LOTRFAEnumHelper.addLoreCategory("AMON_RUDH", "amon_rudh");
            angband = LOTRFAEnumHelper.addLoreCategory("ANGBAND", "angband");
            brethil = LOTRFAEnumHelper.addLoreCategory("BRETHIL", "brethil");
            dorLomin = LOTRFAEnumHelper.addLoreCategory("DOR_LOMIN", "dor-lomin");
            doriath = LOTRFAEnumHelper.addLoreCategory("DORIATH", "doriath");
            dorthonion = LOTRFAEnumHelper.addLoreCategory("DORTHONION", "dorthonion");
            falas = LOTRFAEnumHelper.addLoreCategory("FALAS", "falas");
            gardhBoren = LOTRFAEnumHelper.addLoreCategory("GARDH_BOREN", "gardh_boren");
            gondolin = LOTRFAEnumHelper.addLoreCategory("GONDOLIN", "gondolin");
            hithlum = LOTRFAEnumHelper.addLoreCategory("HITHLUM", "hithlum");
            marchMaedhros = LOTRFAEnumHelper.addLoreCategory("MARCH_MAEDHROS", "march_maedhros");
            nanGuruthos = LOTRFAEnumHelper.addLoreCategory("NAN_GURUTHOS", "nan_guruthos");
            nargothrond = LOTRFAEnumHelper.addLoreCategory("NARGOTHROND", "nargothrond");
            ossiriand = LOTRFAEnumHelper.addLoreCategory("OSSIRIAND", "ossiriand");
            taurdru = LOTRFAEnumHelper.addLoreCategory("TAURDRU", "taurdru");
            tolInGaurhoth = LOTRFAEnumHelper.addLoreCategory("TOL_IN_GAURHOTH", "tol-in-gaurhoth");
            eastBeleriand = LOTRFAEnumHelper.addLoreCategory("EAST_BELERIAND", "east_beleriand");
            westBeleriand = LOTRFAEnumHelper.addLoreCategory("WEST_BELERIAND", "west_beleriand");
        }
        
        public static LoreCategory forName(String name) {
            for(LoreCategory category : LoreCategory.values()) {
                if(category.categoryName.equalsIgnoreCase(name)) return category;
            }
            return null;
        }
    }

}
