package eoa.lotrfa.common.block;

import eoa.lotrfa.common.tile.LOTRTileEntityPettyDwarvenForge;
import lotr.common.block.LOTRBlockDwarvenForge;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class LOTRBlockPettyDwarvenForge extends LOTRBlockDwarvenForge {

    @Override
    public TileEntity createNewTileEntity(World world, int i) {
        return new LOTRTileEntityPettyDwarvenForge();
    }
}
