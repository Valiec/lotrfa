package eoa.lotrfa.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lotr.common.block.LOTRBlockSlabBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemSlab;
import net.minecraft.util.IIcon;

public class LOTRFABlockSlab2 extends LOTRBlockSlabBase {

    public LOTRFABlockSlab2(boolean flag) {
        super(flag, Material.rock, 8);
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public IIcon getIcon(int i, int j) {
        if ((j &= 7) == 0) {
            return LOTRFABlocks.rockSarllith.getIcon(i, 0);
        }
        if (j == 1) {
            return LOTRFABlocks.brick.getIcon(i, 4);
        }
        if (j == 2) {
            return LOTRFABlocks.brick.getIcon(i, 5);
        }
        if (j == 3) {
            return LOTRFABlocks.brick.getIcon(i, 7);
        }
        if (j == 4) {
            return LOTRFABlocks.brick.getIcon(i, 8);
        }
        if (j == 5) {
            return LOTRFABlocks.pillar.getIcon(i, 6);
        }
        if (j == 6) {
            return LOTRFABlocks.pillar.getIcon(i, 7);
        }
        if (j == 7) {
            return LOTRFABlocks.pillar.getIcon(i, 8);
        }
        return super.getIcon(i, j);
    }

    public static class SlabDouble extends ItemSlab {
        public SlabDouble(Block block) {
            super(block, (BlockSlab) LOTRFABlocks.slabSingle2, (BlockSlab) LOTRFABlocks.slabDouble2, true);
        }
    }

    public static class SlabSingle extends ItemSlab {
        public SlabSingle(Block block) {
            super(block, (BlockSlab) LOTRFABlocks.slabSingle2, (BlockSlab) LOTRFABlocks.slabDouble2, false);
        }
    }

}
