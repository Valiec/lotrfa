package eoa.lotrfa.common.block;

import lotr.common.block.LOTRBlockPillarBase;
import net.minecraft.block.material.Material;

public class LOTRFABlockPillarGond extends LOTRBlockPillarBase {

    public LOTRFABlockPillarGond() {
        super(Material.rock);
        this.setPillarNames("gondolin");

    }
}
