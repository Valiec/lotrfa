package eoa.lotrfa.common.block;

import lotr.common.LOTRCreativeTabs;
import net.minecraft.block.BlockTorch;
import net.minecraft.creativetab.CreativeTabs;

public class LOTRFABlockTorch extends BlockTorch{
	
    public LOTRFABlockTorch() {
        this.setCreativeTab((CreativeTabs)LOTRCreativeTabs.tabDeco);
    }

}
