package eoa.lotrfa.common.block;

import java.util.Random;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.lotrfa.common.LOTRFACreativeTabs;
import lotr.common.util.LOTRCommonIcons;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Facing;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class LOTRFABlockBarrier extends Block {

    public LOTRFABlockBarrier() {
        super(Material.portal);
        this.setCreativeTab(LOTRFACreativeTabs.tabFA);
        this.setBlockUnbreakable();
        this.setResistance(Float.MAX_VALUE);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }
    
    @Override
    public Item getItemDropped(int p_149650_1_, Random random, int p_149650_3_) {
        return null;
    }
    
    @Override
    public boolean canPlaceTorchOnTop(World world, int x, int y, int z) {
        return true;
    }
    
    @Override
    public boolean canEntityDestroy(IBlockAccess world, int x, int y, int z, Entity entity) {
        return false;
    }
    
    @Override
    @SideOnly(value = Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess world, int i, int j, int k, int side) {
        if (world.getBlockMetadata(i, j, k) != world.getBlockMetadata(i - Facing.offsetsXForSide[side], j - Facing.offsetsYForSide[side], k - Facing.offsetsZForSide[side])) {
            return true;
        }
        
        if (world.getBlock(i, j, k) == this) return false;
        
        return super.shouldSideBeRendered(world, i, j, k, side);
    }
    
    @SideOnly(value = Side.CLIENT)
    @Override
    public IIcon getIcon(IBlockAccess world, int i, int j, int k, int side) {
        ItemStack heldItem = Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem();
        if (heldItem != null && heldItem.getItem() != Item.getItemFromBlock(this)) return LOTRCommonIcons.iconEmptyBlock;
        
        return super.getIcon(world, i, j, k, side);
    }
}
