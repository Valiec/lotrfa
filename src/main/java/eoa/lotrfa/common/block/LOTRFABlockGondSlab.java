package eoa.lotrfa.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lotr.common.block.LOTRBlockSlabBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemSlab;
import net.minecraft.util.IIcon;

public class LOTRFABlockGondSlab extends LOTRBlockSlabBase {

    public LOTRFABlockGondSlab(boolean flag) {
        super(flag, Material.rock, 4);
        // TODO Auto-generated constructor stub
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public IIcon getIcon(int i, int j) {
        if ((j &= 7) == 0) {
            return LOTRFABlocks.rockSarnlum.getIcon(i, 0);
        }
        if (j == 1) {
            return LOTRFABlocks.brickSarnlum.getIcon(i, 0);
        }
        if (j == 2) {
            return LOTRFABlocks.pillarSarnlum.getIcon(i, 0);
        }
        if (j == 3) {
            return LOTRFABlocks.smoothStoneSarnlum.getIcon(i, 0);
        }
        return super.getIcon(i, j);
    }

    public static class GondSlabDouble extends ItemSlab {
        public GondSlabDouble(Block block) {
            super(block, (BlockSlab) LOTRFABlocks.slabSingleSarnlum, (BlockSlab) LOTRFABlocks.slabDoubleSarnlum, true);
        }
    }

    public static class GondSlabSingle extends ItemSlab {
        public GondSlabSingle(Block block) {
            super(block, (BlockSlab) LOTRFABlocks.slabSingleSarnlum, (BlockSlab) LOTRFABlocks.slabDoubleSarnlum, false);
        }
    }

}
