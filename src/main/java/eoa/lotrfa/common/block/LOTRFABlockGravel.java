package eoa.lotrfa.common.block;

import java.util.Random;
import eoa.lotrfa.common.LOTRFACreativeTabs;
import net.minecraft.block.Block;
import net.minecraft.block.BlockGravel;
import net.minecraft.item.Item;

public class LOTRFABlockGravel extends BlockGravel {

    public LOTRFABlockGravel() {
        this.setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        this.setHardness(0.5f);
        this.setStepSound(Block.soundTypeSand);

    }

    @Override
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_) {
        return Item.getItemFromBlock(this);
    }

}
