package eoa.lotrfa.common.block;

import lotr.common.block.LOTRBlockPillarBase;
import net.minecraft.block.material.Material;

public class LOTRFABlockPillar extends LOTRBlockPillarBase {
    public LOTRFABlockPillar() {
        super(Material.rock);
        this.setPillarNames("pettyDwarven", "pettyDwarvenCracked", "dorLominCracked", "tolInGaurhoth", "tolInGaurhothCracked", "tolInGaurhothMossy", "falas", "falasCracked", "feanorian", "feanorianCracked", "gondolin", "gondolinCracked", "nargothrondCracked");

    }

}
