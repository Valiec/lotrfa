package eoa.lotrfa.common.block;

import lotr.common.block.LOTRBlockBrickBase;

public class LOTRFABrick2 extends LOTRBlockBrickBase {

    public LOTRFABrick2() {
        this.setBrickNames("gondolinCracked", "gondolinMossy","gondolinGold", "gondolinSilver");
    }

}
