package eoa.lotrfa.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lotr.common.block.LOTRBlockSlabBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemSlab;
import net.minecraft.util.IIcon;

public class LOTRFABlockSlab4 extends LOTRBlockSlabBase {
	
    public LOTRFABlockSlab4(boolean flag) {
        super(flag, Material.rock, 2);
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public IIcon getIcon(int i, int j) {
        if ((j &= 7) == 0) {
            return LOTRFABlocks.pillar.getIcon(i, 11);
        }
        if (j == 1) {
            return LOTRFABlocks.pillar.getIcon(i, 12);
        }
        return super.getIcon(i, j);
    }

    public static class SlabDouble extends ItemSlab {
        public SlabDouble(Block block) {
            super(block, (BlockSlab) LOTRFABlocks.slabSingle4, (BlockSlab) LOTRFABlocks.slabDouble4, true);
        }
    }

    public static class SlabSingle extends ItemSlab {
        public SlabSingle(Block block) {
            super(block, (BlockSlab) LOTRFABlocks.slabSingle4, (BlockSlab) LOTRFABlocks.slabDouble4, false);
        }
    }

}
