package eoa.lotrfa.common.block;

import lotr.common.block.LOTRBlockBrickBase;

public class LOTRFABrick extends LOTRBlockBrickBase {

    public LOTRFABrick() {
        this.setBrickNames("pettyDwarf", "pettyDwarfCracked", "gondolin", "tolInGaurhothMossy", "feanorian", "falas", "falasMossy", "feanorianCracked", "falasCracked", "feanorianGold", "feanorianSilver", "feanorianMossy", "falasGold", "falasSilver", "nargothrondGold", "nargothrondSilver");
    }

}
