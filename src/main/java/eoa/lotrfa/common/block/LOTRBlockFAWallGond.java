package eoa.lotrfa.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lotr.common.block.LOTRBlockWallBase;
import net.minecraft.block.Block;
import net.minecraft.util.IIcon;

public class LOTRBlockFAWallGond extends LOTRBlockWallBase {

    public LOTRBlockFAWallGond(Block block) {
        super(block, 2);
        // TODO Auto-generated constructor stub
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public IIcon getIcon(int i, int j) {
        if (j == 0) {
            return LOTRFABlocks.rockSarnlum.getIcon(i, 0);
        }
        if (j == 1) {
            return LOTRFABlocks.brickSarnlum.getIcon(i, 0);
        }
        return super.getIcon(i, j);
    }

}
