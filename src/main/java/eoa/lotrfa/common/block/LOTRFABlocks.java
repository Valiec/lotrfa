package eoa.lotrfa.common.block;

import com.google.common.base.CaseFormat;
import cpw.mods.fml.common.registry.GameRegistry;
import eoa.lotrfa.common.LOTRFA;
import eoa.lotrfa.common.LOTRFACreativeTabs;
import eoa.lotrfa.common.block.table.*;
import lotr.common.LOTRMod;
import lotr.common.block.*;
import lotr.common.item.LOTRItemBlockMetadata;
import net.minecraft.block.Block;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;

public class LOTRFABlocks {
    // Crafting tables
    public static Block craftingTableAngband;
    public static Block craftingTableBrethil;
    public static Block craftingTableDorDaidelos;
    public static Block craftingTableDoriath;
    public static Block craftingTableDorLomin;
    public static Block craftingTableFalathrim;
    public static Block craftingTableFeanorian;
    public static Block craftingTableGondolin;
    public static Block craftingTableHithlum;
    public static Block craftingTableHouseBor;
    public static Block craftingTableHouseUlfang;
    public static Block craftingTableLaegrim;
    public static Block craftingTableNargothrond;
    public static Block craftingTablePettyDwarf;
    public static Block craftingTableTolInGaurhoth;

    // Forges
    public static Block forgePettyDwarf;

    // Stone cubes
    public static Block graniteRough;
    public static Block graniteSmooth;
    public static Block brick;
    public static Block brick2;
    public static Block brickSarnlum;
    public static Block carvedBrickPettyDwarf;
    public static Block carvedBrickSarnlum;
    public static Block carvedBrickTolInGaurhoth;
    public static Block carvedBrickFalas;
    public static Block carvedBrickFeanorian;
    public static Block carvedBrickGondolin;
    public static Block pillar;
    public static Block pillarSarnlum;
    public static Block rockSarnlum;
    public static Block smoothStoneSarnlum;
    public static Block rockSarllith;

    // Stone slabs
    public static Block slabSingle;
    public static Block slabDouble;
    public static Block slabSingle2;
    public static Block slabDouble2;
    public static Block slabSingle3;
    public static Block slabDouble3;
    public static Block slabSingle4;
    public static Block slabDouble4;
    public static Block slabSingleSarnlum;
    public static Block slabDoubleSarnlum;

    // Stone stairs
    public static Block stairsGondolin;
    public static Block stairsGondolinCracked;
    public static Block stairsGondolinMossy;
    public static Block stairsPettyDwarf;
    public static Block stairsPettyDwarfCracked;
    public static Block stairsSarnlum;
    public static Block stairsSarnlumBrick;
    public static Block stairsTolInGaurhothMossy;
    public static Block stairsFeanorian;
    public static Block stairsFeanorianCracked;
    public static Block stairsFeanorianMossy;
    public static Block stairsFalas;
    public static Block stairsFalasCracked;
    public static Block stairsFalasMossy;
    public static Block stairsSarllith;

    // Stone other
    public static Block wall;
    public static Block wallSarnlum;

    // Wood

    // Plants
    public static Block aeglos;
    public static Block seregon;
    
    // Torches
    public static Block gondolinTorch;
    public static Block falasTorch;
    public static Block nargothrondTorch;
    public static Block feanorianTorch;

    // Other
    public static Block anfauglithAsh;
    public static Block barrier;

    public static void init() {
        initBlocks();
        registerBlocks();
        setHarvestLevels();
    }

    private static void initBlocks() {
        // TODO remove creative tabs and step sound were posible and use a LOTRFABrickBase

        // Crafting tables
    	craftingTableAngband = new LOTRBlockTableAngband();
        craftingTableBrethil = new LOTRBlockTableBrethil();
        craftingTableDorDaidelos = new LOTRBlockTableDorDaidelos();
        craftingTableDoriath = new LOTRBlockTableDoriath();
        craftingTableDorLomin = new LOTRBlockTableDorLomin();
        craftingTableFalathrim = new LOTRBlockTableFalathrim();
        craftingTableFeanorian = new LOTRBlockTableFeanorian();
        craftingTableGondolin = new LOTRBlockTableGondolin();
        craftingTableHithlum = new LOTRBlockTableHithlum();
        craftingTableHouseBor = new LOTRBlockTableHouseBor();
        craftingTableHouseUlfang = new LOTRBlockTableHouseUlfang();
        craftingTableLaegrim = new LOTRBlockTableLaegrim();
        craftingTableNargothrond = new LOTRBlockTableNargothrond();
        craftingTablePettyDwarf = new LOTRBlockTablePettyDwarven();
        craftingTableTolInGaurhoth = new LOTRBlockTolInGaurhothTable();

        // Forges
        forgePettyDwarf = new LOTRBlockPettyDwarvenForge();

        // Stone cubes

        graniteRough = new LOTRFABlock(Material.rock).setCreativeTab(LOTRFACreativeTabs.tabBlocks).setHardness(1.6F).setResistance(10.0F);
        graniteSmooth = new LOTRFABlock(Material.rock).setCreativeTab(LOTRFACreativeTabs.tabBlocks).setHardness(1.6F).setResistance(10.0F);

        brick = new LOTRFABrick().setHardness(1.5F).setResistance(10.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        brick2 = new LOTRFABrick2().setHardness(1.5F).setResistance(10.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        brickSarnlum = new LOTRFABrickGond().setHardness(20.0F).setResistance(2000.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        carvedBrickPettyDwarf = new LOTRFABlock(Material.rock).setHardness(1.5F).setResistance(10.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        carvedBrickSarnlum = new LOTRFABlock(Material.rock).setHardness(20.0F).setResistance(2000.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        carvedBrickTolInGaurhoth = new LOTRFABlock(Material.rock).setHardness(1.5F).setResistance(10.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        carvedBrickFeanorian = new LOTRFABlock(Material.rock).setHardness(1.5F).setResistance(10.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        carvedBrickFalas = new LOTRFABlock(Material.rock).setHardness(1.5F).setResistance(10.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        carvedBrickGondolin = new LOTRFABlock(Material.rock).setHardness(1.5F).setResistance(10.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        pillar = new LOTRFABlockPillar().setBlockName("faPillar").setHardness(1.5F).setResistance(10.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        pillarSarnlum = new LOTRFABlockPillarGond().setHardness(20.0F).setResistance(2000.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        rockSarnlum = new LOTRFABlock(Material.rock).setHardness(20.0F).setResistance(2000.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        smoothStoneSarnlum = new LOTRFABlockSmoothStoneGond().setHardness(50.0f).setResistance(2000.0f).setCreativeTab(LOTRFACreativeTabs.tabBlocks);
        rockSarllith = new LOTRFABlock(Material.rock).setHardness(20.0F).setResistance(2000.0F).setCreativeTab(LOTRFACreativeTabs.tabBlocks);

        // Stone slabs
        slabSingle = new LOTRFABlockSlab(false).setHardness(2.0f).setResistance(10.0f).setStepSound(Block.soundTypeStone);
        slabDouble = new LOTRFABlockSlab(true).setHardness(2.0f).setResistance(10.0f).setStepSound(Block.soundTypeStone);
        slabSingle2 = new LOTRFABlockSlab2(false).setHardness(2.0f).setResistance(10.0f).setStepSound(Block.soundTypeStone);
        slabDouble2 = new LOTRFABlockSlab2(true).setHardness(2.0f).setResistance(10.0f).setStepSound(Block.soundTypeStone);
        slabSingle3 = new LOTRFABlockSlab3(false).setHardness(2.0f).setResistance(10.0f).setStepSound(Block.soundTypeStone);
        slabDouble3 = new LOTRFABlockSlab3(true).setHardness(2.0f).setResistance(10.0f).setStepSound(Block.soundTypeStone);
        slabSingle4 = new LOTRFABlockSlab4(false).setHardness(2.0f).setResistance(10.0f).setStepSound(Block.soundTypeStone);
        slabDouble4 = new LOTRFABlockSlab4(true).setHardness(2.0f).setResistance(10.0f).setStepSound(Block.soundTypeStone);
        slabSingleSarnlum = new LOTRFABlockGondSlab(false).setHardness(50.0f).setResistance(2000.0f).setStepSound(Block.soundTypeStone);
        slabDoubleSarnlum = new LOTRFABlockGondSlab(true).setHardness(50.0f).setResistance(2000.0f).setStepSound(Block.soundTypeStone);

        // Stone stairs
        stairsGondolin = new LOTRBlockStairs(brick, 2);
        stairsGondolinCracked = new LOTRBlockStairs(brick2, 0);
        stairsGondolinMossy = new LOTRBlockStairs(brick2, 1);
        stairsPettyDwarf = new LOTRBlockStairs(brick, 0);
        stairsPettyDwarfCracked = new LOTRBlockStairs(brick, 1);
        stairsSarnlum = new LOTRBlockStairs(rockSarnlum, 0).setHardness(50.0f).setResistance(2000.0f);
        stairsSarnlumBrick = new LOTRBlockStairs(brickSarnlum, 1).setHardness(50.0f).setResistance(2000.0f);
        stairsTolInGaurhothMossy = new LOTRBlockStairs(brick, 3);
        stairsFeanorian = new LOTRBlockStairs(brick, 4);
        stairsFeanorianCracked = new LOTRBlockStairs(brick, 7);
        stairsFeanorianMossy = new LOTRBlockStairs(brick, 11);
        stairsFalas = new LOTRBlockStairs(brick, 5);
        stairsFalasCracked = new LOTRBlockStairs(brick, 8);
        stairsFalasMossy = new LOTRBlockStairs(brick, 6);
        stairsSarllith = new LOTRBlockStairs(rockSarllith, 0);

        // Stone other
        wall = new LOTRBlockFAWall(brick);
        wallSarnlum = new LOTRBlockFAWallGond(brickSarnlum);

        // Wood
        
        //Torches
        
        gondolinTorch = new LOTRFABlockTorch();
        falasTorch = new LOTRFABlockTorch();
        nargothrondTorch = new LOTRFABlockTorch();
        feanorianTorch = new LOTRFABlockTorch();

        // Plants
        aeglos = new LOTRBlockFlower();
        seregon = new LOTRBlockFlower();

        // Other
        anfauglithAsh = new LOTRFABlockGravel().setHardness(0.5F).setResistance(2.5F);
        barrier = new LOTRFABlockBarrier();
    }

    private static void registerBlocks() {
        LOTRBlockSlabBase.registerSlabs(slabSingle, slabDouble);
        LOTRBlockSlabBase.registerSlabs(slabSingle2, slabDouble2);
        LOTRBlockSlabBase.registerSlabs(slabSingle3, slabDouble3);
        LOTRBlockSlabBase.registerSlabs(slabSingle4, slabDouble4);
        LOTRBlockSlabBase.registerSlabs(slabSingleSarnlum, slabDoubleSarnlum);

        // Crafting tables
        nameAndRegisterBlock(craftingTableAngband, "craftingTableAngband");
        nameAndRegisterBlock(craftingTableBrethil, "craftingTableBrethil");
        nameAndRegisterBlock(craftingTableDorDaidelos, "craftingTableDorDaidelos");
        nameAndRegisterBlock(craftingTableDoriath, "craftingTableDoriath");
        nameAndRegisterBlock(craftingTableDorLomin, "craftingTableDorLomin");
        nameAndRegisterBlock(craftingTableFalathrim, "craftingTableFalathrim");
        nameAndRegisterBlock(craftingTableFeanorian, "craftingTableFeanorian");
        nameAndRegisterBlock(craftingTableGondolin, "craftingTableGondolin");
        nameAndRegisterBlock(craftingTableHithlum, "craftingTableHithlum");
        nameAndRegisterBlock(craftingTableHouseBor, "craftingTableHouseBor");
        nameAndRegisterBlock(craftingTableHouseUlfang, "craftingTableHouseUlfang");
        nameAndRegisterBlock(craftingTableLaegrim, "craftingTableLaegrim");
        nameAndRegisterBlock(craftingTableNargothrond, "craftingTableNargothrond");
        nameAndRegisterBlock(craftingTablePettyDwarf, "craftingTablePettyDwarf");
        nameAndRegisterBlock(craftingTableTolInGaurhoth, "craftingTableTolInGaurhoth");

        // Forges
        nameAndRegisterBlock(forgePettyDwarf, "forgePettyDwarf");

        // Stone cubes
        nameAndRegisterBlock(graniteRough, "graniteRough");
        nameAndRegisterBlock(graniteSmooth, "graniteSmooth");
        nameAndRegisterBlock(brick, LOTRItemBlockMetadata.class, "brick");
        nameAndRegisterBlock(brick2, LOTRItemBlockMetadata.class, "brick2");
        nameAndRegisterBlock(brickSarnlum, LOTRItemBlockMetadata.class, "brickSarnlum");
        nameAndRegisterBlock(carvedBrickPettyDwarf, "carvedBrickPettyDwarf");
        nameAndRegisterBlock(carvedBrickSarnlum, "carvedBrickSarnlum");
        nameAndRegisterBlock(carvedBrickTolInGaurhoth, "carvedBrickTolInGaurhoth");
        nameAndRegisterBlock(carvedBrickFeanorian, "carvedBrickFeanorian");
        nameAndRegisterBlock(carvedBrickFalas, "carvedBrickFalas");
        nameAndRegisterBlock(carvedBrickGondolin, "carvedBrickGondolin");
        nameAndRegisterBlock(pillar, LOTRItemBlockMetadata.class, "pillar");
        nameAndRegisterBlock(pillarSarnlum, LOTRItemBlockMetadata.class, "pillarSarnlum");
        nameAndRegisterBlock(rockSarnlum, "rockSarnlum");
        nameAndRegisterBlock(smoothStoneSarnlum, LOTRItemBlockMetadata.class, "smoothStoneSarnlum");
        nameAndRegisterBlock(rockSarllith, "rockSarllith");

        // Stone slabs
        nameAndRegisterBlock(slabSingle, LOTRFABlockSlab.SlabSingle.class, "slabSingle");
        nameAndRegisterBlock(slabDouble, LOTRFABlockSlab.SlabDouble.class, "slabDouble");
        nameAndRegisterBlock(slabSingle2, LOTRFABlockSlab2.SlabSingle.class, "slabSingle2");
        nameAndRegisterBlock(slabDouble2, LOTRFABlockSlab2.SlabDouble.class, "slabDouble2");
        nameAndRegisterBlock(slabSingle3, LOTRFABlockSlab3.SlabSingle.class, "slabSingle3");
        nameAndRegisterBlock(slabDouble3, LOTRFABlockSlab3.SlabDouble.class, "slabDouble3");
        nameAndRegisterBlock(slabSingle4, LOTRFABlockSlab4.SlabSingle.class, "slabSingle4");
        nameAndRegisterBlock(slabDouble4, LOTRFABlockSlab4.SlabDouble.class, "slabDouble4");
        nameAndRegisterBlock(slabSingleSarnlum, LOTRFABlockGondSlab.GondSlabSingle.class, "slabSingleSarnlum");
        nameAndRegisterBlock(slabDoubleSarnlum, LOTRFABlockGondSlab.GondSlabDouble.class, "slabDoubleSarnlum");

        // Stone stairs
        nameAndRegisterBlock(stairsGondolin, "stairsGondolin");
        nameAndRegisterBlock(stairsGondolinCracked, "stairsGondolinCracked");
        nameAndRegisterBlock(stairsGondolinMossy, "stairsGondolinMossy"
        		+ "");
        nameAndRegisterBlock(stairsPettyDwarf, "stairsPettyDwarf");
        nameAndRegisterBlock(stairsPettyDwarfCracked, "stairsPettyDwarfCracked");
        nameAndRegisterBlock(stairsSarnlum, "stairsSarnlum");
        nameAndRegisterBlock(stairsSarnlumBrick, "stairsSarnlumBrick");
        nameAndRegisterBlock(stairsTolInGaurhothMossy, "stairsTolInGaurhothMossy");
        nameAndRegisterBlock(stairsFeanorian, "stairsFeanorian");
        nameAndRegisterBlock(stairsFeanorianCracked, "stairsFeanorianCracked");
        nameAndRegisterBlock(stairsFeanorianMossy, "stairsFeanorianMossy");
        nameAndRegisterBlock(stairsFalas, "stairsFalas");
        nameAndRegisterBlock(stairsFalasCracked, "stairsFalasCracked");
        nameAndRegisterBlock(stairsFalasMossy, "stairsFalasMossy");
        nameAndRegisterBlock(stairsSarllith, "stairsSarllith");

        // Stone other
        nameAndRegisterBlock(wall, LOTRItemBlockMetadata.class, "wall");
        nameAndRegisterBlock(wallSarnlum, LOTRItemBlockMetadata.class, "wallSarnlum");

        // Wood
        
        //Torches
        nameAndRegisterBlock(gondolinTorch, "gondolinTorch");
        nameAndRegisterBlock(nargothrondTorch, "nargothrondTorch");
        nameAndRegisterBlock(feanorianTorch, "feanorianTorch");
        nameAndRegisterBlock(falasTorch, "falasTorch");

        // Plants
        nameAndRegisterBlock(aeglos, "aeglos");
        nameAndRegisterBlock(seregon, "seregon");

        // Other
        nameAndRegisterBlock(anfauglithAsh, "anfauglithAsh");
        nameAndRegisterBlock(barrier, "barrier");
    }

    private static void setHarvestLevels() {
        brick.setHarvestLevel("pickaxe", 0);
        brick2.setHarvestLevel("pickaxe", 0);
        brickSarnlum.setHarvestLevel("pickaxe", 4);
        carvedBrickPettyDwarf.setHarvestLevel("pickaxe", 0);
        carvedBrickSarnlum.setHarvestLevel("pickaxe", 4);
        carvedBrickTolInGaurhoth.setHarvestLevel("pickaxe", 0);
        carvedBrickFeanorian.setHarvestLevel("pickaxe", 0);
        carvedBrickFalas.setHarvestLevel("pickaxe", 0);
        carvedBrickGondolin.setHarvestLevel("pickaxe", 0);
        pillarSarnlum.setHarvestLevel("pickaxe", 4);
        rockSarnlum.setHarvestLevel("pickaxe", 4);
        rockSarllith.setHarvestLevel("pickaxe", 4);
        slabSingle.setHarvestLevel("pickaxe", 0);
        slabDouble.setHarvestLevel("pickaxe", 0);
        slabSingle2.setHarvestLevel("pickaxe", 0);
        slabDouble2.setHarvestLevel("pickaxe", 0);
        slabSingle3.setHarvestLevel("pickaxe", 0);
        slabDouble3.setHarvestLevel("pickaxe", 0);
        slabSingle4.setHarvestLevel("pickaxe", 0);
        slabDouble4.setHarvestLevel("pickaxe", 0);
        slabSingleSarnlum.setHarvestLevel("pickaxe", 4);
        slabDoubleSarnlum.setHarvestLevel("pickaxe", 4);
        smoothStoneSarnlum.setHarvestLevel("pickaxe", 4);
        stairsSarnlum.setHarvestLevel("pickaxe", 4);
        stairsSarnlumBrick.setHarvestLevel("pickaxe", 4);
        wallSarnlum.setHarvestLevel("pickaxe", 4);

        anfauglithAsh.setHarvestLevel("shovel", 0);
    }

    public static void registerFireInfo() {
        // Blocks.fire.setFireInfo(block, encouragement, flammibility);
    }

    public static void nameAndRegisterBlock(Block block, String name) {
        String lowerUnderscoreName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, name);
        block.setBlockName(name);
        block.setBlockTextureName(LOTRFA.MODID + ":" + lowerUnderscoreName);
        GameRegistry.registerBlock(block, lowerUnderscoreName);
    }

    // in case some of the lotr: instead of lotrfa: texture names are required
    public static void nameAndRegisterBlock(Block block, String name, String textureName) {
        String lowerUnderscoreName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, name);
        block.setBlockName(name);
        block.setBlockTextureName(textureName);
        GameRegistry.registerBlock(block, lowerUnderscoreName);
    }

    // if you need a meta handler class
    public static void nameAndRegisterBlock(Block block, Class<? extends ItemBlock> metaHandler, String name) {
        String lowerUnderscoreName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, name);
        block.setBlockName(name);
        block.setBlockTextureName(LOTRFA.MODID + ":" + lowerUnderscoreName);
        GameRegistry.registerBlock(block, metaHandler, lowerUnderscoreName);
    }

    // in case some of the lotr: instead of lotrfa: texture names are required
    public static void nameAndRegisterBlock(Block block, Class<? extends ItemBlock> metaHandler, String name, String textureName) {
        String lowerUnderscoreName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, name);
        block.setBlockName(name);
        block.setBlockTextureName(textureName);
        GameRegistry.registerBlock(block, metaHandler, lowerUnderscoreName);
    }
    
    public static void initItemBlockHide() {
    	LOTRMod.angmarTable.setCreativeTab(null);
    	
    }
}
