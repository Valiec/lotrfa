package eoa.lotrfa.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lotr.common.block.LOTRBlockWallBase;
import net.minecraft.block.Block;
import net.minecraft.util.IIcon;

public class LOTRBlockFAWall extends LOTRBlockWallBase {

    public LOTRBlockFAWall(Block block) {
        super(block, 13);
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public IIcon getIcon(int i, int j) {
        if (j == 0) {
            return LOTRFABlocks.brick.getIcon(i, 0);
        }
        if (j == 1) {
            return LOTRFABlocks.brick.getIcon(i, 1);
        }
        if (j == 2) {
            return LOTRFABlocks.brick.getIcon(i, 2);
        }
        if (j == 3) {
            return LOTRFABlocks.brick.getIcon(i, 3);
        }
        if (j == 4) {
            return LOTRFABlocks.brick.getIcon(i, 4);
        }
        if (j == 5) {
            return LOTRFABlocks.brick.getIcon(i, 5);
        }
        if (j == 6) {
            return LOTRFABlocks.rockSarllith.getIcon(i, 0);
        }
        if (j == 7) {
            return LOTRFABlocks.brick.getIcon(i, 6);
        }
        if (j == 8) {
            return LOTRFABlocks.brick.getIcon(i, 7);
        }
        if (j == 9) {
            return LOTRFABlocks.brick.getIcon(i, 8);
        }
        if (j == 10) {
            return LOTRFABlocks.brick.getIcon(i, 11);
        }
        if (j == 11) {
            return LOTRFABlocks.brick2.getIcon(i, 1);
        }
        if (j == 12) {
            return LOTRFABlocks.brick2.getIcon(i, 0);
        }
        return super.getIcon(i, j);
    }

}
