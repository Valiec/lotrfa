package eoa.lotrfa.common.block;

import lotr.common.block.LOTRBlockBrickBase;

public class LOTRFABrickGond extends LOTRBlockBrickBase {

    public LOTRFABrickGond() {
        this.setBrickNames("gondolin");
    }
}
