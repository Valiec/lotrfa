package eoa.lotrfa.common.block;

import lotr.common.block.LOTRBlockSmoothStoneBase;

public class LOTRFABlockSmoothStoneGond extends LOTRBlockSmoothStoneBase {

    public LOTRFABlockSmoothStoneGond() {
        this.setBrickNames("gondolin");
    }
}
