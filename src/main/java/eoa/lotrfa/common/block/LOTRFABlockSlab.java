package eoa.lotrfa.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lotr.common.block.LOTRBlockSlabBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemSlab;
import net.minecraft.util.IIcon;

public class LOTRFABlockSlab extends LOTRBlockSlabBase {

    public LOTRFABlockSlab(boolean flag) {
        super(flag, Material.rock, 8);
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public IIcon getIcon(int i, int j) {
        if ((j &= 7) == 0) {
            return LOTRFABlocks.brick.getIcon(i, 0);
        }
        if (j == 1) {
            return LOTRFABlocks.pillar.getIcon(i, 0);
        }
        if (j == 2) {
            return LOTRFABlocks.brick.getIcon(i, 1);
        }
        if (j == 3) {
            return LOTRFABlocks.pillar.getIcon(i, 1);
        }
        if (j == 4) {
            return LOTRFABlocks.brick.getIcon(i, 2);
        }
        if (j == 5) {
            return LOTRFABlocks.pillar.getIcon(i, 2);
        }
        if (j == 6) {
            return LOTRFABlocks.pillar.getIcon(i, 3);
        }
        if (j == 7) {
            return LOTRFABlocks.brick.getIcon(i, 3);
        }
        return super.getIcon(i, j);
    }

    public static class SlabDouble extends ItemSlab {
        public SlabDouble(Block block) {
            super(block, (BlockSlab) LOTRFABlocks.slabSingle, (BlockSlab) LOTRFABlocks.slabDouble, true);
        }
    }

    public static class SlabSingle extends ItemSlab {
        public SlabSingle(Block block) {
            super(block, (BlockSlab) LOTRFABlocks.slabSingle, (BlockSlab) LOTRFABlocks.slabDouble, false);
        }
    }

}
