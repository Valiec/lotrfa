package eoa.lotrfa.common.block.table;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.lotrfa.common.LOTRFA;
import lotr.common.*;
import lotr.common.block.LOTRBlockCraftingTable;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public abstract class LOTRFABlockCraftingTableBase extends LOTRBlockCraftingTable {
    /**
     * 0 = bottom
     * 1 = top
     * 2 = sides
     */
	@SideOnly(value=Side.CLIENT)
    protected IIcon[] tableIcons;
    
	public LOTRFABlockCraftingTableBase(Material material, LOTRFaction faction, int guiID) {
		super(material, faction, guiID);
	}
	
    @Override
    @SideOnly(value=Side.CLIENT)
    public IIcon getIcon(int i, int j) {
        if (i == 0) {
            return tableIcons[0];
        }
        else if (i == 1) {
            return this.tableIcons[1];
        }
        return this.tableIcons[2];
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public abstract void registerBlockIcons(IIconRegister iconregister);

    @Override
    public boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entityplayer, int side, float f, float f1, float f2) {
        boolean hasRequiredAlignment = LOTRLevelData.getData(entityplayer).getAlignment(this.tableFaction) >= 1;
        if (hasRequiredAlignment) {
            if (!world.isRemote) {
                entityplayer.openGui(LOTRFA.instance, this.tableGUIID, world, i, j, k);
            }
        } else {
            for (int l = 0; l < 8; ++l) {
                double d = i + world.rand.nextFloat();
                double d1 = j + 1.0;
                double d2 = k + world.rand.nextFloat();
                world.spawnParticle("smoke", d, d1, d2, 0.0, 0.0, 0.0);
            }
            if (!world.isRemote) {
                LOTRAlignmentValues.notifyAlignmentNotHighEnough(entityplayer, 1, this.tableFaction);
            }
        }
        return true;
    }
}
