package eoa.lotrfa.common.block.table;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.lotrfa.common.*;
import eoa.lotrfa.common.block.LOTRFABlocks;
import lotr.client.gui.LOTRGuiCraftingTable;
import lotr.common.inventory.LOTRContainerCraftingTable;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class LOTRBlockTablePettyDwarven extends LOTRFABlockCraftingTableBase {
    public LOTRBlockTablePettyDwarven() {
        super(Material.rock, LOTRFAFactions.pettyDwarf, CommonProxy.GUI_ID_PETTY_DWARVEN_TABLE);
        this.setStepSound(Block.soundTypeStone);
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconregister) {
        tableIcons = new IIcon[3];
        this.tableIcons[0] = iconregister.registerIcon(this.getTextureName() + "_bottom");
        this.tableIcons[1] = iconregister.registerIcon(this.getTextureName() + "_top");
        this.tableIcons[2] = iconregister.registerIcon(this.getTextureName() + "_side");
    }
    
    public static class Container extends LOTRContainerCraftingTable {
        public Container(InventoryPlayer inv, World world, int i, int j, int k) {
            super(inv, world, i, j, k, FARecipes.pettyDwarvenRecipes, LOTRFABlocks.craftingTablePettyDwarf);
        }
    }

    public static class Gui extends LOTRGuiCraftingTable {
        public Gui(InventoryPlayer inv, World world, int i, int j, int k) {
            super(new Container(inv, world, i, j, k), "pettyDwarf");
        }
    }
}
