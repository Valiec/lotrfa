package eoa.lotrfa.common;

import cpw.mods.fml.common.IFuelHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.IGuiHandler;
import eoa.lotrfa.common.block.table.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class CommonProxy implements IGuiHandler, IFuelHandler {
    //TODO make this beteer and do the same for the event handler
    public static LOTRFATickHandlerServer tickHandler;
    
	public static final int GUI_ID_ANGBAND_TABLE = 0;
	public static final int GUI_ID_BRETHIL_TABLE = 1;
	public static final int GUI_ID_DOR_DAIDELOS_TABLE = 2;
	public static final int GUI_ID_DORIATH_TABLE = 3;
	public static final int GUI_ID_DOR_LOMIN_TABLE = 4;
	public static final int GUI_ID_FALATHRIM_TABLE = 5;
	public static final int GUI_ID_FEANORIAN_TABLE = 6;
	public static final int GUI_ID_GONDOLIN_TABLE = 7;
	public static final int GUI_ID_HITHLUM_TABLE = 8;
	public static final int GUI_ID_HOUSE_BOR_TABLE = 9;
	public static final int GUI_ID_HOUSE_ULFANG_TABLE = 10;
	public static final int GUI_ID_LAEGRIM_TABLE = 11;
	public static final int GUI_ID_NARGOTHROND_TABLE = 12;
	public static final int GUI_ID_PETTY_DWARVEN_TABLE = 13;
	public static final int GUI_ID_TOL_IN_GAURHOTH_TABLE = 14;

    public void preInit(FMLPreInitializationEvent event) {
        tickHandler = new LOTRFATickHandlerServer();
    }

    public void init(FMLInitializationEvent event) {}

    public void registerRenderers() {}

    public void editTickHanderClient() {}

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer entityplayer, World world, int i, int j, int k) {
        if(ID == GUI_ID_ANGBAND_TABLE) {
        	return new LOTRBlockTableAngband.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_BRETHIL_TABLE) {
        	return new LOTRBlockTableBrethil.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_DOR_DAIDELOS_TABLE) {
        	return new LOTRBlockTableDorDaidelos.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_DORIATH_TABLE) {
        	return new LOTRBlockTableDoriath.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_DOR_LOMIN_TABLE) {
        	return new LOTRBlockTableDorLomin.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_FALATHRIM_TABLE) {
        	return new LOTRBlockTableFalathrim.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_FEANORIAN_TABLE) {
        	return new LOTRBlockTableFeanorian.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_GONDOLIN_TABLE) {
        	return new LOTRBlockTableGondolin.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_HITHLUM_TABLE) {
        	return new LOTRBlockTableHithlum.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_HOUSE_BOR_TABLE) {
        	return new LOTRBlockTableHouseBor.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_HOUSE_ULFANG_TABLE) {
        	return new LOTRBlockTableHouseUlfang.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_LAEGRIM_TABLE) {
        	return new LOTRBlockTableLaegrim.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_NARGOTHROND_TABLE) {
        	return new LOTRBlockTableNargothrond.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_PETTY_DWARVEN_TABLE) {
        	return new LOTRBlockTablePettyDwarven.Gui(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_TOL_IN_GAURHOTH_TABLE) {
        	return new LOTRBlockTolInGaurhothTable.Gui(entityplayer.inventory, world, i, j, k);
        }
        
        return null;
    }

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer entityplayer, World world, int i, int j, int k) {
        if(ID == GUI_ID_ANGBAND_TABLE) {
        	return new LOTRBlockTableAngband.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_BRETHIL_TABLE) {
        	return new LOTRBlockTableBrethil.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_DOR_DAIDELOS_TABLE) {
        	return new LOTRBlockTableDorDaidelos.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_DORIATH_TABLE) {
        	return new LOTRBlockTableDoriath.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_DOR_LOMIN_TABLE) {
        	return new LOTRBlockTableDorLomin.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_FALATHRIM_TABLE) {
        	return new LOTRBlockTableFalathrim.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_FEANORIAN_TABLE) {
        	return new LOTRBlockTableFeanorian.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_GONDOLIN_TABLE) {
        	return new LOTRBlockTableGondolin.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_HITHLUM_TABLE) {
        	return new LOTRBlockTableHithlum.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_HOUSE_BOR_TABLE) {
        	return new LOTRBlockTableHouseBor.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_HOUSE_ULFANG_TABLE) {
        	return new LOTRBlockTableHouseUlfang.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_LAEGRIM_TABLE) {
        	return new LOTRBlockTableLaegrim.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_NARGOTHROND_TABLE) {
        	return new LOTRBlockTableNargothrond.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_PETTY_DWARVEN_TABLE) {
        	return new LOTRBlockTablePettyDwarven.Container(entityplayer.inventory, world, i, j, k);
        }
        else if(ID == GUI_ID_TOL_IN_GAURHOTH_TABLE) {
        	return new LOTRBlockTolInGaurhothTable.Container(entityplayer.inventory, world, i, j, k);
        }
        
        return null;
    }

    @Override
    public int getBurnTime(ItemStack fuel) {
        // TODO Add these things once we get items which can burn
        return 0;
    }

}
