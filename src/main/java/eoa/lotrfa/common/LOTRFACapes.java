package eoa.lotrfa.common;

import lotr.common.LOTRCapes;
import net.minecraft.util.ResourceLocation;

public class LOTRFACapes extends LOTRCapes {
    //TODO make capes actual objects
    //From name or faction
    public static ResourceLocation angband;
    public static ResourceLocation brethil;
    public static ResourceLocation dorDaidelos;
    public static ResourceLocation dorLomin;
    public static ResourceLocation doriath;
    public static ResourceLocation falathrim;
    public static ResourceLocation feanorians;
    public static ResourceLocation gondolin;
    public static ResourceLocation hithlum;
    public static ResourceLocation houseBor;
    public static ResourceLocation houseUlfang;
    public static ResourceLocation laegrim;
    public static ResourceLocation nanDungortheb;
    public static ResourceLocation nargothrond;
    public static ResourceLocation pettyDwarf;
    public static ResourceLocation tolInGaurhoth;
    
    private static ResourceLocation forName(String name) {
        return new ResourceLocation("lotrfa:cape/" + name + ".png");
    }
    
    public static void init() {
        angband = forName("angband");
        brethil = forName("brethil");
        dorDaidelos = forName("dor_daidelos");
        dorLomin = forName("dor_lomin");
        doriath = forName("doriath");
        falathrim = forName("falathrim");
        feanorians = forName("feanorian");
        gondolin = forName("gondolin");
        hithlum = forName("hithlum");
        houseBor = forName("house_bor");
        houseUlfang = forName("house_ulfang");
        laegrim = forName("laegrim");
        nargothrond = forName("nargothrond");
        pettyDwarf = forName("petty_dwarf");
        tolInGaurhoth = forName("tol_in_gaurhoth");
    }

}
