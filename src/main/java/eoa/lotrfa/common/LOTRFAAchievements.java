package eoa.lotrfa.common;

import java.util.ArrayList;
import java.util.List;
import eoa.lotrfa.common.block.LOTRFABlocks;
import eoa.lotrfa.common.item.LOTRFAItems;
import eoa.lotrfa.common.item.LOTRFAMaterial;
import eoa.lotrfa.common.reflection.LOTRFAEnumHelper;
import eoa.lotrfa.common.reflection.LOTRFAReflectionHelper;
import eoa.lotrfa.common.world.biome.LOTRFABiomes;
import lotr.common.*;
import lotr.common.LOTRAchievement.Category;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;

public class LOTRFAAchievements {
    public static LOTRAchievement alignment_angband[];
    public static LOTRAchievement alignment_brethil[];
    public static LOTRAchievement alignment_dorDaidelos[];
    public static LOTRAchievement alignment_dorLomin[];
    public static LOTRAchievement alignment_doriath[];
    public static LOTRAchievement alignment_falathrim[];
    public static LOTRAchievement alignment_feanorians[];
    public static LOTRAchievement alignment_gondolin[];
    public static LOTRAchievement alignment_hithlum[];
    public static LOTRAchievement alignment_houseBor[];
    public static LOTRAchievement alignment_houseUlfang[];
    public static LOTRAchievement alignment_laegrim[];
    public static LOTRAchievement alignment_nargothrond[];
    public static LOTRAchievement alignment_pettyDwarf[];
    public static LOTRAchievement alignment_tolInGaurhoth[];
    
    public static LOTRAchievement doMiniquestAngband;
    public static LOTRAchievement doMiniquestBrethil;
    public static LOTRAchievement doMiniquestDoriath;
    public static LOTRAchievement doMiniquestFalathrim;
    public static LOTRAchievement doMiniquestFeanorian;
    public static LOTRAchievement doMiniquestHouseBor;
    public static LOTRAchievement doMiniquestGondolin;
    public static LOTRAchievement doMiniquestHithlum;
    public static LOTRAchievement doMiniquestDorLomin;
    public static LOTRAchievement doMiniquestHouseUlfang;
    public static LOTRAchievement doMiniquestTolInGaurhoth;
    public static LOTRAchievement doMiniquestLaegrim;
    public static LOTRAchievement doMiniquestNargothrond;
    public static LOTRAchievement doMiniquestPettyDwarf;
    public static LOTRAchievement doMiniquestDorDaidelos;

    public static LOTRAchievement killAngbandOrc;
    public static LOTRAchievement killDorDaedelothOrc;
    public static LOTRAchievement killUtumnoRemnantOrc;
    public static LOTRAchievement killDorDaidelosOrc;
    public static LOTRAchievement killTolInGaurhothOrc;
    public static LOTRAchievement killDorDaidelosIceWarg;
    public static LOTRAchievement killDorDaedelothObsidianWarg;
    public static LOTRAchievement killAngbandFireWarg;
    
    public static LOTRAchievement killUlfangMan;
    public static LOTRAchievement killBorMan;
    public static LOTRAchievement killDorLominMan;
    public static LOTRAchievement killBrethilMan;
    
    public static LOTRAchievement killGondolinElf;
    public static LOTRAchievement killFeanorianElf;
    public static LOTRAchievement killNargothrondElf;
    public static LOTRAchievement killDoriathElf;
    public static LOTRAchievement killHithlumElf;
    public static LOTRAchievement killLaegrimElf;
    public static LOTRAchievement killFalathrimElf;
    
    public static LOTRAchievement killPettyDwarf;
    
    public static LOTRAchievement killBoldog;
    public static LOTRAchievement killWraith;
    public static LOTRAchievement killNanDungorthebSpider;
    
    public static LOTRAchievement tradeDorDaedelothOrcTrader;
    public static LOTRAchievement tradeDorDaedelothOrcCaptain;
    public static LOTRAchievement tradeAngbandOrcTrader;
    public static LOTRAchievement tradeAngbandOrcCaptain;
    public static LOTRAchievement tradeTolInGaurhothOrcTrader;
    public static LOTRAchievement tradeTolInGaurhothOrcCaptain;
    public static LOTRAchievement tradeDorDaidelosOrcTrader;
    public static LOTRAchievement tradeDorDaidelosOrcCaptain;
    public static LOTRAchievement tradeUtumnoRemnantOrcCaptain;
    
    public static LOTRAchievement tradeUlfangTrader;
    public static LOTRAchievement tradeUlfangWarlord;
    public static LOTRAchievement tradeDorLominTrader;
    public static LOTRAchievement tradeDorLominCaptain;
    public static LOTRAchievement hireDorLominFarmer;
    public static LOTRAchievement tradeBorTrader;
    public static LOTRAchievement tradeBorCaptain;
    public static LOTRAchievement hireBorFarmer;
    public static LOTRAchievement tradeBrethilTrader;
    public static LOTRAchievement tradeBrethilCaptain;
    
    public static LOTRAchievement tradePettyDwarfTrader;
    public static LOTRAchievement tradePettyDwarfCommander;
    
    public static LOTRAchievement tradeLaegrimTrader;
    public static LOTRAchievement tradeLaegrimLord;
    public static LOTRAchievement tradeDoriathTrader;
    public static LOTRAchievement tradeDoriathLord;
    public static LOTRAchievement tradeFeanorianTrader;
    public static LOTRAchievement tradeFeanorianLord;
    public static LOTRAchievement tradeNargothrondTrader;
    public static LOTRAchievement tradeNargothrondLord;
    public static LOTRAchievement tradeFalathrimTrader;
    public static LOTRAchievement tradeFalathrimLord;
    public static LOTRAchievement tradeHithlumTrader;
    public static LOTRAchievement tradeHithlumLord;
    public static LOTRAchievement tradeGondolinTrader;
    public static LOTRAchievement tradeGondolinLord;

    public static LOTRAchievement usePettyDwarvenTable;
    public static LOTRAchievement useGondolinTable;
    public static LOTRAchievement useNargothrondTable;
    public static LOTRAchievement useFalasTable;
    public static LOTRAchievement useBrethilTable;
    public static LOTRAchievement useDorLominTable;
    public static LOTRAchievement useFeanorTable;
    public static LOTRAchievement useDoriathTable;
    public static LOTRAchievement useHithlumTable;
    public static LOTRAchievement useLaegrimTable;
    public static LOTRAchievement useBorTable;
    public static LOTRAchievement useUlfangTable;
    public static LOTRAchievement useTolInGaurhothTable;
    public static LOTRAchievement useDorDaidelosTable;
    public static LOTRAchievement useAngbandTable;

    public static LOTRAchievement hireHithlumVinekeeper;

    public static LOTRAchievement enterHelcaraxe;
    public static LOTRAchievement enterNanGuruthos;
    public static LOTRAchievement enterNanDuaith;
    public static LOTRAchievement enterEredEngrin;
    public static LOTRAchievement enterAngband;
    public static LOTRAchievement enterDorDaedeloth;
    public static LOTRAchievement enterAnfauglith;
    public static LOTRAchievement enterLothlann;
    public static LOTRAchievement enterTaurdru;
    public static LOTRAchievement enterHithlum;
    public static LOTRAchievement enterMithrim;
    public static LOTRAchievement enterEredWethrin;
    public static LOTRAchievement enterMithrimMountains;
    public static LOTRAchievement enterEredLomin;
    public static LOTRAchievement enterLammoth;
    public static LOTRAchievement enterDorLomin;
    public static LOTRAchievement enterBaradEithel;
    public static LOTRAchievement enterNevrast;
    public static LOTRAchievement enterVinyamar;
    public static LOTRAchievement enterTaurNuFuin;
    public static LOTRAchievement enterBarahirHideout;
    public static LOTRAchievement enterLadros;
    public static LOTRAchievement enterEchoriath;
    public static LOTRAchievement enterTumladen;
    public static LOTRAchievement enterEredGorgoroth;
    public static LOTRAchievement enterTolInGaurhoth;
    public static LOTRAchievement enterPassSirion;
    public static LOTRAchievement enterFenSerech;
    public static LOTRAchievement enterHimring;
    public static LOTRAchievement enterHimlad;
    public static LOTRAchievement enterMarchMaedhros;
    public static LOTRAchievement enterAmonEreb;
    public static LOTRAchievement enterWestBeleriand;
    public static LOTRAchievement enterTumhalad;
    public static LOTRAchievement enterValeSirion;
    public static LOTRAchievement enterDimbar;
    public static LOTRAchievement enterWoodNuath;
    public static LOTRAchievement enterIvrin;
    public static LOTRAchievement enterFalas;
    public static LOTRAchievement enterBrethil;
    public static LOTRAchievement enterAmonRudh;
    public static LOTRAchievement enterBaradNimras;
    public static LOTRAchievement enterArvernien;
    public static LOTRAchievement enterNimbrethil;
    public static LOTRAchievement enterNanTathren;
    public static LOTRAchievement enterTaurEnFaroth;
    public static LOTRAchievement enterGirdleMelian;
    public static LOTRAchievement enterNanDungortheb;
    public static LOTRAchievement enterDorDinen;
    public static LOTRAchievement enterDryRiver;
    public static LOTRAchievement enterGardhBoren;
    public static LOTRAchievement enterEstolad;
    public static LOTRAchievement enterLisgardh;
    public static LOTRAchievement enterAelinUial;
    public static LOTRAchievement enterEastBeleriand;
    public static LOTRAchievement enterSouthBeleriand;
    public static LOTRAchievement enterBalar;
    public static LOTRAchievement enterTolGalen;
    public static LOTRAchievement enterNogrodMtns;
    public static LOTRAchievement enterThargelion;
    public static LOTRAchievement enterBelegostMtns;
    public static LOTRAchievement enterOssiriand;
    public static LOTRAchievement enterTalathDirnen;
    public static LOTRAchievement enterNanElmoth;
    public static LOTRAchievement enterTaurImDuinath;
    public static LOTRAchievement enterAndram;
    public static LOTRAchievement enterNargothrond;
    public static LOTRAchievement enterDoriath;
    
    public static LOTRAchievement wearFullFeanorian;
    public static LOTRAchievement wearFullPettyDwarf;
    public static LOTRAchievement wearFullDorDaidelos;
    public static LOTRAchievement wearFullNargothrondRanger;
    public static LOTRAchievement wearFullHouseBor;
    public static LOTRAchievement wearFullHouseBorCaptain;
    public static LOTRAchievement wearFullHouseBeor;
    public static LOTRAchievement wearFullBrethil;
    public static LOTRAchievement wearFullBrethilRanger;
    public static LOTRAchievement wearFullDorLomin;
    
    public static LOTRAchievement smeltPettyDwarfSteel;
   
    public static void init() {
        Categories.init();
        editAchievements();
        addAchievements();
    }

    private static void editAchievements() {
        removeAchievement(LOTRAchievement.enterMeneltarma);
        
        // Hithlum
        addAchievementToCategory(LOTRAchievement.smeltElfSteel, Categories.hithlum, 26);
        addAchievementToCategory(LOTRAchievement.wearFullHighElven, Categories.hithlum, 27);
        
        // Doriath
        addAchievementToCategory(LOTRAchievement.wearFullElven, Categories.doriath, 12);
        
        // Falas
        addAchievementToCategory(LOTRAchievement.wearFullDolAmroth, Categories.falas, 11);
        
        // Lothlann
        addAchievementToCategory(LOTRAchievement.wearFullDunlending, Categories.lothlann, 11);
        addAchievementToCategory(LOTRAchievement.wearFullGundabadUruk, Categories.lothlann, 12);
        
        // Ossiriand
        addAchievementToCategory(LOTRAchievement.wearFullWoodElven, Categories.ossiriand, 10);
        addAchievementToCategory(LOTRAchievement.wearFullWoodElvenScout, Categories.ossiriand, 11);
        
        // Taur-nu-Fuin
        addAchievementToCategory(LOTRAchievement.wearFullDolGuldur, Categories.taurNuFuin, 22);
        
        // West Beleriand
        addAchievementToCategory(LOTRAchievement.wearFullRivendell, Categories.westBeleriand, 21);
        
        // Amon Rudh
        
        // Gondolin
        addAchievementToCategory(LOTRAchievement.wearFullGalvorn, Categories.gondolin, 30);
        addAchievementToCategory(LOTRAchievement.wearFullGondolin, Categories.gondolin, 31);
        
        // Dor Daidelos
        addAchievementToCategory(LOTRAchievement.enterForodwaith, Categories.dorDaidelos, 4);
        addAchievementToCategory(LOTRAchievement.smeltUrukSteel, Categories.dorDaidelos, 17);
        addAchievementToCategory(LOTRAchievement.wearFullUruk, Categories.dorDaidelos, 18);
        
        // Ard Galen
        addAchievementToCategory(LOTRAchievement.killTroll, Categories.ardGalen, 13);
        addAchievementToCategory(LOTRAchievement.killMountainTroll, Categories.ardGalen, 14);
        addAchievementToCategory(LOTRAchievement.killBalrog, Categories.ardGalen, 15);
        addAchievementToCategory(LOTRAchievement.killTormentedElf, Categories.ardGalen, 16);
        addAchievementToCategory(LOTRAchievement.smeltOrcSteel, Categories.ardGalen, 21);
        addAchievementToCategory(LOTRAchievement.wearFullOrc, Categories.ardGalen, 22);
        addAchievementToCategory(LOTRAchievement.smeltBlackUrukSteel, Categories.ardGalen, 23);
        addAchievementToCategory(LOTRAchievement.wearFullBlackUruk, Categories.ardGalen, 24);
        addAchievementToCategory(LOTRAchievement.mineNaurite, Categories.ardGalen, 25);
        addAchievementToCategory(LOTRAchievement.mineGulduril, Categories.ardGalen, 26);
        addAchievementToCategory(LOTRAchievement.craftOrcBomb, Categories.ardGalen, 27);
        
        // South Beleriand
        addAchievementToCategory(LOTRAchievement.alignmentGood10_FANGORN, Categories.southBeleriand, 1);
        addAchievementToCategory(LOTRAchievement.alignmentGood100_FANGORN, Categories.southBeleriand, 2);
        addAchievementToCategory(LOTRAchievement.alignmentGood1000_FANGORN, Categories.southBeleriand, 3);
        addAchievementToCategory(LOTRAchievement.killEnt, Categories.southBeleriand, 6);
        addAchievementToCategory(LOTRAchievement.killHuorn, Categories.southBeleriand, 7);
        addAchievementToCategory(LOTRAchievement.talkEnt, Categories.southBeleriand, 8);
        addAchievementToCategory(LOTRAchievement.drinkEntDraught, Categories.southBeleriand, 9);
    }

    private static void addAchievements() {
        // Ard Galen
        alignment_angband = createAlignmentAchievements(LOTRFAFactions.angband, Categories.ardGalen, 1);
        enterAngband = createBiomeAchievement(Categories.ardGalen, 4, new ItemStack(LOTRMod.brick, 1, 0), "enterAngband");
        enterDorDaedeloth = createBiomeAchievement(Categories.ardGalen, 5, new ItemStack(LOTRMod.rock, 1, 0), "enterDorDaedeloth");
        enterAnfauglith = createBiomeAchievement(Categories.ardGalen, 6, LOTRFABlocks.anfauglithAsh, "enterAnfauglith");
        enterNanDuaith = createBiomeAchievement(Categories.ardGalen, 7, new ItemStack(Blocks.sapling, 1, 1), "enterNanDuaith");
        useAngbandTable = new LOTRAchievement(Categories.ardGalen, 8, LOTRFABlocks.craftingTableAngband, "useAngbandTable").setRequiresAlly(LOTRFAFactions.angband);
        doMiniquestAngband = new LOTRAchievement(Categories.ardGalen, 9, LOTRMod.redBook, "doMiniquestAngband").setRequiresAlly(LOTRFAFactions.angband);
        killAngbandOrc = new LOTRAchievement(Categories.ardGalen, 10, LOTRMod.helmetBlackUruk, "killAngbandOrc").setRequiresEnemy(LOTRFAFactions.angband).createTitle();
        killDorDaedelothOrc = new LOTRAchievement(Categories.ardGalen, 11, LOTRMod.orcBone, "killDorDaedelothOrc").setRequiresEnemy(LOTRFaction.getAllOfType(LOTRFaction.FactionType.TYPE_ORC)).createTitle();
        killBoldog = new LOTRAchievement(Categories.ardGalen, 12, LOTRMod.trollBone, "killBoldog").setRequiresEnemy(LOTRFAFactions.angband).createTitle();
        tradeDorDaedelothOrcTrader = new LOTRAchievement(Categories.ardGalen, 17, LOTRMod.silverCoin, "tradeDorDaedelothOrcTrader").setRequiresAlly(LOTRFAFactions.angband);
        tradeDorDaedelothOrcCaptain = new LOTRAchievement(Categories.ardGalen, 18, LOTRMod.silverCoin, "tradeDorDaedelothOrcCaptain").setRequiresAlly(LOTRFAFactions.angband);
        tradeAngbandOrcTrader = new LOTRAchievement(Categories.ardGalen, 19, LOTRMod.silverCoin, "tradeAngbandOrcTrader").setRequiresAlly(LOTRFAFactions.angband);
        tradeAngbandOrcCaptain = new LOTRAchievement(Categories.ardGalen, 20, LOTRMod.silverCoin, "tradeAngbandOrcCaptain").setRequiresAlly(LOTRFAFactions.angband);
        killAngbandFireWarg = new LOTRAchievement(Categories.ardGalen, 28, Items.iron_sword, "killAngbandFireWarg").setRequiresEnemy(LOTRFAFactions.angband).createTitle();
        killDorDaedelothObsidianWarg = new LOTRAchievement(Categories.ardGalen, 29, LOTRMod.stalactiteObsidian, "killDorDaedelothObsidianWarg").setRequiresEnemy(LOTRFAFactions.angband).createTitle();

        // Brethil
        alignment_brethil = createAlignmentAchievements(LOTRFAFactions.brethil, Categories.brethil, 1);
        enterBrethil = createBiomeAchievement(Categories.brethil, 4, LOTRFAItems.brethilBattleaxe, "enterBrethil");
        enterDimbar = createBiomeAchievement(Categories.brethil, 5, Blocks.grass, "enterDimbar");
        useBrethilTable = new LOTRAchievement(Categories.brethil, 6, LOTRFABlocks.craftingTableBrethil, "useBrethilTable").setRequiresAlly(LOTRFAFactions.brethil);
        doMiniquestBrethil = new LOTRAchievement(Categories.brethil, 7, LOTRMod.redBook, "doMiniquestBrethil").setRequiresAlly(LOTRFAFactions.brethil);
        killBrethilMan = new LOTRAchievement(Categories.brethil, 8, Items.bone, "killBrethilMan").setRequiresEnemy(LOTRFAFactions.brethil).createTitle();
        tradeBrethilTrader = new LOTRAchievement(Categories.brethil, 9, LOTRMod.silverCoin, "tradeBrethilTrader").setRequiresAlly(LOTRFAFactions.brethil);
        tradeBrethilCaptain = new LOTRAchievement(Categories.brethil, 10, LOTRMod.silverCoin, "tradeBrethilCaptain").setRequiresAlly(LOTRFAFactions.brethil);
        wearFullBrethil = new LOTRAchievement(Categories.brethil, 11, LOTRFAItems.brethilChestplate, "wearFullBrethil");
        wearFullBrethilRanger = new LOTRAchievement(Categories.brethil, 12, LOTRFAItems.brethilRangerChestplate, "wearFullBrethilRanger");
        
        // Doriath
        alignment_doriath = createAlignmentAchievements(LOTRFAFactions.doriath, Categories.doriath, 1);
        enterGirdleMelian = createBiomeAchievement(Categories.doriath, 4, LOTRMod.mirkVines, "enterGirdleMelian");
        enterDoriath = createBiomeAchievement(Categories.doriath, 5, new ItemStack(LOTRMod.sapling2, 1, 2), "enterDoriath");
        enterNanElmoth = createBiomeAchievement(Categories.doriath, 6, new ItemStack(LOTRMod.sapling, 1, 2), "enterNanElmoth");
        useDoriathTable = new LOTRAchievement(Categories.doriath, 7, LOTRFABlocks.craftingTableBrethil, "useDoriathTable").setRequiresAlly(LOTRFAFactions.doriath);
        doMiniquestDoriath = new LOTRAchievement(Categories.doriath, 8, LOTRMod.redBook, "doMiniquestDoriath").setRequiresAlly(LOTRFAFactions.doriath);
        killDoriathElf = new LOTRAchievement(Categories.doriath, 9, LOTRMod.elfBone, "killDoriathElf").setRequiresEnemy(LOTRFAFactions.doriath).createTitle();
        tradeDoriathTrader = new LOTRAchievement(Categories.doriath, 10, LOTRMod.silverCoin, "tradeDoriathTrader").setRequiresAlly(LOTRFAFactions.doriath);
        tradeDoriathLord = new LOTRAchievement(Categories.doriath, 11, LOTRMod.silverCoin, "tradeDoriathLord").setRequiresAlly(LOTRFAFactions.doriath);

        // East Beleriand
        enterEastBeleriand = createBiomeAchievement(Categories.eastBeleriand, 1, Blocks.grass, "enterEastBeleriand");
        enterEstolad = createBiomeAchievement(Categories.eastBeleriand, 2, new ItemStack(Blocks.wool, 1, 12), "enterEstolad");
        enterAndram = createBiomeAchievement(Categories.eastBeleriand, 3, Blocks.stone, "enterAndram");
        enterAmonEreb = createBiomeAchievement(Categories.eastBeleriand, 4, LOTRFAItems.feanorianSword, "enterAmonEreb");

        // Falas
        alignment_falathrim = createAlignmentAchievements(LOTRFAFactions.falathrim, Categories.falas, 1);
        enterFalas = createBiomeAchievement(Categories.falas, 4, new ItemStack(LOTRFABlocks.brick, 1, 5), "enterFalas");
        enterBaradNimras = createBiomeAchievement(Categories.falas, 5, LOTRMod.daggerRivendell, "enterBaradNimras");
        useFalasTable = new LOTRAchievement(Categories.falas, 6, LOTRFABlocks.craftingTableFalathrim, "useFalasTable").setRequiresAlly(LOTRFAFactions.falathrim);
        doMiniquestFalathrim = new LOTRAchievement(Categories.falas, 7, LOTRMod.redBook, "doMiniquestFalathrim").setRequiresAlly(LOTRFAFactions.falathrim);
        killFalathrimElf = new LOTRAchievement(Categories.falas, 8, LOTRMod.elfBone, "killFalathrimElf").setRequiresEnemy(LOTRFAFactions.falathrim).createTitle();
        tradeFalathrimTrader = new LOTRAchievement(Categories.falas, 9, LOTRMod.silverCoin, "tradeFalathrimTrader").setRequiresAlly(LOTRFAFactions.falathrim);
        tradeFalathrimLord = new LOTRAchievement(Categories.falas, 10, LOTRMod.silverCoin, "tradeFalathrimLord").setRequiresAlly(LOTRFAFactions.falathrim);
        
        // March of Maedhros
        alignment_feanorians = createAlignmentAchievements(LOTRFAFactions.feanorians, Categories.marchMaedhros, 1);
        alignment_houseBor = createAlignmentAchievements(LOTRFAFactions.houseBor, Categories.marchMaedhros, 4);
        enterMarchMaedhros = createBiomeAchievement(Categories.marchMaedhros, 7, LOTRFAItems.feanorianHorseArmor, "enterMarchMaedhros");
        enterHimring = createBiomeAchievement(Categories.marchMaedhros, 8, new ItemStack(LOTRFABlocks.brick, 1, 4), "enterHimring");
        enterHimlad = createBiomeAchievement(Categories.marchMaedhros, 9, LOTRFAItems.feanorianHelmet, "enterFeanorLands");
        enterThargelion = createBiomeAchievement(Categories.marchMaedhros, 10, new ItemStack(LOTRFABlocks.brick, 1, 7), "enterThargelion");
        enterGardhBoren = createBiomeAchievement(Categories.marchMaedhros, 11, new ItemStack(LOTRMod.brick5, 1, 1), "enterGardhBoren");
        useFeanorTable = new LOTRAchievement(Categories.marchMaedhros, 12, LOTRFABlocks.craftingTableFeanorian, "useFeanorTable").setRequiresAlly(LOTRFAFactions.feanorians);
        useBorTable = new LOTRAchievement(Categories.marchMaedhros, 13, LOTRFABlocks.craftingTableHouseBor, "useBorTable").setRequiresAlly(LOTRFAFactions.houseBor);
        doMiniquestFeanorian = new LOTRAchievement(Categories.marchMaedhros, 14, LOTRMod.redBook, "doMiniquestFeanorian").setRequiresAlly(LOTRFAFactions.feanorians);
        doMiniquestHouseBor = new LOTRAchievement(Categories.marchMaedhros, 15, LOTRMod.redBook, "doMiniquestHouseBor").setRequiresAlly(LOTRFAFactions.houseBor);
        killFeanorianElf = new LOTRAchievement(Categories.marchMaedhros, 16, LOTRMod.elfBone, "killFeanorianElf").setRequiresEnemy(LOTRFAFactions.feanorians).createTitle();
        killBorMan = new LOTRAchievement(Categories.marchMaedhros, 17, Items.bone, "killBorMan").setRequiresEnemy(LOTRFAFactions.houseBor).createTitle();
        tradeFeanorianTrader = new LOTRAchievement(Categories.marchMaedhros, 18, LOTRMod.silverCoin, "tradeFeanorianTrader").setRequiresAlly(LOTRFAFactions.feanorians);
        tradeFeanorianLord = new LOTRAchievement(Categories.marchMaedhros, 19, LOTRMod.silverCoin, "tradeFeanorianLord").setRequiresAlly(LOTRFAFactions.feanorians);
        tradeBorTrader = new LOTRAchievement(Categories.marchMaedhros, 20, LOTRMod.silverCoin, "tradeBorTrader").setRequiresAlly(LOTRFAFactions.houseBor);
        tradeBorCaptain = new LOTRAchievement(Categories.marchMaedhros, 21, LOTRMod.silverCoin, "tradeBorCaptain").setRequiresAlly(LOTRFAFactions.houseBor);
        hireBorFarmer = new LOTRAchievement(Categories.marchMaedhros, 22, LOTRMod.silverCoin, "hireBorFarmer").setRequiresAlly(LOTRFAFactions.houseBor);
        wearFullFeanorian = new LOTRAchievement(Categories.marchMaedhros, 23, LOTRFAItems.feanorianChestplate, "wearFullFeanorian");
        wearFullHouseBorCaptain = new LOTRAchievement(Categories.marchMaedhros, 24, LOTRFAItems.borCaptainChestplate, "wearFullHouseBorCaptain");
        wearFullHouseBor = new LOTRAchievement(Categories.marchMaedhros, 25, LOTRFAItems.borChestplate, "wearFullHouseBor");
        
        // Gondolin
        alignment_gondolin = createAlignmentAchievements(LOTRFAFactions.gondolin, Categories.gondolin, 1);
        enterTumladen = createBiomeAchievement(Categories.gondolin, 4, LOTRMod.helmetGondolin, "enterTumladen");
        enterEchoriath = createBiomeAchievement(Categories.gondolin, 5, Blocks.stone, "enterEchoriath");
        enterDryRiver = createBiomeAchievement(Categories.gondolin, 6, Blocks.dirt, "enterDryRiver");
        useGondolinTable = new LOTRAchievement(Categories.gondolin, 7, LOTRFABlocks.craftingTableGondolin, "useGondolinTable").setRequiresAlly(LOTRFAFactions.gondolin);
        doMiniquestGondolin = new LOTRAchievement(Categories.gondolin, 8, LOTRMod.redBook, "doMiniquestGondolin").setRequiresAlly(LOTRFAFactions.gondolin);
        killGondolinElf = new LOTRAchievement(Categories.gondolin, 9, LOTRMod.elfBone, "killGondolinElf").setRequiresEnemy(LOTRFAFactions.gondolin).createTitle();
        tradeGondolinTrader = new LOTRAchievement(Categories.gondolin, 10, LOTRMod.silverCoin, "tradeGondolinTrader").setRequiresAlly(LOTRFAFactions.gondolin);
        tradeGondolinLord = new LOTRAchievement(Categories.gondolin, 11, LOTRMod.silverCoin, "tradeGondolinLord").setRequiresAlly(LOTRFAFactions.gondolin);
        
        // Hithlum
        alignment_hithlum = createAlignmentAchievements(LOTRFAFactions.hithlum, Categories.hithlum, 1);
        alignment_dorLomin = createAlignmentAchievements(LOTRFAFactions.dorLomin, Categories.hithlum, 4);
        enterHithlum = createBiomeAchievement(Categories.hithlum, 7, new ItemStack(LOTRMod.brick3, 1, 2), "enterHithlum");
        enterMithrim = createBiomeAchievement(Categories.hithlum, 8, Blocks.grass, "enterMithrim");
        enterDorLomin = createBiomeAchievement(Categories.hithlum, 9, LOTRMod.helmetPinnathGelin, "enterDorLomin");
        enterNevrast = createBiomeAchievement(Categories.hithlum, 10, Blocks.waterlily, "enterNevrast");
        enterLammoth = createBiomeAchievement(Categories.hithlum, 11, Blocks.grass, "enterLammoth");
        enterEredWethrin = createBiomeAchievement(Categories.hithlum, 12, Blocks.stone, "enterEredWethrin");
        enterEredLomin = createBiomeAchievement(Categories.hithlum, 13, Blocks.stone, "enterEredLomin");
        enterBaradEithel = createBiomeAchievement(Categories.hithlum, 14, new ItemStack(LOTRMod.brick3, 1, 2), "enterBaradEithel");
        enterVinyamar = createBiomeAchievement(Categories.hithlum, 15, new ItemStack(LOTRFABlocks.brick2, 1, 0), "enterVinyamar");
        useHithlumTable = new LOTRAchievement(Categories.hithlum, 16, LOTRFABlocks.craftingTableHithlum, "useHithlumTable").setRequiresAlly(LOTRFAFactions.hithlum);
        useDorLominTable = new LOTRAchievement(Categories.hithlum, 17, LOTRFABlocks.craftingTableDorLomin, "useDorLominTable").setRequiresAlly(LOTRFAFactions.dorLomin);
        doMiniquestHithlum = new LOTRAchievement(Categories.hithlum, 18, LOTRMod.redBook, "doMiniquestHithlum").setRequiresAlly(LOTRFAFactions.hithlum);
        doMiniquestDorLomin = new LOTRAchievement(Categories.hithlum, 19, LOTRMod.redBook, "doMiniquestDorLomin").setRequiresAlly(LOTRFAFactions.dorLomin);
        killHithlumElf = new LOTRAchievement(Categories.hithlum, 20, LOTRMod.elfBone, "killHithlumElf").setRequiresEnemy(LOTRFAFactions.hithlum).createTitle();
        killDorLominMan = new LOTRAchievement(Categories.hithlum, 21, Items.bone, "killDorLominMan").setRequiresEnemy(LOTRFAFactions.dorLomin).createTitle();
        tradeHithlumTrader = new LOTRAchievement(Categories.hithlum, 22, LOTRMod.silverCoin, "tradeHithlumTrader").setRequiresAlly(LOTRFAFactions.hithlum);
        tradeHithlumLord = new LOTRAchievement(Categories.hithlum, 23, LOTRMod.silverCoin, "tradeHithlumLord").setRequiresAlly(LOTRFAFactions.hithlum);
        tradeDorLominTrader = new LOTRAchievement(Categories.hithlum, 24, LOTRMod.silverCoin, "tradeDorLominTrader").setRequiresAlly(LOTRFAFactions.dorLomin);
        tradeDorLominCaptain = new LOTRAchievement(Categories.hithlum, 25, LOTRMod.silverCoin, "tradeDorLominCaptain").setRequiresAlly(LOTRFAFactions.dorLomin);
        hireDorLominFarmer = new LOTRAchievement(Categories.hithlum, 29, LOTRMod.silverCoin, "hireDorlominFarmer").setRequiresAlly(LOTRFAFactions.dorLomin);
        wearFullHouseBeor = new LOTRAchievement(Categories.hithlum, 30, LOTRFAItems.houseBeorChestplate, "wearFullHouseBeor");
        hireHithlumVinekeeper = new LOTRAchievement(Categories.hithlum, 31, LOTRMod.seedsGrapeRed, "hireHithlumVinekeeper");
        wearFullDorLomin = new LOTRAchievement(Categories.hithlum, 28, LOTRFAItems.dorLominChestplate, "wearFullDorLomin");
        
        // Lothlann
        alignment_houseUlfang = createAlignmentAchievements(LOTRFAFactions.houseUlfang, Categories.lothlann, 1);
        enterLothlann = createBiomeAchievement(Categories.lothlann, 4, LOTRMod.wasteBlock, "enterLothlann");
        enterTaurdru = createBiomeAchievement(Categories.lothlann, 5, new ItemStack(LOTRMod.brick3, 1, 10), "enterTaurdru");
        useUlfangTable = new LOTRAchievement(Categories.lothlann, 6, LOTRFABlocks.craftingTableHouseUlfang, "useUlfangTable").setRequiresAlly(LOTRFAFactions.houseUlfang);
        doMiniquestHouseUlfang = new LOTRAchievement(Categories.lothlann, 7, LOTRMod.redBook, "doMiniquestHouseUlfang").setRequiresAlly(LOTRFAFactions.houseUlfang);
        killUlfangMan = new LOTRAchievement(Categories.lothlann, 8, Items.bone, "killUlfangMan").setRequiresEnemy(LOTRFAFactions.houseUlfang).createTitle();
        tradeUlfangTrader = new LOTRAchievement(Categories.lothlann, 9, LOTRMod.silverCoin, "tradeLothlannTrader").setRequiresAlly(LOTRFAFactions.houseUlfang);
        tradeUlfangWarlord = new LOTRAchievement(Categories.lothlann, 10, LOTRMod.silverCoin, "tradeLothlannWarlord").setRequiresAlly(LOTRFAFactions.houseUlfang);
        
        // Taur-nu-Fuin
        alignment_tolInGaurhoth = createAlignmentAchievements(LOTRFAFactions.tolInGaurhoth, Categories.taurNuFuin, 1);
        enterTaurNuFuin = createBiomeAchievement(Categories.taurNuFuin, 7, new ItemStack(LOTRMod.sapling5, 1, 0), "enterTaurNuFuin");
        enterTolInGaurhoth = createBiomeAchievement(Categories.taurNuFuin, 8, new ItemStack(LOTRMod.brick2, 1, 8), "enterTolInGaurhoth");
        enterLadros = createBiomeAchievement(Categories.taurNuFuin, 10, Blocks.dirt, "enterLadros");
        enterPassSirion = createBiomeAchievement(Categories.taurNuFuin, 11, Blocks.grass, "enterPassSirion");
        enterEredGorgoroth = createBiomeAchievement(Categories.taurNuFuin, 12, Blocks.stone, "enterEredGorgoroth");
        enterNanDungortheb = createBiomeAchievement(Categories.taurNuFuin, 13, LOTRMod.webUngoliant, "enterNanDungortheb");
        enterFenSerech = createBiomeAchievement(Categories.taurNuFuin, 14, Blocks.waterlily, "enterFenSerech");
        useTolInGaurhothTable = new LOTRAchievement(Categories.taurNuFuin, 15, LOTRFABlocks.craftingTableTolInGaurhoth, "useTolInGaurhothTable").setRequiresAlly(LOTRFAFactions.tolInGaurhoth);
        doMiniquestTolInGaurhoth = new LOTRAchievement(Categories.taurNuFuin, 16, LOTRMod.redBook, "doMiniquestTolInGaurhoth").setRequiresAlly(LOTRFAFactions.tolInGaurhoth);
        killTolInGaurhothOrc = new LOTRAchievement(Categories.taurNuFuin, 17, LOTRMod.orcBone, "killTolInGaurhothOrc").setRequiresEnemy(LOTRFAFactions.tolInGaurhoth).createTitle();
        killWraith = new LOTRAchievement(Categories.taurNuFuin, 18, Items.bone, "killWraith").setRequiresEnemy(LOTRFAFactions.tolInGaurhoth).createTitle();
        killNanDungorthebSpider = new LOTRAchievement(Categories.taurNuFuin, 19, LOTRMod.webUngoliant, "killNanDungorthebSpider").createTitle();
        tradeTolInGaurhothOrcTrader = new LOTRAchievement(Categories.taurNuFuin, 20, LOTRMod.silverCoin, "tradeTolInGaurhothOrcTrader").setRequiresAlly(LOTRFAFactions.tolInGaurhoth);
        tradeTolInGaurhothOrcCaptain = new LOTRAchievement(Categories.taurNuFuin, 21, LOTRMod.silverCoin, "tradeTolInGaurhothOrcCaptain").setRequiresAlly(LOTRFAFactions.tolInGaurhoth);
        
        // Ossiriand
        alignment_laegrim = createAlignmentAchievements(LOTRFAFactions.laegrim, Categories.ossiriand, 1);
        enterOssiriand = createBiomeAchievement(Categories.ossiriand, 4, new ItemStack(LOTRMod.sapling, 1, 3), "enterOssiriand");
        useLaegrimTable = new LOTRAchievement(Categories.ossiriand, 5, LOTRFABlocks.craftingTableLaegrim, "useLaegrimTable").setRequiresAlly(LOTRFAFactions.laegrim);
        doMiniquestLaegrim = new LOTRAchievement(Categories.ossiriand, 6, LOTRMod.redBook, "doMiniquestLaegrim").setRequiresAlly(LOTRFAFactions.laegrim);
        killLaegrimElf = new LOTRAchievement(Categories.ossiriand, 7, LOTRMod.elfBone, "killLaegrimElf").setRequiresEnemy(LOTRFAFactions.tolInGaurhoth).createTitle();
        tradeLaegrimTrader = new LOTRAchievement(Categories.ossiriand, 8, LOTRMod.silverCoin, "tradeLaegrimTrader").setRequiresAlly(LOTRFAFactions.laegrim);
        tradeLaegrimLord = new LOTRAchievement(Categories.ossiriand, 9, LOTRMod.silverCoin, "tradeLaegrimLord").setRequiresAlly(LOTRFAFactions.laegrim);

        // West Beleriand
        alignment_nargothrond = createAlignmentAchievements(LOTRFAFactions.nargothrond, Categories.westBeleriand, 1);
        enterWestBeleriand = createBiomeAchievement(Categories.westBeleriand, 4, Blocks.grass, "enterWestBeleriand");
        enterTaurEnFaroth = createBiomeAchievement(Categories.westBeleriand, 5, new ItemStack(LOTRMod.sapling5, 1, 0), "enterTaurEnFaroth");
        enterNargothrond = createBiomeAchievement(Categories.westBeleriand, 6, new ItemStack(LOTRMod.brick4, 1, 2), "enterNargothrond");
        enterTalathDirnen = createBiomeAchievement(Categories.westBeleriand, 7, LOTRMod.rivendellBow, "enterTalathDirnen");
        enterNanTathren = createBiomeAchievement(Categories.westBeleriand, 8, new ItemStack(LOTRMod.sapling6, 1, 1), "enterNanTathren");
        enterValeSirion = createBiomeAchievement(Categories.westBeleriand, 9, Blocks.sapling, "enterValeSirion");
        enterWoodNuath = createBiomeAchievement(Categories.westBeleriand, 10, Blocks.sapling, "enterWoodNuath");
        enterArvernien = createBiomeAchievement(Categories.westBeleriand, 11, Blocks.grass, "enterArvernien");
        enterNimbrethil = createBiomeAchievement(Categories.westBeleriand, 12, new ItemStack(Blocks.sapling, 1, 2), "enterNimbrethil");
        enterIvrin = createBiomeAchievement(Categories.westBeleriand, 13, Blocks.flowing_water, "enterIvrin");
        enterAelinUial = createBiomeAchievement(Categories.westBeleriand, 14, new ItemStack(Blocks.tallgrass, 1, 2), "enterAelinUial");
        enterLisgardh = createBiomeAchievement(Categories.westBeleriand, 15, new ItemStack(LOTRMod.reeds, 1, 0), "enterLisgardh");
        useNargothrondTable = new LOTRAchievement(Categories.westBeleriand, 16, LOTRFABlocks.craftingTableNargothrond, "useNargothrondTable").setRequiresAlly(LOTRFAFactions.nargothrond);
        doMiniquestNargothrond = new LOTRAchievement(Categories.westBeleriand, 17, LOTRMod.redBook, "doMiniquestNargothrond").setRequiresAlly(LOTRFAFactions.nargothrond);
        killNargothrondElf = new LOTRAchievement(Categories.westBeleriand, 18, LOTRMod.elfBone, "killNargothrondElf").setRequiresEnemy(LOTRFAFactions.nargothrond).createTitle();
        tradeNargothrondTrader = new LOTRAchievement(Categories.westBeleriand, 19, LOTRMod.silverCoin, "tradeNargothrondTrader").setRequiresAlly(LOTRFAFactions.nargothrond);
        tradeNargothrondLord = new LOTRAchievement(Categories.westBeleriand, 20, LOTRMod.silverCoin, "tradeNargothrondLord").setRequiresAlly(LOTRFAFactions.nargothrond);
        wearFullNargothrondRanger = new LOTRAchievement(Categories.westBeleriand, 22, LOTRFAItems.nargothrondRangerChestplate, "wearFullNargothrondRanger");
        
        // Amon Rudh
        alignment_pettyDwarf = createAlignmentAchievements(LOTRFAFactions.pettyDwarf, Categories.amonRudh, 1);
        enterAmonRudh = createBiomeAchievement(Categories.amonRudh, 4, new ItemStack(LOTRFABlocks.brick, 1, 0), "enterAmonRudh");
        usePettyDwarvenTable = new LOTRAchievement(Categories.amonRudh, 5, LOTRFABlocks.craftingTablePettyDwarf, "usePettyDwarvenTable").setRequiresAlly(LOTRFAFactions.pettyDwarf);
        doMiniquestPettyDwarf = new LOTRAchievement(Categories.amonRudh, 6, LOTRMod.redBook, "doMiniquestPettyDwarf").setRequiresAlly(LOTRFAFactions.pettyDwarf);
        killPettyDwarf = new LOTRAchievement(Categories.amonRudh, 7, LOTRMod.dwarfBone, "killPettyDwarf").setRequiresEnemy(LOTRFAFactions.pettyDwarf).createTitle();
        tradePettyDwarfTrader = new LOTRAchievement(Categories.amonRudh, 8, LOTRMod.silverCoin, "tradePettyDwarfTrader").setRequiresAlly(LOTRFAFactions.pettyDwarf);
        tradePettyDwarfCommander = new LOTRAchievement(Categories.amonRudh, 9, LOTRMod.silverCoin, "tradePettyDwarfCommander").setRequiresAlly(LOTRFAFactions.pettyDwarf);
        wearFullPettyDwarf = new LOTRAchievement(Categories.amonRudh, 11, LOTRFAItems.pettyDwarfChestplate, "wearFullPettyDwarf");

        // Dor Daidelos
        alignment_dorDaidelos = createAlignmentAchievements(LOTRFAFactions.dorDaidelos, Categories.dorDaidelos, 1);
        enterNanGuruthos = createBiomeAchievement(Categories.dorDaidelos, 5, LOTRMod.scimitarUruk, "enterNanGuruthos");
        enterEredEngrin = createBiomeAchievement(Categories.dorDaidelos, 6, LOTRFABlocks.rockSarllith, "enterEredEngrin");
        enterHelcaraxe = createBiomeAchievement(Categories.dorDaidelos, 7, Blocks.packed_ice, "enterHelcaraxe");
        useDorDaidelosTable = new LOTRAchievement(Categories.dorDaidelos, 8, LOTRFABlocks.craftingTableDorDaidelos, "useDorDaidelosTable").setRequiresAlly(LOTRFAFactions.dorDaidelos);
        doMiniquestDorDaidelos = new LOTRAchievement(Categories.dorDaidelos, 9, LOTRMod.redBook, "doMiniquestDorDaidelos").setRequiresAlly(LOTRFAFactions.dorDaidelos);
        killDorDaidelosOrc = new LOTRAchievement(Categories.dorDaidelos, 10, LOTRMod.orcBone, "killDorDaidelosOrc").setRequiresEnemy(LOTRFAFactions.dorDaidelos).createTitle();
        killUtumnoRemnantOrc = new LOTRAchievement(Categories.dorDaidelos, 11, LOTRMod.helmetUruk, "killUtumnoRemnantOrc").setRequiresEnemy(LOTRFAFactions.dorDaidelos).createTitle();
        killDorDaidelosIceWarg = new LOTRAchievement(Categories.dorDaidelos, 12, LOTRMod.stalactiteIce, "killDorDaidelosIceWarg").setRequiresEnemy(LOTRFAFactions.dorDaidelos).createTitle();
        tradeDorDaidelosOrcTrader = new LOTRAchievement(Categories.dorDaidelos, 13, LOTRMod.silverCoin, "tradeDorDaidelosOrcTrader").setRequiresAlly(LOTRFAFactions.dorDaidelos);
        tradeDorDaidelosOrcCaptain = new LOTRAchievement(Categories.dorDaidelos, 14, LOTRMod.silverCoin, "tradeDorDaidelosOrcCaptain").setRequiresAlly(LOTRFAFactions.dorDaidelos);
        tradeUtumnoRemnantOrcCaptain = new LOTRAchievement(Categories.dorDaidelos, 15, LOTRMod.silverCoin, "tradeUtumnoRemnantOrcCaptain").setRequiresAlly(LOTRFAFactions.dorDaidelos);
        wearFullDorDaidelos = new LOTRAchievement(Categories.dorDaidelos, 16, LOTRFAItems.dorDaidelosChestplate, "wearFullDorDaidelos");

        // Ocean
        enterBalar = createBiomeAchievement(Category.OCEAN, 62, LOTRMod.pearl, "enterBalar");

        // Blue Mountains
        enterNogrodMtns = createBiomeAchievement(Category.BLUE_MOUNTAINS, 99, new ItemStack(LOTRMod.rock, 1, 3), "enterNogrodMtns");
        enterBelegostMtns = createBiomeAchievement(Category.BLUE_MOUNTAINS, 66,  new ItemStack(LOTRMod.rock, 1, 3), "enterBelegostMtns");

        // Others
        enterSouthBeleriand = createBiomeAchievement(Categories.southBeleriand, 4, Blocks.grass, "enterSouthBeleriand");
        enterTaurImDuinath = createBiomeAchievement(Categories.southBeleriand, 5, new ItemStack(Blocks.sapling, 1, 0), "enterTaurImDuinath");
    }

    private static LOTRAchievement[] createAlignmentAchievements(LOTRFaction faction, Category category, int startID) {
        return createAlignmentAchievements(faction, category, startID, faction.codeName());
    }

    private static LOTRAchievement[] createAlignmentAchievements(LOTRFaction faction, Category category, int startID, String name) {
        LOTRAchievement[] achievements = new LOTRAchievement[3];
        achievements[0] = createAlignmentAchievement(faction, category, startID, 10, name);
        achievements[1] = createAlignmentAchievement(faction, category, ++startID, 100, name);
        achievements[2] = createAlignmentAchievement(faction, category, ++startID, 1000, name);

        return achievements;
    }

    private static LOTRAchievement createAlignmentAchievement(LOTRFaction faction, Category category, int id, int alignment, String name) {
        return new LOTRAchievement(category, id, LOTRMod.goldRing, "alignmentGood" + alignment + "_" + name.toUpperCase()).setRequiresAlly(faction).setSpecial();
    }

    private static LOTRAchievement createBiomeAchievement(Category category, int id, Block block, String name) {
        return createBiomeAchievement(category, id, new ItemStack(block), name);
    }

    private static LOTRAchievement createBiomeAchievement(Category category, int id, Item item, String name) {
        return createBiomeAchievement(category, id, new ItemStack(item), name);
    }

    private static LOTRAchievement createBiomeAchievement(Category category, int id, ItemStack itemstack, String name) {
        return new LOTRAchievement(category, id, itemstack, name).setBiomeAchievement();
    }

    private static void removeAchievement(LOTRAchievement achievement) {
        achievement.category.list.remove(achievement);
        achievement.category.dimension.allAchievements.remove(achievement);
    }

    private static void addAchievementToCategory(LOTRAchievement achievement, Category category, int id) {
        achievement.category = category;
        achievement.ID = id;
        category.list.add(achievement);
        category.dimension.allAchievements.add(achievement);
    }
    
    public static void runAchievementCheck(EntityPlayer player) {
        ArmorMaterial material = LOTRFAMaterial.getFullArmorMaterial(player);
        LOTRPlayerData data = LOTRLevelData.getData(player);
        
        if(material == LOTRFAMaterial.FEANORIAN.toArmorMaterial()) {
            data.addAchievement(wearFullFeanorian);
        }
        else if(material == LOTRFAMaterial.PETTY_DWARVEN.toArmorMaterial()) {
        	data.addAchievement(wearFullPettyDwarf);
        }
        else if(material == LOTRFAMaterial.DOR_DAIDELOS.toArmorMaterial()) {
        	data.addAchievement(wearFullDorDaidelos);
        }
        else if(material == LOTRFAMaterial.NARGOTHROND_RANGER.toArmorMaterial()) {
        	data.addAchievement(wearFullNargothrondRanger);
        }
        else if(material == LOTRFAMaterial.HOUSE_BOR.toArmorMaterial()) {
        	data.addAchievement(wearFullHouseBor);
        }
        else if(material == LOTRFAMaterial.HOUSE_BOR_CAPTAIN.toArmorMaterial()) {
        	data.addAchievement(wearFullHouseBorCaptain);
        }
    	else if(material == LOTRFAMaterial.BEOR.toArmorMaterial()) {
    		data.addAchievement(wearFullHouseBeor);
    	}
    	else if(material == LOTRFAMaterial.BRETHIL.toArmorMaterial()) {
    		data.addAchievement(wearFullBrethil);
    	}
		else if(material == LOTRFAMaterial.BRETHIL_RANGER.toArmorMaterial()) {
			data.addAchievement(wearFullBrethilRanger);
		}
		else if(material == LOTRFAMaterial.DOR_LOMIN.toArmorMaterial()) {
    		data.addAchievement(wearFullDorLomin);
    	}
    }

    public static class Categories {
        public static Category ardGalen;
        public static Category brethil;
        public static Category doriath;
        public static Category eastBeleriand;
        public static Category falas;
        public static Category marchMaedhros;
        public static Category gondolin;
        public static Category hithlum;
        public static Category lothlann;
        public static Category taurNuFuin;
        public static Category ossiriand;
        public static Category southBeleriand;
        public static Category westBeleriand;
        public static Category amonRudh;
        public static Category dorDaidelos;

        private static void init() {
            // TODO remove most base Category. referances
            hideCategoriesExcept(Category.OCEAN, Category.BLUE_MOUNTAINS, Category.GENERAL, Category.UTUMNO);

            ardGalen = LOTRFAEnumHelper.addAchievementCategory("ARD_GALEN", LOTRFAFactions.angband);
            brethil = LOTRFAEnumHelper.addAchievementCategory("BRETHIL", LOTRFAFactions.brethil);
            doriath = LOTRFAEnumHelper.addAchievementCategory("DORIATH", LOTRFAFactions.doriath);
            eastBeleriand = LOTRFAEnumHelper.addAchievementCategory("EAST_BELERIAND", LOTRFABiomes.beleriandEast);
            falas = LOTRFAEnumHelper.addAchievementCategory("FALAS", LOTRFAFactions.falathrim);
            marchMaedhros = LOTRFAEnumHelper.addAchievementCategory("MARCH_MAEDHROS", LOTRFAFactions.feanorians);
            gondolin = LOTRFAEnumHelper.addAchievementCategory("GONDOLIN", LOTRFAFactions.gondolin);
            hithlum = LOTRFAEnumHelper.addAchievementCategory("HITHLUM", LOTRFAFactions.hithlum);
            lothlann = LOTRFAEnumHelper.addAchievementCategory("LOTHLANN", LOTRFAFactions.houseUlfang);
            taurNuFuin = LOTRFAEnumHelper.addAchievementCategory("LADROS", LOTRFAFactions.tolInGaurhoth);
            ossiriand = LOTRFAEnumHelper.addAchievementCategory("OSSIRIAND", LOTRFAFactions.laegrim);
            southBeleriand = LOTRFAEnumHelper.addAchievementCategory("SOUTH_BELERIAND", LOTRFaction.FANGORN);
            westBeleriand = LOTRFAEnumHelper.addAchievementCategory("WEST_BELERIAND", LOTRFAFactions.nargothrond);
            amonRudh = LOTRFAEnumHelper.addAchievementCategory("AMON_RUDH", LOTRFAFactions.pettyDwarf);
            dorDaidelos = LOTRFAEnumHelper.addAchievementCategory("DOR_DAIDELOS", LOTRFAFactions.dorDaidelos);
        }

        private static void hideCategoriesExcept(Category... categories) {
            categories: for (Category category : Category.values()) {
                for (Category excludedCategory : categories) {
                    if (category == excludedCategory) continue categories;
                }

                category.dimension.achievementCategories.remove(category);
                
                List<LOTRAchievement> achievementsToRemove = new ArrayList<LOTRAchievement>();
                List<LOTRTitle> titlesToRemove = new ArrayList<LOTRTitle>();
                for (LOTRAchievement achievement : category.list) {
                	achievementsToRemove.add(achievement);
                	if(achievement.getAchievementTitle() != null) titlesToRemove.add(achievement.getAchievementTitle());
                }
                for (LOTRAchievement achievement : achievementsToRemove) removeAchievement(achievement);
                for(LOTRTitle title : titlesToRemove) LOTRFAReflectionHelper.removeTitle(title);
            }

        }
    }
}
