Welcome to the LOTR FA submod repository. This is a private repository so any files in her aren't allowed to be shared with others.

__Getting started__

If you're a new dev and you want to contribute to the repository directly you need to install a few programs and tool first.

1. Make a gitlab account if you haven't already
2. Download git https://git-scm.com/downloads and also enable the lfs option
3. Clone the repository, this can be done by going to a folder on your pc and right clicking and opening the git bash here. Once the git bash is open type the command `git clone https://gitlab.com/eras-of-arda/lotrfa.git`
4. Enter your gitlab credentials, this step only needs to be done once for your interaction with gitlab with the git bash.

From here on out the instruction are only if you want be able to build the mod yourself.
5. Open a command line terminal in your main project folder (lotrfa folder). The easiest way to do this in windows is by holding ctrl + shift and right cliking in the folder en selecting the option open powershell here.
6. In powershell type `./gradlew setupDecompWorkspace` this command will take a while to complete. If it doesn't work try `gradlew setupDecompWorkspace`
7. To build the mod either click the `build.cmd` script file on windows or `build.sh` for linux and mac. Another way to do it is by typing `.\build [name]` in a command terminal.

From here on out the instruction are only for coders.
8. Download either eclipse or intellij (most of us use eclipse)
9. Type `.\graldew eclipse` in a command line terminal in the root folder.
10. Open eclipse and select file => import => gradlew project and select the lotrfa root folder and let eclipse import it.
11. You can also setup the debug enviorment but I am to lazy to explain that so contanct dwarfy so he can tell you how to do it.

__Locations__

All code is located in `src/main/java`
All assets are located in `lotr_packaged/assets`

__Git branches__

Once you have setup everything and you are ready to start contributing make sure that you're contributing to the development branch.
There are two branches in the repository the master branch which is up to date with the latest release/beta of the mod and there is the dev branch which is the development branch. All changes should be commited to the development branch.
To switch branches type `git checkout [branch name]` so `git checkout dev`.

__Contributing__

To push something to gitlab you need to use git.
Before you change things you should let git check gitlab to see if there are any new changes with `git pull`.
Make all your need changes to the files, then you can use the command `git status` to see all the changes.
Type `git add [file name]` to add it to stack. If you want to add all your files you can do `git add .`
Once you have added files to the stack you can commit it with the command `git commit -m "[commit message]"`. A commit can be seen as a group of changes to the files. The commit message should be a short and desciptive message of what changed in your commit.
If you have commited all your files you can use the command `git push` to push all your commits to gitlab.