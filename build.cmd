@echo off
setlocal EnableDelayedExpansion

set startTime=%time%


rem Build fase
call gradlew build

echo Clearing lotr_packaged\...
rd /S /Q lotr_packaged\eoa
rd /S /Q lotr_packaged\META-INF
del /Q lotr_packaged\mcmod.info

echo Unpacking jar...
jar xf build\libs\lotrfa-build.jar

echo Moving class files to lotr_packaged
move eoa lotr_packaged\eoa
move META-INF lotr_packaged\META-INF
move mcmod.info lotr_packaged\mcmod.info

echo Cleaning workspace
rd /S /Q assets\

cd lotr_packaged

echo Packing new jar with manifest...
jar cmf META-INF\MANIFEST.MF ..\lotrfa-%1.jar .\*  

cd ..



set endTime=%time%

rem Get elapsed time:
set "end=!endTime:%time:~8,1%=%%100)*100+1!"  &  set "start=!startTime:%time:~8,1%=%%100)*100+1!"
set /A "elap=((((10!end:%time:~2,1%=%%100)*60+1!%%100)-((((10!start:%time:~2,1%=%%100)*60+1!%%100)"

rem Convert elapsed time to HH:MM:SS:CC format:
set /A "cc=elap%%100+100, elap/=100, ss=elap%%60+100, elap/=60, mm=elap%%60+100"

echo Elapsed time: %mm:~1%%time:~2,1%%ss:~1%%time:~8,1%%cc:~1%